ALTER TABLE main_event ADD sub_type VARCHAR2(30); 
COMMENT ON COLUMN main_event.sub_type IS '시리즈OR링크여부';

UPDATE main_event SET sub_type = 'series';