/*------------------------------------------------------------------------------
-- 개체 이름: SCIENCECENTER.GET_COURSE_NAME
-- 만든 날짜: 2015-11-16 오후 5:48:40
-- 마지막으로 수정한 날짜: 2016-01-06 오후 5:14:00
-- 상태: VALID
------------------------------------------------------------------------------*/
CREATE OR REPLACE FUNCTION GET_COURSE_NAME(V_COMPANY_CD IN VARCHAR2, V_COURSE_CD IN VARCHAR2, V_ONLINE_FLAG IN CHAR)
	RETURN VARCHAR2
AS
	V_COURSE_NAME		TL_COURSE.COURSE_NAME%TYPE;

BEGIN

	SELECT DECODE(V_ONLINE_FLAG, 'N', CS.COURSE_NAME, CS.ONLINE_COURSE_NAME)
	INTO V_COURSE_NAME
	FROM TL_COURSE CS
	WHERE CS.COMPANY_CD = V_COMPANY_CD AND CS.COURSE_CD = V_COURSE_CD;

	RETURN V_COURSE_NAME;

END;



