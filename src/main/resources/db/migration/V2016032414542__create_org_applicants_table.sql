CREATE TABLE org_applicants(  
  id NUMBER PRIMARY KEY, 
  org_program_id NUMBER, 
  user_id NUMBER, 
  title VARCHAR2(255), 
  description CLOB, 
  members NUMBER, 
  application_type VARCHAR2(30), 
  application_time TIMESTAMP, 
  reg_dttm TIMESTAMP, 
  updt_dttm TIMESTAMP,
  
  CONSTRAINT org_program_applicant_fk FOREIGN KEY (org_program_id) REFERENCES org_program (id) ON DELETE SET NULL
);


CREATE SEQUENCE org_applicants_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;