/*------------------------------------------------------------------------------
-- 개체 이름: SCIENCECENTER.GET_MEMBER_NAME
-- 만든 날짜: 2015-11-16 오후 5:48:40
-- 마지막으로 수정한 날짜: 2016-01-06 오후 5:14:00
-- 상태: VALID
------------------------------------------------------------------------------*/
CREATE OR REPLACE FUNCTION GET_MEMBER_NAME(V_COMPANY_CD IN VARCHAR2, V_MEMBER_NO IN VARCHAR2)
	RETURN VARCHAR2
AS

	V_MEMBER_NAME		TM_MEMBER.MEMBER_NAME%TYPE;

BEGIN
	V_MEMBER_NAME := '';

	IF V_MEMBER_NO IS NOT NULL THEN
		SELECT MB.MEMBER_NAME
		INTO V_MEMBER_NAME
		FROM TM_MEMBER MB
		WHERE MB.COMPANY_CD = V_COMPANY_CD AND MB.MEMBER_NO = V_MEMBER_NO;
	END IF;

	RETURN V_MEMBER_NAME;

	EXCEPTION
	WHEN NO_DATA_FOUND THEN
		V_MEMBER_NAME := V_MEMBER_NO;

		RETURN V_MEMBER_NAME;

END;



