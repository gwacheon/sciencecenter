ALTER TABLE boards ADD (temp CLOB);
UPDATE boards SET temp=content;
ALTER TABLE boards DROP COLUMN content;
ALTER TABLE boards RENAME COLUMN temp TO content;