ALTER TABLE career_schedules ADD (
	tue_word VARCHAR2(190),
	tue_available NUMBER(1),
	wed_word VARCHAR2(190),
	wed_available NUMBER(1),
	thu_word VARCHAR2(190),
	thu_available NUMBER(1),
	fri_word VARCHAR2(190),
	fri_available NUMBER(1)
);