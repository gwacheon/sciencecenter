DROP TABLE holydays;
DROP SEQUENCE holydays_seq;

CREATE TABLE holidays(
	id NUMBER PRIMARY KEY,
	holyday DATE,
	time_type NUMBER(9),
	name VARCHAR2(100),
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP
);

CREATE SEQUENCE holidays_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;