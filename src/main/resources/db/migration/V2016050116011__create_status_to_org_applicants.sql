ALTER TABLE org_applicants ADD application_status VARCHAR(50); 
COMMENT ON COLUMN org_applicants.application_status IS '단체 예약 상태 값';
CREATE BITMAP INDEX org_status_bitmap_idx ON org_applicants (application_status);