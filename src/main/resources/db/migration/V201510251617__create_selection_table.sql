CREATE TABLE selections(
	id NUMBER PRIMARY KEY,
	article_id NUMBER,
	example VARCHAR2(255),
	seq NUMBER,
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP,
	
	CONSTRAINT article_selections_fk FOREIGN KEY (article_id) REFERENCES articles (id) ON DELETE CASCADE
);
 
 CREATE SEQUENCE selections_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;