INSERT INTO users(id, email, name, password, confirmed, reg_dttm, updt_dttm)
VALUES(users_seq.NEXTVAL, 'admin@admin.com', '관리자', '$2a$10$V2mjPxdUgqEW45EI2diSKOaGqEFtR0O4.0d6qUtjazOLV3PgXSaPm', 1, SYSDATE, SYSDATE);

INSERT INTO user_roles (id, user_id, role, reg_dttm, updt_dttm)
VALUES (user_roles_seq.NEXTVAL, 1, 'ROLE_ADMIN', SYSDATE, SYSDATE);