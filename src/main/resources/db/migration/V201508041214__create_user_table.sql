CREATE TABLE users(
	id NUMBER PRIMARY KEY,
	email VARCHAR2(100) UNIQUE,
	name VARCHAR2(50),
	password VARCHAR2(255),
	phone VARCHAR2(30),
	confirmed NUMBER (1) DEFAULT 0,
	authentication_token VARCHAR2(255),
	reset_password_token VARCHAR2(255),
	sign_in_count NUMBER,
	last_sign_in_at TIMESTAMP,
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP
);

CREATE SEQUENCE users_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;