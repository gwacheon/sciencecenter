CREATE TABLE program_add_infos(
	id NUMBER PRIMARY KEY,
	program_id NUMBER NOT NULL,
	title VARCHAR2(255),
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP,
	CONSTRAINT fk_program_program_add_infos FOREIGN KEY (program_id) REFERENCES programs (id)
	ON DELETE CASCADE
);

CREATE SEQUENCE program_add_infos_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;