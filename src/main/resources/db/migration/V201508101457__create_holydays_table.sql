CREATE TABLE holydays(
	id NUMBER PRIMARY KEY,
	holyday DATE,
	time_type NUMBER(9),
	name VARCHAR2(100),
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP
);

CREATE SEQUENCE holydays_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;