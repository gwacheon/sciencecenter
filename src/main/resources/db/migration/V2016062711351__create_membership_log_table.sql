CREATE TABLE membership_log (
	member_no VARCHAR2(20),
	name VARCHAR2(50),
	reg_dttm TIMESTAMP
);

CREATE INDEX membership_log_no_idx
ON membership_log (member_no);

CREATE INDEX membership_log_name_idx
ON membership_log (name);