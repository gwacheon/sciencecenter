CREATE TABLE dinner_apply_users (
	id NUMBER PRIMARY KEY,
	user_id VARCHAR2(50),
	password VARCHAR2(200),
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP
);

CREATE SEQUENCE dinner_apply_users_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;