/*------------------------------------------------------------------------------
-- 개체 이름: sciecnecenter.GET_ACADEMY_NAME
-- 만든 날짜: 2015-11-16 오후 5:48:40
-- 마지막으로 수정한 날짜: 2016-01-06 오후 5:14:00
-- 상태: VALID
------------------------------------------------------------------------------*/
CREATE OR REPLACE FUNCTION GET_ACADEMY_NAME(V_COMPANY_CD IN VARCHAR2, V_ACADEMY_CD IN VARCHAR2)
	RETURN VARCHAR2
AS
	V_ACADEMY_NAME		TL_ACADEMY.ACADEMY_NAME%TYPE;

BEGIN

	SELECT AC.ACADEMY_NAME
	INTO V_ACADEMY_NAME
	FROM TL_ACADEMY AC
	WHERE AC.COMPANY_CD = V_COMPANY_CD AND AC.ACADEMY_CD = V_ACADEMY_CD;

	RETURN V_ACADEMY_NAME;

END;



