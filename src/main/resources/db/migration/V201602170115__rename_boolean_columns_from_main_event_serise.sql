ALTER TABLE main_event_serise RENAME COLUMN hasNotice TO main_event_series_notice;
ALTER TABLE main_event_serise RENAME COLUMN hasQna TO main_event_series_qna;
ALTER TABLE main_event_serise RENAME COLUMN hasFaq TO main_event_series_faq;
ALTER TABLE main_event_serise RENAME COLUMN hasLibrary TO main_event_series_library;
ALTER TABLE main_event_serise RENAME COLUMN hasPictures TO main_event_series_pictures;
ALTER TABLE main_event_serise RENAME COLUMN hasWinning TO main_event_series_winning;
ALTER TABLE main_event_serise RENAME COLUMN hasRecommand TO main_event_series_recommend;