/*------------------------------------------------------------------------------
-- 개체 이름: sciecnecenter.GET_COMMON_NAME
-- 만든 날짜: 2015-11-16 오후 5:48:40
-- 마지막으로 수정한 날짜: 2016-01-06 오후 5:14:00
-- 상태: VALID
------------------------------------------------------------------------------*/
CREATE OR REPLACE FUNCTION GET_COMMON_NAME(V_COMPANY_CD IN VARCHAR2, V_GROUP_CD IN VARCHAR2, V_COMMON_CD IN VARCHAR2, V_LANG IN CHAR)
	RETURN VARCHAR2
AS
	V_COMMON_NAME		TA_COMMON.COMMON_NAME%TYPE;

BEGIN

	IF V_LANG = 'K' THEN
		SELECT CM.COMMON_NAME
		INTO V_COMMON_NAME
		FROM TA_COMMON CM
		WHERE CM.COMPANY_CD = V_COMPANY_CD AND CM.GROUP_CD = V_GROUP_CD AND CM.COMMON_CD = TRIM(V_COMMON_CD);
	ELSIF V_LANG = 'E' THEN
		SELECT NVL(CM.COMMON_ENG_NAME, CM.COMMON_NAME)
		INTO V_COMMON_NAME
		FROM TA_COMMON CM
		WHERE CM.COMPANY_CD = V_COMPANY_CD AND CM.GROUP_CD = V_GROUP_CD AND CM.COMMON_CD = TRIM(V_COMMON_CD);
	ELSE
		SELECT CM.COMMON_NAME
		INTO V_COMMON_NAME
		FROM TA_COMMON CM
		WHERE CM.COMPANY_CD = V_COMPANY_CD AND CM.GROUP_CD = V_GROUP_CD AND CM.COMMON_CD = TRIM(V_COMMON_CD);
	END IF;

	RETURN V_COMMON_NAME;

END;



