ALTER TABLE boards ADD written_by_admin NUMBER; 
COMMENT ON COLUMN boards.written_by_admin IS '관리자 작성 글 판단 표시';