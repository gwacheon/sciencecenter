/*------------------------------------------------------------------------------
-- 개체 이름: sciecnecenter.GET_PLACE_NAME
-- 만든 날짜: 2015-11-16 오후 5:48:40
-- 마지막으로 수정한 날짜: 2016-01-06 오후 5:14:00
-- 상태: VALID
------------------------------------------------------------------------------*/
CREATE OR REPLACE FUNCTION GET_PLACE_NAME(V_COMPANY_CD IN VARCHAR2, V_FACILITY_CD IN VARCHAR2, V_PLACE_CD IN VARCHAR2)
	RETURN VARCHAR2
AS
	V_PLACE_NAME		TL_PLACE.PLACE_NAME%TYPE;

BEGIN

	SELECT PL.PLACE_NAME
	INTO V_PLACE_NAME
	FROM TL_PLACE PL
	WHERE PL.COMPANY_CD = V_COMPANY_CD AND PL.FACILITY_CD = V_FACILITY_CD AND PL.PLACE_CD = V_PLACE_CD;

	RETURN V_PLACE_NAME;

END;



