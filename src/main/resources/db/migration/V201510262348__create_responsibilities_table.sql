CREATE TABLE responsibilities(
	id NUMBER PRIMARY KEY,
	controller VARCHAR2(255),
	action VARCHAR2(255),
	dept VARCHAR2(255),
	member VARCHAR2(255),
	phone VARCHAR2(255),
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP
);
 
 CREATE SEQUENCE responsibilities_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;