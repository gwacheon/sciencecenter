ALTER TABLE structure_unavailables ADD (
	begin_time TIMESTAMP,
	end_time TIMESTAMP
);

ALTER TABLE structure_unavailables DROP (
	st_time
);
