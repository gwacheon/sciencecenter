ALTER TABLE surveys ADD multiple NUMBER (1) DEFAULT 0;
ALTER TABLE surveys ADD ip_address VARCHAR2(255);
CREATE INDEX survey_ip_address_idx ON surveys(ip_address);

ALTER TABLE articles ADD render_type VARCHAR2(30) DEFAULT 'horizontal';

ALTER TABLE selections ADD direct_input NUMBER(1) DEFAULT 0;
