CREATE TABLE voluntary_times(
	id NUMBER PRIMARY KEY,
	voluntary_id NUMBER,
	title VARCHAR2(255),
	begin_time DATE,
	end_time DATE,
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP,
	
	CONSTRAINT voluntary_voluntary_times_fk FOREIGN KEY (voluntary_id) REFERENCES voluntaries (id) ON DELETE CASCADE
);
 
 CREATE SEQUENCE voluntary_times_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;