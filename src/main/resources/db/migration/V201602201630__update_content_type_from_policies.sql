ALTER TABLE policies ADD (temp CLOB);
UPDATE policies SET temp=content;
ALTER TABLE policies DROP COLUMN content;
ALTER TABLE policies RENAME COLUMN temp TO content;