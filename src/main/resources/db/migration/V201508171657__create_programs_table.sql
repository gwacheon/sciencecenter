CREATE TABLE programs(
	id NUMBER PRIMARY KEY,
	program_no VARCHAR2(100) UNIQUE,
	title VARCHAR2(255),
	content VARCHAR2(4000),
	age_limited NUMBER (1),
	age_type VARCHAR2(20),
	age_begin NUMBER,
	age_end NUMBER,
	homepage_reserved NUMBER (1),
	refund_dttm TIMESTAMP,
	begin_time TIMESTAMP,
	end_time TIMESTAMP,
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP
);

CREATE SEQUENCE programs_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;