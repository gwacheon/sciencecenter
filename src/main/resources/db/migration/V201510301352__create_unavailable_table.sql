CREATE TABLE equipunavailable(
	id NUMBER PRIMARY KEY,
	equipmentId NUMBER,
	equiSerialId NUMBER,
	beginTime TIMESTAMP,
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP,
	
	CONSTRAINT equip_unavailable_reserve_fk FOREIGN KEY (equipmentId) REFERENCES equipments (id) ON DELETE CASCADE,
	CONSTRAINT serials_unavailable_reserve_fk FOREIGN KEY (equiSerialId) REFERENCES equip_serials (id) ON DELETE CASCADE
);
 
 CREATE SEQUENCE equipunavailable_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;