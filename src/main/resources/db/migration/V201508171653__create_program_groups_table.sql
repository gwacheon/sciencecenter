CREATE TABLE program_groups(
	id NUMBER PRIMARY KEY,
	group_no VARCHAR2(100) UNIQUE,
	category VARCHAR2(255),
	open_year NUMBER,
	status VARCHAR2(40),
	online_open TIMESTAMP,
	online_close TIMESTAMP,
	pre_reservation_time TIMESTAMP,
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP
);

CREATE SEQUENCE program_groups_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;