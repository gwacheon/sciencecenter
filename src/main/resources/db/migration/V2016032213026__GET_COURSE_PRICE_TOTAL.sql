/*------------------------------------------------------------------------------
-- 개체 이름: SCIENCECENTER.GET_COURSE_PRICE_TOTAL
-- 만든 날짜: 2015-11-16 오후 5:48:41
-- 마지막으로 수정한 날짜: 2016-01-06 오후 5:14:00
-- 상태: VALID
------------------------------------------------------------------------------*/
CREATE OR REPLACE FUNCTION GET_COURSE_PRICE_TOTAL(V_COMPANY_CD IN VARCHAR2, V_COURSE_CD IN VARCHAR2)
	RETURN VARCHAR2
AS
	V_PRICE_TOT		TL_COURSE_PRICE_DETAIL.COURSE_PRICE%TYPE;

BEGIN

	SELECT SUM(CP.COURSE_PRICE)
	INTO V_PRICE_TOT
	FROM TL_COURSE_PRICE_DETAIL CP
	WHERE CP.COMPANY_CD = V_COMPANY_CD AND CP.COURSE_CD = V_COURSE_CD;

	RETURN V_PRICE_TOT;

END;



