CREATE TABLE main_event_serise (
    id NUMBER PRIMARY KEY,
    main_event_id NUMBER,
    name VARCHAR2(100),
    contents VARCHAR2(4000 BYTE),
    seq NUMBER,
    hasNotice NUMBER,
    hasQna NUMBER,
    hasFaq NUMBER,
    hasLibrary NUMBER,
    hasPictures NUMBER,
    hasWinning NUMBER,
    hasRecommand NUMBER,    
    
    CONSTRAINT main_event_serise_fk FOREIGN KEY (main_event_id) REFERENCES main_event (id) ON DELETE CASCADE
);

CREATE SEQUENCE main_event_serise_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;