CREATE TABLE program_wdays(
	id NUMBER PRIMARY KEY,
	program_id NUMBER NOT NULL,
	wday NUMBER NOT NULL,
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP,
	CONSTRAINT fk_program_program_wday FOREIGN KEY (program_id) REFERENCES programs (id)
);

CREATE SEQUENCE program_wdays_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;