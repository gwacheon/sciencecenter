CREATE TABLE dinner_apply_times (
	id NUMBER PRIMARY KEY,
	apply_begin_time TIMESTAMP,
	apply_end_time TIMESTAMP,
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP
);

CREATE SEQUENCE dinner_apply_times_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;