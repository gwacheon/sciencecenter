CREATE TABLE structure_unavailables (
	id NUMBER PRIMARY KEY,
	structure_id NUMBER,
	st_date DATE,
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP,
	
	CONSTRAINT st_st_unavailables_fk FOREIGN KEY (structure_id) REFERENCES structures (id) ON DELETE CASCADE
);

CREATE SEQUENCE structure_unavailables_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;