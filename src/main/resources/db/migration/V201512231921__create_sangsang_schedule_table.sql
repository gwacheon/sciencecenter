CREATE TABLE sangsang_schedule_title (
	id NUMBER PRIMARY KEY,
	title VARCHAR2(255),
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP
);


CREATE TABLE sangsang_schedules (
	id NUMBER PRIMARY KEY,
	sangsang_schedule_title_id NUMBER,
	s_date TIMESTAMP,
	title VARCHAR2(255),
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP,
	
	CONSTRAINT fk_sangsang_schedule_title FOREIGN KEY (sangsang_schedule_title_id) REFERENCES sangsang_schedule_title (id)
);

CREATE SEQUENCE sangsang_schedules_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;
 
 INSERT INTO sangsang_schedule_title(id, reg_dttm, updt_dttm)
 VALUES (1, SYSDATE, SYSDATE);