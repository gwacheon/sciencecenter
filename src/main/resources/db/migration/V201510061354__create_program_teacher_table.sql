CREATE TABLE program_teacher(
	id NUMBER PRIMARY KEY,
	program_id NUMBER,
	teacher_id NUMBER,
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP,
	
	CONSTRAINT program_teacher_fk FOREIGN KEY (program_id) REFERENCES programs (id),
	CONSTRAINT teacher_program_fk FOREIGN KEY (teacher_id) REFERENCES teachers (id)
);