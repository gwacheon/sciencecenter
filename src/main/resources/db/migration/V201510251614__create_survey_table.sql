CREATE TABLE surveys(
	id NUMBER PRIMARY KEY,
	title VARCHAR2(255),
	begin_time TIMESTAMP,
	end_time TIMESTAMP,
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP
);
 
 CREATE SEQUENCE surveys_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;