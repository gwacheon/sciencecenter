CREATE TABLE user_roles(
	id NUMBER PRIMARY KEY,
	user_id NUMBER NOT NULL,
	role VARCHAR2(45) NOT NULL,
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP,
	CONSTRAINT uc_user_id_role UNIQUE (user_id, role),
	CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES users (id)
);

CREATE SEQUENCE user_roles_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;