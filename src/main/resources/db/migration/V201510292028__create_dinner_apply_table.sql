CREATE TABLE dinner_applies(
	id NUMBER PRIMARY KEY,
	department VARCHAR2(100),
	name VARCHAR2(50),
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP
);

CREATE SEQUENCE dinner_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;
 