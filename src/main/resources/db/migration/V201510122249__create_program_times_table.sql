CREATE TABLE program_times(
	id NUMBER PRIMARY KEY,
	program_id NUMBER NOT NULL,
	title VARCHAR2(255),
	begin_time TIMESTAMP,
	end_time TIMESTAMP,
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP,
	CONSTRAINT fk_program_program_times FOREIGN KEY (program_id) REFERENCES programs (id)
	ON DELETE CASCADE
);

CREATE SEQUENCE program_times_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;