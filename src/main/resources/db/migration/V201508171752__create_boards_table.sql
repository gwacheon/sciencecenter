CREATE TABLE boards(
	id NUMBER PRIMARY KEY,
	type_seq NUMBER,
	user_id NUMBER,
	board_type VARCHAR2(20),
	title VARCHAR2(100),
	content VARCHAR2(4000 BYTE),
	count NUMBER,
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP
);

CREATE SEQUENCE boards_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;
 
CREATE SEQUENCE notice_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;