CREATE TABLE main_banners(
	id NUMBER PRIMARY KEY,
	type_seq NUMBER,
	title VARCHAR2(100),
	is_visible NUMBER(1) DEFAULT 1 NOT NULL,
	banner_image_url VARCHAR2(200),
	locale VARCHAR2(50),
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP
);

CREATE SEQUENCE banners_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;