CREATE TABLE career_schedules (
	id NUMBER PRIMARY KEY,
	career_id NUMBER,
	title VARCHAR2(190),
	cs_date DATE,
	available NUMBER (1),
	seq NUMBER,
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP,
	
	CONSTRAINT career_cs_fk FOREIGN KEY (career_id) REFERENCES careers (id) ON DELETE CASCADE
);

CREATE SEQUENCE career_schedules_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;