/*------------------------------------------------------------------------------
-- 개체 이름: GET_MANAGER_NAME
-- 만든 날짜: 2015-11-16 오후 5:48:40
-- 마지막으로 수정한 날짜: 2016-01-06 오후 5:14:00
-- 상태: VALID
------------------------------------------------------------------------------*/
CREATE OR REPLACE FUNCTION GET_MANAGER_NAME(V_MANAGER_NO IN VARCHAR2)
	RETURN VARCHAR2
AS

	V_MANAGER_NAME		CMS_AD_ME_TB.NAME%TYPE;

BEGIN
	V_MANAGER_NAME := '';

	IF V_MANAGER_NO IS NOT NULL THEN
		SELECT MG.NAME
		INTO V_MANAGER_NAME
		FROM CMS_AD_ME_TB MG
		WHERE TO_CHAR(MG.IDX) = V_MANAGER_NO;
	END IF;

	RETURN V_MANAGER_NAME;

	EXCEPTION
	WHEN NO_DATA_FOUND THEN
		V_MANAGER_NAME := V_MANAGER_NO;

		RETURN V_MANAGER_NAME;

END;



