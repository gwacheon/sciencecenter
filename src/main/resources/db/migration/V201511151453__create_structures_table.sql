CREATE TABLE structures (
	id NUMBER PRIMARY KEY,
	title VARCHAR2(190),
	area NUMBER,
	person VARCHAR2(30),
	price_spring NUMBER,
	price_summer NUMBER,
	price_winter NUMBER,
	etc VARCHAR2(190),
	seq NUMBER,
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP
);

CREATE SEQUENCE structures_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;