
/* 1. 전시관람  */
CREATE SEQUENCE b_exhibitionReview_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

/* 2. 과학교육  */
CREATE SEQUENCE b_educationNotice_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_classBoard_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_reverseScience_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_tree_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_educationArchieve_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;


/* 3. 문화행사  */
CREATE SEQUENCE b_mainEventSeriesBasicInfo_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_mainEventSeriesNotice_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_mainEventSeriesQna_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_mainEventSeriesFaq_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_mainEventSeriesLibrary_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_mainEventSeriesPictures_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_mainEventSeriesWinning_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_mainEventSeriesRecommend_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

  /* 5. 고객소통  */
CREATE SEQUENCE b_lostProperty_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_faqTotal_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_faqScience_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_faqEducation_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_faqRole_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_faqFacilities_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_faqRestaurant_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_faqUser_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_faqDisplay_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_faqReservation_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_faqPark_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_faqHomepage_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_faqMarketing_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_normalLibrary_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_roleLibrary_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_scienceColumn_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;
  
CREATE SEQUENCE b_opinions_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

/* 6. 과학관 소개 */
CREATE SEQUENCE b_notice_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_news_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_report_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_sketch_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_programIntro_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE b_chiefHistory_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;

/* 7. 정보공개 */
CREATE SEQUENCE b_sharedInfoArchive_seq
  START WITH     1
  INCREMENT BY   1
  NOCACHE
  NOCYCLE;



  