CREATE TABLE equip_reserves(
	id NUMBER PRIMARY KEY,
	equipment_id NUMBER,
	equi_serial_id NUMBER,
	begin_time TIMESTAMP,
	end_time TIMESTAMP,
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP,
	
	CONSTRAINT equip_equip_reserve_fk FOREIGN KEY (equipment_id) REFERENCES equipments (id) ON DELETE CASCADE,
	CONSTRAINT serials_equip_reserve_fk FOREIGN KEY (equi_serial_id) REFERENCES equip_serials (id) ON DELETE CASCADE
);
 
 CREATE SEQUENCE equip_reserves_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;