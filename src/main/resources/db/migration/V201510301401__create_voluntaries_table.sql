CREATE TABLE voluntaries(
	id NUMBER PRIMARY KEY,
	title VARCHAR2(255),
	content VARCHAR2(4000),
	begin_date DATE,
	end_date DATE,
	max_member NUMBER,
	wait_member NUMBER,
	reserve_available_day NUMBER,
	location VARCHAR2(255),
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP
);
 
 CREATE SEQUENCE voluntaries_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;