/*------------------------------------------------------------------------------
-- 개체 이름: TL_CAMPAIGN
-- 만든 날짜: 2015-11-16 오후 5:46:25
-- 마지막으로 수정한 날짜: 2016-02-05 오후 3:50:11
-- 상태: VALID
------------------------------------------------------------------------------*/
CREATE TABLE TL_CAMPAIGN (
  COMPANY_CD              VARCHAR2(6 BYTE)        NOT NULL, 
  CAMPAIGN_NO             VARCHAR2(20 BYTE)       NOT NULL, 
  TITLE                   VARCHAR2(500 BYTE)          NULL, 
  SMS_CONTENTS            VARCHAR2(2000 BYTE)         NULL, 
  CALLBACK                VARCHAR2(200 BYTE)          NULL, 
  ACADEMY_CD              VARCHAR2(6 BYTE)            NULL, 
  CLASS_CD                VARCHAR2(6 BYTE)            NULL, 
  YEAR_CD                 VARCHAR2(4 BYTE)            NULL, 
  SEMESTER_CD             VARCHAR2(20 BYTE)           NULL, 
  CAMPAIGN_TARGET_TYPE    CHAR(1 BYTE)                NULL, 
  RESER_FLAG              CHAR(1 BYTE)                NULL, 
  SEND_DATE               VARCHAR2(8 BYTE)            NULL, 
  SEND_TIME               VARCHAR2(6 BYTE)            NULL, 
  SEND_FLAG               CHAR(1 BYTE)                NULL, 
  PROCESS_MSG             VARCHAR2(200 BYTE)          NULL, 
  USE_FLAG                CHAR(1 BYTE)                NULL, 
  INSERT_USER             VARCHAR2(20 BYTE)           NULL, 
  INSERT_DATE             VARCHAR2(8 BYTE)            NULL, 
  INSERT_TIME             VARCHAR2(6 BYTE)            NULL, 
  UPDATE_USER             VARCHAR2(20 BYTE)           NULL, 
  UPDATE_DATE             VARCHAR2(8 BYTE)            NULL, 
  UPDATE_TIME             VARCHAR2(6 BYTE)            NULL, 
  APPLY_STATUS            VARCHAR2(6 BYTE)            NULL, 
  PRICE_NBR               VARCHAR2(200 BYTE)          NULL
);

COMMENT ON TABLE TL_CAMPAIGN IS '캠페인정보';

COMMENT ON COLUMN TL_CAMPAIGN.PRICE_NBR IS '회차';

COMMENT ON COLUMN TL_CAMPAIGN.COMPANY_CD IS '회사코드';

COMMENT ON COLUMN TL_CAMPAIGN.CAMPAIGN_NO IS '캠페인번호';

COMMENT ON COLUMN TL_CAMPAIGN.TITLE IS '제목';

COMMENT ON COLUMN TL_CAMPAIGN.SMS_CONTENTS IS 'SMS내용';

COMMENT ON COLUMN TL_CAMPAIGN.CALLBACK IS '발송자';

COMMENT ON COLUMN TL_CAMPAIGN.ACADEMY_CD IS '아카데미코드';

COMMENT ON COLUMN TL_CAMPAIGN.CLASS_CD IS '분류코드';

COMMENT ON COLUMN TL_CAMPAIGN.YEAR_CD IS '년도';

COMMENT ON COLUMN TL_CAMPAIGN.SEMESTER_CD IS '학기코드';

COMMENT ON COLUMN TL_CAMPAIGN.CAMPAIGN_TARGET_TYPE IS '캠페인대상';

COMMENT ON COLUMN TL_CAMPAIGN.RESER_FLAG IS '예약여부';

COMMENT ON COLUMN TL_CAMPAIGN.SEND_DATE IS '발송일자';

COMMENT ON COLUMN TL_CAMPAIGN.SEND_TIME IS '발송시간';

COMMENT ON COLUMN TL_CAMPAIGN.SEND_FLAG IS '발송여부';

COMMENT ON COLUMN TL_CAMPAIGN.PROCESS_MSG IS '처리내용';

COMMENT ON COLUMN TL_CAMPAIGN.USE_FLAG IS '사용여부';

COMMENT ON COLUMN TL_CAMPAIGN.INSERT_USER IS '등록자';

COMMENT ON COLUMN TL_CAMPAIGN.INSERT_DATE IS '등록일자';

COMMENT ON COLUMN TL_CAMPAIGN.INSERT_TIME IS '등록시간';

COMMENT ON COLUMN TL_CAMPAIGN.UPDATE_USER IS '수정자';

COMMENT ON COLUMN TL_CAMPAIGN.UPDATE_DATE IS '수정일자';

COMMENT ON COLUMN TL_CAMPAIGN.UPDATE_TIME IS '수정시간';

COMMENT ON COLUMN TL_CAMPAIGN.APPLY_STATUS IS '신청상태';

/*------------------------------------------------------------------------------
-- 개체 이름: TL_CAMPAIGN_IDX1
-- 만든 날짜: 2015-11-16 오후 5:46:25
-- 마지막으로 수정한 날짜: 2015-11-16 오후 5:46:25
-- 상태: VALID
------------------------------------------------------------------------------*/
CREATE INDEX TL_CAMPAIGN_IDX1
ON TL_CAMPAIGN (COMPANY_CD, ACADEMY_CD);

/*------------------------------------------------------------------------------
-- 개체 이름: TL_CAMPAIGN_IDX2
-- 만든 날짜: 2015-11-16 오후 5:46:25
-- 마지막으로 수정한 날짜: 2015-11-16 오후 5:46:25
-- 상태: VALID
------------------------------------------------------------------------------*/
CREATE INDEX TL_CAMPAIGN_IDX2
ON TL_CAMPAIGN (COMPANY_CD, YEAR_CD);

/*------------------------------------------------------------------------------
-- 개체 이름: TL_CAMPAIGN_IDX3
-- 만든 날짜: 2015-11-16 오후 5:46:25
-- 마지막으로 수정한 날짜: 2015-11-16 오후 5:46:25
-- 상태: VALID
------------------------------------------------------------------------------*/
CREATE INDEX TL_CAMPAIGN_IDX3
ON TL_CAMPAIGN (COMPANY_CD, SEND_DATE, SEND_TIME);

/*------------------------------------------------------------------------------
-- 개체 이름: TL_CAMPAIGN_IDX4
-- 만든 날짜: 2015-11-16 오후 5:46:25
-- 마지막으로 수정한 날짜: 2015-11-16 오후 5:46:25
-- 상태: VALID
------------------------------------------------------------------------------*/
CREATE INDEX TL_CAMPAIGN_IDX4
ON TL_CAMPAIGN (SEND_DATE, SEND_TIME);

ALTER TABLE TL_CAMPAIGN ADD
(
    CONSTRAINT PK_TL_CAMPAIGN
    PRIMARY KEY ( COMPANY_CD, CAMPAIGN_NO )
        USING INDEX
);





ALTER table TL_CAMPAIGN ADD(
	COURSE_CD VARCHAR2(20),
	PAY_STATUS VARCHAR2(6),
	CANCEL_STATUS VARCHAR2(6),
	REFUND_STATUS VARCHAR2(6),
	RESER_STATUS VARCHAR2(6)
)