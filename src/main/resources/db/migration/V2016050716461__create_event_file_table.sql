CREATE TABLE event_files (
	id NUMBER PRIMARY KEY,
	name VARCHAR2(1000),
	url VARCHAR2(1000),
	event_id NUMBER,
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP
);

CREATE SEQUENCE event_files_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;