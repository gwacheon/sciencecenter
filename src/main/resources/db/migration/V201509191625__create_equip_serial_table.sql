CREATE TABLE equip_serials(
	id NUMBER PRIMARY KEY,
	equipment_id NUMBER,
	serial VARCHAR2(255),
	status VARCHAR2(255),
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP,
	
	CONSTRAINT equipments_fk FOREIGN KEY (equipment_id) REFERENCES equipments (id) ON DELETE CASCADE
);
 
 CREATE SEQUENCE equip_serials_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;