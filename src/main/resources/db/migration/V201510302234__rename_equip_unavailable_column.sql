ALTER TABLE equipunavailable RENAME TO equip_unavailable;

ALTER TABLE equip_unavailable RENAME COLUMN  equipmentId TO equipment_id;
ALTER TABLE equip_unavailable RENAME COLUMN  equiSerialId TO equi_serial_id;
ALTER TABLE equip_unavailable RENAME COLUMN  begintime TO begin_time;