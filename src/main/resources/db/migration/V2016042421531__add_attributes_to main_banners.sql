ALTER TABLE main_banners ADD building_name VARCHAR2(50);
ALTER TABLE main_banners ADD exhibition_name VARCHAR2(50);
ALTER TABLE main_banners ADD description VARCHAR2(200);
ALTER TABLE main_banners ADD banner_link_url VARCHAR2(200);