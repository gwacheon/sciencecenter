CREATE TABLE teachers(
	id NUMBER PRIMARY KEY,
	name VARCHAR2(100),
	affiliation VARCHAR2(100),
	phone VARCHAR2(100),
	mobile VARCHAR2(100),
	email VARCHAR2(100),
	profile VARCHAR2(4000 BYTE),
	evaluation VARCHAR2(4000 BYTE)
);
 
CREATE SEQUENCE teachers_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;