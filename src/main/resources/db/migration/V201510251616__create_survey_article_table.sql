CREATE TABLE articles(
	id NUMBER PRIMARY KEY,
	survey_id NUMBER,
	question VARCHAR2(255),
	q_type VARCHAR2(255),
	seq NUMBER,
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP,
	
	CONSTRAINT survey_articles_fk FOREIGN KEY (survey_id) REFERENCES surveys (id) ON DELETE CASCADE
);
 
 CREATE SEQUENCE articles_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;