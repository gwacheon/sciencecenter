ALTER TABLE equip_reserves ADD member_no VARCHAR2(20); 
COMMENT ON COLUMN equip_reserves.member_no IS 'C2 Member Identifier';
CREATE INDEX equip_reserves_member_no ON equip_reserves (member_no);