ALTER TABLE structure_unavailables DROP (
	st_date
);

ALTER TABLE structure_unavailables ADD (
	st_time TIMESTAMP
);