CREATE TABLE attatchments(
	id NUMBER PRIMARY KEY,
	fileName VARCHAR2(200),
	filePath VARCHAR2(200),
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP
);
 
CREATE SEQUENCE attatchments_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;