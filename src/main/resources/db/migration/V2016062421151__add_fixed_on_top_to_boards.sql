ALTER TABLE boards ADD fixed_on_top NUMBER DEFAULT 0 NOT NULL; 
COMMENT ON COLUMN boards.fixed_on_top IS '상단 고정 여부';

UPDATE boards 
SET fixed_on_top = 0;
