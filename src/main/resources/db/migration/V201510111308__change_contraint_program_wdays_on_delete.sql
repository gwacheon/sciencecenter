ALTER TABLE program_wdays DROP CONSTRAINT fk_program_program_wday;

ALTER TABLE program_wdays
ADD CONSTRAINT fk_program_program_wday FOREIGN KEY (program_id) REFERENCES programs (id)
ON DELETE CASCADE;