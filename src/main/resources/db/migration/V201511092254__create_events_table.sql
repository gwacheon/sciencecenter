CREATE TABLE events (
	id NUMBER PRIMARY KEY,
	type VARCHAR2(20),
	start_date TIMESTAMP,
	end_date TIMESTAMP,
	place VARCHAR2(1000),
	pay VARCHAR2(1000),
    url VARCHAR2(1000),
    content VARCHAR2(4000 BYTE),
    status NUMBER,
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP
);

CREATE SEQUENCE events_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;