CREATE TABLE replies(
	id NUMBER PRIMARY KEY,
	survey_id NUMBER,
	name VARCHAR2(50),
	phone VARCHAR2(50),
	email VARCHAR2(100),
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP,
	
	CONSTRAINT survey_replies_fk FOREIGN KEY (survey_id) REFERENCES surveys (id) ON DELETE CASCADE
);
 
 CREATE SEQUENCE replies_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;