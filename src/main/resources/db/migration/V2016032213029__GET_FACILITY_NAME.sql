/*------------------------------------------------------------------------------
-- 개체 이름: GET_FACILITY_NAME
-- 만든 날짜: 2015-11-16 오후 5:48:40
-- 마지막으로 수정한 날짜: 2016-01-06 오후 5:14:00
-- 상태: VALID
------------------------------------------------------------------------------*/
CREATE OR REPLACE FUNCTION GET_FACILITY_NAME(V_COMPANY_CD IN VARCHAR2, V_FACILITY_CD IN VARCHAR2)
	RETURN VARCHAR2
AS
	V_FACILITY_NAME		TL_FACILITY.FACILITY_NAME%TYPE;

BEGIN

	SELECT FA.FACILITY_NAME
	INTO V_FACILITY_NAME
	FROM TL_FACILITY FA
	WHERE FA.COMPANY_CD = V_COMPANY_CD AND FA.FACILITY_CD = V_FACILITY_CD;

	RETURN V_FACILITY_NAME;

END;



