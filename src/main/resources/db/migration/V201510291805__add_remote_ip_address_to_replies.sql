ALTER TABLE replies ADD remote_ip_address VARCHAR2(255);
CREATE INDEX replies_remote_ip_address_idx ON replies(remote_ip_address);