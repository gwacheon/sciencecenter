/*------------------------------------------------------------------------------
-- 개체 이름: sciecnecenter.GET_WEEK_STR
-- 만든 날짜: 2015-11-16 오후 5:48:40
-- 마지막으로 수정한 날짜: 2016-01-06 오후 5:14:00
-- 상태: VALID
------------------------------------------------------------------------------*/
CREATE OR REPLACE FUNCTION GET_WEEK_STR(V_WEEK IN VARCHAR2)

	RETURN VARCHAR2
AS

	V_WEEK_STR		VARCHAR2(100);

BEGIN

	V_WEEK_STR := '';

	IF V_WEEK IS NOT NULL AND LENGTH(V_WEEK) = 7 THEN
		IF SUBSTR(V_WEEK, 1, 1) = 'Y' THEN
			V_WEEK_STR := '일';
		END IF;

		IF SUBSTR(V_WEEK, 2, 1) = 'Y' THEN
			IF LENGTH(V_WEEK_STR) > 0 THEN
				V_WEEK_STR := V_WEEK_STR || ',';
			END IF;

			V_WEEK_STR := V_WEEK_STR || '월';
		END IF;

		IF SUBSTR(V_WEEK, 3, 1) = 'Y' THEN
			IF LENGTH(V_WEEK_STR) > 0 THEN
				V_WEEK_STR := V_WEEK_STR || ',';
			END IF;

			V_WEEK_STR := V_WEEK_STR || '화';
		END IF;

		IF SUBSTR(V_WEEK, 4, 1) = 'Y' THEN
			IF LENGTH(V_WEEK_STR) > 0 THEN
				V_WEEK_STR := V_WEEK_STR || ',';
			END IF;

			V_WEEK_STR := V_WEEK_STR || '수';
		END IF;

		IF SUBSTR(V_WEEK, 5, 1) = 'Y' THEN
			IF LENGTH(V_WEEK_STR) > 0 THEN
				V_WEEK_STR := V_WEEK_STR || ',';
			END IF;

			V_WEEK_STR := V_WEEK_STR || '목';
		END IF;

		IF SUBSTR(V_WEEK, 6, 1) = 'Y' THEN
			IF LENGTH(V_WEEK_STR) > 0 THEN
				V_WEEK_STR := V_WEEK_STR || ',';
			END IF;

			V_WEEK_STR := V_WEEK_STR || '금';
		END IF;

		IF SUBSTR(V_WEEK, 7, 1) = 'Y' THEN
			IF LENGTH(V_WEEK_STR) > 0 THEN
				V_WEEK_STR := V_WEEK_STR || ',';
			END IF;

			V_WEEK_STR := V_WEEK_STR || '토';
		END IF;
	END IF;

	RETURN V_WEEK_STR;

END;



