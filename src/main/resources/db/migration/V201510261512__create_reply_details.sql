CREATE TABLE reply_details(
	id NUMBER PRIMARY KEY,
	reply_id NUMBER,
	article_id NUMBER,
	reply VARCHAR2(4000),
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP,
	
	CONSTRAINT reply_reply_details_fk FOREIGN KEY (reply_id) REFERENCES replies (id) ON DELETE CASCADE,
	CONSTRAINT article_reply_details_fk FOREIGN KEY (article_id) REFERENCES articles (id) ON DELETE CASCADE
);
 
 CREATE SEQUENCE reply_details_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;