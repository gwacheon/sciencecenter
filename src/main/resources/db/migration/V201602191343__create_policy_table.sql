CREATE TABLE policies (
    id NUMBER PRIMARY KEY,
    type VARCHAR2(100),
    content VARCHAR2(4000 BYTE),
    reg_dttm TIMESTAMP,   
    updt_dttm TIMESTAMP
);

CREATE SEQUENCE policies_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;