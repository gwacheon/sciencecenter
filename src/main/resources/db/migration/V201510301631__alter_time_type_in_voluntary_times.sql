ALTER TABLE voluntary_times MODIFY(
	begin_time TIMESTAMP,
	end_time TIMESTAMP
);