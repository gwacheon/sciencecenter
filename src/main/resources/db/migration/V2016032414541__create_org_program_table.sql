CREATE TABLE org_program (
	id NUMBER PRIMARY KEY,
	title VARCHAR2(255),
	description CLOB,
	min_day NUMBER,
	max_member NUMBER,
	used NUMBER(1) DEFAULT 1 NOT NULL,
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP
);

CREATE SEQUENCE org_program_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;