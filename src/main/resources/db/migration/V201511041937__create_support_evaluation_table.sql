CREATE TABLE support_evaluations (
	id NUMBER PRIMARY KEY,
	year NUMBER,
	seq NUMBER,
	duration VARCHAR2(200),
	evaluator VARCHAR2(200),
	total_score VARCHAR2(200),
	html_data VARCHAR2(2000),
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP
);

CREATE SEQUENCE support_evaluation_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;