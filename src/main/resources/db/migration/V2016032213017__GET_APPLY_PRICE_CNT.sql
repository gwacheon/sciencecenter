/*------------------------------------------------------------------------------
-- 개체 이름: sciecnecenter.GET_APPLY_PRICE_CNT
-- 만든 날짜: 2015-11-16 오후 5:48:41
-- 마지막으로 수정한 날짜: 2016-01-06 오후 5:14:00
-- 상태: VALID
------------------------------------------------------------------------------*/
CREATE OR REPLACE FUNCTION GET_APPLY_PRICE_CNT(V_COMPANY_CD IN VARCHAR2, V_STUDENT_NO IN VARCHAR2, V_COURSE_CD IN VARCHAR2, V_PRICE_NBR IN NUMBER)

	RETURN NUMBER
AS

	V_APPLY_CNT			NUMBER;

BEGIN

	SELECT /*+ INDEX_ASC(CA TL_COURSE_APPLY_IDX1) */
		COUNT(*)
	INTO V_APPLY_CNT
	FROM TL_COURSE_APPLY CA
	WHERE CA.MEMBER_COMPANY_CD = V_COMPANY_CD AND CA.STUDENT_NO = V_STUDENT_NO
		AND CA.COURSE_CD = V_COURSE_CD AND CA.PRICE_NBR = V_PRICE_NBR
		AND CA.CANCEL_FLAG = 'N' AND CA.REFUND_FLAG = 'N' AND CA.USE_FLAG = 'Y';

	RETURN V_APPLY_CNT;
END;



