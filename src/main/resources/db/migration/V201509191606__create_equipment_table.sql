CREATE TABLE equipments(
	id NUMBER PRIMARY KEY,
	name VARCHAR2(100),
	model_name VARCHAR2(100),
	description VARCHAR2(4000 BYTE),
	status VARCHAR2(20),
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP
);

CREATE SEQUENCE equipments_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;
 