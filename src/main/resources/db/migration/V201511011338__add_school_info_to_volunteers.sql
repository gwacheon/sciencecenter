ALTER TABLE volunteers ADD name VARCHAR2(30);
ALTER TABLE volunteers ADD phone VARCHAR2(30);
ALTER TABLE volunteers ADD email VARCHAR2(30);
ALTER TABLE volunteers ADD school VARCHAR2(50);
ALTER TABLE volunteers ADD grade VARCHAR2(30);
ALTER TABLE volunteers ADD group_no VARCHAR2(30);
ALTER TABLE volunteers ADD member_no VARCHAR2(30);
