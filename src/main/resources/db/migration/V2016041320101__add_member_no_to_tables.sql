ALTER TABLE volunteers DROP COLUMN member_no;
ALTER TABLE volunteers ADD member_no VARCHAR2(20);
COMMENT ON COLUMN volunteers.member_no IS 'C2 Member Identifier';
CREATE INDEX volunteers_member_no ON volunteers (member_no);

ALTER TABLE org_applicants ADD member_no VARCHAR2(20);
COMMENT ON COLUMN org_applicants.member_no IS 'C2 Member Identifier';
CREATE INDEX org_applicants_member_no ON org_applicants (member_no);


ALTER TABLE boards ADD member_no VARCHAR2(20);
COMMENT ON COLUMN boards.member_no IS 'C2 Member Identifier';
CREATE INDEX boards_member_no ON boards (member_no);