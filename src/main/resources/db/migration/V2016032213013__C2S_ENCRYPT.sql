/*------------------------------------------------------------------------------
-- 개체 이름: sciecnecenter.C2S_ENCRYPT
-- 만든 날짜: 2015-11-16 오후 5:48:41
-- 마지막으로 수정한 날짜: 2016-01-06 오후 5:14:00
-- 상태: VALID
------------------------------------------------------------------------------*/
CREATE OR REPLACE FUNCTION C2S_ENCRYPT(V_TABLE_NAME IN VARCHAR2, V_COLUMN_NAME IN VARCHAR2, V_DATA IN VARCHAR2)
	RETURN VARCHAR2
AS
	V_ENCRYPT_DATA		VARCHAR2(2000);

BEGIN

	V_ENCRYPT_DATA := V_DATA;

	IF V_DATA IS NOT NULL THEN
		SELECT DGUARD.ENCRYPT(V_TABLE_NAME, V_COLUMN_NAME, V_DATA)
		INTO V_ENCRYPT_DATA
		FROM DUAL;
	END IF;

	RETURN V_ENCRYPT_DATA;
EXCEPTION
	WHEN OTHERS THEN
		RETURN NULL;
END;



