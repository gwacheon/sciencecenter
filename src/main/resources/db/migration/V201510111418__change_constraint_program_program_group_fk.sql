ALTER TABLE programs DROP CONSTRAINT program_group_program_fk;

ALTER TABLE programs
ADD CONSTRAINT program_group_program_fk FOREIGN KEY (group_id) REFERENCES program_groups (id)
ON DELETE CASCADE;