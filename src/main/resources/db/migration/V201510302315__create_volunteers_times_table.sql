CREATE TABLE volunteers(
	id NUMBER PRIMARY KEY,
	voluntary_id NUMBER,
	voluntary_time_id NUMBER,
	user_id NUMBER,
	status VARCHAR2(50),
	waited NUMBER(1),
	introduction VARCHAR2(4000),
	reg_dttm TIMESTAMP,
	updt_dttm TIMESTAMP,
	
	CONSTRAINT voluntary_volunteers_fk FOREIGN KEY (voluntary_id) REFERENCES voluntaries (id) ON DELETE SET NULL,
	CONSTRAINT voluntary_times_volunteers_fk FOREIGN KEY (voluntary_time_id) REFERENCES voluntary_times (id) ON DELETE SET NULL,
	CONSTRAINT user_volunteers_fk FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE SET NULL
);
 
 CREATE SEQUENCE volunteers_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;