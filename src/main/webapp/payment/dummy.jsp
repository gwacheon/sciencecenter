<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.Properties, java.io.*,java.util.*" %>
<%@ page import="java.util.HashMap"%> 
<%@ page import="java.util.Enumeration" %>
<%
	response.setHeader("X-Frame-Options", "ALLOW-FROM http://localhost:8080/sciencecenter/payment/payResponse.jsp");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="../resources/js/C2S/jquery-1.11.3.min.js" ></script>
<script type="text/javascript" src="../resources/js/C2S/defConst.js" ></script>
<link rel="stylesheet" type="text/css" href="../resources/css/C2S/common.css" />
<style type="text/css">
<!-- 
BODY {  font-size: 9pt; line-height: 140%; text-decoration: none}
#SPSDIV{position:absolute;width:639px;height:465px;}
#SPSDIV3{position:absolute;width:320;height:480px;}
#SPSDIV2{position:absolute;width:100%;height:100%;top:0px;left:0px;background:#ffffff;filter:alpha(opacity=80);}
-->
</style>

<script>
	function doOrder()
	{
		var userAgent = new String(navigator.userAgent);
		var windowStatus = '';
		if ( userAgent.indexOf('Trident') > 0  )
		{
			if ( $('input[name=payKind]').val() == PAY_KIND.card )
			{
					if ( userAgent.indexOf('Trident/4.0') > 0 )
						windowStatus = 'left=100, top=100, height=480, width=745, location=no, menubar=no, scrollbars=no, status=no, titlebar=no, toolbar=no, resizable=no';
					else
						windowStatus = 'left=100, top=100, height=480, width=735, location=no, menubar=no, scrollbars=no, status=no, titlebar=no, toolbar=no, resizable=no';
			}
			else
			{
				windowStatus = 'left=100, top=100, height=480, width=735, location=no, menubar=no, scrollbars=no, status=no, titlebar=no, toolbar=no, resizable=no';
			}
		}
		else if ( userAgent.indexOf('AppleWebKit') > 0 && userAgent.indexOf('Chrome') == -1 )
		{
			windowStatus = 'left=100, top=100, height=424, width=738, location=no, menubar=no, scrollbars=no, status=no, titlebar=no, toolbar=no, resizable=no';
		}			
		else
		{
			windowStatus = 'left=100, top=100, height=484, width=740, location=no, menubar=no, scrollbars=no, status=no, titlebar=no, toolbar=no, resizable=no';
		}

		stdpaywin = window.open('', 'stdpaywin', windowStatus);
		$('form[name=mallForm]').attr('target', 'stdpaywin');

		$('form[name=mallForm]').submit();
	}
	

</script>
</head>
<body OnLoad="javascript:doOrder();">
<div>

<form name="mallForm" action="payRequest.jsp"  method="POST"  style='display:none'>
<%
	Enumeration<String> paramNames = request.getParameterNames();

	for (int i = 0 ; paramNames.hasMoreElements() ; i++) {
		String name = (String)paramNames.nextElement();
		String value = request.getParameter(name);

		//value = new String(value.getBytes("ISO-8859-1"), "UTF-8");
%>
		<input type='hidden' name='<%=name%>' value='<%=value%>' ><br>
<%	
		}
%>
</form>

</div>
</body>
</html>