<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.Properties, java.io.*,java.util.*" %>
<%@ page import="java.util.HashMap"%>   
<%@ page import="java.util.Enumeration" %>

<%

	response.setHeader("X-Frame-Options", "ALLOW-FROM http://localhost:8080/sciencecenter/payment/payResponse.jsp");

	String cardApprovNo = request.getParameter("cardApprovNo");
	String cardTradeNo = request.getParameter("cardTradeNo");
	String cardApprovDate = request.getParameter("cardApprovDate");
	String cardName = request.getParameter("cardName");
	String cashReceipt = request.getParameter("cashReceipt");
	String vAccountBankName = request.getParameter("vAccountBankName");
	String vAccount = request.getParameter("vAccount");
	String accountBankName = request.getParameter("accountBankName");
	String rstCode = request.getParameter("rstCode");
	String rstMsg = request.getParameter("rstMsg");

	String payType = request.getParameter("payType");
	String payKind = request.getParameter("payKind");
	
	String returnType = request.getParameter("returnType");
	String encData = request.getParameter("encData");
	
	if ( rstMsg != null )
		rstMsg = new String(rstMsg.getBytes("ISO-8859-1"), "UTF-8");
	
	if ( cardName != null )
		cardName = new String(cardName.getBytes("ISO-8859-1"), "UTF-8");	
	
	if ( vAccountBankName != null )
		vAccountBankName = new String(vAccountBankName.getBytes("ISO-8859-1"), "UTF-8");

	if ( accountBankName != null )
		accountBankName = new String(accountBankName.getBytes("ISO-8859-1"), "UTF-8");	

%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>표준결제창</title>
<script type="text/javascript" src="../resources/js/C2S/defConst.js" ></script>
<script>
	function doResult()
	{
		var userAgent = new String(navigator.userAgent);
		
		var param = {};
		param.cardApprovNo = '<%=cardApprovNo%>';
		param.cardTradeNo = '<%=cardTradeNo%>';
		param.cardApprovDate = '<%=cardApprovDate%>';
		param.cardName= '<%=cardName%>';
		param.cashReceipt= '<%=cashReceipt%>';
		param.vAccountBankName= '<%=vAccountBankName%>';
		param.vAccount = '<%=vAccount%>';
		param.accountBankName = '<%=accountBankName%>';
		param.payType = '<%=payType%>';
		param.payKind = '<%=payKind%>';
		param.returnType = '<%=returnType%>';
		
		var encData = '<%=encData%>';
		var returnType = '<%=returnType%>';
		
		if ( userAgent.indexOf('Trident') > 0 )
		{

			if ( param.returnType == "auth" ) //인증
			{
				opener.parent.csPayAuth('<%=rstCode%>', '<%=rstMsg %>', encData);
				window.close();
				opener.parent.close();

			}
			else
			{ 
				parent.opener.parent.csPayResult('<%=rstCode%>', '<%=rstMsg %>', param);
				window.close();
			}
		}
		else
		{
			if ( param.returnType == 'auth' ) //인증
			{
				parent.opener.parent.csPayAuth('<%=rstCode%>', '<%=rstMsg%>', encData);
				parent.close();
			}
			else
			{
				parent.opener.parent.csPayResult('<%=rstCode%>', '<%=rstMsg%>', param);
			}
		}	
	}
</script>
</head>
<body onLoad="doResult();">

</body>
</html>