<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>표준결제창 테스트 페이지</title>
<script type="text/javascript" src="./script/jquery-1.11.3.min.js" ></script>
<script type="text/javascript" src="./script/defConst.js" ></script>
<link rel="stylesheet" type="text/css" href="./css/common.css" />
<style type="text/css">
<!-- 
BODY {  font-size: 9pt; line-height: 140%; text-decoration: none}
#SPSDIV{position:absolute;width:639px;height:465px;}
#SPSDIV3{position:absolute;width:320;height:480px;}
#SPSDIV2{position:absolute;width:100%;height:100%;top:0px;left:0px;background:#ffffff;filter:alpha(opacity=80);}
-->
</style>
<script>
function doCancel()
{
	var url = 'http://testpg.mainpay.co.kr:8080/csStdPayment/cardCancel.do
	$("form[name=mallForm]").attr("action",url);
	$('form[name=mallForm]').submit();
}
</script>
</head>
<body>
<form name="mallForm" action="dummy.jsp"  method="POST">

<div class="bbsUpdate">
	<table>
		<tr>
			<td colspan="2" class="title01" style="text-align:center">[표준 결제창 취소 테스트]</td>
		</tr>
		<tr>
			<td colspan="2"  class="subtitle01" style="text-align:center">기본정보</td>
		</tr>	
		<tr>
			<th>가맹점 아이디<span class="highlight01">*</span></th>
			<td>
				<select name="mbrId">
					<option selected value='551110'>가맹점 온라인</option>
					<option value='551098'>가맹점 오프라인</option>
				</select>
			</td>
		</tr>
		<tr>
			<th>가맹점 이름<span class="highlight01">*</span></th>
			<td><input type="text" name="mbrName" value="CSQUARE"></td>
		</tr>		
		<tr>
			<th>카드승인번호<span class="highlight01">*</span></th>
			<td><input type="text" name="cardTradeNo" value=""></td>
		</tr>		
		<tr>
			<th>카드승인일(YYMMDD)<span class="highlight01">*</span></th>
			<td><input type="text" name="cardApprovDate" value=""></td>
		</tr>
		<tr>
			<th>결제금액<span class="highlight01">*</span></th>
			<td><input type="text" name="salesPrice" value="1000"></td>
		</tr>
		<tr>
			<td colspan="2" class="subtitle01" style="text-align:center">결제 구분</td>
		</tr>		
		<tr>
			<th>구분<span class="highlight01">*</span></th>
			<td><!-- KeyIn, 3D, ISP -->
				<input type="radio" name="payType" value="CSQ" checked="checked">비인증(KeyIn) 결제 &nbsp;&nbsp;&nbsp;
				<input type="radio" name="payType" value="ISP">ISP 결제 &nbsp;&nbsp;&nbsp;
				<input type="radio" name="payType" value="3D">3D 안심결제 &nbsp;&nbsp;&nbsp;
			</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:center">
				<div onclick="javascript:doCancel();" class="btnType01"><span>취소요청</span></div>
			</td>
		</tr>		
	</table>
</div>
</form>


</body>
</html>
