<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.Properties, java.io.*,java.util.*" %>
<%@ page import="java.util.HashMap"%> 
<%@ page import="java.util.Enumeration" %>
<%
	response.setHeader("X-Frame-Options", "ALLOW-FROM http://localhost:8080/sciencecenter/payment/payResponse.jsp");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="../resources/js/C2S/jquery-1.11.3.min.js" ></script>
<script type="text/javascript" src="../resources/js/C2S/defConst.js" ></script>
<link rel="stylesheet" type="text/css" href="../resources/css/C2S/common.css" />
<style type="text/css">
<!-- 
BODY {  font-size: 9pt; line-height: 140%; text-decoration: none}
#SPSDIV{position:absolute;width:639px;height:465px;}
#SPSDIV3{position:absolute;width:320;height:480px;}
#SPSDIV2{position:absolute;width:100%;height:100%;top:0px;left:0px;background:#ffffff;filter:alpha(opacity=80);}
-->
</style>

<script>
	function doCancel() 
	{
		//var url = 'http://pgtest.mainpay.co.kr:8080/csStdPayment/'; //http://pg.mainpay.co.kr:8080/csStdPayment/  //http://localhost:8080/csquare/
		//var url = 'http://localhost:8080/csquare/';
		var url = 'http://testpg.mainpay.co.kr:8080/csStdPayment/';
		var userAgent = new String(navigator.userAgent);
		$('form[name=cancelForm]').attr('action', url + 'cardCancel.do');		
		if ( userAgent.indexOf('Trident') > 0 )
		{
			$('form[name=mallForm]').attr('target', 'x_frame');
			$('iframe[name=x_frame]').attr('height', '480');
		}
		
		$('form[name=cancelForm]').submit();
	}
	
</script>
</head>
<body OnLoad="javascript:doCancel();">
<iframe name="x_frame" width="100%" style="border:none;"></iframe> 
<form name="cancelForm" action="http://testpg.mainpay.co.kr:8080/csStdPayment/cardCancel.do"  method="POST" style='display:none'>
<%
	Enumeration<String> paramNames = request.getParameterNames();

	for (int i = 0 ; paramNames.hasMoreElements() ; i++) {
		String name = (String)paramNames.nextElement();
		String value = request.getParameter(name);
	
	//	value = new String(value.getBytes("ISO-8859-1"), "UTF-8");
	
%>
		<input type='hidden' name='<%=name%>' value='<%=value%>' ><br>
<%	
		}
%>
</form>
 
</body>
</html>