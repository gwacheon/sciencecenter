<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix='fn' uri='http://java.sun.com/jsp/jstl/functions'%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=0.6, maximum-scale=0.6, minimum-scale=0.6, user-scalable=no" />
<script type="text/javascript" src="../resources/js/C2S/defConst.js" ></script>
<script>
	function doOrder()
	{
		var frm = document.mallForm;
		//var url = 'http://192.168.1.14:8080/csquare/';
		//var url = 'http://192.168.123.1:8080/csquare/';
		var backUrl = frm.callbackUrl.value;
		var url = 'http://203.231.124.159/csStdPayment/';
		if (confirm('결제 하시겠습니까?')) {
			
			if ( $('input[name=payKind1]').val() == PAY_KIND.card )
			{
				$('form[name=mallForm]').attr('action', url + 'mobstep01.do?${_csrf.parameterName}=${_csrf.token}');
				frm.callbackUrl.value = backUrl+'&'+$('form[name=mallForm]').serialize();
			}	
			else if ( $('input[name=payKind1]').val() == PAY_KIND.vAccount )
			{
				$('form[name=mallForm]').attr('action', url + 'mobvcstep01.do?${_csrf.parameterName}=${_csrf.token}');
				frm.callbackUrl.value = backUrl+'&'+$('form[name=mallForm]').serialize();
			}		
			else if ( $('input[name=payKind1]').val() == PAY_KIND.account )
			{
				$('form[name=mallForm]').attr('action', url + 'mobacstep01.do');
			}
		
			$('form[name=mallForm]').submit();
		} else {
			return;
		}

		return;
	}
	
	function csPayResult(resultCode, resultMsg, resultParam)
	{
		/* 
			reusltParam
				cardApprovNo: 카드 승인번호
				cardTradeNo: 거래 번호
				cardName: 카드명
				cardApprovDate: 승인일
				
				cashReceipt: 현금영수증 번호
				
				vAccountBankName: 가상계좌 은행명
				vAccount: 가상계좌 계좌명
		*/
		if ( resultParam.payKind == PAY_KIND_NAME.card )
		{
			payResult(resultParam);
		}
		else if ( resultParam.payKind == PAY_KIND_NAME.account )
		{
			alert(resultParam.accountBankName);
			alert(resultParam.cashReceipt);
		}
		else if ( resultParam.payKind == PAY_KIND_NAME.vAccount )
		{
			VaResult(resultParam);	
		}
	}
	
</script>
<c:set value="/payment/paymentMobileModule?${_csrf.parameterName}=${_csrf.token}" var="Response" />
<form name="mallForm"  method="POST">
	<input type="hidden" name="APPLY_UNIQUE_NBR" value="" />
	<input type="hidden" name="MEMBER_NO" value="${MEMBER_NO }" />
	<input type="hidden" name="SALE_TYPE" value="${SALE_TYPE }" />
	<input type="hidden" name="TOTAL_PRICE" value="" />
	<input type="hidden" name="APPLY_CNT" value="" />
	<input type="hidden" name="PAYMENT_AMT" value="" />
	<input type="hidden" name="COURSE_NAME" value="" />
	<input type="hidden" name="mbrId" value=551098 />
	<input type="hidden" name="mbrName" value="CSQUARE" />
	<input type="hidden" name="mobileNetwork" value="" />
	<input type="hidden" name="buyerName" value=""/>
	<input type="hidden" name="buyerMobile" value="" />
	<input type="hidden" name="productName" value="" />
	<input type="hidden" name="productPrice" value="" />
	<input type="hidden" name="productCount" value="" />
	<input type="hidden" name="salesPrice" value="" />
	<input type="hidden" name="payKind1" value="1" />
	<input type="hidden" name="authType" value="all" />
	<input type="hidden" name="returnType" value="payment" />
	<input type="hidden" name="callbackUrl"	value = "<%=  request.getScheme() + "://" +  request.getServerName() + ":" +  request.getServerPort() + request.getContextPath()%>${Response}" >
	<input type="hidden" name="encData" value ="" > <!-- 암호화된 인증관련 정보 -->
	<input type="hidden" name="noInterest" value="0">
</form>
</head>
</html>