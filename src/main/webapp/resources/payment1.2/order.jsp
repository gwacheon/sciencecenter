<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	/* 
	서명값 생성 (중요)
	제공된 sha256 hash 생성 함수 참조
	
	hashValue = timestamp + Sample.hash(mbrId|salesPrice|oid|timestamp)

	(timestamp 형식은 YYYYMMDDHHMMSS)

	*/
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>표준결제창 테스트 페이지</title>
<!-- 테스트용 -->
<!-- <script type="text/javascript" src="https://testpg.mainpay.co.kr/csStdPayment/resources/script/v1/c2StdPay.js"></script> -->
<!-- 운영환경용 -->
<script type="text/javascript" src="https://pg.mainpay.co.kr/csStdPayment/resources/script/v1/c2StdPay.js" ></script>
</head>
<body>

<!-- 타켓 프레임 -->
<iframe name="x_frame" id='x_frame' style="display:none" width="500" height="100"></iframe>

<!-- 전송폼 -->
<form name="mallForm"  id="mallForm"  method="POST">

<!-- 공통 파라미터 -->
<p><b>씨스퀘어 표준결제 결제요청 샘플 페이지 </b></p>
<p>버전            <input type="text" name="version" value="1.2"></p>
<p>server       
<select name="server">
	<option value='0'>테스트 서버</option>
	<option value='1'>운영서버</option>	
</select></p>
<p>결제종류
<select name="payKind">
	<option selected value='1'>카드</option>
	<option value='2'>가상계좌</option>
	<option value='3'>계좌이체</option>
	<option value='4'>휴대폰</option> 
</select></p>
<p>상점 아이디    <input type="text" name="mbrId" value=""></p> 
<p>가맹점명        <input type="text" name="mbrName" value="씨스퀘어"></p>
<p>구매자명        <input type="text" name="buyerName" value="조재국"></p>
<p>구매자휴대폰      <input type="text" name="buyerMobile" value="01032339782"></p>
<p>구매자메일       <input type="text" name="buyerEmail" value="hong@test.com"></p>
<p>주문번호        <input type="text" name="oid" value="12345678901234567890"></p> 
<p>상품명         <input type="text" name="productName" value="전자 패드"></p>
<p>결제금액        <input type="text" name="salesPrice" value="1000"></p> <!-- 실제 결제되는 금액 -->
<p>상품가격        <input type="text" name="productPrice" value="1100"></p>
<p>상품수량        <input type="text" name="productCount" value="1"></p>
<p>사업자번호       <input type="text" name="bizNo" value="1203483832"></p> <!-- 가맹점사업자 번호 -->
<p>callbackUrl(client to client)        
			    <input type="text" name="callbackUrl" value="http://localhost/clientCallback.jsp"></p> 
<p>returnUrl(server to server)        
			    <input type="text" name="returnUrl" value=""></p>
<p>리턴타입        <input type="text" name="returnType" value="payment"></p> <!-- 고정값 -->
<p>서명값         <input type="text" name="hashValue" value=""></p>
<!--휴대폰 결제수단 사용시 -->
<p>CPCODE       <input type="text" name="CPCODE" value="0111000390"></p> <!-- 테스트용 CPCODE -->
<p>authType     <input type="text" name="authType" value="auth"></p> <!-- 고정 -->
				
<input type="button" value="__결제요청__" onclick="C2StdPay.pay()" /> 
</form>
	
</body>
</html>