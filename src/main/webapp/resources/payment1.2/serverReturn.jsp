﻿﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ page import="java.net.*" %>

<%
	//서버(mainpay) ==> 서버(가맹점)로 데이터 수신
	Enumeration<String> em = request.getParameterNames();
	while (em.hasMoreElements()) {
		String key = em.nextElement();
		String val = (String)request.getParameter(key);			
		out.println(key + " :: " + URLDecoder.decode(val) + "<br>");
	}
	
	/*
	 (중요) 매뉴얼의 returnUrl 응답 규격에 따라 작성 해주세요.
	      비정상 응답 리턴시에 결제가 취소됩니다.
	*/

%>
