<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ page import="java.net.*" %>

<%
	String salesPrice = request.getParameter("salesPrice");
	String cardCode = request.getParameter("cardCode");
	String returnType = request.getParameter("returnType");
	String rstMsg = request.getParameter("rstMsg");
	String cardName = request.getParameter("cardName");
	String cardApprovTime = request.getParameter("cardApprovTime");
	String rstCode = request.getParameter("rstCode");
	String oid = request.getParameter("oid");
	String vAccount = request.getParameter("vAccount");
	String cardApprovNo = request.getParameter("cardApprovNo");
	String payType = request.getParameter("payType");
	String vAccountBankName = request.getParameter("vAccountBankName");
	String cardTradeNo = request.getParameter("cardTradeNo");
	String cashReceipt = request.getParameter("cashReceipt");
	String payKind = request.getParameter("payKind");
	String cardApprovDate = request.getParameter("cardApprovDate");
	
	if ( rstMsg != null )
		rstMsg = new String(rstMsg.getBytes("ISO-8859-1"), "UTF-8");
	
	if ( cardName != null )
		cardName = new String(cardName.getBytes("ISO-8859-1"), "UTF-8");	
	
	if ( vAccountBankName != null )
		vAccountBankName = new String(vAccountBankName.getBytes("ISO-8859-1"), "UTF-8");	

%>
<script>
	
	var agent = navigator.userAgent.toLowerCase();
	if ((agent.indexOf("msie") != -1) || (navigator.appName == 'Netscape' && navigator.userAgent.search('Trident') != -1)) {
		window.opener.CompleteProc("<%=oid%>");
	}else{
<%-- 		parent.opener.parent.CompleteProc("<%=oid%>");	 --%>
		window.opener.CompleteProc("<%=oid%>");
	}
	
	window.open("about:blank","_self").close();
	self.close();
</script>