var Structures = {};

var Structure = function(id, title, picture, area,
		priceSpring, priceSummer, priceWinter, person){
	
	this.id = id;
	this.title = title;
	this.picture = picture;
	this.area = area;
	this.priceSpring = priceSpring;
	this.priceSummer = priceSummer;
	this.priceWinter = priceWinter;
	this.person = person;
}