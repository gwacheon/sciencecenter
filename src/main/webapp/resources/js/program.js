var ProgramManager = function(options){
	this.dataUrl = options.url;
	this.itemWrapper = options.itemWrapper;
	this.selectItem = options.selectItem;
	this.items = [];
	
	if(!_.isUndefined(options.date)){
		this.setDate(options.date);
	}else{
		this.setDate(new Date());
	}
	
	this.addEventHandlers();
}

ProgramManager.prototype.setDate = function(date){
	this.date = new Date(date);
	this.getPrograms(this.date);
	$(".ptech-date").removeClass('active');
	$(".ptech-date[data-date='" + moment(this.date).format("YYYY/MM/DD") + "']").addClass('active');
}

ProgramManager.prototype.getPrograms = function(){
	var manager = this;
	$.ajax({
		url: manager.dataUrl + moment(manager.date).format("YYYY/MM/DD"),
		type: "GET",
		success: function(d){
			manager.items = d.programs;
			manager.renderPrograms();
		}	
	});
}

ProgramManager.prototype.addEventHandlers = function(){
	var manager = this;
	$(document).on("click", manager.selectItem, function(e){
		manager.setDate($(this).data("date"));
	});
}

ProgramManager.prototype.renderPrograms = function(){
	if(_.isUndefined(this.programTemplate)){
		this.programTemplate = Handlebars.compile($("#programs-template").html());
	}
	
	var programHtml = this.programTemplate({
		programs: this.items,
		baseUrl: baseUrl
	});
	
	$(this.itemWrapper).html(programHtml);
}