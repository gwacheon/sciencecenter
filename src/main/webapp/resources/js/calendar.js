/**
 * 달력
 */
function _init(yy,mon){
	var html = '';
	var weekDay = ['일','월','화','수','목','금','토'];
	var prev;
	var next;
	
	html += '<div class="ptech-calendar">'+
				'<div class="calendar-nav">'+
					'<div class="row nav-title">'+
						'<div class="col-md-12 center">';
	
	//년
	html += PrevMon(yy,mon);
	html += CurrentMon(yy,mon);
	html += NextMon(yy,mon);
	html += 	'</div>';
	html += '</div>';
	
	// 요일
	html += '<div class="ptech-week week-label">';
	for(var i=0; i < weekDay.length;i++){
		if(i == 0){
			html += '<div class="ptech-date week-day sunday">'+weekDay[i]+'</div>';
		}else{
			html += '<div class="ptech-date week-day">'+weekDay[i]+'</div>';
		}
	}
	html += '</div>';
	html += '</div>';
	
	// 일자
	html += CalDay(yy,mon);
	html += '</div>';
	
	$(html).appendTo('#program-calendar-wrapper');
	DayAjax(ToMon(yy,mon)+'01');
}

function CurrentMon(yy,mon){
	var rs ='';
	var curMon = '';
	var ymd; 
	
	if(yy == '' && mon ==''){
		ymd = new Date();
		
		if((ymd.getMonth()+1) < 10){
			curMon = '0'+(ymd.getMonth()+1);
		}else{
			curMon = ymd.getMonth()+1;
		}
		
		rs = ymd.getFullYear()+'년'+ curMon+'월';
	}else{	
		ymd = new Date(yy,Number(mon)-1,'01');
	
		if((ymd.getMonth()+1) < 10){
			curMon = '0'+(ymd.getMonth()+1);
		}else{
			curMon = (ymd.getMonth()+1);
		}
		
		rs =  ymd.getFullYear()+'년'+curMon+'월';
	}
	
	return rs;
}

function ToMon(yy,mon){
	var ymd; 
	var rs ='';
	var curMon ='';
	
	if(yy == '' && mon ==''){
		ymd = new Date();
		
		if((ymd.getMonth()+1) < 10){
			curMon = '0'+(ymd.getMonth()+1);
		}else{
			curMon = ymd.getMonth()+1;
		}
		
		rs = ymd.getFullYear().toString() + curMon.toString();
	}else{	
		ymd = new Date(yy,Number(mon)-1,'01');

		if((ymd.getMonth()+1) < 10){
			curMon = '0'+(ymd.getMonth()+1);
		}else{
			curMon = ''+(ymd.getMonth()+1);
		}
		
		rs =  ymd.getFullYear().toString() + curMon.toString();
	}
	
	return rs;
}

// 전월 선택
function PrevMon(yy,mon){
	var rs ='';
	var ymd;
	
	if(yy == '' && mon == ''){
		ymd = new Date();
		rs += '<a href="javascript:void(0)" class="prev-month month-nav-item" onclick="ChangeMon(\''+ymd.getFullYear()+'\',\''+ymd.getMonth()+'\')";>';
		rs += '&lt';
		rs += '</a>';
	}else{
		ymd = new Date(yy,Number(mon)-1,'01');
		rs += '<a href="javascript:void(0)" class="prev-month month-nav-item" onclick="ChangeMon(\''+ymd.getFullYear()+'\',\''+(ymd.getMonth())+'\')";>';
		rs += '&lt';
		rs += '</a>';
	}
		
	return rs;
}

// 다음월 선택
function NextMon(yy,mon){
	var rs ='';
	var ymd;
	
	if(yy == '' && mon == ''){
		ymd = new Date();
		rs += '<a href="javascript:void(0)" class="next-month month-nav-item" onclick="ChangeMon(\''+ymd.getFullYear()+'\',\''+(ymd.getMonth()+2)+'\')";>';
		rs += '&gt;';
		rs += '</a>';
	}else{
		ymd = new Date(yy,Number(mon)+1,'01');
	
		rs += '<a href="javascript:void(0)" class="next-month month-nav-item" onclick="ChangeMon(\''+ymd.getFullYear()+'\',\''+ymd.getMonth()+'\')";>';
		rs += '&gt;';
		rs += '</a>';
	} 
		
	return rs;
}

// 월 변경시 
function ChangeMon(yy,mon){
	$('#program-calendar-wrapper').empty();
	$('#priceList').empty();
	_init(yy,mon);
}


// 날짜 계산
function CalDay(yy,mon){
	var result ='';
	var j =1;
	var dd ='';
	
	result += '<div class="ptech-week">';
	var iCount = Number(CalFirstWeek(yy,mon)+CalLastDay(yy,mon));
	
	var dateObj = new Date();
	var year = dateObj.getFullYear();
	var month = dateObj.getMonth()+1;
	var day = dateObj.getDate();
	
	month = (month < 10) ? '0' + month : month;
	day = (day < 10) ? '0' + day : day;
	
	var today = year +""+ month +""+ day;
	
    for(var i=1;i<=iCount;i++){
	//for(var i=1;i<=42;i++){
    	
		if(i >=(CalFirstWeek(yy,mon)+1) && i <=Number(CalFirstWeek(yy,mon)+CalLastDay(yy,mon))){
			if(j < 10){
				dd ='0'+j;
			}else{
				dd = ''+j;
			}
			
			var onClick = '';
	    	if(today.toString() <= (ToMon(yy,mon)+dd).toString())
	    		onClick = "onclick=CurrentDay("+ToMon(yy,mon)+dd+",this)";
	    	else
	    		onClick = '';
	    	
			var day = ToMon(yy,mon)+dd;
			if(Number(DayLocation()) == Number(j)){
				result += '<div class="ptech-date" id="'+ToMon(yy,mon)+dd+'"'+onClick+'><div>'+j+'</div></div>';
			}else{
				if(Number(DayLocation()) > Number(j)){
					result += '<div class="ptech-date" id="'+ToMon(yy,mon)+dd+'" '+onClick+'><div>'+j+'</div></div>';
				}else{
					if(i%7 == 1){
						result += '<div class="ptech-date sunday" id="'+ToMon(yy,mon)+dd+'" '+onClick+'><div>'+j+'</div></div>';
					}else if(i%7 == 0){
						result += '<div class="ptech-date saturday" id="'+ToMon(yy,mon)+dd+'" '+onClick+'><div>'+j+'</div></div>';
					}else{
						result += '<div class="ptech-date" id="'+ToMon(yy,mon)+dd+'" '+onClick+'><div>'+j+'</div></div>';
					}
				}
			}
			j++;
		}else{
			result += '<div class="ptech-date" onclick=""><div> </div></div>';
		}
		
		if(i%7 == 0){
			result += '</div><div class="ptech-week">';
		}
	}
	return result;
}

// 날짜 해당 월에 1일에 대한 요일
function CalFirstWeek(yy,mon){
	var d = new Date();
	if(mon == ''){
		return new Date(d.getFullYear(),d.getMonth()).getDay();
	}else{
		return new Date(yy,mon-1).getDay();
	}
}

// 날짜 해당 마지막날에 대한 요일
function CalLastWeek(){
	var d = new Date();
	
	return new Date(d.getFullYear(),d.getMonth()+1,0).getDay();
}

// 해당 월에 마지막날짜
function CalLastDay(yy,mon){
	var d = new Date();
	if(mon ==''){
		return new Date(d.getFullYear(),d.getMonth()+1,0).getDate();
	}else{
		return new Date(yy,mon,0).getDate();
	}
}

// 오늘 날짜 구하기
function DayLocation(){
	var ymd = new Date();	
	return ymd.getDate();
}