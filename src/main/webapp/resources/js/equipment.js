
var EquipReserve = function(options){
	this.reserves = [];
	this.serialId = options.equiSerialId;
	
	this.header = $("meta[name='_csrf_header']").attr("content");
	this.token = $("meta[name='_csrf']").attr("content");
	
	this.source = $("#serial-times-template").html();
	this.template = Handlebars.compile(this.source);
	
	$("#submit-reservation-btn").on('click', function(){
		equipReserve.submitReservations();
	});
}

EquipReserve.prototype.addEvents = function(){
	var equipReserve = this;
	
	$("#reservation-confirm-btn").on('click', function(){
		equipReserve.reserves = [];
		
		if($(".check-time:checked").length > 0){
			_.forEach($(".check-time:checked"), function(n, key){
				equipReserve.reserves.push({
					seq: $(n).data('seq'),
					date: $(n).data('date'),
					time: $(n).data('time'),
					serialId: $(n).data('serial')
				});
			});
			
			equipReserve.reserves = _.groupBy(equipReserve.reserves, function(obj){
				return obj.serialId;
			});
			
			equipReserve.confirmReservation();        				
		}else{
			alert("선택된 시간이 없습니다.");
		}
	});
}

EquipReserve.prototype.getReservations = function(date, equipmentId, equiSerialId){
	var equipReserve = this;
	
	$.ajax({
		url: baseUrl + "sangsang/getEquipReserves",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		type : "POST",
		beforeSend: function(xhr) {
            xhr.setRequestHeader(equipReserve.header, equipReserve.token);
            ajaxLoading();
        },
		data: JSON.stringify({
			equipmentId: equipmentId,
			equiSerialId: equiSerialId,
			date: date
		}),
    	success: function(data){
    		var timeWithIndex = [];
    		
    		_.forEach([
           			"09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00",
        			"16:00", "17:00"
    		    ], function(time, idx){
    			timeWithIndex.push({seq: idx, time: time});
    		});
    		
    		var html = equipReserve.template({
    			serial: data.serial,
    			times: timeWithIndex,
    			date: date
   			});
    		
    		$("#equipment-serials[data-id='"+data.serial.id+"']").html(html);
    		
    		if(!_.isUndefined(data.equipReserves)){
    			_.forEach(data.equipReserves, function(equipReserve, key){
    				var d = moment(equipReserve.beginTime);
    				var dataSerial = "[data-serial='" + equipReserve.equiSerialId + "']";
    				var dataDate = "[data-date='" + d.format("YYYY-MM-DD") + "']";
    				var dataTime = "[data-time='" + d.format("HH:mm") + "']";
    				$(".check-time" + dataSerial + dataDate + dataTime).attr("disabled", true);
    				$(".reserve-time-label" + dataSerial + dataDate + dataTime).text("예약완료");
    				
    				console.log($(".reserve-time-label" + dataSerial + dataDate + dataTime));
    			});
    		}
    		
    		if(!_.isUndefined(data.unavailable)){
    			_.forEach(data.unavailable, function(unavailable, key){
    				var d = moment(unavailable.beginTime);
    				var dataDate = "[data-date='" + d.format("YYYY-MM-DD") + "']";
    				var dataSerial = "[data-serial='" + unavailable.equiSerialId + "']";
    				
    				$(".check-time"+ dataSerial + dataDate ).attr("disabled", true);
    				$(".reserve-time-label" + dataSerial + dataDate ).text("예약불가");
    			})
    		}
    		
    		ajaxUnLoading();
    		
    		equipReserve.addEvents();
    	},
    	error: function(err){
    		console.log(err);
    	}
	});
}



EquipReserve.prototype.confirmReservation = function(){
	var equipReserve = this;
	_.forEach(equipReserve.reserves, function(times, serialId){
		_.forEach(times, function(time, idx){
			$("#reservation-infos").append("<li>" + time.time + "</li>");	
		});
	});
	
	$("#reservation-modal").openModal();
}

EquipReserve.prototype.submitReservations = function(){
	var equipReserve = this;
	
	$.ajax({
		url: baseUrl + "sangsang/equipReserves",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		type : "POST",
		beforeSend: function(xhr) {
            xhr.setRequestHeader(equipReserve.header, equipReserve.token);
        },
		data: JSON.stringify(equipReserve.reserves),
    	success: function(data){
    		alert("정상 예약 되었습니다. 추후 로그인 / 나의 예약등과 연계 됩니다.");
    		location.reload();
    	},
    	error: function(err){
    		console.log(err);
    	}
	});
}