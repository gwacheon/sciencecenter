Handlebars.registerHelper('hasPreviousBoard', function(boards, board) {
	
});


Handlebars.registerHelper('formatedDate', function(date, format) {
	  return moment(date).format(format);
	});

Handlebars.registerHelper("beforePage", function(beginPage) {
	return beginPage - 1;
});

Handlebars.registerHelper("nextPage", function(endPage) {
	return endPage + 1;
});

Handlebars.registerHelper("indexOf", function(item) {
	return endPage + 1;
});

Handlebars.registerHelper('for', function(from, to, incr, block) {
    var accum = '';
    for(var i = from; i <= to; i += incr)
        accum += block.fn(i);
    return accum;
});


var Board = function(container, url, options){
	this.container = container;
	this.header = $("meta[name='_csrf_header']").attr("content");
	this.token = $("meta[name='_csrf']").attr("content");
	
	this.url = url;
	this.options = options;
	
	if(this.options.page == null){
		this.options.page = 1;
	}
	
	this.id = this.options.id;
	this.replied = this.options.replied || false;
	this.redirectUrl = this.options.redirectUrl;
	this.currentMemberNo = this.options.memberNo;
	this.isEditable = this.options.isEditable;
	this.deleteUrl = this.options.deleteUrl;

	this.addEvents();
}

Board.prototype.getUrl = function(){
	var url = baseUrl+"boards/getBoards/"+this.options.type + "?";
	var params = [];
	
	if(this.options.page != null && this.options.page > 1){
		params.push("page=" + this.options.page);
	}
	
	return url + params.join(",");
}

Board.prototype.getShowUrl= function(){
	
	return baseUrl+"boards/showBoard/"+this.options.id;
}


Board.prototype.renderBoardList = function(){
	var board = this;
	
	$.ajax({
		url : board.getUrl(),
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		type : "POST",
		beforeSend : function(xhr){
			if(board.header != null && board.token != null){
				xhr.setRequestHeader(board.header, board.token);
			}			
		},
		data: JSON.stringify({
			type: board.options.type,
			page: board.options.page
		}),
		success : function(data){
			if(data.boardType.gallary == null || data.boardType.gallary == false) {
				board.source = $("#board-list-template").html();				
			}
			else{
				board.source = $("#board-gallary-template").html();
			}
			
			board.template = Handlebars.compile(board.source);
			
			if( data.boardType == "faqTotal" ) {
				data.isFaqTotal = true;
				data.url = board.url.slice(0,-9);
			}
			else {
				data.url = board.url;				
			}
			
			$(board.container).html(board.template(data));
		},
		error: function(err){
    		console.log(err);
    	}
	});
}

Board.prototype.renderBoardShow = function(){
	var board = this;
	
	board.source = $("#board-show-template").html();
	board.template = Handlebars.compile(this.source);
	
	$.ajax({
		url : board.getShowUrl(),
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		type : "POST",
		beforeSend : function(xhr){
			if(board.header != null && board.token != null){
				xhr.setRequestHeader(board.header, board.token);
			}			
		},
		success : function(data){
			data.url = board.url;
			data.baseUrl = baseUrl;
			data.baseFileUrl = baseFileUrl;
			data.redirectUrl = board.redirectUrl;
			
			// 사용자가 수정가능한 게시판의 경우, 사용자와 작성자가 일치하면, 자신이 작성한 글임을 setting
			if( board.isEditable &&
					data.board.memberNo == board.currentMemberNo ) {
				data.isMyBoard = true;
			}else {
				data.isMyBoard = false;
			}
			
			$(board.container).html(board.template(data));
			
			
		},
		error: function(err){
    		console.log(err);
    	}
	});
	
	this.addDeleteEvent();
	
	

}

Board.prototype.addEvents = function(){
	var board = this;
}

Board.prototype.addDeleteEvent = function() {
	var board = this;
	
	$(document).on("click", ".board-delete-btn", function(e){
		if( confirm("해당 게시글 및 답글이 전부 삭제됩니다. 정말로 삭제하시겠습니까?") ) {
			var id = board.id;
			
			$.ajax({
				url: board.deleteUrl,
				contentType : "application/json; charset=utf-8",
				beforeSend: function(xhr) {
					ajaxLoading();
					xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));
				},
				data: JSON.stringify({
					id: id
				}),
				type: "POST",
				dataType: "JSON",
				success: function(data) {
					ajaxUnLoading();
					alert("삭제되었습니다.");
					window.location = board.url;
				},
				error: function(err) {
					ajaxUnLoading();
					alert(err);
				}
			});
		}
	});
}