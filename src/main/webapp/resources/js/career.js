var Career = function(id, title, beginDate){
	this.id = id;
	this.title = title;
	this.beginDate = moment(beginDate, "YYYY-MM-DD");
	
	this.dates = [];
	this.schedules = [];
	
	this.setDates();
	
	console.log(this.dates);
	
	this.source   = $("#career-template").html();
	this.template = Handlebars.compile(this.source);
}

Career.prototype.addSchedule = function(schedule){
	this.schedules.push(schedule);
}

Career.prototype.setSchedules = function(schedules){
	this.schedules = schedules;
}

Career.prototype.setDates = function(){
	var dateStrs = ["화", "수", "목", "금"];
	for(var i = 0; i < 4; i++){
		var tempDate = this.beginDate.clone();
		
		this.dates.push({
			dateStr: dateStrs[i],
			date: tempDate.add(i, 'days').format("YYYY-MM-DD")
		});
	}	
}

Career.prototype.render = function(){
	var data = {
		title: this.title,
		dates: this.dates,
		schedules: this.schedules
	}
	
	console.log(data);
	var html    = this.template(data);
	$("#careers-wrapper").append(html);
}

var Schedule = function(id, tueWord, tueAvailable, wedWord, wedAvailable, thuWord, thuAvailable, friWord, friAvailable){
	this.id = id;
	this.tueWord = tueWord;
	this.tueAvailable = tueAvailable === "true";
	this.wedWord = wedWord;
	this.wedAvailable = wedAvailable === "true";
	this.thuWord = thuWord;
	this.thuAvailable = thuAvailable === "true";
	this.friWord = friWord;
	this.friAvailable = friAvailable === "true";
}