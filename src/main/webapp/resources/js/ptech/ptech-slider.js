(function () {
	
	var PTechSlider = function (container, options) {
		return new Slider(container, options);
	};
	
	var Slider = function (container, options) {
		
		this.selector = $(container);
		
		// default setting
		var defaults = {
			slidesPerScreen: 4,	
	        indicators: false,
	        height: 150,
	        zoom: true,
	        interval: 3000
		};
		
		// merge the options and default settings
		options = $.extend(defaults, options);
		
		this.interval = options.interval;
		
		// get all image slides 
		this.slides = $(container).find('.slide');
		
		_.forEach($(this.slides), function(d, i){
			var altString = "related_photo_" + i;
			$(d).attr("data-idx", i).attr("alt", altString);
			
		});
		
		this.numberOfSlides = this.slides.length;
		
		// override the slidesPerScreen depends on the window width size
		// desktop size
		if( $(window).width() > 992 ) {
			this.slidesPerScreen = options.slidesPerScreen;
		}
		// tablet size
		else if( $(window).width() > 600 && $(window).width() <= 992 ) {
			this.slidesPerScreen = 2;
		}
		// mobile size
		else if( $(window).width() > 500 && $(window).width() <= 600 ) {
			options.height = 200;
			this.slidesPerScreen = 1;
		}
		else {
			options.height = 150;
			this.slidesPerScreen = 1;
		}
		// if slidesPerScreen is more than 1, the indicators option will be ALWAYS FALSE
		if( this.slidesPerScreen > 1 ) {
			options.indicators = false;
		}
		
		// set width ( 10 is the left&right margin of each slide )
		this.slideWidth = $(container).find('#image-slider-window').width() * 1 / this.slidesPerScreen;
		this.slides.width(this.slideWidth-10);
		
		// set height of all slides
		this.slides.height(options.height);
		//this.slides.find('img').width("100%").height(options.height); 
	
		// set height of the container
		// this will consider the existence of the indicators
		if( options.indicators ) {
			$(container).height(options.height + 50);  
		}
		else {
			$(container).height(options.height);
		}

		// set width of each slide.
		// depends on the number of slides, set each slide's proper width.
		//this.slideWidthPercent = ( 100 / this.numberOfSlides ) + "%";
		
		// set position of all images
		//this.slides.width(this.slideWidthPercent);

		this.slides.wrapAll('<div id="slidesHolder"></div>');
		this.slides.css({ 'float' : 'left' });
		$('#slidesHolder').css('width', ( this.slideWidth + 10 )* this.numberOfSlides);
		
		// current position of slide
		this.currPosition = 0;  
		
		// call 'this' object as '$this'
		$this = this;
		
		//create and add slide buttons
		$buttonLeft = $('<div class="image-slider-btn left"><i class="material-icons">&#xE314;</i></div>');
		$buttonRight = $('<div class="image-slider-btn right"><i class="material-icons">&#xE315;</i></div>');
		$(container).append($buttonLeft);
		$(container).append($buttonRight);
		
		// slide button event
		$buttonLeft.on('click', function() {
			//update indicator
			$this.selector.find('.indicator-item').eq($this.currPosition).removeClass('active');
			if($this.currPosition == 0) {
				$(container).find('.indicator-item').eq($this.currPosition).addClass('active');
				return false;
			} else {
				$this.currPosition--;
			}
			// move slide
			$('#slidesHolder')
			.animate({'marginLeft' : $this.slideWidth*(-$this.currPosition)});
			// update indicator
			$(container).find('.indicator-item').eq($this.currPosition).addClass('active');
		});
		$buttonRight.on('click', function() {
			//update indicator
			$this.selector.find('.indicator-item').eq($this.currPosition).removeClass('active');
			if($this.currPosition == $this.numberOfSlides - $this.slidesPerScreen) {
				$(container).find('.indicator-item').eq($this.currPosition).addClass('active');
				return false;
			} else {
				$this.currPosition++;
			}
			// move slide
			$('#slidesHolder')
			.animate({'marginLeft' : $this.slideWidth*(-$this.currPosition)});
			// update indicator
			$(container).find('.indicator-item').eq($this.currPosition).addClass('active');
		});
		
		// add indicators
		if( options.indicators ) {
			// indicator wrapper
			var $indicators = $('<ul class="indicators"></ul>');
			// add one indicator for each slide and make it clickable
			this.slides.each( function(index) {
				var $indicator = $('<li class="indicator-item"></li>');
			
				// append them inside the indicator wrapper
				$indicators.append($indicator);
			});
			// add the whole indicator inside the selected container
			$(container).append($indicators);
			// initial slide with active indicator
			$(container).find('.indicator-item').eq(this.currPosition).addClass('active');
			
		}
		
		$zoomIndicatorLeftBtn = $("<a href='#' class='zoom-arrow prev'>&lt;</a>");
		$zoomIndicatorRightBtn = $("<a href='#' class='zoom-arrow next'>&gt;</a>");
		$currentIndexForZoom = 0;
		
		$(document).on("click", ".zoom-arrow", function(e){
			e.preventDefault();
			
			var count = $("#slidesHolder > img").length;
			
		});
		
		// zoom function
		if( options.zoom ) { 
			$($this.slides).on('click', function() {
				$currentIndexForZoom = $(this).data("idx");
				
				// get the current clicked image url
				var image_url = $(this).data('url');
				
				if(image_url == null || image_url == ""){
					image_url = $(this).attr('src');
				}
				
				// get currrent window width and height
				var screenWidth = $(window).width();
				var screenHeight = $(window).height();
				// calculate margin matching current 
				var horizontalMargin = screenWidth * 0.1;
				var verticalMargin = screenHeight * 0.1;

				$zoom_image = $('<img src="'+ baseUrl + image_url +'" class="zoom-image" />');
				
				$('html body').append($zoom_image);
				
				$zoom_image_wrapper = $('<div id="zoom-image-wrapper"></div>');
				$zoom_image.wrapAll($zoom_image_wrapper);
				
				$zoom_image.after($zoomIndicatorLeftBtn);
				$zoom_image.after($zoomIndicatorRightBtn);
					
				$zoom_image.addClass('img-responsive');
				
				$('html body').css('overflow','hidden');
				$('html body').append('<div id="background-overlay"></div>');
				$overlay = $('#background-overlay'); 
				$overlay.css('z-index','1002').css('display','block')
				.css('opacity','0.8');
				
				// closing action while the zoomed image is opened.
				$('#background-overlay').on('click', function() {
					$('#zoom-image-wrapper').remove();
					$('#background-overlay').remove();
					$('html body').css('overflow','scroll');
					
				});
				
				$($zoomIndicatorLeftBtn).on("click", function(e){
					e.preventDefault();
					
					if($currentIndexForZoom > 0) {
						$currentIndexForZoom = $currentIndexForZoom - 1;
					}else {
						$currentIndexForZoom = $this.slides.length - 1
					}
					
					$currentImageUrl = $("img.slide[data-idx='" + $currentIndexForZoom + "']").attr("src");
					$("#zoom-image-wrapper > img").attr("src", $currentImageUrl);
				});
				
				$($zoomIndicatorRightBtn).on("click", function(e){
					e.preventDefault();
					
					if($currentIndexForZoom < $this.slides.length - 1){
						$currentIndexForZoom = $currentIndexForZoom + 1;
					} else {
						$currentIndexForZoom = 0;
					}
					
					$currentImageUrl = $("img.slide[data-idx='" + $currentIndexForZoom + "']").attr("src");
					$("#zoom-image-wrapper > img").attr("src", $currentImageUrl);
				});
			});
		}

		return this;
	}; // var Slider = function (container, options) END...
	
	
	// function setting
	PTechSlider.fn = Slider.prototype = 
	{
			autoSlide: function () {	
				
				window.setInterval( function() {
					
					//update indicator
					$this.selector.find('.indicator-item').eq($this.currPosition).removeClass('active');
					// calculate slide position
					if($this.currPosition == $this.numberOfSlides - $this.slidesPerScreen) {
						$this.currPosition = 0;
					} else {
						$this.currPosition++;
					}
					// move slide
					$('#slidesHolder')
					.animate({'marginLeft' : $this.slideWidth*(-$this.currPosition)});
					// update indicator
					$this.selector.find('.indicator-item').eq($this.currPosition).addClass('active');
						
				}, $this.interval);

				return this;
			},

	};
	
	if(!window.PTechSlider) {
		window.PTechSlider = PTechSlider;
	}
	

})();






