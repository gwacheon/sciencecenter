Handlebars.registerHelper("checkWeek", function(index, amount, scope) {
	if ( index % 7 == amount ) {
		return scope.fn(this);
	} else{
		return scope.inverse(this);
	}	
});

Handlebars.registerHelper('compare', function(lvalue, rvalue, options) {
    if (arguments.length < 3)
        throw new Error("Handlerbars Helper 'compare' needs 2 parameters");

    var operator = options.hash.operator || "==";

    var operators = {
        '==':       function(l,r) { return l == r; },
        '===':      function(l,r) { return l === r; },
        '!=':       function(l,r) { return l != r; },
        '<':        function(l,r) { return l < r; },
        '>':        function(l,r) { return l > r; },
        '<=':       function(l,r) { return l <= r; },
        '>=':       function(l,r) { return l >= r; },
        'typeof':   function(l,r) { return typeof l == r; }
    }

    if (!operators[operator])
        throw new Error("Handlerbars Helper 'compare' doesn't know the operator "+operator);

    var result = operators[operator](lvalue,rvalue);

    if( result ) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
});

//format an ISO date using Moment.js
//http://momentjs.com/
//moment syntax example: moment(Date("2011-07-18T15:50:52")).format("MMMM YYYY")
//usage: {{dateFormat creation_date format="MMMM YYYY"}}
Handlebars.registerHelper('dateFormat', function(context, block) {
	if (window.moment) {
		var f = block.hash.format || "YYYY/MM/DD";
		return moment(context).format(f);
	}else{
		return context;   //  moment plugin not available. return data as is.
	};
});

var PTechCalendar = function (date, options){
	this.setDate(date);
	this.options = options;
	this.renderCalendar();
}

PTechCalendar.prototype.setDate = function(date){
	var date = new Date(date);
	
	var year = date.getFullYear();
	var month = date.getMonth();
	
	this.beginOfMonth = new Date(year, month, 1);
	this.lastOfMonth = new Date(year, month + 1, 0);
	this.prevMonth = new Date(year, month - 1, 1);
	this.nextMonth = new Date(year, month + 1, 1);
	
	var day = this.beginOfMonth.getDay();
	var diff = this.beginOfMonth.getDate() - day + (day == 0 ? -6 : 0); // adjust when day is sunday
	
	this.firstDay = new Date(this.beginOfMonth);
	this.firstDay.setDate(diff);
	this.endDay = new Date(this.lastOfMonth.getFullYear(), 
						this.lastOfMonth.getMonth(), 
						this.lastOfMonth.getDate() + 6 - this.lastOfMonth.getDay() 
					);
}

PTechCalendar.prototype.renderCalendar = function() {
	var dates = [];
	for(var d = this.firstDay; d <= this.endDay; d.setDate(d.getDate() + 1)){
		dates.push(new Date(d));
	}
	
	if(_.isUndefined(this.calendars)){
		this.calendars = $("#ptech-calendar-template").html();
		this.calendarsTemplate = Handlebars.compile(this.calendars);
	}
	
	this.calendarHtml = this.calendarsTemplate({
		beginOfMonth: this.beginOfMonth,
		prevMonth: this.prevMonth,
		nextMonth: this.nextMonth,
		dates: dates
	});
	
	$(this.options.container).html(this.calendarHtml);
	
	this.addEventHandlers();
	
	
	// Custom Event
	var event = jQuery.Event("changed");
	event.month = this.beginOfMonth;
	$("#ptech-calendar-template").trigger(event);
};

PTechCalendar.prototype.addEventHandlers = function(){
	var calendar = this;
	$(".month-nav-item").on("click", function(e){
		e.preventDefault();
		
		calendar.setDate($(this).data("date"));
		calendar.renderCalendar();
	});
}