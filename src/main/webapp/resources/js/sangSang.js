Handlebars.registerHelper('formatedDate', function(date, format) {
  return moment(date).format(format);
});

function prevMonth(date){
	return moment(date).add(-1, 'Month')._d;
}

function nextMonth(date){
	return moment(date).add(1, 'Month')._d;
}


function dates(date){
	ret = [];

	if(!_.isDate(date)){
		date = moment(new Date());
	}

	var beginOfMonth = moment(date).startOf('month').startOf('week');
	var endOfMonth = moment(date).endOf('month').endOf('week');

	for(var temp = beginOfMonth; temp.isBefore(endOfMonth); temp.add(1, 'days')){
		ret.push(temp.clone());
	}

	ret = _.chunk(ret, 7);

	return ret;
}

var SangSangCalendar = function(options) {

	this.header = $("meta[name='_csrf_header']").attr("content");
	this.token = $("meta[name='_csrf']").attr("content");

	this.source = $("#sangsang-calendar-template").html();
	this.template = Handlebars.compile(this.source);
}

SangSangCalendar.prototype.render = function(data){
	
	var source   = $("#sangsang-calendar-template").html();
	var template = Handlebars.compile(source);
	var html    = template(data);
	
	var equipmentId = data.equipmentId;
	var equiSerial = data.equiSerialId;
	var memberNo = data.memberNo;
	
	$("#equipment-calendar-"+data.equiSerialId).html(html);
	
	ajaxLoading();
    
	$.getJSON(baseUrl + "sangsang/getReservationsPerMonth", {
		equiSerialId: data.equiSerialId,
		beginDate: moment(selectedDate).startOf('month').startOf('week').format("YYYY-MM-DD"),
		endDate: moment(selectedDate).endOf('month').endOf('week').format("YYYY-MM-DD")
	}, function(data){
		if(data != null && data.equipReserves != null){
			var dates = [];
			
			_.forEach(data.equipReserves, function(n, key){
				var d = moment(n.beginTime).format("YYYY-MM-DD");
			    
				if(dates.indexOf(d) < 0){
					dates.push(d);
				}
			});
			_.forEach(dates, function(n, key){
				$(".calendar-item[data-id ='"+equiSerial+"'] .calendar-body .date[data-date='" + n + "']").addClass("has-reserve");
			});
			ajaxUnLoading();	
		}
	});
	
	this.getReservations(moment(selectedDate).format("YYYY-MM-DD"), equipmentId, data.equiSerialId);
	
	this.addEvents(equipmentId, data.equiSerialId);
}

SangSangCalendar.prototype.getReservations = function(date, equipmentId, equiSerialId){
	var calander = this;
	
	$.ajax({
		url: baseUrl + "sangsang/getEquipReserves",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		type : "POST",
		beforeSend: function(xhr) {
            xhr.setRequestHeader(calander.header, calander.token);
            ajaxLoading();
        },
		data: JSON.stringify({
			equipmentId : equipmentId,
			equiSerialId: equiSerialId,
			date: date
		}),
    	success: function(data){
    		var timeWithIndex = [];
    		_.forEach([
           			"09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00",
        			"16:00", "17:00"
    		    ], function(time, idx){
    			timeWithIndex.push({seq: idx, time: time});
    		});
    		
    		var source   = $("#sangsang-times-template").html();
    		var template = Handlebars.compile(source);
    		var html    = template({
    			serial: data.serial,
    			times: timeWithIndex,
    			date: date
   			});
    		
    		$(".equipment-serials[data-id='"+data.serial.id+"']").html(html);
    		
    		if(!_.isUndefined(data.equipReserves)){
    			_.forEach(data.equipReserves, function(equipReserve, key){
    				var d = moment(equipReserve.beginTime);
    				var dataSerial = "[data-serial='" + equipReserve.equiSerialId + "']";
    				var dataDate = "[data-date='" + d.format("YYYY-MM-DD") + "']";
    				var dataTime = "[data-time='" + d.format("HH:mm") + "']";
    				$(".check-time" + dataSerial + dataDate + dataTime).attr("disabled", true);
    				$(".reserve-time-label" + dataSerial + dataDate + dataTime).text("예약완료");
    				$(".reserve-text-label" + dataSerial + dataDate + dataTime).text("예약완료");
    			});
    		}
    		
    		if(!_.isUndefined(data.unavailable)){
    			console.log(data.unavailable);
    			_.forEach(data.unavailable, function(unavailable, key){
    				var d = moment(unavailable.beginTime);
    				var dataDate = "[data-date='" + d.format("YYYY-MM-DD") + "']";
    				var dataSerial = "[data-serial='" + unavailable.equiSerialId + "']";
    				
    				$(".check-time"+ dataSerial + dataDate ).attr("disabled", true);
    				$(".reserve-time-label" + dataSerial + dataDate ).text("예약 불가");
    				$(".reserve-text-label" + dataSerial + dataDate ).text("예약 불가");
    				
    			});

    		}
    		
    		ajaxUnLoading();
    		
    	},
    	error: function(err){
    		console.log(err);
    	}
	});
}
SangSangCalendar.prototype.submitReservations = function(){
	var SangSangCalendar = this;
	console.log(JSON.stringify(SangSangCalendar.reserves));
	
		$.ajax({
			url: baseUrl + "sangsang/equipReserves",
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			type : "POST",
			beforeSend: function(xhr) {
	            xhr.setRequestHeader(SangSangCalendar.header, SangSangCalendar.token);
	        },
			data: JSON.stringify(SangSangCalendar.reserves),
	    	success: function(data){
    			alert("정상 예약 되었습니다. 마이페이지에서 예약 내역을 확인하실 수 있습니다.");
	    		location.reload();
	    		
	    	},
	    	error: function(err){
	    		console.log(err);
	    	}
		});
}

SangSangCalendar.prototype.addEvents = function(equipmentId, equiSerialId){
	var sangsangCalander = this;
	$(".calendar-item[data-id ='"+equiSerialId+"'] td.date").on('click',function(e){
		sangsangCalander.getReservations($(this).data('date'),equipmentId, equiSerialId);
		$(".calendar-body .date").removeClass("active");
		$(this).addClass("active");

	});
	
	$(".calendar-item[data-id ='"+equiSerialId+"'] .month-selector").on('click',function(e){
		e.preventDefault();
		
		selectedDate = moment($(this).attr('data-date'))._d;
		sangsangCalander.render({
            selectedDate: moment(selectedDate)._d,
            prevMonth: prevMonth(selectedDate),
            nextMonth: nextMonth(selectedDate),
            dates: dates(selectedDate),
            equipmentId: equipmentId,
            equiSerialId: equiSerialId
        });
	});
	
	$('.calendar-item[data-id='+equiSerialId+'] .equip-reservation-modal').on('click',function(event){
		sangsangCalander.reserves = [];
		
		if($('#equipment-calendar-'+$(this).data('serial-id')+' .check-time:checked').length > 0){
			_.forEach($('#equipment-calendar-'+$(this).data('serial-id')+' .check-time:checked'), function(checkedTime, key){
				$("#reservation-infos").append("<li>" + $(checkedTime).data('time') + "</li>");
				sangsangCalander.reserves.push({
					seq: $(checkedTime).data('seq'),
					date: $(checkedTime).data('date'),
					time: $(checkedTime).data('time'),
					serialId: $(checkedTime).data('serial')
				});
			});
			
			sangsangCalander.reserves = _.groupBy(sangsangCalander.reserves, function(obj){
				return obj.serialId;
			});
			$('.reservation-modal[data-id='+equiSerialId+']').modal();
		}else{
			alert("선택된 시간이 없습니다.");
		}
	});
	
	$('.no-login-modal-btn').on('click',function(){
		if(confirm('로그인이 필요합니다. 로그인페이지로 이동하시겠습니까 ? ')){
			window.location = baseUrl+"users/login";
		}else{
			return false;
		}
		
	});
	$('.reservation-submit-btn[data-id='+equiSerialId+']').on('click',function(){
		sangsangCalander.submitReservations();
	});
}