var Volunteer = function(volunteer){
	this.volunteer = volunteer || {};
	
	this.handlebarSource = null;
	this.template = null;
	
	this.statusMsgs = {
		"accept": "수락",
		"reject": "거절",
		"completion": "수료",
		"return": "반려"
	}
	this.statusConfirmMsgs = {
		"accept": "선택하신 지원자의 자원봉사 신청을 수락하시겠습니까?",
		"reject": "선택하신 지원자의 자원봉사 신청을 거절하시겠습니까?",
		"completion": "선택하신 봉사자의 봉사활동이 수료 되었습니까? 수료 이후에는 봉사자가 본인의 계정으로 수료증을 발급할 수 있게 됩니다.",
		"return": "선택하신 봉사자의 봉사활동을 반려 처리 하시겠습니까?"
	}
}

Volunteer.prototype.setVolunteer = function(volunteer){
	this.volunteer = volunteer;
}

Volunteer.prototype.render = function(){
	if(this.handlebarSource == null){
		this.handlebarSource = $("#introduction-template").html();
		this.template = Handlebars.compile(this.handlebarSource);
	}
	
	return this.template(this.volunteer);
}

Volunteer.prototype.saveStatus = function(status){
	var v = this;
	var alertMsg = "";
	
	v.volunteer.status = status;
	v.volunteer.statusMsg = "";

	if(confirm(v.statusConfirmMsgs[status])){
		var volunteerId = $(this).data('id');
		
		$.ajax({
			type : "POST",
			beforeSend: function(xhr) {
	            xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));
	        },
			url : baseUrl + "admin/voluntaries/volunteers/" + v.volunteer.id + "/" + v.volunteer.status,
			contentType : 'application/json',
			success: function(data){
				
				console.log(status);
				$(".volunteer-row[data-id='" + data.volunteer.id + "']").find(".status").text(v.statusMsgs[status]);
				$(".volunteer-row[data-id='" + data.volunteer.id + "']").find(".status-btn").show();
				$(".volunteer-row[data-id='" + data.volunteer.id + "']").find("." + status + "-btn").hide();
				
				alert("정상 처리 되었습니다.");
			},
			error: function(err){
				console.log(err);
			}
		});
	}
}