function is_email(a){return /^([\w!.%+\-])+@([\w\-])+(?:\.[\w\-]+)+$/.test(a);}
function is_blank(value){return (value == null || value =="");}
function checkPassword(password, passwordConfirmation){
	var pwdExp = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{9,14}$/;
	var ret = {
		err: false,
		errMsg: ""
	};
	
	if(password == ""){
		ret["err"] = true;
		ret["errMsg"] = "비밀번호를 입력해 주세요."
	} else if (!pwdExp.test(password)) {
		ret["err"] = true;
		ret["errMsg"] = "비밀번호 규칙을 확인하시기 바랍니다.\n * 영문소문자, 숫자, 특수문자(!,@,$,%,^,&,* 만 가능)를 모두 1번 이상씩 사용하여 조합(9~14자리 이내)";
	} else if (password != passwordConfirmation) {
		ret["err"] = true;
		ret["errMsg"] = "비밀번호가 일치하지 않습니다.";
	} 
	
	return ret;
}