 var PAY_KIND = {
	card		: '1',
	vAccount	: '2',
	account		: '3',
	mobile		: '4'
};
 
var PAY_KIND_NAME = {
	card		: 'card',
	vAccount	: 'vAccount',
	account		: 'account',
	mobile		: 'mobile'
};

var payment_info = {
	kind:1,
	returnUrl:"",
	callbackUrl:"",
	mbr_id:""
};

var reserve_info = {
	program_cd		: '',						// 프로그램 코드
	program_name	: '',						// 프로그램명
	
	place_cd		: '',						// 장소 코드
	place_name		: '',						// 장소명
	
	play_date		: '',						// 입장 예정일
	play_seq_cd		: '',						// 회차 코드
	play_seq_name	: '',						// 회차명

	total_price		: 0,						// 합계 금액
	total_count 	: 0,						// 예약 인원
//	oid				: '',						// 주문번호
	
	payment			: {
		kind		: 1,						// 결제 종류 (1 : 신용카드, 2 : 가상계좌)
		form		: []						// 결제 관련 Form 정보 (target, action)
	},
	reserve_date	: '',						// 예약일시
	reserve_status	: '',						// 예약상태
	reserve_name	: '?',						// 예약자명
	reserve_phone	: '?'						// 예약자 연락처
};


var user_info = {};



// 결제 방식
function setPayType(num) {
//	payment_info.kind = num;
	setPayInfo(num);

	$("#payment-choice").modal('hide');
	
	doPay();
}



function doPay() {
	$.ajax({
		method: "GET",
		url: context_path + "/entrance/card",
		data: {
			UNIQUE_NBR	: reserve_info.unique_nbr
		}
	}).done(function(data) {
		//여기서 셋팅	
/*
>		mbr_id     = '';
>		salesPrice = data.result.TOT_SUM;
>		oid        = uniNbr;
>		payKind    = data.result.PAY_KIND;
		buyerName  = data.result.MEMBER_NAME;
		buyerMobile = data.result.MEMBER_TEL1;
		buyerEmail = data.result.MEMBER_EMAIL;
		productName = data.result.PROGRAM_NAME;
		salesPrice = data.result.TOT_SUM;
		productPrice = data.result.TOT_SUM;
		productCount = '1';
		openDate = data.result.PLAY_DATE;
		bizNo = '1203483832';
		callbackUrl = '';
		returnUrl ='';
		hashValue = '';
		authType = '';
*/

		payment_info.mbr_id				= data.result.MBR_ID;
//		payment_info.callbackUrl		= '';										// 페이지 이동  
//		payment_info.returnUrl			= '';										// API
			
		user_info.MEMBER_NAME			= data.UserInfo.MEMBER_NAME;				// 구매자 이름
		user_info.MEMBER_CEL			= data.UserInfo.MEMBER_CEL;					// 구매자 휴대폰
		user_info.MEMBER_EMAIL			= data.UserInfo.MEMBER_EMAIL;				// 구매자 이메일
		
//		reserve_info.unique_nbr			= uniNbr;									// 주문번호
		reserve_info.program_cd			= data.result.PROGRAM_CD;					// 상품명
		reserve_info.program_name		= data.result.PROGRAM_NAME;					// 상품명
		reserve_info.total_price		= data.result.TOT_SUM;						// 상품가격
		reserve_info.play_date			= data.result.PLAY_DATE;					// 입장예정일
		
		//------------------------
		
//		reserve_info.total_price		= data.result.TOT_SUM;
//		reserve_info.play_date			= data.result.PLAY_DATE;
//		reserve_info.unique_nbr			= uniNbr;
//
//		reserve_info.reserve_name		= data.result.MEMBER_NAME;
//		reserve_info.reserve_phone		= data.result.MEMBER_TEL1;
//		reserve_info.program_cd			= "";
//		reserve_info.program_name		= data.result.PROGRAM_NAME;
//		reserve_info.total_count		= 1;
//
//		reserve_info.place_cd			= "";
//		reserve_info.place_name			= "";
//		reserve_info.play_seq_cd		= "";
//		reserve_info.play_seq_name		= "";
//		reserve_info.payment			= {};
//		reserve_info.reserve_date		= "";
//		reserve_info.reserve_status		= "";
//		reserve_info.play_start_time	= "";
//		reserve_info.play_end_time		= "";
//		reserve_info.member_price		= [];
//		reserve_info.tran_seq			= "";
//		reserve_info.rsv_nbr			= "";
//		reserve_info.tran_date			= "";
//		reserve_info.tran_time			= ""
		
		
		if (confirm('결제 하시겠습니까?')) {
//			alert('payment_info.kind : ' + payment_info.kind);
			$.ajax({
				method: "GET",
				url: context_path + "/entrance/api/payment/hash",
				data: {
					mbr_id		: payment_info.mbr_id,				// 가맹점 ID
					salesPrice	: reserve_info.total_price,			// 판매가격
					oid			: reserve_info.unique_nbr			// 거래번호
				}
			}).done(function(data) {
				if ((typeof data) == 'string')
					data = JSON.parse(data);
				
				console.log("Data : " + JSON.stringify(data));
				
				if (data.hasOwnProperty("error")) {
					alert("결제 초기화에 실패했습니다.\n나중에 다시 시도해주세요.");
				} else {
					$('form[name=mallForm] > input[name=payKind]')[0].value			= payment_info.kind;					// 결제종류 (1:카드, 2:가상계좌, 3:계좌이체)

					$('form[name=mallForm] > input[name=mbrId]')[0].value			= payment_info.mbr_id;					// 가맹점 코드 
					$('form[name=mallForm] > input[name=mbrName]')[0].value			= '씨스퀘어';								// 가맹점명  
					$('form[name=mallForm] > input[name=buyerName]')[0].value		= user_info.MEMBER_NAME;				// 구매자 이름
					$('form[name=mallForm] > input[name=buyerMobile]')[0].value		= user_info.MEMBER_CEL;					// 구매자 휴대폰
					$('form[name=mallForm] > input[name=buyerEmail]')[0].value		= user_info.MEMBER_EMAIL;				// 구매자 이메일
					$('form[name=mallForm] > input[name=oid]')[0].value				= reserve_info.unique_nbr;				// 주문번호
					$('form[name=mallForm] > input[name=productName]')[0].value		= reserve_info.program_name;			// 상품명
					$('form[name=mallForm] > input[name=salesPrice]')[0].value		= reserve_info.total_price;				// 결제금액
					$('form[name=mallForm] > input[name=productPrice]')[0].value	= reserve_info.total_price;				// 상품가격
					$('form[name=mallForm] > input[name=productCount]')[0].value	= '1';									// 상품수량
					$('form[name=mallForm] > input[name=openDate]')[0].value		= reserve_info.play_date;				// 입장예정일
//					<p>사업자번호       <input type="text" name="bizNo" value="1203483832"></p> <!-- 가맹점사업자 번호 -->
					$('form[name=mallForm] > input[name=callbackUrl]')[0].value		= payment_info.callbackUrl;				// 페이지 이동  
					$('form[name=mallForm] > input[name=returnUrl]')[0].value		= payment_info.returnUrl;				// API
	// 리턴 타입 
					$('form[name=mallForm] > input[name=hashValue]')[0].value		= data.hash;							// 서명값 (hashValue)
					
//					$('form[name=mallForm]').attr('target', payment_info.target);
//					$("form[name=mallForm]").attr('action', payment_info.action);
					
//					$('form[name=mallForm] > input[name=authType]')[0].value		= payment_info.authType;					// 카드사 인증만 지원 
					//alert(payment_info.returnUrl);
					
					//$('form[name=mallForm]').submit();
					C2StdPay.pay();
				}
			}).fail(function(jqXHR, textStatus) {
				alert("결제 초기화에 실패했습니다.\n나중에 다시 시도해주세요.");
			});
		}

		return;
	}).fail(function(jqXHR, textStatus) {
		alert("나중에 다시 시도해주세요.");
	});
}


//callback.jsp에서 호출 
function funcPayResult(param)
{
	var result = ''
		+ 'Result Code : '	+ param.rstCode + '\n'
		+ 'Result Msg : '	+ param.rstMsg + '\n'
		+ 'Result Param : '	+ JSON.stringify(param);
	
	console.log(result);

	
	switch (param.payKind) {
		case PAY_KIND.card :
		case PAY_KIND.vAccount :
			if (param.rstCode == '0000') {
				// 결제 성공
				console.log("결제 성공");
				alert('결제가 완료되었습니다.');
				location.reload();
			} else {
				console.log("결제 실패");
				alert("결제에 실패했습니다.\n나중에 다시 시도해주세요.");
			}
			break;

		default:
			console.log(JSON.stringify(param));
			alert('지원되지 않는 결제 방식입니다.');
			break;
	}
}
