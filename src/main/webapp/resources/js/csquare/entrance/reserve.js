 var PAY_KIND = {
	card		: '1',
	vAccount	: '2',
	account		: '3',
	mobile		: '4'
};
 
var PAY_KIND_NAME = {
	card		: 'card',
	vAccount	: 'vAccount',
	account		: 'account',
	mobile		: 'mobile'
};




var reserve_info = {
	program_cd		: '',						// 프로그램 코드
	program_name	: '',						// 프로그램명
	
	place_cd		: '',						// 장소 코드
	place_name		: '',						// 장소명
	
	play_date		: '',						// 입장 예정일
	play_seq_cd		: '',						// 회차 코드
	play_seq_name	: '',						// 회차명

	total_price		: 0,						// 합계 금액
	total_count 	: 0,						// 예약 인원
//	oid				: '',						// 주문번호
	
	payment			: {
		kind		: 1,						// 결제 종류 (1 : 신용카드, 2 : 가상계좌)
		form		: []						// 결제 관련 Form 정보 (target, action)
	},
	reserve_date	: '',						// 예약일시
	reserve_status	: '',						// 예약상태
	reserve_name	: '?',						// 예약자명
	reserve_phone	: '?'						// 예약자 연락처
};



function getScheduleList(startMonth) {
	// 해당 월의 스케쥴 리스트 조회
	//console.log('스케쥴 리스트 조회');
	//console.log('program_cd	: ' + reserve_info.program_cd);
	//console.log('place_cd	: ' + reserve_info.place_cd);
	//console.log('target_ym	: ' + startMonth);
			
	// 전체 일정 초기화
	$('#program-calendar-wrapper').find('.ptech-date').removeClass('available');
	// 기존 회차 정보 삭제 :2016.07.06
	$('#program-times > table > tbody > tr').remove();
	
	$.ajax({
		method: "GET",
		url: context_path + "/entrance/api/scheduleList",
		data: {
			program_cd	: reserve_info.program_cd,			// '10000002',
			place_cd	: reserve_info.place_cd,			// '1002',
			target_ym	: startMonth
		}
	}).done(function(data) {
		if ((typeof data) == 'string') 
			data = JSON.parse(data);
		
		//console.log("ScheduleList Data : " + JSON.stringify(data));
		var FIRST_PLAY_DATE = 0;
		var cnt = 0;
		for (var i in data.list) {
			try {
				var strDate = data.list[i].PLAY_DATE;
				var DIFF_DATE_CHK = data.list[i].DIFF_DATE;
				
				if(DIFF_DATE_CHK>0){
					cnt++;
					if(cnt == 1 )FIRST_PLAY_DATE = strDate;
					
					strDate = strDate.substring(0, 4) + '/' + strDate.substring(4, 6) + '/' + strDate.substring(6, 8);
					// 특정 일자 일정 Setting
					$('#program-calendar-wrapper > div [data-date="' + strDate + '"]').addClass('available');
				}

			} catch (e) {}
		}

		// 2016.06.20 추가
		// 첫번째 예약 가능일을 선택한 상태로...
        // 당일날짜 이전달 선택 시 오류 수정 : 2016.07.06
		var strDate = FIRST_PLAY_DATE;//data.list[0].PLAY_DATE;
		if(strDate > 0){
			registDateHandler();
			strDate = strDate.substring(0, 4) + '/' + strDate.substring(4, 6) + '/' + strDate.substring(6, 8);
			$('#program-calendar-wrapper > div [data-date="' + strDate + '"]').trigger('click');
		}
		
		
	}).fail(function(jqXHR, textStatus) {
//			alert( "Request failed: " + textStatus );
	});
}




function getSeqList(playDate) {
	reserve_info.play_date = playDate;
	
	// 해당 일의 회차 리스트 조회
	//console.log('회차 리스트 조회');
	//console.log('program_cd	: ' + reserve_info.program_cd);
	//console.log('place_cd	: ' + reserve_info.place_cd);
	//console.log('play_date	: ' + reserve_info.play_date);
	
	// 기존 회차 정보 삭제
	$('#program-times > table > tbody > tr').remove();
	
	$.ajax({
		method: "GET",
		url: context_path + "/entrance/api/seqList",
		data: {
			program_cd	: reserve_info.program_cd,			// '10000002',
			place_cd	: reserve_info.place_cd,			// '1002',
			play_date	: reserve_info.play_date
		}
	}).done(function(data) {
		if ((typeof data) == 'string') 
			data = JSON.parse(data);
		
		//console.log("Data : " + JSON.stringify(data));
		/*
		//인터넷 예매 플래그 적용 시
		if(data.list == 0){
			alert("예매 가능한 정보가 없습니다. 다른 날짜를 선택하여 주세요.");
			return false;
		}
		*/
		
		for (var i in data.list) {
			try {
				var item = data.list[i];
				var play_seq_name = '';
				try {
					play_seq_name = item.PLAY_SEQ_NM + ' ' + item.PLAY_ST_TIME.substring(0, 2) + ':' + item.PLAY_ST_TIME.substring(2, 4) + ' ~ ' + item.PLAY_ED_TIME.substring(0, 2) + ':' +  item.PLAY_ED_TIME.substring(2, 4);
				} catch (e) {
					play_seq_name = item.PLAY_SEQ_NM;
				}
				
				var strHTML = ''
							+ '<tr>'
							+ '    <td class="center">' + item.PLAY_SEQ_NM + '</td>'
							+ '    <td class="center">'
									+ item.PLAY_ST_TIME.substring(0, 2) + ':' + item.PLAY_ST_TIME.substring(2, 4)
									+ ' ~ '
									+ item.PLAY_ED_TIME.substring(0, 2) + ':' + item.PLAY_ED_TIME.substring(2, 4)
							+ '    </td>'
							+ '    <td class="center">' + item.REMAIN_SEAT_CNT + '/' + item.ASSIGN_SEAT_CNT + '</td>'
							+ '    <td class="center">'
							+ (
									//현재 시간 10분 이전 회차는 마감 처리. :2016.07.06
									(item.REMAIN_SEAT_CNT > 0 && item.DIFF_DATETIME >= 10)
									? '<button type="button" class="btn btn-primary reserve-btn" onClick="goStep3(\'' + item.PLAY_SEQ_CD + '\', \'' + play_seq_name + '\', \'' + item.PLAY_ST_TIME + '\', \''+ item.PLAY_ED_TIME + '\', ' + item.REMAIN_SEAT_CNT + ')">선택</button>'
									: '<button type="button" class="btn btn-disabled">마감</button>'
							  )
							+ '    </td>'
							+ '</tr>';
				
				$('#program-times > table > tbody').append(strHTML);
//					$('#program-times > table > tbody:last').append(strHTML);
			} catch (e) {}
		}
	}).fail(function(jqXHR, textStatus) {
//			alert( "Request failed: " + textStatus );
	});
}



function selectProgram(program_cd, program_name, place_cd, place_name) {
	//console.log('selectProgram');
	reserve_info.program_cd		= program_cd;
	reserve_info.program_name	= program_name;
	reserve_info.place_cd		= place_cd;
	reserve_info.place_name		= place_name;
	

	// Custom Event
	var event = jQuery.Event("changed");
	event.month = ptechCalendar.beginOfMonth;
	$("#ptech-calendar-template").trigger(event);
}

function setStep2Subtitle(str) {
	$('#selected-time > .text').text(str);
}

function setStep3Subtitle(str) {
	$("#selected-price > .text").text(str);
}

function setStep4Subtitle(str) {
	$("#step04_subtitle > .text").text(str);
}

function getInputDayLabel(play_date) {
	//var date = string2Date(play_date).format("yyyy-MM-dd");
	var date = string2Date(play_date, 1);
	var week = new Array('일', '월', '화', '수', '목', '금', '토');
	var today = new Date(date.toString()).getDay();
	var todayLabel = week[today];
	
	return todayLabel;
}

function goStep3(playSeqCD, playSeqName, playStartTime, playEndTime, remain_seat_cnt) {
	reserve_info.play_seq_cd		= playSeqCD;
	reserve_info.play_seq_name		= playSeqName;
	reserve_info.play_start_time	= playStartTime;
	reserve_info.play_end_time		= playEndTime;
	reserve_info.remain_seat_cnt	= remain_seat_cnt;
	
	
	$("#selected-time > .text").text($(".reserve-btn:first-child").data("title"));

	$("#step2").removeClass("active");
	$("#step2 .process-contents").slideUp();
	
	$("#step3").addClass("active");
	$("#step3 .process-contents").slideDown();
	
	// 날짜/회차 표기 (2015년 10월 08일 (금) / 2회차 12:00 ∼ 13:00)
	//setStep2Subtitle(string2Date(reserve_info.play_date).format("yyyy년 MM월 dd일") +'('+getInputDayLabel(reserve_info.play_date)+')' +' / ' + reserve_info.play_seq_name);
	setStep2Subtitle(string2Date(reserve_info.play_date, 2) +'('+getInputDayLabel(reserve_info.play_date)+')' +' / ' + reserve_info.play_seq_name);
	
	var ticketTable = $('#step3').find('table');
	// 기존 권종 리스트 정보 삭제
	ticketTable.find('tbody > tr').remove()
	
	// 권종 리스트 조회
	$.ajax({
		method: "GET",
		url: context_path + "/entrance/api/priceTypeList",
		data: {
			program_cd	: reserve_info.program_cd,			// '10000002',
			place_cd	: reserve_info.place_cd,			// '1002',
			play_date	: reserve_info.play_date,
			play_seq_cd	: reserve_info.play_seq_cd
		}
	}).done(function(data) {
		if ((typeof data) == 'string') 
			data = JSON.parse(data);
		
		//console.log("Data : " + JSON.stringify(data));
		
		for (var i in data.list) {
			try {
				var item = data.list[i];
				
				var strHTML = ''
							+ '<tr>'
							+ '    <td>' + item.PRICE_TYPE_NAME + '</td>'
							+ '    <td>' + item.PRICE + '</td>'
							+ '    <td>'
							+ '        <select class="form-control member-price" data-price="' + item.PRICE + '" data-target="#' + item.PRICE_TYPE_CD + '">'
							+ '            <option value="0">인원</option>'
				;
				
				var person_count = 3;
				// 천체투영관 19명까지, 스페이스월드 90명까지
				//person_count = (reserve_info.program_cd == "10000002") ? 19 : 90;
				// 천체 투영관 90개, 스페이스 월드 19개 : 2016.06.13 요청으로 변경 
				person_count = (reserve_info.program_cd == "10000002") ? 150 : 40;
				
				
				for (var j=1; j <= person_count; j++) {
					strHTML += '<option value="' + j + '">' + j + '</option>';
				}
				
				strHTML += ''
							+ '        </select>'
							+ '    </td>'
							+ '    <td id="' + item.PRICE_TYPE_CD + '"></td>'
							+ ((i == 0) ? '    <td rowspan="' + data.list.length + '" id="total-price"></td>' : '')
							+ '</tr>'
				;
				
				ticketTable.find('tbody').append(strHTML);
				
				// SELECT 선택 이벤트 핸들러 등록 필요
				registPersonSelectHandler();
			} catch (e) {}
		}
		
		registPersonSelectHandler();
	}).fail(function(jqXHR, textStatus) {
//			alert( "Request failed: " + textStatus );
	});
}

function sendReserve() {
	//console.log('>>>>> Test : ' + count);
	var maxCnt = 0;

	if (reserve_info.program_cd == '10000002') {
		maxCnt = 150;			// 천체투영관
	} else {
		maxCnt = 40;			// 스페이스월드
	}
	
	var count = 0;
	$('#step3 select').each(function(index) {
		//console.log(index + ' : ' + $(this)[0].value);
		count += parseInt($(this)[0].value);
	});
	
	if (count > maxCnt) {
		// 인당 예매 제한 (19매)
		alert('예약인원이 초과되었습니다. 1인당 ' + maxCnt + '명까지 예약 가능합니다.');
	} else if (count == 0) {
		alert('인원을 선택해 주세요');
	} else if (count > reserve_info.remain_seat_cnt) {
		alert('잔여 인원을 초과하여 예약할수 없습니다.');
	} else {

        // Server에 예약 API 호출
        $.ajax({
            method: "POST",
            url: context_path + "/entrance/api/reserve/step01",
            data: {
                reserve_info : JSON.stringify(reserve_info)     // 예약 정보
            }
        }).done(function(data) {
            if ((typeof data) == 'string') 
                data = JSON.parse(data);
            
            //console.log("Data : " + JSON.stringify(data));
            
            reserve_info.tran_seq   = data.tran_seq;                        // 거래 일련 번호
            reserve_info.unique_nbr = data.unique_nbr;                  // 거래고유번호
            reserve_info.rsv_nbr    = data.rsv_nbr;                     // 예매번호

            reserve_info.tran_date  = data.tran_date;                   // 예약일자
            reserve_info.tran_time  = data.tran_time;                   // 예약시간
            
            
//          reserve_info.reserve_date = new Date();
            try {
                reserve_info.reserve_date = new Date(
                    reserve_info.tran_date.substring(0, 4),
                    reserve_info.tran_date.substring(4, 6),
                    reserve_info.tran_date.substring(6, 8),
                    reserve_info.tran_time.substring(0, 2),
                    reserve_info.tran_time.substring(2, 4),
                    reserve_info.tran_time.substring(4, 6)
                );
            } catch (e) {
                reserve_info.reserve_date = new Date();
            }
            
            if (data.hasOwnProperty("error")) {
                // 2016.07.06
            	if(data.hasOwnProperty("errorCd") && data.errorCd == "RS")
            		alert(data.errorMsg);
            	else
            		alert('예약중 오류가 발생했습니다.');
        		//alert(data.errorMsg);
            } else {
        		// 결제 여부 선택
//        		$('#payment-yn-modal').modal();
            	
            }
        }).fail(function(jqXHR, textStatus) {
        });
	}
}


function goStep4() {
//	$("#payment-yn-modal").modal('hide');

	var count = 0;
	$('#step3 select').each(function(index) {
		//console.log(index + ' : ' + $(this)[0].value);
		count += parseInt($(this)[0].value);
	});
	
	//console.log('>>>>> Test : ' + count);
	var maxCnt = 0;
	
	if (reserve_info.program_cd == '10000002') {
		maxCnt = 150;			// 천체투영관
	} else {
		maxCnt = 40;			// 스페이스월드
	}
	
	if (count > maxCnt) {
		// 인당 예매 제한 (19매)
		alert('예약인원이 초과되었습니다. 1인당 ' + maxCnt + '명까지 예약 가능합니다.');
	} else if (count == 0) {
		alert('인원을 선택해 주세요');
	} else if (count > reserve_info.remain_seat_cnt) {
		alert('잔여 인원을 초과하여 예약할수 없습니다.');
	} else {
		sendReserve();
		
		$("#step3").removeClass("active");
		$("#step4").addClass("active");

		//$("#selected-price > .text").text(numberWithCommas(totalPrice) + "원");
		setStep3Subtitle('총 ' + count + '명');
		setStep4Subtitle('총결제금액 ' + numberWithCommas(totalPrice) + '원');
		
		// 결재하기 버튼에 총 가격 출력			
//		$("#price-for-payment").text(" (" + numberWithCommas(totalPrice) + " 원)");

		$("#step3 .process-contents").slideUp();
		$("#step4 .process-contents").slideDown();
		
		//권종/인원선택 이후 10분 타이머를 생성하여 10분이 넘으면 보류 좌석 자동으로 해지.
		countdownTimer.resetCountdown(); //countdown reset
		countdownTimer.Timer.toggle();   //countdown start
	}
}

// 결제 완료 후 예약 완료 페이지로 이동
function goComplete(resultParam) {
	alert("예약이 접수되었습니다. 감사합니다.");
	
	// 예약 결과 화면 출력 (전문 통신 완료 후)
//	$('#complete-container #btnReservePrint').
	
	$('#complete-container').find('#program_name')[0].innerText		= reserve_info.program_name;							// 프로그램명
	//var play_date = string2Date(reserve_info.play_date).format("yyyy년 MM월 dd일 E");
	//var play_date = string2Date(reserve_info.play_date).format("yyyy년 MM월 dd일");
	var play_date = string2Date(reserve_info.play_date, 1);
	play_date = play_date +" "+ getInputDayLabel(reserve_info.play_date)+"요일";
	
	$('#complete-container').find('#play_date')[0].innerText		= play_date;		
	// 입장 예정일 (요일)
	$('#complete-container').find('#play_seq')[0].innerText			= reserve_info.play_seq_name;							// 회차, 시간
	$('#complete-container').find('#place_name')[0].innerText		= reserve_info.place_name;								// 장소명

	
	// 예약구분		(예 : 상영관 예약)
//	$('#complete-container').find('#oid')[0].innerText				= reserve_info.unique_nbr;								// 예약코드		(예 : R845788555)
	$('#complete-container').find('#oid')[0].innerText				= reserve_info.rsv_nbr;									// 예약코드		(예 : R845788555)
	
	var reserve_date = reserve_info.reserve_date.format("yyyy년 MM월 dd일 a/p hh시 mm분");
	$('#complete-container').find('#reserve_date')[0].innerText		= reserve_date;											// 예약일시		(예 : 2015년 8월 12일 13:34)
	
	$('#complete-container').find('#reserve_name')[0].innerText		= (user_info != null) ? user_info.MEMBER_NAME : '';		// 예약자명		(예 : 성춘향)
	$('#complete-container').find('#reserve_phone')[0].innerText	= (user_info != null) ? user_info.MEMBER_CEL : '';		// 연락처			(예 : 010-1234-5678)
	$('#complete-container').find('#total_count')[0].innerText		= numberWithCommas(reserve_info.total_count) + '명';		// 관람인원		(예 : 5명)
	$('#complete-container').find('#total_price')[0].innerText		= numberWithCommas(reserve_info.total_price) + '원';		// 결제금액		(예 : 300,000원)
	
	switch (resultParam.payKind) {
		case PAY_KIND.card :
//			var result = {
//				"cardApprovNo":"00586783",
//				"cardTradeNo":"CS344160430161657259",
//				"cardApprovDate":"160430",
//				"cardName":"현대카드",
//				"cashReceipt":"null",
//				"vAccountBankName":"null",
//				"vAccount":"null",
//				"accountBankName":"null",
//				"payType":"3D",
//				"payKind":"1",
//				"returnType":"payment"
//			}
		
			// 결제방식		(예 : 신용카드 (하나카드 1234-4567-****-****) 결제완료
			$('#complete-container').find('#payment')[0].innerText			= '신용카드 (' + resultParam.cardName + ') 결제완료';
			$('#complete-container').find('#reserve_status')[0].innerText	= '예약 완료';													// 예약상태		(예 : 예약완료)

			break;
		case PAY_KIND.vAccount :
//			var a = {
//				"cardApprovNo":"null",
//				"cardTradeNo":"null",
//				"cardApprovDate":"null",
//				"cardName":"null",
//				"cashReceipt":"null",
//				"vAccountBankName":"우리은행",
//				"vAccount":"26510172018103    ",
//				"accountBankName":"null",
//				"payType":"vAccount",
//				"payKind":"2",
//				"returnType":"payment"
//			}

			// 결제방식		(예 : 가상계좌 (우리은행 0000000000000) 입금대기
			$('#complete-container').find('#payment')[0].innerText			= '가상계좌 (' + resultParam.vAccountBankName + ' ' + resultParam.vAccount + ') 입금대기';
			$('#complete-container').find('#reserve_status')[0].innerText	= '입금 대기';													// 예약상태		(예 : 예약완료)

			break;
	}

	$('#schedule-container').hide();
	$('#complete-container').show();	
}


function registDateHandler() {
	// 일자 선택
	$('.ptech-date.available').on("click", function(e){
		//console.log('일자 선택 : ' + e.currentTarget.dataset.date);
		//2016/04/26
		var playDate = "";
		if(e.currentTarget.dataset != null){
		   playDate = e.currentTarget.dataset.date.substring(0, 4)
					+ e.currentTarget.dataset.date.substring(5, 7)
					+ e.currentTarget.dataset.date.substring(8, 10);
		}else{
			var tmpVar = e.currentTarget.outerHTML;
			//IE9, 10에서 e.currentTarget.dataset를 인식하지 못하여 수동으로 적용
			//data-date= 기준이 56 지점부터 날짜 끝나는 날까지 적용하여 가져옴
			//playDate = tmpVar.substring(56, 66);
			playDate = tmpVar.substring(tmpVar.indexOf("data-date")+11, tmpVar.indexOf("data-date")+21);
			playDate = playDate.substring(0, 4) + playDate.substring(5, 7) + playDate.substring(8, 10);			
		}
		
		getSeqList(playDate);
	});
}

// 결제 방식
function setPayType(num) {
//	payment_info.kind = num;
	setPayInfo(num);
//	$("#payment-choice").modal('hide');
	doPay();
}

var stdPayPop;
function doPay() {
	if (confirm('결제 하시겠습니까?')) {
		
//		alert('payment_info.kind : ' + payment_info.kind);

		
		$.ajax({
			method: "GET",
			url: context_path + "/entrance/api/payment/hash",
			data: {
				mbr_id		: payment_info.mbr_id,				// 가맹점 ID
				salesPrice	: reserve_info.total_price,			// 판매가격
				oid			: reserve_info.unique_nbr,			// 거래번호
				type        : "enter"
			}
		}).done(function(data) {
			if ((typeof data) == 'string')
				data = JSON.parse(data);
			
			//console.log("Data : " + JSON.stringify(data));
			
			if (data.hasOwnProperty("error")) {
                // 2016.07.06
				if(data.hasOwnProperty("errorCd") && data.errorCd == "XXX"){
					alert(data.errorMsg);
					setStep2Subtitle('');
					setStep3Subtitle('');
					setStep4Subtitle('');
					ExpandStep(2);
				}else{
            		alert("결제 초기화에 실패했습니다.\n나중에 다시 시도해주세요.");
            	}
			} else {
				$('form[name=mallForm] > input[name=payKind]')[0].value			= payment_info.kind;					// 결제종류 (1:카드, 2:가상계좌, 3:계좌이체)

				$('form[name=mallForm] > input[name=mbrId]')[0].value			= payment_info.mbr_id;					// 가맹점 코드 
				$('form[name=mallForm] > input[name=mbrName]')[0].value			= '씨스퀘어';								// 가맹점명  
				$('form[name=mallForm] > input[name=buyerName]')[0].value		= user_info.MEMBER_NAME;				// 구매자 이름
				$('form[name=mallForm] > input[name=buyerMobile]')[0].value		= user_info.MEMBER_CEL;					// 구매자 휴대폰
				$('form[name=mallForm] > input[name=buyerEmail]')[0].value		= user_info.MEMBER_EMAIL;				// 구매자 이메일
				$('form[name=mallForm] > input[name=oid]')[0].value				= reserve_info.unique_nbr;				// 주문번호
				$('form[name=mallForm] > input[name=productName]')[0].value		= "110062" + reserve_info.program_cd;	// 상품명 
				$('form[name=mallForm] > input[name=salesPrice]')[0].value		= reserve_info.total_price;				// 결제금액
				$('form[name=mallForm] > input[name=productPrice]')[0].value	= reserve_info.total_price;				// 상품가격
				$('form[name=mallForm] > input[name=productCount]')[0].value	= '1';									// 상품수량
				$('form[name=mallForm] > input[name=openDate]')[0].value		= reserve_info.play_date.substring(2, 8); // 입장예정일(YYMMDD)
//				<p>사업자번호       <input type="text" name="bizNo" value="1203483832"></p> <!-- 가맹점사업자 번호 -->
				$('form[name=mallForm] > input[name=callbackUrl]')[0].value		= payment_info.callbackUrl;				// 페이지 이동  
				$('form[name=mallForm] > input[name=returnUrl]')[0].value		= payment_info.returnUrl;				// API
// 리턴 타입 
				$('form[name=mallForm] > input[name=hashValue]')[0].value		= data.hash;							// 서명값 (hashValue)
				
//				$('form[name=mallForm]').attr('target', payment_info.target);
//				$("form[name=mallForm]").attr('action', payment_info.action);
				
//				$('form[name=mallForm] > input[name=authType]')[0].value		= payment_info.authType;					// 카드사 인증만 지원 
				//alert(payment_info.returnUrl);
				
				//$('form[name=mallForm]').submit();
				
				$('form[name=mallForm] > input[name=viewType]')[0].value		= (isMobile() ? 'self' : 'popup');		// popup, self, overay, mobile
				$('form[name=mallForm] > input[name=server]')[0].value			= (isProduction ? '1' : '0');			// 0 : 테스트 서버, 1 : 운영서버
				$('form[name=mallForm] > input[name=effTime]')[0].value			= '20';                                   // 유효시간 (20 min)
				C2StdPay.pay();
				stdPayPop = stdpaywin;
			}
		}).fail(function(jqXHR, textStatus) {
			alert("결제 초기화에 실패했습니다.\n나중에 다시 시도해주세요.");
		});
	}

	return;
}



// callback.jsp에서 호출 
function funcPayResult(param)
{
	var result = ''
		+ 'Result Code : '	+ param.rstCode + '\n'
		+ 'Result Msg : '	+ param.rstMsg + '\n'
		+ 'Result Param : '	+ JSON.stringify(param);
	
	//console.log(result);

	
	switch (param.payKind) {
		case PAY_KIND.card :
		case PAY_KIND.vAccount :
			if (param.rstCode == '0000') {
				// 결제 성공
				//console.log("결제 성공");
				goComplete(param);
			}else if (param.rstCode == '9898') {
				alert("결제 시간이 초과하였습니다.\n다시 시도해주세요.");
				setStep2Subtitle('');
				setStep3Subtitle('');
				setStep4Subtitle('');
				ExpandStep(2);
			} else {
				//console.log("결제 실패");
				alert("결제에 실패했습니다.\n다시 시도해주세요.");
				setStep2Subtitle('');
				setStep3Subtitle('');
				setStep4Subtitle('');
				ExpandStep(2);
			}
			break;

		default:
			//console.log(JSON.stringify(param));
			alert('지원되지 않는 결제 방식입니다.');
			setStep2Subtitle('');
			setStep3Subtitle('');
			setStep4Subtitle('');
			ExpandStep(2);
			break;
	}
}

function registPersonSelectHandler() {
    $(".member-price").change(function(e) {
        var price = parseInt($(this).data("price"));
        var calPrice = price * parseInt($(this).val());
        if (calPrice > 0) {
        	calPrice = numberWithCommas(calPrice) + ' 원';
        } else {
        	calPrice = '';
        } 
        $($(this).data("target")).text(calPrice);

        // 합계 금액 계산
        totalPrice = 0;
        _.each($(".member-price"), function(d, i){
            totalPrice += parseInt($(d).data("price")) * parseInt($(d).val());
        });

        $("#total-price").text(numberWithCommas(totalPrice) + " 원");
        reserve_info.total_price = totalPrice;
        
        // 인원수 계산
        totalCount = 0;
        _.each($(".form-control.member-price"), function(d, i){
//          debugger
            var value = Number(d.value);
//          //console.log('인원수 : ' + value);
            if (!isNaN(value))
                totalCount += value;
        });

        
        // 권종/인원 저장
        reserve_info.member_price = [];
        _.each($(".member-price"), function(d, i){
            if (parseInt($(d).val()) > 0) {
                sPriceTypeCd = $(d).data("target").replace(/#/gi, '');
                nCnt = parseInt($(d).val());
                nPrice = parseInt($(d).data("price"));
                
                //console.log('>>> 권종 : ' + sPriceTypeCd);
                //console.log('>>> 금액 : ' + nCnt);
                //console.log('>>> 수량 : ' + nPrice);

                reserve_info.member_price.push({
                    price_type_cd   : sPriceTypeCd,
                    cnt             : nCnt,
                    price           : nPrice
                });
            }
        });
        
        reserve_info.total_count = totalCount;
    });
}




$( document ).ajaxStart(function() {
	ajaxLoading();
});
$( document ).ajaxStop(function() {
	ajaxUnLoading();
})

function ExpandStep(step) {
	switch(step) {
		case 2:
			$('#step2 .process-contents').slideDown();	
			$('#step2').addClass('active');
			$('#step3 .process-contents').slideUp();
			$('#step3').removeClass('active');
			$('#step4 .process-contents').slideUp();
			$('#step4').removeClass('active');
			//날짜/회차 선택 시 HOLD 좌석 데이터 삭제 및 데이터 재 조회 :2016.07.06
			fnDateReserve();
			break;
		case 3:
			$('#step2 .process-contents').slideUp();
			$('#step2').removeClass('active');
			$('#step3 .process-contents').slideDown();	
			$('#step3').addClass('active');
			$('#step4 .process-contents').slideUp();
			$('#step4').removeClass('active');
			//날짜/회차 선택 시 HOLD 좌석 데이터 삭제 및 데이터 재 조회 :2016.07.06
			fnDateReserve();
			break;
		case 4:
			$('#step2 .process-contents').slideUp();
			$('#step2').removeClass('active');
			$('#step3 .process-contents').slideUp();
			$('#step3').removeClass('active');
			$('#step4 .process-contents').slideDown();	
			$('#step4').addClass('active');
			break;
			
	default:
		
		break;
	}
}

function setStepTitleClickHandler(step) {
	$('#step2 .process-title').unbind('click');
	$('#step3 .process-title').unbind('click');
	
	if (step > 2) {
		$('#step2 .process-title').on('click', function(event) {
			setStep2Subtitle('');
			setStep3Subtitle('');
			setStep4Subtitle('');
			ExpandStep(2);
		});
	}
	
	if (step > 3) {
		$('#step3 .process-title').on('click', function(event) {
			setStep3Subtitle('');
			setStep4Subtitle('');
			ExpandStep(3);
		});
	}
}


$(document).ready(function() {
	reserve_info.reserve_name = ((user_info != null) ? user_info.MEMBER_NAME : '');					// 예약자명
	reserve_info.reserve_phone = ((user_info != null) ? user_info.MEMBER_CEL : '');					// 예약자 연락처

	if (isMobile()) {
		// 모바일 환경에서는 결제 방식을 팝업으로 선택 
		$('#payment_method').hide()
	}
	
//	alert('Mobile : ' +  isMobile());
	(function($) {
		var methods = [ 'addClass', 'toggleClass', 'removeClass' ];

		$.each(methods, function(index, method) {
			var originalMethod = $.fn[method];

			$.fn[method] = function() {
				try {
					var oldClass = this[0].className;
					var result = originalMethod.apply(this, arguments);
					var newClass = this[0].className;

					this.trigger(method, [ oldClass, newClass ]);

					return result;
				}
				catch(exception){
				}
			};
		});
	}(window.jQuery || window.Zepto));
	
	// ///////////////////////////////
	// Event Handler//
	
	$('#payment-btn').click(function() {
		// 결제 버튼 클릭 
		if (isMobile()) {
			//$('#payment-choice').modal();
			// 2016.06.15 요청으로 결제 방식 선택 없이 신용카드 결제 
			doPay();
		} else {
			doPay();
		}
	});

	
	$("#ptech-calendar-template").on('changed', function(e) {
		//console.log('ptechCalendar.changed : ' + e.month);
		
		var sMM = '' + (e.month.getMonth() + 1);
		if (sMM.length == 1)
			sMM = '0' + sMM;
		
		var sYYYYMM = '' + e.month.getFullYear();
		sYYYYMM += sMM;
		
		//console.log('ptechCalendar.changed : ' + sYYYYMM);
		
		getScheduleList(sYYYYMM)
	});
	
	$("#step2").on('addClass', function(e, oldClass, newClass) {
//		console.log('Changed from %s to %s due %s', oldClass, newClass, e.type);
		if ($('#step2').hasClass('active')) {
			$('#step2, #step3, #step4').css('cursor', 'auto');
			setStepTitleClickHandler(2);
		}
		
	});
	
	$("#step3").on('addClass', function(e, oldClass, newClass) {
		//console.log('Changed from %s to %s due %s', oldClass, newClass, e.type);
		if ($('#step3').hasClass('active')) {
			$('#step3, #step4').css('cursor', 'auto');
			$('#step2').css('cursor', 'pointer');
			setStepTitleClickHandler(3);
		}
		
	});

	$("#step4").on('addClass', function(e, oldClass, newClass) {
		//console.log('Changed from %s to %s due %s', oldClass, newClass, e.type);
		if ($('#step4').hasClass('active')) {
			// Step4 열릴때
			$('#step4').css('cursor', 'auto');
			$('#step2, #step3').css('cursor', 'pointer');
			setStepTitleClickHandler(4);

			
			
//			// Server에 예약 API 호출
//			$.ajax({
//				method: "POST",
//				url: context_path + "/entrance/api/reserve/step01",
//				data: {
//					reserve_info : JSON.stringify(reserve_info)		// 예약 정보
//				}
//			}).done(function(data) {
//				if ((typeof data) == 'string') 
//					data = JSON.parse(data);
//				
//				console.log("Data : " + JSON.stringify(data));
//				
//				reserve_info.tran_seq	= data.tran_seq;						// 거래 일련 번호
//				reserve_info.unique_nbr	= data.unique_nbr;					// 거래고유번호
//				reserve_info.rsv_nbr	= data.rsv_nbr;						// 예매번호
//
//				reserve_info.tran_date	= data.tran_date;					// 예약일자
//				reserve_info.tran_time	= data.tran_time;					// 예약시간
//				
//				
////				reserve_info.reserve_date = new Date();
//				try {
//					reserve_info.reserve_date = new Date(
//						reserve_info.tran_date.substring(0, 4),
//						reserve_info.tran_date.substring(4, 6),
//						reserve_info.tran_date.substring(6, 8),
//						reserve_info.tran_time.substring(0, 2),
//						reserve_info.tran_time.substring(2, 4),
//						reserve_info.tran_time.substring(4, 6)
//					);
//				} catch (e) {
//					reserve_info.reserve_date = new Date();
//				}
//				
//				if (data.hasOwnProperty("error")) {
//				} else {
//				}
//			}).fail(function(jqXHR, textStatus) {
//			});
		}
	});
	
	
	$('#complete-container #btnReservePrint').on("click", function(e) {
		// 예약증 인쇄 버튼 클릭
		if("Y" == reserve_info.print_yn) {
			alert('이미 예약증을 인쇄했습니다.\n예약증은 1회만 인쇄가 가능합니다.');
			return
		}else{
			
			$.ajax({
				method: "GET",
				url: context_path + "/entrance/print",
				data: {
					UNIQUE_NBR	: reserve_info.unique_nbr,
				}
			}).done(function(data) {
				reserve_info.print_yn = 'Y';
				// window.print();
//				$('#complete-container').show().printElement();
//				$('#complete-container').printThis();
//				$('#complete-container .reservation-receipe-wrapper').printThis();
				PrintProc();
			}).fail(function(jqXHR, textStatus) {
				alert("나중에 다시 시도해주세요.");
			});	
		}
	});

});



function PrintProc(){
	$('#receipt_btn').hide();
//	var AreaContents = document.getElementById('receipt-modal').innerHTML;
//	var AreaContents = $('.reservation-receipe-wrapper')[0].innerHTML;
	var AreaContents = $('#complete-container .reservation-receipe-wrapper')[0].innerHTML;
    var strFeature;
    strFeature = "width=900,height=750,toolbar=no,location=no,directories=no";
    strFeature += ",status=no,menubar=no,scrollbars=yes,resizable=no";
//    var cssBody = '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/external.css"/> \' >';
//	cssBody += '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/ptech.css"/> \' >';
//    cssBody += '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/application.css"/> \' >';
//    cssBody += '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/reservation.css"/> \' >';
//    cssBody += '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/user.css"/> \' >';

    var cssBody = ''
    	+ '<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">'
    	+ '<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">'
//    	+ '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/external.css"/> \' >'
//    	+ '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/ptech.css"/> \' >';
//    	+ '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/application.css"/> \' >';
//    	+ '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/reservation.css"/> \' >';
//    	+ '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/user.css"/> \' >';
    	+ '<link rel="stylesheet" type="text/css" href="' + context_path + '/resources/css/external.css">'
    	+ '<link rel="stylesheet" type="text/css" href="' + context_path + '/resources/css/ptech.css">'
    	+ '<link rel="stylesheet" type="text/css" href="' + context_path + '/resources/css/application.css">'
    	+ '<link rel="stylesheet" type="text/css" href="' + context_path + '/resources/css/reservation.css">'
    	+ '<link rel="stylesheet" type="text/css" href="' + context_path + '/resources/css/user.css">'
    ;

    
    var meta = '<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">';
    meta += '<meta charset="utf-8">';
    meta += '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
    meta += '<meta name="viewport" content="width=device-width, initial-scale=1.0">';
    meta += '<meta name="author" content="">';
    meta += '<meta name="description" content="">';
    
    objWin = window.open('', 'print', strFeature);
    objWin.focus();
    
    
    objWin.document.open();
    objWin.document.write('<html>');
    objWin.document.write(meta);
    objWin.document.write('<head>');
    objWin.document.write(cssBody);
    objWin.document.write('</head><body class="modal-open" style="padding-right:17px;padding-left:17px">');
    objWin.document.write('<div id="content-body"');
    objWin.document.write(AreaContents);
    objWin.document.write('</div>');
    objWin.document.write('</body></html>');
    objWin.document.close();

    var delay = setTimeout(function(){
    	print(objWin);
    },2000);
    
}

function print(Obj){
	$('#receipt_btn').show();
	//console.log(Obj);
	Obj.print();
	Obj.close();
}



function numberWithCommas(x){return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");}

function string2Date(strDate, type) {
	var year	= strDate.substr(0,4);
	var month	= strDate.substr(4,2);
	var day		= strDate.substr(6,2);
	var date	= new Date(year, month, day);  // date로 변경
	
	var dateVal = "";
	if(type == "1") {
		dateVal = year +"-"+ month +"-"+ day;
	} else if(type == "2") {
		dateVal = year +"년 "+ month +"월 "+ day +"일";
	}
	
	return dateVal;
}


// 날짜/회차 선택 시 HOLD 좌석 데이터 삭제 및 데이터 재 조회 :2016.07.06
function fnDateReserve(){
	
/*	
    console.log('place_cd:'+reserve_info.place_cd);
	console.log('program_cd:'+reserve_info.program_cd)
	console.log('play_date:'+reserve_info.play_date)
	console.log('play_seq_cd:'+reserve_info.play_seq_cd)
	console.log('play_start_time:'+reserve_info.play_start_time)
	console.log('unique_nbr:'+reserve_info.unique_nbr)
*/
	
	if(reserve_info.unique_nbr != undefined) {
		$.ajax({
            method: "POST",
            url: context_path + "/entrance/ReleaseHoldSeat",
            data: {
                reserve_info : JSON.stringify(reserve_info)     // 예약 정보
            }
        }).done(function(data) {
            if ((typeof data) == 'string') 
                data = JSON.parse(data);
            
            //console.log("Data : " + JSON.stringify(data));
            
            if(data.result == '000'){
	        	var playDate = $(".ptech-date.available.selected:eq(0)").attr("data-date"); 
	        	playDate =playDate.replace(/\//g, '');
	        	getSeqList(playDate);
            }else{
            	//console.log('보류좌석 리셋 처리 오류');
            }
        	
        }).fail(function(jqXHR, textStatus) {
        });
	}
	
	
}