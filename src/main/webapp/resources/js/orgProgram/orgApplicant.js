var OrgApplicant = function(orgProgramId){
	_.assign(this, {orgProgramId: orgProgramId, hasElementary: false, members: 0, userIds: []});
}

OrgApplicant.prototype.setTitle = function(title){
	_.assign(this, {title: title});
}

OrgApplicant.prototype.setProgramTime = function(date, timeStr){
	var programTime = moment(date.format("YYYY-MM-DD ") + timeStr, 'YYYY-MM-DD HH:mm'); 
	_.assign(this, {
		programTime: programTime, 
		programTimeStr: programTime.format('YYYY-MM-DD HH:mm'), 
		timeStr: timeStr,
		hasElementary: false,
		members: 0
	});
}

OrgApplicant.prototype.setHasElementary = function(hasElementary){
	_.assign(this, {hasElementary: hasElementary});
}

OrgApplicant.prototype.setApplicantType = function(applicantType){
	_.assign(this, {applicantType: applicantType});
}

OrgApplicant.prototype.setDescription = function(description){
	_.assign(this, {description: description});
}

OrgApplicant.prototype.setMembers = function(members){
	_.assign(this, {members: members});
}

OrgApplicant.prototype.addMembers = function(members){
	this.members += members;
}

OrgApplicant.prototype.addUserId = function(id){
	this.userIds.push(id);
}

OrgApplicant.prototype.save = function(url) {
	var applicant = this;
	
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	
	console.log(applicant);
	
	$.ajax({
		url: url,
		type: "POST",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		data: JSON.stringify({applicant: applicant}),
		beforeSend : function(xhr) {
			ajaxLoading();
			xhr.setRequestHeader(header, token);
		},
		success: function(data){
			if (data.orgApplicant.duplicate) {
				alert("이미 선택하신 시간대에 예약을 진행하셨습니다. 인원을 변경하실 경우 기존 예약을 취소하시고 진행하시기 바랍니다.");
			} else {
				alert("신청하신 예약이 정상적으로 처리 되었습니다.");
			}
			ajaxUnLoading();
			location.reload();
		},
		error: function(err){
			ajaxUnLoading();
			console.log(err);
		}
	});
}