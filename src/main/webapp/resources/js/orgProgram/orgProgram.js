Handlebars.registerHelper('formatedDate', function(date, format) {
	return moment(date).format(format);
});

var OrgProgramCalendar = function(options){
	this.applicant = new OrgApplicant(options.orgProgramId);
	this.applicants = [];
	this.existApplicant;
	this.options = options;
	
	this.times = [];
	
	var tempTimeStr = "";
	
	for(var i = 9; i < 18; i++){
		tempTimeStr = i + "";
		if(tempTimeStr.length == 1) {
			tempTimeStr = "0" + tempTimeStr;
		}
		
		this.times.push(tempTimeStr + ":00");
		this.times.push(tempTimeStr + ":30");
	}
	
	this.templateSource = $(this.options.template).html();
	this.template = Handlebars.compile(this.templateSource);
	
	this.timeTemplateSource = $("#org-program-times-template").html();
	this.timeTemplate = Handlebars.compile(this.timeTemplateSource);
	
	if(this.options.date != null){
		this.date = moment(this.options.date);
	}else {
		this.date = moment(new Date());
	}
	
	this.setDate(this.date);
	
	this.addEvent();
}

OrgProgramCalendar.prototype.renderCalendar = function(){
	var calendar = this;
	
	var calendarData = {
		selectedDate: calendar.date,
		prevMonth: calendar.prevMonth,
		nextMonth: calendar.nextMonth,
		dates: calendar.dates
	}
	
	var html = calendar.template(calendarData);
	$(calendar.options.container).html(html);
}

OrgProgramCalendar.prototype.setDate = function(date){
	this.date = date;
	this.setPrevMonth();
	this.setNextMonth();
	this.setMonthDays();
	
	this.renderCalendar();
}

OrgProgramCalendar.prototype.setPrevMonth = function(){
	this.prevMonth = moment(this.date).add(-1, 'Month')._d;
}

OrgProgramCalendar.prototype.setNextMonth = function(){
	this.nextMonth = moment(this.date).add(1, 'Month')._d;
}

OrgProgramCalendar.prototype.setMonthDays = function(){
	this.dates = [];

	var beginOfMonth = moment(this.date).startOf('month').startOf('week');
	var endOfMonth = moment(this.date).endOf('month').endOf('week');
	
	var today = moment(new Date());
	var availableBeginDate = moment(today).add(14, 'days');
	var availableEndDate = moment(today).add(60, 'days');
	
	var disable = false;

	for (var temp = beginOfMonth; temp.isBefore(endOfMonth); temp.add(1,
			'days')) {
		
		if(this.options.hasAdminRole == null){
			disable = today.isAfter(temp);
		}
		
		this.dates.push({
			date: temp.clone(),
			disable: disable
		});
	}

	this.dates = _.chunk(this.dates, 7);
}

OrgProgramCalendar.prototype.addEvent = function(){
	var calendar = this;
	
	$(document).on('click', '#prev-month', function(e){
		e.preventDefault();
		calendar.date = calendar.prevMonth;
		calendar.setDate(calendar.date);
	});
	
	$(document).on('click', '#next-month', function(e){
		e.preventDefault();
		calendar.date = calendar.nextMonth;
		calendar.setDate(calendar.date);
	});
	
	$(document).on('click', '.date', function(e){
		e.preventDefault();
		if(!$(this).hasClass('disabled')) {
			$(".calendar-body .date").removeClass("active");
			$(this).addClass("active").addClass(calendar.options.activeColor);
			
			var dateStr = $(this).data("date");
			calendar.date = moment(dateStr);
			
			var token = $("meta[name='_csrf']").attr("content");
			var header = $("meta[name='_csrf_header']").attr("content");
			
			$.ajax({
				url: calendar.options.url,
				type: "POST",
				contentType : "application/json; charset=utf-8",
				dataType : "json",
				data: JSON.stringify({dateStr: dateStr}),
				beforeSend : function(xhr) {
					ajaxLoading();
					xhr.setRequestHeader(header, token);
				},
				success: function(data){
					calendar.applicants = [];
					
					var times = [];
					var testBool = false;
					_.forEach(calendar.times, function(d, i){
						var applicant = new OrgApplicant(calendar.options.orgProgramId);
						applicant.setProgramTime(calendar.date, d);
						calendar.applicants.push(applicant);
					});
					
					_.forEach(data.applicants, function(d, i){
						console.log(d);
						d.programTimeStr = moment(d.applicationTime).format("YYYY-MM-DD HH:mm");
						
						var idx = _.findIndex(calendar.applicants, function(o){ return o.programTimeStr == d.programTimeStr;});
						
						if(idx >= 0){
							var applicant = calendar.applicants[idx];
							console.log(applicant);
							applicant.addMembers(d.members);
							if(d.applicationType == "elementary"){
								calendar.applicants[idx].setHasElementary(true);
							}
						}
					});
					
					var html = calendar.timeTemplate({applicants: calendar.applicants});
					$("#org-program-times").html(html);
					ajaxUnLoading();
				},
				error: function(err){
					console.log(err);
				}
			});
		}
	});
	
	$(document).on('click', '.reserve-btn', function(e){
		var timeStr = $(this).data("time");
		var applicantIdx = _.findIndex(calendar.applicants, function(o){ return o.timeStr == timeStr;});
		calendar.existApplicant = calendar.applicants[applicantIdx];
		
		if(calendar.existApplicant.hasElementary) {
			$("#applicant_type_elementary").attr("disabled", true);
		}else {
			$("#applicant_type_elementary").attr("disabled", false);
		}
		
		calendar.applicant.setProgramTime(calendar.date, timeStr);
		$("#org-program-reserve-modal").modal();
	});
	
	$(document).on('blur', '#applicant_member', function(e){
		var memberCnt = parseInt($(this).val());
		var applicantType = $("input[name='applicantType']:checked").val();
		var remainMembers = 120 - calendar.existApplicant.members;
		
		if(_.isUndefined(applicantType)){
			alert("신청유형을 선택해 주세요");
			$("#applicant_member").val('');
			$("#applicant_type_kinder").focus();
		}else if(memberCnt > remainMembers) {
			alert("선택하신 프로그램의 최대 예약 가능 인원수는 " + remainMembers + "명 입니다.");
			$("#applicant_member").val(remainMembers);
			$("#applicant_member").focus();
		}else if (applicantType == "elementary" && memberCnt > 30){
			alert("초등부의 경우 최대 30명까지만 신청 가능합니다.");
			$("#applicant_member").val(30);
			$("#applicant_member").focus();
		}else if (applicantType == "kinder" && memberCnt > 50) {
			alert("유치부의 경우 최대 50명까지만 신청 가능합니다.");
			$("#applicant_member").val(50);
			$("#applicant_member").focus();
		}
	});
	
	$(document).on('click', '#submit-applicant', function(e){
		calendar.applicant.setTitle($("#applicant_name").val());
		calendar.applicant.setApplicantType($("input[name='applicantType']:checked").val());
		calendar.applicant.setMembers($("#applicant_member").val());
		calendar.applicant.setDescription($("#applicant_description").val());
		
		if(calendar.applicant.members <= 0){
			alert("신청인원은 1명 이상이어야 합니다.");
			return false;
		}
		
		if(_.isUndefined(calendar.applicant.applicantType) || calendar.applicant.applicantType == null){
			alert("신청유형을 선택하셔야 합니다.");
			return false;
		}
		
		calendar.applicant.save(calendar.options.submitUrl);
		
	});
}

OrgProgramCalendar.prototype.getDisableDates = function(){
	var calendar = this;
	
	$.ajax({
		url: calendar.options.url,
		type: "GET",
		data: {
			startDateStr: moment(calendar.date).startOf('month').startOf('week').format("YYYY-MM-DD"),
			endDateStr: moment(calendar.date).endOf('month').endOf('week').format("YYYY-MM-DD")
		},
		success: function(data){
			if(data.unavailables.length > 0){
				calendar.setUnavailable(data.unavailables);
			}
		},
		error: function(err){
			console.log(err);
		}
	});
}

OrgProgramCalendar.prototype.setUnavailable = function(unavailables){
	_.forEach(unavailables, function(d, i){
		var uDate = moment(new Date(d.stDate)).format("YYYY-MM-DD");
		$(".date[data-date='" + uDate +"']").addClass("unavailable");
	});
}

OrgProgramCalendar.prototype.saveUnavailableDates = function(){
	var calendar = this;
	
	if(confirm("입력하신 날짜와 시간에 대해서 신청 불가능 일자로 저장하시겠습니까?")){
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");
		
		var times = {
			beginTime: $("#unavailable-date").val() + " " + $("#begin-time").val(),
			endTime: $("#unavailable-date").val() + " " + $("#end-time").val()
		}
		
		$.ajax({
			url: calendar.options.url,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			type : "POST",
			beforeSend : function(xhr) {
				ajaxLoading();
				xhr.setRequestHeader(header, token);
			},
			data: JSON.stringify(times),
			success: function(data){
				if(data.unavailable != null){
					console.log(data.unavailable);
					var unavaialbleDate = moment(data.unavailable.beginTime).format("YYYY-MM-DD");
					var beginTime = moment(data.unavailable.beginTime).format("HH:mm");
					var endTime = moment(data.unavailable.endTime).format("HH:mm");
					
					$(".date[data-date='" + unavaialbleDate + "']").addClass("unavailable");
					$(".date[data-date='" + unavaialbleDate + "'] .unavailable-times")
						.append("<div><span class='purple'>" + beginTime + " ~ " + endTime + "</span></div>");
				}
				
				ajaxUnLoading();
			},
			error: function(err){
				ajaxUnLoading();
				console.log(err);
			}
		});
	}
}