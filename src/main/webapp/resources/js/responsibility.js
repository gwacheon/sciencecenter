//담당부서 리스트 관리
var Responsibilities = function(){
	this.responsibilities = [];
}

Responsibilities.prototype.add = function(responsibility){
	this.responsibilities.push(responsibility);
}

Responsibilities.prototype.get = function(idx){
	return this.responsibilities[idx];
}

Responsibilities.prototype.getByTabName = function(tabName){
	var selectedIdx = -1;
	_.forEach(this.responsibilities, function(responsibility, idx){
		if(responsibility.tabName == tabName){
			selectedIdx = idx;
		}
	});
	
	if(selectedIdx >= 0){
		return this.responsibilities[selectedIdx];
	}else{
		return null;
	}
}

Responsibilities.prototype.isEmpty = function(){
	return _.isEmpty(this.responsibilities);
}

//담당부서 관리
var Responsibility = function(requestURI, dept, member, phone, tabName){
	this.requestURI = requestURI;
	this.dept = dept;
	this.member = member;
	this.phone = phone;
	this.tabName = tabName;
}

Responsibility.prototype.render = function(){
	var responsibility = this;
	
	if(this.template == null){
		this.template = $("#responsibilityTemplate").html();
		this.compile = Handlebars.compile(this.template);
	}
	
	this.renderHtml = this.compile(responsibility);
	$("#responsibility-wrapper").html(this.renderHtml)
}

Responsibility.prototype.fillForm = function(){
	$('#requestURI').val(this.requestURI);
	$('#tabName').val(this.tabName);
	$('#dept').val(this.dept);
	$('#member').val(this.member);
	$('#phone').val(this.phone);
}

Responsibility.prototype.submitData = function(){
	var responsibility = this;	
	
	$.ajax({
		url : baseUrl + "admin/responsibilities",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		type : "POST",
		beforeSend: function(xhr){
			ajaxLoading();
			xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));			
		},
		data: JSON.stringify(responsibility),
		success : function(data){
			ajaxUnLoading();
			alert("정상 수정되었습니다.");
			responsibility.render();
		},
		error: function(err){
    		console.log(err);
    	}
	});
}