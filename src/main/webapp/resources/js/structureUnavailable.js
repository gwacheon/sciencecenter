Handlebars.registerHelper('formatedDate', function(date, format) {
	return moment(date).format(format);
});

var StructureCalendar = function(options){
	this.options = options;
	
	this.templateSource = $(this.options.template).html();
	this.template = Handlebars.compile(this.templateSource);
	
	if(this.options.date != null){
		this.date = moment(this.options.date);
	}else {
		this.date = moment(new Date());
	}
	
	this.setDate(this.date);
	
	this.addEvent();
}

StructureCalendar.prototype.renderCalendar = function(){
	var calendar = this;
	
	$.ajax({
		url: calendar.options.url,
		type: "GET",
		data: {
			startDateStr: moment(calendar.date).startOf('month').startOf('week').format("YYYY-MM-DD"),
			endDateStr: moment(calendar.date).endOf('month').endOf('week').format("YYYY-MM-DD")
		},
		success: function(data){
			var calendarData = {
				selectedDate: calendar.date,
				prevMonth: calendar.prevMonth,
				nextMonth: calendar.nextMonth,
				dates: calendar.dates
			}
			
			var html = calendar.template(calendarData);
			$(calendar.options.container).html(html);
			
			if(data.unavailables.length > 0){
				_.forEach(data.unavailables, function(d, i){
					var unavaialbleDate = moment(d.beginTime).format("YYYY-MM-DD");
					var beginTime = moment(d.beginTime).format("HH:mm");
					var endTime = moment(d.endTime).format("HH:mm");
					
					$(".date[data-date='" + unavaialbleDate + "']").addClass("unavailable");
					$(".date[data-date='" + unavaialbleDate + "'] .unavailable-times")
						.append("<div><span class='label'>" + beginTime + " ~ " + endTime + "</span></div>");
				});
			}
		},
		error: function(err){
			console.log(err);
		}
	});
}

StructureCalendar.prototype.setDate = function(date){
	this.date = date;
	this.setPrevMonth();
	this.setNextMonth();
	this.setMonthDays();
	
	this.renderCalendar();
}

StructureCalendar.prototype.setPrevMonth = function(){
	this.prevMonth = moment(this.date).add(-1, 'Month')._d;
}

StructureCalendar.prototype.setNextMonth = function(){
	this.nextMonth = moment(this.date).add(1, 'Month')._d;
}

StructureCalendar.prototype.setMonthDays = function(){
	this.dates = [];

	var beginOfMonth = moment(this.date).startOf('month').startOf('week');
	var endOfMonth = moment(this.date).endOf('month').endOf('week');
	
	var today = moment(new Date());
	var availableBeginDate = moment(today).add(14, 'days');
	var availableEndDate = moment(today).add(60, 'days');
	
	var disable = false;

	for (var temp = beginOfMonth; temp.isBefore(endOfMonth); temp.add(1,
			'days')) {
		
		if(this.options.hasAdminRole == null){
			disable = !(temp.diff(availableBeginDate) > 0 && temp.diff(availableEndDate) <= 0);
		}
		
		this.dates.push({
			date: temp.clone(),
			disable: disable
		});
	}

	this.dates = _.chunk(this.dates, 7);
}

StructureCalendar.prototype.addEvent = function(){
	var calendar = this;
	
	if(calendar.options.hasAdminRole){
		$(document).on('click', '.date', function(e){
			$(".date").removeClass("active");
			$(this).addClass("active");
			$("#unavailable-date").val($(this).data("date"));
		});
		
		$(document).on('click', '#unavailable-confirm-btn', function(e){
			calendar.saveUnavailableDates();
		});
	}else{
		
	}
	
	$(document).on('click', '#prev-month', function(e){
		e.preventDefault();
		calendar.date = calendar.prevMonth;
		calendar.setDate(calendar.date);
	});
	
	$(document).on('click', '#next-month', function(e){
		e.preventDefault();
		calendar.date = calendar.nextMonth;
		calendar.setDate(calendar.date);
	});
}

StructureCalendar.prototype.getDisableDates = function(){
	var calendar = this;
	
	$.ajax({
		url: calendar.options.url,
		type: "GET",
		data: {
			startDateStr: moment(calendar.date).startOf('month').startOf('week').format("YYYY-MM-DD"),
			endDateStr: moment(calendar.date).endOf('month').endOf('week').format("YYYY-MM-DD")
		},
		success: function(data){
			if(data.unavailables.length > 0){
				calendar.setUnavailable(data.unavailables);
			}
		},
		error: function(err){
			console.log(err);
		}
	});
}

StructureCalendar.prototype.setUnavailable = function(unavailables){
	_.forEach(unavailables, function(d, i){
		var uDate = moment(new Date(d.stDate)).format("YYYY-MM-DD");
		$(".date[data-date='" + uDate +"']").addClass("unavailable");
	});
}

StructureCalendar.prototype.saveUnavailableDates = function(){
	var calendar = this;
	
	if(confirm("입력하신 날짜와 시간에 대해서 신청 불가능 일자로 저장하시겠습니까?")){
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");
		
		var times = {
			beginTime: $("#unavailable-date").val() + " " + $("#begin-time").val(),
			endTime: $("#unavailable-date").val() + " " + $("#end-time").val()
		}
		
		$.ajax({
			url: calendar.options.url,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			type : "POST",
			beforeSend : function(xhr) {
				ajaxLoading();
				xhr.setRequestHeader(header, token);
			},
			data: JSON.stringify(times),
			success: function(data){
				if(data.unavailable != null){
					console.log(data.unavailable);
					var unavaialbleDate = moment(data.unavailable.beginTime).format("YYYY-MM-DD");
					var beginTime = moment(data.unavailable.beginTime).format("HH:mm");
					var endTime = moment(data.unavailable.endTime).format("HH:mm");
					
					$(".date[data-date='" + unavaialbleDate + "']").addClass("unavailable");
					$(".date[data-date='" + unavaialbleDate + "'] .unavailable-times")
						.append("<div><span class='label'>" + beginTime + " ~ " + endTime + "</span></div>");
				}
				
				ajaxUnLoading();
			},
			error: function(err){
				ajaxUnLoading();
				console.log(err);
			}
		});
	}
}