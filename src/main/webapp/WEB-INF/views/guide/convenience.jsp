<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="sub-content-nav scrollspy-green">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>

<div class="sub-body">
	<div class="narrow-sub-top guide no-nav-tabs">
		<div id="basic-info-spy" class="container">
			<!-- <h4 class="doc-title">편의시설</h4>
			<h6 class="doc-title exp-desc">
				국립과천과학관은 이용하시는 관람객 여러분의 편의를 최우선으로 생각합니다.<br>
				가장 필요한 서비스를 제공하기위해 끊임없이 노력하겠습니다.
			</h6> -->
		</div>
	</div><!-- support-top END... --> 
	<div id="support-sub-nav">
		<div class="container">
		   	<div class="sub-nav convenience">
				<div class="row">
					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
			     		<a href="#"  class="item" data-name="playroom">유아놀이방</a> 
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
				      	<a href="#" class="item" data-name="emergency">구급실&middot;수유실</a>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
					    <a href="#" class="item" data-name="souvenir">기념품점</a>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
					    <a href="#" class="item" data-name="rest">휴게시설</a>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
					    <a href="#" class="item" data-name="locker">물품보관소</a>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
					    <a href="#" class="item" data-name="parkinglot">주차장</a>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
					    <a href="#" class="item" data-name="stroller">유모차 대여소</a>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
					    <a href="#" class="item" data-name="cafe">과학카페(도시락장소)</a>
					</div>
			     </div>
				  
			</div>
		</div>
	</div> 
	<div id="support-body" class="container">
		<div id="playroom" class="tab-item">	
			<div class="sub-header">
				<div class="row">  
					<div class="col-md-12">
						<h3 class="page-title green">
							<div class="top-line"></div>     
							유아놀이방
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<ul class="circle-list green">
							<li>
								이용 안내
								<ul>
									<li>
										영유아를 위한 수유, 미아보호 등을 위한공간(면적 39m<sup>2</sup>)으로 
										가족단위 관람객이 안심하고 편리하게 관람 하실 수 있습니다.
									</li>
									<li>
										아이들과 함께 오신 가족단위 관람객이 잠시 휴식하실 수 있는 공간입니다.
									</li>  
								</ul>
							</li>
						</ul>
							
					</div>
					<div id="playroom-svg" class="col-md-8 text-right convenience">
						<c:import url="/WEB-INF/views/display/mainBuilding/svgs/display_svg_map_modified.jsp"></c:import>
					</div>
				</div>
			</div>
			<div class="convenience-detail">
				<div class="row">
					<div class="col-xs-6 col-sm-6 col-md-6 detail-img-box">   
						<img src="<c:url value='/resources/img/support/convenience/childcare01.png'/>" class="img-responsive"/>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 detail-img-box">
						<img src="<c:url value='/resources/img/support/convenience/childcare02.png'/>" class="img-responsive"/>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 detail-img-box">
						<img src="<c:url value='/resources/img/support/convenience/childcare03.png'/>" class="img-responsive"/>
						
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 detail-img-box">
						<img src="<c:url value='/resources/img/support/convenience/childcare04.png'/>" class="img-responsive"/>
						
					</div>
				</div>
				
			</div>
		</div> <!--  #playroom END ... -->
		
		<div id="emergency" class="tab-item">	
			<div class="sub-header">
				<div class="row">  
					<div class="col-md-12">
						<h3 class="page-title green">
							<div class="top-line"></div>     
							구급실&middot;수유실
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<ul class="circle-list green">
							<li>
								이용 안내
								<ul>
									<li>
										간단한 의약품들이 비치되어 있어 응급시 처치를 도와드립니다.
									</li>  
									<li>
										1층 어린이 탐구체험관 옆에 위치하고 있으며, 간단한 의약품들이 비치되어 있어 응급시 처치를 도와드립니다.
									</li>
									<li>
										안으로 들어가시면 기저귀 교환대 2개가 보입니다. 기저귀 교환대와 휴지통이 있어서 아기의 기저귀를 위생적이고 편리하게 교환 하실 수 있습니다.
									</li>
									<li>
										다른 관람객에게 신경 쓰지 않고 아기에게 수유할 수 있도록 커튼으로 꾸며져 있습니다.
									</li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="col-md-8 text-right convenience">
						<c:import url="/WEB-INF/views/display/mainBuilding/svgs/display_svg_map_modified.jsp"></c:import>
					</div>
				</div>
			</div>
			<div class="convenience-detail">
				<div class="row">
					<div class="col-xs-6 col-sm-6 col-md-6 detail-img-box">
						<img src="<c:url value='/resources/img/support/convenience/firstaid01.png'/>" class="img-responsive"/>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 detail-img-box">
						<img src="<c:url value='/resources/img/support/convenience/firstaid02.png'/>" class="img-responsive"/>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 detail-img-box">
						<img src="<c:url value='/resources/img/support/convenience/firstaid03.png'/>" class="img-responsive"/>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 detail-img-box">
						<img src="<c:url value='/resources/img/support/convenience/firstaid04.png'/>" class="img-responsive"/>
					</div>
					<div class="col-xs-6 col-md-6 detail-img-box">
						<img src="<c:url value='/resources/img/support/convenience/firstaid05.png'/>" class="img-responsive"/>
					</div>
				</div>
			</div>
		</div> <!--  #emergency END ... -->
		
		<div id="souvenir" class="tab-item">	
			<div class="sub-header">
				<div class="row">  
					<div class="col-md-12">
						<h3 class="page-title green">
							<div class="top-line"></div>     
							기념품점
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<ul class="circle-list green">
							<li>
								이용 안내
								<ul>
									<li>
										과학관 관람을 기념할 수 있는 기념품 등을 구입하실 수 있습니다.
									</li>
									<li>
										과천과학관 중앙 정문으로 들어가면 기념품점이 우측에 위치하여 있으며, 
										각종 문구세트, 인형, 도서 등이 비치되어 있어 기념품을 구매할 수 있습니다.
									</li>  								
								</ul>
							</li>
						</ul>
					</div>
					<div class="col-md-8 text-right convenience">
						<c:import url="/WEB-INF/views/display/mainBuilding/svgs/display_svg_map_modified.jsp"></c:import>
					</div>
				</div>
			</div>
			<div class="convenience-detail">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6 detail-img-box">
						<img src="<c:url value='/resources/img/support/convenience/gift01.png'/>" class="img-responsive"/>
					</div>
				</div>
				
			</div>
		</div> <!--  #souvenir END ... -->
		
		<div id="rest" class="tab-item">	
			<div class="sub-header">
				<div class="row">  
					<div class="col-md-12">
						<h3 class="page-title green">
							<div class="top-line"></div>     
							휴게시설
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<ul class="circle-list green">
							<li>
								이용 안내
								<ul>
									<li>
										과학관 관람 중 편안하게 쉴 수 있는 휴식공간을 제공합니다. 1층과 2층에 마련되어 있습니다.
									</li>  
								</ul>
							</li>
						</ul>
					</div>
					<div class="col-md-8 text-right convenience">
						<c:import url="/WEB-INF/views/display/mainBuilding/svgs/display_svg_map_modified.jsp"></c:import>
					</div>
				</div>
			</div>
			<div class="convenience-detail">
				<div class="row">
					<div class="col-xs-6 col-sm-6 col-md-6 detail-img-box">
						<img src="<c:url value='/resources/img/support/convenience/lounge01.png'/>" class="img-responsive"/>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 detail-img-box">
						<img src="<c:url value='/resources/img/support/convenience/lounge02.png'/>" class="img-responsive"/>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 detail-img-box">
						<img src="<c:url value='/resources/img/support/convenience/lounge03.png'/>" class="img-responsive"/>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 detail-img-box">
						<img src="<c:url value='/resources/img/support/convenience/lounge04.png'/>" class="img-responsive"/>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 detail-img-box">
						<img src="<c:url value='/resources/img/support/convenience/lounge05.png'/>" class="img-responsive"/>
					</div>
				</div>
				
			</div>
		</div> <!--  #rest END ... -->
		
		
		<div id="locker" class="tab-item">	
			<div class="sub-header">
				<div class="row">  
					<div class="col-md-12">
						<h3 class="page-title green">
							<div class="top-line"></div>     
							물품 보관소
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<ul class="circle-list green">
							<li>
								이용 안내
								<ul>
									<li>
										물품보관을 위한 라커가 설치되어 있습니다. 
									</li>
									<li>
										전시장 입장하면 1층 좌측과 우측에 물품 보관을 위한 라커가 설치되어 있습니다. 
									</li>
									<li>
										사용 가능한 라커는 총 250여대입니다.
									</li>
									<li>
										이용 시간 - 9:30 ~ 17:30
									</li>  
								</ul>
							</li>
						</ul>
					</div>
					<div class="col-md-8 text-right convenience">
						<c:import url="/WEB-INF/views/display/mainBuilding/svgs/display_svg_map_modified.jsp"></c:import>
					</div>
				</div>
			</div>
			<div class="convenience-detail">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6 detail-img-box">
						<img src="<c:url value='/resources/img/support/convenience/stockroom01.png'/>" class="img-responsive"/>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 detail-img-box">
						<img src="<c:url value='/resources/img/support/convenience/stockroom02.png'/>" class="img-responsive"/>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 detail-img-box">
						<img src="<c:url value='/resources/img/support/convenience/stockroom03.png'/>" class="img-responsive"/>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 detail-img-box">
						<img src="<c:url value='/resources/img/support/convenience/stockroom04.png'/>" class="img-responsive"/>
					</div>
				</div>
				
			</div>
		</div> <!--  #locker END ... -->
		
		<div id="parkinglot" class="tab-item">	
			<div class="sub-header">
				<div class="row">  
					<div class="col-md-12">
						<h3 class="page-title green">
							<div class="top-line"></div>     
							주차장
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<ul class="circle-list green">
							<li>
								이용 안내
								<ul>
									<li>
										총 1,116대 주차 (대형 38대, 장애인주차구역 44대 포함)가 가능합니다.
									</li>  
									<li>
										주말에는 교통이 혼잡하므로 대중교통(지하철 4호선)을 이용하시면 더욱 편리한 관람이 되실 것입니다.
									</li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="col-md-8 text-right convenience">
						<c:import url="/WEB-INF/views/display/mainBuilding/svgs/display_svg_map_modified.jsp"></c:import>
					</div>
				</div>
			</div>
			<div class="convenience-detail">
				<div class="row">
					<div class="col-xs-6 col-sm-6 col-md-6 detail-img-box">
						<img src="<c:url value='/resources/img/support/convenience/parking01.png'/>" class="img-responsive"/>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 detail-img-box">
						<img src="<c:url value='/resources/img/support/convenience/parking02.png'/>" class="img-responsive"/>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 detail-img-box">
						<img src="<c:url value='/resources/img/support/convenience/parking03.png'/>" class="img-responsive"/>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 detail-img-box">
						<img src="<c:url value='/resources/img/support/convenience/parking04.png'/>" class="img-responsive"/>
					</div>
				</div>
				
			</div>
		</div> <!--  #parkinglot END ... -->
		
		<div id="stroller" class="tab-item">	
			<div class="sub-header">
				<div class="row">  
					<div class="col-md-12">
						<h3 class="page-title green">
							<div class="top-line"></div>     
							유모차대여소
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<ul class="circle-list green">
							<li>
								이용 안내
								<ul>
									<li>
										아기(36개월 이하)와 함께 오시는 분들을 위해 유모차를 대여하실 수 있도록 유모차 대여소가 준비되어 있습니다.
									</li>  
									<li>
										대여 가능한 총 유모차는 50대입니다.
									</li>
									<li>
										대여료는 무료이며, 신분증을 맡기신 뒤 반납할 때 찾아가시면 됩니다.
									</li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="col-md-8 text-right convenience">
						<c:import url="/WEB-INF/views/display/mainBuilding/svgs/display_svg_map_modified.jsp"></c:import>
					</div>
				</div>
			</div>
			<div class="convenience-detail">
				<div class="row">
					<div class="col-xs-6 col-sm-6 col-md-6 detail-img-box">
						<img src="<c:url value='/resources/img/support/convenience/stroller01.png'/>" class="img-responsive"/>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 detail-img-box">
						<img src="<c:url value='/resources/img/support/convenience/stroller02.png'/>" class="img-responsive"/>
					</div>
				</div>
				
			</div>
		</div> <!--  #stroller END ... -->
		
		<div id="cafe" class="tab-item">	
			<div class="sub-header">
				<div class="row">  
					<div class="col-md-12">
						<h3 class="page-title green">
							<div class="top-line"></div>     
							과학카페
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<ul class="circle-list green">
							<li>
								이용 안내
								<ul>
									<li>
										개인 및 단체등 관람객이 가지고 온 점심식사를 할수 있는 공간과 휴식을 위해 마련된 공간이며, 
										과학프로그램이 진행 될 수도 있습니다.
									</li>  
								</ul>
							</li>
						</ul>
					</div>
					<div class="col-md-8 text-right convenience">
						<c:import url="/WEB-INF/views/display/mainBuilding/svgs/display_svg_map_modified.jsp"></c:import>
					</div>
				</div>
			</div>
			<div class="convenience-detail">
				<div class="row">
					<div class="col-xs-6 col-sm-6 col-md-6 detail-img-box">
						<img src="<c:url value='/resources/img/support/convenience/sciencecafe01.png'/>" class="img-responsive"/>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 detail-img-box">
						<img src="<c:url value='/resources/img/support/convenience/sciencecafe02.png'/>" class="img-responsive"/>
					</div>
				</div>
				
			</div>
		</div> <!--  #cafe END ... -->
		
	</div><!-- support body END... -->
</div>


<script type="text/javascript">
	
	$(function() {
		$("a[data-name='${type}']").click();
	});
	
	$('#support-sub-nav .sub-nav .item').on('click',function(e) {
		e.preventDefault();
		var id = $(this).data('name');
		var id_selector = "#" + id;
		$('#support-sub-nav .sub-nav .item').removeClass('active');
		$(this).addClass('active');
		hideSVGMap(".tab-item.active"); // find the current active tab and hide its svg map
		$('#support-body .tab-item').removeClass('active');
		$('#'+id+'.tab-item').addClass('active');
		
		switch(id_selector) {
		
			case "#sciencePlayroom":
			case "#parkinglot":
			case "#others":
				setDefaultSVGMap(id_selector, 3);
				break;
			case "#cafe":
				setDefaultSVGMap(id_selector, 2);
				break;
			default:
				setDefaultSVGMap(id_selector, 1);
				break;
				
		}
		
	});
	
	
	// 각 편의시설별 지도 flash 해당 위치 표시
	$('#playroom').find("g[id='XMLID_14_']").attr('class', 'active'); // 유아놀이방
	$('#emergency').find("g[id='XMLID_13_']").attr('class', 'active'); // 구급실 수유실
	$('#souvenir').find("g[id='XMLID_224_']").attr('class', 'active'); // 기념품점
	$('#rest').find("g[id='XMLID_224_']").attr('class', 'active'); // 휴게시설 + 기념품점
	
	$('#rest').find("g[id='XMLID_222_']").attr('class', 'active'); // 휴게시설
	
	$('#locker').find("g[id='XMLID_222_']").attr('class', 'active'); // 물품보관소
	$('#parkinglot').find("g[id='XMLID_57_']").attr('class', 'active'); // 서 주차장
	$('#parkinglot').find("g[id='XMLID_139_']").attr('class', 'active'); // 대형 주차장
	$('#parkinglot').find("g[id='XMLID_114_']").attr('class', 'active'); // 중앙 주차장, 동 주차장
	$('#stroller').find("g[id='XMLID_225_']").attr('class', 'active'); // 유모차 대여소
	$('#cafe').find("g[id='XMLID_157_']").attr('class', 'active'); // 과학카페
</script>