<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="sub-content-nav scrollspy-green">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>

<div class="sub-body">
	<div id="support-body">
		<div id="basic-guide-spy" class="narrow-sub-top guide">
					
		</div>
		<div class="container">
			<div id="paid-member-spy" class="section">
				<div class="row">
					<div class="col-md-3">
						<h3 class="page-title green">
							<div class="top-line"></div>     
							연간회원
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						국립과천과학관에서는 청소년ㆍ학생 등이 과학관을 보다 많이 이용하고 다양한 교육 및 행사프로그램에 참여할 기회를 부여하고자 국립과천과학관 회원제를 운영하고 있습니다. 국립과천과학관 회원에 가입하시면 상설전시관 입장료 면제, 교육비 할인 등 다양한 혜택을 제공받으실 수 있습니다. 과학관 연간회원에 가입하시어 다양한 과학 교육 ·문화 혜택을 누려보세요!
					</div>
				</div>
				<div class="sub-section">
					<ul class="circle-list green sub-section-title">
						<li>
							회원 종류 및 연회비
						</li>
					</ul>
					<div class="row">
						<div class="col-md-12">
							<div class="content">
								<table class="table table-bordered centered-table">
									<thead>
										<tr>
											<th width="30%" class="border-right">회원종류</th>
											<th width="30%" class="border-right">연회비</th>
											<th width="30%">회원 기간</th>
									</thead>
									<tbody>
										<tr>
											<td class="border-right">연간회원</td>
											<td class="border-right">50,000원</td>
											<td>1년</td>
										</tr>
									</tbody>
								</table>
							</div>
						
						</div>
					</div>
				</div>
				<div class="sub-section">
					<ul class="circle-list green sub-section-title">
						<li>
							회원제 가입 방법 및 절차
						</li>
					</ul>				
					<div class="content">
						<div class="row">
							<div class="col-md-12 content-title-wrapper">
								<ul class="arrow-list text-color green">
									<li>
										온라인 가입
									</li>
								</ul>			
							</div>
						</div>
						<div class="detail">
							<div class="row">
								<div class="col-md-12">
									<div class="info">
										국립과천과학관 홈페이지 회원가입 메뉴에서 연간회원으로 가입하신 후 연회비를 결제하시면 회원등록이 됩니다(온라인회원 가입-로그인 후 '마이페이지'로 가셔서 '회원정보'-'회원정보 변경'에서 '회원제 전환'을 선택 후 결제를 진행하시면 됩니다.)
										<div class="color-red">
											*사진 파일(사진 이미지 규격 : 반명함판 크기, 해상도 200~300dpi) 첨부
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="process-header">
										가입절차
									</div>								
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12 col-md-6 col-lg-3">								
									<div class="process">
										<div class="text">
											과학관 홈페이지 회원가입 메뉴에서 연간회원으로 가입
										</div>
										<i class="fa fa-angle-right" aria-hidden="true"></i>
									</div>
								</div>
								<div class="col-sm-12 col-md-6 col-lg-3">								
									<div class="process ex-color single-line">
										<div class="text">
											연회비 결제
										</div>
										<i class="fa fa-angle-right" aria-hidden="true"></i>
									</div>
								</div>
								<div class="col-sm-12 col-md-6 col-lg-3">								
									<div class="process">
										<div class="text">
											과학관 최초 방문시 본인 확인 후 회원카드 발급
										</div>
										<i class="fa fa-angle-right" aria-hidden="true"></i>
									</div>
								</div>
								<div class="col-sm-12 col-md-6 col-lg-3">								
									<div class="process ex-color single-line">
										<div class="text">
											과학관 입장
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="content">
						<div class="row">
							<div class="col-md-12 content-title-wrapper">
								<ul class="arrow-list text-color green">
									<li>
										방문가입
									</li>
								</ul>							
							</div>
						</div>
						<div class="detail">
							<div class="row">
								<div class="col-md-12">
									<div class="info">
										국립과천과학관 중앙홀 안내데스크(본관 1층)에서 가입신청서를 작성하신 후 연회비를 결제하시면 회원등록이 됩니다.
										<div class="color-red">
											* 사진 파일(사진 이미지 규격 : 반명함판 크기, 해상도 200~300dpi) 지참 / 사진파일이 없는 경우 과학관에서 캠카메라로 촬영해 드립니다.
										</div>
										<div class="color-red">
											* 신청·접수 시간 : 9:30 ~ 16:30 (매주 월요일 휴관, 점심시간 12:30~13:30 제외) 
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="process-header">
										가입절차
									</div>								
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12 col-md-6 col-lg-3">								
									<div class="process  single-line">
										<div class="text">
											가입 신청서 작성
										</div>
										<i class="fa fa-angle-right" aria-hidden="true"></i>
									</div>
								</div>
								<div class="col-sm-12 col-md-6 col-lg-3">								
									<div class="process ex-color single-line">
										<div class="text">
											연회비 결제
										</div>
										<i class="fa fa-angle-right" aria-hidden="true"></i>
									</div>
								</div>
								<div class="col-sm-12 col-md-6 col-lg-3">								
									<div class="process  single-line">
										<div class="text">
											회원카드 발급
										</div>
										<i class="fa fa-angle-right" aria-hidden="true"></i>
									</div>
								</div>
								<div class="col-sm-12 col-md-6 col-lg-3">								
									<div class="process ex-color single-line">
										<div class="text">
											과학관 입장
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="content">
						<div class="row">
							<div class="col-md-12 content-title-wrapper">
								<ul class="arrow-list text-color green">
									<li>
										환불 정산
									</li>
								</ul>							
							</div>
						</div>
						<div class="detail">
							<div class="row">
								<div class="col-md-12">
								
									※연간회원 환불신청 시에는 '마이페이지'로 가셔서 '회원정보 변경'에서 '회원제 전환'을 선택해 주시길 바랍니다.※
									<ul class="dash-list non-padding">
										<li>
											회비의 환불은 과학관이 인정하거나, 별도로 정한기간(회비납부 7일이내)에만 가능하며, 회비납부 7일 이내라도 회원혜택을 받은 이후에는 환불되지 않습니다.
										</li>
									</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="sub-section">
						<ul class="circle-list green sub-section-title">
							<li>
								회원 혜택
							</li>
						</ul>
						<div class="content">
							<div class="row">
								<div class="col-md-12 content-title-wrapper">
									<ul class="arrow-list green">
										<li>
											과학관 상설전시관 입장료 면제	
										</li>
										<li>
											유 · 무상의 교육 및 과학행사프로그램 참여 우선권(50%) 부여
										</li>
										<li>
											외부 위탁기관이 주관하는 유료 특별기획전 관람료는 주관사의 단체할인율을 적용 할인	
										</li>
										<li>
											회원기간 만료 후 1개월 이내 재가입시 연회비 10% 할인	
										</li>
										<li>
											천체 투영관 관람시 50% 할인
										</li>
										<li>
											천체 관측소 관람시 20% 할인
										</li>
										<li>
											식사할인쿠폰 지급
										</li>
										<li>
											스페이스월드 할인(체험:2,000원→1,500원/교육:7,000원→5,000원
										</li>
									</ul>			
								</div>
							</div>
						</div>
					</div>
					<div class="sub-section">
						<ul class="circle-list green sub-section-title">
							<li>
								회원 혜택 이용 요령
							</li>
						</ul>
						<div class="content">
							<div class="row">
								<div class="col-md-12 content-title-wrapper">
									<ul class="arrow-list green">
										<li>
											상설전시관 입장 : <span class="med-gray">안내데스크에서 회원증 제시 ·확인 후 입장</span>
										</li>
										<li>
											교육 우선권 적용: <span class="med-gray">교육프로그램별로 교육생 선발시 회원 우선권을 적용하여 선정</span>
										</li>
									
										<li>
											특별기획전 할인 : <span class="med-gray">해당 매표소에서 회원증을 제시ㆍ확인하고 할인 적용</span>	
										</li>
										<li>
											재가입 할인 : <span class="med-gray">회원기간 만료 후 1개월 이내 재가입시 연회비 10% 할인</span>	
										</li>
									</ul>
									<div class="extra-info med-gray" style="padding-left: 20px;">
										※미취학아동은 입장이 무료이며, 부모님 이름으로 교육이 신청이 가능하므로 연간회원에 가입하실 필요가 없습니다.
									</div>
									<div class="extra-info med-gray" style="padding-left: 20px;">
										※문의 : 국립과천과학관 안내데스크 02-3677-1551
									</div>			
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="online-member-spy" class="section">
					<div class="row">
						<div class="col-md-12">
							<h3 class="page-title green">
								<div class="top-line"></div>     
								온라인회원
							</h3>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							온라인회원으로 가입하시면 과천과학관의 교육 예약을 통한 서비스를 받으 실 수 있습니다
						</div>
					</div>
				</div>
				<!-- <div id="total-member-spy" class="section">
					<div class="row">
						<div class="col-md-3">
							<h3 class="page-title green">
								<div class="top-line"></div>     
								통합회원
							</h3>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							국립과천학관은 통합회원제를 시행하고 있습니다. 
						</div>
						<div class="col-md-12">
							국립과천과학관의 온라인회원 아이디 하나로 국립과천과학관이 운영하는 다양한 홈페이지의 서비스를 편리하게 이용하실 수 있습니다.
						</div>
					</div>
					<div class="sub-section">
						<ul class="circle-list green sub-section-title">
							<li>
								통합회원 아이디로 이용이 가능한 홈페이지
							</li>
						</ul>
						<div class="content">
							<div class="row">
								<div class="col-md-12 content-title-wrapper">
									<ul class="arrow-list green">
										<li>
											온라인 수학 홈페이지<a href="http://milc-seereal.or.kr">(http://milc-seereal.or.kr)</a>
										</li>
										<li>
											무한상상실 홈페이지<a href="https://makers.sciencecenter.go.kr/gnsm_makers/main/">(https://makers.sciencecenter.go.kr/gnsm_makers/main/)</a>
										</li>
										<li>
											국제 SF 영상 축제 홈페이지<a href="http://gisf.org">(http://gisf.org)</a>
										</li>
									</ul>			
								</div>
							</div>
						</div>
					</div>
				</div> -->
			</div>
		</div>
	</div>
</div>