<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="sub-content-nav scrollspy-green">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>

<div class="sub-body">
	<div id="support-body">
		<div id="basic-guide-spy" class="narrow-sub-top guide">
		
		</div>
		<div class="container">
			<div class="section">
				<div class="row">
					<div class="col-md-3">
						<h3 class="page-title green">
							<div class="top-line"></div>     
							후원회원
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						국립과천과학관 후원회 (사)과학사랑희망키움에서 후원회원을 모집합니다.<br>
						국립과천과학관 후원회는 2010년 10월 과학을 사랑하는 각계각층의 사람들이 모여 미래과학자들의 꿈과 희망을 실현 하고자 설립되었습니다.<br>
						특히, 경제적.지역적.문화적 소외계층 청소년들에게 과학을 접하고 즐길수 있도록 노력을 기울여 왔습니다.<br>
						그 결실로 2015년 구글(Google)의 에릭 슈미트 회장 방문과 86만 달러 후원으로 키즈메이커 스튜디오 및 구글 창조놀이터 건립이 진행되고 있습니다.<br>
						과학의 가치는 사회를 변화시키는 힘이 있습니다. 올해 4월부터, 청소년의 미래과학자 희망을 키워가기 위해 후원금을 모집합니다.<br>
						후원회원으로 가입하셔서 국립과천과학관에 사랑의 힘을 더해주시기를 부탁드립니다.<br>
					</div>
				</div>
				<div class="sub-section">
					<ul class="circle-list green sub-section-title">
						<li>
							회원 종류 및 연회비
						</li>
					</ul>
					<div class="row">
						<div class="col-md-12">
							<div class="content table-responsive">
								<div class="color-red">
									* 2016년 후원자에 한해 1회차 납부시 혜택적용 
								</div>
								<table class="table table-bordered centered-table">
									<thead>
										<tr>
											<th>회원종류</th>
											<th>회원비</th>
											<th>혜택적용 기간</th>
									</thead>
									<tbody>
										<tr>
											<td>
												개인회원 (월납)
											</td>
											<td>
												5,000원 / 월
											</td>
											<td>
												<ul class="dash-list no-margin text-left">
													<li>
														최초 3개월 납입시점부터 혜택적용(1, 2개월차에는 혜택없음)
													</li>
													<li>
														3개월 이상 기부금 미납 회원은 혜택이 제한될 수 있음.
													</li>
												</ul>
											</td>
										</tr>
										<tr>
											<td>
												개인회원 (연납)
											</td>
											<td>
												50,000원 / 연
											</td>
											<td>
												<ul class="dash-list no-margin text-left">
													<li>
														연 회원비 납입 후 바로혜택 적용
													</li>
												</ul>
											</td>
										</tr>
										<tr>
											<td>
												기관회원 (월납)
											</td>
											<td>
												10,000원 / 월
											</td>
											<td>
												<ul class="dash-list no-margin text-left">
													<li>
														최초 3개월 납입시점부터 혜택적용(1, 2개월차에는 혜택없음)
													</li>
													<li>
														3개월 이상 기부금 미납 회원은 혜택이 제한될 수 있음.
													</li>
												</ul>
											</td>
										</tr>
										<tr>
											<td>
												기관회원 (연납)
											</td>
											<td>
												100,000원 / 연
											</td>
											<td>
												<ul class="dash-list no-margin text-left">
													<li>
														연 회원비 납입 후 바로혜택 적용
													</li>
												</ul>
											</td>
										</tr>
										<tr>
											<td>
												Honor 평생 개인회원
											</td>
											<td>
												누적 1천만원 이상 (일괄납입 가능)
											</td>
											<td>
												<ul class="dash-list no-margin text-left">
													<li>
														평생 회원비 납입 후 바로혜택 적용
													</li>
												</ul>
											</td>
										</tr>
										<tr>
											<td>
												Honor 평생 기관회원
											</td>
											<td>
												누적 1억원 이상 (일괄납입 가능)
											</td>
											<td>
												<ul class="dash-list no-margin text-left">
													<li>
														평생 회원비 납입 후 바로혜택 적용
													</li>
												</ul>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						
						</div>
					</div>
				</div>
				<div class="sub-section">
					<ul class="circle-list green sub-section-title">
						<li>
							회원제 가입 방법 및 절차
						</li>
					</ul>				
					<div class="content">
						<div class="row">
							<div class="col-md-12 content-title-wrapper">
								<ul class="arrow-list text-color text-bold green">
									<li>
										온라인 가입
									</li>
								</ul>			
							</div>
						</div>
						<div class="detail">
							<div class="row">
								<div class="col-md-12">
									<div class="info">
										국립과천과학관 후원회(과학사랑희망키움) 홈페이지 후원하기 메뉴에서 가입하신 후 후원회비를 입금하시면 회원등록이 됩니다
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="process-header">
										가입절차
									</div>								
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12 col-md-6 col-lg-3">								
									<div class="process">
										<div class="text">
											과학관 후원회(과학사랑희밍키움) 홈페이지 후원하기 메뉴에서 가입
										</div>
										<i class="fa fa-angle-right fa-lg" aria-hidden="true"></i>
									</div>
								</div>
								<div class="col-sm-12 col-md-6 col-lg-3">								
									<div class="process ex-color single-line">
										<div class="text">
											후원회비 입금
										</div>
										<i class="fa fa-angle-right fa-lg" aria-hidden="true"></i>
									</div>
								</div>
								<div class="col-sm-12 col-md-6 col-lg-3">								
									<div class="process">
										<div class="text">
											과학관 최초 방문시 본인 확인 후 회원카드 발급
										</div>
										<i class="fa fa-angle-right fa-lg" aria-hidden="true"></i>
									</div>
								</div>
								<div class="col-sm-12 col-md-6 col-lg-3">								
									<div class="process ex-color single-line">
										<div class="text">
											과학관 입장
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="content">
						<div class="row">
							<div class="col-md-12 content-title-wrapper">
								<ul class="arrow-list text-color text-bold green">
									<li>
										방문가입
									</li>
								</ul>							
							</div>
						</div>
						<div class="detail">
							<div class="row">
								<div class="col-md-12">
									<div class="info">
										국립과천과학관 중앙홀 안내데스크(본관 1층)에서 가입신청서를 작성하신 후 후원회비를 입금하시면 회원등록이 됩니다.
										<div class="color-red">
											* 신청·접수 시간 : 9:30 ~ 16:30 (매주 월요일 휴관, 점심시간 12:30~13:30 제외) 
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="process-header">
										가입절차
									</div>								
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12 col-md-6 col-lg-3">								
									<div class="process  single-line">
										<div class="text">
											가입 신청서 작성
										</div>
										<i class="fa fa-angle-right fa-lg" aria-hidden="true"></i>
									</div>
								</div>
								<div class="col-sm-12 col-md-6 col-lg-3">								
									<div class="process ex-color single-line">
										<div class="text">
											후원회비 입금
										</div>
										<i class="fa fa-angle-right fa-lg" aria-hidden="true"></i>
									</div>
								</div>
								<div class="col-sm-12 col-md-6 col-lg-3">								
									<div class="process  single-line">
										<div class="text">
											회원카드 발급
										</div>
										<i class="fa fa-angle-right fa-lg" aria-hidden="true"></i>
									</div>
								</div>
								<div class="col-sm-12 col-md-6 col-lg-3">								
									<div class="process ex-color single-line">
										<div class="text">
											과학관 입장
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="content">
						<div class="detail">
							<div class="row">
								<div class="col-md-12">								
									위약금 없음 (후원 회원 개념)
								</div>
							</div>
						</div>
					</div>
					<div class="sub-section">
						<ul class="circle-list green sub-section-title">
							<li>
								후원 회원 혜택
							</li>
						</ul>
						<div class="content">
							<div class="row">
								<div class="col-md-12 content-title-wrapper">
									<ul class="arrow-list text-color text-bold green">
										<li>
											개인회원 (월납 & 연납)	
											<ul>
												<li>
													상설전시관 관람료 무료
												</li>
												<li>
													도슨트 전문해설 서비스 무료  
												</li>
												<li>
													창의체험과학탐구 개인과정 교육비 20% 할인
												</li>
												<li>
													천체투영관 관람료 50%할인
												</li>
												<li>
													천체관측소 교육비 20% 할인
												</li>
												<li>
													과학교육 및 문화행사 참여 50% 우선배정
												</li>
												<li>
													자원봉사 활동 50% 우선배정
												</li>
												<li>
													체험전시물 예약 30% 우선배정
												</li>
												<li>
													과학문화 공연 및 특별기획전 관람료 할인(별도공지)
												</li>
												<li>
													과학관 자체 기획전시회 초대 등
												</li>												
											</ul>
										</li>
										<li>
											Honor 평생 개인회원
											<ul>
												<li>
													개인회원 예우 및 혜택 (상단 내용 확인)
												</li>
												<li>
													과학관 내 후원인 명판설치
												</li>
												<li>
													과학의 날 특별전 오픈행사 초대
												</li>
												<li>
													예약 방문 시 전시해설 서비스 제공
												</li>
											</ul>
										</li>
										<li>
											기업회원(월납&연납)
											<ul>
												<li>
													기업회원에 대한 예우 및 혜택은 협의 후 제공
												</li>
											</ul>
										</li>
										<li>
											Honor 평생 기업회원
											<ul>
												<li>
													평생회원 예우 및 혜택
												</li>
												<li>
													감사패 증정
												</li>
											</ul>
										</li>
									</ul>			
								</div>
							</div>
							<div class="color-red">
								*후원회원 혜택은 과학관 정책에 따라 변경 될 수 있습니다.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>