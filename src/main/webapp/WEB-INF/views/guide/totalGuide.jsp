<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-green">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>
	
	<div id="display-body" class="sub-body">
		<div class="narrow-sub-top guide no-nav-tabs">
		</div>
		<div  class="container"> 
			<div id="tour-time-spy">
				<div class="row">
					<div class="col-md-12">
						<h3 class="page-title green">
							<div class="top-line"></div>     
							관람시간
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<ul class="circle-list green">
							<li>오전 9:30 ~ 오후 5:30 <span class="color-red"> *입장마감: 오후 4:30</span><br>
								<ul>
									<li class="med-gray">
										휴관일 1월 1일, 매주 월요일, 설날, 추석
									</li>
									<li class="med-gray">
										(단, 월요일이 공휴일인 경우, 화요일)									
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div id="tour-price-spy">
				<div class="row">
					<div class="col-md-12">
						<h3 class="page-title green">
							<div class="top-line"></div>     
							관람료
						</h3>
					</div>
				</div>
				<div id="display-sub-nav">
					<div class="sub-nav">
						<div class="row">
							<div class="col-xs-6 col-sm-3 col-md-2">
								<a href="#" class="item" data-name="permanent-exhibition">상설전시</a>
								<div class="visible-xs" style="margin-bottom: 15px"></div> 
							</div>					
							<div class="col-xs-6 col-sm-3 col-md-2">
								<a href="#" class="item" data-name="planetarium">천체투영관</a>
								<div class="visible-xs" style="margin-bottom: 15px"></div>
							</div>
							<div class="col-xs-6 col-sm-3 col-md-2">
								<a href="#" class="item" data-name="observatory">천체관측소</a>
							</div>
							<div class="col-xs-6 col-sm-3 col-md-2">
								<a href="#" class="item" data-name="spaceworld">스페이스월드</a>
							</div>
						</div>						
					</div>
				</div>
			</div>
			<div id="permanent-exhibition" class="tab-item price-box-wrapper">
				<div class="row sub-header">
					<div class="col-md-8">
						<ul class="circle-list green">
							<li>
								상설전시 요금안내 (02-3677-1561)
							</li>
						</ul>
					</div>
					<div class="col-md-4">
						<div class="text-right">
							<a href="<c:url value="/display/mainBuilding/basicScience"/>" class="reservation-btn green">
								상설전시 세부 정보 보기 <i class="fa fa-angle-right fa-lg" aria-hidden="true"></i>
							</a>
						</div>
					</div>
				</div>
				<div class="price-box table-responsive">
					<table class="table table-bordered centered-table">
						<thead>
							<tr>
								<th>프로그램 구분</th>
								<th>대상</th>
								<th>요금</th>
								<th>비고</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td >
									어른
								</td>
								<td>
									20 ~ 64 세
								</td>
								<td>
									4,000원 <span class="extra-info">(단체 3,000원)</span>
								</td>
								<td rowspan="5">
									<ul class="arrow-list green text-left">
										<li> 
											20인 이상 단체의 인솔자 1인 무료
											<ul>
												<li class="med-gray">
													(20명당 1인, 단 유치원 등 7세 미만 유아단체의 경우는 10명당 1인)
												</li>
											</ul>
										</li>
										<li> 
											경로우대자, 장애인, 국가유공자 및 기초생활수급자는 증명서 필히 지참
										</li>
										<li> 
											장애1~3급: 장애인과 동반 1인 무료
											장애4급 이상: 장애인 본인만 무료
										</li>
										<li> 
											다자녀사랑카드 보유자: 단체할인 요금 적용
											<ul>
												<li class="med-gray">
													(카드 소지자에 한함)
												</li>
											</ul>
										</li>
									</ul>
								</td>
							</tr>
							<tr>
								<td>
									청소년 및 어린이
								</td>
								<td>
									7 ~ 19 세
								</td>
								<td>
									2,000원 <span class="extra-info">(단체 1,500원)</span>
								</td>
							</tr>
							<tr>
								<td>
									유아
								</td>
								<td>
									7세 미만
								</td>
								<td>
									무료
								</td>
							</tr>
							<tr>
								<td>
									경로우대자
								</td>
								<td>
									65세 이상
								</td>
								<td>
									무료
								</td>
							</tr>
							<tr>
								<td>
									장애인, 국가유공자 및 기초생활수급자
								</td>
								<td>
									연령 구분 없음
								</td>
								<td>
									무료
								</td>
							</tr>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="4">
									<span class="extra-info">
										※ 상설전시관 관람은 별도 사전예약이 필요 없습니다.<br>
										전화 예약 불가(홈페이지의 행사일정 및 예약에서 가능)
									</span>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		
			<div id="planetarium" class="tab-item price-box-wrapper">
				<div class="row sub-header">
					<div class="col-md-8">
						<ul class="circle-list green">
							<li>
								천체투영관 요금안내 (02-3677-1561)
							</li>
						</ul>
					</div>
					<div class="col-md-4">
						<div class="text-right">
							<a href="<c:url value="/display/planetarium"/>" class="reservation-btn green">
								천체투영관 세부 정보 보기 <i class="fa fa-angle-right" aria-hidden="true"></i>
							</a>
						</div>
					</div>
				</div>
				<div class="price-box table-responsive">
					<table class="table table-bordered centered-table">
						<thead>
							<tr>
								<th>프로그램 구분</th>
								<th>대상</th>
								<th>요금</th>
								<th class="no-border-right">비고</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									어른
								</td>
								<td>
									20 ~ 64 세
								</td>
								<td>
									2,000원
								</td>
								<td rowspan="5">
									<ul class="arrow-list green text-left">  
										<li> 
											연간회원 50% 할인
										</li>
										<li> 
											경로우대자, 장애인, 국가유공자 및 기초생활수급자는 증명서 필히 지참
										</li>
										<li> 
											장애1~3급: 장애인과 동반 1인 50% 할인
										</li>
										<li>
											장애4급 이상: 장애인 본인만 50% 할인
										</li>										
									</ul>
								</td>
							</tr>
							<tr>
								<td>
									청소년 및 어린이
								</td>
								<td>
									5 ~ 19 세
								</td>
								<td>
									1,000원
								</td>
							</tr>
							<tr>
								<td>
									경로우대자
								</td>
								<td>
									65세 이상
								</td>
								<td>
									1,000원
								</td>
							</tr>
							<tr>
								<td rowspan="2">
									장애인, 국가유공자 및 기초생활수급자
								</td>
								<td>
									20세 이상
								</td>
								<td>
									1,000원
								</td>
							</tr>
							<tr>
								<td>
									5 ~19 세
								</td>
								<td>
									500원
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		
		
			<div id="observatory" class="tab-item price-box-wrapper">
				<div class="row sub-header">
					<div class="col-md-8">
						<ul class="circle-list green">
							<li>
								천체관측소 요금안내 (02-3677-1565)
							</li>
						</ul>
					</div>
					<div class="col-md-4">
						<div class="text-right">
							<a href="<c:url value="/display/planetarium/observation"/>" class="reservation-btn green">
								천체관측소 세부 정보 보기 <i class="fa fa-angle-right" aria-hidden="true"></i>
							</a>
						</div>
					</div>
				</div>
				<div class="price-box table-responsive">
					<table class="table table-bordered centered-table">
						<thead>
							<tr>
								<th>프로그램 구분</th>
								<th>대상</th>
								<th>요금</th>
								<th>비고</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									낮 프로그램
								</td>
								<td>
									초등학생 이상
								</td>
								<td>
									무료
								</td>
								<td>
									<ul>
										<li> 
											낮프로그램은 전시관 입장료 불필요
										</li>
									</ul>
								</td>
							</tr>
							<tr>
								<td>
									밤 프로그램
								</td>
								<td>
									초등학생 이상
								</td>
								<td>
									10,000원
								</td>
								<td>
									<ul>
										<li> 
											연간회원 20% 할인
										</li>
										<li> 
											단체(10인 이상 ~ 20인 이하)의 인솔교사 1인 무료
										</li>
										<li> 
											단체예약은 학기중에만 운영
										</li>
										<li> 
											온라인 예약자만 참가 가능
										</li>										
									</ul>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div id="spaceworld" class="tab-item price-box-wrapper">
				<div class="row sub-header">
					<div class="col-md-8">
						<ul class="circle-list green">
							<li>
								스페이스월드 요금안내 (02-3677-1402)
							</li>
						</ul>
					</div>
					<div class="col-md-4">
						<div class="text-right">
							<a href="<c:url value="/display/planetarium/spaceWorld"/>" class="reservation-btn green">
								스페이스월드 세부 정보 보기 <i class="fa fa-angle-right" aria-hidden="true"></i>
							</a>
						</div>
					</div>
				</div>
				<div class="price-box table-responsive">
					<table class="table table-bordered centered-table">
						<thead>
							<tr>
								<th>구분</th>
								<th>개인</th>
								<th>단체 할인</th>
								<th>연간회원 할인</th>
								<th>우대고객 할인</th>
								<th>비고</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									체험코스
								</td>
								<td>
									2,000원
								</td>
								<td>
									1,500원
								</td>
								<td>
									1,500원
								</td>
								<td>
									1,000원
								</td>
								<td>
									7세 이상
								</td>
							</tr>
							<tr>
								<td>
									종합코스
								</td>
								<td>
									-
								</td>
								<td>
									5,000원
								</td>
								<td>
									-
								</td>
								<td>
									-
								</td>
								<td>
									단체(평일운영) <span class="color-darkgreen">전화문의</span>
								</td>
							</tr>
							<tr>
								<td>
									심층탐구해설
								</td>
								<td>
									10,000원
								</td>
								<td>
									-
								</td>
								<td>
									8,000원
								</td>
								<td>
									5,000원
								</td>
								<td>
									4~6학년 (주말 운영)
								</td>
							</tr>
							<tr>
								<td>
									전시해설
								</td>
								<td>
									무료
								</td>
								<td>
									-
								</td>
								<td>
									-
								</td>
								<td>
									-
								</td>
								<td>
									
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div id="total-map-spy" class="section scrollspy">
				<div class="row">
					<div class="col-md-12">
						<h3 class="page-title green">
							<div class="top-line"></div>
							과학관 전체지도
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-lg-8 text-right">
						<c:import url="/WEB-INF/views/display/mainBuilding/svgs/display_svg_map_modified.jsp"></c:import>
					</div>
					
					<div class="col-sm-12 col-lg-4">
						<div class="astronomy-description margin-bottom20">
							<div class="row">
								<div class="col-md-12">
									<p>
										<b>
											이밖에 과학광장과 과학문화광장, 과학조각공원, 과학캠프장, 노천극장 등이 조성되어 있으며, 관람객들의 편의를 위하여 유아보호실과 비상구급실, 기념품점, 물품보관소, 휴게시설, 식당 등도 마련되어 있습니다. 
										</b>
									</p>
								</div>
							</div>
							<p>
								<h6 class="doc-title green">관람시간</h6>
								오전 09:30 ~ 오후 5:30, 과학관 관람시간 종료 1시간 전까지 입장가능
							</p>
							<p>
								<h6 class="doc-title green">천체관측 체험 시간</h6>
								평일 오후 2:00 ~ 오후 09:00, 방학/주말 오후 01:30 ~ 오후 09:30
							</p>
						</div>
					</div>
				</div>
				
			</div>
			<div id="display-info-spy"  class="section scrollspy">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-title green">
							<div class="top-line"></div>
							전시관 안내
						</h3>
					</div>
				</div>
				
				<div class="display-section">
					<div class="row">
						<div class="col-sm-12">
							<h5 class="doc-title">상설전시장</h5>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
							<div class="display-card green">
								<a href="<c:url value='/display/mainBuilding/scienceParkForKids' />">
									<div class="card-image">
										<div class="overlay">
											<i class="fa fa-search-plus"></i>
										</div>
										<img src="<c:url value="/resources/img/display/totalDisplay/exhibit_01.png"/>"/>
									</div>
									<div class="card-content">
										<div class="display-card-title">
											아이들의 눈높이에 맞춘 <b>어린이탐구체험관</b>
										</div>
										<p class="display-card-content">
											놀이하듯 신나게 과학을 체험하는 공간!<br>
											어린이탐구체험관은 만4세에서 초등학교 3학년까지의 어린이를
											위한 공간으로 구성되어 있습니다.
										</p>
									</div>
								</a>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
							<a href="<c:url value='/display/mainBuilding/basicScience' />">
								<div class="display-card green">
									<div class="card-image">
										<div class="overlay">
											<i class="fa fa-search-plus"></i>
										</div>
										<img src="<c:url value="/resources/img/display/totalDisplay/exhibit_02.png"/>"/>
									</div>
									<div class="card-content">
										<div class="display-card-title">
											생활 속 과학원리를 체험하는 <b>기초과학관</b>
										</div>
										<p class="display-card-content">
											과학의 원리를 몸으로 느끼다!<br>
											기초과학관은 첨단기술의 기초가 되는 지식을 제공하고, 생활 속
											과학원리를 체험할 수 있는 공간입니다.
										</p>
									</div>
								</div>
							</a>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
							<a href="<c:url value='/display/mainBuilding/traditionalSciences' />">
								<div class="display-card green">
									<div class="card-image">
										<div class="overlay">
											<i class="fa fa-search-plus"></i>
										</div>
										<img src="<c:url value="/resources/img/display/totalDisplay/exhibit_03.png"/>"/>
									</div>
									<div class="card-content">
										<div class="display-card-title">
											조상들의 슬기를 깨닫는 <b>전통과학관</b>
										</div>
										<p class="display-card-content">
											온고지신으로 미래와 소통하다!<br>
											전통과학관은 조상의 지혜가 깃든 존통과학기술을 체험할 수 있는
											공간으로, 전통과학기술에 숨어있는 과학적 원리를 체험하며 한국
											과학기술의 뿌리를 재발견하고 자긍심을 고취시킬 수 있는
											공간입니다.
										</p>
									</div>
								</div>
							</a>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
							<a href="<c:url value='/display/mainBuilding/advancedTechnology1' />">
								<div class="display-card green">
									<div class="card-image">
										<div class="overlay">
											<i class="fa fa-search-plus"></i>
										</div>
										<img src="<c:url value="/resources/img/display/totalDisplay/exhibit_04.png"/>"/>
									</div>
									<div class="card-content">
										<div class="display-card-title">
											과학기술의 미래가 한눈에 보이는 <b>첨단기술관</b>
										</div>
										<p class="display-card-content">
											놀이하듯 신나게 과학을 체험하는 공간!<br>
											어린이탐구체험관은 만4세에서 초등학교 3학년까지의 어린이를
											위한 공간으로 구성되어 있습니다.
										</p>
									</div>
								</div>
							</a>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
							<a href="<c:url value='/display/mainBuilding/naturalHistory' />">
								<div class="display-card green">
									<div class="card-image">
										<div class="overlay">
											<i class="fa fa-search-plus"></i>
										</div>
										<img src="<c:url value="/resources/img/display/totalDisplay/exhibit_05.png"/>"/>
									</div>
									<div class="card-content">
										<div class="display-card-title">
											살아움직이는 지구를 만나는 <b>자연사관</b>
										</div>
										<p class="display-card-content">
											45억년 지구의 신비를 발견하다!<br>
											자연사관은 약 2,580m<sup>2</sup>의 전시공간에 46억년 동안의 지구환경과
											생태계의 변화를 경험할 수 있도록 구성되어 있습니다.
										</p>
									</div>
								</div>
							</a>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
							<a href="<c:url value='/display/frontier/hallOfFame' />">
								<div class="display-card green">
									<div class="card-image">
										<div class="overlay">
											<i class="fa fa-search-plus"></i>
										</div>
										<img src="<c:url value="/resources/img/display/totalDisplay/exhibit_06.png"/>"/>
									</div>
									<div class="card-content">
										<div class="display-card-title">
											우리 과학기술의 발전을 경험하는 <b>명예의 전당</b>
										</div>
										<p class="display-card-content">
											우수한 우리 과학기술을 느끼다!<br>
											명예의전당은 과학기술인에게는 명예와 자부심을 높이고 청소년에게는 우리 과학기술의
											우수성과 발전과정을 경험하는 공간입니다.
										</p>
									</div>
								</div>
							</a>
						</div>				
					</div>
				</div>
				
				<div class="display-section">
					<div class="row margin-bottom20">
						<div class="col-sm-12">
							<h5 class="doc-title">옥외전시장</h5>
						</div>
						<div class="col-sm-12">
							옥외전시장은 우주항공, 역사의광장, 지질동산, 공룡동산으로 조성되어 있습니다.
						</div>
					</div>
					<div class="row">
						<div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 out-display">
							<img src="<c:url value="/resources/img/display/totalDisplay/out_display_01.png"/>" 
								class="out-display-item img-responsive"/>
							<p>
								우주항공
							</p>
						</div>
						<div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 out-display">
							<img src="<c:url value="/resources/img/display/totalDisplay/out_display_04.png"/>" class="out-display-item img-responsive"/>
							<p>
								역사의광장
							</p>
						</div>
						<div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 out-display">
							<img src="<c:url value="/resources/img/display/totalDisplay/out_display_05.png"/>" class="out-display-item img-responsive"/>
							<p>
								지질동산
							</p>
						</div>
						<div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 out-display">
							<img src="<c:url value="/resources/img/display/totalDisplay/out_display_06.png"/>" class="out-display-item img-responsive"/>
							<p>
								공룡동산 
							</p>
						</div>
					</div>
				</div>
				
				<div class="display-section">
					<div class="row">
						<div class="col-sm-12">
							<h5 class="doc-title">생태체험학습장</h5>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="card display-card green margin-bottom20">
								<div class="card-image">
									<img src="<c:url value="/resources/img/display/totalDisplay/live_display_01.png"/>"/>
								</div>
								<div class="card-content">
									<div class="display-card-title">
										<b>곤충생태관</b> 자연의 품 속에서 생명의 신비한 숨소리까지!
									</div>
									<p class="display-card-content live-display">
										육상곤충, 수서곤층, 나비, 거미, 유충사육실, 곤충표본실 등 48주제 전시물이 있습니다.
									</p>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="card display-card green margin-bottom20">
								<div class="card-image">
									<img src="<c:url value="/resources/img/display/totalDisplay/live_display_02.png"/>"/>
								</div>
								<div class="card-content">
									<div class="display-card-title">
										<b>생태공원</b> 자연속에서 과학을 만나다!
									</div>
									<p class="display-card-content live-display">
										생태공원은 생태체험학습장 겸 휴식공간으로 총 16,542m2 면적에 생태연못,
										자생야생화원, 자원식물원, 푸른향기원 등의 테마로 구성되어 있습니다.
									</p>
								</div>
							</div>
						</div>	
					</div>
				</div>
				
				<div class="">
					<div class="row">
						<div class="col-sm-12">
							<h5 class="doc-title">천문시설</h5>
						</div>
						<div class="col-sm-12">
							천문시설은 지금 25m의 돔스크린에 밤하늘을 재현하여 우주를 체험할 수 있도록 한 전체투영관과 대형 천체망원경으로 우주를 관찰할 수 있는 천체관측소로 이루어져 있습니다.
						</div>
					</div>
					<div class="row margin-bottom20 margin-top20">
						<div class="col-xs-12 col-sm-4 col-md-4 margin-bottom20">
							<div class="card display-card green">
								<div class="card-image">
									<img src="<c:url value="/resources/img/display/totalDisplay/astronomy_display_01.png"/>" class="img-responsive"/>
								</div>
								<div class="card-content">
									<div class="display-card-title">
										<b>천체관측소(보조 망원경)</b>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 margin-bottom20">
							<div class="card display-card green">
								<div class="card-image">
									<img src="<c:url value="/resources/img/display/totalDisplay/astronomy_display_02.png"/>" class="img-responsive"/>
								</div> 
								<div class="card-content">
									<div class="display-card-title">
										<b>천체관측소(1m 반사망원경)</b>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 margin-bottom20">
							<div class="card display-card green">
								<div class="card-image">
									<img src="<c:url value="/resources/img/display/totalDisplay/astronomy_display_03.png"/>" class="img-responsive"/>
								</div>
								<div class="card-content">
									<div class="display-card-title">
										<b>천체투영관</b>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="astronomy-description margin-bottom20">
								<div class="row">
									<div class="col-md-12">
										<p>
											<b>
												이밖에 과학광장과 과학문화광장, 과학조각공원, 과학캠프장, 노천극장 등이 조성되어 있으며, 관람객들의 편의를 위하여 유아보호실과 비상구급실, 기념품점, 물품보관소, 휴게시설, 식당 등도 마련되어 있습니다. 
											</b>
										</p>
									</div>
								</div>
								<p>
									<span class="green">관람시간</span> 오전 09:30 ~ 오후 5:30, 과학관 관람시간 종료 1시간 전까지 입장가능
								</p>
								<div class="row">
									<div class="col-md-8">
										<p>
											<span class="green">천체관측 체험 시간</span> 평일 오후 2:00 ~ 오후 09:00, 방학/주말 오후 01:30 ~ 오후 09:30
										</p>								
									</div>
								</div>
								
							</div> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> 
</div> 

<!-- Modal Structure -->
<div id="exhibitionMapModal" class="modal">
  <div class="modal-content">
    <h4>Modal Header</h4>
    <p>A bunch of text</p>
  </div>
  <div class="modal-footer">
    <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Agree</a>
  </div>
</div>


<script type="text/javascript">
	$(document).ready(function(){
	    
	 	// 전시관 Map 1층으로.
		setDefaultSVGMap('#total-map-spy', 1);
	    
		$('.modal-trigger').on('click', function() {
			
			// get image url form data-url tag
			var url = $(this).data('url');
			$('#exhibitionMapModal .modal-content').append('<img src="' + url + '"/>');
			// open Modal
			$('#exhibitionMapModal').openModal();
		});
		
		// 관람료 안내 tab
		$("a[data-name='permanent-exhibition']").click();
		
	}); 
  	
	$('#display-sub-nav .sub-nav .item').on('click',function(e) {
		e.preventDefault();
		var id = $(this).data('name');
		
		$('#display-sub-nav .sub-nav .item').removeClass('active');
		$(this).addClass('active');
		
		$('#display-body .tab-item').removeClass('active');
		$('#'+id+'.tab-item').addClass('active');
	});  
	
</script>

