<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="sub-content-nav scrollspy-green">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	<%-- <nav id="scrollspy-nav" class="navbar-default navbar-static has-scroll">
        <div class="container">
            <ul class="nav navbar-nav">
                <c:import url="/WEB-INF/views/guide/navbar.jsp"/>
                <li>
                    <a href="#basic-info-spy" class="item">
                        식음시설
                    </a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <div class="bread-crumbs">
                        관람안내 / 식음시설
                    </div>
                </li>
            </ul>
        </div>
    </nav> --%>
</div>

<div class="sub-body">
	<div class="narrow-sub-top guide no-nav-tabs">
		<div id="basic-info-spy" class="container">
			<!-- <h4 class="doc-title">식음시설</h4>
			<h6 class="doc-title exp-desc">
				국립과천과학관은 이용하시는 관람객 여러분의 편의를 최우선으로 생각합니다.<br>
				가장 필요한 서비스를 제공하기위해 끊임없이 노력하겠습니다.
			</h6> -->
		</div>
	</div><!-- support-top END... -->
	<div id="support-sub-nav">  
		<div class="container">
		   	<div class="sub-nav">
				<div class="row">	
					<div class="col-xs-6 col-sm-3 col-md-3">
			     		<a href="#"  class="item" data-name="foodCourt">푸드코트</a> 
			     		<div class="visible-xs" style="margin-bottom: 15px"></div> 
					</div>
					<!-- <div class="col-xs-6 col-sm-4 col-md-2">
				      	<a href="#" class="item" data-name="snack">스낵</a>
				      	<div class="visible-xs" style="margin-bottom: 15px"></div> 
					</div> -->
					<div class="col-xs-6 col-sm-3 col-md-3">
					    <a href="#" class="item" data-name="cafe">카페</a>
					    <div class="visible-xs" style="margin-bottom: 15px"></div> 
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3">
					    <a href="#" class="item" data-name="convStore">편의점</a>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3">
					    <a href="<c:url value ='/dinnerApply' />" class="item" data-name="dinnerApply">석식신청(직원용)</a>
					</div>				     
				</div>  
			</div>
		</div>
	</div> 
	<div id="support-body" class="container">
		<div id="foodCourt" class="tab-item">	
			<div class="sub-header">
				<div class="row">  
					<div class="col-xs-6 col-md-3">
						<h3 class="page-title green">
							<div class="top-line"></div>     
							푸드코트
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="margin-bottom20">
							<img src="<c:url value='/resources/img/support/food/food_court/main_logo.gif'/>" class="img-responsive" alt="foot court main logo"/>
						</div>
						<ul class="circle-list green">
							<li>
								이용 안내
								<ul>
									<li>
										전시관 2층에는 관람객을 위한 테이스티 플래닛이 위치해 있습니다. 
									</li>
									<li>
										테이스티 플래닛은 가츠엔(일식), 누들(면식), 봄이온소반(한식), 브라운그릴(양식) 4개로 구성 되어 있습니다. 
									</li>
									<li>
										Happiness on the go 'Take me out', 간단하지만 만족스러운 음식으로 달콤한 여유를 찾을 수 있는 Take-out 코너입니다. 
									</li>  
									<li>
										 단체메뉴 예약 : 02)502-6743 <br>
										예약문의시간 : AM 10:00 ~ 17:00 <br> 
										점심시간 : 12:00 ~ 14:00 
									</li>  
								</ul>
							</li>
						</ul>
						<div class="margin-bottom20">
							<img src="<c:url value='/resources/img/support/food/snack/snack_logo.png'/>" class="img-responsive" alt="snack section logo"/>
						</div>
						<ul class="circle-list green">
							<li>
								이용 안내
								<ul>
									<li>
										Happiness on the go 'Take me out', 간단하지만 만족스러운 음식으로 달콤한 여유를 찾을 수 있는 Take-out 코너입니다. 
									</li>  
								</ul>
							</li>
						</ul>
					</div>
					<div class="col-md-8 text-right convenience">
						<c:import url="/WEB-INF/views/display/mainBuilding/svgs/display_svg_map_modified.jsp"></c:import>
					</div>
				</div>
			</div>
			<div class="food-detail">
				<div class="row">
					<div class="col-sm-6 margin-bottom30">   
						<img src="<c:url value='/resources/img/support/food/food_court/foodcourt01.png'/>" class="img-responsive" alt="food court image 01"/>
					</div>
					<div class="col-sm-6 margin-bottom30">
						<img src="<c:url value='/resources/img/support/food/food_court/foodcourt02.png'/>" class="img-responsive" alt="food court image 02"/>
					</div>
					<div class="col-sm-6 margin-bottom30">
						<img src="<c:url value='/resources/img/support/food/snack/snack01.png'/>" class="img-responsive" alt="food court image 03"/>
					</div>
					<div class="col-sm-6 margin-bottom30">
						<img src="<c:url value='/resources/img/support/food/snack/snack02.png'/>" class="img-responsive" alt="food court image 04"/>
					</div>
				</div>
				
				<div class="foodCourt-restaurant-wrapper">
					<div class="sub-nav">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-2-5">
								<a href="#"  class="item" data-name="gatz">가츠엔 (일식)</a> 
							</div>				
							<div class="col-xs-12 col-sm-12 col-md-2-5">
						      	<a href="#" class="item" data-name="noodle">누들</a>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-2-5">  
							    <a href="#" class="item" data-name="soban">봄이온 소반</a>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-2-5">
							    <a href="#" class="item" data-name="brownGrill">브라운 그릴</a>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-2-5">
							    <a href="#" class="item" data-name="group-food">단체</a>
							</div>		
						</div>						
						
					</div>
					  
					<div id="gatz" class="restaurant-item">
						<div class="intro">
							<div class="row">  
								<div class="left-side col-md-6">
									<div class="logo">
										<img src="<c:url value='/resources/img/support/food/food_court/restaurant01_logo.png'/>" class="img-responsive" alt="restaurant-gatz-logo"/>
									</div>
									<div class="detail">
										일본 정통 조리법으로 요리하여 보다 부드러운 함박스테이크와 재료의 맛을 살린 가츠류를 선보이는 캐주얼 일식 전문점입니다.
									</div>
								</div>
								<div class="right-side col-md-6">
									<img src="<c:url value='/resources/img/support/food/food_court/restaurant01_image.png'/>" class="img-responsive" alt="restaurant-gatz-image"/>
								</div>
							</div>
						</div>
						<div class="menu">
							<div class="row">
								<div class="col-md-12">
									<div class="header">
										대표 메뉴
									</div>	
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="menu-item">
										<div class="image">
											<img src="<c:url value='/resources/img/support/food/food_court/restaurant01_menu01.png'/>" class="img-responsive" alt="cheese gatz"/>
										</div>
										<div class="detail">
											<div class="menu-name">
												치즈돈가스 &amp; 미니우동 9,500원<br>
												(단품 치즈돈가스 : 8,000원)
											</div>
											<div class="menu-detail">
												치즈에 담긴 과학원리는 가열,응고,숙성!<br>
   												치즈돈가스에 미니우동을 함께 드시면 더욱 맛있어요.
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="row">
										<div class="col-xs-12 col-sm-6">
											<div class="menu-item-sm">
												<div class="image">
													<img src="<c:url value='/resources/img/support/food/food_court/restaurant01_menu02.png'/>" class="img-responsive" alt="coconut shrimp rice"/>
												</div>
												<div class="detail">
													<div class="menu-name">
														코코넛새우오므라이스 7,000원
													</div>
													<div class="menu-detail">
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6">
											<div class="menu-item-sm">
												<div class="image">
													<img src="<c:url value='/resources/img/support/food/food_court/restaurant01_menu03.png'/>" class="img-responsive" alt="stake"/>
												</div>
												<div class="detail">
													<div class="menu-name">
														함박스테이크 8,000원
													</div>
													<div class="menu-detail">
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6">
											<div class="menu-item-sm">
												<div class="image">
													<img src="<c:url value='/resources/img/support/food/food_court/restaurant01_menu04.png'/>" class="img-responsive" alt="stake"/>
												</div>
												<div class="detail">
													<div class="menu-name">
														치즈퐁듀돈가스(4꼬치) 10,000원
													</div>
													<div class="menu-detail">
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6">
											<div class="menu-item-sm">
												<div class="image">
													<img src="<c:url value='/resources/img/support/food/food_court/restaurant01_menu04.png'/>" class="img-responsive" alt="stake"/>
												</div>
												<div class="detail">
													<div class="menu-name">
														치즈퐁듀돈가스(3꼬치) 8,500원
													</div>
													<div class="menu-detail">
													</div>
												</div>
											</div>
										</div>
									
									
									</div>
								</div>
								
							</div>
							
							<div class="row">
							</div>
						</div>
					</div><!--  id="gatz" END ... -->
					<div id="noodle" class="restaurant-item">
						<div class="intro">
							<div class="row">  
								<div class="left-side col-md-6">
									<div class="logo">
										<img src="<c:url value='/resources/img/support/food/food_court/restaurant02_logo.png'/>" class="img-responsive" alt="restaurant-noodle-logo"/>
									</div>
									<div class="detail">
										전 세계의 소문난 면요리를 한국인의 입맛에 맞도록 재현한 면 코너입니다.
									</div>
								</div>
								<div class="right-side col-md-6">
									<img src="<c:url value='/resources/img/support/food/food_court/restaurant02_image.png'/>" class="img-responsive" alt="restaurant-noodle-image"/>
								</div>
							</div>
						</div>
						<div class="menu">
							<div class="row">
								<div class="col-md-12">
									<div class="header">
										대표 메뉴
									</div>	
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="menu-item">
										<div class="image">
											<img src="<c:url value='/resources/img/support/food/food_court/restaurant02_menu01.png'/>" class="img-responsive" alt="noodle menu 1"/>
										</div>
										<div class="detail">
											<div class="menu-name">
												냉오징어숙회면&감자만두(3pcs) 8,000원<br>
												(단품 냉오징어숙회면 : 7,000원)
											</div>
											<div class="menu-detail">
												바다의 신선함을 담은 오징어와<br>
   												땅의 생명력을 담은 감자의 쫄깃한 만남!
											</div>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-3">
									<div class="menu-item-sm">
										<div class="image">
											<img src="<c:url value='/resources/img/support/food/food_court/restaurant02_menu02.png'/>" class="img-responsive" alt="noodle menu 2"/>
										</div>
										<div class="detail">
											<div class="menu-name">
												꼬치 어묵 우동 6,000원
											</div>
											<div class="menu-detail">
											</div>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-3">
									<div class="menu-item-sm">
										<div class="image">
											<img src="<c:url value='/resources/img/support/food/food_court/restaurant02_menu03.png'/>" class="img-responsive" alt="noodle menu 3"/>
										</div>
										<div class="detail">
											<div class="menu-name">
												올리브 자장면 & 탕수육 7,500원
											</div>
											<div class="menu-detail">
											</div>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-3">
									<div class="menu-item-sm">
										<div class="image">
											<img src="<c:url value='/resources/img/support/food/food_court/restaurant02_menu04.png'/>" class="img-responsive" alt="noodle menu 4"/>
										</div>
										<div class="detail">
											<div class="menu-name">
												단호박콩국수 6,500원
											</div>
											<div class="menu-detail">
											</div>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-3">
									<div class="menu-item-sm">
										<div class="image">
											<img src="<c:url value='/resources/img/support/food/food_court/restaurant02_menu05.png'/>" class="img-responsive" alt="noodle menu 5"/>
										</div>
										<div class="detail">
											<div class="menu-name">
												흑음자콩국수 6,500원
											</div>
											<div class="menu-detail">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div><!--  id="noodle" END ... -->
					<div id="soban" class="restaurant-item">
						<div class="intro">
							<div class="row">  
								<div class="left-side col-md-6">
									<div class="logo">
										<img src="<c:url value='/resources/img/support/food/food_court/restaurant03_logo.png'/>" class="img-responsive" alt="restaurant-soban-logo"/>
									</div>
									<div class="detail">
										전 세계의 소문난 면요리를 한국인의 입맛에 맞도록 재현한 면 코너입니다.
									</div>
								</div>
								<div class="right-side col-md-6">
									<img src="<c:url value='/resources/img/support/food/food_court/restaurant03_image.png'/>" class="img-responsive" alt="restaurant-soban-image"/>
								</div>
							</div>
						</div>
						<div class="menu">
							<div class="row">
								<div class="col-md-12">
									<div class="header">
										대표 메뉴
									</div>	
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="menu-item">
										<div class="image">
											<img src="<c:url value='/resources/img/support/food/food_court/restaurant03_menu01.png'/>" class="img-responsive" alt="soban menu 1"/>
										</div>
										<div class="detail">
											<div class="menu-name">
												무지개 비빔밥 7,000원
											</div>
											<div class="menu-detail">
												빨주노초파남보 무지개빛 7가지 식재료가<br>
												먹음직스럽게 잘 어우러진 건강 비빔밥을 맛보세요!
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="row">
										<div class="col-xs-12 col-sm-6">
											<div class="menu-item-sm">
												<div class="image">
													<img src="<c:url value='/resources/img/support/food/food_court/restaurant03_menu02.png'/>" class="img-responsive" alt="soban menu 2"/>
												</div>
												<div class="detail">
													<div class="menu-name">
														불고기 덮밥 7,000원
													</div>
													<div class="menu-detail">
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6">
											<div class="menu-item-sm">
												<div class="image">
													<img src="<c:url value='/resources/img/support/food/food_court/restaurant03_menu03.png'/>" class="img-responsive" alt="soban menu 3"/>
												</div>
												<div class="detail">
													<div class="menu-name">
														사골 우거지탕 6,500원
													</div>
													<div class="menu-detail">
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6">
											<div class="menu-item-sm">
												<div class="image">
													<img src="<c:url value='/resources/img/support/food/food_court/restaurant03_menu04.png'/>" class="img-responsive" alt="soban menu 4"/>
												</div>
												<div class="detail">
													<div class="menu-name">
														나주곰탕 8,000원
													</div>
													<div class="menu-detail">
													</div>
												</div>
											</div>
										</div>
									
									</div>
								</div>
							</div>
						</div>
					</div><!--  id="soban" END ... -->
					<div id="brownGrill" class="restaurant-item">
						<div class="intro">
							<div class="row">  
								<div class="left-side col-md-6">
									<div class="logo">
										<img src="<c:url value='/resources/img/support/food/food_court/restaurant04_logo.png'/>" class="img-responsive" alt="brown-grill-logo"/>
									</div>
									<div class="detail">
										American 스타일과 맛을 제대로 살려낸 인기만점 버거 코너입니다.
									</div>
								</div>
								<div class="right-side col-md-6">
									<img src="<c:url value='/resources/img/support/food/food_court/restaurant04_image.png'/>" class="img-responsive" alt="brown-grill image"/>
								</div>
							</div>
						</div>
						<div class="menu">
							<div class="row">
								<div class="col-md-12">
									<div class="header">
										대표 메뉴
									</div>	
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="menu-item">
										<div class="image">
											<img src="<c:url value='/resources/img/support/food/food_court/restaurant04_menu01.png'/>" class="img-responsive" alt="brown grill menu 1"/>
										</div>
										<div class="detail">
											<div class="menu-name">
												우주선버거세트 7,000원
											</div>
											<div class="menu-detail">
												인간의 오랜 꿈 우주여행!<br>
												체다치즈로 만든 맛있는 우주선을 타고 우주여행을 떠나보세요!
											</div>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-3">
									<div class="menu-item-sm">
										<div class="image">
											<img src="<c:url value='/resources/img/support/food/food_court/restaurant04_menu02.png'/>" class="img-responsive" alt="brown grill menu 2"/>
										</div>
										<div class="detail">
											<div class="menu-name">
												새우버거세트 6,500원
											</div>
											<div class="menu-detail">
											</div>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-3">
									<div class="menu-item-sm">
										<div class="image">
											<img src="<c:url value='/resources/img/support/food/food_court/restaurant04_menu03.png'/>" class="img-responsive" alt="brown grill menu 3"/>
										</div>
										<div class="detail">
											<div class="menu-name">
												지구버거세트 7,000원
											</div>
											<div class="menu-detail">
											</div>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-3">
									<div class="menu-item-sm">
										<div class="image">
											<img src="<c:url value='/resources/img/support/food/food_court/restaurant04_menu04.png'/>" class="img-responsive" alt="brown grill menu 4"/>
										</div>
										<div class="detail">
											<div class="menu-name">
												양념감자튀김 2,000원
											</div>
											<div class="menu-detail">
											</div>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-3">
									<div class="menu-item-sm">
										<div class="image">
											<img src="<c:url value='/resources/img/support/food/food_court/restaurant04_menu05.png'/>" class="img-responsive" alt="brown grill menu 5"/>
										</div>
										<div class="detail">
											<div class="menu-name">
												치킨텐더(3pcs) 3,500원												
											</div>
											<div class="menu-detail">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="group-food" class="restaurant-item">
						<div class="menu">
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-3">
									<div class="menu-item-sm">
										<div class="image">
											<img src="<c:url value='/resources/img/support/food/food_court/restaurant05_menu01.png'/>" class="img-responsive" alt="group menu 1"/>
										</div>
										<div class="detail">
											<div class="menu-name">
												함박스테이크 6,000원
											</div>
											<div class="menu-detail">
											</div>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-3">
									<div class="menu-item-sm">
										<div class="image">
											<img src="<c:url value='/resources/img/support/food/food_court/restaurant05_menu02.png'/>" class="img-responsive" alt="group menu 2"/>
										</div>
										<div class="detail">
											<div class="menu-name">
												불고기덮밥 6,500원
											</div>
											<div class="menu-detail">
											</div>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-3">
									<div class="menu-item-sm">
										<div class="image">
											<img src="<c:url value='/resources/img/support/food/food_court/restaurant05_menu03.png'/>" class="img-responsive" alt="group menu 3"/>
										</div>
										<div class="detail">
											<div class="menu-name">
												자장덮밥 5,500원
											</div>
											<div class="menu-detail">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
				
			</div>
		</div> <!--  #foodCourt END ... -->
		

		<%-- <div id="snack" class="tab-item">	
			<div class="sub-header">
				<div class="row">  
					<div class="col-xs-6 col-md-3">
						<h3 class="page-title green">
							<div class="top-line"></div>     
							스낵
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="margin-bottom20">
							<img src="<c:url value='/resources/img/support/food/snack/snack_logo.png'/>" class="img-responsive"/>
						</div>
						<ul class="circle-list green">
							<li>
								이용 안내
								
							</li>
						</ul>
					</div>
					<div class="col-md-8 text-right convenience">
						<c:import url="/WEB-INF/views/display/mainBuilding/svgs/display_svg_map_modified.jsp"></c:import>
					</div>
				</div>
			</div>
			<div class="food-detail">
				<div class="row">
					<div class="col-sm-6 col-md-6 margin-bottom30">
						<img src="<c:url value='/resources/img/support/food/snack/snack01.png'/>" class="img-responsive"/>
					</div>
					<div class="col-sm-6 col-md-6 margin-bottom30">
						<img src="<c:url value='/resources/img/support/food/snack/snack02.png'/>" class="img-responsive"/>
					</div>
				</div>
			</div>
		</div> <!--  #snack END ... --> --%>
		
		<div id="cafe" class="tab-item">	
			<div class="sub-header">
				<div class="row">  
					<div class="col-xs-6 col-md-3">
						<h3 class="page-title green">
							<div class="top-line"></div>     
							카페
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="margin-bottom20">
							<img src="<c:url value='/resources/img/support/food/cafe/cafe_logo.png'/>" class="img-responsive"/>
						</div>
						<ul class="circle-list green">
							<li>
								이용 안내
								<ul>
									<li>
										엄선된 아라비카 원두만을 사용하여 차별화된 공정을 거친 신선한 커피만을 제공합니다.
									</li>  
								</ul>
							</li>
						</ul>
					</div>
					<div class="col-md-8 text-right convenience">
						<c:import url="/WEB-INF/views/display/mainBuilding/svgs/display_svg_map_modified.jsp"></c:import>
					</div>
				</div>
			</div>
			<div class="food-detail">
				<div class="row">
					<div class="col-sm-6 col-md-6 margin-bottom30">
						<img src="<c:url value='/resources/img/support/food/cafe/cafe01.png'/>" class="img-responsive"/>
					</div>
					<div class="col-sm-6 col-md-6 margin-bottom30">
						<img src="<c:url value='/resources/img/support/food/cafe/cafe02.png'/>" class="img-responsive"/>
					</div>
				</div>
				
				<div class="menu-wrapper">
					<div class="row">
						<div class="col-xs-6 col-sm-4 col-md-3">
							<div class="menu">
								<div class="category">
									HOT COFFEE
								</div>
								<ul class="non-padding text-color med-gray">
									<li>에스프레소</li>
									<li>카페 아메리카노</li>
									<li>카푸치노</li>
									<li>카페 라떼</li>
									<li>카페 모카</li>
									<li>카라멜 마끼아또</li>
									<li>리얼 바닐라 카페 라떼</li>
								</ul>
							</div>
						</div>
						<div class="col-xs-6 col-sm-4 col-md-3">
							<div class="menu">
								<div class="category">
									ICED COFFEE
								</div>
								<ul class="non-padding text-color med-gray">
									<li>아이스 카페 아메리카노</li>
									<li>아이스 카페 라떼</li>
									<li>아이스 카페 모카</li>
									<li>아이스 카라멜 마끼아또</li>
									<li>아이스 리얼 바닐라 라떼</li>
								</ul>
							</div>
						</div>
						<div class="col-xs-6 col-sm-4 col-md-3 tea-wrapper">
							<div class="menu">
								<div class="category">
									TEA
								</div>
								<ul class="non-padding text-color med-gray">
									<li>유기농 얼그레이</li>
									<li>유기농 녹차</li>
									<li>캐모마일 시트러스</li>
									<li>루이보스 아프간넥타</li>
									<li>초콜릿 민트 트러플</li>
									<li>아이스 퍼브티</li>
									<li>그린티 라떼</li>
									<li>블랙티 라떼</li>
									<li>아이스 그린티 / 블랙티 라떼</li>
									<li>아이스 티(복숭아/레몬)</li> 
								</ul>
							</div>
						</div>
						<div class="col-xs-6 col-sm-4 col-md-3">
							<div class="menu">
								<div class="category">
									OTHERS
								</div>
								<ul class="non-padding text-color med-gray">
									<li>핫 초콜릿 (다크/화이트)</li>
									<li>고구마 라떼</li>
									<li>블랙빈 라떼</li>
									<li>아이스 초콜릿 (다크/화이트)</li>
									<li>아이스 고구마 라떼</li>
									<li>아이스 블랙빈 라떼</li> 
								</ul>
							</div>
						</div>
						<div class="col-xs-6 col-sm-4 col-md-3 etc-wrapper">
							<div class="menu">
								<div class="category">
									ETC
								</div>
								<ul class="non-padding text-color med-gray">
									<li>샷 / 휘핑크림 추가</li>
									<li>향시럽 / 소스 추가</li>
								</ul>
							</div>
						</div>
						<div class="col-xs-6 col-sm-4 col-md-3">
							<div class="menu">
								<div class="category">
									ICED BLENDED
								</div>
								<ul class="non-padding text-color med-gray">
									<li>바닐라 아이스 블렌디드</li>
									<li>그린티 아이스 블렌디드</li>
									<li>초콜릿 아이스 블렌디드</li>
								</ul>
							</div>
						</div>
						<div class="col-xs-6 col-sm-4 col-md-3">
							<div class="menu">
								<div class="category">
									JUICE & ADE
								</div>
								<ul class="non-padding text-color med-gray">
									<li>과일주스 (딸기/바나나/키위)</li>
									<li>혼합주스 (딸기+바나나)</li>
									<li>에이드 (레몬/자몽/청포도/오렌지)</li> 
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> <!--  #cafe END ... -->
		
		<div id="convStore" class="tab-item">	
			<div class="sub-header">
				<div class="row">  
					<div class="col-xs-6 col-md-3">
						<h3 class="page-title green">
							<div class="top-line"></div>     
							편의점
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="margin-bottom20">
							<img src="<c:url value='/resources/img/support/food/conv_store/cu_logo.png'/>" class="img-responsive"/>
						</div>
						<ul class="circle-list green">
							<li>
								이용 안내
								<ul>
									<li>
										신선함으로 일상을 충전하고 활력을 얻는 공간.
									</li>
									<li>
										고객의 하루가 시작되고 머무르고 마무리되는 Fresh한 경험을 통해 일상을 Refresh!!
									</li>    
									<li>
										CU가 함께 합니다.
									</li>
									<li>
										영업시간 : 9:30 ~ 18:00
									</li>    
								</ul>
							</li>
						</ul>
					</div>
					<div class="col-md-8 text-right convenience">
						<c:import url="/WEB-INF/views/display/mainBuilding/svgs/display_svg_map_modified.jsp"></c:import>
					</div>
				</div>
			</div>
			<div class="food-detail">
				<div class="row">
					<div class="col-sm-6 col-md-6 margin-bottom30">
						<img src="<c:url value='/resources/img/support/food/conv_store/cu01.png'/>" class="img-responsive"/>
					</div>
					<div class="col-sm-6 col-md-6 margin-bottom30">
						<img src="<c:url value='/resources/img/support/food/conv_store/cu02.png'/>" class="img-responsive"/>
					</div>
				</div>  
				
			</div>
		</div> <!--  #convStore END ... -->

	</div><!-- support body END... -->
</div>


<script type="text/javascript">
	
	$(function() {
		$("a[data-name='${type}']").click();
		
		if( '${type}'== "foodCourt") {
			$("a[data-name='gatz']").click();			
		}
		
	});
	
	
	$('#support-sub-nav .sub-nav .item').on('click',function(){
		var id = $(this).data('name');
		var id_selector = "#" + id;
		$('#support-sub-nav .sub-nav .item').removeClass('active');
		$(this).addClass('active');
		hideSVGMap(".tab-item.active");
		$('#support-body .tab-item').removeClass('active');
		$('#'+id+'.tab-item').addClass('active');
		
		switch(id_selector) {
			case "#foodCourt":	
				setDefaultSVGMap(id_selector, 2);
				break;
			case "#convStore":
				setDefaultSVGMap(id_selector, 3);
				break;
			default:
				setDefaultSVGMap(id_selector, 1);
				break;
		
		}
	});
	
	
	$('#foodCourt .foodCourt-restaurant-wrapper .sub-nav .item').on('click', function() {
		
		var id = $(this).data('name');
		$('#foodCourt .foodCourt-restaurant-wrapper .sub-nav .item').removeClass('active');
		$(this).addClass('active');
		$('#foodCourt .foodCourt-restaurant-wrapper .restaurant-item').removeClass('active');
		$('#'+id+'.restaurant-item').addClass('active');
		event.preventDefault();
	});
	
	// 각 편의시설별 지도 flash 해당 위치 표시
	$('#foodCourt').find("g[id='XMLID_158_']").attr('class', 'active'); // 푸드코트
	$('#cafe').find("g[id='XMLID_12_']").attr('class', 'active'); // 카페
	$('#convStore').find("g[id='XMLID_585_']").attr('class', 'active'); // 편의점
	
</script>