<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-green">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>
	<div id="display-body" class="sub-body">
		<div id="basic-info-spy" class="scrollspy">
			<div class="narrow-sub-top guide">
				<div class="sub-banner-tab-wrapper sub-banner-tab-green">
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<ul class="nav nav-tabs">
									<li class="col-xs-6 col-xs-offset-3 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 active">
										<a href = "javascript:void(0)" onclick="ChangeClass('CL6004')" class="item">온라인 예약 바로가기</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-title green">
							<div class="top-line"></div>
							체험전시물
						</h3>
					</div>
				</div>
				<div class="row description">
					<div class="col-md-12">
						<ul class="circle-list green">
							<li>
								예약 사전 안내
								<ul>
									<li>
										체험전시물은 온라인 60%(후원회원 포함), 현장 40%를 할당하여 예약 및 발권하오니, 가급적 온라인 예약을 활용하시기 바랍니다.
									</li>
									<li>
										( )안 시간대는 매주 주말, 공휴일 및 방학(1, 8월경)만 운영합니다.
									</li>
									<li>
										체험전시물은 단체예약을 받지 않습니다.
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
		<div id="reservation-guide-spy">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-title green">
							<div class="top-line"></div>
							전시관별 예약 안내
						</h3>
					</div>
				</div>
				<div class="row margin-bottom20">
					<div class="col-md-12 text-right">
						<a href="javascript:void(0)"
							onclick="ChangeClass('CL6004')"
							 class="reservation-btn">온라인 예약 바로가기</a>
						<a href="#" class="reservation-btn" data-toggle="modal" data-target="#ticket-on-site-modal">
							현장발권 안내
						</a>
					</div>
				</div>
				<div class="display-guide-wrapper">
					<div class="sub-nav">
						<div class="row">
							<div class="col-xs-6 col-sm-3 col-md-3">
								<a href="#" class="item active" data-name="basicScience">
									기초과학관
								</a>
							</div>
							<div class="col-xs-6 col-sm-3 col-md-3">
								<a href="#" class="item" data-name="sciencePark">
									어린이탐구체험관
								</a>
							</div>
							<div class="col-xs-6 col-sm-3 col-md-3">
								<a href="#" class="item" data-name="nobelPrizeAndI">
									노벨상과 나
								</a>
							</div>
							<div class="col-xs-6 col-sm-3 col-md-3">
								<a href="#" class="item" data-name="advancedTechnology1">
									첨단기술1관
								</a>
							</div>
							<div class="col-xs-6 col-sm-3 col-md-3">
								<a href="#" class="item" data-name="advancedTechnology2">
									첨단기술2관
								</a>
							</div>
							<div class="col-xs-6 col-sm-3 col-md-3">
								<a href="#" class="item" data-name="naturalHistory">
									자연사관
								</a>
							</div>
							<div class="col-xs-6 col-sm-3 col-md-3">
								<a href="#" class="item" data-name="traditionalScience">
									전통과학관
								</a>
							</div>
						</div>
					</div>
					<!--  전시관별 이용안내 상세 -->
					<div id="guide-items">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- 현장발권 안내 Modal -->
<div class="modal fade" id="ticket-on-site-modal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" 
					aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">현장발권 시스템 안내</h4>
			</div>
			<div class="modal-body">
				<ul class="circle-list teal">
					<li>
						체험 전시물 이용 예매는 편리한 현장 발권 시스템을 사용하시기 바랍니다.
						<img src="<c:url value='/resources/img/display/experience/spot_ticket.jpg' />" class="img-responsive" alt="현장 발권 시스템">
					</li>
				</ul>
			  	<ul class="circle-list teal">
			  		<li>
			  			안내 사항
			  			<ul>
			  				<li>
			  					발권기 사용은 당일 선착순으로 이용 가능합니다.
			  				</li>
			  				<li>
			  					1인 1매 발권 가능합니다.<br>
			  					*무료 체험이므로 다른 사람들도 함께 체험할 수 있도록 배려하여 주시기 바랍니다.
			  				</li>
			  				<li>
			  					발권은 오전 9시30분 1일 1회 오픈됩니다.
			  				</li>
			  			</ul>
			  		</li>
			  		<li>
			  			운영 시간
			  			<ul>
			  				<li>
			  					오전 9시 30분부터 예약 시작<br>
			  					*당일 오픈 체험 전시물을 이용하실 수 있습니다.
			  				</li>
			  			</ul>
			  		</li>
			  		<li>
			  			위치 안내
			  			<ul>
			  				<li>
			  					과학관 1층 중앙홀 커피숍 오른쪽으로 가시면 현장발권기를 찾으실 수 있습니다.
			  				</li>
			  			</ul>
			  			<img src="<c:url value='/resources/img/display/experience/spot_ticket_position.jpg' />" class="img-responsive" alt="현장 발권기 위치">
			  		</li>
			  	</ul>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">닫기</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function() {
		// 전시관별 이용안내 tab click event
		changeGuideTab();
		// 전시관별 이용안내 첫 tab click
		$("a[data-name='basicScience']").click();
		
		
	});
	
	//이미지 슬라이더
	//PTechSlider('#image-slider').autoSlide();
	PTechSlider('#image-slider',
		{
		slidesPerScreen: 4,
		indicators: true
		}		
	);
	
	// 전시관별 이용안내 click event
	function changeGuideTab() {
		$('.display-guide-wrapper .sub-nav .item').on('click', function() {

			var id = $(this).data('name');
			$('.display-guide-wrapper .sub-nav .item').removeClass('active');
			$(this).addClass('active');
			$('.display-guide-wrapper .guide-item').removeClass('active');
			$('#' + id + '.guide-item').addClass('active');
			event.preventDefault();
		});
	}
</script>

<script id="guide-item-template" type="text/x-handlebars-template">
{{#each guideItems}}
<div id="{{name}}" class="guide-item">
	<ul class="circle-list teal">
		<li>
			{{koreanName}}<span class="med-gray text-right"></span>
		</li>
	</ul>
	<div class="table-responsive">
	<table class="table table-bordered centered-table">
		<thead>
			<tr>
				<th width="20%">
					체험전시물
				</th>
				<th width="20%">
					운영시간
				</th>
				<th width="40%">
					이용신청방법
				</th>
				<th width="20%">
					이용 제한
				</th>
			</tr>
		</thead>
		<tbody>
			{{#each exhibitions}}
				<tr>
					<td>
						{{{name}}}
					</td>
					<td>
						{{{time}}}
					</td>
					<td>
						{{{howto}}}
					</td>
					<td>
						{{{limit}}}
					</td>
				</tr>
			{{/each}}
		</tbody>
	</table>
	</div>
	<div class="med-gray">
		문의 : {{call}}
	</div>
</div>
{{/each}}
</script> 


<form name="frmIndex">
	<input type="hidden" name="ACADEMY_CD" value="ACD006" />
	<input type="hidden" name="COURSE_CD" value="" />
	<input type="hidden" name="SEMESTER_CD" value="" />
	<input type="hidden" name="TYPE" value="" />
	<input type="hidden" name="FLAG" value="" />
	<input type="hidden" name="CLASS_CD" value="CL6004" />
</form>


<script type="text/javascript">
	var guideItems = [
		{
			name: "basicScience",
			koreanName: "기초과학관",
			exhibitions: [
				{
					name: "태풍체험<br>(20분 운영)",
					time: "10:30, 11:30, 13:30, (14:30), 15:30, 16:30",
					howto: "1회 15명",
					limit: "초등학생 이상 (성인가능)<br>(120cm 이상)"
				},
				{
					name: "지진체험<br>(15분 운영)",
					time: "10:00, 11:00, 13:00, 14:00, 15:00, 16:00, (17:00)",
					howto: "1회 15명",
					limit: "초등학생 이상 (성인가능)<br>(120cm 이상)"
				},
				{
					name: "정전기 체험<br>(20분 운영)",
					time: "10:40,13:40",
					howto: "1회 10명",
					limit: "초등학생 3학년 이상"
				},
				{
					name: "테슬라코일<br>(5분 운영)",
					time: "10:15, 11:15, 12:15, 13:15, 15:15, 16:15, 17:15",
					howto: "자유관람",
					limit: "연령 제한 없음"
				},
				{
					name: "남극 세종과학기지 연구원과의 화상통화<br>(극지체험실)",
					time: "매월 2,4주 일요일, 10:30~11:00",
					howto: "1회 10명(현장 선착순) (*기초과학관데스크에 참여신청)<br>Tip : 미리 예상질문을 생각해옵니다.<br>남극세종과학기지의 사정에 의해 일정이 변경될 수 있습니다.",
					limit: "7세 이상"
				}
			],
			call: "02-3677-1500"
		},
		{
			name: "sciencePark",
			koreanName: "어린이탐구체험관",
			exhibitions: [
				{
					name: "3D영상관<br>(최대 15분 소요)",
					time: "10:00(A), (11:00(A)), 14:00(B), 15:00(B), 16:00(C), 17:00(C)<br>(A) 쌀왕국과 황금들판<br>(B) 샌디의 공룡알 구출작전<br>(C) 로켓의 발사",
					howto: "1회 30명",
					limit: "5세 ~ 10세"
				},
				{
					name: "인체탐험 매직버스<br>(10분 소요)",
					time: "10:00, 11:00, 13:30, (14:30), 15:30, 16:30",
					howto: "1회 20명",
					limit: "초등 1 ~ 3학년 신장 120~150cm (신장 측정 후 입장)"
				}
			],
			call: "02-3677-1500"
		},
		{
			name: "nobelPrizeAndI",
			koreanName: "노벨상과 나",
			exhibitions: [
				{
					name: "노벨자동차경주<br>(5분 소요/인)",
					time: "10:30, 15:30, 16:30",
					howto: "1회 4명 (현장 선착순)<br>※체험물 앞",
					limit: "초등 3학년 이상<br>(키 130cm이상)"
				}
			],
			call: "02-3677-1500"
		},
		{
			name: "advancedTechnology1",
			koreanName: "첨단기술1관",
			exhibitions: [
				{
					name: "뇌파는 마술사<br>(5분 소요/인)",
					time: "10:00, 11:00, 13:00, (15:00), 16:00",
					howto: "1회 20명",
					limit: "초등 ~ 성인"
				},
				{
					name: "마인드레이싱카<br>(3분 소요/인)",
					time: "10:00, 11:00, 13:00, (15:00), 16:00",
					howto: "1회 12명",
					limit: "초등 ~ 성인"
				},
				{
					name: "알파경주<br>(3분 소요/인)",
					time: "10:30, 11:30, 13:30, (15:30), 16:30",
					howto: "1회 24명",
					limit: "초등 ~ 성인"
				},
				{
					name: "로봇댄스공연<br>(15분 운영/회)",
					time: "10:30, 11:30, 13:30, 14:30, 15:30, 16:30",
					howto: "자유관람",
					limit: "1회 250명"
				},
				{
					name: "생명과학실험실",
					time: "교차진행<br>주중 30분씩,일4회<br>11:00(초파리)<br>14:00(지렁이)<br>15:00(초파리)<br>16:00(지렁이)",
					howto: "1회 20명 (인터넷 사전예약 우선, 잔여분 현장 접수 가능)",
					limit: "초등2~중학생"
				}
			],
			call: "02-3677-1500"
		},
		{
			name: "advancedTechnology2",
			koreanName: "첨단기술2관",
			exhibitions: [
				{
					name: "우주여행극장<br>(15분 운영/회)",
					time: "(10:00), 10:30, (11:00), 11:30, 13:00, 13:30, 15:30, 16:30",
					howto: "1회 30명",
					limit: "초등~성인<br>(120cm 이상)"
				},
				{
					name: "자이로스코프<br>(5분 소요/인)",
					time: "10:00, 11:00, 15:30, 16:30",
					howto: "1회 9명",
					limit: "초등~성인<br>130cm 이상<br>60kg 미만"
				},
				{
					name: "유인조종장치 (MMU)<br>(5분 소요/인)",
					time: "10:00, 11:00, 15:30, 16:30",
					howto: "1회 8명",
					limit: "초등~성인<br>130cm 이상<br>60kg 미만"
				},
				{
					name: "월면점프<br>(5분 소요/인)",
					time: "10:30, 11:30, 13:30, 16:00",
					howto: "1회 8명",
					limit: "초등~성인<br>130cm 이상<br>60kg 미만"
				},
				{
					name: "항공기 시뮬레이터<br>(10분 소요/인)",
					time: "10:30, 11:30, 13:30, 16:00",
					howto: "1회 6명",
					limit: "초등~성인, 140cm 이상"
				},
				{
					name: "행글라이더 체험<br>(1분 소요/인)",
					time: "10:00, 11:00, 15:30, 16:30",
					howto: "1회 10명",
					limit: "5세 이상"
				}
			],
			call: "02-3677-1500"
		},
		{
			name: "naturalHistory",
			koreanName: "자연사관",
			exhibitions: [
				{
					name: "생동하는 지구 (S.O.S)<br>(15분 소요/회)",
					time: "(10:00), 10:30, (11:00), 11:30, 13:00, 13:30, 15:30, 16:30",
					howto: "1회 40명",
					limit: "초등 4학년 이상"
				},
				{
					name: "수족관 뒤에는 무슨 일이!<br>(15분 소요)",
					time: "(11:00)<br>방학기간도 주말 및 공휴일 한정",
					howto: "1회 15명",
					limit: "초등 1학년 이상"
				}
				
			],
			call: "02-3677-1500"
		},
		{
			name: "traditionalScience",
			koreanName: "전통과학관",
			exhibitions: [
				{
					name: "한의학 체험 (맥파분석)<br>(5분 소요/인)",
					time: "10:00, 15:00",
					howto: "1회 5명",
					limit: "중학생 이상"
				},
				{
					name: "한의학 체험 (사상체질진단)<br>(5분 소요/인)",
					time: "11:00, 16:00",
					howto: "1회 5명",
					limit: "중학생 이상"
				}
				
			],
			call: "02-3677-1500"
		}
	];
	
	var guideItemSource = $("#guide-item-template").html();
	var guideItemTemplate = Handlebars.compile(guideItemSource);
	var guideItemHtml = guideItemTemplate({guideItems: guideItems});
	$("#guide-items").html(guideItemHtml);
	
	
	function ChangeClass(Cd){
		var frm = document.frmIndex;
		frm.CLASS_CD.value = Cd;
		frm.action = '<c:url value="/schedules"/>?${_csrf.parameterName}=${_csrf.token}';
		frm.method = 'post';
		frm.submit();
	}
</script>