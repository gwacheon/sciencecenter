<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-green">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>
	
	<div id="support-body" class="sub-body">
		<div id="basic-guide-spy" class="narrow-sub-top guide">
			<div class="sub-banner-tab-wrapper sub-banner-tab-green">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<ul class="nav nav-tabs">
								<li class="col-xs-6 col-xs-offset-3 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 active">
									<a href = "javascript:void(0)" onclick="ChangeClass('CL6003')" class="item">전시해설 예매</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>			
		</div>
		
		
		<div  class="container"> 
			<div id="#basic-guide-spy">
				<div class="row">
					<div class="col-md-12">
						<h3 class="page-title green">
							<div class="top-line"></div>     
							도슨트 전문 해설
						</h3>
					</div>
				</div>
				<div class="row margin-bottom20">
					<div class="col-md-12">
						<p>
							도슨트 전문해설은 「우주」, 「옷」 등 색다른 주제를 가지고 상설전시관 전역을 둘러보는 해설 프로그램으로,<br>
							온라인 사전예약을 통해 하루에 2~3회씩 운영되는 유료서비스입니다.
						</p>
						<ul class="circle-list green text-bold item-margin-bottom">
							<li>
								참가대상
								<ul>
									<li>
										만 10세 이상 (7~9세의 경우, 성인보호자 동반시 참가 가능)
									</li>
								</ul>
							</li>
							<li>
								참가인원
								<ul>
									<li>
										코스당 20명 
									</li>
								</ul>
							</li>
							<li>
								진행시간
								<ul>
									<li>
										일 2~3회 
									</li>
									<li>
										<span class="color-red">※ 신청인원에 따라 코스가 1개 혹은 2개로 운영됩니다.</span>
									</li>
									<li>
										<span class="color-red">※ 현장 상황에 따라 당일 해설 코스는 변경 될 수 있습니다.</span> 
									</li>
									<li>
										<span class="color-red">※ 체험물의 경우 안전을 위해 제한사항이 있을 수 있습니다.</span>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered centered-table">
								<thead>
									<tr>
										<th>
											회차
										</th>
										<th>
											시간
										</th>
										<th>
											코스										
										</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											1
										</td>
										<td>
											10:30 ~ 12:00
										</td>
										<td>
											A 코스(우주) / B 코스(옷) / C코스(뇌)
										</td>
									</tr>
									<tr>
										<td>
											2
										</td>
										<td>
											13:30 ~ 15:00
										</td>
										<td>
											A 코스(우주) / B 코스(옷) / C코스(뇌)
										</td>
									</tr>
									<tr>
										<td>
											3
										</td>
										<td>
											15:30 ~ 17:00
										</td>
										<td>
											A 코스(우주) / B 코스(옷) / C코스(뇌)
										</td>
									</tr>
								</tbody>
							</table>
						</div>	<!--  table-responsive END ... -->
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<ul class="circle-list green text-bold item-margin-bottom ">
							<li>
								해설주제 및 주요 코스
								<ul class="non-bullet text-color teal">
									<li>
										A. 지구인의 우주생활 (자연사관-첨단기술2관)
										<ul class="dashed-list">
											<li>
												- 지구와는 다른 우주의 특징을 알아보고, 우주에서 생활하기 위한 전략과 그 안에 숨은 과학 원리를 찾아본다.
											</li>
										</ul>
									</li>
									<li>
										B. 우리 몸을 보호해요, 옷! (자연사관-전통과학관-첨단기술2관)
										<ul class="dashed-list">
											<li>
												- 몸을 보호하기 위한 동물들의 전략들을 관찰해 보고, 인간이 옷을 발달시킨 역사적 과정 및 과학기술의 적용 원리를 이해한다.
											</li>
										</ul>
									</li>
									<li>
										C. 인간의 뇌와 인공지능 (기초과학관-어린이탐구체험관-첨단기술1관)
										<ul class="dashed-list">
											<li>
												- 뇌의 기능에 관하여 살펴보고 뇌가 착각하는 것들이 무엇이 있는지 알아보는 과정이다. 그리고 최근 이슈가 되고 있는 인공지능에 관해 이야기해본다.
											</li>
										</ul>
									</li>
								</ul>
							</li>
							<li>
								참가비용
								<ul>
									<li>
										1인당 1,000원
									</li>
								</ul>
							</li>
							<li>
								참가방법
								<ul>
									<li>
										과학관 홈페이지 내 온라인 예약(1일 전 마감) 및 결제(신용카드 온라인 결제만 가능) 후 현장 참가
										<div class="row">
											<div class="col-sm-12 col-md-4 col-lg-3">								
												<div class="process single-line">
													<div class="text">
														홈페이지 예약 신청
													</div>
													<i class="fa fa-angle-right fa-lg" aria-hidden="true"></i>
												</div>
											</div>
											<div class="col-sm-12 col-md-4 col-lg-3">								
												<div class="process ex-color single-line">
													<div class="text">
														온라인 신용카드 결제
													</div>
													<i class="fa fa-angle-right fa-lg" aria-hidden="true"></i>
												</div>
											</div>
											<div class="col-sm-12 col-md-4 col-lg-3">								
												<div class="process single-line">
													<div class="text">
														현장 방문 및 해설 참가
													</div>
													<i class="fa fa-angle-right fa-lg" aria-hidden="true"></i>
												</div>
											</div>
										</div>
									</li>
								</ul>
								<ul class="non-bullet">
									<li>
										① 홈페이지 로그인
									</li>
									<li>
										② 도슨트전문해설 탭에서 원하는 날짜와 회차를 선택하여 예약
									</li>
									<li>
										③ 홈페이지 마이페이지에서 신용카드로 온라인 결제 
									</li>
									<li>
										④ 예약 접수 완료 확인 <span class="color-red">(*결제를 하지 않고, 예약만 하고 오시면 참가 불가합니다. 현장 예약 및 결제 불가!!)</span>
									</li>
									<li>
										⑤ 중앙홀 해설데스크 방문하여 예약자 명단 확인
									</li>
									<li>
										⑥ 해설 프로그램 참가
									</li>
									<li>
										<span class="color-red">※ 아래 예약 관련 사항을 필히 확인하시고, 예약 및 결제 진행해주시기 바랍니다.</span>
									</li>
									<li>
										<span class="color-red">※ 모임장소 : 중앙홀 해설데스크 (상행 에스컬레이터 뒤)</span>
									</li>
								</ul>
							</li>
							<li>
								예약 접수
								<ul>
									<li>
										홈페이지에서 예약 신청 후 온라인 결제까지 완료해야 예약이 접수됩니다.
									</li>
									<li>
										예약 신청 당일에 온라인 결제를 완료하지 않으면 자동으로 예약 취소됩니다.
									</li>
									<li>
										예약 접수 및 결제 가능한 시간은 7일 전 00:00 ~ 1일 전 20:00입니다.
									</li>
									<li>
										홈페이지 신청은 로그인 후 이용가능하며, 회원 1인당 20명까지 접수 가능합니다.
									</li>
									<li>
										결제는 예약 후 홈페이지 '마이페이지'에서 온라인 신용카드 결제만 가능합니다.
									</li>
									<li>
										해설 참가 비용은 1인당 1,000원이며, 상설전시관 입장료는 별도(성인 4,000원, 청소년 2,000원)입니다.
									</li>
									<li>
										단체 문의 : 02-3677-1403
									</li>
								</ul>
							</li>
							<li>
								예약 취소 및 환불
								<ul>
									<li>
										홈페이지 ‘마이페이지’에서 예약 접수 상황을 확인할 수 있으며, 홈페이지에서 예약을 취소하면 카드 결제도 자동 취소됩니다.
									</li>
									<li>
										예약‧결제 취소는 1일 전 23:59까지 가능하며, 해설 당일에는 취소 및 환불이 불가합니다.
									</li>
									<li>
										예약 취소 후에 재접수 할 경우에는 신청 및 결제를 처음부터 다시 진행해야 합니다.
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<form name="frmIndex">
	<input type="hidden" name="ACADEMY_CD" value="ACD006" />
	<input type="hidden" name="COURSE_CD" value="" />
	<input type="hidden" name="SEMESTER_CD" value="" />
	<input type="hidden" name="TYPE" value="" />
	<input type="hidden" name="FLAG" value="" />
	<input type="hidden" name="CLASS_CD" value="CL6003" />
</form>

<script type="text/javascript">
	function ChangeClass(Cd){
		var frm = document.frmIndex;
		frm.CLASS_CD.value = Cd;
		frm.action = '<c:url value="/schedules"/>?${_csrf.parameterName}=${_csrf.token}';
		frm.method = 'post';
		frm.submit();
	}
</script>