<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="under-construction-wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-4">
				<div class="image">
				</div>
			</div>
			<div class="col-md-8">
				<div class="text">
					<div class="header">
						<span class="emphasis">페이지준비중</span> 입니다.
					</div>
					<div class="value">
						현재 페이지는 <span class="emphasis">업데이트</span> 중입니다.
						빠른 시일 내에 찾아뵙도록 하겠습니다.<br>
						감사합니다. 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>