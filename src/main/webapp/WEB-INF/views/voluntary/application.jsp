<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-purple">
		<nav id="scrollspy-nav" class="navbar-default navbar-static has-scroll">
			<div class="container">
				<ul class="nav navbar-nav">
					<c:import url="/WEB-INF/views/schedules/navbar.jsp"/>
					<li>
						<a href="#basic-info-spy" class="item">
							신청안내
						</a>				
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<div class="bread-crumbs">
							예약/신청 / 자원봉사
						</div>
					</li>
				
				</ul>
			</div>
		</nav>
	</div>

	<div class="sub-body">
		<div id="basic-info-spy" class="narrow-sub-top volunteer">
			<div class="container">
				<h4 class="doc-title">
					자원봉사 신청
				</h4>
				<h6 class="doc-title exp-desc">
					국립과천과학관에서는 학생 및 일반인을 대상으로 자원봉사자를 모집합니다.<br>
					국립과천과학관에서 다양한 봉사활동을 함께 하실 분들은 많은 지원 부탁드립니다.
				</h6>
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<ul class="nav nav-tabs">
							<li class="col-xs-6">
								<a class="item" href="<c:url value='/voluntary'/>">신청안내 (학생/일반)</a>
							</li>
	        				<li class="col-xs-6 active">
	        					<a class="item" href="<c:url value='/voluntary/application'/>" >자원봉사 신청 (학생)</a>
        					</li>		
						</ul>
					</div>							
				</div>
			</div>	
		</div>
	</div>
	
	<div class="container">
		<div id="intro">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title purple">
						<div class="top-line"></div>
						자원봉사 신청(학생)
					</h3>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<h4 class="page-sub-title black-font">
						<strong class="voluntary-accent">&middot; STEP 01</strong> 프로그램 선택
						<small>(프로그램을 선택하시면 신청 가능한 목록이 표시됩니다.)</small>
					</h4>
					
					<div class="table-responsive">
						<table class="table centered-table table-purple voluntary-table">
							<thead>
								<tr>
									<th>수강신청</th>
									<th>기간</th>
									<th>프로그램명</th>
									<th>장소</th>
									<th>상세/예약</th>
								</tr>
							</thead>
							
							<c:forEach var="voluntary" items="${voluntaries }">
								<tr>
									<td>접수중</td>
									<td>
										<fmt:formatDate pattern="yyyy-MM-dd" value="${voluntary.beginDate}" />
										~
										<fmt:formatDate pattern="yyyy-MM-dd" value="${voluntary.endDate}" />
									</td>
									<td>
										${voluntary.title }
									</td>
									<td>
										${voluntary.location }
									</td>
									<td>
										<a href="#" class="btn select-btn btn-purple" data-id="${voluntary.id }">선택하기</a>
									</td>
								</tr>
							</c:forEach>
						</table>
					</div>
					
					<h4 class="page-sub-title black-font">
						<strong class="voluntary-accent">&middot; STEP 02</strong> 날짜 및 시간 선택
					</h4>
					
					<div class="table-responsive">
						<table class="table centered-table table-purple voluntary-table">
							<thead>
								<tr>
									<th>날짜</th>
									<th>시간</th>
									<th>신청인원 / 정원</th>
									<th>선택</th>
								</tr>
							</thead>
							<tbody id="voluntary-times">
								
							</tbody>
						</table>
					</div>
					
					<div class="row">
						<form id="applicationForm" class="col-md-12 purple" style="display: none">
							<h4 class="page-sub-title black-font">
								<strong class="voluntary-accent">&middot; STEP 03</strong> 신청서 작성
							</h4>
							
							<input type="hidden" id="voluntary_time_id" name="voluntary-time-id">
							<input type="hidden" id="volunteer_id" name="id">
							
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label for="name">이름</label>
										<input id="name" type="text" class="form-control" name="name"
											value="${currentMember.memberName }">
									</div>
								</div>
								
								<div class="col-md-4">
									<div class="form-group">
										<label for="phone">휴대폰</label>
										<input id="phone" type="text" class="form-control" name="phone"
											value="${currentMember.memberCel }">
									</div>
								</div>
								
								<div class="col-md-4">
									<div class="form-group">
										<label for="email">이메일</label>
										<input id="email" type="text" class="form-control" name="email"
											value="${currentMember.memberEmail }">
									</div>
								</div>
								
								<div class="col-md-4">
									<div class="form-group">
										<label for="school">학교명</label>
										<input id="school" type="text" class="form-control" name="school">
									</div>
								</div>
								
								<div class="col-md-2 col-xs-3">
									<div class="form-group">
										<label for="grade">학년</label>
										<input id="grade" type="text" class="form-control" name="grade">
									</div>
								</div>
								
								<div class="col-md-2 col-xs-3">
									<div class="form-group">
										<label for="group_no">반</label>
										<input id="group_no" type="text" class="form-control" name="group_no">
									</div>
								</div>
								
								<div class="col-md-2 col-xs-3">
									<div class="form-group">
										<label for="my_no">번호</label>
										<input id="my_no" type="text" class="form-control" name="my_no">
									</div>
								</div>
								
								<div class="col-md-2 col-xs-3">
									<div class="form-group">
										<label for="other_id">1365 ID</label>
										<input id="other_id" type="text" class="form-control" name="other_id">
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label for="introduction">지원동기</label>
										<textarea id="introduction" class="form-control" rows="10" name="introduction"></textarea>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<button id="submit-application-docs" type="submit" class="btn applicant-btn btn-purple right">
										신청서 등록하기
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(".select-btn").click(function(e){
		e.preventDefault();
		var id = $(this).data('id');
		
		$.ajax({
			url: baseUrl + "voluntary/" + id + "/times",
			type: "GET",
			success: function(data){
				if(data.voluntary.voluntaryTimes == null || data.voluntary.voluntaryTimes.length == 0){
					alert("현재 접수신청 가능한 시간대가 없습니다.");
				}else{
					var limitTime = moment(data.currentTime).add(data.voluntary.reserveAvailableDay, 'd');
					var times = [];
					if(data.voluntary != null && data.voluntary.voluntaryTimes != null){
						_.forEach(data.voluntary.voluntaryTimes, function(vTime, key){
							var timeObj = {}
							timeObj.id = vTime.id;
							timeObj.beginDate = moment(vTime.beginTime).format("YYYY-MM-DD");
							timeObj.beginTime = moment(vTime.beginTime).format("HH:mm");
							timeObj.endTime = moment(vTime.endTime).format("HH:mm");
							timeObj.applicationMembers = vTime.applicationMembers;
							timeObj.waitMember = data.voluntary.waitMember;
							timeObj.maxMember = data.voluntary.maxMember;
							
							if(moment(timeObj.beginDate).isBefore(limitTime)){
								timeObj.available = true;		
							}else{
								timeObj.available = false;
							}
							
							times.push(timeObj);
						});
					}
					
					var source = $("#times-template").html();
					var template = Handlebars.compile(source);
					var html = template({times: times});
					$("#voluntary-times").html(html);
					
					$("#applicationForm")[0].reset();
					$("#applicationForm").hide();	
				}
			}
		});
	});
	
	$(document).on("click", ".select-time-btn", function(e){
		e.preventDefault();
		var timeId = $(this).data('id');
		$("#voluntary_time_id").val(timeId);
		$("#applicationForm").show();
	});
	
	$(document).on("click", "#submit-application-docs", function(e){
		e.preventDefault();
		
		var timeId = $("#voluntary_time_id").val();
		
		var volunteerInfo = {
			name: $("#name").val(),
			phone: $("#phone").val(),
			email: $("#email").val(),
			school: $("#school").val(),
			grade: $("#grade").val(),
			group_no: $("#group_no").val(),
			my_no: $("#my_no").val(),
			other_id: $("#other_id").val(),
			introduction: $("#introduction").val()
		};
		
		$.ajax({
			url: baseUrl + "voluntary/application/" + timeId,
			beforeSend: function(xhr) {
				ajaxLoading();
	            xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));
	        },
	        contentType : 'application/json',
	        type: "POST",
	        dataType: "JSON",
	        data: JSON.stringify(volunteerInfo),
	        success: function(data){
	        	ajaxUnLoading();
	        	$("#applicationForm")[0].reset();
				$("#applicationForm").hide();
				
				console.log(data);
				
	        	if(data.volunteer != null && data.volunteer.reserved){
	        		alert("이미 접수처리 된 봉사활동 입니다. Mypage에서 신청 내역을 확인하시기 바랍니다.")
	        		location.href = baseUrl + "mypage/voluntaries";
	        	} else if (data.volunteer != null && !data.volunteer.reserved) {
	        		alert("정상 접수처리 되었습니다. 신청 결과는 홈페이지의 Mypage에서 확인할 수 있습니다.");
	        		location.href = baseUrl + "mypage/voluntaries";
	        	} else {
	        		alert("시스템 문제로 정상처리 되지 못하였습니다. 계속 문제 발생 시 시스템 관리자에게 문의 바랍니다.");
	        	}
	        	
	        	
	        	
	        	
	        },
	        error: function(err){
	        	ajaxUnLoading();
	        	console.log(err);
	        }
		});
	});
</script>

<script id="times-template" type="text/x-handlebars-template">
{{#each times}}
	<tr>
		<td>{{this.beginDate}}</td>
		<td>{{this.beginTime}} ~ {{this.endTime}}</td>
		<td>{{this.applicationMembers}} / {{this.waitMember}}</td>
		<td>
			{{#if this.available}}
				<a href="#" class="btn btn-purple select-time-btn available" data-id="{{id}}">신청하기</a>
			{{else}}
				
			{{/if}}
		</td>
	</tr>
{{/each}}
</script>