<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="sub-content-nav scrollspy-orange">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>

<div class="sub-body">
	<div id="basic-info-spy" class="narrow-sub-top communication">
		<div class="sub-banner-tab-wrapper sub-banner-tab-orange">
			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="support-sub-nav">
		<div class="container">
		   	<div class="sub-nav convenience">
				<div class="row">	
			   		<c:forEach var="evaluation" items="${supportEvaluations}" varStatus="status">
			     		<div class="col-xs-6 col-sm-3 col-md-2">
				     		<a href="#"  class="item" data-name="eval-${status.index }">
				     			<c:out value='${evaluation.year }' />
				     			<c:out value="${evaluation.seq }" />
				     		</a> 
			     		</div>	
			   		</c:forEach>
			     </div>				  
			</div>
		</div>
	</div> 
	<div id="support-body" class="margin-bottom30">
		<div class="container">
			<c:forEach var="evaluation" items="${supportEvaluations }" varStatus="status" >
				<div id="eval-${status.index }"class="section tab-item">
					<div class="row">
						<div class="col-md-12">
							<h3 class="page-title orange">
								<div class="top-line"></div>      
								<c:out value="${evaluation.year }" />
								<c:out value="${evaluation.seq }" /> 
								평가 결과
							</h3>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div>
								우리 과학관의 모든 임직원들은, 대한민국의 모든 국민이 즐겁게 과학문화 생활을 누릴 수 있도록
								최상의 서비스를 제공할 것을 약속드립니다.  
							</div>
						</div>
					</div>
					<div class="sub-section">
						<ul class="circle-list orange sub-section-title">
							<li>
								<c:out value="${evaluation.year }" />
								<c:out value="${evaluation.seq }" />
							 	국립과천과학과 고객서비스헌장의 서비스 실천 이행에 관한 평가결과를 알려드립니다.
							</li>
						</ul>
						<div class="content">
							<div class="row">
								<div class="col-md-12 content-title-wrapper">
									<ul class="arrow-list orange content-title">
										<li>
											평가대상기간
											<ul>
												<li>
													<c:out value="${evaluation.duration }" />
												</li>
											</ul>		
										</li>
										<li>
											평가자
											<ul>
												<li>
													<c:out value="${evaluation.evaluator }" />
												</li>
											</ul>			
										</li>
										<li>
											평가점수
											<ul>
												<li>
													<c:out value="${evaluation.totalScore }" />
												</li>
											</ul>			
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 table-responsive">
								<c:out value="${evaluation.htmlData }" escapeXml="false" />													
							</div>
							<div class="col-md-12">
								<div class="table-extra-info">
									점수환산기준 : 매우우수(100점), 우수(90점), 보통(80점), 미흡(70점), 불량(60점)
								</div>
							</div>
						</div>
					</div>
				</div>
			</c:forEach>
		</div>
	</div>
</div>


<script type="text/javascript">
	
	$(function() {
		$("a[data-name='eval-0']").click();
	});

	$('#support-sub-nav .sub-nav .item').on('click',function() {
		var id = $(this).data('name');
		$('#support-sub-nav .sub-nav .item').removeClass('active');
		$(this).addClass('active');
		$('#support-body .tab-item').removeClass('active');
		$('#'+id+'.tab-item').addClass('active');
	});
	
	$('table').addClass("table").addClass("centered-table").addClass("table-bordered");
	
</script>