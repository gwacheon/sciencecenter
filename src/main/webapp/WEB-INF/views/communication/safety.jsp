<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="sub-content-nav scrollspy-orange">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>


<div class="sub-body">
	<div id="support-body">
		<div id="basic-info-spy" class="narrow-sub-top communication">
			<div class="sub-banner-tab-wrapper sub-banner-tab-orange">
				<div class="container">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div id="evacuation-spy" class="scrollspy">
				<div class="row">
					<div class="col-md-12">
						<h3 class="page-title orange">
							<div class="top-line"></div>     
							관람객 대피 (비상대피로 안내)
						</h3>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<ul class="circle-list orange text-color">
							<li>
								1층 비상대피로
							</li>
						</ul>						
						<ul class="dash-list">
							<li>
								중앙홀 정문 및 후문, 어울림홀 입구, 상상홀 입구, 교육관 입구 통로를 이용하여 외부로 대피									
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<img src="<c:url value='../img/support/safety/exit_1F.png' />" class="img-responsive"/>					
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 safety-image">
						<img src="<c:url value='../img/support/safety/exit_1F_1.png' />" class="img-responsive"/>		
					</div>
					<div class="col-md-6 safety-image">
						<img src="<c:url value='../img/support/safety/exit_1F_2.png' />" class="img-responsive"/>		
					</div>
					<div class="col-md-6 safety-image">
						<img src="<c:url value='../img/support/safety/exit_1F_3.png' />" class="img-responsive"/>		
					</div>
					<div class="col-md-6 safety-image">
						<img src="<c:url value='../img/support/safety/exit_1F_4.png' />" class="img-responsive"/>		
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<ul class="circle-list orange text-color">
							<li>
								2층 비상대피로
							</li>
						</ul>
						<ul class="dash-list">
							<li>
								중앙홀 에스컬레이터 양측 계단, 교육관 비상계단, 어울림홀 비상계단, 첨단기술2관 에스컬레이터
								계단을 이용하여 내려간 후 외부로 대피	
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<img src="<c:url value='../img/support/safety/exit_2F.png' />" class="img-responsive"/>
					</div>
				</div>				
				<div class="row">
					<div class="col-md-6 safety-image">
						<img src="<c:url value='../img/support/safety/exit_2F_1.png' />" class="img-responsive"/>		
					</div>
					<div class="col-md-6 safety-image">
						<img src="<c:url value='../img/support/safety/exit_2F_2.png' />" class="img-responsive"/>		
					</div>
					<div class="col-md-6 safety-image">
						<img src="<c:url value='../img/support/safety/exit_2F_3.png' />" class="img-responsive"/>		
					</div>
					<div class="col-md-6 safety-image">
						<img src="<c:url value='../img/support/safety/exit_2F_4.png' />" class="img-responsive"/>		
					</div>
				</div>
			</div><!-- evacuation-spy END ... -->
			<div id="rule-spy" class="scrollspy">
				<div class="row">
					<div class="col-md-12">
						<h3 class="page-title orange">
							<div class="top-line"></div>     
							안전사고 수칙
						</h3>
					</div>
				</div>
				<div>
					쾌적하고 안전한 과학관 관람을 위해 함께 지켜야 할 사항을 안내해 드리며
					단체학생들에게 관람점 교육을 통하여 안전한 관람이 될 수 있도록 많은 협조 부탁드립니다.
				</div>
				<div class="row">
					<div class="col-md-12">
						<ul class="circle-list orange text-color">
							<li>
								과학관(전시관내)에서 뛰어다니는 행동금지!
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<ul class="dash-list">
							<li>
								체험학습으로 방문시 과제를 부여받은 학생들이 실내 공간에서 뛰어다니는 경우,
								다른 관람객과 부딪혀 다치는 등 사고 가능성이 높습니다.
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<ul class="circle-list orange text-color">
							<li>
								에스컬레이터 안전수칙 "꼭" 지키기!
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<ul class="dash-list">
							<li>
								에스컬레이터 서서 손잡이 잡고 타기
							</li>
							<li>
								뛰거나 걸어서 올라가고 내려오는 행동 하지 않기
							</li>
							<li>
								역주행하는 행동 절대하지 않기
							</li>
						</ul>
					</div>
				</div>		
				<div class="row">
					<div class="col-md-12">
						<ul class="circle-list orange text-color">
							<li>
								유아단체의 경우 에스컬레이터 이용을 삼가주시기 바랍니다.
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<ul class="dash-list">
							<li>
								아이들이 에스컬레이터 이용 중 넘어지면 매우 위험하오니,
								에스컬레이터 옆 계단이나 좌우측에 있는 엘레베이터를 통해 아이들이 
								층간 이동을 할 수 있도록 부탁드립니다.
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<ul class="circle-list orange text-color">
							<li>
								관내 상해사고 발생 시를 대비하여 구급실을 운영하고 있습니다.
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<ul class="dash-list">
							<li>
								사고발생 후 당일 구급실 진료기록 없이 귀가하는 경우,
								보험사 조사과정에서 당사자에게 불리하게 작용될 수도 있습니다.
							</li>
						</ul>					
					</div>
				</div>						
				
				
				<div class="row">
					<div class="col-md-12">
						<ul class="circle-list orange text-color">
							<li>
								안전관리 전담직원을 운영하고 있습니다.
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<ul class="dash-list">
							<li>
								사고발생시 신속한 조치 및 안전사고 발생을 예방하기 위하여 
								전담 안전직 원 및 전시관 운영총괄파트에서 6명의 직원이 수시로 관리하고 있으며, 
								각 전시관 운영직원이 순차적으로 순찰활동을 하며 관리를 하고 있습니다. 
								무엇보다 사전에 안전사고 예방을 위하여 학생들에게 사고가 발생되지 않도록 당부하여 주시기 바랍니다.
							</li>
						</ul>
					</div>
				</div>											
				<div class="row">
					<div class="col-md-12">
						<ul class="circle-list orange text-color">
							<li>
								안전관리 강화를 위한 훈련 및 교육을 강화하고 있습니다.
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">						
						<ul class="dash-list">
							<li>
								범 정부차원에서 안전사고 예방을 위한 안전관리 활동이 강화되고 있습니다.
							</li>
							<li>
								우리과학관은 화재 및 긴급 상황발생시 신속하게 대처할 수 있도록 
								각 전시관을 운영하고 있는 운영직원들에게 년 10회 이상 비상대피 훈련을 실시하고 있으며, 
								심폐소생술, 응급환자발생시 대처 방법 교육 및 실습을 통하여 만일에 발생할 수 있는 
								사고에 적극적으로 대처할 수 있도록 하고 있습니다.
							</li>
							<li>
								또한 과천소방서와 연계하여 상시 훈련을 실시하고 있으니 이점 안심하셔도 되겠습니다.
							</li>
						</ul>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<ul class="circle-list orange text-color">
							<li>
								시설물 안전점검에 만전을 기하고 있습니다.
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">						
						<ul class="dash-list">
							<li>
								우리 과학관은 화재와 자연재난을 대비하여 전기·소방시설을 포함한 관내 시설물을 정기적으로 점검함으로써 
								전시관 안전관리와 쾌적한 관람환경 조성에 만전을 기하고 있습니다.
								<div class="extra-info">
									※ 화재안전 우수건물 지정(한국화재보험협회, 2013.10월)
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div><!-- rule-spy END ...  -->
		</div>
	</div>
</div>

