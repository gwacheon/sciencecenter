<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<div class="sub-content-nav scrollspy-orange">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>

<div id="communication-library">
	
    <jsp:include page="_faqHeader.jsp"/>
    <div class="container">
    	<div id="board-show" class="board-show-orange">
    	</div>
    </div>
</div>

<c:import url="/WEB-INF/views/boards/show.jsp"></c:import>

<script type="text/javascript">
    var board = new Board(
            "#board-show",
            "<c:url value='/communication/faq/${type}'/>",
            {
                id: <c:out value="${id}"/>
            }
        );
    
    board.renderBoardShow();
</script>