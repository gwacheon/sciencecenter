<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="sub-content-nav scrollspy-orange">
    <nav id="scrollspy-nav" class="navbar-default navbar-static has-scroll">
        <div class="container">
            <ul class="nav navbar-nav">
                <c:import url="/WEB-INF/views/communication/navbar.jsp"/>
                <li>
                    <a href="#lost-property-guide-spy" class="item">
                        분실물 보관소 안내
                    </a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <div class="bread-crumbs">
                        고객소통 / 분실물 보관소
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</div>

<div class="sub-body">
	<div id="communication-body">
		<div id="lost-property-guide-spy" class="narrow-sub-top communication">
		</div>
		<div class="sub-banner-tab-wrapper sub-banner-tab-orange">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<ul id="community-tabs" class="nav nav-tabs">
							<li class="col-xs-12 col-sm-6 active">
								<a href="#" class="item">
									분실물 보관소 안내
								</a>
							</li>
							<li class="col-xs-12 col-sm-6">
								<a href="<c:url value='/communication/lostProperty' />" class="item">
									분실물 목록
									
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div id="lost-property-guide" class="item">
				<div class="section">
					<div class="row">
						<div class="col-md-12">
							<h3 class="page-title orange">
								<div class="top-line"></div>     
								분실물 보관소 안내
							</h3>
						</div>
					</div>
					<div class="sub-section">  
						<div class="row">
							<div class="col-md-12">
								<ul class="circle-list orange sub-section-title">
									<li>
										분실물 안내
									</li>
								</ul>
								<div>  
									국립과천과학관에 접수된 분실물은 다음과 같이 처리됩니다.
								</div>
							</div>					
						</div>
						<div class="row">
							<div class="col-sm-12 col-md-6 col-lg-3">								
								<div class="process">
									<div class="text">
										분실물 습득
										<div class="sub-text">
											직원 또는 관람객 습득
										</div>
									</div>
									<i class="fa fa-angle-right" aria-hidden="true"></i>
								</div>
							</div>
							<div class="col-sm-12 col-md-6 col-lg-3">								
								<div class="process ex-color">
									<div class="text">
										분실물 접수
										<div class="sub-text">
											1층 안내데스크의 분실물 보관소에 접수
										</div>
									</div>
									<i class="fa fa-angle-right" aria-hidden="true"></i>
								</div>
							</div>
							<div class="col-sm-12 col-md-6 col-lg-3">								
								<div class="process">
									<div class="text">
										분실물 인계
										<div class="sub-text">
											1개월간 홈페이지 게시 및 분실물 보관소에 보관
										</div>
									</div>
									<i class="fa fa-angle-right" aria-hidden="true"></i>
								</div>
							</div>
							<div class="col-sm-12 col-md-6 col-lg-3">								
								<div class="process ex-color">
									<div class="text">
										분실자 인계
										<div class="sub-text">
												or
										</div>
										<div>
											경찰서 인계 
											<div class="sub-text">
												(분실자 나타나지 않는 경우)
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div><!--  sub-section END ... -->
					<div class="sub-section">  
						<div class="row">
							<div class="col-md-4">
								<ul class="circle-list orange sub-section-title">
									<li>
										분실물 보관소 위치
									</li>
								</ul>
								<div class="content">
									<div class="content-title-wrapper">
										<ul class="arrow-list orange content-title">
											<li>
												위치
												<ul>
													<li>
														1층 안내데스크
													</li>
												</ul>							
											</li>
											<li>
												연락처
												<ul>
													<li>
														02-3677-1541~3
													</li>
												</ul>							
											</li>
										</ul>
									</div>
								</div>
							</div>					
							<div class="col-md-8 text-right convenience">
								<c:import url="/WEB-INF/views/display/mainBuilding/svgs/display_svg_map.jsp"></c:import>
							</div>
						</div>
					</div><!--  sub-section END ... -->
				</div><!--  section END ... -->
			</div><!--  id="lost-property-spy END  -->
		</div>
	</div>
</div>



<script type="text/javascript">
    $( function() {
    	// turn on svg map
    	setDefaultSVGMap("#lost-property-spy", 1);
    	$('#lost-property-spy').find("g[id='XMLID_223_']").attr('class', 'active');
    });
</script>