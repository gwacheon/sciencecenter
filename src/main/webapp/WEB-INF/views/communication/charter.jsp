<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="sub-content-nav scrollspy-orange">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>

<div class="sub-body">
	<div id="basic-info-spy" class="narrow-sub-top communication">
		<div class="sub-banner-tab-wrapper sub-banner-tab-orange">
			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="support-body">
		<div class="container">
			<div class="section">
				<div class="row">
					<div class="col-md-3">
						<h3 class="page-title orange">
						<div class="top-line"></div>     
							고객서비스헌장
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="charter-wrapper">
							<div class="item">
								국립과천과학관은 모든 국민에게 최선의 서비스를 제공할 것을 약속드리며 다음과같이 실천할 것을 다짐합니다.
							</div>
							<div class="item">
								하나, 우리는 세계적 수준의 <b>명품전시환경</b>을 구축하고 <b>평생 과학교육의 중심기관</b>으로 자리매김하도록 노력하겠습니다.
							</div>
							<div class="item">
								하나, 우리는 보다 최선의 서비스 제공을 위해 <b>전시.교육.연구분야의 전문성을 강화</b>하도록 꾸준히 노력하겠습니다.
							</div>
							<div class="item">
								하나, 우리는 불합리하거나 불편을 야기하는 요소를 제거함으로써 <b>고객 편의 제고</b>를 위해 최선을 다하겠습니다.
							</div>
							<div class="item">
								하나, 우리는 <b>고객의 소중한 의견</b>을 과학관 운영에 반영함으로써 항상 <b>고객과 함께하는 과학관</b>이 되겠습니다.
							</div>
							<div class="item">
								하나, 우리는 국가 과학문화 확산을 위해 하나의 밀알이 된다는 자세로 <b>고객에 대한 봉사와 헌신</b>을 실천하겠습니다.
							</div>													
						</div>
					</div>
				</div>
				<div class="sub-section">
					<ul class="circle-list orange sub-section-title">
						<li>
							우리 국립과천과학관 전 직원은 아래와 같이 고객 여러분께 서비스를 제공할 것을 약속 드리겠습니다.
						</li>
					</ul>
					<div class="content">
						<div class="row">
							<div class="col-md-12 content-title-wrapper">
								<ul class="arrow-list orange content-title">
									<li>
										최상의 전시 서비스 제공
									</li>
								</ul>		
							</div>
						</div>
						<div class="detail">
							<ul class="dash-list non-padding">
								<li>
									매년 예산의 10% 이상을 신규 전시물 개발에 투자하여 항상 새로운 전시환경을 유지하겠습니다.
								</li>
								<li>
									매년 5건 이상의 자체 기획전시회를 개최함으로써 과천과학관만의 특별한 전시서비스를 제공하겠습니다.
								</li>
								<li>
									전시관 별로 전문 과학 해설사를 상시 배치하여, 관람 중 궁금증을 현장에서 해결하실 수 있도록 최선을 다하겠습니다.
								</li>
								<li>
									전시품 정상 가동률을 99% 이상으로 유지하기 위하여 매일 아침 사전점검을 실시하겠습니다.
								</li>
								<li>
									고장 시에는 특별한 경우를 제외하고는 당일 수리를 원칙으로 하고, 안내 표지 등을 통해 사전 안내를 하겠습니다.
								</li>
							</ul>
						</div>
					</div>
					<div class="content">
						<div class="row">
							<div class="col-md-12 content-title-wrapper">
								<ul class="arrow-list orange content-title">
									<li>
										다양하고 즐거운 경험 제공
									</li>
								</ul>		
							</div>
						</div>
						<div class="detail">
							<ul class="dash-list non-padding">
								<li>
									연 4회 이상의 과학문화공연을 개최함으로써, 과학문화 대중화에 앞장 서 나가겠습니다.
								</li>
								<li>
									공인된 실력을 갖춘 강사진들과 함께 믿을 수 있는 교육을 실시하겠습니다.
								</li>
								<li>
									전시관 내 식당 등에서는 믿을 수 있는 건강한 음식을 제공 하겠습니다.
								</li>
							</ul>
						</div>
					</div>
					<div class="content">
						<div class="row">
							<div class="col-md-12 content-title-wrapper">
								<ul class="arrow-list orange content-title">
									<li>
										친절한 서비스
									</li>
								</ul>		
							</div>
						</div>
						<div class="detail">
							<ul class="dash-list non-padding">
								<li>
									교육, 문화 행사 등의 고객 참여를 위해서 최소한 1주 전에는 홈페이지 등을 통해서 공지하겠습니다.
								</li>
								<li>
									단체 고객 등 상담이 필요하신 고객분을 위하여 전담상담요원을 배치하겠습니다.
								</li>
								<li>
									표준화된 고객 대응 업무 매뉴얼을 비치하여 고객 서비스 기준과 절차를 정하고, 이를 실천하겠습니다.
								</li>
								<li>
									정기적인 고객 서비스 교육을 실시하여 항상 배우는 자세로 서비스 제고를 위해서 노력하겠습니다.
								</li>
								<li>
									고객 서비스 실명제를 유지하기 위하여 모든 직원들은 이름표를 달고 근무하겠습니다.
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="sub-section">
					<ul class="circle-list orange sub-section-title">
						<li>
							저희는 고객님과 이렇게 소통하겠습니다!
						</li>
					</ul>
					<div class="content">
						<div class="row">
							<div class="col-md-12 content-title-wrapper">
								<ul class="arrow-list orange content-title">
									<li>
										고객님이 과학관을 방문하시면,
									</li>
								</ul>		
							</div>
						</div>
						<div class="detail">
							<ul class="dash-list non-padding">
								<li>
									가시고자 하는 곳을 쉽게 찾으실 수 있도록 안내 표지판을 게시 하겠습니다.
								</li>
								<li>
									고객님이 방문 후 5분 이상 기다리지 않도록 노력하겠습니다. 다만, 담당자가 부재중일 경우 다른 직원이 친절하게 안내해 드리겠습니다.
								</li>
								<li>
									고객님의 의견을 잘 듣고, 질문사항은 이해하시기 쉽도록 자세히 설명 드리겠습니다.
								</li>
								<li>
									고객님의 불만 및 제안 사항 등을 전시관 곳곳에 비치된 고객의 소리함을 통해 접수하고, 일주일 내에 처리결과를 회신하여 드리겠습니다.
								</li>
								<li>
									관람 중 분실한 물건은 중앙홀 안내데스크를 통해 신속하게 찾으실 수 있도록 최선을 다하겠습니다.
								</li>
							</ul>
						</div>
					</div>
					<div class="content">
						<div class="row">
							<div class="col-md-12 content-title-wrapper">
								<ul class="arrow-list orange content-title">
									<li>
										고객님이 과학관에 전화를 주시면,
									</li>
								</ul>	
							</div>
						</div>
						<div class="detail">
							<ul class="dash-list non-padding">
								<li>
									전화벨이 3번 이상 울리기 전에 신속하게 받고, 인사말, 성명을 분명하게 말씀 드리겠습니다.
								</li>
								<li>
									간단한 사항은 전화 수신자가 바로 답변 드리겠습니다. 부득이하게 담당직원만이 답변할 수 있는 사항에 대해서는 양해를 구한 후, 업무담당자에게 연결하겠습니다.
								</li>
								<li>
									가능한 모든 질의에 대해서는 즉시 답변 드리겠으며, 바로 답변이 곤란한 경우는 2영업일 이내에 전화, 이메일, SNS 등 고객님이 원하시는 방법으로 답변 드리겠습니다.
								</li>
								<li>
									고객이 전화를 끊으시기 전에 더 궁금한 사항은 없는 지 여쭤보겠습니다.
								</li>
								<li>
									친절한 전화응대를 위해 직원교육 및 모니터링을 지속적으로 실시하겠습니다.
								</li>
							</ul>
						</div>
					</div>
					<div class="content">
						<div class="row">
							<div class="col-md-12 content-title-wrapper">
								<ul class="arrow-list orange content-title">
									<li>
										고객님이 저희 홈페이지를 방문하시면,
									</li>
								</ul>		
							</div>
						</div>
						<div class="detail">
							<ul class="dash-list non-padding">
								<li>
									전시장과 관련된 모든 정보를 한 눈에 알아보실 수 있도록 안내해 드리겠습니다.
								</li>
								<li>
									홈페이지를 통해 고객님이 처리하실 수 있는 다양한 업무를 계속 개발해 나가겠습니다.
								</li>
								<li>
									인터넷 질의 사항 중 관리자 이메일로 접수된 사항에 대해서는 질의일로부터 1영업일 이내 답변을 원칙으로 하고, 부득이한 사정으로 처리시간을 요하는 경우에도 처리 과정과 처리시한을 미리 알려드리겠습니다.
								</li>
							</ul>
						</div>
					</div>
					<div class="content">
						<div class="row">
							<div class="col-md-12 content-title-wrapper">
								<ul class="arrow-list orange content-title">
									<li>
										저희 과학관에서 불만족, 불편한 점이 있으시면
									</li>
								</ul>		
							</div>
						</div>
						<div class="detail">
							<ul class="dash-list non-padding">
								<li>
									담당부서에서 바로 처리해 드리겠습니다.
									<div class="table-responsive">
										<table class="table table-bordered centered-table">
											<thead>
												<tr>
													<th>불편사항</th>
													<th>연락처</th>
													<th>비고</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>주차·매표·편의시설 등이 불편하십니까?</td>
													<td>02-3677-1363</td>
													<td>운영지원과</td>
												</tr>
												<tr>
													<td>전시관 내 전시물 및 프로그램에 관한 의견이 있으십니까?</td>
													<td>02-3677-1375	</td>
													<td>전시운영총괄과</td>
												</tr>
												<tr>
													<td>전시장 운영요원 등에 의견이 있으십니까?</td>
													<td>02-3677-1371</td>
													<td>전시운영총괄과</td>
												</tr>
												<tr>
													<td>교육동 과학교육에 의견이 있으십니까?</td>
													<td>02-3677-1523</td>
													<td>과학탐구교육과</td>
												</tr>
												<tr>
													<td>과학문화공연 및 문화시설 대관이 궁금하십니까?</td>
													<td>02-3677-1429</td>
													<td>과학문화진흥과</td>
												</tr>
												<tr>
													<td>홈페이지 이용 등이 불편하십니까?</td>
													<td>02-3677-1390</td>
													<td>전시기획연구과</td>
												</tr>
												<tr>
													<td>기타 과학관에 관한 의견이 있으십니까?</td>
													<td>02-3677-1315</td>
													<td>경영기획과s</td>
												</tr>
											</tbody>
										</table>
									</div>
								</li>
								<li>
									주소 - (427-060) 경기도 과천시 상하벌로 110 국립과천과학관
								</li>
								<li>
									홈페이지 - www.sciencecenter.go.kr
								</li>
								<li>
									블로그 - http://nsm2010.blog.me/
								</li>
								<li>
									트위터 - @scientorium
								</li>
								<li>
									페이스북 - http://www.facebook.com/scientorium
								</li>
								<li>
									해당 불만족 및 불편사항에 대해 공식적으로 점검을 실시하고, 3영업일 이내 그 결과를 직접 알려 드리겠습니다.
								</li>
								<li>
									불친절한 직원은 재교육하고, 잘못한 경우에는 사과문을 보내드리겠습니다.
								</li>
								
							</ul>
						</div>
					</div>
					<div class="content">
						<div class="row">
							<div class="col-md-12 content-title-wrapper">
								<ul class="arrow-list orange content-title">
									<li>
										고객만족도 제고를 위해서 항상 노력하겠습니다.
									</li>
								</ul>		
							</div>
						</div>
						<div class="detail">
							<ul class="dash-list non-padding">
								<li>
									외부기관 또는 자체조사를 통해, 고객면접 및 설문조사 등의 방법으로 객관적인 고객만족도 조사를 1년에 1회 이상 실시하겠습니다.
								</li>
								<li>
									고객만족도 평가 결과는 홈페이지 및 관내 게시판에 한달 이상 게시하고, 양질의 서비스를 제공할 수 있도록 노력하겠습니다
								</li>
								<li>
									본 고객서비스 헌장을 이행하지 못한 것을 지적해 주신 고객님께는 저희 관련 규정에 따라 보상해 드리겠습니다.
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>