<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<div class="sub-content-nav scrollspy-orange">
	<div class="navbar-detailed">
		<div class="sub-category-wrapper">
			<div class="container">
				<ul>
					<li class="sub-category active">
						<a href="#">
							이용약관
						</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="sub-child-wrapper">
			<div class="container">
				<ul></ul>
			</div>
		</div>
	</div>
</div>

<div class="sub-body">
	<div class="narrow-sub-top communication">
		<div class="sub-banner-tab-wrapper sub-banner-tab-orange">
			<div class="container">
				<div class="row">   
					<div class="col-md-10 col-md-offset-1">
						<ul class="nav nav-tabs">
							<li class="col-xs-12 col-sm-4" data-name="policy">
								<a href = "#" class="item" data-name="policy">이용약관</a>
							</li>
							<li class="col-xs-12 col-sm-4" data-name="media">
								<a href = "#" class="item" data-name="media">
									영상정보처리기기 운영/관리 방침
								</a>
							</li>
							<li class="col-xs-12 col-sm-4" data-name="privacy">
								<a href = "#" class="item" data-name="privacy">개인정보 취급방침</a>
							</li>
						</ul>     
					</div>    
				</div>
			</div>
		</div>
	</div><!-- support-top END... -->
	<div id="support-body" class="container">
		<c:forEach var="policy" items="${policies }">
			<div id="${policy.type }" class="policy-wrapper tab-item">	
				<div class="sub-header">
					<div class="row">  
						<div class="col-md-12">
							<h3 class="page-title orange">
								<div class="top-line"></div>     
								<spring:message code="policy.${policy.type}" />
							</h3>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="content">							
								<c:out value="${policy.content }" escapeXml="false" />
							</div>
							<c:choose>
								<c:when test="${policy.type eq 'policy' }">
									<c:forEach var="prev" items="${prevPolicy }">
										<div>
											<a href="#" data-toggle="modal" data-target="#prev-policy-${prev.id }-modal">
												<fmt:formatDate value="${prev.updtDttm}" pattern="yyyy/MM/dd"/> 이전내용 보기
											</a>
										</div>
									</c:forEach>
								</c:when>
								<c:when test="${policy.type eq 'privacy' }">
									<c:forEach var="prev" items="${prevPrivacy }">
										<div>
											<a href="#" data-toggle="modal" data-target="#prev-privacy-${prev.id }-modal">
												<fmt:formatDate value="${prev.updtDttm}" pattern="yyyy/MM/dd"/> 이전내용 보기
											</a>
										</div>
									</c:forEach>
								</c:when>
								<c:otherwise>
									
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</div>
			</div>
		</c:forEach>
	</div><!-- support body END... -->
</div>

<c:if test="${not empty prevPolicy }">
	<c:forEach var="prev" items="${prevPolicy }">
		<div class="modal fade" id="prev-policy-${prev.id }-modal" tabindex="-1">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="myModalLabel">
		        	이용약관 <fmt:formatDate value="${prev.updtDttm}" pattern="yyyy/MM/dd hh:mm:ss"/>
		        </h4>
		      </div>
		      <div class="modal-body" style="max-height: 500px; overflow-y: scroll;">
		          <c:out value="${prev.content }" escapeXml="false" />
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-primary" data-dismiss="modal">닫기</button>
		      </div>
		    </div>
		  </div>
		</div>
	</c:forEach>
</c:if>


<c:if test="${not empty prevPrivacy }">
	<c:forEach var="prev" items="${prevPrivacy }">
		<div class="modal fade" id="prev-privacy-${prev.id }-modal" tabindex="-1">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="myModalLabel">
		        	개인정보 처리 방침 <fmt:formatDate value="${prev.updtDttm}" pattern="yyyy/MM/dd hh:mm:ss"/>
		        </h4>
		      </div>
		      <div class="modal-body" style="max-height: 500px; overflow-y: scroll;">
		          <c:out value="${prev.content }" escapeXml="false" />
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-primary" data-dismiss="modal">닫기</button>
		      </div>
		    </div>
		  </div>
		</div>
	</c:forEach>
</c:if>



<script type="text/javascript">
	
	$(function() {
		//nav-tabs click event
		$(".nav-tabs .item[data-name='${type}']").click();
		// add class to table tag
		$('table').addClass("table").addClass("table-orange centered-table");
		
	});
	
	
	$('.narrow-sub-top .nav-tabs .item').on('click',function(){
		var id = $(this).data('name');
		var id_selector = "#" + id;
		$('.narrow-sub-top .nav-tabs li').removeClass('active');
		$(".narrow-sub-top .nav-tabs li[data-name='" + id + "']").addClass('active');
		$('#support-body .tab-item').removeClass('active');
		$('#'+id+'.tab-item').addClass('active');
	});
	
	
	

</script>