<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="sub-content-nav scrollspy-orange">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>

<div class="sub-body">
	<div id="basic-info-spy" class="narrow-sub-top communication">
		<div class="sub-banner-tab-wrapper sub-banner-tab-orange">
			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="communication-body">
		<div class="container">
			<div class="section">
				<div class="row">
					<div class="col-md-12">
						<h3 class="page-title orange">
							<div class="top-line"></div>     
							연간 방문객
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="charter-wrapper visitors">
							<div class="header">
								<b>국립과천과학관</b>은 <span class="emphasis">연간 상설전시관 입장객 118만명</span>의 <b>과학문화명소</b>로 자리잡았습니다.
							</div>
							<div>
								국립과천과학관은 고객의 눈높이와 수요에 맞추기 위해 지속적인 서비스 개선 노력을 하였습니다.
							</div>
							<div>
								다양한 주제의 체험형 특별기획전을 제공하고, 
								전시물간 연계를 통해 과학원리에 대한 심층적 이해가 가능한 전시공간을 구성하였습니다. 
								현장체험 중심의 과학교육에 대한 고객의 수요를 반영하여 융합인재교육(STEAM)의 기틀을 마련하고, 
								소통형 문화 프로그램을 확대 편성하였습니다.								
							</div>						
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="sub-section">
							<ul class="circle-list orange sub-section-title">
								<li>
									연도별 방문객 변화
								</li>
							</ul>
							<div class="support-chart">
								<canvas id="chart-visitors-year"></canvas>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="sub-section">
							<ul class="circle-list orange sub-section-title">
								<li>
									상설전시관 월별 방문객(명)
								</li>
							</ul>
							<div class="support-chart">
								<canvas id="chart-visitors-month"></canvas>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="sub-section">
							<ul class="circle-list orange sub-section-title">
								<li>
									전시관 관람객 현황(만명)
								</li>
							</ul>
							<div class="content">
								<div class="row">
									<div class="col-md-12 content-title-wrapper">
										<ul class="arrow-list orange content-title">
											<li>
												방문이용객 현황 : 247.8만명(교육,문화행사, 시설이용객 포함)
											</li>
											<li>
												2015년 부터 특별기획전시는 방문이용객에서 제외
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="support-chart">
								<canvas id="chart-visitors-building" style="height: 300px;"></canvas>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="sub-section">
							<div class="sub-section">
								<ul class="circle-list orange sub-section-title">
									<li>
										단체 관람객 현황
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="sub-section">
							<div class="support-chart border">
								<canvas id="chart-visitors-area"></canvas>
								<div class="caption">
									지역별 주요 단체 관람객 현황(%)
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="sub-section">
							<div class="support-chart border">
								<canvas id="chart-visitors-age"></canvas>
								<div class="caption">
									연령별 주요 단체 관람객 현황(%)
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	
	$(function() {		
		chartVisitorsYear();
		chartVisitorsMonth();
		chartVisitorsBuilding();
		chartVisitorsArea();
		chartVisitorsAge();
	});
	
	
	var barColorNavy = "151,187,205";
	var barColorGray = "220,220,220";
	var barColorRed = "229,136,126";
	var barColorYellow = "245,197,74";
	var barColororange = "136,216,176";
	var barColorViolet = "191,158,206";
	
	/* 연도별 방문객 변화 데이터 */
	function chartVisitorsYear() {
		
		/* Chart.defaults.global.multiTooltipTemplate = function (label) {
		    return label.datasetLabel + '년 : ' + label.value.toString() + "명";
		};  */
		 
		//Get context with jQuery - using jQuery's .get() method.
		var ctx = $('#chart-visitors-year').get(0).getContext("2d");

		var data = {
		    labels: ["2008","2009","2010","2011","2012","2013","2014","2015"],
		    datasets: [
		        {
		            label: "방문객 수",
		            fillColor: "rgba(" + barColorNavy + ",0.5)",
		            strokeColor: "rgba(" + barColorNavy + ",0.8)",
		            highlightFill: "rgba(" + barColorNavy + ",0.75)",
		            highlightStroke: "rgba(" + barColorNavy + ",1)",
		            data: [470000,1300000,2010000,2330000,2470000,2450000,2460000,2470000]
		        }
		        
		    ]
		};
		
		var options = {
				responsive: true,
				scaleFontSize: 9,
				
				multiTooltipTemplate: function (label) {
					console.log(label);
					return label + '년 : ' + label.value.toString() + "명";
				} 
		}
		
		// This will get the first returned node in the jQuery collection.
		var myBarChart1 = new Chart(ctx).Bar(data, options);
	}
	
	/* 상설전시관 월별 방문객 데이터 */
	function chartVisitorsMonth() {
		
		/* Chart.defaults.global.multiTooltipTemplate = function (label) {
		    return label.datasetLabel + '년 : ' + label.value.toString() + "명";
		};  */
		 
		//Get context with jQuery - using jQuery's .get() method.
		var ctx = $('#chart-visitors-month').get(0).getContext("2d");

		var data = {
		    labels: ["01월","02월","03월","04월","05월","06월","07월","08월","09월","10월","11월","12월"],
		    datasets: [
		        {
		            label: "방문객 수",
		            fillColor: "rgba(" + barColorNavy + ",0.2)",
		            strokeColor: "rgba(" + barColorNavy + ",1)",
		            pointColor: "rgba(" + barColorNavy + ",1)",
		            pointStrokeColor: "#fff",
		            pointHighlightFill: "#fff",
		            pointHighlightStroke: "rgba(" + barColorNavy + ",1)",
		            data: [113569,63480,53974,111506,129231,17710,72775,146916,78159,150014,100168,148256]
		        }
		        
		    ]
		};
		
		var options = {
				responsive: true,
				scaleBeginAtZero: true,
				//scaleFontSize: 14
				multiTooltipTemplate: function (label) {
					return label.datasetLabel + '년 : ' + label.value.toString() + "명";
				}
		}
		
		// This will get the first returned node in the jQuery collection.
		var myLineChart = new Chart(ctx).Line(data, options);
	}
	
	/* 전시관 관람객 현황 데이터 */
	function chartVisitorsBuilding() {
		
		/* Chart.defaults.global.multiTooltipTemplate = function (label) {
		    return label.datasetLabel + '년 : ' + label.value.toString() + "명";
		};  */
		 
		//Get context with jQuery - using jQuery's .get() method.
		var ctx = $('#chart-visitors-building').get(0).getContext("2d");

		var data = {
		    labels: ["2010","2011","2012","2013","2014","2015"],
		    datasets: [
		        {
		            label: "상설전시관",
		            fillColor: "rgba(" + barColorNavy + ",0.5)",
		            strokeColor: "rgba(" + barColorNavy + ",0.8)",
		            highlightFill: "rgba(" + barColorNavy + ",0.75)",
		            highlightStroke: "rgba(" + barColorNavy + ",1)",
		            data: [132,135,12,116,104,118]
		        },
		        {
		            label: "특별전시관",
		            fillColor: "rgba(" + barColorRed + ",0.5)",
		            strokeColor: "rgba(" + barColorRed + ",0.8)",
		            highlightFill: "rgba(" + barColorRed + ",0.75)",
		            highlightStroke: "rgba(" + barColorRed + ",1)",
		            data: [9,13,24,11,22,0]
		        },
		        {
		            label: "천문우주관",
		            fillColor: "rgba(" + barColorYellow + ",0.5)",
		            strokeColor: "rgba(" + barColorYellow + ",0.8)",
		            highlightFill: "rgba(" + barColorYellow + ",0.75)",
		            highlightStroke: "rgba(" + barColorYellow + ",1)",
		            data: [22,24,23,32,36,29]
		        },
		        {
		            label: "곤충생태관",
		            fillColor: "rgba(" + barColororange + ",0.5)",
		            strokeColor: "rgba(" + barColororange + ",0.8)",
		            highlightFill: "rgba(" + barColororange + ",0.75)",
		            highlightStroke: "rgba(" + barColororange + ",1)",
		            data: [0,0,0,32,36,35]
		        }
		        
		    ]
		};
		
		var options = {
				responsive: true,
				scaleFontSize: 9,
				multiTooltipTemplate: function (label) {
					return label.datasetLabel + ' : ' + label.value.toString() + "만명";
				}
		}
		
		// This will get the first returned node in the jQuery collection.
		var myBarChart2 = new Chart(ctx).Bar(data, options);
	}
	
	/* 단체 관람객 현황 (지역별) 데이터 */
	function chartVisitorsArea() {
		 
		//Get context with jQuery - using jQuery's .get() method.
		var ctx = $('#chart-visitors-area').get(0).getContext("2d");
		
		var data = [
            {
                value: 5,
                color: "rgba(" + barColorNavy + ",.7)",
                highlight: "rgba(" + barColorNavy + ",1)",
                label: "충청권"
            },
            {
                value: 7,
                color: "rgba(" + barColorRed + ",.7)",
                highlight: "rgba(" + barColorRed + ",1)",
                label: "전라권"
            },
            {
                value: 12,
                color: "rgba(" + barColorYellow + ",.7)",
                highlight: "rgba(" + barColorYellow + ",1)",
                label: "경상권"
            },
            {
                value: 3,
                color: "rgba(" + barColororange + ",.7)",
                highlight: "rgba(" + barColororange + ",1)",
                label: "강원권"
            },
            {
                value: 72,
                color: "rgba(" + barColorViolet + ",.7)",
                highlight: "rgba(" + barColorViolet + ",1)",
                label: "수도권"
            }
        ]
		
		
		
		var options = {
				responsive: true,
				multiTooltipTemplate: function (label) {
					return label.value.toString() + "%";
				}
		}
		
		// This will get the first returned node in the jQuery collection.
		var myPieChart1 = new Chart(ctx).Pie(data, options);
		
	}
	/* 단체 관람객 현황 (연령별) 데이터 */
	function chartVisitorsAge() {
		//Get context with jQuery - using jQuery's .get() method.
		var ctx = $('#chart-visitors-age').get(0).getContext("2d");
		
		var data = [
            {
                value: 18.1,
                color: "rgba(" + barColorNavy + ",.7)",
                highlight: "rgba(" + barColorNavy + ",1)",
                label: "유아"
            },
            {
                value: 31.6,
                color: "rgba(" + barColorRed + ",.7)",
                highlight: "rgba(" + barColorRed + ",1)",
                label: "초등"
            },
            {
                value: 25.0,
                color: "rgba(" + barColorYellow + ",.7)",
                highlight: "rgba(" + barColorYellow + ",1)",
                label: "중등"
            },
            {
                value: 12.1,
                color: "rgba(" + barColororange + ",.7)",
                highlight: "rgba(" + barColororange + ",1)",
                label: "고등"
            },
            {
                value: 1.1,
                color: "rgba(" + barColorGray + ",.7)",
                highlight: "rgba(" + barColorGray + ",1)",
                label: "성인"
            },
            {
                value: 2.2,
                color: "rgba(" + barColorViolet + ",.7)",
                highlight: "rgba(" + barColorViolet + ",1)",
                label: "기타"
            }
        ]
		
		
		
		var options = {
			responsive: true,
			multiTooltipTemplate: function (label) {
				return label.datasetLabel + ' : ' + label.value.toString() + "%";
			}
		}
		
		// This will get the first returned node in the jQuery collection.
		var myPieChart2 = new Chart(ctx).Pie(data, options);
		
	}
</script>
