<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<div class="sub-content-nav scrollspy-orange">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>

<div id="support-body" class="sub-body">
	<div id="basic-info-spy" class="narrow-sub-top communication">
		
	</div>
	<div id="support-body">
		<div class="container">
			
			<div id="contentwarp">

				<div>
					<script type="text/javascript" language="javascript" src="http://www.epeople.go.kr/js/frame/jsFrameForOpener.js"></script>
					<noscript>스크립트 기능이 중지되어 있어, 국민신문고 화면을 정상적으 로 표시할 수 없습니다.</noscript>
					<iframe src="https://www.epeople.go.kr/jsp/user/frame/pc/mycvreq/UPcMyCvreqList.jsp?anc_code=1710000&channel=1710125&menu_code=PC002" title="나의민원" id="content3" name="content3" width="100%" height="1500" frameborder="0" marginwidth="0" marginheight="0" onload="resizeFrame2();" ></iframe><!--<iframe src="http://www.epeople.go.kr/jsp/user/frame/pc/mycvreq/UPcMyCvreqList.jsp?anc_code=1710000&channel=1710125&menu_code=PC002" title="나의민원" id="mergerFrame" name="mergerFrame" width="100%" height="100%" frameborder="0" marginwidth="0" marginheight="0" onload="AfterSubmitProcess();" ></iframe>-->
					<noframes><a href="http://www.epeople.go.kr/" title="국민신문고 이동" target="_blank">국민신문고 직접접속</a></noframes>
				</div>
			
            </div>
            <!-- //contentwarp -->
					
							
			<script type="text/javascript">
			<!--
			
			function doResize()
			{
				var iframe = document.getElementById('content2');
			
				iframe.height = 0;
				var temp = eval(iframe.name + ".document.body.scrollHeight") + 30;
				iframe.height = temp;
			}
			
			function doResize2()
			{
				var iframe = document.getElementById('content3');
			
			
			
				iframe.height = 0;
				var temp = eval(iframe.name + ".document.body.scrollHeight") + 30;
				iframe.height = temp;
			}
			
			function doResize3()
			{
				var iframe = document.getElementById('content2');
			
			
			
				iframe.height = 0;
				var temp = eval(iframe.name + ".document.body.scrollHeight") + 30;
				iframe.height = temp;
			
			
			}
			
			function doResizeChat(name)
			{
				var iframe = document.getElementById('chat');
			
			
			
			
				iframe.height = 816;
			
				var temp = eval(iframe.name + ".document.body.scrollHeight") + 70;
			
			
			    if(temp < 768){
			        temp = 768;
			    }
			
			    iframe.height = temp;
			}
			
			
			
			
			
			/* iframe에서 부모함수 호출 */
			function go_parent_page(action){
				window.location.href = action;
			}
			
			function doIframe(){
			    o = document.getElementsByTagName('iframe');
			
			    for(var i=0;i<o.length;i++){  
			        if (/\bautoHeight\b/.test(o[i].className)){
			            setHeight(o[i]);
			            addEvent(o[i],'load', doIframe);
			        }
			    }
			}
			
			function setHeight(e){
			    if(e.contentDocument){
			        e.height = e.contentDocument.body.offsetHeight + 35; //높이 조절
			    } else {
			        e.height = e.contentWindow.document.body.scrollHeight;
			    }
			}
			
			
			function addEvent(obj, evType, fn){
			    if(obj.addEventListener)
			    {
			    obj.addEventListener(evType, fn,false);
			    return true;
			    } else if (obj.attachEvent){
			    var r = obj.attachEvent("on"+evType, fn);
			    return r;
			    } else {
			    return false;
			    }
			}
			
			
			
			if(document.getElementById && document.createTextNode){
				addEvent(window, 'load', doIframe);
			}
			
			
			function resizeFrame() {
				var min_h = 100;
				
				        var oIFrame = document.getElementById("content2");
				 
				        try {          
				          
				          var oDoc = oIFrame.contentDocument || oIFrame.contentWindow.document;        
			
				          var frmHeight = 0;
				          
				          if (/MSIE/.test(navigator.userAgent)) {
				            
				            oIFrame.height = 0;
				            frmHeight = eval(oIFrame.name + ".document.body.scrollHeight") + 30;
				        	
				          } else {
				        	  
				            var s = oDoc.body.appendChild(document.createElement('DIV'))
				            s.style.clear = 'both';
				 
				            frmHeight = s.offsetTop;
				            s.parentNode.removeChild(s);
				          }
			
				          if (frmHeight < min_h) frmHeight = min_h;
			
				          //   oIFrame.style.height = frmHeight;
				       oIFrame.height = frmHeight;
				 
				} catch (e) { }
				    }
				   
			
			
				function resizeFrame2() {
				var min_h = 100;
				
				        var oIFrame = document.getElementById("content3");
				 
				        try {          
				          
				          var oDoc = oIFrame.contentDocument || oIFrame.contentWindow.document;        
			
				          var frmHeight = 0;
				          
				          if (/MSIE/.test(navigator.userAgent)) {
				            
				            oIFrame.height = 0;
				            frmHeight = eval(oIFrame.name + ".document.body.scrollHeight") + 30;
				        	
				          } else {
				        	  
				            var s = oDoc.body.appendChild(document.createElement('DIV'))
				            s.style.clear = 'both';
				 
				            frmHeight = s.offsetTop;
				            s.parentNode.removeChild(s);
				          }
			
				          if (frmHeight < min_h) frmHeight = min_h;
			
				          //   oIFrame.style.height = frmHeight;
				       oIFrame.height = frmHeight;
				 
				} catch (e) { }
				    }
			//-->
			</script>
			
			<noscript>스크립트 기능이 중지되어 있어, 국민신문고 화면을 정상적으 로 표시할 수 없습니다.</noscript>

		</div><!--  container END ... -->
	</div>
</div>






<script type="text/javascript">
<!--

function doResize()
{
	var iframe = document.getElementById('content2');

	iframe.height = 0;
	var temp = eval(iframe.name + ".document.body.scrollHeight") + 30;
	iframe.height = temp;
}

function doResize2()
{
	var iframe = document.getElementById('content3');



	iframe.height = 0;
	var temp = eval(iframe.name + ".document.body.scrollHeight") + 30;
	iframe.height = temp;
}

function doResize3()
{
	var iframe = document.getElementById('content2');



	iframe.height = 0;
	var temp = eval(iframe.name + ".document.body.scrollHeight") + 30;
	iframe.height = temp;


}

function doResizeChat(name)
{
	var iframe = document.getElementById('chat');




	iframe.height = 816;

	var temp = eval(iframe.name + ".document.body.scrollHeight") + 70;


    if(temp < 768){
        temp = 768;
    }

    iframe.height = temp;
}





/* iframe에서 부모함수 호출 */
function go_parent_page(action){
	window.location.href = action;
}

function doIframe(){
    o = document.getElementsByTagName('iframe');

    for(var i=0;i<o.length;i++){  
        if (/\bautoHeight\b/.test(o[i].className)){
            setHeight(o[i]);
            addEvent(o[i],'load', doIframe);
        }
    }
}

function setHeight(e){
    if(e.contentDocument){
        e.height = e.contentDocument.body.offsetHeight + 35; //높이 조절
    } else {
        e.height = e.contentWindow.document.body.scrollHeight;
    }
}


function addEvent(obj, evType, fn){
    if(obj.addEventListener)
    {
    obj.addEventListener(evType, fn,false);
    return true;
    } else if (obj.attachEvent){
    var r = obj.attachEvent("on"+evType, fn);
    return r;
    } else {
    return false;
    }
}



if(document.getElementById && document.createTextNode){
	addEvent(window, 'load', doIframe);
}


function resizeFrame() {
	var min_h = 100;
	
	        var oIFrame = document.getElementById("content2");
	 
	        try {          
	          
	          var oDoc = oIFrame.contentDocument || oIFrame.contentWindow.document;        

	          var frmHeight = 0;
	          
	          if (/MSIE/.test(navigator.userAgent)) {
	            
	            oIFrame.height = 0;
	            frmHeight = eval(oIFrame.name + ".document.body.scrollHeight") + 30;
	        	
	          } else {
	        	  
	            var s = oDoc.body.appendChild(document.createElement('DIV'))
	            s.style.clear = 'both';
	 
	            frmHeight = s.offsetTop;
	            s.parentNode.removeChild(s);
	          }

	          if (frmHeight < min_h) frmHeight = min_h;

	          //   oIFrame.style.height = frmHeight;
	       oIFrame.height = frmHeight;
	 
	} catch (e) { }
	    }
	   


	function resizeFrame2() {
	var min_h = 100;
	
	        var oIFrame = document.getElementById("content3");
	 
	        try {          
	          
	          var oDoc = oIFrame.contentDocument || oIFrame.contentWindow.document;        

	          var frmHeight = 0;
	          
	          if (/MSIE/.test(navigator.userAgent)) {
	            
	            oIFrame.height = 0;
	            frmHeight = eval(oIFrame.name + ".document.body.scrollHeight") + 30;
	        	
	          } else {
	        	  
	            var s = oDoc.body.appendChild(document.createElement('DIV'))
	            s.style.clear = 'both';
	 
	            frmHeight = s.offsetTop;
	            s.parentNode.removeChild(s);
	          }

	          if (frmHeight < min_h) frmHeight = min_h;

	          //   oIFrame.style.height = frmHeight;
	       oIFrame.height = frmHeight;
	 
	} catch (e) { }
	    }
//-->
</script>