<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="sub-content-nav scrollspy-orange">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>

<div id = "communication-wrapper" >
	
	<div id="communication-body" class="sub-body">
		<div id="basic-info-spy" class="narrow-sub-top communication">
		<div class="sub-banner-tab-wrapper sub-banner-tab-orange">
			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						
					</div>
				</div>
			</div>
		</div>
	</div>
		<div  class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title orange">
						<div class="top-line"></div>
						 소셜네트워크
					</h3>
				</div>
			</div> 
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="social-item-wrapper">
						<div class="social-item naver">
							<div class="logo">
								<img src="<c:url value="/resources/img/communication/sns_01.png"/>" class="img-responsive" />
							</div>
							<div class="content-wrapper">
								<div class="content">
									<h5>국립과천과학관<br class="visible-xs visible-sm"> 네이버 블로그</h5>
									<div class="link">http://nsm2010.blog.me</div>
									<a class="btn" href="http://nsm2010.blog.me" target="_blank">
										바로가기
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="social-item-wrapper">
						<div class="social-item daum">
							<div class="logo"> 
								<img src="<c:url value="/resources/img/communication/sns_02.png"/>" class="img-responsive" />
							</div>
							<div class="content-wrapper">
								<div class="content">
									<h5>국립과천과학관<br class="visible-xs visible-sm"> 다음 카페</h5>
									<div class="link">http://cafe.daum.net/SBEL</div>
									<a class="btn" href="http://cafe.daum.net/SBEL" target="_blank">
										바로가기
									</a> 
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="social-item-wrapper">
						<div class="social-item facebook">
							<div class="logo">
								<img src="<c:url value="/resources/img/communication/sns_03.png"/>" class="img-responsive" />
							</div>
							<div class="content-wrapper">
								<div class="content">
									<h5>국립과천과학관<br class="visible-xs visible-sm"> 페이스북</h5>
									<div class="link">http://www.facebook.com/scientorium</div>
									<a class="btn" href="http://www.facebook.com/scientorium" target="_blank">
										바로가기
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="social-item-wrapper">
						<div class="social-item twitter">
							<div class="logo">
								<img src="<c:url value="/resources/img/communication/sns_04.png"/>" class="img-responsive" />
							</div>
							<div class="content-wrapper">
								<div class="content">
									<h5>국립과천과학관<br class="visible-xs visible-sm"> 트위터</h5>
									<div class="link">http://twitter.com/scientorium</div>
									<a class="btn" href="http://twitter.com/scientorium" target="_blank">
										바로가기
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> 
	</div>
</div> 
