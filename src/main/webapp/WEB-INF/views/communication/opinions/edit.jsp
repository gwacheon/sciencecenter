<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>

<div class="sub-content-nav scrollspy-orange">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>

<div id="communication-library">

	<div id="support-body" class="sub-body">
		<div id="basic-info-spy" class="narrow-sub-top communication">
			
		</div>
		<div  class="container">
			<div id="board-spy" class="row">
				<div class="col-md-12">
					<h3 class="page-title orange">
						<div class="top-line"></div>
						 의견수렴 게시글 수정
					</h3>
				</div>
			</div>
			
			<div id="auth-form">
				<div class="row">
					<div class="col-md-12">
					<c:url var="action" value='/communication/opinions/private/update' />
					<c:url var="method" value='POST' />
			
					<jsp:include page="/WEB-INF/views/boards/form.jsp">
						<jsp:param name="action" value="${action}" />
						<jsp:param name="method" value="${method}" />
					</jsp:include>
					</div>
				</div>
			</div>
			
		</div> 
	</div>
</div> 


