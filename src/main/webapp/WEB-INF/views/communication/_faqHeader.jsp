<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

 
    <div id="communication-body" class="sub-body">
    	<div id="basic-info-spy" class="narrow-sub-top communication">
			<div class="sub-banner-tab-wrapper sub-banner-tab-orange">
				<div class="container">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							
						</div>
					</div>
				</div>
			</div>
		</div>
        <div class="container">
            <div id="board-spy" class="row">
                <div class="col-md-12">
                    <h3 class="page-title orange">
						<div class="top-line"></div>
                        자주묻는질문(FAQ)
                    </h3>
                </div>
            </div>
            <div id="faq-nav">
                <div class="row">
                	<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                        <a href="<c:url value='/communication/faq/faqTotal'/>"  class="item <c:if test="${type == 'faqTotal'}">active</c:if>" >전체 FAQ</a> 
                	</div>
                	<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                        <a href="<c:url value='/communication/faq/faqScience'/>" class="item <c:if test="${type == 'faqScience'}">active</c:if>" >과학행사</a>
                	</div>
                	<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                        <a href="<c:url value='/communication/faq/faqEducation'/>" class="item <c:if test="${type == 'faqEducation'}">active</c:if>" >교육</a>
                	</div>
                	<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                        <a href="<c:url value='/communication/faq/faqRole'/>" class="item <c:if test="${type == 'faqRole'}">active</c:if>"  >비영리법인</a>
                	</div>
                	<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                        <a href="<c:url value='/communication/faq/faqFacilities'/>" class="item <c:if test="${type == 'faqFacilities'}">active</c:if>" >편의시설/주차</a>
                	</div>
                	<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                        <a href="<c:url value='/communication/faq/faqRestaurant'/>" class="item <c:if test="${type == 'faqRestaurant'}">active</c:if>" >식당</a>
                	</div>
                	<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                        <a href="<c:url value='/communication/faq/faqUser'/>" class="item <c:if test="${type == 'faqUser'}">active</c:if>" >연간회원</a>
                	</div>
                	<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                        <a href="<c:url value='/communication/faq/faqDisplay'/>" class="item <c:if test="${type == 'faqDisplay'}">active</c:if>" >전시 관람</a>
                	</div>
                	<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                        <a href="<c:url value='/communication/faq/faqReservation'/>" class="item <c:if test="${type == 'faqReservation'}">active</c:if>" >전시관 예약</a>
                	</div>
                	<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                        <a href="<c:url value='/communication/faq/faqHomepage'/>" class="item <c:if test="${type == 'faqHomepage'}">active</c:if>" >홈페이지</a>
                	</div>
                	<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                        <a href="<c:url value='/communication/faq/faqMarketing'/>" class="item <c:if test="${type == 'faqMarketing'}">active</c:if>" >홍보자료</a>
                	</div>
                     </div>
                </div>
            </div>
        </div>
    </div>