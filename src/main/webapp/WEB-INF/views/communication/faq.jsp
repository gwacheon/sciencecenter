<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<div class="sub-content-nav scrollspy-orange">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>

<div id="communication-library">
	
    <jsp:include page="_faqHeader.jsp"/>
    <div class="container">
        <div class="row">
				<div class="col-md-12">
					<div id="board-wrapper" class="table-orange">
			 			<c:url var="currentPath" value="/communication/faq/${type }"/>
			 			
			 			<c:import url="/WEB-INF/views/boards/list.jsp">
			 				<c:param name="path" value="${currentPath }"/>
			 				<c:param name="isFaq" value="faq"/>
			 			</c:import>
		 			</div>
				</div>
			</div>
    </div>
</div>