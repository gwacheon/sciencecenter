<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="sub-content-nav scrollspy-orange">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>

<div class="sub-body">
	<div id="basic-info-spy" class="narrow-sub-top communication">
		<div class="sub-banner-tab-wrapper sub-banner-tab-orange">
			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="support-body">
		<div class="container">
			<div class="section">
				<div class="row">
					<div class="col-md-12">
						<h3 class="page-title orange">
						<div class="top-line"></div>     
							12대 행동강령 실천의지
						</h3>
					</div>
					<div class="col-md-12">
						<ul class="circle-list orange">
							<li>
								멋있는 직원이 되는 7가지 방법
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="practical-will-wrapper">
							<div class="item">
								<span class="em">
									1. “할 수 있습니다.”
								</span>
								- 긍정적인 사람
							</div>
							<div class="item">
								<span class="em">
									2. “기꺼이 해 드리겠습니다.”
								</span>
								 - 적극적인 사람
							</div>
							<div class="item">
								<span class="em">
									3. “잘못된 것은 즉시 고치겠습니다.”
								</span>
								 - 겸허한 사람
							</div>
							<div class="item">
								<span class="em">
									4. “참 좋은 말씀입니다.”
								</span>
								 - 수용적인 사람
							</div>
							<div class="item">
								<span class="em">
									5. “대단히 고맙습니다.”
								</span>
								 - 감사할 줄 아는 사람
							</div>
							<div class="item">
								<span class="em">
									6. “도울 일 없습니까?”
								</span>
								 - 여유 있는 사람
							</div>
							<div class="item">
								<span class="em">
									7. “이 순간 할일이 무엇일까?”
								</span>
								 - 일을 찾아 할 줄 아는 사람
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>