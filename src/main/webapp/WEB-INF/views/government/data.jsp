<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="sub-content-nav scrollspy-teal">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>
<div id="government-body" class="sub-body">
	
	<div id="basic-info-spy" class="narrow-sub-top government">
		<div class="sub-banner-tab-wrapper sub-banner-tab-teal">
			<div class="container">
				<div class="row">
					
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			 <div class="col-sm-12 table-responsive">
			 	<p>
			 		<b>
			 			공공데이터의 제공 및 이용활성화에 관한 법률에 따라 공공데이터제공책임관(제12조)과 제공목록(제19조)을 아래와 같이 공표하며, 법률 제27조에 따라 제공목록에 포함되지 않은 공공데이터를 신청하실 수 있습니다.
			 		</b>
			 	</p>
			 	<table class="table table-teal centered-table">
					<thead>
						<tr>
							<th>
								구분
							</th>
							<th>
								부서 / 직위
							</th>
							<th>
								성명
							</th>
							<th>
								연락처
							</th>
							
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								공공데이터개방책임관
							</td>
							<td>
								전시기획연구과/과장
							</td>
							<td>
								권일찬
							</td>
							<td>
								02-3677-1380
							</td>
						</tr>
						<tr>
							<td>
								공공데이터제공실무담당자
							</td>
							<td>
								전시기획연구과/주무관
							</td>
							<td>
								이진영
							</td>
							<td>
								02-3677-1389
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>