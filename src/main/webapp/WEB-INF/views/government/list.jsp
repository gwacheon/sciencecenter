<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<div class="sub-content-nav scrollspy-teal">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>


<div id="government-body" class="sub-body">
	<div id="basic-info-spy" class="narrow-sub-top government">
		<div class="sub-banner-tab-wrapper sub-banner-tab-teal">
			<div class="container">
				<div class="row">
					
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			 <div class="col-sm-12">
			 	<p>
		 			<b>검색옵션을 이용하면 좀 더 상세한 정보목록을 검색할 수 있습니다.</b>
			 	</p>
			 </div>
			 <div class="col-sm-12">
			 	<iframe height="1400px" frameborder="0" width="100%" src="http://www.open.go.kr/pa/iframe/infoWonmun/cateSearch/orginList.do?iframeNstCd=1710000&iframeType=01&iframeWidth=01" title="정보목록"></iframe>
			 </div>
		</div>
	</div>
</div>