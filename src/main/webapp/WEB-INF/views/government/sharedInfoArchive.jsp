<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="sub-content-nav scrollspy-teal">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>
<div id="government-body" class="sub-body">
	<div id="basic-info-spy" class="narrow-sub-top government">
		<div class="sub-banner-tab-wrapper sub-banner-tab-teal">
			<div class="container">
				<div class="row">
					
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="page-title teal">
					<div class="top-line"></div>
					공개정보자료실
				</h3>
			</div>
		</div>
		<div class="row">
	 		<div class="col-md-12">
	 			<div id="board-wrapper" class="table-teal">
		 			<c:url var="currentPath" value="/government/sharedInfoArchive"/>
		 			
		 			<c:import url="/WEB-INF/views/boards/list.jsp">
		 				<c:param name="path" value="${currentPath }"/>
		 			</c:import>
	 			</div>
	 		</div>
		</div>
	</div>
</div>