<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="sub-content-nav scrollspy-teal">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>

<div id="government-body" class="sub-body">
	<div id="basic-info-spy" class="narrow-sub-top government">
		<div class="sub-banner-tab-wrapper sub-banner-tab-teal">
			<div class="container">
				<div class="row">
					
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			 <div class="col-sm-12 table-responsive">
			 	<table class="table table-teal centered-table">
					<thead>
						<tr>
							<th>
								주요업무
							</th>
							<th>
								목록
							</th>
							<th>
								내용
							</th>
							<th>
								공개시기
							</th>
							<th>
								담당과
							</th>
							<th>
								담당자
							</th>
							<th>
								연락처
							</th>
						</tr>
					</thead>
					<tbody>
										<tr>
											<td>정보공개 제도운영</td>
											<td>정보공개책임관 지정</td>
											<td><a href="sciencecenter/government/data" >정보공개책임관 지정현황</a></td>
											<td>수시</td>
											<td>운영지원과</td>
											<td>이재필 주무관</td>
											<td>☎ 1352</td>
										</tr>
										<tr>
											<td>공용차량 관리</td>
											<td>공용차량 정수현황</td>
											<td><a href="#" >연간 공용차량 정수 및 운영현황</a></td>
											<td>연1회</td>
											<td>운영지원과</td>
											<td>한종수 주무관</td>
											<td>☎ 1352</td>
										</tr>
										<tr>
											<td>업무추진비 사용</td>
											<td>업무추진비 사용 내역</td>
											<td><a href="#" >기관장 업무추진비 사용내역</a></td>
											<td>월1회</td>
											<td>운영지원과</td>
											<td>김지현 주무관</td>
											<td>☎ 1353</td>
										</tr>
                                        <tr>
											<td>업무추진비 사용</td>
											<td>업무추진비 사용 내역</td>
											<td><a href="#" >전시연구단장 업무추진비 사용내역</a></td>
											<td>월1회</td>
											<td>운영지원과</td>
											<td>김지현 주무관</td>
											<td>☎ 1353</td>
										</tr>
										<tr>
											<td>관련규정</td>
											<td>과천과학관 관련 규정</td>
											<td><a href="#" >과천과학관 운영관련 규정</a></td>
											<td>수시</td>
											<td>경영기획과</td>
											<td>심원무 사무관</td>
											<td>☎ 1311</td>
										</tr>
										<tr>
											<td>세입·세출예산 결산자료</td>
											<td>세입·세출예산 결산자료</td>
											<td><a href="#" >국립과천과학관 세입·세출예산 결산자료</a></td>
											<td>연1회</td>
											<td>운영지원과</td>
											<td>이영종 주무관</td>
											<td>☎ 1356</td>
										</tr>
										<tr>
											<td>수의계약현황</td>
											<td>수의계약현황</td>
											<td><a href="#" >수의계약현황</a></td>
											<td>월1회</td>
											<td>운영지원과</td>
											<td>이영종 주무관</td>
											<td>☎ 1356</td>
										</tr>
										<tr>
											<td>업무계획</td>
											<td>사업계획</td>
											<td><a href="#">2015년도 기관의 목표, 비전, 현황 </a></td>
											<td>연1회</td>
											<td>경영기획과</td>
											<td>강진영 주무관</td>
											<td>☎ 1315</td>
										</tr>
										<tr>
											<td>업무계획</td>
											<td>사업계획</td>
											<td><a href="#">기관의 사업계획</a></td>
											<td>연1회</td>
											<td>경영기획과</td>
											<td>강진영 주무관</td>
											<td>☎ 1315</td>
										</tr>
							
										<tr>
											<td>보도자료</td>
											<td>보도자료</td>
											<td><a href="#">과학관 행사, 교육, 특별전 등 과학관 주요사항 보도 내용</a></td>
											<td>수시</td>
											<td>고객창출과</td>
											<td>정지원 주무관</td>
											<td>☎ 1346</td>
										</tr>
										<tr>
											<td>고객서비스헌장</td>
											<td>고객서비스헌장 이행 결과</td>
											<td><a href="#">고객서비스 헌장의 서비스실천이행 관련 평가결과</a></td>
											<td>연1회</td>
											<td>고객창출과</td>
											<td>연세용 사무관</td>
											<td>☎ 1341</td>
										</tr>
										<tr>
											<td>관람객 통계</td>
											<td>연간방문객 현황<br>(지역별, 연령별 등)</td>
											<td><a href="#">국립과천과학관 연간방문객 현황<br>(지역별, 연령별 등)</a></td>
											<td>연1회</td>
											<td>고객창출과</td>
											<td>심인보 주무관</td>
											<td>☎ 1344</td>
										</tr>
										<tr>
											<td>과학문화행사</td>
											<td>과학문화</td>
											<td><a href="#">수학문화축전 개요, 참석방법 등 행사전반에 관한 내용</a></td>
											<td>연1회</td>
											<td>과학문화진흥과</td>
											<td>노한숙 주무관</td>
											<td>☎ 1428</td>
										</tr>
										<tr>
											<td>과학문화행사</td>
											<td>과학문화</td>
											<td><a href="#">SF과학축제 안내</a></td>
											<td>연1회</td>
											<td>과학문화진흥과</td>
											<td>이태우 연구사</td>
											<td>☎ 1425</td>
										</tr>
										<tr>
											<td>과학문화행사</td>
											<td>과학문화</td>
											<td><a href="#">어울림홀 공연 정보안내</a></td>
											<td>수시</td>
											<td>과학문화진흥과</td>
											<td>윤덕호 주무관</td>
											<td>☎ 1429</td>
										</tr>
										<tr>
											<td>교육</td>
											<td>과학교육</td>
											<td><a href="#">교육대상, 신청방법, 모집일정, 교육비 등 운영계획 및 안내</a></td>
											<td>수시</td>
											<td>과학탐구교육과</td>
											<td>최주한 주무관</td>
											<td>☎ 1523</td>
										</tr>
							
									</tbody>
				</table>
			</div>
		</div>
	</div>
</div>