<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div id="users-login-wrapper">
	<div id="login-banner">
		<h1 class="doc-title white center">국립과천과학관 통합회원</h1>
		<h5 class="doc-title white center">
			국립과천과학관은 통합회원제를 시행하고 있습니다.<br>
			국립과천과학관이 운영하는 다양한 홈페이지를 하나의 아이디로 편리하게 이용하실 수 있습니다.
		</h5>
		
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div id="login-form">
						<div class="row">
							<div class="col-md-12">
								<h3 class="doc-title teal center margin-bottom20">통합회원 로그인</h3>
							</div>
							
							<div class="col-md-8 col-md-offset-2">
								<c:if test="${not empty error and error != '' }">
									<div class="alert alert-danger">
										<c:if test="${error == 'NOT_EXIST_MEMBER' }">
											존재하지 않는 사용자 입니다.
										</c:if>
										
										<c:if test="${error == 'NOT_MATCH_PASSWORD' }">
											비밀번호가 일치하지 않습니다.
										</c:if>
									</div>
								</c:if>
							
								<c:url value="/users/login" var="loginUrl" />
								<form:form action="${loginUrl}" method="POST">
									<div class="form-group">
									    <label for="email">
				      						<i class="fa fa-user"></i> 아이디
				      					</label>
									    <input id="memberId" type='text' name='memberId' autofocus="autofocus" class="form-control">
									</div>
									
									<div class="form-group">
									    <label for="password">
				      						<i class="fa fa-lock"></i> <spring:message code="user.auth.password"/>
				      					</label>
									    <input id="password" type='password' name='password' autocomplete="off" class="form-control">
									</div>
									
									<div class="form-group">
										<div class="row">
											<div class="col-xs-6">
												<div class="center checkbox checkbox-primary">
													<input type="checkbox" id="remember-id" name="remember-id" class="styled"/>
													<label for="remember-id"><spring:message code="user.auth.remomberId"/></label>
						                        </div>
					                        </div>
					                        
					                        <div class="col-xs-6">
												<div class="center checkbox checkbox-primary">
													<input name="rememberMe" class="styled"  type="checkbox" id="remember-me"/>
							                        <label for="remember-me"><spring:message code="user.auth.remomberMe"/></label>
						                        </div>
					                        </div>
										</div>
									</div>
									
									<div class="form-group center">
										<button class="btn btn-primary wide-btn" type="submit" name="action">
								        	로그인 하기
								      	</button>
									</div>
								</form:form>
							</div>
							
							<div class="col-md-12">
								<div class="line gray margin-bottom30"></div>
							</div>
						</div>
						
						<div class="row margin-bottom20">
							<div class="col-md-10">
								<h6 class="doc-title vertical-middle-btn">
									아직 <span class="teal">국립과천과학관 통합 회원</span>이 아니세요?
									
									국립과천과학관의 다양한 서비스를 만나보세요!
								</h6>
							</div>
							
							<div class="col-md-2">
								<a class="btn btn-white right" href="<c:url value='/users/signupIntro'/>" >
						        	회원가입
						      	</a>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-12">
								<div class="line gray margin-bottom30"></div>
							</div>
							
							<div class="col-md-6">
								<h6 class="doc-title vertical-middle-btn">
									<span class="teal">아이디와 비밀번호가</span> 기억나지 않으세요?
								</h6>
							</div>
							
							<div class="col-md-6 clearfix">
								<a class="btn btn-white right margin-left20" href="<c:url value='/users/findPassword'/>" >
						        	비밀번호 찾기
						      	</a>
						      	
						      	<a class="btn btn-white right" href="<c:url value='/users/findId'/>" >
						        	아이디 찾기
						      	</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>