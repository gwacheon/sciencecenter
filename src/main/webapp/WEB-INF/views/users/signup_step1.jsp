<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div id="auth-form" class="container">
	<div class="row">
		<ul id="breadcrumb">
		  	<li class="col s2">
		  		<a href="#">
		  			회원가입
				</a>
	  		</li>
		  	<li class="col s2">
		  		<a href="#" class="active">
		  			1. 회원인증
		  		</a>
		  	</li>
		  	
		  	<li class="col s2">
		  		<a href="#">
		  			2. 회원정보입력
		  		</a>
		  	</li>
		  	<li class="col s2">
		  		<a href="#">
		  			3. 회원가입완료
		  		</a>
		  	</li>
		</ul>
	</div>
	<div class="row">
		<div class="col offset-l1 l5 s12">
			<div class="card-panel">
				<span class="card-title">
					본인 인증 회원가입
				</span>
		      	<p>
		      		아이핀, 주민등록번호 인증 시 신용평가 기관을 통하여 실명확인을 진행하며, 실명확인 용도 외 별도 저장되지 않습니다.<br>
		      		<a href="<c:url value='/users/signup_step2'/>" class="btn">
		      			인증하기
	      			</a>
		      	</p>
			</div>
		</div>
		<div class="col l5 s12">
			<div class="card-panel">
				<span class="card-title">
					공공 아이피 인증
				</span>
		      	<p>
		      		국립과천과학관은 공공 I-PIN과 전혀 관계가 없으며 관련 문의는 공공 I-PIN콜센터 (02-818-3050)로 하시기 바랍니다.<br>
		      		<a href="<c:url value='/users/signup_step2'/>" class="btn">
		      			인증하기
	      			</a>
		      	</p>
			</div>
		</div>
	</div>
</div>