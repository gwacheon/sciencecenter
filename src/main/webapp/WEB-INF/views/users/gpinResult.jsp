<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="gov.mogaha.gpin.sp.proxy.*" %>
<%@page import="kr.co.mainticket.Utility.*"%>
<%@page import="kr.co.mainticket.Entity.Entity"%>
<%@page import="kr.co.mainticket.Science.Interlock.Member.Check"%>
<%@page import="kr.co.mainticket.Science.Config.EncManager"%>
<%
	response.addHeader("Cache-Control", "private");

	String MemberName = "";
	String GenderFlag = "";
	String RegNoDate = "";
	String CertifyType = "A00004";
	String CertifyKey = "";
	String DuplicateKey = "";
	
	String GpinCertifyType = Utility.CheckNull((String)session.getAttribute("GPIN_CERTIFY_TYPE"));
	
	boolean CheckFlag = false;
	String CheckMsg = "";

	try{
		//HOST
		String GPinClientHost = "localhost";
		// PORT
		String GPinClientPort = "80";
		// TOMCAT_CONTEXT
		String GPinClientContext = "";
		// HTTP Timeout
		int GPinClientTimeout = 15000;
		
		GPinProxy proxy = GPinProxy.getInstance(this.getServletConfig().getServletContext());
		//out.println("11111111111111");
		//proxy.Init(GPinClientHost + ":" + GPinClientPort, GPinClientContext, GPinClientTimeout);
		//out.println("2222222222222");
		String[] AttrNames = new String[] {"dupInfo", "virtualNo", "realName", "sex", "age", "birthDate", "nationalInfo", "authInfo", "GPIN_AQ_SERVICE_SITE_USER_CONFIRM"}; 
		
		String SamlResponse = request.getParameter("SAMLResponse");
		
		//out.println("33333333333333");
		String[] AttrData = proxy.parseSAMLResponse(SamlResponse, AttrNames);
		//proxy.parseSAMLResponse(request, session);
		//out.println("444444444444444");
		String DupInfo = AttrData[0];
		String VirtualNo = AttrData[1];
		MemberName = AttrData[2];
		
		if(Utility.StringToInt(AttrData[3]) % 2 == 1){
			GenderFlag = "M";
		}
		else{
			GenderFlag = "F";
		}
		//out.println("5555555555555");
		String Age = AttrData[4];
		String BirthDate = AttrData[5];
		String NationalInfo = AttrData[6];
		String AutoInfo = AttrData[7];
		String GpinAqServiceSiteUserConfirm = AttrData[8];
		//System.out.println("66666666666666");
		RegNoDate = BirthDate;
		CertifyType = "A00004";
		CertifyKey = VirtualNo;
		DuplicateKey = DupInfo;
		
		if(GpinCertifyType.equals("MEMBER")){
			String CompanyCd = "110062";
			
			Entity DataEntity = new Entity(1);
			
			DataEntity.setField("CERTIFY_TYPE", CertifyType);
			DataEntity.setField("DUPLICATE_KEY", DuplicateKey);
		
			Check mgr = new Check();
		
			boolean DuplicateFlag = mgr.CheckDuplicate(CompanyCd, DataEntity);
			
			if(DuplicateFlag){
				CheckFlag = true;
			}
			else{
				String RspCd = mgr.getErrCode();
				String RspMsg = mgr.getErrMessage();
				
				if(RspMsg.equals("EXIST_DUPLICATE_KEY")){
					CheckMsg = "이미 가입된 정보가 있습니다.";
				}
				else{
					CheckMsg = "잠시 후 다시 이용해 주세요.\\n[" + RspCd + ", " + RspMsg + "]";
				}
			}
		}
		else if(GpinCertifyType.equals("PW")){
			CheckFlag = true;
		}
	}catch(Exception e){
		e.printStackTrace();
		CheckFlag = false;
		CheckMsg = "잠시 후 다시 이용해 주세요.";
	}
	
	// Initialize for Algorithm Duplicate(GPin, DB)
	try{
		EncManager.getEncData("TM_MEMBER", "MEMBER_TEL", "INIT");
		System.out.println("GPIN Init");
	}catch(Exception e){
	}
%>

<script type="text/javascript">
		window.onload = function(){
			try{
<%
	if(CheckFlag){
		if(GpinCertifyType.equals("MEMBER")){
%>
				opener.document.getElementById("MEMBER_NAME").value = "<%=MemberName%>";
				opener.document.getElementById("REG_NO_DATE").value = "<%=RegNoDate%>";
				opener.document.getElementById("GENDER_FLAG").value = "<%=GenderFlag%>";
				opener.document.getElementById("CERTIFY_TYPE").value = "<%=CertifyType%>";
				opener.document.getElementById("CERTIFY_KEY").value = "<%=CertifyKey%>";
				opener.document.getElementById("DUPLICATE_KEY").value = "<%=DuplicateKey%>";
				
				var FormObj = opener.document.getElementById("MemberForm");
				
				FormObj.submit();
				window.close();
<%
		}
		else if(GpinCertifyType.equals("PW")){
%>
			var form = GetObject("LoginForm");
			
			form.action = "/gnsm_member/html/member/SearchPwAct.jsp";
			
			form.submit();
<%
		}
	}
	else{
%>				<%=request.getParameter("SAMLResponse")%>
				alert("<%=CheckMsg%>");
				//window.close();
<%
	}
%>
			}catch(e){
				//alert(e.Message);
				<%=request.getParameter("SAMLResponse")%>
				alert("메인화면이 닫혔습니다. 다시 시도해 주십시오.");
				//window.close();
			}
		}
	</script>