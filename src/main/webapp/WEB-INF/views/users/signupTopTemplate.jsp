<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<h1 class="doc-title white center">
	국립과천과학관 통합회원
</h1>
<h5 class="doc-title white center">
	국립과천과학관은 통합회원제를 시행하고 있습니다.<br>
	국립과천과학관이 운영하는 다양한 홈페이지를 하나의 아이디로 편리하게 이용하실 수 있습니다.
</h5>
<div class="center">
	<a href="<c:url value='/guide/paidMember?scrollspyName=total-member-spy'/>" class="benefit-link"
		target="_blank">
		<i class="fa fa-user"></i> 통합회원 혜택 자세히보기
	</a>
</div>