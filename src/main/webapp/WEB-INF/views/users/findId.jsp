<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div id="users-login-wrapper">
	<div id="login-banner">
		<c:import url="/WEB-INF/views/users/signupTopTemplate.jsp"/>
		<div id="auth-box" class="container">
			<div class="row">
				<div id="" class="auth-card col-xs-12 col-md-10 col-md-offset-1">
					<div class="row">
						<div class="col-md-12 auth-item-box">
							<h4 class="center margin-bottom20">
								통합회원 아이디 찾기
							</h4>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<h5 class="certiication-title center">
								회원님의 <span>이름</span>과 <span>이메일</span>을 입력해 주시기 바랍니다.
							</h5>
						</div>
						<div class="col-md-12 center margin-bottom20">
							국립과천과학관은 회원님의 개인정보를 신중히 취급합니다.<br>
							가입하신 정보는 회원님의 동의없이 공개되지 않으며, 개인정보 보호정책의 보호를 받습니다.<br>
							<i class="fa fa-check"></i> 는 필수 입력 사항입니다.
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<div class="signup-form-wrapper">
								<c:url var="action" value="/users/findId"/>
								<form:form id="find-id-form"  class=""
									action="${action}" method="POST" commandName="member" modelAttribute="member">
									
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<form:label path="memberName">
													<i class="fa fa-check"></i> 이름
												</form:label>
												<form:input path="memberName" required="true" class="form-control"/>
											</div>
										</div>
									</div>
									
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<form:label path="memberEmail">
													<i class="fa fa-check"></i> 이메일
												</form:label>
												<form:input path="memberEmail" required="true" class="form-control" tyep="email"/>
											</div>
										</div>
									</div>
									
									<div class="row">
										<div class="col-md-12">
											<div id="id-description" class="center margin-bottom20">
											
											</div>
										</div>
									</div>
									
									<div class="row">
										<div class="col-md-12 center">
											<button id="find-id-btn" type="button" class="btn btn-primary btn-wide">
												찾기
											</button>
										</div>
									</div>
								</form:form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$("#find-id-btn").click(function(e){
		var name = $("#memberName").val();
		var email = $("#memberEmail").val();
		
		if (name != "" && email != ""){
			$.ajax({
				url: baseUrl + "users/findId",
				beforeSend: function(xhr) {
					ajaxLoading();
					xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));
				},
				contentType : 'application/json',
		        type: "POST",
		        data: JSON.stringify({"memberName": name, "memberEmail": email}),
		        success: function(data){
		        	ajaxUnLoading();
		        	if (data.member != null) {
		        		if (data.member.rspCd == "0000") {
		        			var idText = "회원님의 아이디는 " + data.member.memberId + " 입니다.";
		        			$("#id-description").text(idText);
		        		} else if (data.member.rspCd == "SI04") {
		        			alert("입력하신 정보에 해당하는 사용자 정보가 없습니다. 이름과 이메일을 다시 확인하시기 바랍니다.");
		        		}
		        		console.log(data);
		        	}
		        },
		        error: function(err){
		        	ajaxUnLoading();
		        	console.log(err);
		        }
			})
		} else {
			alert("이름과 이메일을 입력하시기 바랍니다.");
		}
	});
</script>