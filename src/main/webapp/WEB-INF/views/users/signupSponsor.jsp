<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<div id="users-login-wrapper">
	<div id="login-banner">
		<c:import url="/WEB-INF/views/users/signupTopTemplate.jsp"/>
		<div id="auth-box" class="container">
			<div class="row">
				<div id="" class="auth-card col-xs-12 col-md-10 col-md-offset-1">

					<div class="row">
						<div class="col-md-12">
							<h5 class="certiication-title center">
								<span>후원회원 전환 신청</span>
							</h5>
						</div>
						<div class="col-md-12 center margin-bottom20">
							국립과천과학관은 회원님의 개인정보를 신중히 취급합니다.<br>
							가입하신 정보는 회원님의 동의없이 공개되지 않으며, 개인정보 보호정책의 보호를 받습니다.<br>
						</div>
						<div class="col-md-12">
							<div class="signup-form-wrapper">
								<c:url var = "action" value="/users/signup"/>
								<form:form id="signup-form"  class=""
									action="${action}" method="POST" commandName="user" modelAttribute="user">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<form:label path="name">이름</form:label>
												<form:input path="name" required="true" class="form-control"/>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<form:label path="phone">본인 휴대폰</form:label>
												<form:input path="phone" required="true" class="form-control" placeholder="본인 휴대폰(-없이 입력)"/>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>국적</label>
												<div class="radio radio-teal">
													<input type="radio" name="local" id="local" value="local" checked>
													<label for="local">내국인</label>
													<input type="radio" name="foreigner" id="foreigner" value="foreigner">
													<label for="foreigner">외국인</label>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>성별</label>
												<div class="radio radio-teal">
													<input type="radio" name="mail" id="male" value="mail" checked>
													<label for="male">남자</label>
													<input type="radio" name="female" id="female" value="female">
													<label for="female">여자</label>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<div class="col-md-12">
												<label>생년월일</label>
											</div>
											<div class="col-md-5">
												<div class="form-inline form-group">
													<select class="form-control teal">
														<option value="0">선택</option>
														<c:forEach var="i" begin="1940" end="2000">
															<option value="${i }">${i }</option>
														</c:forEach>
													</select>
													<label>년</label>
													<select class="form-control teal">
														<option value="0">선택</option>
														<c:forEach var="i" begin="1" end="12">
															<option value="${i }">${i }</option>
														</c:forEach>
													</select>
													<label>월</label>
													<select class="form-control teal">
														<option value="0">선택</option>
														<c:forEach var="i" begin="1" end="31">
															<option value="${i }">${i }</option>
														</c:forEach>
													</select>
													<label>일</label>
												</div>
											</div>
											<div class="col-md-4">
												<div class="radio radio-teal">
													<input type="radio" name="inlineRadioOptions" id="dob-sun" value="dob-sun" checked>
													<label for="dob-sun">양력</label>
													<input type="radio" name="inlineRadioOptions" id="dob-moon" value="option2">
													<label for="dob-moon">음력</label>
												</div>
												<div class="radio radio-teal">
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<form:label path="email">이메일</form:label>
												<form:input path="email" required="true" class="form-control" placeholder="이메일"/>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label>기부금영수증 발급여부</label>
												<p class="help-block">
													국세청 연말정산간소화서비스 기부금영수증 발급을 위한 주민등록번호 수집 및 이용에 동의하십니까?
												</p>
												<div class="radio radio-teal">
													<input type="radio" name="receipt-yes" id="receipt-yes" value="receipt-yes">
													<label for="receipt-yes">예</label>
													<input type="radio" name="receipt-no" id="receipt-no" value="receipt-no" checked>
													<label for="receipt-no">아니오</label>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label>약관동의</label>
												<p class="help-block">
													서비스 이용약관 <a href="#">전문보기</a>
												</p>
												<div class="radio radio-teal">
													<input type="radio" name="service-agree-yes" id="service-agree-yes" value="service-agree-yes">
													<label for="service-agree-yes">동의합니다.</label>
													<input type="radio" name="service-agree-no" id="service-agree-no" value="service-agree-no" checked>
													<label for="service-agree-no">동의하지 않습니다.</label>
												</div>
												<p class="help-block">
													개인정보 처리방침 <a href="#">전문보기</a>
												</p>
												<div class="radio radio-teal">
													<input type="radio" name="privacy-agree-yes" id="privacy-agree-yes" value="privacy-agree-yes">
													<label for="privacy-agree-yes">동의합니다.</label>
													<input type="radio" name="privacy-agree-no" id="privacy-agree-no" value="privacy-agree-no" checked>
													<label for="privacy-agree-no">동의하지 않습니다.</label>
												</div>
												<p class="help-block">
													개인정보의 수집 및 이용 <a href="#">전문보기</a>
												</p>
												<div class="radio radio-teal">
													<input type="radio" name="privacy-collect-agree-yes" id="privacy-collect-agree-yes" value="privacy-collect-agree-yes">
													<label for="privacy-collect-agree-yes">동의합니다.</label>
													<input type="radio" name="privacy-collect-agree-no" id="privacy-collect-agree-no" value="privacy-collect-agree-no" checked>
													<label for="privacy-collect-agree-no">동의하지 않습니다.</label>
												</div>
												<p class="help-block">
													개인정보의 제3자 제공 <a href="#">전문보기</a>
												</p>
												<div class="radio radio-teal">
													<input type="radio" name="privacy-give-agree-yes" id="privacy-give-agree-yes" value="privacy-give-agree-yes">
													<label for="privacy-give-agree-yes">동의합니다.</label>
													<input type="radio" name="privacy-give-agree-no" id="privacy-give-agree-no" value="privacy-give-agree-no" checked>
													<label for="privacy-give-agree-no">동의하지 않습니다.</label>
												</div>
											</div>
											
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<h4>후원방법선택</h4>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<div class="checkbox checkbox-teal">
													<input name="same-user-info" id="same-user-info-checkbox" class="styled" type="checkbox" value="same-user-info">
													<label for="same-user-info">후원자정보와 동일</label>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label>결제방법</label>
												<div class="radio radio-teal">
													<input type="radio" name="payment-credit" id="payment-credit" value="payment-credit" checked>
													<label for="payment-credit">자동이체(CMS)</label>
													<input type="radio" name="payment-cms" id="payment-cms" value="payment-cms">
													<label for="payment-cms">신용카드</label>
													<a href="#" class="btn btn-primary">
														결제방법 안내
													</a>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label>은행</label>
												<select class="form-control">
													<option value="0">선택</option>
												</select>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<form:label path="name">계좌번호</form:label>
												<form:input path="name" type="text" required="true" class="form-control validate" 
													placeholder="'-'을 빼고 입력해주세요"/>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label>정기후원일</label>
												<select class="form-control">
													<option value="0">선택</option>
												</select>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<form:label path="name">예금주 성명</form:label>
												<form:input path="name" type="text" required="true" class="form-control validate" />
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">										
											<p class="help-block">
												* 첫 후원금은 후원신청 즉시 결제되며, 다음 달부터 정기후원일에 후원금이 출금됩니다.
											</p>
											<p class="help-block">
												* 단, 금융서비스 제한시간(23:30~00:30) 후원 시, 익일 이후 업무시간에 후원금이 결제됩니다.<br>
												(경남은행은 주말 신청 시, 평일 업무시간 결제)
											</p>
											<p class="help-block">
												* 후원 신청 후 해당 은행에서 자동이체 등록승인 문자가 중복 안내될 수 있습니다.
											</p>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label>CMS 약관동의</label>
												<p class="help-block">
													개인정보의 수집 및 이용 동의 <a href="#">전문보기</a>
												</p>
												<div class="radio radio-teal">
													<input type="radio" name="privacy-collect-agree-yes" id="privacy-collect-agree-yes" value="privacy-collect-agree-yes">
													<label for="privacy-collect-agree-yes">동의합니다.</label>
													<input type="radio" name="privacy-collect-agree-no" id="privacy-collect-agree-no" value="privacy-collect-agree-no" checked>
													<label for="privacy-collect-agree-no">동의하지 않습니다.</label>
												</div>
												<p class="help-block">
													개인정보의 제3자 제공 동의<a href="#">전문보기</a>
												</p>
												<div class="radio radio-teal">
													<input type="radio" name="privacy-give-agree-yes" id="privacy-give-agree-yes" value="privacy-give-agree-yes">
													<label for="privacy-give-agree-yes">동의합니다.</label>
													<input type="radio" name="privacy-give-agree-no" id="privacy-give-agree-no" value="privacy-give-agree-no" checked>
													<label for="privacy-give-agree-no">동의하지 않습니다.</label>
												</div>
											</div>
											
										</div>
									</div>
									<div class="row margin-top30">
										<div class="col-md-12 center">
											<a id="signup-cancel-btn" href="#" class="btn btn-primary btn-wide">
												가입 취소
											</a>
											<a id="move-home-btn" href="<c:url value='/' />" style="display:none;">
												Home
											</a>
											<button id="signup-btn" type="submit" class="btn btn-primary btn-wide">
												후원회원 가입
											</button>
											<a id="next-btn" href="<c:url value='/users/signupSponsorComplete' />" style="display:none;">
												next
											</a>
										</div>
									</div>
								</form:form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	
	$("#signup-cancel-btn").on("click", function(e) {
		e.preventDefault();
		if( confirm("후원회원 가입을 정말로 취소하시겠습니까?") ) {
			$("#move-home-btn")[0].click();	
		} 
	});
	
	$("#signup-btn").on("click", function(e) {
		console.log("signup clicked")
		e.preventDefault();
		$("#next-btn")[0].click();
	});
</script>