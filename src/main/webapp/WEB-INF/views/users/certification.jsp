<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@page import="kr.co.mainticket.Utility.*"%>
<%@page import="kr.co.mainticket.Entity.*"%>
<%@page import="com.sci.comm.secu.SciSecuManager"%>
<%@page import="com.sci.comm.secu.aes.SciHttpSecuX"%>

<div id="users-login-wrapper">
	<div id="login-banner">
		<c:import url="/WEB-INF/views/users/signupTopTemplate.jsp"/>
		<div id="auth-box" class="container">
			<div class="row">
				<div id="" class="auth-card col-xs-12 col-md-10 col-md-offset-1">
					<div class="row">
						<div class="col-md-12 auth-item-box">
							<h4 class="center margin-bottom20">
								통합회원 가입
							</h4>
						</div>
						
						<div class="col-xs-6 col-md-2-5">
							<div class="auth-steps active">
								01. 본인인증
							</div>
						</div>
						
						<div class="col-xs-6 col-md-2-5">
							<div class="auth-steps">
								02. 이용약관동의
							</div>
						</div>
						
						<div class="col-xs-6 col-md-2-5">
							<div class="auth-steps">
								03. 개인정보동의
							</div>
						</div>
						
						<div class="col-xs-6 col-md-2-5">
							<div class="auth-steps">
								04. 회원정보입력
							</div>
						</div>
						
						<div class="col-xs-6 col-md-2-5">
							<div class="auth-steps">
								05. 회원가입완료
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<h5 class="certiication-title center">
								 <span>회원가입 본인인증</span> 
							</h5>
						</div>
						<div class="col-md-12 center">
							아이핀, 휴대폰 인증시 신용평가 기관을 통하여 실명확인을 진행하며,<br>
							실명확인 용도 외 별도 저장 되지 않습니다.
						</div>
						<div class="col-md-offset-3 col-md-9 margin-top30 margin-bottom30">
							<a class="btn btn-primary btn-wide center" onclick="CertifyIPin('MEMBER'); return false;" >
					        	아이핀 인증
					      	</a>
					      	<a class="btn btn-primary btn-wide center" onclick="javascript:CertifyNamePhone(); return false" >
					        	휴대폰 인증
					      	</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<form id="CertifyNameFormPhone" name="CertifyNameFormPhone" method="post" action="">
	<input type="hidden" name="reqInfo" value="${reqInfo }" />
	<input type="hidden" name="retUrl" value="32http://www.sciencecenter.go.kr/scipia/gnsm_member/html/GPin/phoneAuthResponse.jsp" />
</form>
<form id="MemberForm" action = "<c:url value="/users/signup/privacy" />">
 	<input type="hidden" id="MEMBER_NAME"/>
 	<input type="hidden" id="REG_NO_DATE"/>
 	<input type="hidden" id="DUPLICATE_KEY"/>
</form>

<form id="gpin-form" name="gpin-form" method = "post" action="<c:url value='/users/signup/myPinAuthRequestResult'/>">
	
</form>

<script type="text/javascript">
	
	function CertifyNamePhone(){
		window.open('', 'CertifyNamePhone', 'width=460, height=480, resizable=0, scrollbars=no, status=0, titlebar=0, toolbar=0, left=300, top=200');
		
		var form = document.getElementById("CertifyNameFormPhone");
		
		form.action = 'https://pcc.siren24.com/pcc_V3/jsp/pcc_V3_j10.jsp'; // 가상식별 실명확인서비스 URL
		form.target = 'CertifyNamePhone';
		
		form.submit();
	}
	
	function CertifyIPin(CertifyType){
		window.open("myPinAuthRequest?CERTIFY_TYPE=" + CertifyType, "CertifyIPin", "directories=no,toolbar=no,width=450,height=550");
	}
	
</script>
