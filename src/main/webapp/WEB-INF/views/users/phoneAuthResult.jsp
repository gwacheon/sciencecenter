<%@page import="kr.co.mainticket.Science.Interlock.Member.Check"%>
<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="kr.co.mainticket.Utility.*"%>
<%@page import="kr.co.mainticket.Entity.*"%>
<%@page import="com.sci.comm.secu.SciSecuManager"%>
<%@page import="com.sci.comm.secu.aes.SciHttpSecuX"%>

<%
	request.setCharacterEncoding("UTF-8");
	Box params = HttpUtility.getBox(request);
	String CertifyType = "A00005";
	String CheckMsg = "";
	boolean CheckFlag = false;
	
    // 변수 --------------------------------------------------------------------------------
    String retInfo		= "";																// 결과정보

	String name			= "";                                                               //성명
	String sex			= "";																//성별
	String birYMD		= "";																//생년월일
	String fgnGbn		= "";																//내외국인 구분값
	
    String di			= "";																//DI
    String ci1			= "";																//CI
    String ci2			= "";																//CI
    String civersion    = "";                                                               //CI Version
    
    String reqNum		= "";                                                               // 본인확인 요청번호
    String result		= "";                                                               // 본인확인결과 (Y/N)
    String certDate		= "";                                                               // 검증시간
    String certGb		= "";                                                               // 인증수단
	String cellNo		= "";																// 핸드폰 번호
	String cellCorp		= "";																// 이동통신사
	String addVar		= "";


	//복화화용 변수
	String encPara		= "";
	String encMsg		= "";
	String msgChk       = "N";  
	
    //-----------------------------------------------------------------------------------------------------------------
    
	try{
		String cookiereqNum = (String)session.getAttribute("CertReqNum");
	
		retInfo = params.getString("retInfo").trim();
		 // 1. 암호화 모듈 (jar) Loading
        com.sci.v2.pcc.secu.SciSecuManager sciSecuMg = new com.sci.v2.pcc.secu.SciSecuManager();
        //쿠키에서 생성한 값을 Key로 생성 한다.
        retInfo  = sciSecuMg.getDec(retInfo, cookiereqNum);

        // 2.1차 파싱---------------------------------------------------------------
        String[] aRetInfo1 = retInfo.split("\\^");

		encPara  = aRetInfo1[0];         //암호화된 통합 파라미터
        encMsg   = aRetInfo1[1];    //암호화된 통합 파라미터의 Hash값
		
		String  encMsg2   = sciSecuMg.getMsg(encPara);
			// 3.위/변조 검증 ---------------------------------------------------------------
        if(encMsg2.equals(encMsg)){
            msgChk="Y";
        }

		if(msgChk.equals("N")){
%>
		    <script language=javascript>
            alert("비정상적인 접근입니다.!!<%=msgChk%>");
		    </script>
<%
			return;
		}


        // 복호화 및 위/변조 검증 ---------------------------------------------------------------
		retInfo  = sciSecuMg.getDec(encPara, cookiereqNum);

        String[] aRetInfo = retInfo.split("\\^");
		
        name		= aRetInfo[0];
		birYMD		= aRetInfo[1];
        sex			= aRetInfo[2];        
        fgnGbn		= aRetInfo[3];
        di			= aRetInfo[4];
        ci1			= aRetInfo[5];
        ci2			= aRetInfo[6];
        civersion	= aRetInfo[7];
        reqNum		= aRetInfo[8];
        result		= aRetInfo[9];
        certGb		= aRetInfo[10];
		cellNo		= aRetInfo[11];
		cellCorp	= aRetInfo[12];
        certDate	= aRetInfo[13];
		addVar		= aRetInfo[14];
		
			
		
		String CompanyCd = "110062";
		
		Entity DataEntity = new Entity(1);
		
		DataEntity.setField("CERTIFY_TYPE", CertifyType);
		DataEntity.setField("DUPLICATE_KEY", di);
	
		Check mgr = new Check();
	
		boolean DuplicateFlag = mgr.CheckDuplicate(CompanyCd, DataEntity);
		
		if(DuplicateFlag){
			CheckFlag = true;
		}
		else{
			String RspCd = mgr.getErrCode();
			String RspMsg = mgr.getErrMessage();
			
			if(RspMsg.equals("EXIST_DUPLICATE_KEY")){
				CheckMsg = "이미 가입된 정보가 있습니다.";
			}
			else{
				CheckMsg = "잠시 후 다시 이용해 주세요.\\n[" + RspCd + ", " + RspMsg + "]";
			}
		}
	}catch(Exception e){
		e.printStackTrace();
		CheckFlag = false;
		CheckMsg = "인증 복호화 오류";
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>국립과천과학관 통합 회원가입</title>
	<meta name="title" content="국립과천과학관 통합 회원가입" />
	<meta name="keywords" content="국립과천과학관 통합 회원가입" />
	<meta name="description" content="국립과천과학관 통합 회원가입을 위한 사이트입니다. " />
	
	<script src="/C2S/comm.js" type="text/javascript"></script>
	
	<script type="text/javascript">
		window.onload = function(){
			try{
<%
	if(CheckFlag){
%>
				opener.document.getElementById("MEMBER_NAME").value = "<%=name%>";
				opener.document.getElementById("REG_NO_DATE").value = "<%=birYMD%>";
				opener.document.getElementById("GENDER_FLAG").value = "<%=sex%>";
				opener.document.getElementById("CERTIFY_TYPE").value = "<%=CertifyType%>";
				opener.document.getElementById("CERTIFY_KEY").value = "<%=di%>";
				opener.document.getElementById("DUPLICATE_KEY").value = "<%=di%>";
				
				var FormObj = opener.document.getElementById("MemberForm");
				
				FormObj.submit();
<%
	}
	else{
%>
				alert("<%=CheckMsg%>");
<%
	}
%>
				//window.close();
			}catch(e){
				//alert(e.Message);
				alert("메인화면이 닫혔습니다. 다시 시도해 주십시오.");
				window.close();
			}
		}
	</script>
</head>
<body>
</body>
</html>