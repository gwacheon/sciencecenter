<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<div id="users-login-wrapper">
	<div id="login-banner">
		<h1 class="doc-title white center">'과학에 대한 재미와 즐거움을 마음껏 누릴 수 있는'</h1>
		<h2 class="doc-title white center margin-bottom20">
			국립과천과학관에 오신 것을 환영합니다.
		</h2>
		<h5 class="doc-title white center">
			온라인회원(통합회원)가입은 무료이며, 후원회원 또는 연간회원 가입 시 보다 다양한 혜택을 받으실 수 있습니다.
		</h5>
		
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div id="login-form">
						<div class="row">
							<div class="col-md-6 col-lg-4 auth-item-box long-box">
								<h3 class="doc-title teal">
									연간회원
								</h3>
								<h5 class="doc-title">
									<span class="teal">국립과천과학관 연간회원</span>이 되시면...
								</h5>
								
								<ol>
									<li>과학관 상설전시관 입장료 면제</li>
									<li>유/무상의 교육 및 과학행사 프로그램 참여 우선권 부여</li>
									<li>유상프로그램 참여시 교육비 20% 할인</li>
									<li>외부 위탁기관이 주관하는 유료 특별기획전 관람료 할인(별도공지)</li>
									<li>천체 관측소 관람 시 20% 할인</li>
								</ol>
								
								<div class="alert-msg">
									※ 연간회원 가입 전 환불 정산 내용 필독 (자세히 보기 클릭)
								</div>
								
								<div class="btns margin-top20">
									<a class="btn btn-teal reverse" 
										href="<c:url value='/guide/paidMember'/>" 
										target="_blank">
							        	자세히 보기
							      	</a>
							      	<a href="<c:url value='/mypage/updateMemberShip' />" class="btn btn-primary">
							      		가입하기
							      	</a>
							      	<a id="move-signup-sponsor" class="btn btn-teal" 
							      		href="<c:url value='/users/signupSponsor'/>" style="display:none;">
							        	회원가입
							      	</a>
								</div>
							</div>
							
							<div class="col-md-6 col-lg-4 auth-item-box long-box">
								<h3 class="doc-title teal">
									온라인회원(무료)
								</h3>
								<h5 class="doc-title">
									<span class="teal">국립과천과학관 온라인 회원</span>이 되시면...
								</h5>
								
								<div>
									과천과학과의 온라인 예약 서비스를 이용한<br>
									다양한 예약 및 커뮤니티 이용 등<br>
									다양한 서비스를 제공 받을 수 있습니다.<br>
									또한 국립과천과학관의 온라인회원 아이디 하나(통합회원 아이디)로<br>
									국립과천과학관이 운영하는 다양한 홈페이지의 서비스를<br>
									편리하게 이용하실 수 있습니다.		
								</div>
								
								<div class="btns margin-top20">
							      	<a class="btn btn-teal" href="<c:url value='/users/signup/certification'/>" >
							        	가입하기
							      	</a>
								</div>
							</div>
						
						
							<div class="col-md-6 col-lg-4 auth-item-box">
								<h3 class="doc-title teal">
									후원회원
								</h3>
								<h5 class="doc-title">
									<span class="teal">국립과천과학관 후원회원</span>이 되시면...
								</h5>
								<div class="alert-msg">
									※ 후원회원 가입 후 온라인회원에 가입하시어 후원회원 전환하시면 다양한 혜택을 받으실 수 있습니다. (자세히 보기 클릭)		
								</div>
								
								<div class="btns margin-top20">
									<a class="btn btn-teal reverse" 
										href="<c:url value='/guide/fundMember'/>" 
										target="_blank">
							        	자세히 보기
							      	</a>
							      	<a class="btn btn-teal" target="_blank" href="http://scienlove.or.kr" >
							        	가입하기
							      	</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>