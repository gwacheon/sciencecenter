<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@page language="java" session="true" %>

<div id="users-login-wrapper">
	<div id="login-banner">
		<c:import url="/WEB-INF/views/users/signupTopTemplate.jsp"/>
		<div id="auth-box" class="container">
			<div class="row">
				<div id="" class="auth-card col-xs-12 col-md-10 col-md-offset-1">
					<div class="row">
						<div class="col-md-12 auth-item-box">
							<h4 class="center margin-bottom20">
								통합회원 가입
							</h4>
						</div>
						
						<div class="col-xs-6 col-md-2-5">
							<div class="auth-steps">
								01. 본인인증
							</div>
						</div>
						
						<div class="col-xs-6 col-md-2-5">
							<div class="auth-steps">
								02. 이용약관동의
							</div>
						</div>
						
						<div class="col-xs-6 col-md-2-5">
							<div class="auth-steps">
								03. 개인정보동의
							</div>
						</div>
						
						<div class="col-xs-6 col-md-2-5">
							<div class="auth-steps active">
								04. 회원정보입력
							</div>
						</div>
						
						<div class="col-xs-6 col-md-2-5">
							<div class="auth-steps">
								05. 회원가입완료
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<h5 class="certiication-title center">
								<span>회원정보 입력</span>
							</h5>
						</div>
						<div class="col-md-12 center margin-bottom20">
							국립과천과학관은 회원님의 개인정보를 신중히 취급합니다.<br>
							가입하신 정보는 회원님의 동의없이 공개되지 않으며, 개인정보 보호정책의 보호를 받습니다.<br>
							<i class="fa fa-check"></i> 는 필수 입력 사항입니다.
							
						</div>
						<div class="col-md-12">
							<div class="signup-form-wrapper">
								<c:url var="action" value="/users/signup"/>
								<form:form id="signup-form"  class=""
									action="${action}" method="POST" commandName="member" modelAttribute="member">
									<form:input type="hidden" path="duplicateKey" value="<%=session.getAttribute("DUPLICATE_KEY")%>"/>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<form:label path="memberName"><i class="fa fa-check"></i>이름</form:label>
												<form:input path="memberName" required="true" class="form-control" value="<%=session.getAttribute("auth-name")%>" disabled/>

											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<form:label path="memberId"><i class="fa fa-check"></i>아이디</form:label>
												<div class="form-inline">
												<form:input path="memberId" type="text" required="true" class="form-control validate" />
												<a id="id-check-btn" href="#" class="btn btn-primary">
													중복확인
												</a> 
												</div>
											</div>
											<p id="validEmailHelpBlock" class="help-block has-success hide">
												사용가능한 ID 입니다.
											</p>
											<p id="invalidEmailHelpBlock" class="help-block has-error hide">
												이미 존재하는 ID 입니다.
											</p>
											<p id="emptyEmailHelpBlock" class="help-block has-error hide">
												ID를 입력해주세요.
											</p>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<form:label path="password"><i class="fa fa-check"></i>비밀번호</form:label>
												<form:input type="password" path="password" required="true" class="form-control" maxlength="20"/>
											</div>
											<p id="invalidPasswordHelpBlock" class="help-block has-error hide">
												비밀번호를 잘못 입력 하였습니다. 비밀번호와 비밀번호 확인을 동일하게 입력하셔야 됩니다.
											</p>											
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label for="passwordConfirmation"><i class="fa fa-check"></i>비밀번호 확인</label>
												<input type="password" id="passwordConfirmation" class="form-control">
											</div>
										</div>
										<div class="col-md-12">										
											<p class="help-block">
												* 영문대소문자, 숫자, 특수문자(!,@,$,%,^,&,*만 가능)를 모두 1번 이상씩 사용하여 조합(9~14자리 이내)
											</p>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label for="postcode">우편번호</label>
												<div class="form-inline">
													<input id="postcode" name="memberZipCd" type="text" class="form-control">
												</div>
											</div>
										</div>
										<div class="col-md-6">
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<form:label path="memberAddr1">주소</form:label>
												<form:input path="memberAddr1" required="true" class="form-control"/>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<form:label path="memberAddr2">상세주소</form:label>
												<form:input path="memberAddr2" required="true" class="form-control"/>
											</div>	
										</div>
									</div>
									
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<form:label path="memberCel"><i class="fa fa-check"></i>본인 휴대폰</form:label>
												<form:input path="memberCel" required="true" type ="tel "class="form-control phone-number-check" placeholder="본인 휴대폰(-없이 입력)"/>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<form:label path="memberEmail"><i class="fa fa-check"></i>본인 이메일</form:label>
												<form:input path="memberEmail" required="true" class="form-control" placeholder="본인 이메일"/>
											</div>
										</div>
									</div>
									
									<div class="row">
										<div class="col-md-12">
											<label><i class="fa fa-check"></i>생년월일</label>
										</div>
										<div class="col-md-5">
											<div class="form-inline form-group">
												<form:hidden path="birthDate"/>
												<select id="birth-year" class="form-control teal">
													<option value="">선택</option>
													<jsp:useBean id="now" class="java.util.Date" />
													<fmt:formatDate var="currentYear" value="${now}" pattern="yyyy" />
													<c:set var="defaultYear" value="${currentYear - 20 }"></c:set>
													<c:forEach var="y" begin="0" end="120">
														<option value="${currentYear - y}"
															<c:if test="${y == 20 }">selected</c:if>>${currentYear -y }</option>
													</c:forEach>
												</select>
												<label>년</label>
												<select id="birth-month" class="form-control teal">
													<option value="">선택</option>
													<c:forEach var="i" begin="1" end="12">
														<option value="<fmt:formatNumber type="number" minIntegerDigits="2" value="${i}" />">
															${i }
														</option>
													</c:forEach>
												</select>
												<label>월</label>
												<select id="birth-day" class="form-control teal">
													<option value="">선택</option>
													<c:forEach var="i" begin="1" end="31">
														<option value="<fmt:formatNumber type="number" minIntegerDigits="2" value="${i}" />">
															${i }
														</option>
													</c:forEach>
												</select>
												<label>일</label>
											</div>
										</div>
									</div>
									
									<div class="row">
										<div class="col-md-12">
											<label>정보수신여부</label>
										</div>
									</div>
									<div class="row">
										<div class="col-md-2">
											<div class="checkbox checkbox-teal">
												<input name="emailFlag" id="emailFlag" class="styled" type="checkbox">
												<form:label path="emailFlag">이메일</form:label>
											</div>
										</div>
										<div class="col-md-2">
											<div class="checkbox checkbox-teal">
												<input name="dmFlag" id="dmFlag" class="styled" type="checkbox">
												<form:label path="dmFlag">뉴스레터</form:label>
											</div>
										</div>	
										<div class="col-md-2">
											<div class="checkbox checkbox-teal">
											  	<input name="smsFlag" id="smsFlag" class="styled" type="checkbox">
												<form:label path="smsFlag">문자수신</form:label>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">										
											<p class="help-block">
												* 국립과천과학관의 다양한 서비스를 받아보실 수 있습니다.<br>
												(교육예약 및 결제 확인, 이벤트, 과학정보 등을 받아 보실 수 있습니다.)
											</p>
										</div>
									</div>
									
									<div class="row margin-top30">
										<div class="col-md-12 center">
											<a id="signup-cancel-btn" href="#" class="btn btn-primary btn-wide">
												가입 취소
											</a>
											<a id="move-home-btn" href="<c:url value='/' />" style="display:none;">
												Home
											</a>
											<button id="signup-btn" type="submit" class="btn btn-primary btn-wide">
												가입하기
											</button>
											<a id="next-btn" href="<c:url value='/users/signupComplete' />" style="display:none;">
												next
											</a>
										</div>
									</div>
								</form:form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var isDuplicateId = true;
	$("#memberName").prop('readonly', true);
	$("#signup-cancel-btn").on("click", function(e) {
		e.preventDefault();
		if( confirm("회원가입을 정말로 취소하시겠습니까?") ) {
			$("#move-home-btn")[0].click();	
		} 
	});
	
	function checkForm(){
		var alertMsg = "";
		var focusId = "";
		
		if(isDuplicateId) {
			alertMsg = "아이디 중복 체크를 확인해주세요.";
		}
		
		if($("#memberName").val() == ""){
			alertMsg = "이름을 입력하세요.";
			focusId = "#memberName";
		}
		
		var passwordCheck = checkPassword($("#password").val(), $("#passwordConfirmation").val());
		
		if (passwordCheck["err"]) {
			alertMsg = passwordCheck["errMsg"];
			focusId = "#password";
		}
		
		if($("#memberCel").val() == ""){
			alertMsg = "휴대폰번호를 입력하세요.";
			focusId = "#memberCel";
		}
		
		if($("#memberEmail").val() == ""){
			alertMsg = "이메일 주소를 입력하세요.";
			focusId = "#memberEmail";
		}
		
		if($("#birth-year").val() == "" || $("#birth-month").val() == "" || $("#birth-day").val() == "") {
			alertMsg = "생년월일을 입력하세요.";
			focusId = "#birth-year";
		}else {
			var birthDate = $("#birth-year").val() + $("#birth-month").val() + $("#birth-day").val()
			$("#birthDate").val(birthDate);
		}
		
		if ( alertMsg != "" ){
			alert(alertMsg);
			$(focusId).focus();
			return false;
		} else {
			return true;
		}
	}
	
	$("#signup-btn").on("click", function(e) {
		e.preventDefault();
		
		if(checkForm()) {
			$("#signup-form").submit();
		}
	});
	
	$("#id-check-btn").click(function(e){
		e.preventDefault();
		var id = $("#memberId").val();
		
		if(id == ""){
			alert("아이디를 입력하셔야 합니다.");
		}else {
			$.getJSON("<c:url value='/users/signup/checkId'/>", {memberId: id}, function(data){
				var member = data.member;
				console.log(member);
				if(member.usableFlag) {
					if (confirm("사용 가능한 ID 입니다. 입력하신 ID로 사용하시겠습니까?")){
						isDuplicateId = false;
						$("#memberId").prop('readonly', true);
					}
				} else {
					alert("이미 사용중인 ID 입니다. 다른 ID를 입력하시기 바랍니다.");
				}
			});
		}
	});
</script>

<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script>
	var openDaumApi = false;
	$("#postcode").focus(function(e){
		if (!openDaumApi){
			openDaumApi = true;
			
			new daum.Postcode({
		        oncomplete: function(data) {
		        	console.log(data);
		        	$("#postcode").val(data.zonecode);
		        	$("#memberAddr1").val(data.roadAddress).prop('readonly', true);
		        	$("#memberAddr2").focus();
		        	
		        	openDaumApi = false;
		        }
		    }).open();
		}	
	});
</script>


<script type="text/javascript">
	
	
$(function(){
    $(".phone-number-check").on('keydown', function(e){
       // 숫자만 입력받기
        var trans_num = $(this).val().replace(/-/gi,'');
	var k = e.keyCode;
				
	if(trans_num.length >= 11 && ((k >= 48 && k <=126) || (k >= 12592 && k <= 12687 || k==32 || k==229 || (k>=45032 && k<=55203)) ))
	{
  	    e.preventDefault();
	}
    }).on('blur', function(){ // 포커스를 잃었을때 실행합니다.
        if($(this).val() == '') return;

        // 기존 번호에서 - 를 삭제합니다.
        var trans_num = $(this).val().replace(/-/gi,'');
      
        // 입력값이 있을때만 실행합니다.
        if(trans_num != null && trans_num != '')
        {
           // 총 핸드폰 자리수는 11글자이거나, 10자여야 합니다.
           if(trans_num.length==11 || trans_num.length==10) 
           {   
               // 유효성 체크
               var regExp_ctn = /^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})([0-9]{3,4})([0-9]{4})$/;
               if(regExp_ctn.test(trans_num))
               {
                   // 유효성 체크에 성공하면 하이픈을 넣고 값을 바꿔줍니다.
                   trans_num = trans_num.replace(/^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})-?([0-9]{3,4})-?([0-9]{4})$/, "$1-$2-$3");                  
                   $(this).val(trans_num);
               }
               else
               {
                   alert("유효하지 않은 전화번호 입니다.");
                   $(this).val("");
                   $(this).focus();
               }
           }
           else 
           {
               alert("유효하지 않은 전화번호 입니다.");
               $(this).val("");
               $(this).focus();
           }
      }
  });  
});
</script>
