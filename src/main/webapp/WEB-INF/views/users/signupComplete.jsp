<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<div id="users-login-wrapper">
	<div id="login-banner">
		<c:import url="/WEB-INF/views/users/signupTopTemplate.jsp"/>
		<div id="auth-box" class="container">
			<div class="row">
				<div id="" class="auth-card col-xs-12 col-md-10 col-md-offset-1">
					<div class="row">
						<div class="col-md-12 auth-item-box">
							<h4 class="center margin-bottom20">
								통합회원 가입
							</h4>
						</div>
						
						<div class="col-xs-6 col-md-2-5">
							<div class="auth-steps">
								01. 본인인증
							</div>
						</div>
						
						<div class="col-xs-6 col-md-2-5">
							<div class="auth-steps">
								02. 이용약관동의
							</div>
						</div>
						
						<div class="col-xs-6 col-md-2-5">
							<div class="auth-steps">
								03. 개인정보동의
							</div>
						</div>
						
						<div class="col-xs-6 col-md-2-5">
							<div class="auth-steps">
								04. 회원정보입력
							</div>
						</div>
						
						<div class="col-xs-6 col-md-2-5">
							<div class="auth-steps active">
								05. 회원가입완료
							</div>
						</div>
					</div>
					<div id="signup-complete-box">
						<div class="row">
							<div class="col-md-3">
								<div class="left-image">
									<img src="<c:url value='/resources/img/users/complete_character1.png' />" class="img-responsive" alt="complete-image1" />
								</div>
							</div>
							<div class="col-md-6">
								<div class="row">
									<div class="col-md-12">
										<h5 class="certiication-title center">
											<span>홍길동님의<br>국립과천과학관</span>
											 회원가입을 축하드립니다!
										</h5>
									</div>
									<div class="col-md-12 center margin-bottom20">
										로그인하셔서 많은 혜택을 누리시길 바랍니다.
									</div>
									<div class="col-md-12 center">
										<a href="<c:url value='/users/login'/>" class="btn btn-primary btn-wide">
											로그인하기
										</a>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="right-image">
									<img src="<c:url value='/resources/img/users/complete_character2.png' />" class="img-responsive" alt="complete-image1" />
								</div>
							</div>
							
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
