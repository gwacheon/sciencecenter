<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div id="auth-form" class="container">
	<div class="row">
		<ul id="breadcrumb">
		  	<li class="col s2">
		  		<a href="#">
		  			회원가입
				</a>
	  		</li>
		  	<li class="col s2">
		  		<a href="#" >
		  			1. 회원인증
		  		</a>
		  	</li>
		  	
		  	<li class="col s2">
		  		<a href="#" class="active">
		  			2. 회원정보입력
		  		</a>
		  	</li>
		  	<li class="col s2">
		  		<a href="#">
		  			3. 회원가입완료
		  		</a>
		  	</li>
		</ul>
	</div>
	<div class="row">
		<div class="card col m6 offset-m3"> 
			<h3>회원가입</h3>
			<c:url var = "action" value="/users/signup"/>
			<form:form id="signup-form"  class=""
				action="${action}" method="POST" commandName="user" modelAttribute="user">
				<div class="row">
					<div class="input-field col s12">
						<form:input path="email" type="email" required="true" class="validate" /> 
						<form:label path="email">이메일</form:label>
						<p id="validEmailHelpBlock" class="help-block has-success hide">
							사용가능한 이메일 입니다.
						</p>
						<p id="invalidEmailHelpBlock" class="help-block has-error hide">
							이미 존재하는 이메일입니다.
						</p>
						<p id="emptyEmailHelpBlock" class="help-block has-error hide">
							이메일을 입력해주세요.
						</p>
						<p id="emailFormHelpBlock" class="help-block has-error hide">
							잘못된 이메일 형식입니다.
						</p>
					</div>
				</div>
				
				<div class="row">
					<div class="input-field col s12">
						<form:input type="password" path="password" required="true" class="form-control" maxlength="20"/>
						<form:label path="password">비밀번호</form:label>
						<p class="help-block">
							비밀번호는 최대 20자까지 입력하실 수 있습니다.
						</p>
						
						<p id="invalidPasswordHelpBlock" class="help-block has-error hide">
							비밀번호를 잘못 입력 하였습니다. 비밀번호와 비밀번호 확인을 동일하게 입력하셔야 됩니다.
						</p>
					</div>
				</div>
				
				<div class="row">
					<div class="input-field col s12">
						<form:input type="password" path="passwordConfirmation" required="true" class="form-control" maxlength="20"/>
						<form:label path="passwordConfirmation">비밀번호 확인</form:label>
					</div>
				</div>
				
				<div class="row">
					<div class="input-field col s12">
						<form:input path="name" required="true" class="form-control"/>
						<form:label path="name">이름</form:label>
					</div>
				</div>
				
				<div class="row">
					<div class="input-field col s12">
						<form:input path="phone" required="true" class="form-control"/>
						<form:label path="phone">연락처</form:label>
					</div>
				</div>
				
				<div class="row">
					<div class="input-field col s6">
						<form:label path="zipcodeNew">우편번호</form:label>
						<form:input path="zipcodeNew" required="true" class="form-control"/>
					</div>
					<div class="input-field col s6">
						<button id = "openDaumApi" type="button" class="btn waves-effect waves-light">우편번호</button>
					</div>
					<div class="input-field col s12">
						<form:label path="address1">주소</form:label>
						<form:input path="address1" required="true" class="form-control"/>
					</div>
					<div class="input-field col s12">
						<form:label path="address2">상세주소</form:label>
						<form:input path="address2" required="true" class="form-control"/>
					</div>
				</div>
				
				<div class="row">
					<div class="input-field col s12 check-privacy">
						개인정보동의
						<pre>
							<jsp:include page="./privacy.jsp" />
						</pre>
						<p>
					      <input type="checkbox" id="privacy-check" />
					      <label for="privacy-check">동의</label>
					    </p>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12 check-privacy">	
						정보이용약관동의
						<pre>
							<jsp:include page="./information_confirm.jsp" />
						</pre>
						<p>
					      <input type="checkbox" id="info-check" />
					      <label for="info-check">동의</label>
					    </p>
					</div>
				</div>
				<div class="actions">
					<button id="submit_btn" type="button" class="btn waves-effect waves-light">
						회원가입 
					</button>
				</div>
			</form:form>
		</div>
	</div>
</div>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js?autoload=false"></script>
<script type="text/javascript">
	$(function(){
		$('#submit_btn').on('click',function(){
			if(!$('#privacy-check').is(":checked")){
				alert('개인정보동의에 동의해주세요.');
			}else if(!$('#info-check').is(":checked")){
				alert('정보이용약관동의에 동의해주세요.');
			}else{
				$('#signup-form').submit();
			}
		});
		
		$("#email").blur(function(){
			checkEmail();
		});
		
		$('#openDaumApi').on('click',function(){
			openDaumApi();
		})
		
	})
	
	function checkEmail(){
		var email = $('#email').val();
		if(is_blank(email)){
			$('#emptyEmailHelpBlock').removeClass('hide');
			$('#invalidEmailHelpBlock').addClass('hide');
			$('#validEmailHelpBlock').addClass('hide');
			$('#emailFormHelpBlock').addClass('hide');
			
		}else if(is_email(email)){
			$('#emptyEmailHelpBlock').addClass('hide');
			$('#invalidEmailHelpBlock').addClass('hide');
			$('#validEmailHelpBlock').addClass('hide');
			$('#emailFormHelpBlock').addClass('hide');
			$.getJSON(baseUrl + "users/" + email + "/check.json", function(data){
				if(data.email === undefined){
					$('#validEmailHelpBlock').removeClass('hide');
				}else{
					$('#invalidEmailHelpBlock').removeClass('hide');
				}
			});	
		}else{
			$('#emptyEmailHelpBlock').addClass('hide');
			$('#invalidEmailHelpBlock').addClass('hide');
			$('#validEmailHelpBlock').addClass('hide');
			$('#emailFormHelpBlock').removeClass('hide');
		}
	}
	
	
	function openDaumApi(){
		daum.postcode.load(function(){
			new daum.Postcode({
		        oncomplete: function(data) {
		            $('#zipcodeNew').val(data.zonecode);
		            
		            $('#address1').val(data.roadAddress);
		            $('#address2').focus();
		            
		            $("label[name='zipcodeNew']").addClass('active');
		            $("label[name='address1']").addClass('active');
		        }
		    }).open();	
		})
		
	}
</script>