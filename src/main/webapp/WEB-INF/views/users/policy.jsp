<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<div id="users-login-wrapper">
	<div id="login-banner">
		<c:import url="/WEB-INF/views/users/signupTopTemplate.jsp"/>
		<div id="auth-box" class="container">
			<div class="row">
				<div id="" class="auth-card col-xs-12 col-md-10 col-md-offset-1">
					<div class="row">
						<div class="col-md-12 auth-item-box">
							<h4 class="center margin-bottom20">
								통합회원 가입
							</h4>
						</div>
						
						<div class="col-xs-6 col-md-2-5">
							<div class="auth-steps">
								01. 본인인증
							</div>
						</div>
						
						<div class="col-xs-6 col-md-2-5">
							<div class="auth-steps active">
								02. 이용약관동의
							</div>
						</div>
						
						<div class="col-xs-6 col-md-2-5">
							<div class="auth-steps">
								03. 개인정보동의
							</div>
						</div>
						
						<div class="col-xs-6 col-md-2-5">
							<div class="auth-steps">
								04. 회원정보입력
							</div>
						</div>
						
						<div class="col-xs-6 col-md-2-5">
							<div class="auth-steps">
								05. 회원가입완료
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<h5 class="certiication-title center">
								<span>국립과천과학관 이용약관</span>
							</h5>
						</div>
						<div class="col-md-12 center margin-bottom20">
							회원가입을 하시기 전에 아래 국립과천과학관 이용약관을 충분히 검토하시기 바랍니다.
						</div>
						<div class="col-md-12">
							<div class="privacy-box">
								<c:out value="${policy.content }" escapeXml="false" />
								
								<a href="<c:url value='/communication/policies'/>" target="_blank">
									자세히보기
								</a>
							</div>
						</div>
						<div class="col-md-12 center">
							<div class="checkbox checkbox-teal">
								<input name="agreement-checkbox" id="agreement-checkbox" class="styled" type="checkbox" value="xx">
								<label for="agreement-checkbox">이용약관에 동의합니다.</label>
							</div>
						</div>
						<div class="col-md-12 center">
							<a id="next-btn" href="<c:url value='/users/signup/privacy'/>" class="btn btn-primary btn-wide" disabled>
								다음 단계
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	
	$("#agreement-checkbox").on("change", function(e) {
		console.log("checkbox clicked");
		if( $(this).is(":checked") ) {
			console.log("it's checked");
			$("#next-btn").removeAttr("disabled");
		}
		else {
			console.log("it's un-checked");
			$("#next-btn").attr("disabled", "disabled");
		}
		
	});
	
	$("#next-btn").on("click", function(e) {
		if( $("#agreement-checkbox").is(":checked") == false ) {
			e.preventDefault();
		}
	});
</script>