﻿<%@ page import='java.util.* , java.io.*'%>
<%@include file="MaFpsCommon.jsp"%>
<%!
  public static String kLog(String _path, String _msg) {
	  String strLogPath = "log.dat";
    _path += strLogPath; //생성할 파일명을 전체경로에 결합
        			   
    try{
                    BufferedWriter fw=new BufferedWriter(new FileWriter(new File(_path)));
                    fw.write(_msg); //파일에다 작성
                    fw.write("\r\n\r\n"); //파일에다 작성
                    fw.flush();
                    fw.close(); //파일핸들 닫기
                
                  }catch (IOException e) { 
                    System.out.println(e.toString()); //에러 발생시 메시지 출력
                  }
                  
    return _msg;
	}
  public String Hash(String str, String hashtype)
    {
       java.security.MessageDigest sh = null;
       String SHA = ""; 
       try{
        sh =  java.security.MessageDigest.getInstance(hashtype); 
        sh.update(str.getBytes()); 
        byte byteData[] = sh.digest();
        StringBuffer sb = new StringBuffer(); 
        for(int i = 0 ; i < byteData.length ; i++){
         sb.append(Integer.toString((byteData[i]&0xff) + 0x100, 16).substring(1));
        }
        SHA = sb.toString();
        
       }catch(Exception e){
        SHA = e.toString();
       }
       return SHA;
    
    }	
%>
<%
  String requestFileNameAndPath = request.getParameter("fn");
  String requestFileServerIp = request.getParameter("fs");  
  String requestDatFile = request.getParameter("prtdat");
	String id_str=session.getId();
  //out.println(id_str);
  String filePath = strDownFolder;
  int iPrtDatUpdate = 0;
  
	if(requestFileNameAndPath == null)
	{
	  if(requestDatFile != null)
  	{
  	  //filePath = request.getRealPath(strPrtDatDownFolder) + "/"; //파일을 생성할 전체경로
		  filePath = strPrtDatDownFolder;
  	  requestFileNameAndPath = "MaPrintInfoEPSmain.dat";
		  iPrtDatUpdate = 1;
  	}
	}
	else
	{
	  if(requestFileNameAndPath.length() > 17 || !requestFileNameAndPath.matches("-?\\d+(\\.\\d+)?"))
	    return;
	    
	    //userid 
    //String strUserID = (String)session.getAttribute("userid");  
  
    //MD5, MD4, SHA-1, SHA-256, SHA-512 등 사용 가능.
    //String strHashID = Hash(strUserID, "MD5");
   // requestFileNameAndPath += strHashID + ".matmp";
   
   String strParamCookie = (String)session.getAttribute("strCookie");
		strParamCookie += requestFileNameAndPath + ".matmp";
		requestFileNameAndPath = strParamCookie;
	}
	  
    //kLog(strDownFolder, strUserID);
            	
  InputStream in = null;
  OutputStream os = null;
  File file = null;
  int     iFileSize = 0;
  String  strOnlyFileName         = new String();
      
	//os = response.getOutputStream();
    // NAS를 사용하거나 프린터 파일 저해상도 파일을 받아올때
   if(iUseNas == 1 || requestFileNameAndPath == null)
  { 
  try{
    //out.println(filePath);
    // 파일을 읽어 스트림에 담기
    file = new File(filePath, requestFileNameAndPath);
	
    if(file.isFile () )
    {  
      iFileSize = (int)file.length();
      strOnlyFileName = file.getName();
		if(iPrtDatUpdate != 1)
		{
			strOnlyFileName += ".matmp";
		}     
		out.clear();
		out = pageContext.pushBody();
		//out.println(out);
		response.reset();
		response.setHeader("Content-Transfer-Encoding", "binary");
		response.setHeader("Pragma","no-cache;");
		response.setHeader("Expires", "-1;");
		response.setContentType("application/x-msdownload");
		response.setHeader("Content-Disposition", "attachment; fileName=" + strOnlyFileName+ ";");
		response.setContentLength((int)iFileSize);
    }
    
    in = new FileInputStream(file);
    os = response.getOutputStream();
    
    byte b[] = new byte[(int)file.length()+1];
    int leng = 0;
    
    while( (leng = in.read(b)) > 0 ){
    os.write(b,0,leng);
    }
  }catch (IOException e) { 
    String[] sArray1 = e.toString().split(" "); 
    out.println(sArray1[0]); //에러 발생시 메시지 출력
    return;
  }finally {
    if(in != null)
      in.close();
    if(os != null)
      os.close();
    
    if(requestDatFile == null && file != null)
      file.delete();
  }
  }
	else {
	
		//strFileServerIp
		byte[] byteIP = MaBase64Utils.base64Decode( requestFileServerIp);
		strFileServerIp = new String(byteIP);
		out.println("strFileServerIp =" +strFileServerIp );
		// create instance
		masavefile clMaSaveFile = new masavefile();

		// Call Ma2DCode library
		try
		{
			String  strRet = clMaSaveFile.strGetMetaFile (
        			                strFileServerIp,
        			                iFileServerPort,
        			                requestFileNameAndPath,
        			                "",
                                    "" );

    		String	strRetCode = strRet.substring( 0, 5 );
    		String	strRetData = strRet.substring( 5 );
    		int		iRetCode = Integer.parseInt( strRetCode );

    		out.println("requestFileNameAndPath =" + requestFileNameAndPath);
    		out.println("strRetCode =" + strRetCode);
    		    		
    		// Success ...
	    	if( iRetCode == 0 )
	        {    
				//out.clear();
				os = response.getOutputStream();
			    byte[] byteRetData = strRetData.getBytes();
	            os.write( byteRetData, 0, byteRetData.length  ); 

    	        System.out.println( "[SUCESS]" );
    	    }
    	    else
    	    {
    	        System.out.println( "[FAIL]" );
    	    }
		}
		catch (IOException e) { 
			System.out.println(e.toString()); //에러 발생시 메시지 출력
			out.println(e.toString());
		}finally {
			if(os != null)
			  os.close();			
		}	
			
	}   
%>