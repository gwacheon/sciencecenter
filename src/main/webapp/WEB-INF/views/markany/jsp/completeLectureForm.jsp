<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt" %>
<%@ taglib prefix='fn' uri='http://java.sun.com/jsp/jstl/functions'%>
<%@ taglib uri="http://www.markany.com/epagesafer" prefix="MarkAnyFps"%>

<%@include file="./MaFpsCommon.jsp"%>
<MarkAnyFps:MarkAnyHtmlData beanName="MaFpsHtmlContent">

<!-- step1. end  -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link rel="stylesheet" type="text/css" href="http://www.sciencecenter.go.kr/scipia/resources/css/application.css">
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title>수료증</title>
<style type="text/css">
	.table-label{
		margin-left: 80px;
	}
</style>
</head>
	<body>
		<div id="voluntary-confirmation-form">
			<div class="confirmation-logo">
				<img src="http://www.sciencecenter.go.kr/scipia/resources/img/main_a/logo_kr.png" class="img-responsive" alt="국립과천과학관" />
			</div>
			<div class="title">
				수료증
			</div>
			<div class="description" style="width:100%">
				<span class="table-label">일련&nbsp;번호</span>제 ${lecture.CERTIFICATE_NO } 호<br/><br/>
		 		<span class="table-label">학&nbsp;교&nbsp;명</span> ${lecture.SCHOOL_NAME } ${lecture.GRADE_NAME }학년 ${lecture.CLASS_NAME }반<br/><br/>
		 		<span class="table-label">성&nbsp;&nbsp;&nbsp;명</span>  ${lecture.STUDENT_NAME }<br/><br/>
		 		<span class="table-label">교육과정명</span>  ${lecture.COURSE_NAME }<br/><br/>
		 		<span class="table-label">교육&nbsp;기간</span> 
		 		<c:if test="${lecture.SCHEDULE_FLAG eq 'Y'}">
		 			${fn:substring(lecture.COURSE_DATE,0,4)} 년 
		 			${fn:substring(lecture.COURSE_DATE,4,6)} 월 
		 			${fn:substring(lecture.COURSE_DATE,6,8)} 일 
		 		</c:if>
		 		<c:if test="${lecture.SCHEDULE_FLAG != 'Y'}">
		 			${fn:substring(lecture.COURSE_START_DATE,0,4)} 년 
		 			${fn:substring(lecture.COURSE_START_DATE,4,6)} 월 
		 			${fn:substring(lecture.COURSE_START_DATE,6,8)} 일 ~
		 			${fn:substring(lecture.COURSE_END_DATE,0,4)} 년 
		 			${fn:substring(lecture.COURSE_END_DATE,4,6)} 월 
		 			${fn:substring(lecture.COURSE_END_DATE,6,8)} 일 
		 		</c:if>
		 		<c:if test="${lecture.CERTIFICATE_HOURS != ''}">
		 			(${lecture.CERTIFICATE_HOURS} 시간)
		 		</c:if>
		 			<br/><br/>
		 			<div style="text-align: center; font-size: 20px;margin-top:50px;">
		 				위 사람은 국립과천과학관이 운영하는 상기 교육<br/>
		 				과정을 성실히 이수하였으므로 이 증서를 수여합니다.
		 			</div>
	 			<div style=" text-align: center; margin-top: 100px; font-size:25px;">
					<jsp:useBean id="now" class="java.util.Date" />
			 		<fmt:formatDate pattern="yyyy년 MM월 dd일" value="${now }" />
		 		</div>
		 		<div style="text-align: center;margin-bottom: 50px;">
			 		<img style="    position: absolute;left: 200px; top:640px"src="http://www.sciencecenter.go.kr/scipia/resources/img/markany/confirmation_name.png" class="img-responsive" alt="국립과천과학관" />
			 		<img style="margin-left: 200px;" src="http://www.sciencecenter.go.kr/scipia/resources/img/markany/confirmation_sign.gif" class="img-responsive" alt="국립과천과학관" />
		 		</div>
			</div>
		</div>
		<!-- 페이지 구분자 -->
		<!-- MarkAny Page Gubun -->
		<!-- 페이지 구분자 end -->
	</body>
</html>
</MarkAnyFps:MarkAnyHtmlData>
<!-- step2. include  -->
		<%
		    //String	strHtmlData = out.toString();
		    //out = ((matostring) out).getOldJspWriter();
		    
			//byte	byteReadHtmlData[] = strHtmlData.getBytes ("UTF-8"); 
			//int 	iReadHtmlDataSize = byteReadHtmlData.length;        
			String  strHtmlData = MaFpsHtmlContent.getHtmlContent();
	byte	byteReadHtmlData[] = strHtmlData.getBytes("utf-8");
	int 	iReadHtmlDataSize = byteReadHtmlData.length; 
		%> 
		<%@include file="./MaFpsTail.jsp"%>
		<script language="javascript">
		function resize_window(){
		      window.resizeTo(800,730);
		  }
		  window.onload = function() {
		   resize_window();
		  }
		</script>
		<!-- step2. end  -->