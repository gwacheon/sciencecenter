<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt" %>
<%@ taglib prefix='fn' uri='http://java.sun.com/jsp/jstl/functions'%>
<%@ taglib uri="http://www.markany.com/epagesafer" prefix="MarkAnyFps"%>

<%@include file="./MaFpsCommon.jsp"%>

<MarkAnyFps:MarkAnyHtmlData beanName="MaFpsHtmlContent">
<%
     //out = matostring.newInstance(out);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link rel="stylesheet" type="text/css" href="http://www.sciencecenter.go.kr/scipia/resources/css/application.css">
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title>봉사활동 확인서</title>
</head>
	<body>
		<div id="voluntary-confirmation-form">
			<div class="confirmation-logo">
				<img src="http://www.sciencecenter.go.kr/scipia/resources/img/main_a/logo_kr.png" class="img-responsive" alt="국립과천과학관" />
			</div>
			<div class="title">
				봉사활동 확인서
			</div>
			<div class="description">
				<span class="table-label">발급&nbsp;번호</span>${lecture.PREVENTION_NO }<br/><br/>
		 		<span class="table-label">학&nbsp;교&nbsp;명</span> ${lecture.SCHOOL_NAME } ${lecture.GRADE_NAME }학년 ${lecture.CLASS_NAME }반<br/><br/>
		 		<span class="table-label">성&nbsp;&nbsp;&nbsp;명</span>  ${lecture.STUDENT_NAME }<br/><br/>
		 		<span class="table-label">기&nbsp;&nbsp;&nbsp;   간</span>
		 			<c:if test="${lecture.SCHEDULE_FLAG eq 'Y'}">
			 			${fn:substring(lecture.COURSE_DATE,0,4)} 년 
			 			${fn:substring(lecture.COURSE_DATE,4,6)} 월 
			 			${fn:substring(lecture.COURSE_DATE,6,8)} 일 
			 		</c:if>
			 		<c:if test="${lecture.SCHEDULE_FLAG != 'Y'}">
			 			${fn:substring(lecture.COURSE_START_DATE,0,4)} 년 
			 			${fn:substring(lecture.COURSE_START_DATE,4,6)} 월 
			 			${fn:substring(lecture.COURSE_START_DATE,6,8)} 일 ~
			 			${fn:substring(lecture.COURSE_END_DATE,0,4)} 년 
			 			${fn:substring(lecture.COURSE_END_DATE,4,6)} 월 
			 			${fn:substring(lecture.COURSE_END_DATE,6,8)} 일 
			 		</c:if> 
		 			${fn:substring(lecture.COURSE_START_TIME,0,2)}:${fn:substring(lecture.COURSE_START_TIME,2,4)} ~
		 			${fn:substring(lecture.COURSE_END_TIME,0,2)}:${fn:substring(lecture.COURSE_END_TIME,2,4)}
		 			<c:if test="${lecture.CERTIFICATE_HOURS != ''}">
			 			(${lecture.CERTIFICATE_HOURS} )
			 		</c:if>
		 			<br/><br/>
		 		<span class="table-label">장&nbsp;&nbsp;&nbsp;   소</span> ${lecture.FACILITY_NAME} ${lecture.PLACE_NAME}<br/><br/>
		 		<span class="table-label">내&nbsp;&nbsp;&nbsp;   용</span> 전시관 체험물 안내 및 질서유지, 행사 지원 등
		 		<!--<c:if test="${empty lecture.CONFIRM_DATA}">전시관 체험물 안내 및 질서유지, 행사 지원 등</c:if>
		 			<c:if test="${not empty lecture.CONFIRM_DATA}">${lecture.CONFIRM_DATA}</c:if> -->
		 		<br/><br/>
		 		
				<div style=" text-align: center; margin-top: 100px; margin-bottom:20px; font-size:25px;">
					<jsp:useBean id="now" class="java.util.Date" />
			 		<fmt:formatDate pattern="yyyy년 MM월 dd일" value="${now }" />
		 		</div>
		 		<div style="text-align: center; margin-bottom: 30px;">
			 		<img style="    position: absolute;left: 200px; top:610px"src="http://www.sciencecenter.go.kr/scipia/resources/img/markany/confirmation_name.png" class="img-responsive" alt="국립과천과학관" />
			 		<img style="margin-left: 270px;" src="http://www.sciencecenter.go.kr/scipia//resources/img/markany/confirmation_sign.gif" class="img-responsive" alt="국립과천과학관" />
		 		</div>
			</div>
		</div>
		<!-- 페이지 구분자 -->
		<!-- MarkAny Page Gubun -->
		<!-- 페이지 구분자 end -->
		
		
	</body>
</html>
</MarkAnyFps:MarkAnyHtmlData>
<!-- step2. include  -->
		<%
		    //String	strHtmlData = out.toString();
		    //out = ((matostring) out).getOldJspWriter();
		    
			//byte	byteReadHtmlData[] = strHtmlData.getBytes ("UTF-8"); 
			//int 	iReadHtmlDataSize = byteReadHtmlData.length;        
			String  strHtmlData = MaFpsHtmlContent.getHtmlContent();
	byte	byteReadHtmlData[] = strHtmlData.getBytes("utf-8");
	int 	iReadHtmlDataSize = byteReadHtmlData.length; 
		%> 
		<%@include file="./MaFpsTail.jsp"%>
		<script language="javascript">
		function resize_window(){
		      window.resizeTo(800,730);
		  }
		  window.onload = function() {
		   resize_window();
		  }
		</script>
		<!-- step2. end  -->