﻿<%@ page import="java.util.*,java.io.*, java.text.*,  java.lang.Integer, java.net.*" %>
<%@ page import="com.markany.EPageSafer.*"%>
<%@ page import="com.markany.fps.*"%>
<%@ page import="com.markany.futils.*"%>
<jsp:useBean id="MaFpsHtmlContent" class="com.markany.fps.MaFpsHtmlContent" scope="request" />
<%

	/**
		MaFpsCommon.jsp
		ePageSAFER의 서버, 클라이언트와 통신 및 옵션을 설정하기 위한 페이지입니다.
		각 연동의 설정, 미리보기 및 출력의 옵션을 설정 할 수 있습니다.
		추후 정책 및 환경이 변경될 경우 아래의 변수들을 변경하시면 됩니다.
		
		- 해당 페이지는 돋움체를 사용하여 정렬하였습니다. 
	*/
	
	//#############################		common value set	###############################	
	String  	strApp              			= new String("maepagesafer");
	String  	strPVersion     				= new String("25111");
	
	
	//#############################		2D Bacode value set	###############################
	String		strMAServerIP 					= new String("127.0.0.1");
	int			iMAServerPort 					= 18100;
	int			iCellBlockCount 				= 16;
	int			iCellBlockRow					= 2;
		
	//#############################		client value set	###############################	
	String  	strProtocolName     			= new String(request.getScheme()+"://");
	String 		strServerName					= request.getServerName();
	int			iServerPort						= request.getServerPort();
	String  	strDomain           			= new String(strProtocolName+ strServerName +":"+ iServerPort);
	
  	//String strUserID            = (String)session.getAttribute("user_no");  	
	
	//#############################		setting the MA_FPSFM & setting the .matmp file  ############	
	int			iUseNas							= 0;	//1: FM이 필요없는 경우 0:FM이 필요한 경우

	//iUseNas:0인 경우 아래 설정을 사용해야함. 사용안할 경우라도 변수는 임의의 값으로 설정해야 함
	//String		strFileServerIp					= InetAddress.getLocalHost().getHostAddress();    
	String		strFileServerIp					= "152.99.239.18"; 
	int 		iFileServerPort					= 18430;		
	
	//iUseNas:1인 경우 메타파일 임시폴더를 설정해야 함. 사용안할 경우라도 변수는 임의의 값으로 설정해야 함
	//*** strDownFolder: 물리적 경로사용 예)/usr/local/apache/htdocs/FPS/ibuuni/markany_noax 
	//String    strCurrentPath       			= pageContext.getServletContext().getRealPath(""); // Weblogic
	String  	strCurrentPath 					  = getServletContext().getRealPath("/WEB-INF/views/markany");
	//String  	strDownFolder       			= strCurrentPath + "/fn"; //metafile location	//log
	String  	strDownFolder       			= "/app/markany2/FPS/files/tmp";	
	String    	strPrtDatDownFolder   	= strCurrentPath + "/bin/";                 //dat 파일

	//#############################		setting Install Page  ############	
	int		    iUseInstallPage			      	= 1;	//1: 수동설치 페이지 사용 0: exe 파일을 바로 다운로드
	String    strInstallFilePath       			= new String(strCurrentPath + "/bin/Setup_ePageSafer.exe");
	String    strInstallFileName        		= "Setup_ePageSafer.exe";

	//#############################		setting the was file	###############################		
	//String  	strContextPath            		= request.getContextPath();
	String  	strContextPath            		= "/scipia";
	String  	strUrlHome						= strContextPath + new String("/markany");
	//String  	strJspHome						= new String("/scipia/markany");
	String  	strJspHome						= new String("/scipia/markany");

	String  	strDownURL  	        		= new String(strDomain + strJspHome+ "/Mafndown?fn=");  //metafile jsp //log
	String    	strPrtDatDownURL       			= new String(strDomain + strJspHome+ "/Mafndown?prtdat=MaPrintInfoEPSmain.dat");//log  
	String    	strSessionCheck    				= new String(strDomain + strJspHome+ "/MaSessionCheck");
	String    	strInstallCheck    				= new String(strDomain + strJspHome+ "/MaSessionCheck_Install");
	String    	strIePopupURL    				= new String(strDomain + strJspHome+ "/MaIePopup");
	String  	strSessionURL    				= new String(strDomain + strJspHome+ "/MaSetInstall?param=MARKANYEPS" + strPVersion);  


	//#############################		setting the web file	###############################	
	String    	strJsWebHome          			= strDomain + strContextPath + "/resources/js/markany";                    //js 파일 웹 루트경로
	String		strImagePath					= new String(strContextPath + "/resources/img/markany");  //imamge 파일 웹 루트경로
	String		strSudongInstallURL				= new String(strUrlHome + "/html/Install_Page_Windows");	//수동설치파일경로
	
	//String		strPrintURL						= new String("bak.jsp"); // printparam
	//String  	strPrintParam					= new String("?EndPrint=1"); // printurl
	String		strPrintURL						= new String("");
	String  	strPrintParam					= new String("");
	String  	strSilentOption					= new String(""); //silent
	String  	strDataFileName					= new String("");

	//#############################		Client option set	###############################	
	String		PSSTRING						= "";
	String		PSSTRING2						= "";
	String		FAQURL							= "1";
	//String	CP2PARAM						= "1^100^50";
	String		CHARSET							= "UTF-8";
	String		LANGUAGE						= "1^1^"; // 0 사용안함, 1 사용^ 0 OS언어설정, 1 한국어, 2 영어, 3 일본어
//	String		TITLE							= "";
	String		PRINTERDAT						= "";//new String("MaPrintInfoEPSetc.dat");
	String		PRINTERVER						= "20150717";
	String		PRINTERUPDATE					= MaBase64Utils.base64Encode( strPrtDatDownURL.getBytes("utf-8") );
	String		VIRTUAL							= "Vk1fQUxMT1dBTEw=";  //허용 가상 프로그램	all 허용 : Vk1fQUxMT1dBTEw=, 미허용 : ""
	String 		strFunctionGubun				= new String( "MA" );  // 수정불가 - fix 값
	//String    	PAGEMARGIN           			= new String( "0.75^0.75^0.75^0.75" ); //PAGEMARGIN L^T^R^B
	String    	PAGEMARGIN           			= new String( "" ); //PAGEMARGIN L^T^R^B
	String 		strScope				    	= new String( "2" );           // 1 : binary, 2 : BASE64, 3 : compress binary, 4 : e-mail
	String 		strWidthHeight					= new String( "1" );     // 1 : 세로문서 , 2 : 가로문서
	String 		strFolder				    	= iCellBlockCount + "^" + iCellBlockRow + "^"; // 2D CellCount:2D CellRow
	String		strErrorFilePath				= new String( "" );    // not use
	String		strPrintCount			  		= new String("1"); // 인쇄가능한 횟수       

	//#############################		Add Client option set	###############################	
	String 		SHRINKTOFIT						= new String("1"); // 크기에 맞춰 출력
	String		FIXEDSIZE						= new String("0"); // 윈도우 사이즈 고정
	String		strPrtProtocol					= new String("");  //
	String		PRTAFTEREXIT					= new String("");  // 출력 후 종료
	String		NO2DBARCODE						= new String("");  // 바코드 없이 출력
	String 		strCPParams						= new String(""); //복사방지마크 설정값 : 복사방지마크개수^CD1_X^CD1_Y^CD2_X^CD2_Y...
	String		STNODATA			  			= new String("0");        // Nodata일 경우 인쇄버튼 비활성화
	String		BUCLOSE			  				= new String("1");        // 닫기버튼 추가 0 활성화 , 1 비활성화
	String		VIEWPAGE			  			= new String("0");        // VIEWPAGE 옵션
	String		WINDOWSIZE			  			= new String("800^900");        // 윈도우 사이즈 가로^세로
	String		ZOOMINCONTENT 			  		= new String("100");        // 줌인 기능 테스트
	
	// WaterMark - Option
	String		strWmImagePath					= new String("/");
	String		strWmPosStartX					= new String("160");		/* 단위 : mm */
	String		strWmPosStartY					= new String("250");		/* 단위 : mm */
	String  	strWmPosEndX			  		= new String("0");			/* 0:마지막장 1:첫번째 장, 2:각장 */
	String  	strWmPosEndY			  		= new String("260");		/* not use */
	// 만약 strWmKey가 "0" 일경우, WaterMark를 지원하지 않음
	String  	strWmKey				    	= new String("0");  /* 마크애니마크애니 */

	// 2D Data
	String		str2dPosLajerX					= new String("100");		/* 단위 : mm */
	String		str2dPosLajerY					= new String("270");		/* 단위 : mm */
	String  	str2dPosInkX			  		= new String("0");			/* not use */
	String  	str2dPosInkY			  		= new String("0");			/* not use */	
	

	//#############################		CP, WM value	Set	###############################	
    String      WMPARAM							= new String("");
    //String      strCPParam						= new String("YXx8X05EkdN5ivPkRb9igSDN5SiRQT13ZKQL5g8Xy1InS5n/+tteu6Lg16RTLa3K"); // 모든 프린터지원
    //String      strCPParam						= new String("YXx8X05EkdN5ivPkRb9igSdN+SBpnGI8WOtVWwvKA+UwO0vAD+RUGqgNF24FdGiWMF4wXg=="); // 등록 프린터지원    	
	String      strCPParam						= new String(""); // 등록 프린터지원    
    String      strCPSubParam           		= new String ("0^8^265^528^264");	
	//String      strCPSubParam           		= new String ("1^8^265^528^528");	 // 0: 원본만(0^8^265^528^264), 1: 원본+사본레이어(1^8^265^528^528), 설정안함(""): 사본레이어만 ( 0^x^y^w^h)
		
	//#############################		TagLib Set		###############################	
	File    fileAMeta           = File.createTempFile ("~Tmp", ".mark");
    String  strOrgFileName      = fileAMeta.getAbsolutePath();

%>