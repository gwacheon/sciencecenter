﻿<%@ page language="java" contentType="text/html; charset=euc-kr" pageEncoding="euc-kr"%>
<%@include file="MaFpsCommon.jsp"%>
<div id='markanybody'></div>
<%!
	public static String replaceAll(String dest,String src,String rep) {
	    String retstr="";
	    String left="";
	    int pos=0;
	    if(dest==null) return retstr;
	    while(true) {
	        if((pos=dest.indexOf(src))!=-1) {
	            left = dest.substring(0, pos);
	            dest = dest.substring(pos+src.length(), dest.length());
	            retstr=retstr+left+rep;
	            pos=pos+src.length();
	        }
	        else {
	            retstr=retstr+dest;
	            break;
	        }
	    }
	    return retstr;
	}

	/**
	*	20151008_hcchoi<br>
	*	JSP 디버그용 메소드<br>
	*	writeLogPath로 설정한 경로에 로그 파일이 생성됩니다.<br>
	*	ex) jspTraceLog(변수명{String}, 변수값{Object})
	*/
	public void jspTraceLog(String variableName, Object Value) throws IOException{
		Date now = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
				
		//String methodName =Thread.currentThread().getStackTrace()[1].getMethodName();					//현제 메소드명
		//String writeLogPath = getServletContext().getRealPath("") + "\\UB\\" +format.format(now) + ".txt"; // NT
		String writeLogPath = getServletContext().getRealPath("") + "/UB/" +format.format(now) + ".txt"; // UNIX
				
		BufferedWriter fw = null;
		
		char variableNameArr[] = variableName.toCharArray();
		char variableNameArr2[] = new char[20];
		
		System.arraycopy(variableNameArr, 0, variableNameArr2, 0, variableNameArr.length);
		
		try{
			fw = new BufferedWriter(new FileWriter(writeLogPath, true));
			String log = variableName + "=[" + Value + "]";
			fw.write(log, 0, log.length());
			fw.newLine();
		}catch(IOException e){
			System.out.println("TraceLog FileWriter Error");
		}finally{
			if(fw != null){
				fw.close();
			}
		}
	}

	/**
	*	20151008_hcchoi<br>
	*	사용자 브라우져 정보가져오기<br>
	*	{Sring[{브라우져명, 브라우져 버전}]}
	*/
	
	public static String[] getBrowserInfo(String info){
		String[] otherBrowsers={"Firefox","Opera", "OPR",  "Chrome", "Safari"};
		String browsername = "";
		String browserversion = "";
		String browserInfoArr[] = new String[2];
		
		if((info.indexOf("MSIE") < 0) && (info.indexOf("Trident") < 0)){
			  for(int i=0; i< otherBrowsers.length;  i++){
				  if(info.indexOf(otherBrowsers[i]) >= 0)
				  {
					browsername=otherBrowsers[i];
					break;
				  }
			  }
			  String subsString = info.substring( info.indexOf(browsername));
			  String Info[] = (subsString.split(" ")[0]).split("/");
			  browsername = Info[0];
			  browserversion = Info[1];
			  
			  if(browsername.indexOf("OPR") >= 0)
					  browsername = "Opera";
		  }
		  else{
			  if(info.indexOf("MSIE") >= 0)
			  {
				String tempStr = info.substring(info.indexOf("MSIE"),info.length());
				browserversion = tempStr.substring(4,tempStr.indexOf(";"));
			  }
			  else if(info.indexOf("Trident") >= 0)
			  {
				String tempStr = info.substring(info.indexOf("Trident"),info.length());
				if(tempStr.substring(7,tempStr.indexOf(";")).indexOf("7") >= 0)
				  browserversion = "11";
				else
				  browserversion = "0";
			  }
                
			  browsername    = "IE";
		  }

		if (browserversion != null && browserversion.indexOf(".") >= 0) {
			String[]sArray1 = browserversion.split("[.]");
			if (sArray1.length > 0)
				browserversion = sArray1[0];
		}
		
		browserInfoArr[0] = browsername;
		browserInfoArr[1] = browserversion;
		
		return browserInfoArr;
	}
	
%>

<%

	//	Jar Debug
	//jspTraceLog("strUBData=" , strUBData);

	try
	{			
	
		String osversion = "";
		String strPath = "";
		String strAddData = "";
		int iRetBro = 0;
		String browsername = "";
		String browserversion = "";
		int iBrowserVer = 0;
	  String strSignature = "MARKANYEPS";
	  
		String info = request.getHeader("User-Agent");
		if (info != null) {
			String browserInfoArr[] = getBrowserInfo(info);
			browsername = browserInfoArr[0];
			browserversion = browserInfoArr[1];
			iBrowserVer = Integer.parseInt(browserversion.trim());
		}
      
      HttpSession session1 = request.getSession(false);
      if(session1 == null) {
          session1 = request.getSession(true);
          out.println("신규세션생성");
      }
      else
      {
        out.println("기존세션존재");
      }
      
      String strCookie = session.getId();
      out.println("strCookie" + strCookie);
	  			
		int iSessionCheck = 0;
		 
    String pversion = (String)session.getAttribute("productversion");    
  	//out.println("pversion" + pversion);
  	     
    if(pversion != null && pversion.indexOf(strSignature) >= 0)
    {
      String	strVersion = replaceAll( pversion, strSignature, "");
      int iCurrent = Integer.parseInt(strPVersion);
      int iSession = Integer.parseInt(strVersion);
    	  
      if(iSession >= iCurrent)
        iSessionCheck = 1;
    }
  	
  	//out.println("iSessionCheck" + iSessionCheck);
  	//out.println("strJsWebHome" + strJsWebHome);

		if(iSessionCheck == 0)
	    {
%>
			<HTML>
			<HEAD>
			<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
			<TITLE>::: 전자확인증 발급 :::</TITLE>

				<style type="text/css">
				body, td {  line-height: 11pt}
				body, td { font-size: 9pt; font-family: 돋움,Arial; color: #6D6D6D}
				A:link { text-decoration: none ; color: #0000FF }
				A:visited { text-decoration: none ; color: #989A9A }
				A:active { text-decoration: none ; color: #000000 }
				A:hover { text-decoration: none; color:#989A9A }
				font {font-size: 8pt;text-decoration: none;}
				.sub_font {font-family:"굴림체";color:000000;font-size:9pt;text-decoration:none}
				</style>
			</HEAD>
			<BODY LEFTMARGIN="0" TOPMARGIN="0" RIGHTMARGIN="0" bottommargin="0" marginwidth="0" marginheight="0"   >
			<!--iframe id ="hidpopWinC" name="hidpopWinC" width=0 height=0 frameborder=0 marginheight=0 marginwidth=0 scrolling=auto></iframe-->
			<div id="loadingBar" style="background-color:#ffffff;width:100%;">
			  <table align="center" border="0" width="400" style="margin-top:130px">
			  <tr>
				<td width="450" height="400" colspan="4" bgcolor="#FFFFFF">
					<p align="center"><span class="sub_font">문서 출력을 위한 환경을 점검하고 있습니다.<br><br></p>
					<p align="center"><span class="sub_font">* Microsoft Internet Explorer 8 이하 버전인 경우 팝업 항상 허용을 눌러주십시오.</p>
				</td>
			  </tr>
			</table>
			</div>
				<script language="javascript">
				  document.oncontextmenu = document.body.oncontextmenu = function() {return false;}
				  var vstrSessionURL         = "<%=strSessionURL%>";
				  var vstrSudongInstallURL  = "<%=strSudongInstallURL%>";
				  var vstrCookie 				    = "<%=strCookie%>";
				  var iVersion              = "<%=strPVersion%>";
				  var vstrSessionCheck      = "<%=strInstallCheck%>";
				  var vstrJsWebHome         = "<%=strJsWebHome%>";
			  </script>
			  <script src="<%=strJsWebHome%>/MaVerCheck.js" charset="euc-kr"></script> 
			  <script language="javascript">
				  var vstrApp               = "<%=strApp%>"; 
				  var vstrIePopupURL        = "<%=strIePopupURL%>";
				  LaunchRegistApp(vstrApp, "installcheck", vstrIePopupURL ); 
			  </script>
			  </BODY>
			</HTML>
<%          
        }
        else if(iSessionCheck == 1)
        {
%>
			<html xmlns="http://www.w3.org/1999/xhtml">
			<html>
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=euc-kr"/>
				<title> E - Certification </title>  
				<style>
				.container {
					position: relative;
					margin: 0 auto;
				}
				#content{
					display: block;
					font-size: 11px;
					margin-top: 40px;
					text-align: center;
				}

				img{
					margin: 0 auto;
					display: block;
				}
				</style>
				<script language="javascript">
					var vbrowsername = "<%=browsername%>";
					//alert("설치 되어 있습니다.");
					//window close
					if ( vbrowsername == 'Firefox') {
						var win = window.open('about:blank', '_self');
						win.close();
					} else if (vbrowsername == 'Chrome') {
						window.open('', '_self');
						window.close();
					} else {
						window.open('about:blank', '_self').close();
					}
				</script> 
				</head>
				<body>
				  <!--iframe id ="hidpopWinC" name="hidpopWinC" width=0 height=0 frameborder=0 marginheight=0 marginwidth=0 scrolling=auto></iframe-->
					<div class="container">
						<div id="content">
							설치 되어 있습니다.
						</div>
					</div>
				</body>
			</html>
<%
        }
	}
	catch( Exception e )
	{}
	finally 
	{}	
%>
<script>
function resize_window(){
      window.resizeTo(450, 450);
  }
  //window.onload = function() {
   resize_window();
  //}
</script>
