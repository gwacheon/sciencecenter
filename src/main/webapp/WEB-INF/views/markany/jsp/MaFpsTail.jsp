﻿<%@ page import="java.io.*"%>
<%@ page import="java.util.*,java.io.*, java.text.*,  java.lang.Integer, java.net.*" %>
<%@ page import="com.markany.futils.*"%>
<div id='markanybody'></div>
<%!
	public static String replaceAll(String dest,String src,String rep) {
	    String retstr="";
	    String left="";
	    int pos=0;
	    if(dest==null) return retstr;
	    while(true) {
	        if((pos=dest.indexOf(src))!=-1) {
	            left = dest.substring(0, pos);
	            dest = dest.substring(pos+src.length(), dest.length());
	            retstr=retstr+left+rep;
	            pos=pos+src.length();
	        }
	        else {
	            retstr=retstr+dest;
	            break;
	        }
	    }
	    return retstr;
	}
	
	public String Hash(String str, String hashtype)
    {
       java.security.MessageDigest sh = null;
       String SHA = "";               
       try{
        sh =  java.security.MessageDigest.getInstance(hashtype); 
        sh.update(str.getBytes()); 
        byte byteData[] = sh.digest();
        StringBuffer sb = new StringBuffer(); 
        for(int i = 0 ; i < byteData.length ; i++){
         sb.append(Integer.toString((byteData[i]&0xff) + 0x100, 16).substring(1));
        }
        SHA = sb.toString();
        
       }catch(Exception e){
        SHA = e.toString();
       }
       return SHA;
    
    }	
	
	/**
	*	20151008_hcchoi<br>
	*	JSP 디버그용 메소드<br>
	*	writeLogPath로 설정한 경로에 로그 파일이 생성됩니다.<br>
	*	ex) jspTraceLog(변수명{String}, 변수값{Object})
	*/
	public void jspTraceLog(String variableName, Object Value) throws IOException{
		Date now = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
				
		//String methodName =Thread.currentThread().getStackTrace()[1].getMethodName();					//현제 메소드명
		String writeLogPath = getServletContext().getRealPath("") + "markany/" +format.format(now) + ".txt";
				
		BufferedWriter fw = null;
		
		char variableNameArr[] = variableName.toCharArray();
		char variableNameArr2[] = new char[20];
		
		System.arraycopy(variableNameArr, 0, variableNameArr2, 0, variableNameArr.length);
		
		try{
			fw = new BufferedWriter(new FileWriter(writeLogPath, true));
			String log = variableName + "=[" + Value + "]";
			fw.write(log, 0, log.length());
			fw.newLine();
		}catch(IOException e){
			System.out.println("TraceLog FileWriter Error");
		}finally{
			if(fw != null){
				fw.close();
			}
		}
	}
	
	
	/**
	*	20151008_hcchoi<br>
	*	사용자 브라우져 정보가져오기<br>
	*	{Sring[{브라우져명, 브라우져 버전}]}
	*/
	
	public static String[] getBrowserInfo(String info){
		String[] otherBrowsers={"Firefox","Opera", "OPR",  "Chrome", "Safari"};
		String browsername = "";
		String browserversion = "";
		String browserInfoArr[] = new String[2];
		
		if((info.indexOf("MSIE") < 0) && (info.indexOf("Trident") < 0)){
			  for(int i=0; i< otherBrowsers.length;  i++){
				  if(info.indexOf(otherBrowsers[i]) >= 0)
				  {
					browsername=otherBrowsers[i];
					break;
				  }
			  }
			  String subsString = info.substring( info.indexOf(browsername));
			  String Info[] = (subsString.split(" ")[0]).split("/");
			  browsername = Info[0];
			  browserversion = Info[1];
			  
			  if(browsername.indexOf("OPR") >= 0)
					  browsername = "Opera";
		  }
		  else{
			  if(info.indexOf("MSIE") >= 0)
			  {
				String tempStr = info.substring(info.indexOf("MSIE"),info.length());
				browserversion = tempStr.substring(4,tempStr.indexOf(";"));
			  }
			  else if(info.indexOf("Trident") >= 0)
			  {
				String tempStr = info.substring(info.indexOf("Trident"),info.length());
				if(tempStr.substring(7,tempStr.indexOf(";")).indexOf("7") >= 0)
				  browserversion = "11";
				else
				  browserversion = "0";
			  }
                
			  browsername    = "IE";
		  }

		if (browserversion != null && browserversion.indexOf(".") >= 0) {
			String[]sArray1 = browserversion.split("[.]");
			if (sArray1.length > 0)
				browserversion = sArray1[0];
		}
		
		browserInfoArr[0] = browsername;
		browserInfoArr[1] = browserversion;
		
		return browserInfoArr;
	}
	
%>

<%

	
	
	
	//	Jar Debug
	/*
	String jarParameter = "";
	jarParameter = "clMaPreStreamBase64.strMaPrestreamWmByte(\r\n"
		+ ("\t\t\t\t"+strMAServerIP+",\r\n")
		+ ("\t\t\t\t"+iMAServerPort+",\r\n")
		+ ("\t\t\t\t"+strFunctionGubun+",\r\n")
		+ ("\t\t\t\t"+strScope+",\r\n")
		+ ("\t\t\t\t"+strWidthHeight+",\r\n")
		+ "\t\t\t\t"+strPrintCount+",\r\n"
		+ "\t\t\t\t"+strFolder+",\r\n"
		+ "\t\t\t\t,byteReadHtmlData,\r\n"
		+ "\t\t\t\t"+iReadHtmlDataSize+",\r\n"
		+ "\t\t\t\t"+strErrorFilePath+" ,\r\n"
		+ "\t\t\t\t"+strWmImagePath+" ,\r\n"
		+ "\t\t\t\t"+strWmPosStartX+" ,\r\n"
		+ "\t\t\t\t"+strWmPosStartY+",\r\n"
		+ ("\t\t\t\t"+strWmPosEndX+",\r\n")
		+ "\t\t\t\t"+strWmPosEndY+",\r\n"
		+ "\t\t\t\t"+strWmKey+" ,\r\n"
		+ "\t\t\t\t"+str2dPosLajerX+" ,\r\n"
		+ "\t\t\t\t"+str2dPosLajerY+",\r\n"
		+ ("\t\t\t\t"+str2dPosInkX+",\r\n")
		+ "\t\t\t\t"+str2dPosInkY+")";
	  
	jspTraceLog("jar Call" , jarParameter);			
	//	Jar Debug End
	*/
			
	// create instance
	    maprestreambase64 clMaPreStreamBase64 = new maprestreambase64();
			String strMaPrestreamReturn = new String(clMaPreStreamBase64.strMaPrestreamWmByte(
                            				strMAServerIP,
                            				iMAServerPort,
											strFunctionGubun,
											strScope,
											strWidthHeight,
											strPrintCount,
											strFolder,
											byteReadHtmlData,
											iReadHtmlDataSize,
											strErrorFilePath,
											strWmImagePath,
											strWmPosStartX,
											strWmPosStartY,
											strWmPosEndX,
											strWmPosEndY,
											strWmKey,
											str2dPosLajerX,
											str2dPosLajerY,
											str2dPosInkX,
											str2dPosInkY )
  		);
  		String strTmpAMetaData = new String( strMaPrestreamReturn.substring( maprestreambase64.iRetCodeEndIdx ) );
  		String strRetCode = new String( strMaPrestreamReturn.substring( maprestreambase64.iRetCodeStartIdx, maprestreambase64.iRetCodeEndIdx ) );
  		int		iRetCode = Integer.parseInt( strRetCode );
		
		if(iRetCode != 0)
  		{
  		  out.print("### Markany :: Error code : " + iRetCode);
  		  return;
  		}	
		

	//Success ...
	if( iRetCode == 0 )
	{
		
		try
		{			
			int		iAMetaDataSize = strTmpAMetaData.length();
			String	strAMetaData = replaceAll( strTmpAMetaData, "\n", "\\n");
			 
			//skkim script call - with meta value
			String osversion = "";
			String strPath = "";
			String strAddData = "";
			int iRetBro = 0;
			String browsername = "";
			String browserversion = "";
			int iBrowserVer = 0;
		
			boolean bLinuxSock = true;
			boolean bMacSock = true;
			
			String info = request.getHeader("User-Agent");
			if (info != null) {
				String browserInfoArr[] = getBrowserInfo(info);
				browsername = browserInfoArr[0];
				browserversion = browserInfoArr[1];
				iBrowserVer = Integer.parseInt(browserversion.trim());
			}
      
      HttpSession session1 = request.getSession(false);
      if(session1 == null) {
          session1 = request.getSession(true);
      }
      
      String strCookie = session.getId();
	  			
		int iSessionCheck = 0;
		String	strVersion = null;
    	String pversion = (String)session.getAttribute("productversion");     
		//out.println(pversion);
    	if(pversion != null && pversion.indexOf("MARKANYEPS") >= 0)
    	{
    	  strVersion = replaceAll( pversion, "MARKANYEPS", "");
    	  int iCurrent = Integer.parseInt(strPVersion);
    	  int iSession = Integer.parseInt(strVersion);
    	      	  
    	  if(iSession >= iCurrent)
    	    iSessionCheck = 1;
    	}
			
			if (info.indexOf("Windows") > 0) {
				strAddData = "#IMGURL=http://" + strServerName + ":" + String.valueOf(iServerPort) + "/" + strImagePath;
				strAddData += "#META_SIZE=" + String.valueOf(iAMetaDataSize);
				strAddData += "#CPPARAM=YXx8X05EkdN5ivPkRb9igSdN+SBpnGI8WOtVWwvKA+UwO0vAD+RUGqgNF24FdGiWMF4wXg==";
				strAddData += "#CPSUBPARAM=" + strCPSubParam; // 원본단독사용 0, 같이사용 1 0^x^y^w^h
				strAddData += "#PRTIP=" + strServerName;
				strAddData += "#PRTPORT=" + String.valueOf(iServerPort);
				strAddData += "#PRTPARAM=" + strPrintParam;
				strAddData += "#PRTURL=" + strPrintURL;
				strAddData += "#DOCTYPE=1";
				strAddData += "#PRTTYPE=0";
				strAddData += "#PSSTRING=" + PSSTRING;
				strAddData += "#PSSTRING2=" + PSSTRING2;
				strAddData += "#FAQURL=" + FAQURL;
				//strAddData += "#CP2PARAM=" + CP2PARAM;
				strAddData += "#WMPARAM=" + WMPARAM;
				strAddData += "#TITLE=MarkAny Client";
				strAddData += "#PRINTERDAT=" + strDataFileName;
				strAddData += "#PRINTERVER=" + PRINTERVER;
				strAddData += "#PRINTERUPDATE=" + PRINTERUPDATE;
				strAddData += "#CHARSET=" + CHARSET;
				strAddData += "#VIRTUAL=" + VIRTUAL; //가상허용
				strAddData += "#LANGUAGE=" + LANGUAGE; // 언어설정
				strAddData += "#PAGEMARGIN=0.75^0.75^0.75^0.75"; //PAGEMARGIN L^T^R^B
				strAddData += "#PRINTCNT=" + strPrintCount; // 인쇄 가능횟수 옵션
//----------------------------------------------------------------------------------------
				strAddData += "#STNODATA=" + STNODATA; // Nodata일 경우 인쇄버튼 
				strAddData += "#BUCLOSE=" + BUCLOSE; // 닫기버튼 추가
				strAddData += "#WINDOWSIZE=" + WINDOWSIZE; // 윈도우 사이즈 가로^세로
				strAddData += "#SHRINKTOFIT=" + SHRINKTOFIT;//크기에 맞게 축소하여 출력
				strAddData += "#FIXEDSIZE=" + FIXEDSIZE; // 윈도우 크기 고정
				//strAddData += "#PRTPROTOCOL=" + strPrtProtocol; //
				strAddData += "#PRTAFTEREXIT=" + PRTAFTEREXIT; // 출력 후 종료
				strAddData += "#NO2DBARCODE=" + NO2DBARCODE;  // 2D바코드 없이 출력
				//strAddData += "#COPYPARAMS=" + strCPParams; // 여러 복사방지마크 옵션
				strAddData += "#VIEWPAGE=" + VIEWPAGE; // ViewPage옵션
				strAddData += "#ZOOMINCONTENT =" + ZOOMINCONTENT ; // ViewPage옵션
				
//----------------------------------------------------------------------------------------
				
				strAddData = replaceAll(strAddData, "\\n", "\n");
				if (info.indexOf("MSIE") >= 0 || info.indexOf("Trident") >= 0) {
					iRetBro = 3;
				} else {
					iRetBro = 4;
				}
			}
			
			if(iRetBro == 3  || iRetBro == 4) ///url request
			{
				//MD5, MD4, SHA-1, SHA-256, SHA-512 등 사용 가능.
				//String strUserID = "markany";
				//session.setAttribute("userid", strUserID);
				//String strHashID = Hash(strUserID, "MD5");
				
				java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyyMMddHHmmssSSS");
				String strEncFileServerIP = MaBase64Utils.base64Encode(strFileServerIp.getBytes("euc-kr"));

				String today = formatter.format(new java.util.Date()); 				
				strPath = strCookie + today + ".matmp";
				String filePath = strDownFolder + "/";//request.getRealPath(strDownFolder) + "/"; //파일을 생성할 전체경로
				filePath += strPath; //생성할 파일명을 전체경로에 결합

				strDownURL += today; //down url 
				
				//out.println("strPath =" + strPath);
				//out.println("strDownURL" + strDownURL);
				//out.println("strDownFolder" + strDownFolder);

				if(iUseNas == 1)
				{
					BufferedWriter fw = null;
					try{
						fw=new BufferedWriter(new FileWriter(new File(filePath)));
						fw.write(strTmpAMetaData + strAddData); //파일에다 작성
						fw.flush();
					}catch (IOException e) { 
						String[] sArray1 = e.toString().split(" "); 
						out.println(e.toString());
						//out.println(sArray1[0]); //에러 발생시 메시지 출력
						return;
					}finally {
						if(fw != null)
						  fw.close(); //파일핸들 닫기
					}
				}else {
				masavefile clMaSaveFile = new masavefile();
				
				// FileSerevrIP 정보를 파라미터로 보냄
				strDownURL += "&fs=" +strEncFileServerIP;
				
				// Call Ma2DCode library
				try
				{
					strRetCode = clMaSaveFile.strSaveMetaFile (
		        			                strFileServerIp,
		        			                iFileServerPort,
		        			                strPath,
		        			                strTmpAMetaData + strAddData,
		        			                "",
		                                    "" );
		
		    		iRetCode = Integer.parseInt( strRetCode );
					
				}
				catch( UnsatisfiedLinkError e )
				{
					System.err.println("\t:"+e.toString());
					System.exit(1);
				}				

				} //if(iUseNas) 
				
				session.setAttribute("strDownURL", strDownURL); 
				session.setAttribute("strCookie", strCookie); 
				
				
			} else if (iRetBro == 2 && bLinuxSock == false) {
				strDownURL = "plugin";
			} else if (iRetBro == 1 && bMacSock == false) {
				strDownURL = "plugin";
			} else { 
				strDownURL = "";
			}
		if( iRetCode == 0 ) //file 생성 성공
		{
			//multi os 는 추후 추가 
			if(iRetBro ==  1 || iRetBro == 2){
				%>
				<html>
					<head>
					<meta http-equiv="Content-Type" content="text/html; charset=euc-kr"/>
					<title>E-Certification</title>
					<style>
					.container {
						position: relative;
						margin: 0 auto;
					}
					#content{
						display: block;
						font-size: 11px;
						margin-top: 40px;
						text-align: center;
					}
		
					img{
						margin: 0 auto;
						display: block;
					}
					</style>
					</head>
					<body>
						<div class="container">
							<div id="content">
								<img src="<%=strImagePath%>/loading.gif"><br>
								<img src="<%=strImagePath%>/Ma_progressBar.gif"><br>
								<img src="<%=strImagePath%>/notice.png"><br>
							</div>
						</div>
					</body>
				</html>
				<script>
				</script>
	<%
			}else if(iRetBro ==  3 || iRetBro == 4){
				%>
				<script>
				  var vstrCookie = "<%=strCookie%>";
				  var vstrSessionCheck = "<%=strSessionCheck%>";
					var resize_height = 450;	//default
				</script>
				<%
	  if(iSessionCheck == 0)
	    {
	      if(browsername.indexOf("Chrome") >= 0 || browsername.indexOf("Opera") >= 0 ) 
			{
%>
				<HTML>
				<HEAD>
				<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
				<TITLE>::: 전자확인증 발급 :::</TITLE>
					<style type="text/css">
						body, td	{ line-height: 10pt }
						body, td	{ font-size: 9pt; font-family: 돋움,Arial; color: #6D6D6D }
						.container	{ width: 100%; text-align: center; }
						.section	{ width: 500px; text-align: center; display: block; }
						.content	{ display: block; width: 500px; margin: 0 auto; }
						#browserChkImg	{ margin-top: 100px; margin-bottom: 20px;}
						#browserChkComment	{ margin-bottom: 150px; text-align: left; width: 430px; height: 80px; margin: 0 auto;}
					</style>
				</HEAD>
				<BODY LEFTMARGIN="0" TOPMARGIN="0" RIGHTMARGIN="0" bottommargin="0" marginwidth="0" marginheight="0">
				<iframe id ="hidpopWinC" name="hidpopWinC" width=0 height=0 frameborder=0 marginheight=0 marginwidth=0 scrolling=auto></iframe>
				<div class="container">
					<div class="content">
						<div class="section">
							<img id="browserChkImg">
						</div>
						<div class="section">
							<p id="browserChkComment">
							</p>
						</div>
						
						<div class="section">
							<!-- 문서 발급을 위한 환경을 점검하고 있습니다.<br> -->
							<!-- 아래 [문서발급] 버튼을 눌러 문서 발급을 계속 진행됩니다.<br><br> -->
							<input type="button" onclick="location.href='<%=strJspHome%>/MaSessionCheck'" value="Print">
							<br><br><hr>
							<p>* 미설치, PC에 설치된 버전이 낮을 경우 문서발급을 누르시면 설치페이지로 이동합니다.</p>
							<p>* Microsoft Internet Explorer 8 이하 버전인 경우 팝업 항상 허용을 눌러주십시오.</p>
							<hr>
						</div>
					</div>
				</div>
					<script language="javascript">
						document.oncontextmenu = document.body.oncontextmenu = function() {return false;}
						var vstrSessionURL 	= "<%=strSessionURL%>";
						var vstrDownURL 	= "<%=strDownURL%>";
						var vstrSudongInstallURL         = "<%=strSudongInstallURL%>";
						var iVersion  = "<%=strPVersion%>";
				  </script>
				  <script src="<%=strJsWebHome%>/MaVerCheck.js" charset="euc-kr"></script> 
				  <script language="javascript">
						var browserName = get_browser();
						var browserChkImg = document.getElementById("browserChkImg");
						var browserChkComment = document.getElementById("browserChkComment");
						
						//alert(browserName);
						switch (browserName){
						case "Opera":
							resize_height = 600;
							browserChkImg.src = "<%=strImagePath%>/check_guide_opera.png";
							browserChkComment.innerHTML = 	'* 외부 프로토콜 요청 팝업이 보이실 경우 다음과 같이 진행해주십시오<br>'
															+ '<br> 1. [mareportsafer. 링크 항상 열기] 체크 합니다.'
															+ '<br> 2. [허용] 버튼을 누릅니다.'
															+ '<br> 3. [Print] 버튼을 눌러 발급을 계속 진행합니다.';
							break;
						case "Chrome":
							resize_height = 850;
							browserChkImg.src = "<%=strImagePath%>/check_guide_chrome.png";
							browserChkComment.innerHTML = 	'* 외부 프로토콜 요청 팝업이 보이실 경우 다음과 같이 진행해주십시오<br>'
															+ '<br> 1. [위와같은 유형의 모든 링크에 대해 내 선택을 기억합니다.] 체크 합니다.'
															+ '<br> 2. [어플리케이션 시작] 버튼을 누릅니다.'
															+ '<br> 3. [Print] 버튼을 눌러 발급을 계속 진행합니다.';
							break;
						case "MSIE":
							resize_height = 500;
							browserChkImg.src = "<%=strImagePath%>/Ma_progressBar.gif";
							browserChkComment.innerHTML = 	'<br>1. [문서발급] 버튼을 눌러 발급을 계속 진행합니다.';
															//'* 외부 프로토콜 요청 팝업이 보이실 경우 다음과 같이 진행해주십시오<br>'
															//+ '<br> 1. [위와같은 유형의 모든 링크에 대해 내 선택을 기억합니다.] 체크 합니다.'
															//+ '<br> 2. [어플리케이션 시작] 버튼을 누릅니다.'
															//+ '<br> 3. [문서발급] 버튼을 눌러 발급을 계속 진행합니다.';
							break;
						case "Firefox":
							resize_height = 750;
							browserChkImg.src = "<%=strImagePath%>/check_guide_firefox.png";
							browserChkComment.innerHTML = 	'* 프로그램 실행 팝업이 보이실 경우 다음과 같이 진행해주십시오<br>'
															+ '<br> 1. [mareportsafer 링크에 대한 선택 사항을 기억 합니다.] 체크 합니다.'
															+ '<br> 2. [확인] 버튼을 누릅니다.'
															+ '<br> 3. [문서발급] 버튼을 눌러 발급을 계속 진행합니다.';
							break;
						}
		
						var vstrApp = "<%=strApp%>"; 
						var vstrIePopupURL = "<%=strIePopupURL%>";
						LaunchRegistApp(vstrApp, "registapp", vstrIePopupURL); 
				  </script>
				  </BODY>
				</HTML>
		<%
			}
			else //ie
			{
		%>
				<HTML>
				<HEAD>
				<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
				<!--<meta http-equiv="refresh" content="3;url=<%=strJspHome%>/MaSessionCheck.jsp" />-->
				<TITLE>::: 전자확인증 발급 :::</TITLE>
					<style type="text/css">
						body, td	{ line-height: 10pt }
						body, td	{ font-size: 9pt; font-family: 돋움,Arial; color: #6D6D6D }
						.container	{ width: 100%; text-align: center; }
						.section	{ width: 500px; text-align: center; display: block; }
						.content	{ display: block; width: 500px; margin: 0 auto; }
						#browserChkImg	{ margin-top: 100px; margin-bottom: 20px;}
						#browserChkComment	{ margin-bottom: 150px; text-align: left; width: 430px; height: 80px; margin: 0 auto;}
					</style>
				</HEAD>
				<BODY LEFTMARGIN="0" TOPMARGIN="0" RIGHTMARGIN="0" bottommargin="0" marginwidth="0" marginheight="0">
				<iframe id ="hidpopWinC" name="hidpopWinC" width=0 height=0 frameborder=0 marginheight=0 marginwidth=0 scrolling=auto></iframe>
				<div class="container">
					<div class="content">
						<div class="section">
							<img src="<%=strImagePath%>/loading.gif" id="browserChkImg"><br>
							<img src="<%=strImagePath%>/Ma_progressBar.gif"><br>
							<img src="<%=strImagePath%>/notice.png"><br>
						</div>
						<div class="section">
							<p id="browserChkComment">
							</p>
						</div>
						
						<div class="section">
							<!-- 문서 발급을 위한 환경을 점검하고 있습니다.<br> -->
							<!-- 아래 [문서발급] 버튼을 눌러 문서 발급을 계속 진행됩니다.<br><br> -->
							<!--input type="button" onclick="location.href='<%=strJspHome%>/MaSessionCheck.jsp'" value="문서발급"-->
							<br><br><hr>
							<p>* 문서 발급을 위한 환경을 점검하고 있습니다.</p>
							<p>* Microsoft Internet Explorer 8 이하 버전인 경우 팝업 항상 허용을 눌러주십시오.</p>
							<hr>
						</div>
					</div>
				</div>
					<script language="javascript">
					  document.oncontextmenu = document.body.oncontextmenu = function() {return false;}
					  var vstrSessionURL = "<%=strSessionURL%>";
					  var vstrDownURL = "<%=strDownURL%>"; 
						var vstrSudongInstallURL         = "<%=strSudongInstallURL%>";
						var iVersion  = "<%=strPVersion%>";
						//alert(vstrSessionURL);
						//alert(iVersion);
				  </script>
				  <script src="<%=strJsWebHome%>/MaVerCheck.js" charset="euc-kr"></script> 
				  <script language="javascript">
					var vstrApp = "<%=strApp%>"; 
					var vstrIePopupURL = "<%=strIePopupURL%>";
					LaunchRegistApp(vstrApp, "registapp", vstrIePopupURL); 
				  </script>
				  </BODY>
				</HTML>
<%          
            }
        }
        else if(iSessionCheck == 1)
        {
%>
			<html xmlns="http://www.w3.org/1999/xhtml">
			<html>
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=euc-kr"/>
				<title> E - Certification </title>  
				<style>
				.container {
					position: relative;
					margin: 0 auto;
				}
				#content{
					display: block;
					font-size: 11px;
					margin-top: 40px;
					text-align: center;
				}

				img{
					margin: 0 auto;
					display: block;
				}
				</style>
				</head>
				<body>
				  <iframe id ="hidpopWinC" name="hidpopWinC" width=0 height=0 frameborder=0 marginheight=0 marginwidth=0 scrolling=auto></iframe>
					<div class="container">
						<div id="content">
							<img src="<%=strImagePath%>/loading.gif"><br>
							<img src="<%=strImagePath%>/Ma_progressBar.gif"><br>
							<img src="<%=strImagePath%>/notice.png"><br>
							로딩이 완료될 경우 해당 페이지가 자동으로 종료됩니다.
						</div>
					</div>

						<script src="<%=strJsWebHome%>/MaVerCheck.js" charset="euc-kr"></script> 
						<script language="javascript">
						var vpversion = "<%=pversion%>";
						var vstrSudongInstallURL         = "<%=strSudongInstallURL%>";
						var iVersion  = "<%=strPVersion%>";
						var vstrDownURL = "<%=strDownURL%>";
						var vstrApp = "<%=strApp%>";  
						LaunchApp(vstrApp, "sockmeta"); 
					  </script>           
				</body>
			</html>
        
<%
        }
}
%>
			<script>
				document.oncontextmenu = document.body.oncontextmenu = function () {
					return false;
				}
				var vstrProtocolName = "<%=strProtocolName%>"
				var vstrinfo = "<%=info%>";
				var vstrosversion = "<%=osversion%>";
				var vstrDownURL = "<%=strDownURL%>";
				var vBrowser = "<%=browsername%>";
				var vBrowserVer = "<%=browserversion%>";
				var vstrDownFolder = "<%=strDownFolder%>";
				var vstrSudongInstallURL = "<%=strSudongInstallURL%>";
				var iVersion  = "<%=strPVersion%>";
				var vstrSilentOption = "<%=strSilentOption%>";
				var vstrApp = "<%=strApp%>";
			</script>
			<script src="<%=strJsWebHome%>/MaVerCheck.js" charset="euc-kr"></script> 
			<%
			/*
			jspTraceLog("strCurrentPath" , strCurrentPath);	
			jspTraceLog("strPrtDatDownURL" , strPrtDatDownURL);	
			String deubgArr[] = strAddData.split("#");
			for(int deubgArr_i=0;deubgArr_i<deubgArr.length;deubgArr_i++){
				String strAddDataVal[] = deubgArr[deubgArr_i].split("=");
				try{
					jspTraceLog(strAddDataVal[0] , strAddDataVal[1]);
				}catch(Exception e){
					jspTraceLog(strAddDataVal[0] , "null");
				}
			}
			jspTraceLog("## strDownFolder" , strDownFolder);
			*/
		}//if( iRetCode == 0 )
		else //file 생성 실패
		{
		%>
		 Error !!! <%= iRetCode %>
		<%
		}			
		} 
		catch( Exception e )
		{}
		//out.println( "[SUCESS]" );			
	}
	else
	{
	%>
		Error !!! <%= iRetCode %>
	<%
	}
	//jspTraceLog("## JSP END" , "###################################################################");
	%>


<script>
function resize_window(){
      window.resizeTo(550, resize_height);
  }
  //window.onload = function() {
   resize_window();
  //}
</script>
