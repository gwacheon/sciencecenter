﻿<%@page language="java" contentType="text/html; charset=euc-kr"%> 
<%@ page import='java.util.* , java.io.*'%>
<%@ page import="java.text.DecimalFormat"%>
<%@ page import="java.io.FileOutputStream"%>
<%@ page import="java.io.IOException"%>
<%@include file="MaFpsCommon.jsp"%>
<%!
  	public static String replaceAll(String dest,String src,String rep) {
	    String retstr="";
	    String left="";
	    int pos=0;
	    if(dest==null) return retstr;
	    while(true) {
	        if((pos=dest.indexOf(src))!=-1) {
	            left = dest.substring(0, pos);
	            dest = dest.substring(pos+src.length(), dest.length());
	            retstr=retstr+left+rep;
	            pos=pos+src.length();
	        }
	        else {
	            retstr=retstr+dest;
	            break;
	        }
	    }
	    return retstr;
	}
	
		/**
	*	20151008_hcchoi<br>
	*	사용자 브라우져 정보가져오기<br>
	*	{Sring[{브라우져명, 브라우져 버전}]}
	*/
	
	public static String[] getBrowserInfo(String info){
		String[] otherBrowsers={"Firefox","Opera", "OPR",  "Chrome", "Safari"};
		String browsername = "";
		String browserversion = "";
		String browserInfoArr[] = new String[2];
		
		if((info.indexOf("MSIE") < 0) && (info.indexOf("Trident") < 0)){
			  for(int i=0; i< otherBrowsers.length;  i++){
				  if(info.indexOf(otherBrowsers[i]) >= 0)
				  {
					browsername=otherBrowsers[i];
					break;
				  }
			  }
			  String subsString = info.substring( info.indexOf(browsername));
			  String Info[] = (subsString.split(" ")[0]).split("/");
			  browsername = Info[0];
			  browserversion = Info[1];
			  
			  if(browsername.indexOf("OPR") >= 0)
					  browsername = "Opera";
		  }
		  else{
			  if(info.indexOf("MSIE") >= 0)
			  {
				String tempStr = info.substring(info.indexOf("MSIE"),info.length());
				browserversion = tempStr.substring(4,tempStr.indexOf(";"));
			  }
			  else if(info.indexOf("Trident") >= 0)
			  {
				String tempStr = info.substring(info.indexOf("Trident"),info.length());
				if(tempStr.substring(7,tempStr.indexOf(";")).indexOf("7") >= 0)
				  browserversion = "11";
				else
				  browserversion = "0";
			  }
                
			  browsername    = "IE";
		  }

		if (browserversion != null && browserversion.indexOf(".") >= 0) {
			String[]sArray1 = browserversion.split("[.]");
			if (sArray1.length > 0)
				browserversion = sArray1[0];
		}
		
		browserInfoArr[0] = browsername;
		browserInfoArr[1] = browserversion;
		
		return browserInfoArr;
	}
	
%>
<%

	String browsername = "";
	String browserversion = "";
	int iBrowserVer = 0;
	int iNotIatall = 0;
	
	String info = request.getHeader("User-Agent");
	if (info != null) {
		String browserInfoArr[] = getBrowserInfo(info);
		browsername = browserInfoArr[0];
		browserversion = browserInfoArr[1];
		iBrowserVer = Integer.parseInt(browserversion.trim());
	}

	int iTotal = 20;
	int iCnt =0;
	boolean bValidVersion = false;

	while (true) {
		strParamPversion = (String)session.getAttribute("productversion");

		if (strParamPversion != null) {
			if (strParamPversion.indexOf(strSignature) >= 0) {
				String strTemp = replaceAll(strParamPversion, strSignature, "");
				int iServerVer = Integer.parseInt(strPVersion);
				int iSessionVer = Integer.parseInt(strTemp);

				if (iSessionVer > iServerVer)
					bValidVersion = true;
			}
		}

		if (bValidVersion)
			break;

		iCnt++;

		if (iTotal <= iCnt)
			break;

		Thread.sleep(500);
	}

	if (!bValidVersion)
	{
%>
<script language="javascript">
	alert("설치가 되어 있지 않습니다.");
	//parent.자바스크립트(false);
</script> 
<%
    } else {
%>
<script language="javascript">
	alert("설치 되어 있습니다.");
	//parent.자바스크립트(true);
</script> 
<%		
	}
%>