﻿<%@page language="java" contentType="text/html; charset=euc-kr"%> 
<%@ page import='java.util.* , java.io.*, java.lang.*'%>
<%@include file="MaFpsCommon.jsp"%>
<%!
  public static String replaceAll(String dest,String src,String rep) {
	    String retstr="";
	    String left="";
	    int pos=0;
	    if(dest==null) return retstr;
	    while(true) {
	        if((pos=dest.indexOf(src))!=-1) {
	            left = dest.substring(0, pos);
	            dest = dest.substring(pos+src.length(), dest.length());
	            retstr=retstr+left+rep;
	            pos=pos+src.length();
	        }
	        else {
	            retstr=retstr+dest;
	            break;
	        }
	    }
	    return retstr;
	}
	
%>
<%
  String strParamDownURL = (String)session.getAttribute("strDownURL");
  String strParamCookie = (String)session.getAttribute("strCookie");
  String strParamPversion = (String)session.getAttribute("productversion");
  String strSignature = "MARKANYEPS";
  
  //out.println("strParamDownURL" + strParamDownURL);
  //out.println("strParamCookie" + strParamCookie);
  //out.println("strParamPversion" + strParamPversion);

  int iTotal = 20;
  int iCnt =0;
  boolean bValidVersion = false;
  
  while (true) {
  	strParamPversion = (String)session.getAttribute("productversion");

  	if (strParamPversion != null) {
  		if (strParamPversion.indexOf(strSignature) >= 0) {
  			String strTemp = replaceAll(strParamPversion, strSignature, "");
  			int iServerVer = Integer.parseInt(strPVersion);
  			int iSessionVer = Integer.parseInt(strTemp);

  			if (iSessionVer >= iServerVer)
  				bValidVersion = true;
  		}
  	}

  	if (bValidVersion)
  		break;

  	iCnt++;

  	if (iTotal <= iCnt)
  		break;

  	Thread.sleep(500);
  }

  if (!bValidVersion) //file delete
  {
  	String[]result = strParamDownURL.split("fn=");
  	if (result.length > 1) {
  		String filePath = strDownFolder;
  		String requestFileNameAndPath = strParamCookie + result[1];
  		requestFileNameAndPath += ".matmp";

  		File file = new File(filePath, requestFileNameAndPath);
  		if (file.exists()) {
  			file.delete ();
  		}
  	}
	response.sendRedirect(strSudongInstallURL);
  }
  
  
%>

<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=euc-kr"/>
	<title>E-Certification</title>
	<style>
	.container {
		position: relative;
		margin: 0 auto;
	}
	#content{
		display: block;
		font-size: 11px;
		margin-top: 40px;
		text-align: center;
	}

	img{
		margin: 0 auto;
		display: block;
	}
	</style>
	</head>
	<body>
		<div class="container">
			<div id="content">
				<img src="<%=strImagePath%>/loading.gif"><br>
				<img src="<%=strImagePath%>/Ma_progressBar.gif"><br>
				<img src="<%=strImagePath%>/notice.png"><br>
			</div>
		</div>
	</body>
</html> 
<script src="<%=strJsWebHome%>/MaVerCheck.js" charset="euc-kr"></script> 
<script language="javascript">
  var vstrSudongInstallURL = "<%=strSudongInstallURL%>";
  var vSession = getCookie("JSESSIONID");
  var vstrCookie = "<%=strParamCookie%>";
  var vpversion = "<%=strParamPversion%>";
  var vstrDownURL = "<%=strParamDownURL%>";
  var iVersion  = "<%=strPVersion%>";
  var vstrApp = "<%=strApp%>"; 
  LaunchApp(vstrApp, "sockmeta");  
</script> 
