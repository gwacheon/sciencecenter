<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-purple">
	   <nav id="scrollspy-nav"
            class="navbar-default navbar-static has-scroll">
            <div class="container">
                <ul class="nav navbar-nav">
                	<c:import url="/WEB-INF/views/schedules/navbar.jsp"/>
                    <li class="active">
                    	<a href="#board-spy" class="item">
                    		행사/공연
                    	</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <div class="bread-crumbs">예약/신청 / 행사/공연</div>
                    </li>
                </ul>
            </div>
        </nav>
	</div>
	<div id="events-body" class="sub-body">
		<div class="narrow-sub-top event" data-type="event">
			<div class="container">
				<c:choose>
					<c:when test="${empty param.status }">
						<h4 class="doc-title">현재 진행중인 행사/공연</h4>		
					</c:when>
					<c:otherwise>
						<h4 class="doc-title">마감된 행사/공연</h4>
					</c:otherwise>
				</c:choose>
				<h6 class="doc-title exp-desc">
					국립과천과학관에서는  과학문화발전에 기여하고자 다양한 행사들을 진행하고 있으니 많은 참여 바랍니다.
				</h6>
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<ul class="nav nav-tabs">
							<li class="col-xs-6 <c:if test="${ empty param.status || param.status eq 'true' }">active</c:if>">
								<a href="<c:url value='/eventsTotal' />">
									현재 진행중인 행사/공연
								</a>
							</li>
							<li class="col-xs-6 <c:if test="${ param.status eq 'false' }">active</c:if>">
								<a href="<c:url value='/eventsTotal?status=false' />">
									마감된 행사/공연
								</a>
							</li>															
						</ul>						
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title purple">
						<div class="top-line"></div>
						행사/공연 안내
					</h3>
				</div>		
			</div>
			<div class="row">
				<c:forEach var="event" items="${events }">
				   	<div class="col-md-6 col-lg-4 event-list-wrapper">
				   		<div class="event margin-bottom20">
						   	<div class="image">
						   		<c:choose>
						   			<c:when test="${not empty event.pictureUrl  }">
						   				<img src="<c:url value='${baseFileUrl }'/>${event.pictureUrl}" class="img-responsive" alt="${event.title }"/>
						   			</c:when>
						   			<c:otherwise>
					   					<img src="<c:url value='/resources/img/noimage.jpeg'/>" class="img-responsive" alt="${event.title }"/>
						   			</c:otherwise>
						   		</c:choose>
							   	   		
							   	<div class="button-wrapper">
							   		<div class="button">
								   		<a href="<c:url value='/eventsTotal/${event.id }?status=${event.status }'/>">
								   			<i class="fa fa-search-plus"></i> 상세보기
							   			</a>						   		
							   		</div>
							   		<div class="button">
								   		<a target="_blank" href="http://${event.url }">
								   			<i class="fa fa-home"></i> 해당 페이지 바로가기
								   		</a>				  		   		
							   		</div>
							   	</div>
						   	</div>
						   	<div class="desc">
							   	<div class="title">
								   	<c:out value="${event.title }" />
							   	</div>
							   	<div class="detail">
							   		<div class="header">
							   			<i class="fa fa-calendar-check-o"></i> 기간
							   		</div>
							   		<div class="content">
							   			<fmt:formatDate pattern="yyyy-MM-dd" value="${event.startDate}" />
							   			~
									   	<fmt:formatDate pattern="yyyy-MM-dd" value="${event.endDate}" />
									  s 	
							   		</div>
							   	</div>
							   	<div class="detail">
							   		<div class="header">
							   			<i class="fa fa-map-marker"></i> 장소
							   		</div>
							   		<div class="content">
									   <c:out value="${event.place }" />
							   		</div>
							   	</div>
							   	<div class="detail">
							   		<div class="header">
							   			<i class="fa fa-credit-card"></i> 참가비
							   		</div>
						   			<div class="content">
									   	<c:out value="${event.pay }" />
							   		</div>
							   	</div>
						   	</div>
				   		</div>
				   	</div>
				</c:forEach>
			</div>
			<div class="row">
				<div class="col-md-12 center-align">
					<div id="events-pagination" class="table-purple">
						<c:import url="pagination.jsp"></c:import>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>