<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<style type="text/css">

    object{
      max-width: 100%;
    }

    .container{
      position: relative;
      width: 100%;
      margin: auto;
    }

    .responsive-img{
      max-width: 100%;
    }

    #wrapper .item{
    	position: absolute;
    }

    #exhibition-1-1{position: absolute; left: 17.2%;top: 49.5%;width: 10.9%;}
    #exhibition-1-2{position: absolute; left:31%;top:47.2%;width:6.8%;}
    #exhibition-1-3{position: absolute; left:36.7%;top:33.6%;width:6.2%;}
    #exhibition-1-4{position: absolute; left:47.5%;top:18.5%;width:6.4%;}
</style>

<div class="container">
	
	<img src="<c:url value='/img/svgs/01/01-bg.png'/>" class="responsive-img">

	<object id="exhibition-1-1" class="item" type="image/svg+xml" data="<c:url value='/img/svgs/01/01-01.svg'/>"></object>
</div>

<div class="container margin-bottom30">
	<div class="row margin-top30">
		<div class="col s3">
			<div>장비 대여</div>
			<div>장소 대관</div>
		</div>
		
		<div class="col s9">
			<div class="row">
				<div class="input-field col s12">
					<select id="selected-equipment">
						<c:forEach items="${equipments }" var="equipment">
							<option value="${equipment.id }">${equipment.name } (${equipment.modelName })</option>
						</c:forEach>
				    </select>
				    <label>장비 선택</label>
				</div>
			</div>
			
			<div class="row">
				<div class="col s12">
					<div id="equipment-calendars">
						
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col s12">
					<div id="equipment-serials">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<c:import url="../sangsang/equipReserve.jsp"></c:import>

<script id="equipment-calendar-template" type="text/x-handlebars-template">

<div class="month-selector" style="text-align: center">
	<a id="prev-month" class="month-selector" href="#" data-date="{{prevMonth}}">
		&lt;
	</a>

	<div id="selected-month" style="display: inline-block">
		{{formatedDate selectedDate "YYYY.MM"}}
	</div>

	<a id="next-month" href="#" class="month-selector" data-date="{{nextMonth}}">
		&gt;
	</a>
</div>

<div id="equip-calendar" class="calendar-area">
	<table id="calendar-table" class="ui celled table unstackable">
		<thead>
			<tr>
				<th class="center">일</th>
				<th class="center">월</th>
				<th class="center">화</th>
				<th class="center">수</th>
				<th class="center">목</th>
				<th class="center">금</th>
				<th class="center">토</th>
			</tr>
		</thead>
		<tbody class="calendar-body">
			{{#each dates}}
				<tr>
					{{#each this}}
						<td class="center date" data-date="{{formatedDate this "YYYY-MM-DD"}}">
							{{formatedDate this "DD"}}
							<div class="circle"></div>
						</td>
					{{/each}}
				</tr>
			{{/each}}
		</tbody>
	</table>
</div>
</script>



<script type="text/javascript">
	
	$(document).ready(function() {
		$('select').material_select();
	});
	
	Handlebars.registerHelper('formatedDate', function(date, format) {
	  return moment(date).format(format);
	});
	
	var selectedDate = moment(new Date())._d;
	var selectedEquipment;
	
	function prevMonth(date){
		return moment(date).add(-1, 'Month')._d;
	}
	
	function nextMonth(date){
		return moment(date).add(1, 'Month')._d;
	}
	
	function dates(date){
		ret = [];

		if(!_.isDate(date)){
			date = moment(new Date());
		}

		var beginOfMonth = moment(date).startOf('month').startOf('week');
		var endOfMonth = moment(date).endOf('month').endOf('week');

		for(var temp = beginOfMonth; temp.isBefore(endOfMonth); temp.add(1, 'days')){
			ret.push(temp.clone());
		}

		ret = _.chunk(ret, 7);

		return ret;
	}
	
	function renderCalendar(){
		var data = {
				selectedDate: selectedDate,
				prevMonth: prevMonth(selectedDate),
				nextMonth: nextMonth(selectedDate),
				dates: dates(selectedDate)
			};
		var source   = $("#equipment-calendar-template").html();
		var template = Handlebars.compile(source);
		var html    = template(data);
		$("#equipment-calendars").html(html);
		
		$.getJSON(baseUrl + "sangsang/getReservationsInMonth", {
			equipmentId: $("#selected-equipment").val(),
			beginDate: moment(selectedDate).startOf('month').startOf('week').format("YYYY-MM-DD"),
			endDate: moment(selectedDate).endOf('month').endOf('week').format("YYYY-MM-DD")
		}, function(data){
			
			if(data != null && data.equipReserves != null){
				var dates = [];
				_.forEach(data.equipReserves, function(n, key){
					var d = moment(n.beginTime).format("YYYY-MM-DD");
					
					if(dates.indexOf(d) < 0){
						dates.push(d);
					}
				});
				
				_.forEach(dates, function(n, key){
					$(".calendar-body .date[data-date='" + n + "']").addClass("has-reserve");
				});
				
			}
		});
		
		addEvents();
	}
	
	function addEvents(){
		$(".month-selector").on('click', function(e){
			e.preventDefault();
			selectedDate = moment($(this).attr('data-date'))._d;
			renderCalendar();
		});
		
		$("td.date").on('click', function(e){
			equipReserve.getReservations($(this).data('date'));
			$(".calendar-body .date").removeClass("active");
			$(this).addClass("active");
		});
	}
	
	$(function(){
		selectedEquipment = $("#selected-equipment").val();
		renderCalendar();
		
		$("#selected-equipment").on("change", function(){
			renderCalendar();
			$("#equipment-serials").html('');
		})
	});
</script>