<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-teal">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>
	<div id="display-body" class="sub-body">
		<div id="basic-info-spy" class="narrow-sub-top display">
			<div class="sub-banner-tab-wrapper sub-banner-tab-teal">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<ul class="nav nav-tabs">
								<li class="col-xs-12 col-sm-6 col-md-3">
									<a href="<c:url value="/display/frontier/kidsMakerStudio" />" class="item">
										키즈메이커스튜디오
									</a>
								</li>
								<li class="col-xs-12 col-sm-6 col-md-3">
									<a href="<c:url value="/display/frontier/freeMarket" />" class="item">
										메이커프리마켓 
									</a>
								</li>
								<li class="col-xs-12 col-sm-6 col-md-3">
									<a href="<c:url value="/display/frontier/familySLand" />" class="item">
										패밀리창작놀이터
									</a>
								</li>
								<li class="col-xs-12 col-sm-6 col-md-3 active">
									<a href="#" class="item">
										무한상상실
									</a>
								</li>
							</ul>   
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="support-sub-nav">  
			<div class="container">
			   	<div class="sub-nav">
					<div class="row">	
						<div class="col-xs-6 col-md-2-5">
				     		<a href="<c:url value="/display/frontier/infiniteImagination" />" class="item">
								<i class="fa fa-book" aria-hidden="true"></i> 무한상상실 안내
							</a> 
				     		<div class="visible-xs" style="margin-bottom: 15px"></div> 
						</div>
						<div class="col-xs-6 col-md-2-5">
				     		<a href="<c:url value="/sangsang/list/device" />" class="item active">
								<i class="fa fa-print" aria-hidden="true"></i> 장비 예약
							</a> 
				     		<div class="visible-xs" style="margin-bottom: 15px"></div> 
						</div>
						<div class="col-xs-6 col-md-2-5">
						    <a href="<c:url value="/display/frontier/incubator" />" class="item">
								<i class="fa fa-cube" aria-hidden="true"></i> 메이커인큐베이터 
							</a>
						    <div class="visible-xs" style="margin-bottom: 15px"></div> 
						</div>
						<div class="col-xs-6 col-md-2-5">
						    <a href="javascript:void(0)" class="item" onclick="AcademyChoice('ACD008')">
								<i class="fa fa-calendar-check-o" aria-hidden="true"></i> 워크숍 예약
							</a>
							<div class="visible-xs" style="margin-bottom: 15px"></div> 
						</div>
						<div class="col-xs-6 col-md-2-5">
						    <a href="http://cyber.scientorium.go.kr/museum_2015/vrLoad.jsp?pavilion=indoor&xml_name=img_001.xml&angle=-90"
								class="item">
								<i class="fa fa-desktop" aria-hidden="true"></i> 사이버 전시관
							</a>
						</div>				     
					</div>  
				</div>
			</div>
		</div> 
        <div class="container">
            <div id="basic-info-spy" class="section scrollspy">
                <div class="row">
                    <div class="col-sm-3 col-md-2 col-lg-2">
                        <h3 class="page-title teal">
                        	<div class="top-line"></div>
                            <c:choose>
                            	<c:when test="${sangsangType eq 'device'}">
                            		장비 예약
                            	</c:when>
                            	<c:otherwise>
                            		무한상상실 대관
                            	</c:otherwise>
                            </c:choose>
                        </h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p>
                            국립과천과학관 무한상상실은 대형 CNC 라우터, 레이저 커터, 3D프린터, 비닐 커터, 
                            탁상용 CNC조각기 등 다양한 디지털 제작 장비를 보유하고 있습니다.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
	                    <c:forEach items="${equipments }" var="equipment">
							<div class="sangsang-equip-item">
								<div class="row">
								    <div class="col-md-3">
								        <div class="sangsnag-img">
								         <img src="<c:url value="${baseFileUrl }${equipment.picture }"/>" class="img-responsive"/>
								      </div>
								    </div>
								    <div class="col-md-9">
								        <div class="sangsang-content">
											<a href="#" class="title">${equipment.name }
											(${equipment.modelName })</a>
											<%-- <a href="<c:url value="/sangsang/${equipment.id}"/>" class="title">${equipment.name }
											(${equipment.modelName })</a> --%>
											<c:out value="${equipment.description }" escapeXml="false" />
										</div>
										<%-- <div class="sangsang-btn">
											<c:choose>
						                    	<c:when test="${sangsangType eq 'device'}">
						                    		<a href="<c:url value="/sangsang/${equipment.id}"/>" >장비 예약하기</a>
						                    	</c:when>
						                    	<c:otherwise>
						                    		<a href="<c:url value="/sangsang/${equipment.id}"/>" >무한상상실 대관 예약하기</a>
						                    	</c:otherwise>
						                    </c:choose>
										</div> --%>
									</div>
								</div> 
							</div>
		                </c:forEach>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>
<form name="frmIndex">
	<input type="hidden" name="ACADEMY_CD" value="ACD008" />
	<input type="hidden" name="COURSE_CD" value="" />
	<input type="hidden" name="SEMESTER_CD" value="" />
	<input type="hidden" name="TYPE" value="" />
	<input type="hidden" name="FLAG" value="" />
	<input type="hidden" name="CLASS_CD" value="CL8001" />
</form>
<script type="text/javascript">
	function AcademyChoice(Cd){
		var frm = document.frmIndex;
		frm.CLASS_CD.value = Cd;
		frm.action = '<c:url value="/schedules"/>?${_csrf.parameterName}=${_csrf.token}';
		frm.method = 'post';
		frm.submit();
	}
</script>