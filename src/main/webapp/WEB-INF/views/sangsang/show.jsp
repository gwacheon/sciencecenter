<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div id="sub-content-wrapper">
    <div class="sub-content-nav scrollspy-teal">
    	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
    </div>    
</div>
<div id="display-body" class="sub-body">
	<div id="basic-info-spy" class="narrow-sub-top display">
		<div class="sub-banner-tab-wrapper sub-banner-tab-teal">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul class="nav nav-tabs">
							<li class="col-xs-12 col-sm-6 col-md-3">
								<a href="<c:url value="/display/frontier/kidsMakerStudio" />" class="item">
									키즈메이커스튜디오
								</a>
							</li>
							<li class="col-xs-12 col-sm-6 col-md-3">
								<a href="<c:url value="/display/frontier/freeMarket" />" class="item">
									메이커프리마켓 
								</a>
							</li>
							<li class="col-xs-12 col-sm-6 col-md-3">
								<a href="<c:url value="/display/frontier/familySLand" />" class="item">
									패밀리창작놀이터
								</a>
							</li>
							<li class="col-xs-12 col-sm-6 col-md-3 active">
								<a href="#" class="item">
									무한상상실
								</a>
							</li>
						</ul>   
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="support-sub-nav">  
		<div class="container">
		   	<div class="sub-nav">
				<div class="row">	
					<div class="col-xs-6 col-md-2-5">
			     		<a href="<c:url value="/display/frontier/infiniteImagination" />" class="item">
							<i class="fa fa-book" aria-hidden="true"></i> 무한상상실 안내
						</a> 
			     		<div class="visible-xs" style="margin-bottom: 15px"></div> 
					</div>
					<div class="col-xs-6 col-md-2-5">
			     		<a href="<c:url value="/sangsang/list/device" />" class="item active">
							<i class="fa fa-print" aria-hidden="true"></i> 장비 예약
						</a> 
			     		<div class="visible-xs" style="margin-bottom: 15px"></div> 
					</div>
					<div class="col-xs-6 col-md-2-5">
					    <a href="<c:url value="/display/frontier/incubator" />" class="item">
							<i class="fa fa-cube" aria-hidden="true"></i> 메이커인큐베이터 
						</a>
					    <div class="visible-xs" style="margin-bottom: 15px"></div> 
					</div>
					<div class="col-xs-6 col-md-2-5">
					    <a href="javascript:void(0)" class="item" onclick="AcademyChoice('ACD008')">
							<i class="fa fa-calendar-check-o" aria-hidden="true"></i> 워크숍 예약
						</a>
						<div class="visible-xs" style="margin-bottom: 15px"></div> 
					</div>
					<div class="col-xs-6 col-md-2-5">
					    <a href="http://cyber.scientorium.go.kr/museum_2015/vrLoad.jsp?pavilion=indoor&xml_name=img_001.xml&angle=-90"
							class="item">
							<i class="fa fa-desktop" aria-hidden="true"></i> 사이버 전시관
						</a>
					</div>				     
				</div>  
			</div>
		</div>
	</div> 
	<div class="container">
	    <div id="basic-info-spy" class="section scrollspy">
	        <div class="row">
	            <div class="col-md-4">
	                <h3 class="page-title teal">
						<div class="top-line"></div>
	                    장비 예약
	                </h3>
	            </div>
	        </div>
	    </div>
	</div>
	<div class="container">
	   <div class="row">
	       <div class="col-md-12 step-title">
	           <span>STEP 01</span> 장비 선택
	       </div>
	       <div class="col-md-12">
	           <div class="row">
	               <div class="col-md-3">
	                   <div class="sangsnag-img">
	                       <img src="<c:url value="${baseFileUrl }${equipment.picture }"/>" class="img-responsive"/>
	                    </div>
	               </div>
	               <div class="col-md-9">
	                   <div class="sangsang-content">
	                        <a href="<c:url value="/sangsang/${equipment.id}"/>" class="title">${equipment.name }
	                        (${equipment.modelName })</a>
	                        <c:out value="${equipment.description }" escapeXml="false" />
	                    </div>
	               </div>
               </div>
           </div>
	   </div>
	</div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12 step-title">
            <span>STEP 02</span> 날짜 및 시간 선택
        </div>
    </div>
    <c:forEach var='serial' items='${equipment.equipSerials}'>
        <div id="equipment-calendar-${serial.id }" class="equip-calendar">
            
        </div>
    </c:forEach>
	
</div>

<c:import url="../sangsang/equipReserve.jsp"></c:import>
<script type="text/javascript" src="<c:url value='/resources/js/sangSang.js'/>"></script>
<script type="text/javascript">
	
	var selectedDate = moment(new Date())._d;
	var selectedEquipment;
	var memberNo = '<c:out value="${memberNo}"/>';
	$(function(){
	    selectedEquipment = <c:out value="${equipment.id}"/>;
		<c:forEach var='serial' items='${equipment.equipSerials}'>
			var calendar = new SangSangCalendar();
	        calendar.render({
	            selectedDate: selectedDate,
	            prevMonth: prevMonth(selectedDate),
	            nextMonth: nextMonth(selectedDate),
	            dates: dates(selectedDate),
	            equipmentId: selectedEquipment,
	            equiSerialId: '${serial.id}',
	            memberNo : memberNo
	        });   
		</c:forEach>		
	});
</script>