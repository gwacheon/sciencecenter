<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>



<script id="sangsang-calendar-template"
	type="text/x-handlebars-template">
<div class="row calendar-item" data-id = "{{equiSerialId}}">
    <div class="col-md-12">
        장비 이름 : {{serial.serial}}
    </div>

    <div class="col-md-5">
        <div class="calendar-wrapper">
            <div class="month-selector" style="text-align: center" data-id="{{equiSerialId}}">
                <a id="prev-month" class="month-selector" href="#" data-date="{{prevMonth}}">
                    &lt;
                </a>

                <div id="selected-month" style="display: inline-block">
                    {{formatedDate selectedDate "YYYY.MM"}}
                </div>

                <a id="next-month" href="#" class="month-selector" data-date="{{nextMonth}}">
                    &gt;
                </a>
            </div>

            <div id="equip-calendar" class="calendar-area">
                <table id="calendar-table" class="ui celled table unstackable">
                    <thead>
                        <tr>
                            <th class="center">S</th>
                            <th class="center">M</th>
                            <th class="center">T</th>
                            <th class="center">W</th>
                            <th class="center">T</th>
                            <th class="center">F</th>
                            <th class="center">S</th>
                        </tr>
                    </thead>
                    <tbody class="calendar-body">
                        {{#each dates}}
                            <tr>
                                {{#each this}}
                                    <td class="center date" data-date="{{formatedDate this "YYYY-MM-DD"}}" data-id="{{../../equiSerialId}}">
                                        {{formatedDate this "DD"}}
                                        <div class="circle"></div>
                                    </td>
                                {{/each}}
                            </tr>
                        {{/each}}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-7"> 
        <div class="equipment-serials" data-id='{{equiSerialId}}'></div>
    </div>
	<div class="col-md-12">
		{{#compare memberNo '' operator="!="}}
			<button class="btn btn-primary btn-lg equip-reservation-modal right" data-serial-id="{{equiSerialId}}" >
  				예약	
			</button>
		{{else}}
			<button class="btn btn-primary btn-lg no-login-modal-btn right" data-serial-id="{{equiSerialId}}" >
  				예약
			</button>
		{{/compare}}
	</div>
</div>

<!-- Modal Structure -->
<div data-id='{{equiSerialId}}' class="modal fade reservation-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">예약 확인</h4>
			</div>
			<div class="modal-body">
				<p>선택하신 장비와 시간에 대한 예약을 진행하시겠습니까?</p>
				<ul id="reservation-infos">
					
				</ul>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">취소</button>
				<button type="button" class="btn btn-primary reservation-submit-btn" data-id='{{equiSerialId}}'>예약</button>

			</div>
		</div>
	</div>
</div>
</script>

<script id="sangsang-times-template" type="text/x-handlebars-template">
    <div class="row">
        <div class="col-md-12">
            <table class="table time-table">
                <thead>
                    <tr>
                        <th>
                             날짜
                        </th>
                        <th>
                             시간
                        </th>
                        <th>
                             상태
                        </th>
                        <th>
                             예약 선택
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {{#each times}}
                        <tr>
                            <td>
                                {{../date}}
                            </td>
                            <td>
                                {{time}}
                            </td>
                            <td class="reserve-text-label" data-date="{{../date}}" data-time="{{time}}" data-seq="{{seq}}" 
                                    data-serial="{{../serial.id}}">
                                예약 가능                                
                            </td>
                            <td>
                                <input class="check-time" type="checkbox" id="{{../serial.id}}_{{../date}}_{{time}}"
                                    data-date="{{../date}}" data-time="{{time}}" data-seq="{{seq}}" 
                                    data-serial="{{../serial.id}}">
                                <label for="{{../serial.id}}_{{../date}}_{{time}}" class="reserve-time-label"
                                    data-date="{{../date}}" data-time="{{time}}" data-seq="{{seq}}" 
                                    data-serial="{{../serial.id}}">
                                예약가능
                    </label>
                            </td>
                        </tr>
                    {{/each}}                    
                </tbody>
            </table>
        </div>
    </div>
</script>