<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<div id="sub-content-wrapper">
	<div class="sub-content-nav <c:choose><c:when test="${eventType.type eq 'camp' }">scrollspy-blue</c:when><c:otherwise>scrollspy-purple</c:otherwise></c:choose>">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>
	<div id="events-body" class="sub-body">
		<div class="narrow-sub-top <c:choose><c:when test="${eventType.type eq 'camp' }">guide</c:when><c:otherwise>events</c:otherwise></c:choose>" 
			data-type="${eventType.type }">
	        <div class="sub-banner-tab-wrapper <c:choose><c:when test="${eventType.type eq 'camp' }">sub-banner-tab-blue</c:when><c:otherwise>sub-banner-tab-purple</c:otherwise></c:choose>">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<ul class="nav nav-tabs">
								<li class="col-xs-6 <c:if test="${ empty param.status || param.status eq 'true' }">active</c:if>">
									<a href="<c:url value='/events/list/${eventType.type }' />" class="item">
										현재 진행중인 <spring:message code="event.${eventType.type}" />
									</a>
								</li>
								<li class="col-xs-6 <c:if test="${ param.status eq 'false' }">active</c:if>">
									<a href="<c:url value='/events/list/${eventType.type }' />?status=false" class="item">
										마감된 <spring:message code="event.${eventType.type}" />
									</a>
								</li>	
							</ul>   
						</div>
					</div>
				</div>
			</div>
        </div>
		<div class="container">
			<div class="row">
                <div class="col-md-12">
                    <h3 class="page-title <c:choose><c:when test="${eventType.type eq 'camp' }">blue</c:when><c:otherwise>purple</c:otherwise></c:choose>">
						<div class="top-line"></div>
                        <spring:message code="event.${eventType.type}" /> 상세보기
                    </h3>
                </div>      
            </div>
			<div class="row">
				<div class="col-md-12">
					<div class="event-show-wrapper margin-bottom20 <c:if test="${eventType.type eq 'camp' }">camp</c:if>">
						<div class="row">
							<div class="col-md-4 image">
								<c:choose>
						   			<c:when test="${not empty event.pictureUrl  }">
						   				<img src="<c:url value='${baseFileUrl }'/>${event.pictureUrl}" class="img-responsive" alt="${event.title }"/>
						   			</c:when>
						   			<c:otherwise>
					   					<img src="<c:url value='/resources/img/noimage.jpeg'/>" class="img-responsive" alt="${event.title }"/>
						   			</c:otherwise>
						   		</c:choose>
							</div>
							<div class="col-md-8 desc">
								<div class="row">
									<div class="col-md-12">
										<div class="title">
											<c:out value="${event.title }" />
										</div>
										<div class="detail">
											<div class="header">
												<i class="fa fa-calendar-check-o"></i> 기간
											</div>
											<div class="value">
												<fmt:formatDate pattern="yyyy-MM-dd" value="${event.startDate}" />
									   			~
											   	<fmt:formatDate pattern="yyyy-MM-dd" value="${event.endDate}" />
											</div>
										</div>
										<div class="detail">
											<div class="header">
												<i class="fa fa-map-marker"></i> 장소
											</div>
											<div class="value">
												<c:out value="${event.place }" />
											</div>
										</div>
										<div class="detail">
											<div class="header">
												<i class="fa fa-credit-card"></i> 참가비
											</div>
											<div class="value">
												<c:out value="${event.pay }" />
											</div>
										</div>
										<div class="detail">
											<div class="header">
												<i class="fa fa-file-text"></i> 첨부파일
											</div>
												<c:forEach var="file" items="${event.attatchments }">
													<div class="value">
														<a href="<c:url value='${baseFileUrl }'/>${file.url}" >
															<c:out value="${file.name }" />
														</a>
													</div>
												</c:forEach>
										</div>
									</div>
								</div>
								<c:if test="${not empty event.url }">
									<div class="row">
										<div class="col-md-12 right-align">
											<div class="link-btn">
												<a target="_blank" href="http://${event.url }">
													<i class="fa fa-home" aria-hidden="true"></i> ${eventType.link}
												</a>
											</div>
										</div>
									</div>
								</c:if>
							</div>	
						</div>
						<div class="row">
							<div class="col-md-12">
								 <div class="content">
			 					 	<c:out value="${event.content }" escapeXml="false"/>
								 </div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="<c:choose><c:when test="${eventType.type eq 'camp' }">board-show-blue</c:when><c:otherwise>board-show-purple</c:otherwise></c:choose>">
				<div class="row margin-bottom20">
					<div class="col-md-12 text-right">
						<div class="board-list-btn <c:if test="${eventType.type eq 'camp' }">camp</c:if>">
							<a href="<c:url value='/events/list/${eventType.type }'/>">
								글 목록
							</a>					
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function() {
		var selector = $('#events-top');
		
		var type = selector.data('type');
		
		switch (type) {
			case "event":
				selector.addClass('event');
				break;
			case "play":
				selector.addClass('play');
				break;
			case "camp":
				selector.addClass('camp');
				break;
			default:
				break;
		}
	});
</script>

