<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-purple">
	   <nav id="scrollspy-nav" class="navbar-default navbar-static has-scroll">
			<div class="container">
				<ul class="nav navbar-nav">
					<c:import url="/WEB-INF/views/events/navbar.jsp"/>
					<li>
						<a href="#board-spy" class="item">
							특별기획전
						</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
				    <li>
						<div class="bread-crumbs">
							문화행사 / 특별기획전
						</div>
				    </li>
				</ul>
			</div>
		</nav>
	</div>
	<div id="intro-body" class="sub-body">
		<div id="board-spy" class="narrow-sub-top events">
			
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title purple">
						<div class="top-line"></div>
						특별기획전
					</h3>
				</div>
			</div>
		 	<div class="row">
		 		<div class="col-md-12">
		 			<div id="board-wrapper" class="table-purple">
			 			<c:url var="currentPath" value="/events/specialEvent"/>
			 			
			 			<c:import url="/WEB-INF/views/boards/list.jsp">
			 				<c:param name="path" value="${currentPath }"/>
			 			</c:import>
		 			</div>
		 		</div>
			</div>
		</div>
	</div>
</div>


