<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="introduce-library">
	<div class="sub-content-nav scrollspy-purple">
       <nav id="scrollspy-nav" class="navbar-default navbar-static has-scroll">
            <div class="container">
                <ul class="nav navbar-nav">
                	<c:import url="/WEB-INF/views/intro/navbar.jsp"/>
                    <li>
                        <a href="#board-spy" class="item">
                            특별기획전
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <div class="bread-crumbs">
                            문화행사 / 특별기획전
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
	<div id="support-body" class="sub-body">
		<div  class="container">
			<div id="board-spy" class="row">
				<div class="col-md-12">
					<h3 class="page-title purple">
						<div class="top-line"></div>
						 특별기획전
					</h3>
				</div>
			</div>
			<div id="board-show" class="board-show-purple">
			
			</div>
		</div> 
	</div>
</div> 

<c:import url="/WEB-INF/views/boards/show.jsp"></c:import>

<script type="text/javascript">
var board = new Board(
        "#board-show",
        "<c:url value='/events/specialEvent'/>",
        {
            id: <c:out value="${id}"/>,
        }
    );
board.renderBoardShow();
</script>