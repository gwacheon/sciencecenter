<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<li class="dropdown">
	<a class="dropdown-toggle" type="button" data-toggle="dropdown" role="button" 
		aria-haspopup="true" aria-expanded="true">
		<spring:message code="${currentCategory }" text=''/><i class="fa fa-angle-down"></i>
	</a>
	<ul class="dropdown-menu">
		<c:set var="categoryNo" value="4"/>
		<c:forEach var="i" begin="1" end="12">
			<spring:message var="sub_category" code="main.nav.catetory${categoryNo}.sub${i}" scope="application" text='' />
			
			<c:if test="${not empty sub_category }">
				<spring:message var="sub_category_url" code="main.nav.catetory${categoryNo}.sub${i}.url" scope="application" text='' />
				<li>
					<a href="<c:url value="${sub_category_url }" />" >
						<c:out value="${sub_category }" />
					</a>
					<spring:message var="sub_child_category" code="main.nav.catetory${categoryNo}.sub${i }.sub1" scope="application" text='' />
					
					<c:if test="${sub_child_category != '' }">
						<ul class="right-menus">
						
							<c:if test="${ i == 2 }">
								<c:forEach items="${navMainEvents }" var="mainEvent">
									<c:if test="${mainEvent.mainEventType eq 'major' }">
										<li>
											<a href="<c:url value='/mainEvents/${mainEvent.id }' />">
			 									<c:out value ="${mainEvent.name }"/>
			 								</a>
										</li>
									</c:if>
								</c:forEach>
							</c:if>
							<c:if test="${ i == 3 }">
								<c:forEach items="${navMainEvents }" var="mainEvent">
									<c:if test="${mainEvent.mainEventType eq 'minor' }">
										<li>
											<a href="<c:url value='/mainEvents/${mainEvent.id }' />">
			 									<c:out value ="${mainEvent.name }"/>
			 								</a>
										</li>
									</c:if>
								</c:forEach>
							</c:if>
						
							<c:forEach var="j" begin="1" end="8">
								<spring:message var="sub_child_category" code="main.nav.catetory${categoryNo}.sub${i}.sub${j}" scope="application" text='' />
								<c:if test="${sub_child_category != '' && sub_child_category ne 'dynamicEvents' }">
								
									<spring:message var="sub_child_category_url" code="main.nav.catetory${categoryNo}.sub${i}.sub${j}.url" scope="application" text='' />
									<li>
										<a href="<c:url value="${sub_child_category_url }" />">
											${sub_child_category } 
										</a>
									</li>
								</c:if>
							</c:forEach>
						</ul>
					</c:if>
				</li>
			</c:if>
		</c:forEach>
	</ul>
</li>