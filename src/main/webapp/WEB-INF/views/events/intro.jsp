<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<div class="sub-content-nav scrollspy-purple">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>

<div id="sub-content-wrapper">
	<div id="events-body" class="sub-body">
		<div id="basic-info-spy" class="narrow-sub-top events">
		<div class="sub-banner-tab-wrapper sub-banner-tab-purple">
			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						
					</div>
				</div>
			</div>
		</div>
	</div>
		<div class="container">
			<div class="section scrollspy">
				<div class="row margin-top20">
					<div class="col-md-12">
						<h3 class="page-title purple">
							<div class="top-line"></div>
							행사안내
						</h3>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<ul class="circle-list blue">
							<li>
								과학문화행사 연간 행사
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table id="education-yearly-time-table" class="table table-bordered centered-table">
								<thead>
									<tr>
										<th colspan="2" width="20%">
											일정
										</th>
										<th>
											행사명
										</th>
										<th width="30%">
											주요내용
										</th>
										<th>
											비고
										</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th>
											1~2월
										</th>
										<td width="20%">
											'15년 12월 30일 ~
											1월 31일 
										</td>
										<td>
											뮤지컬 샤갈의 마을에 눈이 내리면
										</td>
										<td>
											색채화가 샤갈을 소재로 만든 창작뮤지컬
										</td>
										<td>
											(대관) 수엔터테인먼트
										</td>
									</tr>
									
									<tr>
										<th rowspan="2">
											3월
										</th>
										<td>
											9일
										</td>
										<td>
											부분일식 특별 관측회
										</td>
										<td>
											달이 해의 일부를 가리는 부분일식관측
										</td>
										<td>
										</td>
									</tr>
									<tr>
										<td>
											26일
										</td>
										<td>
											우주를 듣는다
										</td>
										<td>
											‘중력파 검출’ 특강
										</td>
										<td>
										</td>
									</tr>
									<tr>
										<th rowspan="2">
											4월
										</th>
										<td>
											19일~24일
										</td>
										<td>
											★ 2016 해피사이언스데이
										</td>
										<td>
											<ul>
												<li>
													과학체험마당
												</li>
												<li>
													사이언스쇼 경진대회		
												</li>
												<li>
													레고브릭 페스티벌
												</li>
												<li>
													이벤트 행사
												</li>
											</ul>
										</td>
										<td>
										</td>
									</tr>
									<tr>
										
										<td>
											19일~23일
										</td>
										<td>
											목성 특별 관측회
										</td>
										<td>
											목성의 줄무늬와 4대위성 관측
										</td>
										<td>
										</td>
									</tr>
									<tr>
										<th rowspan="2">
											5월
										</th>
										<td>
											21일
										</td>
										<td>
											사이언스 톡톡
										</td>
										<td>
											돔영상+공연+토크+과학정보
										</td>
										<td>
										</td>
									</tr>
									<tr>
										<td>
											28일
										</td>
										<td>
											우주를 듣는다
										</td>
										<td>
											‘우주 레서피’ 저자특강
										</td>
										<td>
										</td>
									</tr>
									<tr>
										<th rowspan="2">
											6월
										</th>
										<td>
											
										</td>
										<td>
											꼬마박사 장영실
										</td>
										<td>
											14가지 과학실험을 소재로 한 어린이 과학뮤지컬
										</td>
										<td>
											(공동) 사이언스팀
										</td>
									</tr>
									<tr>
										<td>
											25일
										</td>
										<td>
											우주를 듣는다
										</td>
										<td>
											‘외계지적생명체탐사(SETI) 프로젝트’ 특강
										</td>
										<td>
									
									</tr>
									
									<tr>
										<th rowspan="2">
											7월
										</th>
										<td>
											30일
										</td>
										<td>
											사이언스 톡톡
										</td>
										<td>
											돔영상+공연+토크+과학정보
										</td>
										<td>
										</td>
									</tr>
									<tr>
										<td>
											7월12일~8월21일
										</td>
										<td>
											어린이 캣
										</td>
										<td>
											
										</td>
										<td>
											(대관) 극단 뮤다드
										</td>
									</tr>
									<tr>
										<th rowspan="4">
											8월
										</th>
										<td>
											6일
										</td>
										<td>
											사이언스 톡톡
										</td>
										<td>
											돔영상+공연+토크+과학정보
										</td>
										<td>
										</td>
									</tr>
									<tr>
										<td>
											13일
										</td>
										<td>
											5행성 대규모 특별 관측회
										</td>
										<td>
											수성, 금성, 화성, 목성, 토성 관측
										</td>
										<td>
										</td>
									</tr>
									<tr>
										<td>
											27일
										</td>
										<td>
											★ 2016 골드버그대회
										</td>
										<td>
											<ul>
												<li>
													골드버그장치제작경연대회
												</li>
											</ul>
										</td>
										<td>
										</td>
									</tr>
									<tr>
										<td>
											27일
										</td>
										<td>
											우주를 듣는다
										</td>
										<td>
											‘우주의 끝을 찾아서’ 저자특강
										</td>
										<td>
										</td>
									</tr>
									<tr>
										<th rowspan="5">
											9월
										</th>
										<td>
											10일~11일
										</td>
										<td>
											★ 제6회 전국청소년과학송경연대회
										</td>
										<td>
											과학원리나 현상을 소재로 노래를 만들어 표현하는 대회
										</td>
										<td>
										</td>
									</tr>
									<tr>
										<td>
											24일
										</td>
										<td>
											우주를 듣는다
										</td>
										<td>
											‘우주의 형광등, 행성상 성운’ 특강
										</td>
										<td>
										</td>
									</tr>
									<tr>
										<td>
											9월~12월
										</td>
										<td>
											2016 과학체험한마당
										</td>
										<td>
											<ul>
												<li>
													과학체험부스
												</li>
												<li>
													크로마키 체험관	
												</li>
											</ul>
										</td>
										<td>
										</td>
									</tr>
									<tr>
										<td>
											9월 24일, 10월 1일
										</td>
										<td>
											★ 2016 과학노벨상을 말하다
										</td>
										<td>
											<ul>
												<li>
													노벨상에세이경연대회
												</li>
											</ul>
										</td>
										<td>
										</td>
									</tr>
									<tr>
										<td>
											9월 23일~10월 21일
										</td>
										<td>
											갤럭시키즈
										</td>
										<td>
											kbs 애니메이션 갤럭시키즈를 원작으로한 우주캐릭터 뮤지컬
										</td>
										<td>
											(공동) 문화아이콘
										</td>
									</tr>
									<tr>
										<th rowspan="4">
											10월
										</th>
										<td>
											1일
										</td>
										<td>
											사이언스 톡톡
										</td>
										<td>
											돔영상+공연+토크+과학정보
										</td>
										<td>
										</td>
									</tr>
									<tr>
										<td>
											8일~9일
										</td>
										<td>
											★ 2016 무한상상 메이커랜드
										</td>
										<td>
											직접만든 프로젝트를 공유, 체험하는 메이커들의 DIY축제
										</td>
										<td>
											(공동) 한국통신학회, LG연암문화제단, 한국업사이클디자인협회, 에코 11 
										</td>
									</tr>
									<tr>
										<td>
											8일
										</td>
										<td>
											국제수리과학창의대회
										</td>
										<td>
											<ul>
												<li>
													수리과학경진대회
												</li>
												<li>
													전시체험 및 이벤트
												</li>
											</ul>
										</td>
										<td>
											(공동) 미래융합창조문화재단
										</td>
									</tr>
									<tr>
										<td>
											22일~23일
										</td>
										<td>
											★ 제6회 수학문화축전
										</td>
										<td>
											<ul>
												<li>
													수학체험마당
												</li>
												<li>
													수학고민클리닉
												</li>
												<li>
													수학토크콘서트
												</li>
												<li>
													생활 속 수학발견대회
												</li>
												<li>
													원목카프라경연대회
												</li>
												<li>
													음악 속 수학이야기
												</li>
												<li>
													프랙탈 전시
												</li>
											</ul>
										</td>
										<td>
											(공동) 고등과학원 국가수리과학연구소
										</td>
									</tr>
									<tr>
										<th rowspan="4">
											11월
										</th>
										<td>
											10월 28일 ~ 11월 6일
										</td>
										<td>
											★ 미래상상SF축제
										</td>
										<td>
											<ul>
												<li>
													청소년 SF창작 그리기대회
												</li>
												<li>
													미래상상 SF포럼
												</li>
												<li>
													SF시네마토크
												</li>
												<li>
													제4회 돔영화제
												</li>
												<li>
													 SF이벤트쇼
												</li>
												<li>
													SF크로마키 체험관
												</li>
												<li>
													SF과학체험마당
												</li>
											</ul>
										</td>
										<td>
											(공동)
										</td>
									</tr>
									<tr>
										<td>
											12일, 19일
										</td>
										<td>
											2016 과학노벨상을 말하다
										</td>
										<td>
											<ul>
												<li>
													노벨상궁금증 대중강연회
												</li>
											</ul>
										</td>
										<td>
										</td>
									</tr>
									<tr>
										<td>
											26일
										</td>
										<td>
											우주를 듣는다
										</td>
										<td>
											은하중심의 거대 블랙홀’ 특강
										</td>
										<td>
										</td>
									</tr>
									<tr>
										<td>
											11월 29~12월25일
										</td>
										<td>
											호두까기인형
										</td>
										<td>
											호두까기인형과 함께하는 따뜻한 가족뮤지컬
										</td>
										<td>
											(대관) 극단 뮤다드
										</td>
									</tr>
									<tr>
										<th rowspan="3">
											12월
										</th>
										<td>
											10일
										</td>
										<td>
											사이언스 톡톡
										</td>
										<td>
											돔영상+공연+토크+과학정보
										</td>
										<td>
										</td>
									</tr>
									<tr>
										<td>
											17일
										</td>
										<td>
											★ 2016 과학노벨상을 말하다
										</td>
										<td>
											<ul>
												<li>
													노벨상시상식 토크한마당
												</li>
											</ul>
										</td>
										<td>
										</td>
									</tr>
									<tr>
										<td>
											31일
										</td>
										<td>
											우주를 듣는다
										</td>
										<td>
											‘우주이야기’ 특강
										</td>
										<td>
										</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="7">
											* 행사 세부일정은 기관의 사정에 의해 변동될 수 있습니다.
										</td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>