<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	
<div class="dropdown hall-select hallselect-purple">
	<a class="dropdown-toggle" type="button" data-toggle="dropdown"
		role="button" aria-haspopup="true" aria-expanded="true"> 
		<c:out value="${ mainEvent.name}" /> 지난행사로 이동 <i class="fa fa-angle-down" aria-hidden="true"></i>
	</a>
	<ul class='dropdown-menu'>
		<c:forEach var="series" items="${mainEvent.series }">
			<li>
				<a href="<c:url value='/mainEvents/${mainEvent.id }/series/${series.id}' />">
					<c:out value="${series.name}" />
				</a>
			</li>
		</c:forEach>
	</ul>
</div>