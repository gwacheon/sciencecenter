<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>

<div id="communication-library">
	<div class="sub-content-nav scrollspy-purple">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>
	<div id="support-body" class="sub-body">
		<div  class="container">
			<div id="board-spy" class="row">
				<div class="col-md-12">
					<h3 class="page-title purple">
						<div class="top-line"></div>
						 ${mainEventSeries.name} Q&amp;A 작성
					</h3>
				</div>
			</div>
			
			<div id="auth-form">
				<div class="row">
					<div class="col-md-12">
					<c:url var="action" value='/mainEvents/${mainEvent.id}/series/${mainEventSeries.id }/${boardType.name }/private/create' />
					<c:url var="method" value='POST' />
			
					<jsp:include page="/WEB-INF/views/boards/form.jsp">
						<jsp:param name="action" value="${action}" />
						<jsp:param name="method" value="${method}" />
					</jsp:include>
					</div>
				</div>
			</div>
			
		</div> 
	</div>
</div> 


