<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-purple">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
		<%-- <nav id="scrollspy-nav"
			class="navbar-default navbar-static has-scroll">
			<div class="container">
				<ul class="nav navbar-nav">
					<c:import url="/WEB-INF/views/events/navbar.jsp"/>
					<li class="active">
						<a href="#board-spy" class="item">
							${mainEvent.name}
						</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<div class="bread-crumbs">문화행사 / ${mainEvent.name}</div>
					</li>
				</ul>
			</div>
		</nav> --%>
	</div>
	<div id="events-body" class="sub-body">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title purple">
						<div class="top-line"></div>
						${mainEventSeries.name}
					</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-right">
					<c:import url="/WEB-INF/views/events/mainEvents/eventSeriesNav.jsp"/>
				</div>
			</div>
			<div class="row">
				<div id="main-event-wrapper" class="col-md-12">
					<div class="row">
						<div class="col-md-12 ">
							<div class="main-events">
								<!-- Nav tabs -->
								<ul class="nav nav-tabs">
									<li class="event-item col-md-2 active">
										<a href="#home" aria-controls="home"> 
											행사개요 
										</a>
									</li>
									<c:forEach var="boardName" items="${mainEventSeries.visibleBoardNames }">
										<li role="presentation" class="event-item col-md-2">											
											<a href="<c:url value='/mainEvents/${mainEvent.id}/series/${mainEventSeries.id }/${boardName }' />">
												<spring:message code="board.${boardName }" text=''/>
											</a>
										</li>
									</c:forEach>
								</ul>
								<!-- Tab panes -->
								<div class="tab-content">
									<c:out value="${mainEventSeries.contents}" escapeXml="false" />
									
								</div>
							</div>
						</div>						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		 
	});
</script>