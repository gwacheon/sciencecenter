<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-purple">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
		<nav id="scrollspy-nav"
			class="navbar-default navbar-static has-scroll">
			<div class="container">
				<ul class="nav navbar-nav">
					<li class="dropdown">
						<a class="dropdown-toggle" type="button" data-toggle="dropdown" role="button" 
							aria-haspopup="true" aria-expanded="true">
							<spring:message code="${currentCategory }" text=''/><i class="material-icons">&#xE313;</i>
						</a>
						<ul class="dropdown-menu">
							<c:forEach var="i" begin="1" end="9">
								<c:set var="main_category" value="main.nav.catetory${i }"/>
								<spring:message var="main_category_url" code="main.nav.catetory${i }.url" scope="application"/>
								<c:if test="${not empty main_category && main_category eq currentCategory}">
								 	<c:forEach var="j" begin="1" end="12">
										<spring:message var="sub_category" code="main.nav.catetory${i}.sub${j}" scope="application" text='' />
										<spring:message var="sub_category_url" code="main.nav.catetory${i}.sub${j}.url" scope="application" text='' />
										<c:if test="${not empty sub_category }">
											<li>
												<a href="<c:url value="${sub_category_url }" />" >
													<c:out value="${sub_category }" />
												</a>
											</li>
										</c:if>
									</c:forEach>
								</c:if>
							</c:forEach>
							<c:forEach items="${mainEvents }" var="mainEvent">
								<li>
									<a href="<c:url value="/events/mainEvents/${mainEvent.id }" />">
	 									<c:out value ="${mainEvent.name }"/>
	 								</a>
								</li>
							</c:forEach>
						</ul>
					</li>
					<li class="active">
						<a href="#board-spy" class="item">
							${mainEvent.name}
						</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<div class="bread-crumbs">행사 공연 캠프 / ${mainEvent.name}</div>
					</li>
				</ul>
			</div>
		</nav>
	</div>
	<div id="events-body" class="sub-body">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title purple">
						<div class="top-line"></div>
						${mainEvent.name}
						${mainEvent.series[0].name}
					</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-right">
					<div class="dropdown hall-select">
						<a class="dropdown-toggle" type="button" data-toggle="dropdown"
							role="button" aria-haspopup="true" aria-expanded="true"> 다른
							회차로 이동 <i class="fa fa-angle-down" aria-hidden="true"></i>
						</a>
						<ul class='dropdown-menu'>
							<c:forEach var="series" items="${mainEvent.series }">
								<li>
									<a href="<c:url value='/events/mainEvents/${series.id}' />">
										<c:out value="${series.name}" />
									</a>
								</li>
							</c:forEach>
						</ul>
					</div>
				</div>
			</div>
			<div class="row">
				<div id="main-event-wrapper" class="col-md-12">
					<div class="row">
						
						<div class="col-md-12 ">
							<div class="main-events">
								<!-- Nav tabs -->
								<ul class="nav nav-tabs" role="tablist">
									<li role="presentation" class="event-item col-md-2 active">
										<a href="#home"
										aria-controls="home" role="tab" data-toggle="tab"> 
											기본안내 
										</a>
									</li>
									<li role="presentation" class="event-item col-md-2 ">
										<a href="#profile"
										aria-controls="profile" role="tab" data-toggle="tab">
											공지사항
										</a>
									</li>
									<li role="presentation" class="event-item col-md-2 ">
										<a href="#messages"
										aria-controls="messages" role="tab" data-toggle="tab">
											Q&A 
										</a>
									</li>
									<li role="presentation" class="event-item col-md-2 ">
										<a href="#settings"
										aria-controls="settings" role="tab" data-toggle="tab">
											Faq 
										</a>
									</li>
									<li role="presentation" class="event-item col-md-2 ">
										<a href="#settings"
										aria-controls="settings" role="tab" data-toggle="tab">
											자료실 
										</a>
									</li>
									<li role="presentation" class="event-item col-md-2 ">
										<a href="#settings"
										aria-controls="settings" role="tab" data-toggle="tab">
											사진첩 
										</a>
									</li>
									<li role="presentation" class="event-item col-md-2 ">
										<a href="#settings"
										aria-controls="settings" role="tab" data-toggle="tab">
											수상작 
										</a>
									</li>
									<li role="presentation" class="event-item col-md-2 ">
										<a href="#settings"
										aria-controls="settings" role="tab" data-toggle="tab">
											수상작추천관람코스 
										</a>
									</li>
								</ul>
								<!-- Tab panes -->
								<div class="tab-content">
									<div role="tabpanel" class="tab-pane active" id="home">
										<c:out value="${mainEvent.series[0].contents}" escapeXml="false" />
									</div>
									<div role="tabpanel" class="tab-pane" id="profile">...</div>
									<div role="tabpanel" class="tab-pane" id="messages">...</div>
									<div role="tabpanel" class="tab-pane" id="settings">...</div>
								</div>
							</div>
						</div>						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	<script type="text/javascript">
	$(function() {
		 
	});
</script>