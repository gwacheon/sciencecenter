<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-purple">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>
	<div id="events-body" class="sub-body">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title purple">
						<div class="top-line"></div>
						${mainEventSeries.name}
					</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-right">
					<c:import url="/WEB-INF/views/events/mainEvents/eventSeriesNav.jsp"/>
				</div>
			</div>
			<div class="row">
				<div id="main-event-wrapper" class="col-md-12">
					<div class="row">
						<div class="col-md-12 ">
							<div class="main-events">
								<!-- Nav tabs -->
								<ul class="nav nav-tabs">
									<li class="event-item col-md-2 <c:if test="${type eq 'basicInfo' }">active</c:if>">
										
										<a href="<c:url value='/mainEvents/${mainEvent.id}/series/${mainEventSeries.id }' />"> 
											기본안내 
										</a>
									</li>
									<c:forEach var="boardName" items="${mainEventSeries.visibleBoardNames }">
										<li role="presentation" class="event-item col-md-2 <c:if test="${type.name eq boardName }">active</c:if>">											
											<a href="<c:url value='/mainEvents/${mainEvent.id}/series/${mainEventSeries.id }/${boardName }' />">
												<spring:message code="board.${boardName }" text=''/>
											</a>
										</li>
									</c:forEach>
								</ul>
								<!-- Tab panes -->
								<div class="tab-content">
									<div id="board-show" class="board-show-purple">
			
									</div>
								</div>
							</div>
						</div>						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<c:import url="/WEB-INF/views/boards/show.jsp"></c:import>
<script type="text/javascript">
	var board = new Board(
	        "#board-show",
	        "<c:url value='/mainEvents/${mainEvent.id}/series/${mainEventSeries.id }/${type.name }'/>",
	        {
	            id: <c:out value="${id}"/>,
	            memberNo: "${currentMemberNo}",
	            redirectUrl: "/mainEvents/${mainEvent.id}/series/${mainEventSeries.id }/${type.name }",
	            isEditable : true,
	            deleteUrl : "<c:url value='/communication/opinions/private/delete'/>"
	        }
	    );
	board.renderBoardShow();
</script>