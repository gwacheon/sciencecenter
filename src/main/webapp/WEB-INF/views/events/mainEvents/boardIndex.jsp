<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-purple">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>
	<div id="events-body" class="sub-body">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title purple">
						<div class="top-line"></div>
						${mainEventSeries.name}
					</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-right">
					<c:import url="/WEB-INF/views/events/mainEvents/eventSeriesNav.jsp"/>
				</div>
			</div>
			<div class="row">
				<div id="main-event-wrapper" class="col-md-12">
					<div class="row">
						<div class="col-md-12 ">
							<div class="main-events">
								<!-- Nav tabs -->
								<ul class="nav nav-tabs">
									<li class="event-item col-md-2 <c:if test="${type eq 'basicInfo' }">active</c:if>">
										
										<a href="<c:url value='/mainEvents/${mainEvent.id}/series/${mainEventSeries.id }' />"> 
											기본안내 
										</a>
									</li>
									<c:forEach var="boardName" items="${mainEventSeries.visibleBoardNames }">
										<li role="presentation" class="event-item col-md-2 <c:if test="${type.name eq boardName }">active</c:if>">											
											<a href="<c:url value='/mainEvents/${mainEvent.id}/series/${mainEventSeries.id }/${boardName }' />">
												<spring:message code="board.${boardName }" text=''/>
											</a>
										</li>
									</c:forEach>
								</ul>
								<!-- Tab panes -->
								<div class="tab-content">
									<div id="board-wrapper" class="table-purple">
										<c:url var="currentPath" value="/mainEvents/${mainEvent.id}/series/${mainEventSeries.id }/${type.name }"/>
							 			
							 			<c:choose>
							 				<c:when test="${type.hasMainPicture}">
							 					<c:import url="/WEB-INF/views/boards/gallery.jsp">
									 				<c:param name="path" value="${currentPath }"/>
									 			</c:import>
							 				</c:when>
							 				<c:otherwise>
									 			<c:import url="/WEB-INF/views/boards/list.jsp">
									 				<c:param name="path" value="${currentPath }"/>
									 			</c:import>
									 			<c:if test="${type.name  eq 'mainEventSeriesQna'}">	
													<div class="text-right">
														<a href="<c:url value='/mainEvents/${mainEvent.id}/series/${mainEventSeries.id }/${type.name }/private/new' />" class="btn btn-purple">
															글쓰기
														</a>
													</div>
												</c:if>
							 				</c:otherwise>
							 			</c:choose>
									</div>
								</div>
							</div>
						</div>						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>