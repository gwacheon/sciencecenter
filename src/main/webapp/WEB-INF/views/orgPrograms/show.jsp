<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-purple">
		<nav id="scrollspy-nav" class="navbar-default navbar-static has-scroll">
			<div class="container">
				<ul class="nav navbar-nav">
					<c:import url="/WEB-INF/views/schedules/navbar.jsp"/>
					<li>
						<a href="basic-info-spy" class="item active">
							단체예약
						</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<div class="bread-crumbs">
							예약 신청 / 단체예약 
						</div>
					</li>
				</ul>
			</div>
		</nav>
	</div>
	
	<div class="sub-body">
		<div id="basic-info-spy" class="narrow-sub-top structure-rent no-nav-tabs">
			<div class="container">
				<div class='row'>
					<div class="col-md-12">
						<h4 class="doc-title">
							단체 예약 안내	
						</h4>
						<h6 class="doc-title exp-desc">
							국립과천과학관에서는 과학기술문화 확산을 위한 교육 및 행사를 장려하기 위해<br>
							홀, 숙소, 강의실, 실험실등 다양한 시설 대관 서비스를 제공합니다.						
						</h6>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="page-title purple">
					<div class="top-line"></div>
					단체 예약
				</h3>
			</div>
			
			<div class="col-md-12">
				${orgProgram.title }
			</div>
			
			<div class="col-md-12">
				<c:out value="${orgProgram.description }" escapeXml="false" />
			</div>
		</div>
	</div>
</div>