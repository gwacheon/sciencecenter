<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-purple">
		<nav id="scrollspy-nav" class="navbar-default navbar-static has-scroll">
			<div class="container">
				<ul class="nav navbar-nav">
					<c:import url="/WEB-INF/views/schedules/navbar.jsp"/>
					<li>
						<a href="#basic-info-spy" class="item active">
							단체예약
						</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<div class="bread-crumbs">
							예약 신청 / 단체예약 
						</div>
					</li>
				</ul>
			</div>
		</nav>
	</div>
	
	<div class="sub-body">
		<div id="basic-info-spy" class="narrow-sub-top structure-rent no-nav-tabs">
			<div class="container">
				<div class='row'>
					<div class="col-md-12">
						<h4 class="doc-title">
							단체 예약 안내	
						</h4>
						<h6 class="doc-title exp-desc">
							국립과천과학관에서는 과학기술문화 확산을 위한 교육 및 행사를 장려하기 위해<br>
							홀, 숙소, 강의실, 실험실등 다양한 시설 대관 서비스를 제공합니다.						
						</h6>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="page-title purple">
					<div class="top-line"></div>
					단체 예약
				</h3>
			</div>
			
			<div class="col-md-12">
				${orgProgram.title }
			</div>
			
			<div class="col-md-5">
				<div id="org-program-calendars"></div>
			</div>
			<div class="col-md-7">
				<div id="org-program-times">
				</div>
			</div>
		</div>
	</div>
</div>

<div id="org-program-reserve-modal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">단체예약</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-8">
						<div class="form-group">
							<label for="applicant_name">신청자명</label>
							<input type="text" class="form-control" id="applicant_name"
								value="${member.memberName }" readonly>
						</div>	
					</div>
					
					<div class="col-sm-4">
						<div class="form-group">
							<label for="">신청유형</label>
							
							<div class="radio radio-purple">								
								<input type="radio" name="applicantType" id="applicant_type_kinder" value="kinder" checked>
								<label for="applicant_type_kinder"> 유치부 </label>
								
								<input type="radio" name="applicantType" id="applicant_type_elementary" value="elementary">
								<label for="applicant_type_elementary"> 초등부 </label>
							</div>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label for="applicant_member">신청인원</label>
					<input type="number" class="form-control" id="applicant_member">
					<p class="help-block">초등부는 최대 30명, 유치부는 최대 50명까지 신청 가능합니다.</p>
				</div>
				
				<div class="form-group">
					<label for="applicant_member">비고</label>
					<textarea class="form-control" id="applicant_description"></textarea>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-purple reverse" data-dismiss="modal">취소</button>
				<button id="submit-applicant" type="button" class="btn btn-purple">예약신청</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script type="text/javascript" src="<c:url value='/resources/js/orgProgram/orgProgram.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/orgProgram/orgApplicant.js'/>"></script>
<script type="text/javascript">
	$(function(){
		var orgProgramCalendar = new OrgProgramCalendar({
			orgProgramId: "${orgProgram.id}",
			date: moment(new Date()),
			container: "#org-program-calendars",
			template: "#org-program-calendar-template",
			baseUrl: baseUrl,
			activeColor: "purple",
			url: "<c:url value='/orgPrograms/${orgProgram.id}/calendars'/>",
			submitUrl: "<c:url value='/orgPrograms/${orgProgram.id}/applicant'/>"
		});
	});	
</script>

<script id="org-program-calendar-template" type="text/x-handlebars-template">
<div class="month-selector" style="text-align: center">
	<a id="prev-month" class="month-selector purple" href="#" data-date="{{prevMonth}}">
		&lt;
	</a>

	<div id="selected-month" style="display: inline-block">
		{{formatedDate selectedDate "YYYY.MM"}}
	</div>

	<a id="next-month" href="#" class="month-selector purple" data-date="{{nextMonth}}">
		&gt;
	</a>
</div>

<div id="structure-calendar" class="calendar-area">
	<table id="calendar-table" class="ui celled table unstackable">
		<thead>
			<tr>
				<th class="center">일</th>
				<th class="center">월</th>
				<th class="center">화</th>
				<th class="center">수</th>
				<th class="center">목</th>
				<th class="center">금</th>
				<th class="center">토</th>
			</tr>
		</thead>
		<tbody class="calendar-body">
			{{#each dates}}
				<tr>
					{{#each this}}
						<td class="center date {{#if this.disable}}disabled{{else}}available{{/if}}" data-date="{{formatedDate this.date "YYYY-MM-DD"}}">
							<div class="circle">
								{{formatedDate this.date "DD"}}
							</div>
						</td>
					{{/each}}
				</tr>
			{{/each}}
		</tbody>
	</table>
</div>
</script>

<script id="org-program-times-template" type="text/x-handlebars-template">
<table class="table">
	<thead>
		<tr>
			<td class="center">시간</td>
			<td class="center">초등부 예약가능</td>
			<td class="center">현황</td>
			<td></td>	
		</tr>
	</thead>
	<tbody>
		{{#each applicants}}
			<tr>	
				<td class="center">{{timeStr}}</td>
				<td class="center" data-elementary="{{hasElementary}}">
					{{#if hasElementary}}
						X
					{{else}}
						O
					{{/if}}
				</td>
				<td class="center">
					{{members}} / ${orgProgram.maxMember}
				</td>
				<td class="text-right">
					<a href="#" class="reserve-btn btn btn-purple btn-sm reverse" 
						data-idx="{{@index}}" data-time="{{timeStr}}">
						예약신청
					</a>
				</td>
			</tr>
		{{/each}}
	</tbody>
</table>
</script>