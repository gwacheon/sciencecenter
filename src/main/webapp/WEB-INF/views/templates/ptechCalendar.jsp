<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<script id="ptech-calendar-template" type="text/x-handlebars-template">
  <div class="ptech-calendar">
	<div class="calendar-nav">
		<div class="row nav-title">
			<div class="col-md-12 center">
				<a href="#" class="prev-month month-nav-item" data-date="{{dateFormat prevMonth}}">
					&lt;
				</a>

				{{dateFormat beginOfMonth  format="YYYY년 MM월"}}

				<a href="#" class="next-month month-nav-item" data-date="{{dateFormat nextMonth}}">
					&gt;
				</a>
			</div>
		</div>
		<div class="ptech-week week-label">
			<div class="ptech-date week-day sunday">일</div>
			<div class="ptech-date week-day">월</div>
			<div class="ptech-date week-day">화</div>
			<div class="ptech-date week-day">수</div>
			<div class="ptech-date week-day">목</div>
			<div class="ptech-date week-day">금</div>
			<div class="ptech-date week-day">토</div>
		</div>
	</div>

	
    {{#each dates}}
		{{#checkWeek @index 0}}
			<div class="ptech-week">
		{{/checkWeek}}

		{{#checkWeek @index 0}}
			<div class="ptech-date sunday" data-date="{{dateFormat this}}" onclick=''>
				<div>
					{{dateFormat this format="DD"}}
				</div>
			</div>
		{{else}}
			{{#checkWeek @index 6}}
				<div class="ptech-date saturday" data-date="{{dateFormat this}}" onclick=''>
					<div>
						{{dateFormat this format="DD"}}
					</div>
				</div>
			{{else}}
				<div class="ptech-date" data-date="{{dateFormat this}}" onclick=''>
					<div>
						{{dateFormat this format="DD"}}
					</div>
				</div>
			{{/checkWeek}}
		{{/checkWeek}}

		{{#checkWeek @index 6}}
			{{#compare @index 0 operator="!="}}
				</div>
			{{/compare}}
		{{/checkWeek}}
	{{/each}}
  </div>
</script>