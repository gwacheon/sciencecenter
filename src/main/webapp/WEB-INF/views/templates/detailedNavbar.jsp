<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="navbar-detailed">
	<div class="sub-category-wrapper">
		<div class="container">
			<ul>
				<c:forEach var="i" begin="1" end="9">
					<c:set var="sub_category" value="${crumb.mainCategory}.sub${i }" />
					
					<spring:message var="sub_category_name" code="${sub_category }" scope="application" text='' />
					<spring:message var="sub_category_dynamic_name" code="${sub_category }.dynamic_name" scope="application" text='' />
					<spring:message var="sub_category_url" code="${sub_category }.url" scope="application" text='' />
					<c:if test="${not empty sub_category_name && sub_category_name != '' }">
						<c:choose>
							<c:when test="${ sub_category_dynamic_name eq 'dynamicMajorEventsCategory'}">
								<c:set var="loopFinished" value="false"/>
								<c:forEach items="${navMainEvents }" var="mainEvent" varStatus="status">
									<c:if test="${loopFinished eq 'false' }">
										<c:if test="${ mainEvent.subType eq 'series' }">
											<li class="sub-category <c:if test="${crumb.subCategoryNumber eq '2' }">active</c:if>">
												<a href="<c:url value="/mainEvents/${ mainEvent.id }" />" class="sub-item">
													<c:out value ="${sub_category_name }"/>
												</a>
											</li>
											<c:set var="loopFinished" value="true"/>												
										</c:if>	
									</c:if>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<li class="sub-category <c:if test="${sub_category eq crumb.subCategory}">active</c:if>">
									<a href="<c:choose><c:when test="${sub_category eq crumb.subCategory }">#</c:when><c:otherwise><c:url value='${sub_category_url }' /></c:otherwise></c:choose>">
										<c:out value="${sub_category_name }" />
									</a>
								</li>
							</c:otherwise>
						</c:choose>
					</c:if>
				</c:forEach>
			</ul>
		</div>
	</div>
	<div class="sub-child-wrapper">
		<div class="container">
			<ul>
				<c:if test="${crumb.mainCategoryNumber eq '4' && crumb.subCategoryNumber eq '2' }">
					<c:set var="subChildIndex" value="1"/>
					<c:forEach items="${navMainEvents }" var="mainEvent">
						<li class="sub-child <c:if test='${crumb.mainEventId eq mainEvent.id }'>active</c:if>">
							<c:choose>
								<c:when test="${mainEvent.subType eq 'link' }">
									<a href="<c:url value='${mainEvent.linkUrl }' />" class="sub-item" target="_blank">
										<c:out value ="${mainEvent.name }"/>
									</a>
								</c:when>
								<c:otherwise>
									<a href="<c:url value="/mainEvents/${ mainEvent.id }" />" class="sub-item">
										<c:out value ="${mainEvent.name }"/>
									</a>						
								</c:otherwise>
							</c:choose>
						</li>
					</c:forEach>
				</c:if>
				<c:forEach var="i" begin="1" end="12">
					<c:set var="sub_child" value="${crumb.subCategory }.sub${i }" />
					<spring:message var="sub_child_name" code="${sub_child }" scope="application" text='' />
					<spring:message var="sub_child_url" code="${sub_child }.url" scope="application" text='' />
					<c:if test="${not empty sub_child_name && sub_child_name != '' && sub_child_name ne 'dynamicEvents' }">
						<c:if test="${crumb.dynamicEvent eq 'true' }">
							<c:if test="${sub_child_url eq 'dynamicMajorEvents' }">
							</c:if>
						</c:if>
						<li class="sub-child <c:if test='${sub_child eq crumb.subChild}'>active</c:if>">
							<a href="<c:choose><c:when test="${sub_child eq crumb.subChild }">#</c:when><c:otherwise><c:url value='${sub_child_url }' /></c:otherwise></c:choose>">
								<c:out value="${sub_child_name }" />
							</a>
						</li>
					</c:if>
				</c:forEach>
			</ul>
		</div>
	</div>
</div>