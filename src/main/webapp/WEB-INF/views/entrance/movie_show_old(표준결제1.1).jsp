<%@page import="org.json.JSONObject"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="java.util.*"%>
<%@page import="kr.go.sciencecenter.util.DateUtil"%>
<%@page import="kr.go.sciencecenter.model.api.Member"%>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%

	List<HashMap<String, String>> pList = (List<HashMap<String, String>>)request.getAttribute("ProgramList");
	List<HashMap<String, String>> classList = (List<HashMap<String, String>>)request.getAttribute("ClassList");
	String AcademyCd = (String)request.getAttribute("AcademyCd");
	String ClassCd = (String)request.getAttribute("ClassCd");
	
	//out.println("### AcademyCd : " + AcademyCd);
	//out.println("### ClassCd : " + ClassCd);
	//out.println("### classList : " + classList);
%>

<script src="<%=request.getContextPath()%>/resources/payment1.1/script/defConst.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/csquare/dateformat.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/csquare/util.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/csquare/entrance/reserve.js"></script>

<%
	String strUserInfo = "";
	JSONObject json = null;

try {
		Map user_info = (Map)request.getAttribute("UserInfo");
		if (user_info != null)
			json = new JSONObject(user_info);
//		out.print(">>>>> START" + "<BR>\n");
//		out.print(">>>>> Name : " + user_info.get("MEMBER_NAME") + "<BR>\n");
//		out.print(">>>>> END" + "<BR>\n");
	} catch (Exception e) {
	}
%>

<script>
	var context_path = '<%=request.getContextPath()%>';
	var user_info = <%= (json == null) ? "null" : json.toString() %>;
</script>

<!-- 예약 완료 화면 -->
<div id="complete-container" class="container margin-top20"
	style="display: none">
	<div class="row">
		<div class="col-md-12">
			<a href="#" class="btn btn-teal visible-xs"> 예약내역 바로가기 </a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="reservation-receipe-wrapper">
				<div class="receipe-header">
					<h5 class="doc-title">예약증</h5>
				</div>

				<div class="receipe-body margin-bottom20">
					<h2 class="doc-title" id="program_name">천체투영관</h2>
					<h5 class="doc-title" id="play_date">2015년 10월 08일 금요일</h5>
					<h5 class="doc-title" id="play_seq">2회차 12:00~13:00</h5>
					<h5 class="doc-title" id="place_name">국립과천과학관 천체투영관</h5>
				</div>

				<div class="receipe-info">
					<div class="table-responsive">
						<table class="table lined-table">
							<tr>
								<th>예약구분</th>
								<td>상영관 예약</td>
								<th>예약코드</th>
								<td><div id="oid">R845788555</div></td>
							</tr>
							<tr>
								<th>예약일시</th>
								<td><div id="reserve_date">2015년 8월 12일 13:34</div></td>
								<th>예약상태</th>
								<td><div id="reserve_status">예약완료</div></td>
							</tr>
							<tr>
								<th>예약자명</th>
								<td><div id="reserve_name">성춘향</div></td>
								<th>연락처</th>
								<td><div id="reserve_phone">010-1234-5678</div></td>
							</tr>
							<tr>
								<th>관람인원</th>
								<td><div id="total_count">5명</div></td>
								<th>결제금액</th>
								<td><div id="total_price">300,000원</div></td>
							</tr>
							<tr>
								<th>결제방식</th>
								<td colspan="3"><span class="teal" id="payment">신용카드 (하나카드 1234-4567-****-****) 결제완료</span></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-12 center margin-bottom20">
			<a href="#" class="btn btn-teal reverse hidden-xs margin-right20">예약증 인쇄하기</a> 
			<a href="#" class="btn btn-teal hidden-xs">예약내역 바로가기</a>
		</div>
	</div>
</div>

	<div class="sub-content-nav scrollspy-purple">
		<nav id="scrollspy-nav" class="navbar-default navbar-static">
			<div class="container">
				<ul class="nav navbar-nav">
					<li class="dropdown">
						<a class="dropdown-toggle" type="button" data-toggle="dropdown" role="button" 
							aria-haspopup="true" aria-expanded="true">
							<spring:message code="${currentCategory }" text=''/><i class="material-icons">&#xE313;</i>
						</a>
						<ul class="dropdown-menu">
							<c:forEach var="i" begin="1" end="9">
								<c:set var="main_category" value="main.nav.catetory${i }"/>
								<spring:message var="main_category_url" code="main.nav.catetory${i }.url" scope="application"/>
								<c:if test="${not empty main_category && main_category eq currentCategory}">
								 	<c:forEach var="j" begin="1" end="12">
										<spring:message var="sub_category" code="main.nav.catetory${i}.sub${j}" scope="application" text='' />
										<spring:message var="sub_category_url" code="main.nav.catetory${i}.sub${j}.url" scope="application" text='' />
										<c:if test="${not empty sub_category }">
											<li>
												<a href="<c:url value="${sub_category_url }" />" >
													<c:out value="${sub_category }" />
												</a>
											</li>
										</c:if>
									</c:forEach>
								</c:if>
							</c:forEach>
						</ul>
					</li> 
					
					<c:set value="" var="AcademyName" />
					<c:forEach items="${AcamedyList }" var="AcamedyList">
						<c:if test="${AcademyCd eq AcamedyList.ACADEMY_CD }">
							<li class="active">
								<a href="javascript:void(0)" class="item" onclick="AcademyChoice('${AcamedyList.ACADEMY_CD}')">
									${AcamedyList.ACADEMY_NAME}
									<c:set value="${AcamedyList.ACADEMY_NAME}" var="AcademyName" /> 
								</a>				
							</li>
						</c:if>
						<c:if test="${AcademyCd ne AcamedyList.ACADEMY_CD }">
							<li>
								<a href="javascript:void(0)" class="item" onclick="AcademyChoice('${AcamedyList.ACADEMY_CD}')">
									${AcamedyList.ACADEMY_NAME}
								</a>				
							</li>
						</c:if>
						
					</c:forEach>
					<li>
						<a href="#" class="item">
							행사&middot;공연
						</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<div class="bread-crumbs">
							전시관람 / 기초과학관
						</div>
					</li>
				
				</ul>
			</div>
		</nav>
	</div>

<!-- 예약 화면 -->
<div id="schedule-container" class="sub-body">
	<div id="schedule-banner" class="hidden-xs">
		<h3>예약하기</h3>
					<p>
						국립과천과학관에서 진행되는 다양한 전시 체험 등을 한 눈에 쉽게 살펴보시고, <br>
						보다 간편하게 예약 가능합니다.
					</p>
					<div id="schedule-types">
						<div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<ul class="link-tabs">
									<%
										for(int i=0; i<classList.size(); i++){
											HashMap<String, String> obj = (HashMap)	classList.get(i);
											String cd = obj.get("CLASS_CD");
									%>
										<li class="tab col-xs-4">
											<a href="javascript:fnClassSelect('<%= cd %>')" <% if(cd.equals(ClassCd)){ %> class="active" <% } %>>
												<%= obj.get("CLASS_NAME") %>
											</a>
										</li>
									<%
										}
									%>	
									<!-- 	
										<li class="tab col-xs-4">
											<a href="#">
												천체관측소
											</a>
										</li>
										<li class="tab col-xs-4">
											<a href="#" class="active">
												스페이스월드
											</a>
										</li>
									 -->	
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="container margin-bottom30">
					<div class="row">
						<div class="col-md-12">
							<h4 class="schedule-title">
								스페이스월드 체험프로그램 예약
							</h4>
						</div>
					</div>


					<div id="step2" class="row reservation-process">
						<div class="col-md-12">
							<div class="process-wrapper clearfix">
								<div class="process-title">
									01. 날짜/회차 선택 <span id="selected-time" class="right"><span class="text"></span> <i class="fa fa-chevron-down down-icon"></i><i class="fa fa-chevron-up up-icon"></i></span>
								</div>
								<div class="process-contents" style="display: none;">
									<div class="row">
										<div class="col-md-6">
											<div id="program-calendar-wrapper"></div>
											<div class="reservation-status-infos right">
												<span class="info-item available"></span> 예약가능일
												<span class="info-item waited"></span> 접수예정일
											</div>
										</div>

										<div class="col-md-6">
											<div id="program-times" style="display: none">
												<table class="table centered-table">
													<thead>
														<tr>
															<th class="center">회차</th>
															<th class="center">시간</th>
															<th class="center">잔여/인원</th>
															<th></th>
														</tr>
													</thead>

													<tbody>
														<tr>
															<td class="center">1회차</td>
															<td class="center">10:00~11:00</td>
															<td class="center">30/30</td>
															<td class="center">
																<button type="button" class="btn btn-disabled">
																	마감
																</button>
															</td>
														</tr>
														<tr>
															<td class="center">2회차</td>
															<td class="center">12:00~13:00</td>
															<td class="center">25/30</td>
															<td class="center">
																<button type="button" class="btn btn-primary reserve-btn" data-id="1" data-title="2015년 5월 25일 1회차 10:00">
																	선택
																</button>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

<div id="step3" class="row reservation-process">
	<div class="col-md-12">
		<div class="process-wrapper clearfix">
			<div class="process-title">
				02. 권종/인원선택 <span id="selected-price" class="right"><span class="text"></span> <i class="fa fa-chevron-down down-icon"></i><i class="fa fa-chevron-up up-icon"></i></span>
			</div>
			<div class="process-contents" style="display: none">
				<div class="col-xs-12">
					<table class="table centered-table">
						<thead>
							<tr>
								<th width="20%">권종</th>
								<th width="20%">요금</th>
								<th width="20%">인원 선택</th>
								<th width="20%">금액</th>
								<th width="20%">결제금액</th>
							</tr>
						</thead>
						
						<tbody>
							<tr>
								<td>어른(20세~)</td>
								<td>2,000</td>
								<td>
									<select class="form-control member-price" data-price="2000"
										data-target="#adult-price">
										<option value="0">인원</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
									</select>
								</td>
								<td id="adult-price">원</td>
								<td rowspan="4" id="total-price"></td>
							</tr>
							<tr>
								<td>청소년(~19세)</td>
								<td>1,000</td>
								<td>
									<select class="form-control member-price" data-price="1000"
										data-target="#student-price">
										<option value="0">인원</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
									</select>
								</td>
								<td id="student-price">원</td>
							</tr>
							<tr>
								<td>우대</td>
								<td>1,000</td>
								<td>
									<select class="form-control member-price" data-price="1000"
										data-target="#membership-price">
										<option value="0">인원</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
									</select>
								</td>
								<td id="membership-price">원</td>
							</tr>
							<tr>
								<td>우대할인</td>
								<td>500</td>
								<td>
									<select class="form-control member-price" data-price="500"
										data-target="#membership-dc-price">
										<option value="0">인원</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
									</select>
								</td>
								<td id="membership-dc-price">원</td>
							</tr>
						</tbody>
					</table>
				</div>
				
				<div class="col-xs-12">
					<div class="margin-top20 margin-bottom20 center">
						<button class="btn btn-purple submit-pricing-btn">
							선택 완료
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="step4" class="row reservation-process">
	<div class="col-xs-12">
		<div class="process-wrapper clearfix">
			<div class="process-title">
				03. 결제하기 
			</div>

			<div class="process-contents" style="display: none;">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<div class="tab-content">
							<div class="row margin-top30 margin-bottom30">	
<!-- 
								<div class="margin-top20 margin-bottom20 center">
									<input type="radio" value="1" name="payKind" id="payKind1" onclick="setPayInfo(1);" checked=""><lable for="payKind1">신용카드</lable>
									<input type="radio" value="2" name="payKind" id="payKind2" onclick="setPayInfo(2);"><lable for="payKind2">가상계좌</lable>
								</div>
 -->
								<div class="col-sm-12 center">
									<a id="payment-btn" href="#" class="btn btn-purple" onClick="$('#payment-choice').modal()">
										결제하기  <span id="price-for-payment"></span>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<div id="member-modal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<h4 class="modal-title">대상자 추가등록</h4>
		</div>
		<div class="modal-body">
			<div>
				<form id="add-user-form" action="">
					<table class="table centered-table">
						<tbody>
							<tr>
								<th>이름</th>
								<td><input type="text" name="addUserName" id="add-user-name" class="form-control"/></td>
							</tr>
							<tr>
								<th>성별</th>
								<td class="left">
									<div class="radio radio-primary left">
										<input name="addUserSex" type="radio" id="add-user-sex-male" value="male" checked/>
										<label for="add-user-sex-male">남성</label>

										<input name="addUserSex" type="radio" id="add-user-sex-female" value="female" />
										<label for="add-user-sex-female">여성</label>
									</div>
								</td>
							</tr>
							<tr>
								<th>휴대폰</th>
								<td>
									<div class="row">
										<div class="col-xs-4 col-md-3">
											<select id="add-user-phone1" name="addUserPhone1" class="form-control">
												<option value="010" selected>010</option>
												<option value="011">011</option>
											</select>
											<label for="add-user-phone" style="display: none"></label>
										</div>
										<div class="col-xs-4 col-md-3">
											<input name="addUserPhone2" type="text" id="add-user-phone2"  class="form-control"/>
											<label for="add-user-phone2" style="display: none"></label>
										</div>
										<div class="col-xs-4 col-md-3">
											<input name="addUserPhone3" type="text" id="add-user-phone3"  class="form-control"/>
											<label for="add-user-phone3" style="display: none"></label>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<th>생년월일</th>
								<td>
									<div class="row">
										<div class="col-xs-4 col-md-3">
											<select id="add-user-birth-year" name="addUserBirthYear" class="form-control">

												<option value='2015'>2015년</option>

												<option value='2014'>2014년</option>

												<option value='2013'>2013년</option>

												<option value='2012'>2012년</option>

												<option value='2011'>2011년</option>

												<option value='2010'>2010년</option>

												<option value='2009'>2009년</option>

												<option value='2008'>2008년</option>

												<option value='2007'>2007년</option>

												<option value='2006'>2006년</option>

												<option value='2005'>2005년</option>

												<option value='2004'>2004년</option>

												<option value='2003'>2003년</option>

												<option value='2002'>2002년</option>

												<option value='2001'>2001년</option>

												<option value='2000'>2000년</option>

												<option value='1999'>1999년</option>

												<option value='1998'>1998년</option>

												<option value='1997'>1997년</option>

												<option value='1996'>1996년</option>

												<option value='1995'>1995년</option>

												<option value='1994'>1994년</option>

												<option value='1993'>1993년</option>

												<option value='1992'>1992년</option>

												<option value='1991'>1991년</option>

												<option value='1990'>1990년</option>

												<option value='1989'>1989년</option>

												<option value='1988'>1988년</option>

												<option value='1987'>1987년</option>

												<option value='1986'>1986년</option>

												<option value='1985'>1985년</option>

												<option value='1984'>1984년</option>

												<option value='1983'>1983년</option>

												<option value='1982'>1982년</option>

												<option value='1981'>1981년</option>

												<option value='1980'>1980년</option>

												<option value='1979'>1979년</option>

												<option value='1978'>1978년</option>

												<option value='1977'>1977년</option>

												<option value='1976'>1976년</option>

												<option value='1975'>1975년</option>

												<option value='1974'>1974년</option>

												<option value='1973'>1973년</option>

												<option value='1972'>1972년</option>

												<option value='1971'>1971년</option>

												<option value='1970'>1970년</option>

												<option value='1969'>1969년</option>

												<option value='1968'>1968년</option>

												<option value='1967'>1967년</option>

												<option value='1966'>1966년</option>

												<option value='1965'>1965년</option>

												<option value='1964'>1964년</option>

												<option value='1963'>1963년</option>

												<option value='1962'>1962년</option>

												<option value='1961'>1961년</option>

												<option value='1960'>1960년</option>

												<option value='1959'>1959년</option>

												<option value='1958'>1958년</option>

												<option value='1957'>1957년</option>

												<option value='1956'>1956년</option>

												<option value='1955'>1955년</option>

												<option value='1954'>1954년</option>

												<option value='1953'>1953년</option>

												<option value='1952'>1952년</option>

												<option value='1951'>1951년</option>

												<option value='1950'>1950년</option>

												<option value='1949'>1949년</option>

												<option value='1948'>1948년</option>

												<option value='1947'>1947년</option>

												<option value='1946'>1946년</option>

												<option value='1945'>1945년</option>

												<option value='1944'>1944년</option>

												<option value='1943'>1943년</option>

												<option value='1942'>1942년</option>

												<option value='1941'>1941년</option>

												<option value='1940'>1940년</option>

												<option value='1939'>1939년</option>

												<option value='1938'>1938년</option>

												<option value='1937'>1937년</option>

												<option value='1936'>1936년</option>

												<option value='1935'>1935년</option>

												<option value='1934'>1934년</option>

												<option value='1933'>1933년</option>

												<option value='1932'>1932년</option>

												<option value='1931'>1931년</option>

												<option value='1930'>1930년</option>

												<option value='1929'>1929년</option>

												<option value='1928'>1928년</option>

												<option value='1927'>1927년</option>

												<option value='1926'>1926년</option>

												<option value='1925'>1925년</option>

												<option value='1924'>1924년</option>

												<option value='1923'>1923년</option>

												<option value='1922'>1922년</option>

												<option value='1921'>1921년</option>

												<option value='1920'>1920년</option>

												<option value='1919'>1919년</option>

												<option value='1918'>1918년</option>

												<option value='1917'>1917년</option>

												<option value='1916'>1916년</option>

												<option value='1915'>1915년</option>

												<option value='1914'>1914년</option>

												<option value='1913'>1913년</option>

												<option value='1912'>1912년</option>

												<option value='1911'>1911년</option>

												<option value='1910'>1910년</option>

												<option value='1909'>1909년</option>

												<option value='1908'>1908년</option>

												<option value='1907'>1907년</option>

												<option value='1906'>1906년</option>

												<option value='1905'>1905년</option>

												<option value='1904'>1904년</option>

												<option value='1903'>1903년</option>

												<option value='1902'>1902년</option>

												<option value='1901'>1901년</option>

												<option value='1900'>1900년</option>

											</select>
											<label for="add-user-birth-year" style="display: none"></label>
										</div>
										<div class="col-xs-4 col-md-3">
											<select id="add-user-birth-month" name="addUserBirthMonth" class="form-control">

												<option value='1'>1월</option>

												<option value='2'>2월</option>

												<option value='3'>3월</option>

												<option value='4'>4월</option>

												<option value='5'>5월</option>

												<option value='6'>6월</option>

												<option value='7'>7월</option>

												<option value='8'>8월</option>

												<option value='9'>9월</option>

												<option value='10'>10월</option>

												<option value='11'>11월</option>

												<option value='12'>12월</option>

											</select>
											<label for="add-user-birth-month" style="display: none"></label>
										</div>
										<div class="col-xs-4 col-md-3">
											<select id="add-user-birth-day" name="addUserBirthDay" class="form-control">

												<option value='1'>1일</option>

												<option value='2'>2일</option>

												<option value='3'>3일</option>

												<option value='4'>4일</option>

												<option value='5'>5일</option>

												<option value='6'>6일</option>

												<option value='7'>7일</option>

												<option value='8'>8일</option>

												<option value='9'>9일</option>

												<option value='10'>10일</option>

												<option value='11'>11일</option>

												<option value='12'>12일</option>

												<option value='13'>13일</option>

												<option value='14'>14일</option>

												<option value='15'>15일</option>

												<option value='16'>16일</option>

												<option value='17'>17일</option>

												<option value='18'>18일</option>

												<option value='19'>19일</option>

												<option value='20'>20일</option>

												<option value='21'>21일</option>

												<option value='22'>22일</option>

												<option value='23'>23일</option>

												<option value='24'>24일</option>

												<option value='25'>25일</option>

												<option value='26'>26일</option>

												<option value='27'>27일</option>

												<option value='28'>28일</option>

												<option value='29'>29일</option>

												<option value='30'>30일</option>

												<option value='31'>31일</option>

											</select>
											<label for="add-user-birth-day" style="display: none"></label>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<th>
									학교
								</th>
								<td class="left">
									<input type="text" name="addUserSchool" id="add-user-school" class="form-control"/>
									<label for="add-user-school" style="display: none"></label>
								</td>
							</tr>

							<tr>
								<td class="center" colspan="2">
									<a href="#!" class="btn btn-white" data-dismiss="modal">
										취소		
									</a>
									<button type="button" class="add-user-btn btn btn-primary">
										추가		
									</button>
								</td>
							</tr>
						</tbody>
					</table>
				</form>

				<div id="add-users">
					<div>
						<h5>추가 대상자</h5>
					</div>
					<table class="table centered-table">
						<thead>
							<tr>
								<th class="center">이름</th>
								<th class="center">성별</th>
								<th class="center">휴대폰</th>
								<th class="center">생년월일</th>
								<th class="center">학교</th>
							</tr>
						</thead>
						<tbody id="add-users-body">

						</tbody>
						<tfoot>
							<tr>
								<td class="center" colspan="5">
									<button type="button" class="submit-add-users btn btn-primary">
										저장	
									</button>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-teal" data-dismiss="modal">확인</button>
		</div>
	</div>
	<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script id="user-info-template" type="text/x-handlebars-template">
	{{#eachInMap users}}
	<tr>
		<td>
			<input type="checkbox" name="userId" id="user-id-{{val.id}}" value="{{val.id}}"/>
			<label for="user-id-{{val.id}}"></label>
		</td>
		<td>{{val.name}}</td>
		<td>{{val.relationshipText}}</td>
		<td>{{val.sexText}}</td>
		<td>{{dateFormat val.birthDay format="YYYY.MM.DD"}}</td>
		<td>{{val.phone1}}-{{val.phone2}}-{{val.phone3}}</td>
	</tr>
	{{/eachInMap}}
</script>

<script type="text/javascript">
	Handlebars.registerHelper( 'eachInMap', function ( map, block ) {
		var out = '';
		Object.keys( map ).map(function( prop ) {
			out += block.fn( {key: prop, val: map[ prop ]} );
		});
		return out;
	} );

	//Render Users
	$(function(){
		var source   = $("#user-info-template").html();
		var template = Handlebars.compile(source);
		var html = template({users: users});
		
		$("#user-infos").html(html);
	});

	
	var totalPrice = 0;
/*	
	$(".member-price").change(function(e){
		var price = parseInt($(this).data("price"));
		var calPrice = price * parseInt($(this).val());
		$($(this).data("target")).text(numberWithCommas(calPrice) + " 원");

		totalPrice = 0;
		_.each($(".member-price"), function(d, i){
			totalPrice += parseInt($(d).data("price")) * parseInt($(d).val());
		});

		$("#total-price").text(numberWithCommas(totalPrice) + " 원");
	});
*/	
</script>

<script type="text/javascript">
	$(".submit-pricing-btn").click(function(){
		if (parseInt($('#total-price')[0].innerText) > 0) {
			$("#step3").removeClass("active");
			$("#step4").addClass("active");

			$("#selected-price > .text").text(numberWithCommas(totalPrice) + "원");

			$("#price-for-payment").text(" (" + numberWithCommas(totalPrice) + " 원)");

			$("#step3 .process-contents").slideUp();
			$("#step4 .process-contents").slideDown();
		} else {
			alert('인원을 선택해 주세요');			
		}
		
	});
</script>

<script type="text/javascript">
	var addUsers = [];

	$(".add-user-btn").click(function(e){
		var addUser = {
			name: $("#add-user-name").val(),
			sex : $("input[name='addUserSex']:checked").val(),
			phone1: $("#add-user-phone1").val(),
			phone2: $("#add-user-phone2").val(),
			phone3: $("#add-user-phone3").val(),
			birthYear: $("#add-user-birth-year").val(),
			birthMonth: $("#add-user-birth-month").val(),
			birthDay: $("#add-user-birth-day").val(),
			school: $("#add-user-school").val()
		};

		addUsers.push(addUser);

		var source   = $("#add-user-template").html();
		var template = Handlebars.compile(source);
		var html    = template(addUser);
		$("#add-users-body").append(html);

		$("#add-user-form")[0].reset();
	});

	$(".submit-add-users").click(function(e){
		ajaxLoading();
		alert("저장프로세스 수행");
		ajaxUnLoading();
		$('#member-modal').closeModal();
	});
</script>

<script id="add-user-template" type="text/x-handlebars-template">
	<tr>
		<td class="center">{{name}}</td>
		<td class="center">{{sex}}</td>
		<td class="center">{{phone1}}-{{phone2}}-{{phone3}}</td>
		<td class="center">{{birthYear}}.{{birthMonth}}.{{birthDay}}</td>
		<td class="center">{{school}}</td>
	</tr>
</script>

<script type="text/javascript">
	//Init Sample Users
	var users = {
		"1": {
			id: 1,
			name: "성춘향",
			relationship: "self",
			relationshipText: "본인",
			sex:"female",
			sexText: "여성",
			birthDay: moment("1999.01.01", "YYYYMMDD"),
			phone1: "010",
			phone2: "1234",
			phone3: "1234"
		},
		"2": {
			id: 2,
			name: "이몽룡",
			relationship: "spouse",
			relationshipText: "배우자",
			sex:"male",
			sexText: "남성",
			birthDay: moment("1999.01.01", "YYYYMMDD"),
			phone1: "010",
			phone2: "1234",
			phone3: "1234"
		}
	};
</script>
</div>

<div id="responsibility-wrapper">
	
</div>


		<div id="auth-modal" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">로그인</h4>
					</div>
					<div class="modal-body">
						<div class="remote-auth-form">
							<form>
								<div class="form-group">
									<label for="exampleInputEmail1">Email address</label>
									<input type="text" class="form-control" id="exampleInputEmail1" placeholder="Email">
								</div>
								<div class="form-group">
									<label for="exampleInputPassword1">Password</label>
									<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
								</div>
								<button id="login-btn" type="button" class="btn btn-teal">로그인</button>
							</form>
						</div>

						<div class="line gray margin-top20 margin-bottom20"></div>

						<div class="auth-links">
							<div class="margin-bottom20">
								<span class="margin-right20">&middot; 아이디/비밀번호가 생각나지 않으세요?</span>

								<a href="#" class="btn btn-teal reverse">
									아이디 찾기
								</a>

								<a href="#" class="btn btn-teal reverse">
									비밀번호 찾기
								</a>
							</div>

							<div class="margin-bottom20">
								<span class="margin-right20">&middot; 국립과천과학관의 다양한 서비스를 만나보세요!</span>

								<a href="#" class="btn btn-teal reverse">
									회원가입
								</a>
							</div>
						</div>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->


		<!-- 결제 방식 Modal -->
		<div class="modal fade" id="payment-choice">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="myModalLabel">결제방식을 선택해 주세요.</h4>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" onclick="setPayType(1);">신용카드</button>
						<button type="button" class="btn btn-primary" onclick="setPayType(2);">가상계좌</button>
					</div>
				</div>
			</div>
		</div>

		<script id="ptech-calendar-template" type="text/x-handlebars-template">
						<div class="ptech-calendar">
							<div class="calendar-nav">
								<div class="row nav-title">
									<div class="col-md-12 center">
										<a href="#" class="prev-month month-nav-item" data-date="{{dateFormat prevMonth}}">
											<i class="fa fa-chevron-left"></i>
										</a>

										{{dateFormat beginOfMonth  format="YYYY년 MM월"}}

										<a href="#" class="next-month month-nav-item" data-date="{{dateFormat nextMonth}}">
											<i class="fa fa-chevron-right"></i>
										</a>
									</div>
								</div>
								<div class="ptech-week week-label">
									<div class="ptech-date week-day">일</div>
									<div class="ptech-date week-day">월</div>
									<div class="ptech-date week-day">화</div>
									<div class="ptech-date week-day">수</div>
									<div class="ptech-date week-day">목</div>
									<div class="ptech-date week-day">금</div>
									<div class="ptech-date week-day">토</div>
								</div>
							</div>


							{{#each dates}}
							{{#checkWeek @index 0}}
							<div class="ptech-week">
								{{/checkWeek}}

								{{#checkWeek @index 0}}
								<div class="ptech-date sunday" data-date="{{dateFormat this}}" onclick=''>
									<div>
										{{dateFormat this format="DD"}}
									</div>
								</div>
								{{else}}
								{{#checkWeek @index 6}}
								<div class="ptech-date saturday" data-date="{{dateFormat this}}" onclick=''>
									<div>
										{{dateFormat this format="DD"}}
									</div>
								</div>
								{{else}}
								<div class="ptech-date" data-date="{{dateFormat this}}" onclick=''>
									<div>
										{{dateFormat this format="DD"}}
									</div>
								</div>
								{{/checkWeek}}
								{{/checkWeek}}

								{{#checkWeek @index 6}}
								{{#compare @index 0 operator="!="}}
							</div>
							{{/compare}}
							{{/checkWeek}}
							{{/each}}
						</div>
					</script>

					<script type="text/javascript">
						//Init Calendar
						var ptechCalendar = new PTechCalendar(new Date(), {
							container: "#program-calendar-wrapper"
						});

						//Init Sample Date
						var sampleDates = [];
						sampleDates.push(moment().format("YYYY/MM/DD"));
						sampleDates.push(moment().add(1, 'day').format("YYYY/MM/DD"));
						sampleDates.push(moment().add(2, 'day').format("YYYY/MM/DD"));
						sampleDates.push(moment().add(3, 'day').format("YYYY/MM/DD"));
						sampleDates.push(moment().add(7, 'day').format("YYYY/MM/DD"));
						sampleDates.push(moment().add(8, 'day').format("YYYY/MM/DD"));
						sampleDates.push(moment().add(9, 'day').format("YYYY/MM/DD"));

						_.forEach(sampleDates, function(d, i){
							$(".ptech-date[data-date='" + d + "']").addClass("available");
						});

						$(document).on("click", ".ptech-date.available", function(e){
							e.preventDefault();
							$(".ptech-date.available").removeClass("selected");
							$(this).addClass('selected');
							
							$("#program-times").show();
						});

						$(".reserve-btn").click(function(e){
							$("#auth-modal").modal();
						});

						$(document).on("click", "#login-btn", function(){
							$("#auth-modal").modal("hide");

							$("#selected-time > .text").text($(".reserve-btn:first-child").data("title"));

							$("#step2").removeClass("active");
							$("#step2 .process-contents").slideUp();
							
							$("#step3").addClass("active");
							$("#step3 .process-contents").slideDown();
						});
					</script>

		<script type="text/javascript">
			$(function(){
				$(".movie-program-row").click(function(e){
					e.preventDefault();
					$(".movie-program-row").removeClass("active");
					$("#selected-program > .text").text($(this).data("title"))
					$(this).addClass("active");

					$("#step1").removeClass("active");
					$("#step1 .process-contents").slideUp();
					
					$("#step2").addClass("active");
					$("#step2 .process-contents").slideDown();
				});

				$('ul.tabs.page-tabs > .tab > a').click(function(e){
					e.preventDefault();
					var tabName = $(this).attr('href');

					currentResponsibility = responsibilities.getByTabName(tabName);

					if(currentResponsibility != null){
						currentResponsibility.render();


					}else {
						$("#responsibility-wrapper").html('');
					}

					if($("#tabName").length == 1){
						$("#tabName").val(tabName);	
					}
				});

				$(document).on("change", "#all-payment-agree", function(){
					if($(this).is(":checked")){
						$("input[name='payment-agreement']").prop("checked", true);
					}else{
						$("input[name='payment-agreement']").prop("checked", false);
					}
				});

				$(document).on("change", ".receipe-type", function(){
					$("#receipe-type-option-wrapper").show();
				});

				$(document).on("change", "#receipe-option", function(){
					if($(this).val() == "phone"){
						$("#receipe-type-corporate").hide();
						$("#receipe-type-phone").show();
					}else {
						$("#receipe-type-phone").hide();
						$("#receipe-type-corporate").show();
					}
				});
			});
</script>

<div id="ajaxLoader">
	<img src="<%=request.getContextPath()%>/resources/img/ajax-loader.gif" alt="ajax-loader"/>
</div>

<script type="text/javascript">
	function ajaxLoading(){
		$("body").css("opacity", "0.5");
		$("#ajaxLoader").show();
	}

	function ajaxUnLoading(){
		$("body").css("opacity", "1");
		$("#ajaxLoader").hide();
	}
</script>
<form name="frmIndex">
	<input type="hidden" name="ACADEMY_CD" value="" />
	<input type="hidden" name="COURSE_CD" value="" />
	<input type="hidden" name="SEMESTER_CD" value="" />
	<input type="hidden" name="TYPE" value="" />
	<input type="hidden" name="FLAG" value="" />
	<input type="hidden" name="CLASS_CD" value="${ClassCd}" />
</form>
<script type="text/javascript">
function fnClassSelect(cd) {
	var frm = document.frmIndex;
	frm.ACADEMY_CD.value = "<%= AcademyCd %>";
	frm.CLASS_CD.value = cd;
	frm.action = '<c:url value="/entrance"/>?${_csrf.parameterName}=${_csrf.token}';
	frm.method = 'post';
	frm.submit();
}


function AcademyChoice(Cd){
	var frm = document.frmIndex;
	frm.ACADEMY_CD.value = Cd;
	if("ACD007" == Cd){
		frm.action = '<c:url value="/entrance"/>?${_csrf.parameterName}=${_csrf.token}';
	}else{
		frm.action = '<c:url value="/schedules"/>?${_csrf.parameterName}=${_csrf.token}';
	}
	frm.method = 'post';
	frm.submit();
}

	$(function(){
		$('body').scrollspy({ 
			target: '#scrollspy-nav',
			offset: 230
		});

		$("#scrollspy-nav.has-scroll a.item").click(function(e){
			e.preventDefault();

			var target = this.hash;
			var $target = $(target);

			$('html, body').stop().animate({
				'scrollTop': $target.offset().top - 150
			}, 500, 'swing', function () {
				window.location.hash = target;
			});
		});


		$('.dropdown-button').dropdown({
			inDuration: 300,
			outDuration: 225,
				    constrain_width: false, // Does not change width of dropdown to that of the activator
				    gutter: 0, // Spacing from edge
				    belowOrigin: false // Displays dropdown below the button
				  }
				  );
	});
</script>

<script type="text/javascript">
	$(".up-down-toggle-btn").click(function(e){
		if($(this).hasClass("active")){
			$(this).removeClass("active");
			$($(this).data("target")).slideUp();			
		}else {
			$(this).addClass("active");
			$($(this).data("target")).slideDown();
		}
	});
</script>




<!-- 결제 관련 START -->
<script>
	var payment_info = {};			// 결제 관련 정보
	
	
	function setPayInfo(kind) {
		payment_info.kind = kind;
		
		if (isMobile()) {
			// 서버 
			var pay_url = 'http://pgtest.mainpay.co.kr:8080/csStdPayment/';			// 개발
			//var pay_url = 'http://pg.mainpay.co.kr/csStdPayment/';				// 상용

			switch (payment_info.kind) {
				case 1:							// 신용카드
					payment_info.action = pay_url + 'mobstep01.do';						// 카드
					break;	
				case 2:							// 가상계좌
					payment_info.action = pay_url + 'mobvcstep01.do';					// 가상계좌 
					//payment_info.action = pay_url + 'mobacstep01.do';					// 계좌이체
					alert('가상계좌 결제');
					break;
			}
			
			
			payment_info.authType = 'auth';			// 카드사 인증만 지원 
			payment_info.target = '_self';
			payment_info.returnUrl = '<%= request.getScheme() + "://" +  request.getServerName() + ":" +  request.getServerPort() + request.getContextPath() + "/entrance/reserve/complete" %>';
			payment_info.callbackUrl = '<%=  request.getScheme() + "://" +  request.getServerName() + ":" +  request.getServerPort() + request.getContextPath() + "/resources/payment1.1/payResponse.jsp" %>';
		} else {
			switch (payment_info.kind) {
				case 1:							// 신용카드
					break;	
				case 2:							// 가상계좌
					break;
			}

			payment_info.action		 = '<%=request.getContextPath()%>/resources/payment1.1/dummy.jsp';
			payment_info.target		 = 'x_frame';
			payment_info.returnUrl = '<%= request.getScheme() + "://" +  request.getServerName() + ":" +  request.getServerPort() + request.getContextPath() + "/entrance/reserve/callback" %>';
			payment_info.callbackUrl = '<%=  request.getScheme() + "://" +  request.getServerName() + ":" +  request.getServerPort() + request.getContextPath() + "/resources/payment1.1/payResponse.jsp" %>';
		}
	}

	
	$(document).ready(function() {
		// 기본 카드 결제로 Setting
		setPayInfo(1);
		payment_info.mbr_id = '<%=(String)request.getAttribute("MbrId")%>';
	
		
		// 첫번째 항목 고정 
		selectProgram('<%= pList.get(0).get("PROGRAM_CD") %>', '<%= pList.get(0).get("PROGRAM_NAME") %>', '<%= pList.get(0).get("PLACE_CD") %>', '<%= pList.get(0).get("PLACE_NAME") %>');
		$("#step2").addClass("active");
		$("#step2 .process-contents").slideDown();
	});


</script>

	<div>
		<iframe name="x_frame" style="display:none" width="500" height="100"></iframe>

		<form name="mallForm" action="<%=request.getContextPath()%>/resources/payment1.1/dummy.jsp"  method="POST">
 			<input type="hidden" name="target" value="x_frame">
			<input type="hidden" name="xid" value="">
			<input type="hidden" name="eci" value="">
			<input type="hidden" name="cavv" value="">
			<input type="hidden" name="cardno" value="">
			<input type="hidden" name="hs_useamt_sh" value="">
			<!-- 암호화된 인증관련 정보 -->
			<input type="hidden" name="encData" value="">
			<input type="hidden" name="callbackUrl"	value = '<%=  request.getScheme() + "://" +  request.getServerName() + ":" +  request.getServerPort() + request.getContextPath() + "/resources/payment1.1/payResponse.jsp" %>' >
		
			<!-- 가맹점 아이디 -->
			<input type="hidden" name="mbrId"			value="551110">
			<!-- 가맹점 이름 -->
			<input type="hidden" name="mbrName"			value="CSQUARE">
			<!-- 결제종류 (1:카드, 2:가상계좌, 3:계좌이체) -->
			<input type="hidden" name="payKind"			value="2">
			<!-- 구매자 이름 -->
			<input type="hidden" name="buyerName"		value="조재국">
			<!-- 상품명 -->
			<input type="hidden" name="productName"		value="전자 패드">
			<!-- 상품가격 -->
			<input type="hidden" name="productPrice"	value="1000">
			<!-- 상품수량 -->
			<input type="hidden" name="productCount"	value="1">
			<!-- 결제금액 -->
			<input type="hidden" name="salesPrice"		value="1000">
			<!-- 주문번호 -->
			<input type="hidden" name="oid"				value="">
			<!-- 입장(오픈)일 -->
			<input type="hidden" name="openDate"		value="160311">
			<!-- 사업자번호 -->
			<input type="hidden" name="bizNo"			value="1388302738">
			  
			<input type="hidden" name="returnUrl"		value="<%=  request.getScheme() + "://" +  request.getServerName() + ":" +  request.getServerPort() + request.getContextPath() + "/entrance/reserve/callback" %>">
			<input type="hidden" name="hashValue"		value="">
			<input type="hidden" name="version"			value="1.1">
			
			
			<!-- 결제 구분 (payment:인증 & 승인, auth:인증) -->
			<input type="hidden" name="returnType"		value="payment">

			<input type="hidden" name="authType"		value="all">
			
			<input type="hidden" name="targetUrl"		value="http://pgtest.mainpay.co.kr:8080/csStdPayment/">
<!-- 
			<input type="hidden" name="targetUrl" value="https://pg.mainpay.co.kr/csStdPayment/">
			<input type="hidden" name="targetUrl" value="http://pgstq.mainpay.co.kr/csStdPayment/">
			<input type="hidden" name="targetUrl" value="http://localhost:8080/">			
 -->
		</form>
	</div>
<!-- 결제 관련 END -->

</body>
</html>