<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.*"%>

<%

	List<HashMap<String, String>> classList = (List<HashMap<String, String>>)request.getAttribute("AcademyClassList");
	String AcademyCd = (String)request.getAttribute("AcademyCd");
	String ClassCd = (String)request.getAttribute("ClassCd");
%>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-purple">
		<nav id="scrollspy-nav" class="navbar-default navbar-static">
			<div class="container">
				<ul class="nav navbar-nav">
					<li class="dropdown">
						<a class="dropdown-toggle" type="button" data-toggle="dropdown" role="button" 
							aria-haspopup="true" aria-expanded="true">
							<spring:message code="${currentCategory }" text=''/><i class="material-icons">&#xE313;</i>
						</a>
						<ul class="dropdown-menu">
							<c:forEach var="i" begin="1" end="9">
								<c:set var="main_category" value="main.nav.catetory${i }"/>
								<spring:message var="main_category_url" code="main.nav.catetory${i }.url" scope="application"/>
								<c:if test="${not empty main_category && main_category eq currentCategory}">
								 	<c:forEach var="j" begin="1" end="12">
										<spring:message var="sub_category" code="main.nav.catetory${i}.sub${j}" scope="application" text='' />
										<spring:message var="sub_category_url" code="main.nav.catetory${i}.sub${j}.url" scope="application" text='' />
										<c:if test="${not empty sub_category }">
											<li>
												<a href="<c:url value="${sub_category_url }" />" >
													<c:out value="${sub_category }" />
												</a>
											</li>
										</c:if>
									</c:forEach>
								</c:if>
							</c:forEach>
						</ul>
					</li> 
					
					<c:set value="" var="AcademyName" />
					<c:forEach items="${AcamedyList }" var="AcamedyList">
						<c:if test="${AcademyCd eq AcamedyList.ACADEMY_CD }">
							<li class="active">
								<a href="javascript:void(0)" class="item" onclick="AcademyChoice('${AcamedyList.ACADEMY_CD}')">
									${AcamedyList.ACADEMY_NAME}
									<c:set value="${AcamedyList.ACADEMY_NAME}" var="AcademyName" /> 
								</a>				
							</li>
						</c:if>
						<c:if test="${AcademyCd ne AcamedyList.ACADEMY_CD }">
							<li>
								<a href="javascript:void(0)" class="item" onclick="AcademyChoice('${AcamedyList.ACADEMY_CD}')">
									${AcamedyList.ACADEMY_NAME}
								</a>				
							</li>
						</c:if>
						
					</c:forEach>
					<li>
						<a href="#" class="item">
							행사&middot;공연
						</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<div class="bread-crumbs">
							전시관람 / 기초과학관
						</div>
					</li>
				
				</ul>
			</div>
		</nav>
	</div>
	
	<div id="schedule-container" class="sub-body">
		<div id="schedule-banner" class="hidden-xs">
			<h3>천문우주 프로그램 예약 및 검색</h3>
			<p>
				학기별 새로운 과정을 개설하여 어린이들이 즐겁게 과학관의 현장 전시물 앞에서<br>
				생생한 과학의 흥미, 체험, 탐구, 토론 등을 통해 과학을 직접 체험할 수 있는 학교 밖 과학교육프로그램입니다.
			</p>
		</div>
		<div id="schedule-types">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<ul class="link-tabs">
							<%
								for(int i=0; i<classList.size(); i++){
									HashMap<String, String> obj = (HashMap)	classList.get(i);
									String cd = obj.get("CLASS_CD");
							%>
								<li class="tab col-xs-4">
									<a href="javascript:ChangeClass('<%= cd %>');" <% if(cd.equals(ClassCd)){ %> class="active" <% } %> >
										<%= obj.get("CLASS_NAME") %>
									</a>
								</li>
							<%
								}
							%>
							
							<!-- 
							<c:if test="${empty AcademyClassList}">
								<li class="tab col-xs-6">
									<a href="#" class="active">
										개인 ${AcademyName }
									</a>
								</li>
								<li class="tab col-xs-6">
									<a href="#">
										단체 ${AcademyName }
									</a>
								</li>
							</c:if>
							 -->
						</ul>
					</div>
				</div>
			</div>
		</div>
		
		<div id="schedule-search-form">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h4 class="search-title">
							프로그램 대상 <small>(원하시는 항목을 선택하여 예약하세요.)</small>
						</h4>
					</div>
					
					<div class="col-md-2 col-xs-4">
						<div class="checkbox checkbox-purple">
							<input name="target-checkbox" class="styled"  type="checkbox" id="typeAll" checked/>
	                        <label for="typeAll">전체보기</label>
                        </div>
					</div>
					
					<div class="col-md-2 col-xs-4">
						<div class="checkbox checkbox-purple">
							<input name="target-checkbox" class="styled"  type="checkbox" id="type1"/>
	                        <label for="type1">6~7세</label>
                        </div>
					</div>
					
					<div class="col-md-2 col-xs-4">
						<div class="checkbox checkbox-purple">
							<input name="target-checkbox" class="styled"  type="checkbox" id="type2"/>
	                        <label for="type2">초등학생</label>
                        </div>
					</div>
					<div class="col-md-2 col-xs-4">
						<div class="checkbox checkbox-purple">
							<input name="target-checkbox" class="styled"  type="checkbox" id="type3"/>
	                        <label for="type3">중학생</label>
                       	</div>
					</div>
					<div class="col-md-2 col-xs-4">
						<div class="checkbox checkbox-purple">
							<input name="target-checkbox" class="styled"  type="checkbox" id="type4"/>
	                        <label for="type4">성인</label>
                        </div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<h4 class="search-title has-top-line">
							참가비 구분
						</h4>
					</div>
					
					<div class="col-md-2 col-xs-4">
						<div class="checkbox checkbox-purple">
							<input name="target-price" class="styled"  type="checkbox" id="pricingAll" checked/>
	                        <label for="pricingAll">전체보기</label>
                        </div>
					</div>
					
					<div class="col-lg-1 col-md-2 col-xs-4">
						<div class="checkbox checkbox-purple">
							<input name="target-price" class="styled"  type="checkbox" id="pricing"/>
	                        <label for="pricing">유료</label>
                        </div>
					</div>
					
					<div class="col-lg-1 col-md-2 col-xs-4">
						<div class="checkbox checkbox-purple">
							<input name="target-price" class="styled"  type="checkbox" id="non-pricing"/>
	                        <label for="non-pricing">무료</label>
                        </div>
					</div>
					
					<div class="col-lg-2 col-md-2 right">
						<button class="btn btn-purple right hidden-xs">
							검색하기
						</button>
					</div>
				</div>
			</div>
		</div>
		
		<div id="schedule-search-condition">
			<div class="container">
				<div class="row">
					<div class="col-md-3 has-form">
						<select class="form-control purple">
							<option value="12">12개씩 보기</option>
							<option value="24">24개씩 보기</option>
							<option value="36">36개씩 보기</option>
						</select>
					</div>
					
					<div id="multi-forms" class="col-md-6 center has-form">
						<a href="#" class="month-navigator prev-month">
							&#9664;
						</a>
						<select class="year-selector form-control purple">
							<c:forEach var="year" begin="2015" end="2025">
								<option value="${year }">${year }</option>
							</c:forEach>
						</select>
						
						<select class="month-selector form-control purple">
							<c:forEach var="month" begin="1" end="12">
								<option value="${month }">${month }월</option>
							</c:forEach>
						</select>
						
						<a href="#" class="month-navigator next-month">
							&#9654;
						</a>
					</div>
					
					<div class="col-md-3 has-form">
						<input type="text" name="search-key"
							class="search-key form-control purple" placeholder="검색어를 입력하세요">
					</div>
					<div class="col-md-12 has-form visible-xs">
						<button class="btn btn-purple right">
							검색하기
						</button>
					</div>
				</div>
			</div>
		</div>
		
		<div id="schedule-items">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
  							<table class="table centered-table schedule-table responsive-table">
								<thead>
									<tr>
										<th>프로그램명</th>
										<th>대상</th>
										<th>교육비</th>
										<th>정원/접수현황(대기)</th>
										<th>수강신청</th>
										<th>예약</th>
									</tr>
								</thead>
								
								<tbody>
								<%
									if("CL7001".equals(ClassCd)) {
								%>	
									<tr>
										<td><a href="javascript:void(0)">천체투영관</a></td>
										<td class="center">&nbsp;</td>
										<td class="center">&nbsp;</td>
										<td class="center">&nbsp;</td>
										<td class="center">&nbsp;</td>
										<td class="center">
											<!-- <a href="#" class="btn detail-btn">조회</a> -->
											<a href="javascript:fnReserve('10000002');" class="btn reserve-btn">예약</a>
										</td>
									</tr>
								<%
									}else if("CL7002".equals(ClassCd)){
								%>
									<tr>
										<td><a href="javascript:void(0)">스페이스월드</a></td>
										<td class="center">&nbsp;</td>
										<td class="center">&nbsp;</td>
										<td class="center">&nbsp;</td>
										<td class="center">&nbsp;</td>
										<td class="center">
											<!-- <a href="#" class="btn detail-btn">조회</a> -->
											<a href="javascript:fnReserve('10000003');" class="btn reserve-btn">예약</a>
										</td>
									</tr>
								<%
									}
								%>	<!-- 
									<c:choose>
										<c:when test="${not empty lecture.infos }">
											<c:forEach var="l" items="${lecture.infos }">
													<tr>
														<td><a href="javascript:void(0)">${l.courseName } </a></td>
														<td class="center">수강생 학부모 및 일반 성인</td>
														<td class="center">무료</td>
														<td class="center">${l.courseCapacity }/${l.reserCnt }(${l.waitCnt })명</td>
														<td class="center">
															<c:set value="" var="Type" />
															<c:if test="${l.courseCapacity-l.reserCnt > 0}">
																<span class="label processing"> 접수중 </span>
																<c:set value="RESER" var="Type" />
															</c:if> 
															<c:if test="${l.courseCapacity-l.reserCnt <= 0}">
																<c:if test="${(l.waitCapacity - l.waitCnt) > 0}">
																	<span class="label processing"> 대기자 </span>
																	<c:set value="WAIT" var="Type" />
																</c:if>
	
																<c:if test="${(l.waitCapacity - l.waitCnt) <= 0}">
																	<span class="label processing"> 마감 </span>
																</c:if>
															</c:if>
														</td>
														<td class="center">
															<a href="<c:url value='/schedules/${l.courseCd }'/>" class="btn detail-btn">조회</a>
															<a href="javascript:void(0)" class="btn reserve-btn" 
																onclick="ShowList('${l.courseCd}','${l.semesterCd}','${Type}','${l.scheduleFlag }')">
																예약
															</a>
														</td>
													</tr>
												</c:forEach>
										</c:when>
										
										<c:otherwise>
											<tr>
												<td colspan="6"><center>해당 목록이 없습니다.</center></td>
											</tr>
										</c:otherwise>
										
									</c:choose>
									 -->
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="col s12 center">
						<div class="pagination-wrapper">
							<ul class="pagination">
								<li>
									<a href="#" class="page-link  has-arrow ${page.className}"
										data-page="#">
										<i class="fa fa-angle-left"></i>
									</a>
								</li>
								<li>
									<a href="#" class="page-link active" data-page="1">
										1
									</a>
								</li>
								<li>
									<a href="#" class="page-link  has-arrow ${page.className}"
										data-page="#">
										<i class="fa fa-angle-right"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<form name="frmIndex">
	<input type="hidden" name="ACADEMY_CD" value="${AcademyCd}" />
	<input type="hidden" name="COURSE_CD" value="" />
	<input type="hidden" name="SEMESTER_CD" value="" />
	<input type="hidden" name="TYPE" value="" />
	<input type="hidden" name="FLAG" value="" />
	<input type="hidden" name="CLASS_CD" value="${ClassCd}" />
</form>
<script type="text/javascript">



function ChangeClass(Cd){
	var frm = document.frmIndex;
	frm.CLASS_CD.value = Cd;
	frm.action = '<c:url value="/schedules"/>?${_csrf.parameterName}=${_csrf.token}';
	frm.method = 'get';
	frm.submit();
}

function fnReserve(pCd) {
	var frm = document.frmIndex;
	frm.ACADEMY_CD.value = "<%= AcademyCd %>";
	frm.CLASS_CD.value = "<%= ClassCd %>";
	frm.PROGRAM_CD.value = pCd;
	frm.action = '<c:url value="/entrance/reserve"/>?${_csrf.parameterName}=${_csrf.token}';
	frm.method = 'post';
	frm.submit();
}

$(document).ready(function() {
    	$("input[name='target-checkbox']").change(function(e){
    		if($(this).attr("id") != "typeAll"){
    			$("#typeAll").prop("checked", false);
    		}else{
    			if($(this).is(":checked")){
    				$("input[name='target-checkbox']").prop("checked", false);
    				$(this).prop("checked", true);
    			}
    		}
    	});
    	
    	$("input[name='target-price']").change(function(e){
    		if($(this).attr("id") != "pricingAll"){
    			$("#pricingAll").prop("checked", false);
    		}else{
    			if($(this).is(":checked")){
    				$("input[name='target-price']").prop("checked", false);
    				$(this).prop("checked", true);
    			}
    		}
    	});
  	});
	
	function AcademyChoice(Cd){
		var frm = document.frmIndex;
		frm.ACADEMY_CD.value = Cd;
// 		if("ACD007" == Cd){
// 			frm.action = '<c:url value="/entrance"/>?${_csrf.parameterName}=${_csrf.token}';
// 		}else{
// 			frm.action = '<c:url value="/schedules"/>?${_csrf.parameterName}=${_csrf.token}';
// 		}
		frm.action = '<c:url value="/schedules"/>?${_csrf.parameterName}=${_csrf.token}';
		frm.method = 'post';
		frm.submit();
	}
	
	function ShowList(CourseCd,SemesterCd,Type,flag){
		var frm = document.frmIndex;
		frm.COURSE_CD.value = CourseCd;
		frm.SEMESTER_CD.value = SemesterCd;
		frm.TYPE.value = Type;
		if(flag == 'true')	frm.FLAG.value = "Y";
		else		frm.FLAG.value = "N";
		
// 		frm.action = '<c:url value="/schedules/Show"/>?${_csrf.parameterName}=${_csrf.token}';
		frm.action = '<c:url value="/schedules/Show"/>';
		frm.method = 'POST';
		frm.submit();
		
	}
</script>
