<%@page import="org.json.JSONObject"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="java.util.*"%>
<%@page import="kr.go.sciencecenter.util.DateUtil"%>
<%@page import="kr.go.sciencecenter.model.api.Member"%>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%
	JSONObject json = (JSONObject)request.getAttribute("result");

	if ("0000".equals(request.getParameter("rstCode"))) {
		// 결제 성공, DB Insert 성공 
		if ("1".equals(request.getParameter("payKind"))) {
			// TODO : 신용카드
	//		 postPaymentCard(request);
		} else if ("2".equals(request.getParameter("payKind"))) {
			// TODO : 가상계좌 (가상계좌는 만들어 졌으나 실제 입금된 것은 아님 
	//		 postPaymentVA(request);
		}  else {
			// 지원하지 않는 결제 타입 
		}
	} else {
		// 결제 실패
		
	}

%>


<script src="<%=request.getContextPath()%>/resources/js/csquare/dateformat.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/csquare/util.js"></script>

<script>
	var param = <%=json.toString()%>;
	
	try{ 
	    window.opener.funcPayResult(param);  
	    self.close(); 
	}catch(e){
	} 
</script>