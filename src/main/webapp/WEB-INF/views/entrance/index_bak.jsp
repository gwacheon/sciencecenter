<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

	    

	
		<div id="sub-content-wrapper">
			<div class="sub-content-nav scrollspy-purple">
				<nav id="scrollspy-nav" class="navbar-default navbar-static">
					<div class="container">
						<ul class="nav navbar-nav">
							<li class="dropdown">
								<a class="dropdown-toggle" type="button" data-toggle="dropdown" role="button" 
									aria-haspopup="true" aria-expanded="true">
									<spring:message code="${currentCategory }" text=''/><i class="material-icons">&#xE313;</i>
								</a>
								<ul class="dropdown-menu">
									<c:forEach var="i" begin="1" end="9">
										<c:set var="main_category" value="main.nav.catetory${i }"/>
										<spring:message var="main_category_url" code="main.nav.catetory${i }.url" scope="application"/>
										<c:if test="${not empty main_category && main_category eq currentCategory}">
										 	<c:forEach var="j" begin="1" end="12">
												<spring:message var="sub_category" code="main.nav.catetory${i}.sub${j}" scope="application" text='' />
												<spring:message var="sub_category_url" code="main.nav.catetory${i}.sub${j}.url" scope="application" text='' />
												<c:if test="${not empty sub_category }">
													<li>
														<a href="<c:url value="${sub_category_url }" />" >
															<c:out value="${sub_category }" />
														</a>
													</li>
												</c:if>
											</c:forEach>
										</c:if>
									</c:forEach>
								</ul>
							</li> 
							
							<c:set value="" var="AcademyName" />
							<c:forEach items="${AcamedyList }" var="AcamedyList">
								<c:if test="${AcademyCd eq AcamedyList.ACADEMY_CD }">
									<li class="active">
										<a href="javascript:void(0)" class="item" onclick="AcademyChoice('${AcamedyList.ACADEMY_CD}')">
											${AcamedyList.ACADEMY_NAME}
											<c:set value="${AcamedyList.ACADEMY_NAME}" var="AcademyName" /> 
										</a>				
									</li>
								</c:if>
								<c:if test="${AcademyCd ne AcamedyList.ACADEMY_CD }">
									<li>
										<a href="javascript:void(0)" class="item" onclick="AcademyChoice('${AcamedyList.ACADEMY_CD}')">
											${AcamedyList.ACADEMY_NAME}
										</a>				
									</li>
								</c:if>
								
							</c:forEach>
							<li>
								<a href="#" class="item">
									행사&middot;공연
								</a>
							</li>
						</ul>
						
						<ul class="nav navbar-nav navbar-right">
							<li>
								<div class="bread-crumbs">
									전시관람 / 기초과학관
								</div>
							</li>

						</ul>
					</div>
				</nav>
			</div>

			<div id="schedule-container" class="sub-body">
				<div id="schedule-banner" class="hidden-xs">
					<h3>창의체험 프로그램 예약 및 검색</h3>
					<p>
						학기별 새로운 과정을 개설하여 어린이들이 즐겁게 과학관의 현장 전시물 앞에서<br>
						생생한 과학의 흥미, 체험, 탐구, 토론 등을 통해 과학을 직접 체험할 수 있는 학교 밖 과학교육프로그램입니다.
					</p>
					<div id="schedule-types">
						<div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<ul class="link-tabs">
										<li class="tab col-xs-6">
											<a href="#" class="active">
												개인 창의 체험
											</a>
										</li>
										<li class="tab col-xs-6">
											<a href="#">
												단체 창의 체험
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div id="schedule-search-form">
					<div class="container">
						<div class="row hidden-xs">
							<div class="col-md-12">
								<h4 class="search-title">
									프로그램 대상 <small>(원하는 항목을 선택하여 예약하세요.)</small>
								</h4>
							</div>

							<div class="col-md-2 col-xs-4">
								<div class="checkbox checkbox-purple">
									<input name="target-checkbox" class="styled"  type="checkbox" id="typeAll" checked/>
									<label for="typeAll">전체보기111</label>
								</div>
							</div>

							<div class="col-md-2 col-xs-4">
								<div class="checkbox checkbox-purple">
									<input name="target-checkbox" class="styled"  type="checkbox" id="type1"/>
									<label for="type1">6~7세</label>
								</div>
							</div>

							<div class="col-md-2 col-xs-4">
								<div class="checkbox checkbox-purple">
									<input name="target-checkbox" class="styled"  type="checkbox" id="type2"/>
									<label for="type2">초등학생</label>
								</div>
							</div>
							<div class="col-md-2 col-xs-4">
								<div class="checkbox checkbox-purple">
									<input name="target-checkbox" class="styled"  type="checkbox" id="type3"/>
									<label for="type3">중학생</label>
								</div>
							</div>
							<div class="col-md-2 col-xs-4">
								<div class="checkbox checkbox-purple">
									<input name="target-checkbox" class="styled"  type="checkbox" id="type4"/>
									<label for="type4">성인</label>
								</div>
							</div>
						</div>

						<div class="row hidden-xs">
							<div class="col-md-12">
								<h4 class="search-title has-top-line">
									참가비 구분
								</h4>
							</div>

							<div class="col-md-2 col-xs-4">
								<div class="checkbox checkbox-purple">
									<input name="target-price" class="styled"  type="checkbox" id="pricingAll" checked/>
									<label for="pricingAll">전체보기</label>
								</div>
							</div>

							<div class="col-lg-1 col-md-2 col-xs-4">
								<div class="checkbox checkbox-purple">
									<input name="target-price" class="styled"  type="checkbox" id="pricing"/>
									<label for="pricing">유료</label>
								</div>
							</div>

							<div class="col-lg-1 col-md-2 col-xs-4">
								<div class="checkbox checkbox-purple">
									<input name="target-price" class="styled"  type="checkbox" id="non-pricing"/>
									<label for="non-pricing">유료</label>
								</div>
							</div>

							<div class="col-lg-2 col-md-2 right">
								<button class="btn btn-purple right hidden-xs">
									검색하기
								</button>
							</div>
						</div>
					</div>
				</div>

				<div id="schedule-search-condition" class="hidden-xs">
					<div class="container">
						<div class="row">
							<div class="col-md-3 has-form hidden-xs">
								<select class="form-control purple">
									<option value="12">12개씩 보기</option>
									<option value="24">24개씩 보기</option>
									<option value="36">36개씩 보기</option>
								</select>
							</div>

							<div id="multi-forms" class="col-md-6 center has-form">
								<a href="#" class="month-navigator prev-month  hidden-xs">
									&#9664;
								</a>
								<select class="year-selector form-control purple">

									<option value="2015">2015</option>

									<option value="2016">2016</option>

									<option value="2017">2017</option>

									<option value="2018">2018</option>

									<option value="2019">2019</option>

									<option value="2020">2020</option>

									<option value="2021">2021</option>

									<option value="2022">2022</option>

									<option value="2023">2023</option>

									<option value="2024">2024</option>

									<option value="2025">2025</option>

								</select>

								<select class="month-selector form-control purple">

									<option value="1">1월</option>

									<option value="2">2월</option>

									<option value="3">3월</option>

									<option value="4">4월</option>

									<option value="5">5월</option>

									<option value="6">6월</option>

									<option value="7">7월</option>

									<option value="8">8월</option>

									<option value="9">9월</option>

									<option value="10">10월</option>

									<option value="11">11월</option>

									<option value="12">12월</option>

								</select>

								<a href="#" class="month-navigator next-month hidden-xs">
									&#9654;
								</a>
							</div>

							<div class="col-md-3 has-form">
								<input type="text" name="search-key"
								class="search-key form-control purple" placeholder="검색어를 입력하세요">
							</div>
							<div class="col-md-12 has-form visible-xs">
								<button class="btn btn-purple right">
									검색하기
								</button>
							</div>
						</div>
					</div>
				</div>

				<div id="schedule-items">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive table-purple">
									<table class="table centered-table schedule-table responsive-table">
										<thead>
											<tr>
												<th>프로그램</th>
												<th class="hidden-xs">수강대상</th>
												<th class="hidden-xs">참가비</th>
												<th class="hidden-xs">잔여/정원/대기</th>
												<th>수강신청</th>
												<th>조회/예약</th>
											</tr>
										</thead>

										<tbody>
											<tr>
												<td>
													<a href="#">
														엄마의 과학관 산책
													</a>
												</td>
												<td class="center hidden-xs">수강생 학부모 및 일반 성인</td>
												<td class="center hidden-xs">무료</td>
												<td class="center hidden-xs">20명</td>
												<td class="center">
													<span class="label processing">
														접수중
													</span>
												</td>
												<td class="center">
													<a href="#" class="btn detail-btn">
														조회
													</a>
													<a href="/sciencecenter/schedules/1" class="btn reserve-btn">
														예약
													</a>
												</td>
											</tr>
											<tr>
												<td>
													<a href="#">
														영리한 재난대처! 과학에게 배웠어요~1기
													</a>
												</td>
												<td class="center hidden-xs">초1~3</td>
												<td class="center hidden-xs">6,000원</td>
												<td class="center hidden-xs">13명</td>
												<td class="center">
													<span class="label waiting">
														대기접수
													</span>
												</td>
												<td class="center">
													<a href="#" class="btn detail-btn">
														조회
													</a>
													<a href="/sciencecenter/schedules/1" class="btn reserve-btn">
														예약
													</a>
												</td>
											</tr>
											<tr>
												<td>영리한 재난대처! 과학에게 배웠어요~2기</td>
												<td class="center hidden-xs">초1~3</td>
												<td class="center hidden-xs">6,000원</td>
												<td class="center hidden-xs">13명</td>
												<td class="center">
													<span class="label ended">
														정원마감
													</span>
												</td>
												<td class="center">
													<a href="#" class="btn detail-btn">
														조회
													</a>
													<a href="/sciencecenter/schedules/1" class="btn reserve-btn">
														예약
													</a>
												</td>
											</tr>
										</tbody>
									</table>

									<div class="pagination-wrapper">
										<ul class="pagination">
											<li>
												<a href="/sciencecenter/support/notice?page=1" data-page="1" class="page-link {{page.className}}">
													<i class="fa fa-chevron-left"></i>
												</a>
											</li>
											<li>
												<a href="/sciencecenter/support/notice?page=4" data-page="4" class="page-link"><i class="fa fa-ellipsis-h"></i></a>
											</li>
											<li>
												<a href="/sciencecenter/support/notice?page=5" class="page-link">5</a>
											</li>
											<li>
												<a href="/sciencecenter/support/notice?page=6" class="page-link">6</a>
											</li>
											<li>
												<a href="/sciencecenter/support/notice?page=7" class="page-link">7</a>
											</li>
											<li class="active">
												<a href="#" class="page-link">8</a>
											</li>
											<li>
												<a href="/sciencecenter/support/notice?page=9" class="page-link">9</a>
											</li>
											<li>
												<a href="/sciencecenter/support/notice?page=10" class="page-link">10</a>
											</li>
											<li>
												<a href="/sciencecenter/support/notice?page=11" class="page-link">11</a>
											</li>
											<li>
												<a href="/sciencecenter/support/notice?page=12" data-page="12" class="page-link"><i class="fa fa-ellipsis-h"></i></a>
											</li>
											<li>
												<a href="/sciencecenter/support/notice?page=143" data-page="143" class="page-link">
													<i class="fa fa-chevron-right"></i>
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
	
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$("input[name='target-checkbox']").change(function(e){
			if($(this).attr("id") != "typeAll"){
				$("#typeAll").prop("checked", false);
			}else{
				if($(this).is(":checked")){
					$("input[name='target-checkbox']").prop("checked", false);
					$(this).prop("checked", true);
				}
			}
		});

		$("input[name='target-price']").change(function(e){
			if($(this).attr("id") != "pricingAll"){
				$("#pricingAll").prop("checked", false);
			}else{
				if($(this).is(":checked")){
					$("input[name='target-price']").prop("checked", false);
					$(this).prop("checked", true);
				}
			}
		});
	});
</script>
</div>







<div id="responsibility-wrapper">

</div>



<div id="quick-link" class="hidden-xs">
	<div class="container">
		<div class="row">
			<div class="col-sm-1">
				<img src="img/main_a/familysite/arrow_grey01.png" class="arrow left"/>
			</div>
			<div class="col-sm-10" style="padding: 0;"> 
				<img src="img/main_a/familysite/familysite_01.png" />
				<img src="img/main_a/familysite/familysite_02.png" />
				<img src="img/main_a/familysite/familysite_03.png" />
				<img src="img/main_a/familysite/familysite_04.png" />
				<img src="img/main_a/familysite/familysite_05.png" />
				<img src="img/main_a/familysite/familysite_06.png" />
			</div>
			<div class="col-sm-1">
				<img src="img/main_a/familysite/arrow_grey02.png" class="arrow right"/>
			</div>
		</div>
	</div>
</div>
<div id="footer">
			<div class="footer-bottom">
				<div class="container">
					<div class="row">
						<div class="col-md-2 hidden-xs">
							<img src="img/main_a/footer-logo.png" class="img-responsive footer-logo-img margin-top20">
						</div>
						<div class="col-md-10">
							<div class="row footer-links">
								<div class="col-md-8">
									<a href="/gnsm_web/?sub_num=307">
										이용약관
									</a>
									<a href="/gnsm_web/?sub_num=305">
										개인정보 취급방침
									</a>
									<a href="/gnsm_web/?sub_num=602">
										이메일 무단수신거부
									</a>
									<a href="/gnsm_web/popup/email/email.jsp" target="_blank">
											운영자에게
									</a>
								</div>

								<div class="col-md-4 item">
									<div class="dropup pull-right">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">
											관련 사이트 이동
											<i class="fa fa-angle-up"></i>
										</a>
										<ul class="dropdown-menu">
											<li>
												<a href="http://www.president.go.kr" target="_blank">청와대</a>
											</li>
											<li>
												<a href="http://www.msip.go.kr" target="_blank">미래창조과학부</a>
											</li>
											<li>
												<a href="http://www.science.go.kr" target="_blank">국립중앙과학관</a>
											</li>
											<li>
												<a href="http://www.ssm.go.kr/v2/kor/index.asp" target="_blank">국립서울과학관</a>
											</li>
											<li>
												<a href="http://www.nssc.go.kr" target="_blank">원자력안전위원회</a>
											</li>
											<li>
												<a href="http://www.copyright.or.kr" target="_blank">한국저작권위원회</a>
											</li>
											<li>
												<a href="http://www.ibs.re.kr" target="_blank">기초과학연구원</a>
											</li>
											<li>
												<a href="http://www.isbb.or.kr" target="_blank">국제과학비지니스벨트</a>
											</li>
											<li>
												<a href="http://www.ntis.go.kr" target="_blank">국가과학기술지식정보서비스</a>
											</li>
											<li>
												<a href="http://www.now.go.kr" target="_blank">과학기술정책정보서비스</a>
											</li>
											<li>
												<a href="http://www.korea.kr/newsWeb/index.jsp" target="_blank">공감코리아</a>
											</li>
											<li>
												<a href="http://www.ntis.go.kr" target="_blank">국가과학기술지식정보서비스</a>
											</li>
										</ul>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="address">
										<!-- 13817 경기도 과천시 상하별로 110 국립과천과학관<br>
										<strong style="margin-right: 10px">대표전화</strong> 02-2677-1500 <strong style="margin-right: 10px;margin-left: 30px">Fax</strong> 02-3677-1328 -->
										<strong style="margin-right: 10px">관람시간</strong>  오전 9:30 ~ 오후 5:30 (<b>입장마감</b> 오후 4:30)  <b>휴관일</b> 1월 1일, 매주 월요일<br> 
										<strong style="margin-right: 10px">주소</strong>  13817 경기도 과천시 상하벌로 110 국립과천과학관 <br>
										<strong style="margin-right: 10px">대표전화</strong>  02-3677-1500   <strong style="margin-right: 10px; margin-left: 10px;">Fax</strong>  02-3677-1328
									</div>
								</div>

								<div class="col-md-6">
									<div class="row">
										<div class="col-md-4">
											<div class="footer-link-images">
												<img src="img/main/footer-link-image1.png" alt="" class="visible-lg">
												<div class="footer-link-texts">
													<a href="http://www.sstm.org.cn/kjg_web/html/kjg_korea/portal/index/index.htm" target="_blank">
														<div class="sub-title">상해과학 기술관</div>
														<div class="sub-text">해외과학 기술관</div>
													</a>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="footer-link-images">
												<img src="img/main/footer-link-image2.png" alt="" class="visible-lg">
												<div class="footer-link-texts">
													<a href="http://60.247.10.155:8007/portal/findportal.do?portalid=orga2e3e19908bfa" target="_blank">
														<div class="sub-title">중국과학 기술관</div>
														<div class="sub-text">해외협력 과학관</div>
													</a>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="footer-link-images">
												<img src="img/main/footer-link-image3.png" alt="" class="visible-lg">
												<div class="footer-link-texts">
													<a href="http://www.miraikan.jst.go.jp/ko/" target="_blank">
														<div class="sub-title">일본과학 미래관</div>
														<div class="sub-text">해외협력 과학관</div>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="mobile-search-btn">
			<button id="mobile-search-modal-btn" class="btn btn-purple">
				<i class="fa fa-search"></i>
			</button>
		</div>

<div id="ajaxLoader">
	<img src="img/ajax-loader.gif" alt="ajax-loader"/>
</div>

<div id="mobile-search-modal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">프로그램 검색</h4>
      </div>
      <div class="modal-body">
        <div class="row visible-xs">
					<div class="col-md-12">
						<h4 class="search-title">
							프로그램 대상 <small>(원하는 항목을 선택하여 예약하세요.)</small>
						</h4>
					</div>

					<div class="col-md-2 col-xs-4">
						<div class="checkbox checkbox-purple">
							<input name="target-checkbox" class="styled" type="checkbox" id="typeAll" checked="">
							<label for="typeAll">전체보기</label>
						</div>
					</div>

					<div class="col-md-2 col-xs-4">
						<div class="checkbox checkbox-purple">
							<input name="target-checkbox" class="styled" type="checkbox" id="type1">
							<label for="type1">6~7세</label>
						</div>
					</div>

					<div class="col-md-2 col-xs-4">
						<div class="checkbox checkbox-purple">
							<input name="target-checkbox" class="styled" type="checkbox" id="type2">
							<label for="type2">초등학생</label>
						</div>
					</div>
					<div class="col-md-2 col-xs-4">
						<div class="checkbox checkbox-purple">
							<input name="target-checkbox" class="styled" type="checkbox" id="type3">
							<label for="type3">중학생</label>
						</div>
					</div>
					<div class="col-md-2 col-xs-4">
						<div class="checkbox checkbox-purple">
							<input name="target-checkbox" class="styled" type="checkbox" id="type4">
							<label for="type4">성인</label>
						</div>
					</div>
				</div>

				<div class="row visible-xs">
					<div class="col-md-12">
						<h4 class="search-title has-top-line">
							참가비 구분
						</h4>
					</div>

					<div class="col-md-2 col-xs-4">
						<div class="checkbox checkbox-purple">
							<input name="target-price" class="styled" type="checkbox" id="pricingAll" checked="">
							<label for="pricingAll">전체보기</label>
						</div>
					</div>

					<div class="col-lg-1 col-md-2 col-xs-4">
						<div class="checkbox checkbox-purple">
							<input name="target-price" class="styled" type="checkbox" id="pricing">
							<label for="pricing">유료</label>
						</div>
					</div>

					<div class="col-lg-1 col-md-2 col-xs-4">
						<div class="checkbox checkbox-purple">
							<input name="target-price" class="styled" type="checkbox" id="non-pricing">
							<label for="non-pricing">유료</label>
						</div>
					</div>

					<div class="col-lg-2 col-md-2 right">
						<button class="btn btn-purple right hidden-xs">
							검색하기
						</button>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<h4 class="search-title has-top-line">
							날짜 및 프로그램명
						</h4>
					</div>
					<div id="multi-forms" class="col-xs-6 has-form">
						<select class="year-selector form-control purple">

							<option value="2015">2015</option>

							<option value="2016">2016</option>

							<option value="2017">2017</option>

							<option value="2018">2018</option>

							<option value="2019">2019</option>

							<option value="2020">2020</option>

							<option value="2021">2021</option>

							<option value="2022">2022</option>

							<option value="2023">2023</option>

							<option value="2024">2024</option>

							<option value="2025">2025</option>

						</select>

						<select class="month-selector form-control purple">

							<option value="1">1월</option>

							<option value="2">2월</option>

							<option value="3">3월</option>

							<option value="4">4월</option>

							<option value="5">5월</option>

							<option value="6">6월</option>

							<option value="7">7월</option>

							<option value="8">8월</option>

							<option value="9">9월</option>

							<option value="10">10월</option>

							<option value="11">11월</option>

							<option value="12">12월</option>

						</select>
					</div>

					<div class="col-xs-6 has-form">
						<input type="text" name="search-key" class="search-key form-control purple" placeholder="검색어를 입력하세요">
					</div>
				</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-purple reverse" data-dismiss="modal">취소</button>
        <button type="button" class="btn btn-purple">검색</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	function ajaxLoading(){
		$("body").css("opacity", "0.5");
		$("#ajaxLoader").show();
	}

	function ajaxUnLoading(){
		$("body").css("opacity", "1");
		$("#ajaxLoader").hide();
	}

	$("#mobile-search-modal-btn").click(function(e){
		$("#mobile-search-modal").modal();
	});
</script>

<script type="text/javascript">
	$(function(){
		$('body').scrollspy({ 
			target: '#scrollspy-nav',
			offset: 230
		});

		$("#scrollspy-nav.has-scroll a.item").click(function(e){
			e.preventDefault();

			var target = this.hash;
			var $target = $(target);

			$('html, body').stop().animate({
				'scrollTop': $target.offset().top - 150
			}, 500, 'swing', function () {
				window.location.hash = target;
			});
		});


		$('.dropdown-button').dropdown({
			inDuration: 300,
			outDuration: 225,
					    constrain_width: false, // Does not change width of dropdown to that of the activator
					    gutter: 0, // Spacing from edge
					    belowOrigin: false // Displays dropdown below the button
					  }
					  );
	});
</script>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-66853433-1', 'auto');
	ga('send', 'pageview');
</script>
</body>
</html>