<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ page import="java.net.*" %>
<%@ page import="org.json.*" %>

<%
// 모바일 결제 완료 화면

// Log 출력
/*
Enumeration<String> em = request.getParameterNames();
while (em.hasMoreElements()) {
	String key = em.nextElement();
	String val = (String)request.getParameter(key);			
	out.println(key + " :: " + URLDecoder.decode(val) + "<br>");
}
*/	

// Data Sample
/*
	가상계좌 결제
	http://118.67.173.151:8080/sciencecenter/entrance/reserve/complete?mbrId=551110&resCode=0000&resMsg=success&rstCode=0000&rstMsg=success&salesPrice=1000&payKind=2&oid=201605271100620010000013&vAccountTradeNo=0527VA867338&vAccount=26510255618560&vCriticalDate=2016%EB%85%8405%EC%9B%9428%EC%9D%BC&vAccountBankName=%EC%9A%B0%EB%A6%AC%EC%9D%80%ED%96%89&vAccountBankCode=20&payType=vAccount&hashValue=20160527155634d65dcdd0ff1348a7fbefc4b7c646e572f9c8052b27327847af9ad1c4c8516596&vAccountApprovTime=155730
	------------------------------------------------
	mbrId				: 551110
	resCode				: 0000
	resMsg				: success
	rstCode				: 0000
	rstMsg				: success
	salesPrice			: 1000
	payKind 			: 2
	oid 				: 201605271100620010000013
	vAccountTradeNo 	: 0527VA867338
	vAccount 			: 26510255618560 
	vCriticalDate 		: 2016년05월28일
	vAccountBankName	: 우리은행
	vAccountBankCode	: 20
	payType				: vAccount
	hashValue			: 20160527155634d65dcdd0ff1348a7fbefc4b7c646e572f9c8052b27327847af9ad1c4c8516596
	vAccountApprovTime	: 155730
	

	
	신용카드 결제
	http://118.67.173.151:8080/sciencecenter/entrance/reserve/complete?mbrId=551110&resCode=0000&resMsg=success&rstCode=0000&rstMsg=success&salesPrice=1000&payKind=1&oid=201605281100620010000001&payType=3D&authType=auth&cardTradeNo=CS326160528102215911&cardApprovDate=160528&cardApprovTime=102208&cardName=현대체크카 &cardCode=10&cardApprovNo=00346430&hashValue=20160528102108e355bb2a1c8acccfea968d648877c38018f920cb8ece33a6f0aec8ffc6b0eb92
	------------------------------------------------
	mbrId				: 551110
	resCode				: 0000
	resMsg				: success
	rstCode				: 0000
	rstMsg				: success
	salesPrice			: 1000
	payKind				: 1
	oid					: 201605281100620010000001
	payType				: 3D
	authType			: auth
	cardTradeNo			: CS326160528102215911
	cardApprovDate		: 160528
	cardApprovTime		: 102208
	cardName			: 현대체크카 
	cardCode			: 10
	cardApprovNo		: 00346430
	hashValue			: 20160528102108e355bb2a1c8acccfea968d648877c38018f920cb8ece33a6f0aec8ffc6b0eb92
	
	
*/

JSONObject jsonReserve = (JSONObject)request.getAttribute("reserve_info");;

if ("0000".equals((String)request.getParameter("resCode"))) {
%>
	<script src="<%=request.getContextPath()%>/resources/js/csquare/util.js"></script>

	<script type="text/javascript">
		var user_info = null;
		var reserve_info = <%=jsonReserve.toString()%>;

		$(document).ajaxStart(function() {
			ajaxLoading();
		});
		$(document).ajaxStop(function() {
			ajaxUnLoading();
		})
	
		var context_path = '<%=request.getContextPath()%>';
		
		$(document).ready(function() {
			// 회원 정보 조회
			var url = context_path + '/entrance/api/current/user';
			$.ajax({
				method: "GET",
				url: url,
				data: {}
			}).done(function(data) {
				if ((typeof data) == 'string') 
					data = JSON.parse(data);
				
				console.log("Data : " + JSON.stringify(data));
		
				if (!data.hasOwnProperty('error'))
					user_info = data.user_info;
			}).fail(function(jqXHR, textStatus) {
			});
			
			// 결제 정보
			var payKind = <%= request.getParameter("payKind") %>;
			var resCode = <%= request.getParameter("resCode") %>;
			var payResultMsg = '';
			
			if (resCode == 0) {
//		    	alert('TOT_CNT : ' + reserve_info.TOT_CNT);
//		    	alert('TOT_AMT : ' + reserve_info.TOT_AMT);
//		    	alert('TOT_CNT : ' + numberWithCommas(reserve_info.TOT_CNT));
//		    	alert('TOT_AMT : ' + numberWithCommas(reserve_info.TOT_AMT));

				// 결제 성공
				switch (payKind) {
					case 1 :						// 신용카드
						// 신용카드 (하나카드 1234-4567-****-****) 결제완료
						payResultMsg = '신용카드 (<%=request.getParameter("cardName")%>) 결제완료';
						break;
					case 2 : 						// 가상계좌
						// 가상계좌 (우리은행 0000000000000) 입금대기
//						payResultMsg = '가상계좌 입금 대기 (' + '<%=request.getParameter("vAccountBankName")%>' + ' ' + '<%= request.getParameter("vAccount") %>' + ', 입금 기한 : ' + '<%= request.getParameter("vCriticalDate")%>' +')';
						payResultMsg = '가상계좌 입금 대기 (<%=request.getParameter("vAccountBankName")%> <%=request.getParameter("vAccount")%>, 입금 기한 : <%=request.getParameter("vCriticalDate")%>)';
						break;
				}
				$('#payment').text(payResultMsg);
				$('.place_nm').text(reserve_info.PROGRAM_NM);
				$('#play_date').text(
					reserve_info.PLAY_DATE.substring(0,4) + '년 ' + 
					reserve_info.PLAY_DATE.substring(4,6) + '월 ' +
					reserve_info.PLAY_DATE.substring(6,8) + '일 ' +
					reserve_info.WEEK_NM + '요일'
				);
				$('#play_seq_nm').text(reserve_info.PLAY_SEQ_NM);
				
				
				
				$('#rsv_nbr').text(reserve_info.RSV_NBR);
				$('#tran_date_time').text(
					reserve_info.TRAN_DATE.substring(0,4) + '년 ' +
					reserve_info.TRAN_DATE.substring(4,6) + '월 ' +
					reserve_info.TRAN_DATE.substring(6,8) + '일 ' + 
					reserve_info.TRAN_TIME.substring(0,2) + '시 ' +
					reserve_info.TRAN_TIME.substring(2,4) + '분'
				);
				
		    	$('#member_id').text(reserve_info.MEMBER_NAME);
		    	$('#member_cel').text(reserve_info.MEMBER_TEL1);
		    	
		    	$('#tot_cnt').text(numberWithCommas(reserve_info.TOT_CNT));
		    	$('#tot_amt').text(numberWithCommas(reserve_info.TOT_AMT));
		    	
				alert("예약이 접수되었습니다. 감사합니다.");
			} else {
				// 결제 실패
				
			}
			
			
			$('#btnReserveList').on('click', function() {
				location.replace('<%=request.getContextPath()%>/entrance/payment');
			});

			function btnReserveList_click() {
//				alert('click');
				var context_path = '<%=request.getContextPath()%>';
				var url = context_path + '/entrance/payment';
//				alert(url);
				location.replace(url);
			}
		});
		
		
		function numberWithCommas(x){return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");}

		function string2Date(strDate) {
			var year	= strDate.substr(0,4);
			var month	= strDate.substr(4,2);
			var day		= strDate.substr(6,2);
			var date	= new Date(year, month, day);  // date로 변경
			
			return date;
		}

	</script>
	
	<div class="container margin-top20">
		<div class="row">
			<div class="col-md-12">
				<a href="#" class="btn btn-teal visible-xs" id="btnReserveList">
					예약내역 바로가기
				</a>
			</div>
		</div>
 		
		<div class="row">
			<div class="col-md-12">
				<div class="reservation-receipe-wrapper">
					<div class="receipe-header">
						<h5 class="doc-title">예약증</h5>
					</div>
	
					<div class="receipe-body margin-bottom20">
						<h2 class="doc-title"><span class="place_nm">-</span></h2>
						<h5 class="doc-title"><span id="play_date">-</span></h5>
						<h5 class="doc-title"><span id="play_seq_nm">-</span></h5>
						<h5 class="doc-title">국립과천과학관 <span class="place_nm">-</span></h5>
					</div>
	
					<div class="receipe-info">
						<div class="table-responsive">
						  <table class="table lined-table">
						    <tr>
						    	<th>예약구분</th>
						    	<td>상영관 예약</td>
						    	<th>예약코드</th>
						    	<td><span id="rsv_nbr">예약번호</span></td>
						    </tr>
						    <tr>
						    	<th>예약일시</th>
						    	<td id="tran_date_time">예약일시</td>
						    	<th>예약상태</th>
						    	<td>예약완료</td>
						    </tr>
						    <tr>
						    	<th>예약자명</th>
						    	<td><div id="member_id">예약자명</div></td>
						    	<th>연락처</th>
						    	<td><div id="member_cel">연락처</div></td>
						    </tr>
						    <tr>
						    	<th>관람인원</th>
						    	<td><span id="tot_cnt">0</span>명</td>
						    	<th>결제금액</th>
						    	<td><span id="tot_amt">0</span>원</td>
						    </tr>
						    <tr>
						    	<th>결제방식</th>
						    	<td colspan="3"><span class="teal" id="payment">신용카드 (하나카드 1234-4567-****-****) 결제완료</span></td>
						    </tr>
						  </table>
						</div>
					</div>
				</div>
			</div>
	
			<div class="col-md-12 center margin-bottom20">
				<a href="#" id="btnReservePrint" class="btn btn-teal reverse hidden-xs margin-right20">
					예약증 인쇄하기
				</a>
				<a href="#" id="btnReserveList" class="btn btn-teal hidden-xs">
					예약내역 바로가기
				</a>
			</div>
		</div>
	</div>
<%
} else {
%>
	<<script>
		$(document).ready(function() {
			alert("결제에 실패했습니다.");
			location.replace('<%=request.getContextPath()%>');  
		});
	</script>

<%	
}
%>