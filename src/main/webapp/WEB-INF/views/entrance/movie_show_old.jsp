<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="java.util.*"%>
<%@page import="kr.go.sciencecenter.util.DateUtil"%>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%
List<HashMap<String, String>> pList = (List<HashMap<String, String>>)request.getAttribute("ProgramList");
%>
<!-- 예약 완료 화면 -->
<div id="complete-container" class="container margin-top20"
	style="display: none">
	<div class="row">
		<div class="col-md-12">
			<a href="#" class="btn btn-teal visible-xs"> 예약내역 바로가기 </a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="reservation-receipe-wrapper">
				<div class="receipe-header">
					<h5 class="doc-title">예약증</h5>
				</div>

				<div class="receipe-body margin-bottom20">
					<h2 class="doc-title" id="">천체투영관</h2>
					<h5 class="doc-title" id="">2015년 10월 08일 금요일</h5>
					<h5 class="doc-title" id="">2회차 12:00~13:00</h5>
					<h5 class="doc-title" id="">국립과천과학관 천체투영관</h5>
				</div>

				<div class="receipe-info">
					<div class="table-responsive">
						<table class="table lined-table">
							<tr>
								<th>예약구분</th>
								<td>상영관 예약</td>
								<th>예약코드</th>
								<td><div id="">R845788555</div></td>
							</tr>
							<tr>
								<th>예약일시</th>
								<td><div id="">2015년 8월 12일 13:34</div></td>
								<th>예약상태</th>
								<td><div id="">예약완료</div></td>
							</tr>
							<tr>
								<th>예약자명</th>
								<td><div id="">성춘향</div></td>
								<th>연락처</th>
								<td><div id="">010-1234-5678</div></td>
							</tr>
							<tr>
								<th>관람인원</th>
								<td><div id="count">5명</div></td>
								<th>결제금액</th>
								<td><div id="price">300,000원</div></td>
							</tr>
							<tr>
								<th>결제방식</th>
								<td colspan="3"><span class="teal">신용카드 (하나카드 1234-4567-****-****) 결제완료</span></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-12 center margin-bottom20">
			<a href="#" class="btn btn-teal reverse hidden-xs margin-right20">예약증 인쇄하기</a> 
			<a href="#" class="btn btn-teal hidden-xs">예약내역 바로가기</a>
		</div>
	</div>
</div>



<!-- 예약 화면 -->
<div id="schedule-container" class="sub-body">
	<div id="schedule-banner" class="hidden-xs">
		<h3>예약하기</h3>
					<p>
						국립과천과학관에서 진행되는 다양한 전시 체험 등을 한 눈에 쉽게 살펴보시고, <br>
						보다 간편하게 예약 가능합니다.
					</p>
					<div id="schedule-types">
						<div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<ul class="link-tabs">
										<li class="tab col-xs-4">
											<a href="#">
												천체투영관
											</a>
										</li>
										<li class="tab col-xs-4">
											<a href="#">
												천체관측소
											</a>
										</li>
										<li class="tab col-xs-4">
											<a href="#" class="active">
												스페이스월드
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="container margin-bottom30">
					<div class="row">
						<div class="col-md-12">
							<h4 class="schedule-title">
								스페이스월드 체험프로그램 예약
							</h4>
						</div>
					</div>

					<div id="step1" class="row reservation-process active">
						<div class="col-md-12">
							<div class="process-wrapper clearfix">
								<div class="process-title">
									01. 프로그램 선택 <span id="selected-program" class="right"><span class="text"></span> <i class="fa fa-chevron-down down-icon"></i><i class="fa fa-chevron-up up-icon"></i></span>
								</div>

								<div class="process-contents program-choice">
									<div class="row">
										<div class="col-md-12">
											<a href="#" class="reservation-warning-btn btn btn-purple right up-down-toggle-btn" data-target="#reservation-warning-wrapper">
												예약 유의사항 <i class="fa fa-chevron-down down-icon"></i><i class="fa fa-chevron-up up-icon"></i>
											</a>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div id="reservation-warning-wrapper" style="display: none;">
												<ul class="square">
													<li><span class="purple margin-right10">예약순서</span><br>①프로그램 선택 → ②날짜/회차 선택 → ③인원 선택 → ④요금 선택 → ⑤결제</li>
													<li>날짜 및 인원 변경 시 선택내역 취소 후 다시 선택해 주시기 바랍니다.</li>
													<li>예약완료 후 예약확인 및 취소 내역을 반드시 확인하여 주시기 바랍니다.</li>
												</ul>
											</div>
										</div>
									</div>

<%
		for (int i = 0; i < pList.size(); i++) {
			String sYY = pList.get(i).get("START_DATE").substring(0, 4);
			String sMM = pList.get(i).get("START_DATE").substring(4, 6);
			String sDD = pList.get(i).get("START_DATE").substring(6, 8);
			String eYY = pList.get(i).get("END_DATE").substring(0, 4);
			String eMM = pList.get(i).get("END_DATE").substring(4, 6);
			String eDD = pList.get(i).get("END_DATE").substring(6, 8);
			
			String sWeekName = DateUtil.getWeekName(pList.get(i).get("START_DATE"));
			String eWeekName = DateUtil.getWeekName(pList.get(i).get("END_DATE"));
%>
									<div class="row movie-program-row" onclick="" data-id="1" data-title="천체투영관">
										<div class="col-sm-7 mobile-text-center" onclick="selectProgram('<%= pList.get(i).get("PROGRAM_CD") %>', '<%= pList.get(i).get("PROGRAM_NAME") %>', '<%= pList.get(i).get("PLACE_CD") %>')">
											<%= pList.get(i).get("PROGRAM_NAME") %>
										</div>
										<div class="col-sm-5 mobile-text-center text-right">
											<%= sYY %>.<%= sMM %>.<%= sDD %> (<%= sWeekName %>) ~ <%= eYY %>.<%= eMM %>.<%= eDD %> (<%= eWeekName %>)
										</div>
									</div>

<%			
		}
%>

<!-- 
									<div class="row movie-program-row" onclick="" data-id="1" data-title="천체투영관">
										<div class="col-sm-7 mobile-text-center">
											천체투영관
										</div>
										<div class="col-sm-5 mobile-text-center text-right">
											2015.01.01 (월) ~ 2015.03.01 (금)
										</div>
									</div>

									<div class="row movie-program-row" onclick="" data-id="1" data-title="천체투영관">
										<div class="col-sm-7 mobile-text-center">
											천체투영관
										</div>
										<div class="col-sm-5 mobile-text-center text-right">
											2015.01.01 (월) ~ 2015.03.01 (금)
										</div>
									</div>
 -->
									
								</div>
							</div>
						</div>
					</div>

					<div id="step2" class="row reservation-process">
						<div class="col-md-12">
							<div class="process-wrapper clearfix">
								<div class="process-title">
									02. 날짜/회차 선택 <span id="selected-time" class="right"><span class="text"></span> <i class="fa fa-chevron-down down-icon"></i><i class="fa fa-chevron-up up-icon"></i></span>
								</div>
								<div class="process-contents" style="display: none;">
									<div class="row">
										<div class="col-md-6">
											<div id="program-calendar-wrapper"></div>
											<div class="reservation-status-infos right">
												<span class="info-item available"></span> 예약가능일
												<span class="info-item waited"></span> 접수예정일
											</div>
										</div>

										<div class="col-md-6">
											<div id="program-times" style="display: none">
												<table class="table centered-table">
													<thead>
														<tr>
															<th class="center">회차</th>
															<th class="center">시간</th>
															<th class="center">잔여/인원</th>
															<th></th>
														</tr>
													</thead>

													<tbody>
														<tr>
															<td class="center">1회차</td>
															<td class="center">10:00~11:00</td>
															<td class="center">30/30</td>
															<td class="center">
																<button type="button" class="btn btn-disabled">
																	마감
																</button>
															</td>
														</tr>
														<tr>
															<td class="center">2회차</td>
															<td class="center">12:00~13:00</td>
															<td class="center">25/30</td>
															<td class="center">
																<button type="button" class="btn btn-primary reserve-btn" data-id="1" data-title="2015년 5월 25일 1회차 10:00">
																	선택
																</button>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

<div id="step3" class="row reservation-process">
	<div class="col-md-12">
		<div class="process-wrapper clearfix">
			<div class="process-title">
				03. 권종/인원선택 <span id="selected-price" class="right"><span class="text"></span> <i class="fa fa-chevron-down down-icon"></i><i class="fa fa-chevron-up up-icon"></i></span>
			</div>
			<div class="process-contents" style="display: none">
				<div class="col-xs-12">
					<table class="table centered-table">
						<thead>
							<tr>
								<th width="20%">권종</th>
								<th width="20%">요금</th>
								<th width="20%">인원 선택</th>
								<th width="20%">금액</th>
								<th width="20%">결제금액</th>
							</tr>
						</thead>
						
						<tbody>
							<tr>
								<td>어른(20세~)</td>
								<td>2,000</td>
								<td>
									<select class="form-control member-price" data-price="2000"
										data-target="#adult-price">
										<option value="0">인원</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
									</select>
								</td>
								<td id="adult-price">원</td>
								<td rowspan="4" id="total-price"></td>
							</tr>
							<tr>
								<td>청소년(~19세)</td>
								<td>1,000</td>
								<td>
									<select class="form-control member-price" data-price="1000"
										data-target="#student-price">
										<option value="0">인원</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
									</select>
								</td>
								<td id="student-price">원</td>
							</tr>
							<tr>
								<td>우대</td>
								<td>1,000</td>
								<td>
									<select class="form-control member-price" data-price="1000"
										data-target="#membership-price">
										<option value="0">인원</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
									</select>
								</td>
								<td id="membership-price">원</td>
							</tr>
							<tr>
								<td>우대할인</td>
								<td>500</td>
								<td>
									<select class="form-control member-price" data-price="500"
										data-target="#membership-dc-price">
										<option value="0">인원</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
									</select>
								</td>
								<td id="membership-dc-price">원</td>
							</tr>
						</tbody>
					</table>
				</div>
				
				<div class="col-xs-12">
					<div class="margin-top20 margin-bottom20 center">
						<button class="btn btn-purple submit-pricing-btn">
							선택 완료
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="step4" class="row reservation-process">
	<div class="col-xs-12">
		<div class="process-wrapper clearfix">
			<div class="process-title">
				04. 결제하기 
			</div>

			<div class="process-contents" style="display: none;">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<div class="tab-content">
							<div class="row margin-top30 margin-bottom30">	
								<div class="col-sm-12 center">
									<a id="payment-btn" href="#" class="btn btn-purple" onClick="doPay()">
										결제하기  <span id="price-for-payment"></span>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<div id="member-modal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<h4 class="modal-title">대상자 추가등록</h4>
		</div>
		<div class="modal-body">
			<div>
				<form id="add-user-form" action="">
					<table class="table centered-table">
						<tbody>
							<tr>
								<th>이름</th>
								<td><input type="text" name="addUserName" id="add-user-name" class="form-control"/></td>
							</tr>
							<tr>
								<th>성별</th>
								<td class="left">
									<div class="radio radio-primary left">
										<input name="addUserSex" type="radio" id="add-user-sex-male" value="male" checked/>
										<label for="add-user-sex-male">남성</label>

										<input name="addUserSex" type="radio" id="add-user-sex-female" value="female" />
										<label for="add-user-sex-female">여성</label>
									</div>
								</td>
							</tr>
							<tr>
								<th>휴대폰</th>
								<td>
									<div class="row">
										<div class="col-xs-4 col-md-3">
											<select id="add-user-phone1" name="addUserPhone1" class="form-control">
												<option value="010" selected>010</option>
												<option value="011">011</option>
											</select>
											<label for="add-user-phone" style="display: none"></label>
										</div>
										<div class="col-xs-4 col-md-3">
											<input name="addUserPhone2" type="text" id="add-user-phone2"  class="form-control"/>
											<label for="add-user-phone2" style="display: none"></label>
										</div>
										<div class="col-xs-4 col-md-3">
											<input name="addUserPhone3" type="text" id="add-user-phone3"  class="form-control"/>
											<label for="add-user-phone3" style="display: none"></label>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<th>생년월일</th>
								<td>
									<div class="row">
										<div class="col-xs-4 col-md-3">
											<select id="add-user-birth-year" name="addUserBirthYear" class="form-control">

												<option value='2015'>2015년</option>

												<option value='2014'>2014년</option>

												<option value='2013'>2013년</option>

												<option value='2012'>2012년</option>

												<option value='2011'>2011년</option>

												<option value='2010'>2010년</option>

												<option value='2009'>2009년</option>

												<option value='2008'>2008년</option>

												<option value='2007'>2007년</option>

												<option value='2006'>2006년</option>

												<option value='2005'>2005년</option>

												<option value='2004'>2004년</option>

												<option value='2003'>2003년</option>

												<option value='2002'>2002년</option>

												<option value='2001'>2001년</option>

												<option value='2000'>2000년</option>

												<option value='1999'>1999년</option>

												<option value='1998'>1998년</option>

												<option value='1997'>1997년</option>

												<option value='1996'>1996년</option>

												<option value='1995'>1995년</option>

												<option value='1994'>1994년</option>

												<option value='1993'>1993년</option>

												<option value='1992'>1992년</option>

												<option value='1991'>1991년</option>

												<option value='1990'>1990년</option>

												<option value='1989'>1989년</option>

												<option value='1988'>1988년</option>

												<option value='1987'>1987년</option>

												<option value='1986'>1986년</option>

												<option value='1985'>1985년</option>

												<option value='1984'>1984년</option>

												<option value='1983'>1983년</option>

												<option value='1982'>1982년</option>

												<option value='1981'>1981년</option>

												<option value='1980'>1980년</option>

												<option value='1979'>1979년</option>

												<option value='1978'>1978년</option>

												<option value='1977'>1977년</option>

												<option value='1976'>1976년</option>

												<option value='1975'>1975년</option>

												<option value='1974'>1974년</option>

												<option value='1973'>1973년</option>

												<option value='1972'>1972년</option>

												<option value='1971'>1971년</option>

												<option value='1970'>1970년</option>

												<option value='1969'>1969년</option>

												<option value='1968'>1968년</option>

												<option value='1967'>1967년</option>

												<option value='1966'>1966년</option>

												<option value='1965'>1965년</option>

												<option value='1964'>1964년</option>

												<option value='1963'>1963년</option>

												<option value='1962'>1962년</option>

												<option value='1961'>1961년</option>

												<option value='1960'>1960년</option>

												<option value='1959'>1959년</option>

												<option value='1958'>1958년</option>

												<option value='1957'>1957년</option>

												<option value='1956'>1956년</option>

												<option value='1955'>1955년</option>

												<option value='1954'>1954년</option>

												<option value='1953'>1953년</option>

												<option value='1952'>1952년</option>

												<option value='1951'>1951년</option>

												<option value='1950'>1950년</option>

												<option value='1949'>1949년</option>

												<option value='1948'>1948년</option>

												<option value='1947'>1947년</option>

												<option value='1946'>1946년</option>

												<option value='1945'>1945년</option>

												<option value='1944'>1944년</option>

												<option value='1943'>1943년</option>

												<option value='1942'>1942년</option>

												<option value='1941'>1941년</option>

												<option value='1940'>1940년</option>

												<option value='1939'>1939년</option>

												<option value='1938'>1938년</option>

												<option value='1937'>1937년</option>

												<option value='1936'>1936년</option>

												<option value='1935'>1935년</option>

												<option value='1934'>1934년</option>

												<option value='1933'>1933년</option>

												<option value='1932'>1932년</option>

												<option value='1931'>1931년</option>

												<option value='1930'>1930년</option>

												<option value='1929'>1929년</option>

												<option value='1928'>1928년</option>

												<option value='1927'>1927년</option>

												<option value='1926'>1926년</option>

												<option value='1925'>1925년</option>

												<option value='1924'>1924년</option>

												<option value='1923'>1923년</option>

												<option value='1922'>1922년</option>

												<option value='1921'>1921년</option>

												<option value='1920'>1920년</option>

												<option value='1919'>1919년</option>

												<option value='1918'>1918년</option>

												<option value='1917'>1917년</option>

												<option value='1916'>1916년</option>

												<option value='1915'>1915년</option>

												<option value='1914'>1914년</option>

												<option value='1913'>1913년</option>

												<option value='1912'>1912년</option>

												<option value='1911'>1911년</option>

												<option value='1910'>1910년</option>

												<option value='1909'>1909년</option>

												<option value='1908'>1908년</option>

												<option value='1907'>1907년</option>

												<option value='1906'>1906년</option>

												<option value='1905'>1905년</option>

												<option value='1904'>1904년</option>

												<option value='1903'>1903년</option>

												<option value='1902'>1902년</option>

												<option value='1901'>1901년</option>

												<option value='1900'>1900년</option>

											</select>
											<label for="add-user-birth-year" style="display: none"></label>
										</div>
										<div class="col-xs-4 col-md-3">
											<select id="add-user-birth-month" name="addUserBirthMonth" class="form-control">

												<option value='1'>1월</option>

												<option value='2'>2월</option>

												<option value='3'>3월</option>

												<option value='4'>4월</option>

												<option value='5'>5월</option>

												<option value='6'>6월</option>

												<option value='7'>7월</option>

												<option value='8'>8월</option>

												<option value='9'>9월</option>

												<option value='10'>10월</option>

												<option value='11'>11월</option>

												<option value='12'>12월</option>

											</select>
											<label for="add-user-birth-month" style="display: none"></label>
										</div>
										<div class="col-xs-4 col-md-3">
											<select id="add-user-birth-day" name="addUserBirthDay" class="form-control">

												<option value='1'>1일</option>

												<option value='2'>2일</option>

												<option value='3'>3일</option>

												<option value='4'>4일</option>

												<option value='5'>5일</option>

												<option value='6'>6일</option>

												<option value='7'>7일</option>

												<option value='8'>8일</option>

												<option value='9'>9일</option>

												<option value='10'>10일</option>

												<option value='11'>11일</option>

												<option value='12'>12일</option>

												<option value='13'>13일</option>

												<option value='14'>14일</option>

												<option value='15'>15일</option>

												<option value='16'>16일</option>

												<option value='17'>17일</option>

												<option value='18'>18일</option>

												<option value='19'>19일</option>

												<option value='20'>20일</option>

												<option value='21'>21일</option>

												<option value='22'>22일</option>

												<option value='23'>23일</option>

												<option value='24'>24일</option>

												<option value='25'>25일</option>

												<option value='26'>26일</option>

												<option value='27'>27일</option>

												<option value='28'>28일</option>

												<option value='29'>29일</option>

												<option value='30'>30일</option>

												<option value='31'>31일</option>

											</select>
											<label for="add-user-birth-day" style="display: none"></label>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<th>
									학교
								</th>
								<td class="left">
									<input type="text" name="addUserSchool" id="add-user-school" class="form-control"/>
									<label for="add-user-school" style="display: none"></label>
								</td>
							</tr>

							<tr>
								<td class="center" colspan="2">
									<a href="#!" class="btn btn-white" data-dismiss="modal">
										취소		
									</a>
									<button type="button" class="add-user-btn btn btn-primary">
										추가		
									</button>
								</td>
							</tr>
						</tbody>
					</table>
				</form>

				<div id="add-users">
					<div>
						<h5>추가 대상자</h5>
					</div>
					<table class="table centered-table">
						<thead>
							<tr>
								<th class="center">이름</th>
								<th class="center">성별</th>
								<th class="center">휴대폰</th>
								<th class="center">생년월일</th>
								<th class="center">학교</th>
							</tr>
						</thead>
						<tbody id="add-users-body">

						</tbody>
						<tfoot>
							<tr>
								<td class="center" colspan="5">
									<button type="button" class="submit-add-users btn btn-primary">
										저장	
									</button>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-teal" data-dismiss="modal">확인</button>
		</div>
	</div>
	<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script id="user-info-template" type="text/x-handlebars-template">
	{{#eachInMap users}}
	<tr>
		<td>
			<input type="checkbox" name="userId" id="user-id-{{val.id}}" value="{{val.id}}"/>
			<label for="user-id-{{val.id}}"></label>
		</td>
		<td>{{val.name}}</td>
		<td>{{val.relationshipText}}</td>
		<td>{{val.sexText}}</td>
		<td>{{dateFormat val.birthDay format="YYYY.MM.DD"}}</td>
		<td>{{val.phone1}}-{{val.phone2}}-{{val.phone3}}</td>
	</tr>
	{{/eachInMap}}
</script>

<script type="text/javascript">
	Handlebars.registerHelper( 'eachInMap', function ( map, block ) {
		var out = '';
		Object.keys( map ).map(function( prop ) {
			out += block.fn( {key: prop, val: map[ prop ]} );
		});
		return out;
	} );

	//Render Users
	$(function(){
		var source   = $("#user-info-template").html();
		var template = Handlebars.compile(source);
		var html = template({users: users});
		
		$("#user-infos").html(html);
	});

	
	var totalPrice = 0;
/*	
	$(".member-price").change(function(e){
		var price = parseInt($(this).data("price"));
		var calPrice = price * parseInt($(this).val());
		$($(this).data("target")).text(numberWithCommas(calPrice) + " 원");

		totalPrice = 0;
		_.each($(".member-price"), function(d, i){
			totalPrice += parseInt($(d).data("price")) * parseInt($(d).val());
		});

		$("#total-price").text(numberWithCommas(totalPrice) + " 원");
	});
*/	
</script>

<script type="text/javascript">
	$(".submit-pricing-btn").click(function(){
		$("#step3").removeClass("active");
		$("#step4").addClass("active");

		$("#selected-price > .text").text(numberWithCommas(totalPrice) + "원");

		$("#price-for-payment").text(" (" + numberWithCommas(totalPrice) + " 원)");

		$("#step3 .process-contents").slideUp();
		$("#step4 .process-contents").slideDown();
	});
</script>

<script type="text/javascript">
	var addUsers = [];

	$(".add-user-btn").click(function(e){
		var addUser = {
			name: $("#add-user-name").val(),
			sex : $("input[name='addUserSex']:checked").val(),
			phone1: $("#add-user-phone1").val(),
			phone2: $("#add-user-phone2").val(),
			phone3: $("#add-user-phone3").val(),
			birthYear: $("#add-user-birth-year").val(),
			birthMonth: $("#add-user-birth-month").val(),
			birthDay: $("#add-user-birth-day").val(),
			school: $("#add-user-school").val()
		};

		addUsers.push(addUser);

		var source   = $("#add-user-template").html();
		var template = Handlebars.compile(source);
		var html    = template(addUser);
		$("#add-users-body").append(html);

		$("#add-user-form")[0].reset();
	});

	$(".submit-add-users").click(function(e){
		ajaxLoading();
		alert("저장프로세스 수행");
		ajaxUnLoading();
		$('#member-modal').closeModal();
	});
</script>

<script id="add-user-template" type="text/x-handlebars-template">
	<tr>
		<td class="center">{{name}}</td>
		<td class="center">{{sex}}</td>
		<td class="center">{{phone1}}-{{phone2}}-{{phone3}}</td>
		<td class="center">{{birthYear}}.{{birthMonth}}.{{birthDay}}</td>
		<td class="center">{{school}}</td>
	</tr>
</script>

<script type="text/javascript">
	//Init Sample Users
	var users = {
		"1": {
			id: 1,
			name: "성춘향",
			relationship: "self",
			relationshipText: "본인",
			sex:"female",
			sexText: "여성",
			birthDay: moment("1999.01.01", "YYYYMMDD"),
			phone1: "010",
			phone2: "1234",
			phone3: "1234"
		},
		"2": {
			id: 2,
			name: "이몽룡",
			relationship: "spouse",
			relationshipText: "배우자",
			sex:"male",
			sexText: "남성",
			birthDay: moment("1999.01.01", "YYYYMMDD"),
			phone1: "010",
			phone2: "1234",
			phone3: "1234"
		}
	};
</script>
</div>

<div id="responsibility-wrapper">
	
</div>


		<div id="auth-modal" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">로그인</h4>
					</div>
					<div class="modal-body">
						<div class="remote-auth-form">
							<form>
								<div class="form-group">
									<label for="exampleInputEmail1">Email address</label>
									<input type="text" class="form-control" id="exampleInputEmail1" placeholder="Email">
								</div>
								<div class="form-group">
									<label for="exampleInputPassword1">Password</label>
									<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
								</div>
								<button id="login-btn" type="button" class="btn btn-teal">로그인</button>
							</form>
						</div>

						<div class="line gray margin-top20 margin-bottom20"></div>

						<div class="auth-links">
							<div class="margin-bottom20">
								<span class="margin-right20">&middot; 아이디/비밀번호가 생각나지 않으세요?</span>

								<a href="#" class="btn btn-teal reverse">
									아이디 찾기
								</a>

								<a href="#" class="btn btn-teal reverse">
									비밀번호 찾기
								</a>
							</div>

							<div class="margin-bottom20">
								<span class="margin-right20">&middot; 국립과천과학관의 다양한 서비스를 만나보세요!</span>

								<a href="#" class="btn btn-teal reverse">
									회원가입
								</a>
							</div>
						</div>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<script id="ptech-calendar-template" type="text/x-handlebars-template">
						<div class="ptech-calendar">
							<div class="calendar-nav">
								<div class="row nav-title">
									<div class="col-md-12 center">
										<a href="#" class="prev-month month-nav-item" data-date="{{dateFormat prevMonth}}">
											<i class="fa fa-chevron-left"></i>
										</a>

										{{dateFormat beginOfMonth  format="YYYY년 MM월"}}

										<a href="#" class="next-month month-nav-item" data-date="{{dateFormat nextMonth}}">
											<i class="fa fa-chevron-right"></i>
										</a>
									</div>
								</div>
								<div class="ptech-week week-label">
									<div class="ptech-date week-day">일</div>
									<div class="ptech-date week-day">월</div>
									<div class="ptech-date week-day">화</div>
									<div class="ptech-date week-day">수</div>
									<div class="ptech-date week-day">목</div>
									<div class="ptech-date week-day">금</div>
									<div class="ptech-date week-day">토</div>
								</div>
							</div>


							{{#each dates}}
							{{#checkWeek @index 0}}
							<div class="ptech-week">
								{{/checkWeek}}

								{{#checkWeek @index 0}}
								<div class="ptech-date sunday" data-date="{{dateFormat this}}" onclick=''>
									<div>
										{{dateFormat this format="DD"}}
									</div>
								</div>
								{{else}}
								{{#checkWeek @index 6}}
								<div class="ptech-date saturday" data-date="{{dateFormat this}}" onclick=''>
									<div>
										{{dateFormat this format="DD"}}
									</div>
								</div>
								{{else}}
								<div class="ptech-date" data-date="{{dateFormat this}}" onclick=''>
									<div>
										{{dateFormat this format="DD"}}
									</div>
								</div>
								{{/checkWeek}}
								{{/checkWeek}}

								{{#checkWeek @index 6}}
								{{#compare @index 0 operator="!="}}
							</div>
							{{/compare}}
							{{/checkWeek}}
							{{/each}}
						</div>
					</script>

					<script type="text/javascript">
						//Init Calendar
						var ptechCalendar = new PTechCalendar(new Date(), {
							container: "#program-calendar-wrapper"
						});

						//Init Sample Date
						var sampleDates = [];
						sampleDates.push(moment().format("YYYY/MM/DD"));
						sampleDates.push(moment().add(1, 'day').format("YYYY/MM/DD"));
						sampleDates.push(moment().add(2, 'day').format("YYYY/MM/DD"));
						sampleDates.push(moment().add(3, 'day').format("YYYY/MM/DD"));
						sampleDates.push(moment().add(7, 'day').format("YYYY/MM/DD"));
						sampleDates.push(moment().add(8, 'day').format("YYYY/MM/DD"));
						sampleDates.push(moment().add(9, 'day').format("YYYY/MM/DD"));

						_.forEach(sampleDates, function(d, i){
							$(".ptech-date[data-date='" + d + "']").addClass("available");
						});

						$(document).on("click", ".ptech-date.available", function(e){
							e.preventDefault();
							$(".ptech-date.available").removeClass("selected");
							$(this).addClass('selected');
							
							$("#program-times").show();
						});

						$(".reserve-btn").click(function(e){
							$("#auth-modal").modal();
						});

						$(document).on("click", "#login-btn", function(){
							$("#auth-modal").modal("hide");

							$("#selected-time > .text").text($(".reserve-btn:first-child").data("title"));

							$("#step2").removeClass("active");
							$("#step2 .process-contents").slideUp();
							
							$("#step3").addClass("active");
							$("#step3 .process-contents").slideDown();
						});
					</script>

		<script type="text/javascript">
			$(function(){
				$(".movie-program-row").click(function(e){
					e.preventDefault();
					$(".movie-program-row").removeClass("active");
					$("#selected-program > .text").text($(this).data("title"))
					$(this).addClass("active");

					$("#step1").removeClass("active");
					$("#step1 .process-contents").slideUp();
					
					$("#step2").addClass("active");
					$("#step2 .process-contents").slideDown();
				});

				$('ul.tabs.page-tabs > .tab > a').click(function(e){
					e.preventDefault();
					var tabName = $(this).attr('href');

					currentResponsibility = responsibilities.getByTabName(tabName);

					if(currentResponsibility != null){
						currentResponsibility.render();


					}else {
						$("#responsibility-wrapper").html('');
					}

					if($("#tabName").length == 1){
						$("#tabName").val(tabName);	
					}
				});

				$(document).on("change", "#all-payment-agree", function(){
					if($(this).is(":checked")){
						$("input[name='payment-agreement']").prop("checked", true);
					}else{
						$("input[name='payment-agreement']").prop("checked", false);
					}
				});

				$(document).on("change", ".receipe-type", function(){
					$("#receipe-type-option-wrapper").show();
				});

				$(document).on("change", "#receipe-option", function(){
					if($(this).val() == "phone"){
						$("#receipe-type-corporate").hide();
						$("#receipe-type-phone").show();
					}else {
						$("#receipe-type-phone").hide();
						$("#receipe-type-corporate").show();
					}
				});
			});
</script>

<div id="ajaxLoader">
	<img src="img/ajax-loader.gif" alt="ajax-loader"/>
</div>

<script type="text/javascript">
	function ajaxLoading(){
		$("body").css("opacity", "0.5");
		$("#ajaxLoader").show();
	}

	function ajaxUnLoading(){
		$("body").css("opacity", "1");
		$("#ajaxLoader").hide();
	}
</script>

<script type="text/javascript">
	$(function(){
		$('body').scrollspy({ 
			target: '#scrollspy-nav',
			offset: 230
		});

		$("#scrollspy-nav.has-scroll a.item").click(function(e){
			e.preventDefault();

			var target = this.hash;
			var $target = $(target);

			$('html, body').stop().animate({
				'scrollTop': $target.offset().top - 150
			}, 500, 'swing', function () {
				window.location.hash = target;
			});
		});


		$('.dropdown-button').dropdown({
			inDuration: 300,
			outDuration: 225,
				    constrain_width: false, // Does not change width of dropdown to that of the activator
				    gutter: 0, // Spacing from edge
				    belowOrigin: false // Displays dropdown below the button
				  }
				  );
	});
</script>

<script type="text/javascript">
	$(".up-down-toggle-btn").click(function(e){
		if($(this).hasClass("active")){
			$(this).removeClass("active");
			$($(this).data("target")).slideUp();			
		}else {
			$(this).addClass("active");
			$($(this).data("target")).slideDown();
		}
	});
</script>




<% // LJ %>

<!-- 결제 관련 START -->
	<script src="<%=request.getContextPath()%>/resources/payment1.1/script/defConst.js"></script>
	
	<div>
		<iframe name="x_frame" style="display:none" width="500" height="100"></iframe>

		<form name="mallForm" action="<%=request.getContextPath()%>/resources/payment1.1/dummy.jsp"  method="POST">
 			<input type="hidden" name="target" value="x_frame">
			<input type="hidden" name="xid" value="">
			<input type="hidden" name="eci" value="">
			<input type="hidden" name="cavv" value="">
			<input type="hidden" name="cardno" value="">
			<input type="hidden" name="hs_useamt_sh" value="">
			<!-- 암호화된 인증관련 정보 -->
			<input type="hidden" name="encData" value="">
			<input type="hidden" name="callbackUrl"	value = '<%=  request.getScheme() + "://" +  request.getServerName() + ":" +  request.getServerPort() + request.getContextPath() + "/resources/payment1.1/payResponse.jsp" %>' >
		
			<!-- 가맹점 아이디 -->
			<input type="hidden" name="mbrId"			value="551110">
			<!-- 가맹점 이름 -->
			<input type="hidden" name="mbrName"			value="CSQUARE">
			<!-- 결제종류 (1:카드, 2:가상계좌, 3:계좌이체) -->
			<input type="hidden" name="payKind"			value="1">
			<!-- 구매자 이름 -->
			<input type="hidden" name="buyerName"		value="조재국">
			<!-- 상품명 -->
			<input type="hidden" name="productName"		value="전자 패드">
			<!-- 상품가격 -->
			<input type="hidden" name="productPrice"	value="1000">
			<!-- 상품수량 -->
			<input type="hidden" name="productCount"	value="1">
			<!-- 결제금액 -->
			<input type="hidden" name="salesPrice"		value="1000">
			<!-- 주문번호 -->
			<input type="hidden" name="oid"				value="">
			<!-- 입장(오픈)일 -->
			<input type="hidden" name="openDate"		value="160311">
			<!-- 사업자번호 -->
			<input type="hidden" name="bizNo"			value="1203483832">
			  
			<input type="hidden" name="returnUrl"		value="<%=  request.getScheme() + "://" +  request.getServerName() + ":" +  request.getServerPort() + request.getContextPath() + "/entrance/reserve/callback" %>">
			<input type="hidden" name="hashValue"		value="">
			<input type="hidden" name="version"			value="1.1">
			
			
			<!-- 결제 구분 (payment:인증 & 승인, auth:인증) -->
			<input type="hidden" name="returnType"		value="payment">
			
			<input type="hidden" name="targetUrl"		value="http://pgtest.mainpay.co.kr:8080/csStdPayment/">
<!-- 
			<input type="hidden" name="targetUrl" value="https://pg.mainpay.co.kr/csStdPayment/">
			<input type="hidden" name="targetUrl" value="http://pgstq.mainpay.co.kr/csStdPayment/">
			<input type="hidden" name="targetUrl" value="http://localhost:8080/">			
 -->
		</form>
	</div>
	
	<script>
		function doPay()
		{
			
			reserve_info.oid = 'LJ_20160427_0012';
			
			
			if (confirm('결제 하시겠습니까?')) {
				$.ajax({
					method: "GET",
					url: '<c:url value="/entrance/api/payment/hash" />',
					data: {
						mbrId		: reserve_info.mbr_id,				// 가맹점 ID
						salesPrice	: reserve_info.total_price,			// 판매가격
						oid			: reserve_info.oid					// 거래번호
					}
				}).done(function(data) {
					if ((typeof data) == 'string') 
						data = JSON.parse(data);
					
					console.log("Data : " + JSON.stringify(data));
					
					if (data.hasOwnProperty("error")) {
						alert("결제 초기화에 실패했습니다.\n나중에 다시 시도해주세요.");
					} else {
						$('form[name=mallForm]').attr('target', 'x_frame');
						$("form[name=mallForm]").attr("action","<%=request.getContextPath()%>/resources/payment1.1/dummy.jsp");

						$('form[name=mallForm] > input[name=buyerName]')[0].value		= '김종민';					// 구매자 이름
						$('form[name=mallForm] > input[name=productName]')[0].value		= reserve_info.program_name;			// 상품명
						$('form[name=mallForm] > input[name=productPrice]')[0].value	= reserve_info.total_price;				// 상품가격
						$('form[name=mallForm] > input[name=productCount]')[0].value	= '1';						// 상품수량
						$('form[name=mallForm] > input[name=salesPrice]')[0].value		= reserve_info.total_price;				// 결제금액
						$('form[name=mallForm] > input[name=oid]')[0].value				= reserve_info.oid;					// 주문번호
						$('form[name=mallForm] > input[name=openDate]')[0].value		= reserve_info.play_date;				// 입장예정일
						$('form[name=mallForm] > input[name=hashValue]')[0].value		= data.hash;				// hashValue
						
						
						$('form[name=mallForm]').submit();
					}
				}).fail(function(jqXHR, textStatus) {
					alert("결제 초기화에 실패했습니다.\n나중에 다시 시도해주세요.");
				});
				
			}
	
			return;
		}
		
		
		function csPayResult(resultCode, resultMsg, resultParam)
		{
			console.log('Result Code : '	+ resultCode);
			console.log('Result Msg : '		+ resultMsg);
			console.log('Result Param : '	+ JSON.stringify(resultParam));
			
			
			/* 
				reusltParam
					cardApprovNo: 카드 승인번호
					cardTradeNo: 거래 번호
					cardName: 카드명
					cardApprovDate: 승인일
					
					cashReceipt: 현금영수증 번호
					
					vAccountBankName: 가상계좌 은행명
					vAccount: 가상계좌 계좌명
			*/
			if ( resultParam.payKind == PAY_KIND.card )
			{
//				alert(resultParam.cardApprovNo);
//				alert(resultParam.cardTradeNo);
//				alert(resultParam.payType);
				
				switch (resultCode) {
					case "00" :
						// 결제 성공
						console.log("결제 성공");
						goComplete();
						break;
					default :
						console.log("결제 실패");
					break;
				}
			}
			else if ( resultParam.payKind == PAY_KIND.account )
			{
				alert(resultParam.accountBankName);
				alert(resultParam.cashReceipt);
			}
			else if ( resultParam.payKind == PAY_KIND.vAccount )
			{
				alert(resultParam.vAccountBankName);
				alert(resultParam.vAccount);  
			}
		}
	</script>
<!-- 결제 관련 END -->

<script>
	var reserve_info = {
		program_cd		: '',						// 프로그램 코드
		program_name	: '',						// 프로그램명
		place_cd		: '',						// 장소 코드
		play_date		: '',						// 입장 예정일
		play_seq_cd		: '',						// 회차 코드
		total_price		: 0,						// 합계 금액
		total_count 	: 0,						// 예약 인원
		mbr_id			: '551110',					// 가맹점 ID
		oid				: '551110098'				// 주문번호
	};
	
	
	function getScheduleList(startMonth) {
		// 해당 월의 스케쥴 리스트 조회
		console.log('스케쥴 리스트 조회');
		console.log('program_cd	: ' + reserve_info.program_cd);
		console.log('place_cd	: ' + reserve_info.place_cd);
		console.log('target_ym	: ' + startMonth);
				
		// 전체 일정 초기화
		$('#program-calendar-wrapper').find('.ptech-date').removeClass('available');

		$.ajax({
			method: "GET",
			url: "/sciencecenter/entrance/api/scheduleList",
			data: {
				program_cd	: reserve_info.program_cd,			// '10000002',
				place_cd	: reserve_info.place_cd,			// '1002',
				target_ym	: startMonth
			}
		}).done(function(data) {
			if ((typeof data) == 'string') 
				data = JSON.parse(data);
			
			console.log("Data : " + JSON.stringify(data));
			
			for (var i in data.list) {
				try {
					var strDate = data.list[i].PLAY_DATE;
					strDate = strDate.substring(0, 4) + '/' + strDate.substring(4, 6) + '/' + strDate.substring(6, 8);
					// 특정 일자 일정 Setting
					$('#program-calendar-wrapper > div [data-date="' + strDate + '"]').addClass('available')

				} catch (e) {}
			}
			
			registDateHandler();
		}).fail(function(jqXHR, textStatus) {
//			alert( "Request failed: " + textStatus );
		});
	}
	
	
	
	
	function getSeqList(playDate) {
		reserve_info.play_date = playDate;
		
		// 해당 일의 회차 리스트 조회
		console.log('회차 리스트 조회');
		console.log('program_cd	: ' + reserve_info.program_cd);
		console.log('place_cd	: ' + reserve_info.place_cd);
		console.log('play_date	: ' + reserve_info.play_date);
		
		// 기존 회차 정보 삭제
		$('#program-times > table > tbody > tr').remove();
		
		
		$.ajax({
			method: "GET",
			url: "/sciencecenter/entrance/api/seqList",
			data: {
				program_cd	: reserve_info.program_cd,			// '10000002',
				place_cd	: reserve_info.place_cd,			// '1002',
				play_date	: reserve_info.play_date
			}
		}).done(function(data) {
			if ((typeof data) == 'string') 
				data = JSON.parse(data);
			
			console.log("Data : " + JSON.stringify(data));
			
			for (var i in data.list) {
				try {
					var item = data.list[i];
					
					var strHTML = ''
								+ '<tr>'
								+ '    <td class="center">' + item.PLAY_SEQ_NM + '</td>'
								+ '    <td class="center">'
										+ item.PLAY_ST_TIME.substring(0, 2) + ':' + item.PLAY_ST_TIME.substring(2, 4)
										+ ' ~ '
										+ item.PLAY_ED_TIME.substring(0, 2) + ':' + item.PLAY_ED_TIME.substring(2, 4)
								+ '    </td>'
								+ '    <td class="center">' + item.REMAIN_SEAT_CNT + '/' + '??' + '</td>'
								+ '    <td class="center">'
								+ (
										(item.REMAIN_SEAT_CNT > 0)
										? '<button type="button" class="btn btn-primary reserve-btn" onClick="goStep3(\'' + item.PLAY_SEQ_CD + '\')">선택</button>'
										: '<button type="button" class="btn btn-disabled">마감</button>'
								  )
								+ '    </td>'
								+ '</tr>';
					
					$('#program-times > table > tbody').append(strHTML);
//					$('#program-times > table > tbody:last').append(strHTML);
				} catch (e) {}
			}
		}).fail(function(jqXHR, textStatus) {
//			alert( "Request failed: " + textStatus );
		});
	}
	
	
	
	function selectProgram(program_cd, program_name, place_cd) {
		reserve_info.program_cd = program_cd;
		reserve_info.program_name = program_name;
		reserve_info.place_cd = place_cd;

		// Custom Event
		var event = jQuery.Event("changed");
		event.month = ptechCalendar.beginOfMonth;
		$("#ptech-calendar-template").trigger(event);
	}
	

	function goStep3(playSeqCD) {
		reserve_info.play_seq_cd = playSeqCD;
		
		$("#selected-time > .text").text($(".reserve-btn:first-child").data("title"));

		$("#step2").removeClass("active");
		$("#step2 .process-contents").slideUp();
		
		$("#step3").addClass("active");
		$("#step3 .process-contents").slideDown();
		
		
		var ticketTable = $('#step3').find('table');
		// 기존 권종 리스트 정보 삭제
		ticketTable.find('tbody > tr').remove()
		
		// 권종 리스트 조회
		$.ajax({
			method: "GET",
			url: "/sciencecenter/entrance/api/priceTypeList",
			data: {
				program_cd	: reserve_info.program_cd,			// '10000002',
				place_cd	: reserve_info.place_cd,			// '1002',
				play_date	: reserve_info.play_date,
				play_seq_cd	: reserve_info.play_seq_cd
			}
		}).done(function(data) {
			if ((typeof data) == 'string') 
				data = JSON.parse(data);
			
			console.log("Data : " + JSON.stringify(data));
			
			for (var i in data.list) {
				try {
					var item = data.list[i];
					
					var strHTML = ''
								+ '<tr>'
								+ '    <td>' + item.PRICE_TYPE_NAME + '</td>'
								+ '    <td>' + item.PRICE + '</td>'
								+ '    <td>'
								+ '        <select class="form-control member-price" data-price="' + item.PRICE + '" data-target="#' + item.PRICE_TYPE_CD + '">'
								+ '            <option value="0">인원</option>'
								+ '            <option value="1">1</option>'
								+ '            <option value="2">2</option>'
								+ '            <option value="3">3</option>'
								+ '        </select>'
								+ '    </td>'
								+ '    <td id="' + item.PRICE_TYPE_CD + '"></td>'
								+ ((i == 0) ? '    <td rowspan="' + data.list.length + '" id="total-price"></td>' : '')
								+ '</tr>'
					;
					
					ticketTable.find('tbody').append(strHTML);
				} catch (e) {}
			}
			
			$(".member-price").change(function(e){
				var price = parseInt($(this).data("price"));
				var calPrice = price * parseInt($(this).val());
				$($(this).data("target")).text(numberWithCommas(calPrice) + " 원");

				// 합계 금액 계산
				totalPrice = 0;
				_.each($(".member-price"), function(d, i){
					totalPrice += parseInt($(d).data("price")) * parseInt($(d).val());
				});

				$("#total-price").text(numberWithCommas(totalPrice) + " 원");
				reserve_info.total_price = totalPrice;
				
				// 인원수 계산
				totalCount = 0;
				_.each($(".form-control.member-price"), function(d, i){
					debugger
					var value = Number(d.value);
					if (isNaN(value))
						totalCount += value;
				});
				
				reserve_info.total_count = totalCount;
				
			});

		}).fail(function(jqXHR, textStatus) {
//			alert( "Request failed: " + textStatus );
		});
	}
	
	
	// 결제 완료 후 예약 완료 페이지로 이동
	function goComplete() {
		alert("예약이 접수되었습니다. 감사합니다.");
		
		// TODO : 예약 결과 화면 출력 (전문 통신 완료 후)
		// 프로그램
		// 예약일 (요일)
		// 회차, 시간
		// 장소

		// 예약구분		(예 : 상영관 예약)
		// 예약코드		(예 : R845788555)
		// 예약일시		(예 : 2015년 8월 12일 13:34)
		// 예약상태		(예 : 예약완료)
		// 예약자명		(예 : 성춘향)
		// 연락처		(예 : 010-1234-5678)
		// 관람인원		(예 : 5명)
		// 결제금액		(예 : 300,000원)
		// 결제방식		(예 : 신용카드 (하나카드 1234-4567-****-****) 결제완료
		
		$('#schedule-container').hide();
		$('#complete-container').show();	
	}
	
	
	function registDateHandler() {
		// 일자 선택
		$('.ptech-date.available').on("click", function(e){
			console.log('일자 선택 : ' + e.currentTarget.dataset.date);
			//2016/04/26
			var playDate = e.currentTarget.dataset.date.substring(0, 4)
						+ e.currentTarget.dataset.date.substring(5, 7)
						+ e.currentTarget.dataset.date.substring(8, 10);
				
			getSeqList(playDate);
		});
	}

	
	
	$(document).ready(function() {
		/////////////////////////////////
		// Event Handler//
		
		$("#ptech-calendar-template").on('changed', function(e) {
			//console.log('ptechCalendar.changed : ' + e.month);
			
			var sMM = '' + (e.month.getMonth() + 1);
			if (sMM.length == 1)
				sMM = '0' + sMM;
			
			var sYYYYMM = '' + e.month.getFullYear();
			sYYYYMM += sMM;
			
			console.log('ptechCalendar.changed : ' + sYYYYMM);
			
			getScheduleList(sYYYYMM)
		});
	});
	
	
	function numberWithCommas(x){return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");}
</script>
</body>
</html>