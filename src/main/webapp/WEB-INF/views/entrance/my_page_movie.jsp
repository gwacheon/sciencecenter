<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix='fn' uri='http://java.sun.com/jsp/jstl/functions'%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.List"%>
<%@ page import="org.json.JSONObject"%>
<%

	List PaymentList = (List<HashMap<String,String>>)request.getAttribute("PaymentList");
	System.out.println(PaymentList.toString());

	String strUserInfo = "";
	JSONObject json = null;

try {
		Map user_info = (Map)request.getAttribute("UserInfo");
		if (user_info != null)
			json = new JSONObject(user_info);
//		out.print(">>>>> START" + "<BR>\n");
//		out.print(">>>>> Name : " + user_info.get("MEMBER_NAME") + "<BR>\n");
//		out.print(">>>>> END" + "<BR>\n");
	} catch (Exception e) {
	}
%>

<style>
.my-reservation-tab .label{
	margin-bottom: 2px;
}
</style>

<script>
	var context_path = '<%=request.getContextPath()%>';
	var user_info = <%= (json == null) ? "null" : json.toString() %>;	
</script>

<script src="<%=request.getContextPath()%>/resources/js/csquare/printThis.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/csquare/dateformat.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/csquare/util.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/csquare/entrance/pay_in_list.js"></script>

<!-- 표준결제 1.2 테스트용 -->
<!-- <script type="text/javascript" src="https://testpg.mainpay.co.kr/csStdPayment/resources/script/v1/c2StdPay.js"></script> -->
<!-- 표준결제 1.2 운영환경용 -->
<script type="text/javascript" src="https://pg.mainpay.co.kr/csStdPayment/resources/script/v1/c2StdPay.js" ></script>


		<div class="container">
			<div class="row margin-top30">
				<div class="col-md-3">
					<c:import url="/WEB-INF/views/mypage/side.jsp">
						<c:param name="active" value="reservations"></c:param>
					</c:import>
				</div>
				
				<div class="col-md-9">
					<h4 class="doc-title teal">
						예약 확인 및 취소
					</h4>

					<div>
					  <!-- Nav tabs -->
					  <ul class="nav nav-tabs" role="tablist">
					    <li role="presentation">
					    	<a href="<%= request.getContextPath() %>/payment" aria-controls="home" role="tab">프로그램 예약</a>
				    	</li>
					    <li role="presentation" class="active">
					    	<!-- <a href="my_page_movie.html">상영관 예약</a>  -->
					    	<a href="<%= request.getContextPath() %>/entrance/payment">천체투영관/스페이스월드 예약</a>
				    	</li>
					  </ul>

					  <!-- Tab panes -->
					  <div class="my-reservation-tab tab-content">
					    <div role="tabpanel" class="tab-pane active clearfix" id="programs">
					    	
					    <form name="frmSearch">
					    	<input type="hidden" name="pageNum" value="${pageNum }" />
					    	
					    	<div class="search-forms-in-mypage">
					    		<div class="hidden-xs">
						    		<div class="row">
						    			<div class="col-lg-2 col-md-3 col-xs-6">
						    				<div class="form-group">
											    <label class="sr-only" for="start-date"></label>
											    <div class="input-group">
											      <input type="text" class="form-control datepicker" id="start-date" name="start-date" placeholder="YYYY.MM.DD" value="${start_date }">
											    </div>
											  </div>
						    			</div>

						    			<div class="col-lg-2 col-md-3 col-xs-6">
						    				<div class="form-group">
											    <label class="sr-only" for="end-date"></label>
											    <input type="text" class="form-control datepicker" id="end-date" name="end-date" placeholder="YYYY.MM.DD" value="${end_date }">
											  </div>
						    			</div>
						    			<div class="col-lg-8 col-md-12 hidden-xs">
							    			<button class="btn btn-teal" type="button" onClick="fnSearch();">
							    				검색
							    			</button>
							    			<button class="btn btn-teal reverse" type="button" onClick="setPeriod('30');">
							    				1 개월
							    			</button>
							    			<button class="btn btn-teal reverse" type="button" onClick="setPeriod('90');">
							    				3 개월
							    			</button>
							    			<button class="btn btn-teal reverse" type="button" onClick="setPeriod('180');">
							    				6 개월
							    			</button>
							    			<button class="btn btn-teal reverse" type="button" onClick="setPeriod('365');">
							    				12 개월
							    			</button>
							    		</div>
						    		</div>
					    		</div>
					    	</div>
					    </form>

					    	<div class="table-responsive table-teal">
					    		<table class="table centered-table">
					    			<thead>
					    				<tr>
					    					<th>예약일</th>
					    					<th>프로그램</th>
					    					<th>인원</th>
					    					<th>상태</th>
					    					<th class="hidden-xs">예약증 출력</th>
					    				</tr>
					    			</thead>

					    			<tbody>
				    			<%
				    				if(PaymentList.size() >0) {
				    					for(int i=0; i<PaymentList.size(); i++){
				    						HashMap obj = (HashMap)PaymentList.get(i);
				    						String labelClass = "";
				    						String type = "";
				    						if("입금대기".equals(obj.get("TRAN_TYPE_NM"))) {
				    							labelClass = "label warning-label";
				    							type = "0";
				    						}
				    						if("결제완료".equals(obj.get("TRAN_TYPE_NM"))) {
				    							if(obj.get("LAST_TRAN_TYPE").equals("01")) {
				    								labelClass = "label green-label";
					    							type = "5";
				    							} else {
					    							labelClass = "label danger-label";
					    							type = "1";
				    							}
				    						}
				    						if("예약완료".equals(obj.get("TRAN_TYPE_NM"))) {
				    							labelClass = "label green-label";
				    							type = "2";
				    						}
				    						if("결제취소".equals(obj.get("TRAN_TYPE_NM"))) {
				    							labelClass = "label green-label";
				    							type = "3";
				    						}
				    						if("결제기한만료".equals(obj.get("TRAN_TYPE_NM"))) {
				    							labelClass = "label danger-label";
				    							type = "4";
				    						}
				    			%>	
					    				
					    				<tr>
					    					<td><%= obj.get("TRAN_DATE") %></td>
					    					<td class="left">
					    						<h6 class="doc-title">
					    							<%= obj.get("PROGRAM_NAME") %> (유료)
					    						</h6>
					    						<%= obj.get("PLAY_DATE") %>(<%= obj.get("WEEK_NM") %>)<br>
					    						<%= obj.get("PLAY_SEQ_NM") %>
					    					</td>
					    					<td>
					    						<%= obj.get("RESERVE_CNT") %> 명
					    					</td>
					    					<td>
					    						<span class="<%= labelClass %>">
					    						<% 	if("5".equals(type)) { %>
					    								발권완료
					    						<%	}else{ %>
					    								<%= obj.get("TRAN_TYPE_NM") %>
					    						<%	} %>
					    						</span><br>
					    						<%
					    							if("0".equals(type)) {
					    						%>
					    							입금기한 <%= obj.get("VA_CRITICALDATE") %>
					    						<%
					    							}
					    							
					    							//카드결제완료 , 가상계좌는 결제취소 없음
					    							if("1".equals(type)) {
					    						%>
					    							<% 	if("000002".equals(obj.get("PAYMENT_TYPE"))){ %>
					    							<a href="javascript:bill('<%= obj.get("UNIQUE_NBR") %>','1')" >영수증출력</a><br>
					    							<a href="javascript:cancelCard('<%= obj.get("INF_UNIQUE_NBR") %>', '<%= obj.get("PROGRAM_CD") %>', '<%= obj.get("PROGRAM_NAME") %>')" >결제취소</a>
					    							<%	}else{ %>
					    							<a href="javascript:popupCancelVa('<%= obj.get("INF_UNIQUE_NBR") %>', '<%= obj.get("PROGRAM_CD") %>', '<%= obj.get("PROGRAM_NAME") %>', <%= obj.get("TOT_SUM") %>, '<%= obj.get("PLAY_DATE")%>', '<%= obj.get("PLAY_SEQ_NM")%>', '<%= obj.get("UNIQUE_NBR")%>')" >결제취소</a>
					    							<%	} %>
					    						<%
					    							}
					    							
					    							//예약만완료
					    							if("2".equals(type)) {
					    						%>
					    						<%-- 예약상태는 않보이록 처리 : 2016.07.06.
					    							<a href="javascript:doReseve('<%= obj.get("UNIQUE_NBR") %>');">결제하기</a><br>
					    						 --%>
					    							<a href="javascript:delReseve('<%= obj.get("UNIQUE_NBR") %>','<%= obj.get("RESERVE_CNT") %>')" >취소하기</a>
					    						<%		
					    							}
					    						
					    							if("3".equals(type)) {
					    						%>
					    							<a href="javascript:bill('<%= obj.get("UNIQUE_NBR") %>','2')" >영수증출력</a><br>
					    							취소일 <%= obj.get("PAY_TRAN_DATE") %>
					    						<%
					    							}
					    						
					    							if("5".equals(type)) {
					    						%>
					    							<a href="javascript:bill('<%= obj.get("UNIQUE_NBR") %>','1')" >영수증출력</a><br>
					    						<%		
					    							}
					    						%>
					    						
					    					</td>
					    					<td class="hidden-xs">
					    					<%	if("1".equals(type)){ %>	
					    						<%	if("N".equals(obj.get("PRINT_YN"))){ %>
					    						<a href="#" data-uniqueNbr="<%= obj.get("UNIQUE_NBR") %>" class="btn btn-teal reverse receipe-btn"><i class="fa fa-search-plus"></i> 예약증</a>
					    						<%	}else{ %>
					    						<a href="#" class="btn btn-teal receipe-btn" onclick="javascript:alert('이미 예약증을 인쇄했습니다.\n예약증은 1회만 인쇄가 가능합니다.');"><i class="fa fa-search-minus"></i> 출력됨</a>
					    						<%	} %>
					    					<%	} %>	
					    					</td>
					    				</tr>
					    		<%
				    					}
				    				}else{	
					    		%>		
					    		<%
				    				}
					    		%>	
					    				<!-- 	
					    				<tr>
					    					<td>2015.11.01</td>
					    					<td class="left">
					    						<h6 class="doc-title">
					    							[천체관측소] 전시해설프로그램 (유료)
					    						</h6>
					    						2015.11.18(수)<br>
					    						전시해설 2회 16:00 ~ 16:40
					    					</td>
					    					<td>
					    						1 명
					    					</td>
					    					<td>
					    						<span class="label green-label">
					    							예약완료
					    						</span><br>
					    						
					    						<a href="#" class="" data-id="1">영수증</a><br>
					    						<a href="#" class="refund-btn" data-id="1">환불하기</a>
					    					</td>
					    					<td class="hidden-xs">
					    						<a href="#" class="btn btn-teal reverse receipe-btn"><i class="fa fa-search-plus"></i> 예약증</a>
					    					</td>
					    				</tr>
					    				<tr>
					    					<td>2015.11.01</td>
					    					<td class="left">
					    						<h6 class="doc-title">
					    							[천체관측소] 전시해설프로그램 (유료)
					    						</h6>
					    						2015.11.18(수)<br>
					    						전시해설 2회 16:00 ~ 16:40
					    					</td>
					    					<td>
					    						1 명
					    					</td>
					    					<td>
					    						<span class="label warning-label">
					    							입금대기
					    						</span><br>
					    						
					    						입금기한 2015.11.OO
					    					</td>
					    					<td class="hidden-xs">
					    						<a href="#" class="btn btn-teal reverse receipe-btn"><i class="fa fa-search-plus"></i> 예약증</a>
					    					</td>
					    				</tr>
					    				 -->
					    			</tbody>
					    		</table>
					    		<div class="pagination-wrapper">
									${pg }
								</div>
					    	</div>
					    </div>
					  </div>
					</div>
				</div>
			</div>
		</div>
	
	<!-- Modal -->
	<div class="modal fade" id="refund-modal">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">환불하기</h4>
	      </div>
	      <div class="modal-body">
	        <h5 class="doc-title">
	        	<span id="program_name">생활 속 빛의 과학</span> <small><span id="date_time">2014.01.28 18:00</span></small>
	        </h5>

	        <table class="table centered-table lined-table">
	        	<tbody>
	        		<tr>
	        			<td class="left">
	        				<strong>총 결제금액</strong><br>
	        				프로그램예약 금액 <span id="billing">27,500</span>원
        				</td>
	        			<td class="text-right"><span id="billing">27,500</span>원</td>
	        		</tr>
	        		<tr>
	        			<td class="left">
	        				<strong>공제금액</strong><br>
	        				송금수수료 <span id="exemption">500</span>원
        				</td>
	        			<td class="text-right">-<span id="exemption">500</span>원</td>
	        		</tr>
	        		<tr>
	        			<td class="left">
	        				<strong>환불예정금액</strong><br>
	        				<span id="billing">27,500</span>원 - <span id="remittance">500</span>원
        				</td>
	        			<td class="text-right"><span id="refund">27,500</span>원</td>
	        		</tr>
	        	</tbody>
	        </table>

	        <div>
	        	<h6 class="doc-title">선택한 내역은 <span class="teal">가상계좌</span>(으)로 결제하였습니다. <small>공제금액 제외 후 환불됩니다.</small>
	        	</h6>

	        	<h6 class="doc-title"><strong>결제정보입력</strong> <small>환불계좌 정보를 입력하세요.</small>
	        	</h6>
	        </div>

	        <div class="form-group">
	        	<label>예금주명</label>
	        	<input type="text" class="form-control" id="depositor">
	        </div>
	        <div class="form-group">
	        	<label>생년월일</label>
	        	<input type="text" class="form-control" id="birth" maxlength="6">
	        	<p class="help-block">(예금주 법정 생년월일 6자리)</p>
	        </div>
	        <div class="form-group">
	        	<label>은행명</label>
	        	<select class="form-control" id="bank">
	        		<option value="">은행선택</option>
	        		<option value="03">기업은행</option>
	        		<option value="11">농협중앙회</option>
	        		<option value="26">신한은행</option>
	        		<option value="81">하나은행</option>
	        		<option value="04">국민은행</option>
	        		<option value="20">우리은행</option>
	        		<option value="71">우체국</option>
	        	</select>
	        </div>
	        <div class="form-group">
	        	<label>계좌번호</label>
	        	<input type="text" class="form-control" id="account">
	        	<p class="help-block">(예금주 계좌번호)</p>
	        </div>
	        <div class="form-group">
	        	<label>전화번호</label>
	        	<input type="text" class="form-control" id="phone">
	        	<p class="help-block">(현금 영수증 재 승인용)</p>
	        </div>

	        <ul>
	        	<li>예금주와 계좌번호가 동일하지 않을 경우 환불처리가 지연될 수 있습니다.</li>
	        	<li>가상계좌 예약은 취소시 송금수수료가 발생합니다.</li>
	        </ul>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-teal reverse" data-dismiss="modal">취소</button>
	        <button type="button" class="btn btn-primary" onclick="doCancelVa()">환불 신청하기</button>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="receipt-modal">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">예약확인</h4>
	      </div>
	      <div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="reservation-receipe-wrapper">
								<div class="receipe-header">
									<h4 class="doc-title">예약증</h4>
								</div>

								<div class="receipe-body margin-bottom20">
									<h2 class="doc-title" id="receipt_program_name">천체투영관</h2>
									<h5 class="doc-title" id="receipt_play_date">2015년 10월 08일 금요일</h5>
									<h5 class="doc-title" id="receipt_paly_seq">2회차 12:00~13:00</h5>
									<h5 class="doc-title" id="receipt_prgram_name2">국립과천과학관 천체투영관</h5>
								</div>

								<div class="receipe-info">
									<div class="table-responsive">
									  <table class="table lined-table">
									    <tr>
									    	<th>예약구분</th>
									    	<td>상영관 예약</td>
									    	<th>예약코드</th>
									    	<td id="receipt_rsv_nbr">R845788555</td>
									    </tr>
									    <tr>
									    	<th>예약일시</th>
									    	<td id="receipt_tran_date">2015년 8월 12일 13:34</td>
									    	<th>예약상태</th>
									    	<td id="receipt_tran_type_nm">예약완료</td>
									    </tr>
									    <tr>
									    	<th>예약자명</th>
									    	<td id="receipt_member_name">성춘향</td>
									    	<th>연락처</th>
									    	<td id="receipt_member_tel">010-1234-5678</td>
									    </tr>
									    <tr>
									    	<th>관람인원</th>
									    	<td id="receipt_reserve_cnt">5명</td>
									    	<th>결제금액</th>
									    	<td id="receipt_tot_sum">300,000원</td>
									    </tr>
									    <tr>
									    	<th>결제방식</th>
									    	<td colspan="3" id="receipt_payment_type">신용카드 (하나카드 1234-4567-****-****) 결제완료</td>
									    </tr>
									  </table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" id="receipt_unique_nbr" name="receipt_unique_nbr" value="">
				<input type="hidden" id="receipt_print_yn" name="receipt_print_yn" value="">
				<input type="hidden" id="receipt_sts" name="receipt_sts" value="">
				<div class="modal-footer">
	        <button type="button" class="btn btn-teal reverse" data-dismiss="modal" onclick="javascript:paymentRefresh();">닫기</button>
	        <button type="button" class="btn btn-primary" onClick="javascript:fnPrint();">인쇄하기</button>
	      </div>
			</div>
		</div>
	</div>

	<div id="ajaxLoader">
		<img src="../img/ajax-loader.gif" alt="ajax-loader"/>
	</div>


	<!-- 결제 방식 Modal -->
	<div class="modal fade" id="payment-choice">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">결제방식을 선택해 주세요.</h4>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" onclick="setPayType(1);">신용카드</button>
<!-- 					<button type="button" class="btn btn-primary" onclick="setPayType(2);">가상계좌</button> -->
				</div>
			</div>
		</div>
	</div>

	<!-- 결제 관련 START -->
	<div>
		<iframe name="x_frame" id='x_frame' style="display:none" width="500" height="100"></iframe>

		<form name="mallForm"  id="mallForm"  method="POST">
			<input type="hidden" name="server"			value="1">							<!-- 0 : 테스트 서버, 1 : 운영서버 --> 
			<input type="hidden" name="version"			value="1.2">
			
			<input type="hidden" name="payKind"			value="">							<!-- 결제 종류 --> 
			<input type="hidden" name="mbrId"			value="">							<!-- 상점 아이디 --> 
			<input type="hidden" name="mbrName"			value="씨스퀘어">						<!-- 가맹점명 -->
			<input type="hidden" name="buyerName"		value="조재국">						<!-- 구매자명 -->
			<input type="hidden" name="buyerMobile"		value="01032339782">				<!-- 구매자휴대폰 -->
			<input type="hidden" name="buyerEmail"		value="hong@test.com">				<!-- 구매자메일 -->
			<input type="hidden" name="oid"				value="12345678901234567890">		<!-- 주문번호 --> 
			<input type="hidden" name="productName"		value="전자 패드">						<!-- 상품명 -->
			<input type="hidden" name="salesPrice"		value="1000">						<!-- 결제금액 - 실제 결제되는 금액 -->
			<input type="hidden" name="openDate"		value="">							<!-- 입장 예정일 -->			
			<input type="hidden" name="productPrice"	value="1100">						<!-- 상품가격 -->
			<input type="hidden" name="productCount"	value="1">							<!-- 상품수량 -->
			<input type="hidden" name="bizNo"			value="1388302738">					<!-- 가맹점사업자 번호 -->
			
			<input type="hidden" name="callbackUrl"		value='<%=  request.getScheme() + "://" +  request.getServerName() + request.getContextPath() + "/entrance/reserve/payment/callback" %>'> 
			<input type="hidden" name="returnUrl"		value='<%=  request.getScheme().replace("https", "http") + "://" +  request.getServerName() + request.getContextPath() + "/entrance/reserve/payment/return" %>'>
			<input type="hidden" name="returnType"		value="payment">					<!-- 리턴타입 - 고정값 -->
			<input type="hidden" name="hashValue"		value="">							<!-- 서명값 -->
			<input type="hidden" name="authType"		value="auth">						<!-- authType - 고정값 -->
		</form>
	</div>
	<!-- 결제 관련 END -->
	
	<c:import url="/WEB-INF/views/myPayment/receipeModal.jsp"></c:import>
	
	<script type="text/javascript">
	$(function(){
		$('.datepicker').pickadate({
			format: "yyyy.mm.dd"
		});
	});
	</script>

	<script type="text/javascript">
		$(function(){
			$(".receipe-btn").click(function(e){
				e.preventDefault();
				//var id = $(this).data("id");
				//id를 통한 결제정보 획득
			
//				debugger
//				console.log('aaa');
//				console.log($(this).attr('data-uniqueNbr'));
				
				if($(this).attr('data-uniqueNbr') != null) {
					$.ajax({
						method: "GET",
						url: context_path + "/entrance/card",
						data: {
							UNIQUE_NBR	: $(this).attr('data-uniqueNbr')
						}
					}).done(function(data) {
						//console.log(data);
						console.log(data.result);
						//console.log($('#receipt_rsv_nbr'));
						
						$('#receipt_program_name').html(data.result.PROGRAME_NAME);
						$('#receipt_program_name2').html("국립과천과한관 "+data.result.PROGRAME_NAME);
						$('#receipt_rsv_nbr').html(data.result.RSV_NBR);
						$('#receipt_play_seq').html(data.result.PLAY_SEQ_NM);
						$('#receipt_play_date').html(data.result.PLAY_DATE + " " + data.result.WEEK_NM +"요일");
						$('#receipt_paly_seq').html(data.result.PLAY_SEQ_NM);
						$('#receipt_tran_date').html(data.result.TRAN_DATE + " " + data.result.TRAN_TIME_NM);
						$('#receipt_member_name').html(data.result.MEMBER_NAME);
						$('#receipt_member_tel').html(data.result.MEMBER_TEL1);
						$('#receipt_reserve_cnt').html(data.result.RESERVE_CNT + "명");
						$('#receipt_tot_sum').html(comma(data.result.TOT_SUM) + "원");
						$('#receipt_tran_type_nm').html(data.result.TRAN_TYPE_NM);
						
						//현금:000001, 신용카드:000002
						if('000002' == data.result.PAYMENT_TYPE) {
							$('#receipt_payment_type').html(data.result.PAYMENT_TYPE_NM + "(" + data.result.CARD_NAME + ")");	
						}else if('000001' == data.result.PAYMENT_TYPE){
							$('#receipt_payment_type').html(data.result.PAYMENT_TYPE_NM + "(" + data.result.VA_BANKNAME + " " + data.result.VA_ACCOUNT + ")");
						}else{
							$('#receipt_payment_type').html("예약완료");
						}
						
						$('#receipt_unique_nbr').val(data.result.UNIQUE_NBR);
						$('#receipt_print_yn').val(data.result.PRINT_YN);
						$('#receipt_sts').val(data.result.TRAN_TYPE_NM);
						
						$("#receipt-modal").modal();
					}).fail(function(jqXHR, textStatus) {
						alert("나중에 다시 시도해주세요.");
					});
				}
			});

			
		});
	</script>

	<script type="text/javascript">
		$('.dropdown-toggle').dropdown();
		$("#logout-btn").click(function(){
			$("#logout-form").submit();
		});
		
		$(".main-nav-item > a").click(function(e){
			e.preventDefault();
			
			$(this).find(".main-sub-nav").show();
		});
	</script>

	<script type="text/javascript">
		function ajaxLoading(){
			$("body").css("opacity", "0.5");
			$("#ajaxLoader").show();
		}

		function ajaxUnLoading(){
			$("body").css("opacity", "1");
			$("#ajaxLoader").hide();
		}
	</script>

	<script type="text/javascript">
		$(function(){
			$('body').scrollspy({ 
				target: '#scrollspy-nav',
				offset: 230
			});

			$("#scrollspy-nav.has-scroll a.item").click(function(e){
				e.preventDefault();

				var target = this.hash;
				var $target = $(target);

				$('html, body').stop().animate({
					'scrollTop': $target.offset().top - 150
				}, 500, 'swing', function () {
					window.location.hash = target;
				});
			});


			$('.dropdown-button').dropdown({
				inDuration: 300,
				outDuration: 225,
						    constrain_width: false, // Does not change width of dropdown to that of the activator
						    gutter: 0, // Spacing from edge
						    belowOrigin: false // Displays dropdown below the button
						  }
						  );
		});
	</script>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-66853433-1', 'auto');
		ga('send', 'pageview');
	</script>
	
	
	<script>
	function fnSearch(){
		var frm = document.frmSearch;
		frm.pageNum.value = "1";
		frm.action = '<c:url value="/entrance/payment"/>?${_csrf.parameterName}=${_csrf.token}';
		frm.method = 'POST';
		frm.target = '_self';
		frm.submit();
		check = 0;
	}
	
	
	function fn_page(num){
		var frm = document.frmSearch;
		frm.pageNum.value = num;
		frm.action = '<c:url value="/entrance/payment"/>?${_csrf.parameterName}=${_csrf.token}';
		frm.method = 'POST';
		frm.target = '_self';
		frm.submit();
	}
	
	
	function setPeriod(val) {
		var frm = document.frmSearch;
		
		var now = new Date();
	    var year= now.getFullYear();
	    var mon = (now.getMonth()+1)>9 ? ''+(now.getMonth()+1) : '0'+(now.getMonth()+1);
	    var day = now.getDate()>9 ? ''+now.getDate() : '0'+now.getDate();
	    
	    var fnow = new Date();
	    fnow.setDate(fnow.getDate() - val);
	    var fyear= fnow.getFullYear();
	    var fmon = (fnow.getMonth()+1)>9 ? ''+(fnow.getMonth()+1) : '0'+(fnow.getMonth()+1);
	    var fday = fnow.getDate()>9 ? ''+fnow.getDate() : '0'+fnow.getDate();
	    
	    
	    var end_val = year + '.' + mon + '.' + day;
	   	var start_val = fyear + '.' + fmon + '.' + fday;
	    
	   	$(this).attr('start-date').value = start_val;
	    $(this).attr('end-date').value = end_val; 
	    
	}
	
	
	function comma(num){
	    var len, point, str;  
	       
	    num = num + "";  
	    point = num.length % 3 ;
	    len = num.length;  
	   
	    str = num.substring(0, point);  
	    while (point < len) {  
	        if (str != "") str += ",";  
	        str += num.substring(point, point + 3);  
	        point += 3;  
	    }  
	     
	    return str;
	 
	}
	
	
	function fnPrint(){
		var uniqueNbr = $('#receipt_unique_nbr').val();
		var print_yn = $('#receipt_print_yn').val();
		var sts = $('#receipt_sts').val();
		
		if("결제완료" != sts && "입금대기" != sts) {
			alert('입금대기 또는 결제가 완료된 예약만 출력 가능하십니다.');
			return;
		}
		
		if("Y" == print_yn) {
			alert('이미 예약증을 인쇄했습니다.\n예약증은 1회만 인쇄가 가능합니다.');
			return;
		}else{
			
			$.ajax({
				method: "GET",
				url: context_path + "/entrance/print",
				data: {
					UNIQUE_NBR	: uniqueNbr,
				}
			}).done(function(data) {
//				window.print();
//				$('#receipe-modal .reservation-receipe-wrapper').printThis();
				PrintProc();
			}).fail(function(jqXHR, textStatus) {
				alert("나중에 다시 시도해주세요.");
			});	
		}
	}
	
	var check = 0;
	function cancelCard(infNbr, pCd, pName){
		if(check == 0) {
			if (confirm('결제 취소 하시겠습니까?')) {
				check = 1;
				$.ajax({
					method: "GET",
					url: context_path + "/entrance/api/payment/cancel",
					data: {
						INF_UNIQUE_NBR	: infNbr,
						PROGRAM_CD : pCd,
						PROGRAM_NAME : pName
					}
				}).done(function(data) {
					//alert('111');
					//console.log(data);
					fnSearch();
				}).fail(function(jqXHR, textStatus) {
					check = 0;
					alert("나중에 다시 시도해주세요.");
				});
			}
		} else if(check == 1) {
			alert("결제 취소 중입니다.");
		}
	}

	
	//영수증 출력
	function bill(uniqueNbr,refFlag) {
		$.ajax({
			method: "GET",
			url: context_path + "/entrance/card",
			data: {
				UNIQUE_NBR	: uniqueNbr
			}
		}).done(function(data) {
			//console.log(data);
			console.log(data.result);
			//console.log($('#receipt_rsv_nbr'));
			
			data.result.REF_FLAG = refFlag;
			
			ReceipeModal(data.result);
			/*
			$('#receipt_program_name').html(data.result.PROGRAME_NAME);
			$('#receipt_program_name2').html("국립과천과한관 "+data.result.PROGRAME_NAME);
			$('#receipt_rsv_nbr').html(data.result.RSV_NBR);
			$('#receipt_play_seq').html(data.result.PLAY_SEQ_NM);
			$('#receipt_play_date').html(data.result.PLAY_DATE + " " + data.result.WEEK_NM +"요일");
			$('#receipt_tran_date').html(data.result.TRAN_DATE + " " + data.result.TRAN_TIME_NM);
			$('#receipt_member_name').html(data.result.MEMBER_NAME);
			$('#receipt_member_tel').html(data.result.MEMBER_TEL1);
			$('#receipt_reserve_cnt').html(data.result.RESERVE_CNT + "명");
			$('#receipt_tot_sum').html(comma(data.result.TOT_SUM) + "원");
			$('#receipt_tran_type_nm').html(data.result.TRAN_TYPE_NM);
			
			if('000001' == data.result.PAYMENT_TYPE) {
				$('#receipt_payment_type').html(data.result.PAYMENT_TYPE_NM + "(" + data.result.CARD_NAME + ")");	
			}else if('000002' == data.result.PAYMENT_TYPE){
				$('#receipt_payment_type').html(data.result.PAYMENT_TYPE_NM + "(" + data.result.VA_BANKNAME + " " + data.result.VA_ACCOUNT + ")");
			}else{
				$('#receipt_payment_type').html("예약완료");
			}
			
			$('#receipt_unique_nbr').val(data.result.UNIQUE_NBR);
			$('#receipt_print_yn').val(data.result.PRINT_YN);
			$('#receipt_sts').val(data.result.TRAN_TYPE_NM);
			
			$("#receipe-modal").modal();
			*/
		}).fail(function(jqXHR, textStatus) {
			alert("나중에 다시 시도해주세요.");
		});
	}
	
	
	var refund_info = {
		depositor : {}
	};
	
	function popupCancelVa(infNbr, pCd, pName, totSum, pDate, pSeqNm,uniqueNbr) {
		var remittance = 500;
		var refund = totSum - remittance;

		//-----------------------------------------------------------
		refund_info.infNbr			= infNbr;
		refund_info.uniqueNbr		= uniqueNbr;
		refund_info.remittance		= remittance;
		refund_info.totSum			= totSum;
		refund_info.refund			= refund;

		refund_info.program_cd		= pCd;
		refund_info.program_name	= pName;
		refund_info.play_date		= pDate;
		
		
		//-----------------------------------------------------------
		
		// 프로그램명
		$('#refund-modal #program_name')[0].innerText = pName;
		// 날짜 시간
		$('#refund-modal #date_time')[0].innerText = pDate + ' ' + pSeqNm;
		// 결제 금액 
		$('#refund-modal #billing').each(function() {
			$(this)[0].innerText = numberWithCommas(totSum);
		});
		// 송금 수수료
		$('#refund-modal #remittance').each(function() {
			$(this)[0].innerText = numberWithCommas(remittance);
		});
		// 환불 예정 금액
		$('#refund-modal #refund').each(function() {
			$(this)[0].innerText = numberWithCommas(refund);
		});
		
		
		$("#refund-modal").modal();
	}
	
	function doCancelVa() {
		refund_info.depositor.name		= $('#refund-modal #depositor')[0].value;			// 예금주명
		refund_info.depositor.birth		= $('#refund-modal #birth')[0].value;				// 생년월일
		refund_info.depositor.bank		= $('#refund-modal #bank')[0].value;				// 은행명
		refund_info.depositor.account	= $('#refund-modal #account')[0].value;				// 계좌번호
		refund_info.depositor.phone		= $('#refund-modal #phone')[0].value;				// 전화번호
		
		// 환불 데이터 검증
		if (confirm('환불 신청 하시겠습니까?')) {
			if (!isEmpty(refund_info.infNbr)
				&& !isEmpty(refund_info.depositor.name)
				&& !isEmpty(refund_info.depositor.birth)
				&& !isEmpty(refund_info.depositor.bank)
				&& !isEmpty(refund_info.depositor.account)
				&& !isEmpty(refund_info.depositor.phone)
				&& refund_info.depositor.birth.length == 6) {
				// 환불 처리 요청
				$.ajax({
					method: "GET",
					url: context_path + "/entrance/api/payment/cancel",
					data: {
						INF_UNIQUE_NBR	: refund_info.infNbr,
						UNIQUE_NBR		: refund_info.uniqueNbr,
						PROGRAM_CD		: refund_info.program_cd,
						PROGRAM_NAME	: refund_info.program_name,
						
						DEPOSITOR_NAME		: refund_info.depositor.name	,			// 예금주명
						DEPOSITOR_BIRTH		: refund_info.depositor.birth	,			// 생년월일
						DEPOSITOR_BANK		: refund_info.depositor.bank	,			// 은행명
						DEPOSITOR_ACCOUNT	: refund_info.depositor.account	,			// 계좌번호
						DEPOSITOR_PHONE		: refund_info.depositor.phone				// 전화번호
					}
				}).done(function(data) {
					//alert('111');
					//console.log(data);
					fnSearch();
					
				}).fail(function(jqXHR, textStatus) {
					alert("나중에 다시 시도해주세요.");
				});
	
				
				
				
				
				$("#refund-modal").modal("hide");
			} else {
				alert('환불 정보를 확인하세요');
				
			}
		}
	}
	
	//예약취소
	function delReseve(uniNbr, cnt){
		if(confirm("예약취소 하시겠습니까?")) {
			$.ajax({
				method: "GET",
				url: context_path + "/entrance/delReserve",
				data: {
					UNIQUE_NBR	: uniNbr,
					CNT         : cnt
				}
			}).done(function(data) {
				
				if("OK" == data.result)  fnSearch();
				
				
			}).fail(function(jqXHR, textStatus) {
				alert("나중에 다시 시도해주세요.");
			});
		}
	}
	
	
	
	function doReseve(uniNbr) {
		reserve_info.unique_nbr = uniNbr;
		// 결제 방식 선택
		$('#payment-choice').modal();
	}
	
	
	function setPayInfo(kind) {
		payment_info.kind = kind;
		
		if (isMobile()) {
			// 서버 
// 			var pay_url = 'https://pgtest.mainpay.co.kr:8080/csStdPayment/';			// 개발
			var pay_url = 'https://pg.mainpay.co.kr/csStdPayment/';				// 상용

			switch (payment_info.kind) {
				case 1:							// 신용카드
					payment_info.action = pay_url + 'mobstep01.do';						// 카드
					break;	
				case 2:							// 가상계좌
					payment_info.action = pay_url + 'mobvcstep01.do';					// 가상계좌 
					//payment_info.action = pay_url + 'mobacstep01.do';					// 계좌이체
					alert('가상계좌 결제');
					break;
			}
			
			
			payment_info.authType = 'auth';			// 카드사 인증만 지원 
			payment_info.target = '_self';
			payment_info.returnUrl = '<%= request.getScheme().replace("https", "http") + "://" +  request.getServerName() + request.getContextPath() + "/entrance/reserve/complete" %>';
			payment_info.callbackUrl = '<%=  request.getScheme() + "://" +  request.getServerName() + request.getContextPath() + "/resources/payment1.1/payResponse.jsp" %>';
		} else {
			switch (payment_info.kind) {
				case 1:							// 신용카드
					break;	
				case 2:							// 가상계좌
					break;
			}

//			payment_info.action		= '<%=request.getContextPath()%>/resources/payment1.1/dummy.jsp';
//			payment_info.target		= 'x_frame';
			payment_info.returnUrl	= '<%= request.getScheme().replace("https", "http") + "://" +  request.getServerName() + request.getContextPath() + "/entrance/reserve/payment/return" %>';
			payment_info.callbackUrl= '<%=  request.getScheme() + "://" +  request.getServerName() + request.getContextPath() + "/entrance/reserve/payment/callback" %>';
		}
	}
	
	function paymentRefresh(){
		$('.close').trigger('click');
		fnSearch();
	}
	
	var user_info2 = <%= (json == null) ? "null" : json.toString() %>;
	var memberNo = user_info2.MEMBER_NO;
	var memberId = user_info2.MEMBER_ID;
	
	//팝업창 열기
	//var eventCookie=getNoticeCookie("popup-notice");
    //if (eventCookie != "no"){
    	//var popUrl = "../resources/popup-notice.html?MEMBER_NO="+memberNo+"&MEMBER_ID="+memberId;	//팝업창에 출력될 페이지 URL
//     	var popOption = "width=512, height=366, resizable=no, scrollbars=no, status=no;";    //팝업창 옵션(optoin)
    	//var popOption = "width=572, height=406, resizable=no, scrollbars=no, status=no;";    //팝업창 옵션(optoin)
    	//window.open(popUrl,"popup-notice",popOption);
    //}
	
    function getNoticeCookie(cookie_name) {
        var isCookie = false;
        var start, end;
        var i = 0;

        // cookie 문자열 전체를 검색
        while(i <= document.cookie.length) {
             start = i;
             end = start + cookie_name.length;
             // cookie_name과 동일한 문자가 있다면
             if(document.cookie.substring(start, end) == cookie_name) {
                 isCookie = true;
                 break;
             }
             i++;
        }

        // cookie_name 문자열을 cookie에서 찾았다면
        if(isCookie) {
            start = end + 1;
            end = document.cookie.indexOf(";", start);
            // 마지막 부분이라는 것을 의미(마지막에는 ";"가 없다)
            if(end < start)
                end = document.cookie.length;
            // cookie_name에 해당하는 value값을 추출하여 리턴한다.
            return document.cookie.substring(start, end);
        }
        // 찾지 못했다면
        return "";
    }
    
    
    

    function PrintProc(){
    	$('#receipt_btn').hide();
//    	var AreaContents = document.getElementById('receipt-modal').innerHTML;
    	var AreaContents = $('.reservation-receipe-wrapper')[0].innerHTML;
//    	var AreaContents = $('#complete-container .reservation-receipe-wrapper')[0].innerHTML;
        var strFeature;
        strFeature = "width=900,height=750,toolbar=no,location=no,directories=no";
        strFeature += ",status=no,menubar=no,scrollbars=yes,resizable=no";
//        var cssBody = '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/external.css"/> \' >';
//    	cssBody += '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/ptech.css"/> \' >';
//        cssBody += '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/application.css"/> \' >';
//        cssBody += '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/reservation.css"/> \' >';
//        cssBody += '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/user.css"/> \' >';

        var cssBody = ''
        	+ '<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">'
        	+ '<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">'
//        	+ '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/external.css"/> \' >'
//        	+ '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/ptech.css"/> \' >';
//        	+ '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/application.css"/> \' >';
//        	+ '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/reservation.css"/> \' >';
//        	+ '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/user.css"/> \' >';
        	+ '<link rel="stylesheet" type="text/css" href="' + context_path + '/resources/css/external.css">'
        	+ '<link rel="stylesheet" type="text/css" href="' + context_path + '/resources/css/ptech.css">'
        	+ '<link rel="stylesheet" type="text/css" href="' + context_path + '/resources/css/application.css">'
        	+ '<link rel="stylesheet" type="text/css" href="' + context_path + '/resources/css/reservation.css">'
        	+ '<link rel="stylesheet" type="text/css" href="' + context_path + '/resources/css/user.css">'
        ;

        
        var meta = '<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">';
        meta += '<meta charset="utf-8">';
        meta += '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        meta += '<meta name="viewport" content="width=device-width, initial-scale=1.0">';
        meta += '<meta name="author" content="">';
        meta += '<meta name="description" content="">';
        
        objWin = window.open('', 'print', strFeature);
        objWin.focus();
        
        
        objWin.document.open();
        objWin.document.write('<html>');
        objWin.document.write(meta);
        objWin.document.write('<head>');
        objWin.document.write(cssBody);
        objWin.document.write('</head><body class="modal-open" style="padding-right:17px;padding-left:17px">');
        objWin.document.write('<div id="content-body"');
        objWin.document.write(AreaContents);
        objWin.document.write('</div>');
        objWin.document.write('</body></html>');
        objWin.document.close();

        var delay = setTimeout(function(){
        	print(objWin);
        },2000);
        
    }

    function print(Obj){
    	$('#receipt_btn').show();
    	//console.log(Obj);
    	Obj.print();
    	Obj.close();
    }
    
    
    function isEmpty(data) {
    	var result = false;
    	if ((typeof data == 'undefined') || (data == null) || (data.length <= 0)) {
    		result = true;
    	}

    	return result;
    }
    
    
    function Comma(n){
		var result = String(n);
		return result.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
	};
	
 	// 영수증 Modal
	function ReceipeModal(data){
		var html = '';
		//var TranDate = data.RECEIPT_DATE;
		//var TranTime = data.RECEIPT_TIME;
		//var cardName = (typeof data.ISU_NAME == 'undefined')?'':data.ISU_NAME;
		//var installNo = (typeof data.INSTALL_NO == 'undefined')?'':data.INSTALL_NO;
		//var approveNo = (typeof data.APPROVE_NO == 'undefined')?'':data.APPROVE_NO;
		
		
		//if(TranDate.length == 8){
		//	TranDate = TranDate.substring(0,4)+'/'+TranDate.substring(4,6)+'/'+TranDate.substring(6);
		//}
		//if(TranTime.length == 6){
		//	TranTime = TranTime.substring(0,2)+':'+TranTime.substring(2,4);
		//}
		
		html += '<div>';
		html += '<img src=\'<c:url value="/resources/img/main_a/logo_kr.png" /> \' class="img-responsive" alt=""/ >';
		html += '</div>';
		html += '<table class="receipe-table table lined-table margin-bottom30">';
		html += '<tr>';
		html += '<th class="text-left">금액</th>';
		html += '<td class="text-right">'+Comma(data.TOT_SUM)+'원</td>';
		html += '</tr>';
		html += '<tr>';
		//if(data.PAYMENT_TYPE == '000002'){
		//	html += '<th class="text-left">부가세</th>';
		//	html += '<td class="text-right">'+Comma(data.VAT_AMT)+'원</td>';
		//}else if(data.PAYMENT_TYPE == '000003'){
			html += '<th class="text-left">수수료</th>';
			html += '<td class="text-right">0원</td>';
		//}
		html += '</tr>';
		html += '<tr>';
		html += '<th class="text-left">합계</th>';
		html += '<td class="text-right">'+Comma(data.TOT_SUM)+'원</td>';
		html += '</tr>';
		html += '</table>';
		html += '<div class="receipe-payment">';
		html += '<h4 class="teal">결제내역</h4>';
		html += '<div class="clearfix" style="padding: 10px; background-color: #f2f2f2; border: 1px solid #ddd;">';
		//if(data.PAYMENT_TYPE == '000002'){
			if(data.REF_FLAG == '1'){
				html += '신용카드 <span class="right teal"><strong>'+Comma(data.TOT_SUM)+'원</strong></span>';
				html += '<div><span>'+data.CARD_NAME+' ****-****-****-****</span></div>';
			}else{
				html += '신용카드 <span class="right teal"><strong>-'+Comma(data.TOT_SUM)+'원</strong></span>';
				html += '<div><span>'+data.CARD_NAME+' ****-****-****-****</span></div>';
			}
		//}else if(data.PAYMENT_TYPE == '000003'){
		//	if(data.REF_FLAG == 'Y'){
		//		html += '가상계좌 <span class="right teal"><strong> - '+Comma(Number(data.PAYMENT_AMT)+Number(data.CMS_COMMISSION_AMT))+'원</strong></span>';
		//		html += '<div><span></span></div>';
		//	}else{
		//		html += '가상계좌 <span class="right teal"><strong> '+Comma(Number(data.PAYMENT_AMT)+Number(data.CMS_COMMISSION_AMT))+'원</strong></span>';
		//		html += '<div><span></span></div>';
		//	}
		//}
		html += '</div>';
		html += '<table class="table lined-table margin-top30">';
		html += '<tr>';
		html += '<td width="70px">이름</td>';
		html += '<td>'+data.MEMBER_NAME+'</td>';
		html += '<td width="70px">일시</td>';
		html += '<td>'+data.TRAN_DATE+' '+data.TRAN_TYPE_NM+'</td>';
		html += '</tr>';
		//if(data.PAYMENT_TYPE == '000002'){
			html += '<tr>';	
			html += '<td>할부</td>';
			//if(installNo == '00'){
				html += '<td>일시불</td>';
			//}else{
				// 현재 일시불뿐이 없다고 함
				//html += '<td>'+installNo+'개월</td>';
			//	html += '<td>일시불</td>';
			//}
			html += '<td>승인번호</td>';
			html += '<td>'+data.CARD_TRADENO+'</td>';
			html += '</tr>';
		//}
		
		html += '</table>';
		html += '</div>';
		html += '<div class="center-infos">';
		html += '<div class="address margin-bottom30">';
		html += '13817 경기도 과천시 상하벌로 110 국립과천과학관<br>';
		html += '<strong style="margin-right: 10px">대표전화</strong> 02-3677-1500 <strong style="margin-right: 10px;margin-left: 30px">Fax</strong> 02-3677-1328';
		html += '</div>';
		html += '<h6>영수증은 타인에게 양도할 수 없습니다.</h6>';
		html += '<h6>문의사항은 고객센터를 이용해 주세요.</h6>';
		html += '</div>';
		
		$('#reservation-receipe').empty();
		$(html).appendTo('#reservation-receipe');
		
		$("#receipe-modal").modal();
		
	}
    
    
    
    
	</script>
		
	