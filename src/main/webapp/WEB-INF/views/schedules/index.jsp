<%@page import="javax.mail.Session"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<%

//request.getSession().getAttribute("memberNo");
//request.getSession().getAttribute("memberId");
//request.getSession().getAttribute("memberName");


%>

<div id="sub-content-wrapper">

	<div class="sub-content-nav scrollspy-purple">
		<c:import url="/WEB-INF/views/schedules/detailedNavbar.jsp"/>
	</div>

	
	<div id="schedule-container" class="sub-body no-sub-child">

		<div class="narrow-sub-top schedule">
			<div class="sub-banner-tab-wrapper sub-banner-tab-purple">
				<div class="container">
					<div class="row">
						<div class="
							<c:choose>
								<c:when test="${ClassCd eq 'CL4001' }">
									col-md-8 col-md-offset-2
								</c:when>
								<c:when test="${AcademyClassSize eq '1'}">
									col-xs-6 col-xs-offset-3 col-md-4 col-md-offset-4 
								</c:when>
								<c:when test="${AcademyClassSize eq '4'}">
									col-md-10 col-md-offset-1
								</c:when>
								<c:otherwise>								
									col-md-8 col-md-offset-2
								</c:otherwise>
							</c:choose>
							">
							<ul class="nav nav-tabs">
								<c:if test="${ClassCd eq 'CL4001' }">
									<li class="col-xs-12 col-sm-6 col-md-6">
										<a class="item" href="<c:url value="/schedules/voluntary"/>">신청안내 (일반/학생)</a>
									</li>
								</c:if>
								<c:forEach items="${AcademyClassList}" var="l" varStatus="status">
									<li class=" 									
										<c:choose>
											<c:when test="${ClassCd eq 'CL4001' }">
												col-xs-12 col-sm-6 col-md-6
											</c:when>
											<c:when test="${AcademyClassSize eq '1'}">
												col-xs-12 
											</c:when>
											<c:when test="${AcademyClassSize eq '2'}">
												col-xs-12 col-sm-6 col-md-6
											</c:when>
											<c:when test="${AcademyClassSize eq '3'}">
												col-xs-12 col-sm-4 col-md-4
											</c:when>
											<c:when test="${AcademyClassSize eq '4'}">
												col-xs-12 col-sm-3 col-md-3
											</c:when>
											<c:otherwise>
												col-xs-12 col-sm-2 col-md-2
											</c:otherwise>
										</c:choose>
										<c:if test="${ClassCd eq l.CLASS_CD}"> active</c:if>
									">
										<a href="javascript:void(0)" onclick="ChangeClass('${l.CLASS_CD}')" class="item">${l.CLASS_NAME}</a>
									</li>
								</c:forEach>
								
							</ul>     
						</div>
					</div>	
				</div>

			</div>
		</div>

		<%-- <div id="schedule-search-form">

			<div class="container">

				<div class="row">

					<div class="col-md-12">

						<h4 class="search-title">

							프로그램 대상 <small>(원하시는 항목을 선택하여 예약하세요.)</small>

						</h4>

					</div>

					

					<div class="col-md-2 col-xs-4">						

						<div class="checkbox checkbox-purple">
							<c:set var="indexOf" value="${fn:indexOf(reqLecture.age, '00') }"/>

							<input name="target-checkbox" class="styled"  type="checkbox" id="typeAll" value="00" 
								<c:if test="${indexOf >= 0 or reqLecture.age == ''}">checked</c:if>/>

	                        <label for="typeAll">전체보기</label>

                        </div>

					</div>

					

					<div class="col-md-2 col-xs-4">

						<div class="checkbox checkbox-purple">
							<c:set var="indexOf" value="${fn:indexOf(reqLecture.age, '01') }"/>

							<input name="target-checkbox" class="styled"  type="checkbox" id="type1" value="01"
								<c:if test="${indexOf >= 0 }">checked</c:if>/>

	                        <label for="type1">6~7세</label>

                        </div>

					</div>

					

					<div class="col-md-2 col-xs-4">

						<div class="checkbox checkbox-purple">
							<c:set var="indexOf" value="${fn:indexOf(reqLecture.age, '02') }"/>

							<input name="target-checkbox" class="styled"  type="checkbox" id="type2" value="02"
								<c:if test="${indexOf >= 0 }">checked</c:if>/>

	                        <label for="type2">초등학생</label>

                        </div>

					</div>

					<div class="col-md-2 col-xs-4">

						<div class="checkbox checkbox-purple">
							<c:set var="indexOf" value="${fn:indexOf(reqLecture.age, '03') }"/>

							<input name="target-checkbox" class="styled"  type="checkbox" id="type3" value="03"
								<c:if test="${indexOf >= 0 }">checked</c:if>/>

	                        <label for="type3">중학생</label>

                       	</div>

					</div>

					<div class="col-md-2 col-xs-4">

						<div class="checkbox checkbox-purple">
							<c:set var="indexOf" value="${fn:indexOf(reqLecture.age, '04') }"/>

							<input name="target-checkbox" class="styled"  type="checkbox" id="type4" value="04"
								<c:if test="${indexOf >= 0 }">checked</c:if>/>

	                        <label for="type4">성인</label>

                        </div>

					</div>

				</div>

				

				<div class="row">

					<div class="col-md-12">

						<h4 class="search-title has-top-line">

							참가비 구분

						</h4>

					</div>

					

					<div class="col-md-2 col-xs-4">

						<div class="checkbox checkbox-purple">
							<c:set var="indexOf" value="${fn:indexOf(reqLecture.fee, '00') }"/>

							<input name="target-price" class="styled"  type="checkbox" id="pricingAll" value="00"
								<c:if test="${indexOf >= 0 or reqLecture.fee == ''}">checked</c:if>/>

	                        <label for="pricingAll">전체보기</label>

                        </div>

					</div>

					

					<div class="col-lg-1 col-md-2 col-xs-4">

						<div class="checkbox checkbox-purple">
							<c:set var="indexOf" value="${fn:indexOf(reqLecture.fee, '02') }"/>

							<input name="target-price" class="styled"  type="checkbox" id="pricing" value="02"
								<c:if test="${indexOf >= 0 }">checked</c:if>/>

	                        <label for="pricing">유료</label>

                        </div>

					</div>

					

					<div class="col-lg-1 col-md-2 col-xs-4">

						<div class="checkbox checkbox-purple">
							<c:set var="indexOf" value="${fn:indexOf(reqLecture.fee, '01') }"/>

							<input name="target-price" class="styled"  type="checkbox" id="non-pricing" value="01"
								<c:if test="${indexOf >= 0 }">checked</c:if>/>

	                        <label for="non-pricing">무료</label>

                        </div>

					</div>

					

					<div class="col-lg-2 col-md-2 right">

						

					</div>

				</div>

			</div>

		</div> --%>

		

		<div id="schedule-search-condition">

			<div class="container">

				<div class="row">
					<div class="col-md-12">
						<h4 class="search-title">
							프로그램 대상 <small>(원하시는 항목을 검색하여 예약하세요.)</small>
						</h4>
					</div>
				</div>
				<div class="row">

					<div class="col-sm-4 col-md-3 has-form">

						<select id="search-endNum" class="form-control purple">
							<option value="50">50개씩 보기</option>
						</select>

					</div>

					
					<%--
					<div id="multi-forms" class="col-md-6 center has-form">

						<a href="#" class="month-navigator prev-month">

							&#9664;

						</a>

						<select id="search-year" class="year-selector form-control purple">
							<c:set var="searchYear" value="${fn:substring(reqLecture.yyyymm, 0, 4) }"/>
							<fmt:formatDate var="currentYear" value="${now}" pattern="yyyy" />

							<c:forEach var="year" begin="${currentYear }" end="${currentYear + 3 }">

								<option value="${year }" <c:if test="${searchYear != '' and searchYear == year }">selected</c:if>>
									${year }
								</option>

							</c:forEach>

						</select>

						
						
						 <select id="search-month" class="month-selector form-control purple">
							<c:set var="searchMonth" value="${fn:substring(reqLecture.yyyymm, 4, 6) }"/>							
							<fmt:formatDate var="currentMonth" value="${now}" pattern="MM" />

							<c:forEach var="month" begin="1" end="12">
								<c:choose>
									<c:when test="${searchMonth != '' and searchMonth == month }">
									 	<option value="<fmt:formatNumber type="number" minIntegerDigits="2" value="${month}" />" selected>
											${month }월
										</option>
									</c:when>
									<c:when test="${searchMonth != month and month == currentMonth }">
										<option value="<fmt:formatNumber type="number" minIntegerDigits="2" value="${month}" />" selected>
											${month }월
										</option>
									</c:when>
									<c:otherwise>
										<option value="<fmt:formatNumber type="number" minIntegerDigits="2" value="${month}" />">
											${month }월
										</option>
									</c:otherwise>
								</c:choose>

							</c:forEach>

						</select> 

						

						<a href="#" class="month-navigator next-month">

							&#9654;

						</a>

					</div>

					--%>

					<div class="col-xs-4 col-sm-2 col-md-2 has-form right">

						<button id="search-btn" class="btn btn-purple right">
							검색하기
						</button>
					</div>

					<div class="col-xs-8 col-sm-6 col-md-4 has-form right">

						<input type="text" name="search-key inline" id="search-key"
							class="search-key form-control purple" placeholder="검색어를 입력하세요"

							value="${reqLecture.search }">
					</div>

					<!-- <div class="col-md-12 has-form visible-xs">
						<button class="btn btn-purple right">

							검색하기

						</button>

					</div> -->

				</div>

			</div>

		</div>

		

		<div id="schedule-items">

			<div class="container">

				<div class="row">

					<div class="col-md-12">

						<div class="table-responsive">

  							<table class="table centered-table schedule-table responsive-table">

								<thead>

									<tr>

										<th>프로그램명</th>

										<th><c:if test="${ClassCd == 'CL5001' || ClassCd == 'CL5002'}">잔여/정원</c:if><c:if test="${ClassCd != 'CL5001' && ClassCd != 'CL5002'}">대상</c:if></th>

										<th>교육비</th>

<!-- 										<th>정원/접수현황(대기)</th> -->

										<th>상태</th>

										<th>상세/예약</th>

									</tr>

								</thead>

								
								<!--오픈시 삭제 start -->
<%-- 								<c:choose> --%>
<%-- 								<c:when test="${forOpen != 'notyet'}"> --%>
<%-- 								<tbody><tr><td colspan="6"><center>해당 목록이 없습니다.</center></td></tr></tbody> --%>
<%-- 								</c:when> --%>
<%-- 								<c:otherwise> --%>

								<!--오픈시 삭제 end -->
								
								<tbody>
									
									<c:if test="${ClassCd eq 'CL7001'}">
										<tr>
											<td><a href="javascript:void(0)">천체투영관</a></td>
											<td class="center">5세이상 일반</td>
											<td class="center">유료</td>
<!-- 											<td class="center">-</td> -->
											<td class="center">
												<span class="label processing"> 접수중 </span>
											</td>
											<td class="center">
												<a href="<c:url value='/display/planetarium'/>" class="btn detail-btn">상세</a>
 												<a href="<c:url value='/entrance/reserve?PROGRAM_CD=10000002&CLASS_CD=CL7001'/>" class="btn reserve-btn">예약</a>
												<!-- <a href="javascript:getReservationPop();" class="btn reserve-btn">예약</a> -->
											</td>
										</tr>
									</c:if>

									<c:if test="${ClassCd eq 'CL7005'}">
										<tr class="center">
											<td><a href="javascript:void(0)">국제 천체투영관 영화제</a></td>
											<td class="center">전체</td>
											<td class="center">유료</td>
											<td class="center">
												<span class="label processing"> 접수중 </span>
											</td>
											<td class="center">
 												<a href="<c:url value='javascript:getReservationPop();'/>" class="btn reserve-btn">예약</a> 
											</td>
										</tr>
									</c:if>

									
									<c:if test="${ClassCd eq 'CL7002'}">
										<tr>
											<td><a href="javascript:void(0)">스페이스월드 체험코스</a></td>
											<td class="center">7세이상 일반</td>
											<td class="center">유료</td>
<!-- 											<td class="center">-</td> -->
											<td class="center">
												<span class="label processing"> 접수중 </span>
											</td>
											<td class="center">
												<a href="<c:url value='/display/planetarium/spaceWorld'/>" class="btn detail-btn">상세</a>
 												<a href="<c:url value='/entrance/reserve?PROGRAM_CD=10000003&CLASS_CD=CL7002'/>" class="btn reserve-btn">예약</a> 
												<!-- <a href="javascript:getReservationPop();" class="btn reserve-btn">예약</a> -->
											</td>
										</tr>
									</c:if>
									
									<jsp:useBean id="now" class="java.util.Date" />
									<c:choose>
										<c:when test="${not empty lecture.infos }">
											<c:forEach var="l" items="${lecture.infos }">
													<tr>
														<td><a href="javascript:void(0)">${l.courseName } </a></td>
														<td class="center">
															<c:if test="${ClassCd == 'CL5001' || ClassCd == 'CL5002'}">
																<%-- <c:if test="${l.courseCapacity-(l.applyCnt) <= 0}">
																	<c:if test="${(l.waitCapacity - l.waitCnt) > 0}">
																		대기잔여 ${l.waitCapacity-(l.waitCnt)}
																	</c:if>
																</c:if>
																<c:if test="${l.courseCapacity-(l.applyCnt) > 0}">
																	${l.courseCapacity-(l.applyCnt)} / ${l.courseCapacity}
																</c:if> --%>
																<c:choose>
																	<c:when test="${l.courseCapacity-(l.applyCnt) <= 0}">
																		<c:if test="${(l.waitCapacity - l.waitCnt) > 0}">
																			대기잔여 ${l.waitCapacity-(l.waitCnt)}
																		</c:if>
																	</c:when>
																	<c:when test="${l.courseCapacity-(l.applyCnt) > 0}">
																		<c:if test="${(l.waitCapacity - l.waitCnt) > 0 && l.waitCnt > 0}">
																			${l.courseCapacity-(l.applyCnt)} / ${l.courseCapacity}
																		</c:if>
																		<c:if test="${l.waitCnt <= 0}">
																			${l.courseCapacity-(l.applyCnt)} / ${l.courseCapacity}
																		</c:if>
																	</c:when>
																</c:choose>
															</c:if>
															<c:if test="${ClassCd != 'CL5001' && ClassCd != 'CL5002'}">
																${l.targetName }
															</c:if>
															<%-- <c:if test="${l.ageFlag eq true}">
																<c:if test="${l.ageLimitMin != 0 && l.ageLimitMin != null}">
																	${l.ageLimitMin} 세 이상   
																</c:if>
																<c:if test="${l.ageLimitMax != 0 && l.ageLimitMax != null}">
																	${l.ageLimitMax} 세 이하
																</c:if>
															</c:if>
															<c:if test="${l.ageFlag eq false}">수강생 학부모 및 일반 성인</c:if> --%>
														</td>
														<td class="center">
															<c:if test="${l.courseFee > 0}">유료</c:if>
															<c:if test="${l.courseFee == 0}">무료</c:if>
														</td>
<%-- 														<td class="center">${l.courseCapacity }/${l.reserCnt }(${l.waitCnt })명</td> --%>
														<td class="center">
															<c:set value="" var="Type" />
															<fmt:formatDate value="${now}" 				pattern="yyyyMMdd" var="today" 		scope="page" />  
															<fmt:parseDate 	value="${l.courseEndDate}" 	pattern="yyyyMMdd" var="courseED" 	scope="page"/>
															<c:choose>
																<c:when test="${today > l.courseEndDate }">
																	<span class="label processing complete"> 마감 </span>
																</c:when>
																<c:when test="${l.waitCapacity > 0}">
																	<c:choose>
																		<c:when test="${l.courseCapacity-(l.applyCnt) <= 0}">
																			<c:if test="${(l.waitCapacity - l.waitCnt) > 0}">
																				<span class="label processing"> 대기자 </span>
																				<c:set value="WAIT" var="Type" />
																			</c:if>
																			<c:if test="${(l.waitCapacity - l.waitCnt) <= 0}">
																				<span class="label processing complete"> 마감 </span>
																			</c:if>
																		</c:when>
																		<c:when test="${l.courseCapacity-(l.applyCnt) > 0}">
																			<c:if test="${(l.waitCapacity - l.waitCnt) <= 0}">
																				<span class="label processing complete"> 마감 </span>
																			</c:if>
																			<c:if test="${(l.waitCapacity - l.waitCnt) > 0 && l.waitCnt > 0}">
																				<span class="label waiting"> 대기자 </span>
																				<c:set value="WAIT" var="Type" />
																			</c:if>
																			<c:if test="${l.waitCnt <= 0}">
																				<span class="label processing"> 접수중 </span>
																				<c:set value="RESER" var="Type" />
																			</c:if>
																		</c:when>
																	</c:choose>
																</c:when>
																<c:when test="${l.waitCapacity == 0}">
																	<c:if test="${ClassCd == 'CL5001' || ClassCd == 'CL5002'}">
																		<c:if test="${(l.courseCapacity - l.applyCnt) <= 0}">
																			<span class="label processing complete"> 마감 </span>
																		</c:if>
																		<c:if test="${l.courseCapacity-(l.applyCnt) > 0}">
																			<span class="label processing"> 접수중 </span>
																			<c:set value="RESER" var="Type" />
																		</c:if>
																	</c:if>
																	<c:if test="${ClassCd != 'CL5001' && ClassCd != 'CL5002'}">
																			<span class="label processing"> 접수중 </span>
																			<c:set value="RESER" var="Type" />
																	</c:if>
																</c:when>
																<%-- <c:otherwise>
																	<c:if test="${l.courseCapacity-(l.applyCnt) <= 0}">
																		<c:if test="${(l.waitCapacity - l.waitCnt) > 0}">
																			<span class="label processing"> 대기자 </span>
																			<c:set value="WAIT" var="Type" />
																		</c:if>
			
																		<c:if test="${ClassCd == 'CL5001' || ClassCd == 'CL5002'}">
																			<c:if test="${(l.waitCapacity - l.waitCnt) <= 0}">
																				<span class="label processing"> 마감 </span>
																			</c:if>
																		</c:if>
																		<c:if test="${ClassCd != 'CL5001' && ClassCd != 'CL5002'}">
																				<span class="label processing"> 접수중 </span>
																				<c:set value="RESER" var="Type" />
																		</c:if>
																	</c:if>
																	<c:if test="${l.courseCapacity-(l.applyCnt) > 0}">
																		<span class="label processing"> 접수중 </span>
																		<c:set value="RESER" var="Type" />
																	</c:if>
																</c:otherwise> --%>
															</c:choose>
														</td>
														<td class="center">
															<a href="<c:url value='/schedules/${l.courseCd }'/>?ACADEMY_CD=${l.academyCd}&CLASS_CD=${l.classCd}&COURSE_CD=${l.courseCd}&SEMESTER_CD=${l.semesterCd}&TYPE=${Type}&FLAG=${l.scheduleFlag}&OWNER_LIMIT_FLAG=${l.ownerLimitFlag }" class="btn detail-btn">상세</a>
															<c:if test="${Type != ''}">
															<a href="javascript:void(0)" class="btn reserve-btn" 
																onclick="ShowList('${l.courseCd}','${l.semesterCd}','${Type}','${l.scheduleFlag }','${l.ownerLimitFlag }')">
																예약
															</a>
															</c:if>
															<%--c:if test="${l.semesterCd == 'SM110062153330000113'}">
															<a href="javascript:void(0)" class="btn reserve-btn" 
																onclick="ShowList('${l.courseCd}','${l.semesterCd}','true','${l.scheduleFlag }','${l.ownerLimitFlag }')">
																예약
															</a>
															</c:if--%>
															
														</td>
													</tr>
												</c:forEach>
										</c:when>

										
										

										<c:otherwise>
											<c:if test="${ClassCd != 'CL7001' && ClassCd != 'CL7002' && ClassCd != 'CL7005'}">

												<tr>

													<td colspan="5"><center>해당 목록이 없거나 읽어오지 못했습니다. <strong style="color:RED">새로고침(F5) 또는 분야선택</strong>을 하셔서 확인하시기 바랍니다.</center></td>

												</tr>
											</c:if>

										</c:otherwise>
										

									</c:choose>

									<c:if test="${ClassCd == 'CL5001' || ClassCd == 'CL5002'}">
									<br><b style= "color:RED; font-size:20px"><a href="/scipia/education/educationNotice/17980">과목별 접수 시간 변경안내(클릭)</a> 확인 바랍니다.
										불편을 드려 진심으로 죄송합니다. <br> 대기자에서 예약자로 전환된 수강생은 <span style="color:BLUE">반드시 전환당일(23:50까지) 수강료 결제</span>를 부탁드리겠습니다.
										<!-- <a href="/scipia/education/educationNotice/17980"></a>--></b>
									</c:if>
								</tbody>
								
								<!--오픈시 삭제 start -->
<%-- 								</c:otherwise> --%>
<%-- 								</c:choose> --%>
								<!--오픈시 삭제 end -->
								

							</table>

						</div>

					</div>

					

					<div class="col s12 center">

						<div class="pagination-wrapper">

							<ul class="pagination">

								<li>

									<a href="#" class="page-link  has-arrow ${page.className}"

										data-page="#">

										<i class="fa fa-angle-left"></i>

									</a>

								</li>

								<li>

									<a href="#" class="page-link active" data-page="1">

										1

									</a>

								</li>

								<li>

									<a href="#" class="page-link  has-arrow ${page.className}"

										data-page="#">

										<i class="fa fa-angle-right"></i>

									</a>

								</li>

							</ul>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

<form name="frmIndex">

	<input type="hidden" name="ACADEMY_CD" value="${AcademyCd}" />

	<input type="hidden" name="COURSE_CD" value="" />
	
	<input type="hidden" name="endNum" value="50" />

	<input type="hidden" name="SEMESTER_CD" value="" />

	<input type="hidden" name="TYPE" value="" />

	<input type="hidden" name="FLAG" value="" />
	<input type="hidden" name="CLASS_CD" value="${ClassCd}" />
	<input type="hidden" name="OWNER_LIMIT_FLAG" value="" />

</form>

<script type="text/javascript">

	$(document).ready(function() {
		
		var searchEndNum = '${reqLecture.endNum}';
		
		if(searchEndNum != null || searchEndNum != "")
			$("#search-endNum").val(searchEndNum);
		
		
		$("#search-btn").click(function(e){
			var params = [];
			
			var searchKey = $("#search-key").val();
			var year = $("#search-year").val();
			var month = $("#search-month").val();
			var ages = [];
			var fees = [];
			var endNum = $("#search-endNum").val();
			
			_.each($("input[name='target-checkbox']:checked"), function(d, i){
				ages.push($(d).val());
			});
			
			ages = ages.join(",");
			
			_.each($("input[name='target-price']:checked"), function(d, i){
				fees.push($(d).val());
			});
			
			fees = fees.join(",");
			
			//params.push("fee=00");
			//params.push("age=00");
			params.push("search=" + searchKey);
			//params.push("year=" + year);
			//params.push("month=" + month);
			params.push("endNum=" + endNum);
			params.push("CLASS_CD=${ClassCd}" );
			params.push("ACADEMY_CD=${AcademyCd}" );
			console.log(params);
			
			location.href= baseUrl + "schedules?" + params.join("&"); 
		});
		

    	$("input[name='target-checkbox']").change(function(e){

    		if($(this).attr("id") != "typeAll"){

    			$("#typeAll").prop("checked", false);

    		}else{

    			if($(this).is(":checked")){

    				$("input[name='target-checkbox']").prop("checked", false);

    				$(this).prop("checked", true);

    			}

    		}

    	});

    	

    	$("input[name='target-price']").change(function(e){

    		if($(this).attr("id") != "pricingAll"){

    			$("#pricingAll").prop("checked", false);

    		}else{

    			if($(this).is(":checked")){

    				$("input[name='target-price']").prop("checked", false);

    				$(this).prop("checked", true);

    			}

    		}

    	});

  	});

	

	function AcademyChoice(Cd){

		var frm = document.frmIndex;

		frm.ACADEMY_CD.value = Cd;
		frm.CLASS_CD.value = "";

		frm.action = '<c:url value="/schedules"/>';

		frm.method = 'GET';

		frm.submit();

	}


	function ShowList(CourseCd,SemesterCd,Type,flag, ownerLimitFlag){

		var frm = document.frmIndex;

		frm.COURSE_CD.value = CourseCd;

		frm.SEMESTER_CD.value = SemesterCd;

		frm.TYPE.value = Type;
		
		frm.OWNER_LIMIT_FLAG.value = ownerLimitFlag

		if(flag == 'true')	frm.FLAG.value = "Y";

		else		frm.FLAG.value = "N";

		
		frm.action = '<c:url value="/schedules/Show"/>';

		frm.method = 'GET';

		frm.submit();

		

	}
	
	function ChangeClass(Cd){
		var frm = document.frmIndex;
		frm.CLASS_CD.value = Cd;
		frm.action = '<c:url value="/schedules"/>';
		frm.method = 'GET';
		frm.submit();
	}
	
	
	//팝업창 열기
	function getReservationPop(){
		var eventCookie=getNoticeCookie("popup-notice");
	    //if (eventCookie != "no"){
	    	var popUrl = "http://www.sciencecenter.go.kr/scientorium/mypage/Member_Reservation_List.jsp";	//팝업창에 출력될 페이지 URL
	    	//var popUrl = "http://www.sciencecenter.go.kr/scientorium/reservation/Member_Reservation_Frm.jsp";	//팝업창에 출력될 페이지 URL
//	     	var popOption = "width=512, height=366, resizable=no, scrollbars=no, status=no;";    //팝업창 옵션(optoin)
	    	var popOption = "width=997, height=740, resizable=no, scrollbars=no, status=no;";    //팝업창 옵션(optoin)
	    	window.open(popUrl,"popup-notice",popOption);
	    //}
	}
	
    
    function getNoticeCookie(cookie_name) {
        var isCookie = false;
        var start, end;
        var i = 0;

        // cookie 문자열 전체를 검색
        while(i <= document.cookie.length) {
             start = i;
             end = start + cookie_name.length;
             // cookie_name과 동일한 문자가 있다면
             if(document.cookie.substring(start, end) == cookie_name) {
                 isCookie = true;
                 break;
             }
             i++;
        }

        // cookie_name 문자열을 cookie에서 찾았다면
        if(isCookie) {
            start = end + 1;
            end = document.cookie.indexOf(";", start);
            // 마지막 부분이라는 것을 의미(마지막에는 ";"가 없다)
            if(end < start)
                end = document.cookie.length;
            // cookie_name에 해당하는 value값을 추출하여 리턴한다.
            return document.cookie.substring(start, end);
        }
        // 찾지 못했다면
        return "";
    }

</script>

