<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<form name="frmStep2">
	<input type="hidden" name="COMPANY_CD" value="${COMPANY_CD }" />
	<input type="hidden" name="MEMBER_NO" value="${MEMBER_NO }" />
	<input type="hidden" name="COURSE_CD" value="${COURSE_CD }" />
	<input type="hidden" name="APPLY_TYPE"	value="${APPLY_TYPE }" />
	<input type="hidden" name="PRICE_NBR" value="" />
	<input type="hidden" name="OWNER_LIMIT_FLAG" value="" />
	<input type="hidden" name="AGE_FLAG" value="" />
	<input type="hidden" name="AGE_TYPE" value="" />
	<input type="hidden" name="AGE_LIMIT_MIN" value="" />
	<input type="hidden" name="AGE_LIMIT_MAX" value="" />
	<input type="hidden" name="LIMIT_CNT" value="${LIMIT_CNT }" />
	<input type="hidden" name="VACANCY_CNT" value="" />
	<input type="hidden" name="WVACANCY_CNT" value="" />
	<input type="hidden" name="COURSE_PRICE" value="" />
	<input type="hidden" name="DAY" value="" />
	<input type="hidden" name="SCHEDULE_FLAG" value="${SCHEDULE_FLAG}" />
	<input type="hidden" name="EXIST_RECEIPT_FLAG" value="" />
	<input type="hidden" name="EXIST_DATE_FLAG" value="" />
</form>
<div id="step2" class="row reservation-process">
	<div class="col-md-12">
		<div class="process-wrapper clearfix">
			<div class="process-title" >
				02. 수강생 선택 <div class="right" id="Step2Id"></div>
			</div>
			
			<div class="process-contents" style="display: none">
				<div class="process-members">
					<div class="right margin-top20 margin-bottom20">
						<button type="button" class="margin-right20 btn btn-white" data-toggle="modal" data-target="#family-modal">가족추가</button>
					</div>
					
					<!-- 가족 리스트 -->
					<div class="table-responsive">
						<div style="padding: 10px;"><font color="red">연령 제한에 의해 수강신청할 수 없는 경우 수강생이 선택되지 않습니다. 
수강 신청이 되지 않는 경우 가족 또는 수강생의 학년/반을 확인 및 업데이트하여 다시 시도해 주세요.</font><br/>
							<strong>· 본인 및 가족회원</strong>
						</div>
						<table class="table centered-table">
							<thead>
								<tr>
									<th>선택</th>
									<th>이름</th>
									<th>관계</th>
									<th>성별</th>
									<th>생년월일</th>
									<th>대상</th>
								</tr>
							</thead>
							<tbody id="user-infos">
							</tbody>
						</table>
					</div>
					
					<c:if test="${AcademyCd eq 'ACD004' || SEMESTER_CD eq 'SM110062162000000041'}">
					<div class="row">
						<div class="col-md-12">
							<strong>&nbsp;&nbsp;&nbsp;· 부가정보 입력</strong>
						<table class="table centered-table" >
							<thead>
								<tr>
									<th>학교</th>
									<th>학년</th>
									<th>반</th>
									<th>번호</th>
								</tr>
							</thead>
							<tbody id="user-infos">
								<tr>
									<td><input type="text" name="OPD001" id="OPD001" size="7"/></td>
									<td><input type="text" name="OPD002" id="OPD002" size="7"/></td>
									<td><input type="text" name="OPD003" id="OPD003" size="7"/></td>
									<td><input type="text" name="OPD004" id="OPD004" size="7"/></td>
								</tr>
							</tbody>
						
						</table>
						</div>
					</div>
					</c:if>
					<div class="row" id="div_friend_id_1" style="display:none">
						<div class="col-md-12">
							<div class="right margin-top20 margin-bottom20" id="FamilyAddBtn">
								 
							</div>	
						</div>
					</div>
					<div class="table-responsive" id="friend" style="display:none">
						<div>
							<strong>&nbsp;&nbsp;&nbsp;· 수강생 목록</strong>
						</div>
						<table class="table centered-table" >
							<thead>
								<tr>
									<th>선택</th>
									<th>이름</th>
									<th>성별</th>
									<th>생년월일</th>
									<th>학교</th>
									<th>휴대폰</th>
									<th>대상</th>
								</tr>
							</thead>
							<tbody id="friend-infos">
							</tbody>
						</table>
					</div>
					
					<div class="margin-top20 margin-bottom20 center">
						<button class="btn btn-purple member-btn" onclick="FamilyChoise();">
							선택 완료
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript"> 
	
	// 버튼 
	function FamilyAdd(){
		
		var frmStep2 = document.frmStep2;
		var html = '';
		
		if(frmStep2.SCHEDULE_FLAG.value == 'Y'){
			html += '<button type="button" class="margin-right20 btn btn-white" data-toggle="modal" data-target="#friend-modal">';
			html += '수강생추가';
			html += '</button>';
			
			$('#div_friend_id_1').show();
			$('#friend').show();
		}
		
		$('#FamilyAddBtn').empty(); 
		$(html).appendTo('#FamilyAddBtn');
		
		FamilList();
	}
	
	// 가족 리스트
	function FamilList(){
		var html ="";
		var friendHtml ="";
		var targetCnt = 0;
		var frmStep2 = document.frmStep2;
		
		$.ajax({
			type : 'get',
			url  : '<c:url value="/schedules/FamilyList"/>?${_csrf.parameterName}=${_csrf.token}',
			data : $('form[name=frmStep2]').serialize(),
			dataType : 'json',
			success : function(data){
				
				// 가족 리스트
				$.each(data.FamilyList,function(k,v){
					
					var gender ='';
					var familyTel = '';
					var limitLec = v.AGE_RESULT;
					
					if(v.GENDER_FLAG != '' && (typeof v.GENDER_FLAG == 'undefined') == false){
						gender = (v.GENDER_FLAG=="M")?"남":"여";
					}					
// 					var birth = (typeof v.BIRTH_DATE == 'undefined')?"":v.BIRTH_DATE;
					var birth = (typeof v.BIRTH_DAY == 'undefined')?"":v.BIRTH_DAY;
					
					if(birth.length == 8){
						birth = birth.substring(0,4)+'.'+birth.substring(4,6)+'.'+birth.substring(6);
					}
					
					if(v.FAMILY_TEL != '' && (typeof v.FAMILY_TEL == 'undefined') == false){
						familyTel = v.FAMILY_TEL;
					}else if(v.FAMILY_CEL != '' && (typeof v.FAMILY_CEL == 'undefined') == false){
						familyTel = v.FAMILY_CEL
					}
					
					if(familyTel.length == 11){
						familyTel = familyTel.substring(0,3)+'-'+familyTel.substring(3,7)+'-'+familyTel.substring(7);
					}
					
					html += '<tr>';
					if(limitLec == 'Y'){
						var membershipCnt = v.MEMBERSHIP_CNT;
						var existReceiptFlag = frmStep2.EXIST_RECEIPT_FLAG.value;
						var existdateFlag = frmStep2.EXIST_DATE_FLAG.value;
						
						if(existReceiptFlag == 'Y' && existdateFlag == 'Y') {
							if(membershipCnt > 0) html += '<td><input type="checkbox" name="family" id="name_'+v.FRIEND_NO+'" value="'+v.FAMILY_NO+'"></td>';
							else html += '<td><input type="checkbox" name="family" id="name_'+v.FRIEND_NO+'" value="'+v.FAMILY_NO+'" disabled></td>';
						} else
							html +=	    '<td><input type="checkbox" name="family" id="name_'+v.FRIEND_NO+'" value="'+v.FAMILY_NO+'"></td>';
					}else{
						//html +=	    '<td><input type="checkbox" name="family" id="name_'+v.FRIEND_NO+'" value="'+v.FAMILY_NO+'" onclick="ChkFalse(this);"></td>';
						html +=	    '<td><input type="checkbox" name="family" id="name_'+v.FRIEND_NO+'" value="'+v.FAMILY_NO+'" disabled></td>';
					}
					html += '<td>'+v.FAMILY_NAME+'</td>';
					html += '<td>'+v.RELATION_TYPE_NAME+'</td>';
					html += '<td>'+gender+'</td>';
					html += '<td>'+birth+'</td>';
					html += '<td>';
					if(limitLec == 'Y'){
						html += '<select name="FAMILY_TARGET" id="FAMILY_TARGET_'+targetCnt+'">';
						html += '<option value="">=== 선택 ===</option>';
						$.each(data.TargetList,function(k,v){
							html += '<option value="'+v.COMMON_CD+'">'+v.COMMON_NAME+'</option>'
						});
						html += '</select>';
					}else{
						html += '<select name="FAMILY_TARGET" id="FAMILY_TARGET_'+targetCnt+'">';
						html += '<option value="">수강대상 아님</option>';
						html += '</select>';
					}
					
					
					html +=	'</td>';
					html += '</tr>';
					
					
					
					targetCnt++;
				});
				
				$('#user-infos').empty();
				$(html).appendTo('#user-infos');
				
				// 가족외 리스트
				if(data.FriendList != '' && frmStep2.SCHEDULE_FLAG.value == 'Y'){
					$.each(data.FriendList,function(k,v){
						
						var gender ='';
						var familyTel = '';
						var limitLec = v.AGE_RESULT;
						var SchoolNm ='';
						
						if(v.GENDER_FLAG != '' && (typeof v.GENDER_FLAG == 'undefined') == false){
							gender = (v.GENDER_FLAG=="M")?"남":"여";
						}					
						var birth = (typeof v.BIRTH_DAY == 'undefined')?"":v.BIRTH_DAY;
						var SchoolNm = (typeof v.SCHOOL_NAME == 'undefined')?"":v.SCHOOL_NAME;
						
						if(birth.length == 8){
							birth = birth.substring(0,4)+'.'+birth.substring(4,6)+'.'+birth.substring(6);
						}
						
						if(v.FRIEND_TEL != '' && (typeof v.FRIEND_TEL == 'undefined') == false){
							familyTel = v.FRIEND_TEL;
						}else if(v.FRIEND_CEL != '' && (typeof v.FRIEND_CEL == 'undefined') == false){
							familyTel = v.FRIEND_CEL
						}
						
						if(familyTel.length == 11){
							familyTel = familyTel.substring(0,3)+'-'+familyTel.substring(3,7)+'-'+familyTel.substring(7);
						}
						
						friendHtml += '<tr>';
						if(limitLec == 'Y'){
							friendHtml +=	    '<td><input type="checkbox" name="friend" id="name_'+v.FRIEND_NO+'" value="'+v.FRIEND_NO+'"></td>';
						}else{
							friendHtml +=	    '<td ><input type="checkbox" name="friend" id="name_'+v.FRIEND_NO+'" value="'+v.FRIEND_NO+'" onclick="ChkFalse(this);"></td>';
						}
						friendHtml += 	'<td>'+v.FRIEND_NAME+'</td>';
						friendHtml += 	'<td>'+gender+'</td>';
						friendHtml += 	'<td>'+birth+'</td>';
						friendHtml += 	'<td>'+SchoolNm+'</td>';
						friendHtml += 	'<td>'+familyTel+'</td>';
						friendHtml += '<td>';
						friendHtml += '<select name="FRIEND_TARGET" id="FRIEND_TARGET_'+targetCnt+'">';
						friendHtml += '<option value="">=== 선택 ===</option>';
						$.each(data.TargetList,function(k,v){
							friendHtml += '<option value="'+v.COMMON_CD+'">'+v.COMMON_NAME+'</option>'
						});
						
						
						friendHtml += '</select>';
						friendHtml +=	'</td>';
						friendHtml += '</tr>';
						
						targetCnt++;
					});
					
					$('#friend-infos').empty();
					$(friendHtml).appendTo('#friend-infos');
					$('#friend').show();
				}
			}
		});
	}
	
	function ChkFalse(Obj){
		var id = Obj.id;
		alert('선택한 회원은 나이제한에 의해 수강신청할 수 없습니다.');
		document.getElementById(id).checked = false;
		return;
	}
	
	// 체크박스 선택
	function FamilyChoise(){
		var frm = document.frmStep2;
		
		var chkFamily = document.getElementsByName("family");
		var chkFriend = document.getElementsByName("friend");
		var selectFamily = document.getElementsByName("FAMILY_TARGET");
		var selectFriend = document.getElementsByName("FRIEND_TARGET");
		
		var LimitCnt  	= frm.LIMIT_CNT.value;
		var setNo =[];
		var cnt = 0;
		var cnt2 = 0;
		
		
// 		$("input:checkbox[name='family']").each(function() {
// 	        if($(this).is(":checked")){
	        	//봉사활동시 옵션정보
	        	if($("#ACADEMY_CD").val() == "ACD004" || $("#SEMESTER_CD").val() == "SM110062162000000041"){
	        		if($("#OPD001").val() == "") {
		        		alert("선택하신 봉사자의 학교를 넣으세요.");
		        		$("#OPD001").focus();
		        		return;
		        	}
		        	if($("#OPD002").val() == "") {
		        		alert("선택하신 봉사자의 학년을 넣으세요.");
		        		$("#OPD002").focus();
		        		return;
		        	}
		        	if($("#OPD003").val() == "") {
		        		alert("선택하신 봉사자의 반을 넣으세요.");
		        		$("#OPD003").focus();
		        		return;
		        	}
		        	if($("#OPD004").val() == "") {
		        		alert("선택하신 봉사자의 번호를 넣으세요.");
		        		$("#OPD004").focus();
		        		return;
		        	}
	        	}
	        	//봉사활동시 옵션정보
	        	
// 	        }
// 	   	});
		
			
		// 가족
		for(var i=0;i < chkFamily.length;i++){
			if(chkFamily[i].checked){
				
				var optionList = $("#OPD001").val() + "^"
        		+ $("#OPD002").val() + "^"
        		+ $("#OPD003").val() + "^"
        		+ $("#OPD004").val();
				
				setNo.push(chkFamily[i].value+','+selectFamily[i].value+','+optionList);
				cnt++;
			} 
		}
		
		for(var k=0;k < selectFamily.length;k++){
			if(selectFamily[k].value != ''){
				cnt2++;
			}
		}
		
		// 친구
		for(var j=0;j < chkFriend.length;j++){
			if(chkFriend[j].checked){
				setNo.push(chkFriend[j].value+','+selectFriend[j].value);
				cnt++;
			}
		}
		
		for(var f=0; f < selectFriend.length;f++){
			if(selectFriend[f].value != ''){
				cnt2++;
			}
		}
		
		if(cnt == 0){
			alert('1명 이상 선택해 주세요.');
			return;
		}
		
		if(cnt2 == 0){
			alert('대상을 선택해 주세요');
			return;
		}
		else if(cnt2 < cnt){
			alert('대상을 선택해 주세요');
			return;
		}
		else{
			if(CheckReser(cnt)){
				StudentChoice(setNo);
			}
			else{
				var frm = document.frmStep2;
				alert('잔여인원이 없거나, 신청 가능 인원을 초과했습니다(가능인원: '+frm.VACANCY_CNT.value+' 명)');
				return;
			}
		}
	}
	
	function CheckReser(cnt){
		var frm = document.frmStep2;
		var bl = true;
		
		var ApplyType 	= frm.APPLY_TYPE.value;
		var LimitCnt  	= frm.LIMIT_CNT.value;
		var VacancyCnt  = frm.VACANCY_CNT.value;
		var WVacancyCnt = frm.WVACANCY_CNT.value;
		var Cnt			= cnt;
		
		if(LimitCnt == 0){
			LimitCnt = 999;
		}
		
		if(ApplyType == 'RESER'){
			if(VacancyCnt >= LimitCnt){
				if(LimitCnt >= Cnt){
					bl = true;
				}
				else{
					bl = false;
				}
			}
			else{
				if(VacancyCnt >= Cnt){
					bl = true;
				}
				else{
					bl = false;
				}
			}
		}
		else if(ApplyType == 'WAIT'){
			if(WVacancyCnt >= LimitCnt){
				if(LimitCnt >= Cnt){
					bl = true;
				}
				else{
					bl = false;
				}
			}
			else{
				if(WVacancyCnt >= Cnt){
					bl = true;
				}
				else{
					bl = false;
				}
			}
		}
		
		return bl;
	}
	
	// 요금선택 step3
	function StudentChoice(arr){
		var frm = document.frmStep3;
		var frm2 = document.frmStep2;
		var Str = '총 '+arr.length+'명';
		
		frm.DAY.value = frm2.DAY.value;
		frm.PRICE_NBR.value = frm2.PRICE_NBR.value;
		
		$('#Step2Id').text(Str);
		
		$("#step2").removeClass("active");
		$("#step3").addClass("active");
		$("#step2").css("cursor","pointer");
		
		$('#step3').find('.process-title').attr('onclick','step3Slide()');
		$("#step2 .process-contents").slideUp();
		$("#step3 .process-contents").slideDown();
		
		StudentAjax(arr);
	}
	
</script>