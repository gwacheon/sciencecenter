<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="family-modal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">가족 추가</h4>
			</div>
			<div class="modal-body">
				<div>
					<form name="frmAddFamily">
						<table class="table centered-table">
							<tbody>
								<tr>
									<th>이름(<font color="red">*</font>)</th>
									<td><input type="text" name="FAMILY_NAME" id="FAMILY_NAME" class="form-control"/></td>
								</tr>
								<tr>
									<th>성별(<font color="red">*</font>)</th>
									<td class="left">
										<div class="radio radio-primary left">
											<input name="GENDER" type="radio" id="add-user-sex-male" value="M" checked/>
											<label for="add-user-sex-male">남성</label>
											
											<input name="GENDER" type="radio" id="add-user-sex-female" value="F" />
											<label for="add-user-sex-female">여성</label>
									  	</div>
									</td>
								</tr>
								<tr>
									<th>휴대폰(<font color="red">*</font>)</th>
									<td>
										<div class="row">
											<div class="col-xs-4 col-md-3">
												<select id="FAMILY_CEL1" name="FAMILY_CEL1" class="form-control">
													<option value="010" selected>010</option>
													<option value="011">011</option>
													<option value="016">016</option>
													<option value="017">017</option>
												</select>
												<label for="FAMILY_TEL1" style="display: none"></label>
											</div>
											<div class="col-xs-4 col-md-3">
												<input name="FAMILY_CEL2" type="text" id="FAMILY_CEL2"  maxlength="4"  class="form-control" onblur="setNumber(this)" onkeyup="setNumber(this)"/>
												<label for="FAMILY_CEL2" style="display: none"></label>
											</div>
											<div class="col-xs-4 col-md-3">
												<input name="FAMILY_CEL3" type="text" id="FAMILY_CEL3"  maxlength="4" class="form-control" onblur="setNumber(this)" onkeyup="setNumber(this)"/>
												<label for="FAMILY_CEL3" style="display: none"></label>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<th style="padding-right:5px; padding-left:5px;">생년월일(<font color="red">*</font>)</th>
									<td>
										<div class="row">
											<div class="col-xs-4 col-md-3">
												<select id="FAMILY_BIRTH_YEAR" name="FAMILY_BIRTH_YEAR" class="form-control">
													<c:forEach var="i" begin="1950" end="${YEAR }" varStatus="loop">
														<c:if test="${YEAR == (loop.end - i + loop.begin) }">
															<option value='${loop.end - i + loop.begin}' selected="selected">${loop.end - i + loop.begin}년</option>
														</c:if>
														
														<c:if test="${YEAR != (loop.end - i + loop.begin) }">
															<option value='${loop.end - i + loop.begin}'>${loop.end - i + loop.begin}년</option>
														</c:if>
													</c:forEach>
												</select>
												<label for="FAMILY_BIRTH_YEAR" style="display: none"></label>
											</div>
											<div class="col-xs-4 col-md-3">
												<select id="FAMILY_BIRTH_MONTH" name="FAMILY_BIRTH_MONTH" class="form-control">
													<c:forEach var="i" begin="1" end="12" varStatus="loop">
														<c:if test="${i < 10 }">
															<option value='0${i }'>0${i }월</option>
														</c:if>
														
														<c:if test="${i > 9 }">
															<option value='${i }'>${i }월</option>
														</c:if>
													</c:forEach>
												</select>
												<label for="FAMILY_BIRTH_MONTH" style="display: none"></label>
											</div>
											<div class="col-xs-4 col-md-3">
												<select id="FAMILY_BIRTH_DAY" name="FAMILY_BIRTH_DAY" class="form-control">
													<c:forEach var="i" begin="1" end="31" varStatus="loop">
														<c:if test="${i < 10 }">
															<option value='0${i }'>0${i }일</option>
														</c:if>
														
														<c:if test="${i > 9 }">
															<option value='${i }'>${i }일</option>
														</c:if>
													</c:forEach>
												</select>
												<label for="FAMILY_BIRTH_DAY" style="display: none"></label>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<th>
										학교
									</th>
									<td class="left">
										<input type="text" name="FAMILY_SCHOOL_NAME" id="FAMILY_SCHOOL_NAME" class="form-control"/>
										<label for="SCHOOL_NAME" style="display: none"></label>
									</td>
								</tr>
								<tr>
									<th>
										학년
									</th>
									<td class="left">
										<select id="FAMILY_SCHOOL_GRADE" name="FAMILY_SCHOOL_GRADE" class="form-control">
											<option value=''>선택</option>
											<c:forEach var="i" begin="1" end="6" varStatus="loop">
												<option value='${i }'>${i }학년</option>
											</c:forEach>
										</select>
									</td>
								</tr>
								<tr>
									<th>
										반
									</th>
									<td class="left">
										<input type="text" name="FAMILY_SCHOOL_CLASS_NAME" id="FAMILY_SCHOOL_CLASS_NAME" class="form-control"/>
										<label for="CALSS_NAME" style="display: none"></label>
									</td>
								</tr>
								<tr>
									<th>
										번호
									</th>
									<td class="left">
										<input type="text" name="FAMILY_SCHOOL_CLASS_NO" id="FAMILY_SCHOOL_CLASS_NO" class="form-control"/>
										<label for="CALSS_NO" style="display: none"></label>
									</td>
								</tr>
								<tr>
									<td class="center" colspan="2">
										<button type="button" class="add-user-btn btn btn-primary" onclick="AddFamily();" id="Family_Add">
											추가		
										</button>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
					
					<div id="add-users">
						<div>
							<h5 style="float: left; height: 30px;">추가 대상자</h5>
							<div style="margin-left:200px; float:right;"><button type="button" class="btn btn-white" onclick="resetFamily();" style="float:left;">추가대상자 리셋</button></div>
						</div>
						
						<form name="frmRegFamily">
							<input type="hidden" name="SALE_TYPE" value="${SALETYPE }" />
							<input type="hidden" name="MEMBER_NO" value="${MEMBER_NO }" />
							<input type="hidden" name="COMPANY_CD" value="110062" />
							<input type="hidden" name="FAMILY_NO" value="" />
							<table class="table centered-table">
								<thead>
									<tr>
										<th class="center">이름</th>
										<th class="center">성별</th>
										<th class="center">휴대폰</th>
										<th class="center">생년월일</th>
										<th class="center">학교</th>
										<th class="center">학년</th>
									</tr>
								</thead>
								<tbody id="ADD_FAMILY">
									
								</tbody>
							</table>
						</form>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal" onclick="RegFamily();">저장</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script type="text/javascript">
	
	function AddFamily(){
		var frm = document.frmAddFamily;
		
		var name = frm.FAMILY_NAME.value;
		var genderLen = frm.GENDER.length;
		var gender ='';
		var genderFlag ='';
		var familyTel = '';
		var birth = '';
		var schoolNm = frm.FAMILY_SCHOOL_NAME.value;
		var schoolGd = frm.FAMILY_SCHOOL_GRADE.value;
		var classNm = frm.FAMILY_SCHOOL_CLASS_NAME.value;
		var classNo = frm.FAMILY_SCHOOL_CLASS_NO.value;
		
		for(var i=0; i < genderLen;i++){
			if(frm.GENDER[i].checked){
				gender = (frm.GENDER[i].value =='M')?'남':'여';
				genderFlag = frm.GENDER[i].value;
			}	
		}
		
		if(name == ''){
			alert('이름을 입력해 주세요');
			return;
		}else if(gender == ''){
			alert('성별을 체크해 주세요');
			return;
		}else if(frm.FAMILY_CEL1.value ==''){
			alert('휴대폰 번호를 입력해 주세요');
			return;
		}else if(frm.FAMILY_CEL2.value ==''){
			alert('휴대폰 번호를 입력해 주세요');
			return;
		}else if(frm.FAMILY_CEL3.value == ''){
			alert('휴대폰 번호를 입력해 주세요');
			return;
		}
		
		familyTel = frm.FAMILY_CEL1.value+frm.FAMILY_CEL2.value+frm.FAMILY_CEL3.value;
		birth = frm.FAMILY_BIRTH_YEAR.value+frm.FAMILY_BIRTH_MONTH.value+frm.FAMILY_BIRTH_DAY.value;
		
		AddTr(name,genderFlag,gender,familyTel,birth,schoolNm,schoolGd,classNm,classNo);
		
		frm.FAMILY_NAME.value = '';
		frm.FAMILY_CEL2.value = '';
		frm.FAMILY_CEL3.value = '';
		frm.FAMILY_SCHOOL_NAME.value = '';
		frm.FAMILY_SCHOOL_GRADE.value = '';
		frm.FAMILY_BIRTH_YEAR.value  = '${YEAR}';
		frm.FAMILY_BIRTH_MONTH.value  = '01';
		frm.FAMILY_BIRTH_DAY.value   = '01';
		frm.FAMILY_SCHOOL_CLASS_NAME.value   = '';
		frm.FAMILY_SCHOOL_CLASS_NO.value   = '';
	}
	
	function AddTr(name,genderFlag,gender,familyTel,birth,schoolNm,schoolGd,classNm,classNo){
		var html ='';
		var trLen = $('#ADD_FAMILY').children('tr');
		
		if(familyTel.length == 11){
			familyTel = familyTel.substring(0,3)+'-'+familyTel.substring(3,7)+'-'+familyTel.substring(7);
		}else if(familyTel.length == 11){
			familyTel = familyTel.substring(0,3)+'-'+familyTel.substring(3,6)+'-'+familyTel.substring(6);
		}
		
		var birthRs = birth;
		if(birth.length == 8){
			birth = birth.substring(0,4)+'.'+birth.substring(4,6)+'.'+birth.substring(6);
		}
		
			html += '<tr onclick="FamilyModify(\''+name+'\',\''+genderFlag+'\',\''+familyTel+'\',\''+birthRs+'\',\''+schoolNm+'\',\''+schoolGd+'\',this)">';			
			html += '<input type="hidden" name="FAMILY_NM" value="'+name+'"/>'
			html += '<input type="hidden" name="GENDER_FLAG" value="'+genderFlag+'"/>'
			html += '<input type="hidden" name="FAMILY_CEL" value="'+familyTel+'"/>'
			html += '<input type="hidden" name="BIRTHDAY_FAMILY" value="'+birthRs+'"/>'
			html += '<input type="hidden" name="SCHOOL_NM" value="'+schoolNm+'"/>'
			html += '<input type="hidden" name="SCHOOL_GD" value="'+schoolGd+'"/>'
			html += '<input type="hidden" name="CLASS_NM" value="'+classNm+'"/>'
			html += '<input type="hidden" name="CLASS_NO" value="'+classNo+'"/>'
			
			html += '<td class="center">'+name+'</td>';
			html += '<td class="center">'+gender+'</td>';
			html += '<td class="center">'+familyTel+'</td>';
			html += '<td class="center">'+birth+'</td>';
			html += '<td class="center">'+schoolNm+'</td>';
			html += '<td class="center">'+schoolGd+'</td>';
			html += '</tr>';
		
		$(html).appendTo('#ADD_FAMILY');
		$('#Family_Add').text('추가');
	}
	
	function FamilyModify(name,genderFlag,failyTel,birthRs,schoolNm,schoolGd,Obj){
		var frm = document.frmAddFamily;
		var tel = failyTel.split("-");
		var html = "변경";
		var birthDate1 = birthRs.substring(0,4);
		var birthDate2 = birthRs.substring(4,6);
		var birthDate3 = birthRs.substring(6);
		
		frm.FAMILY_NAME.value = name;
		$('#FAMILY_CEL1').val(tel[0])
		frm.FAMILY_CEL2.value = tel[1];
		frm.FAMILY_CEL3.value = tel[2];
		$('#FAMILY_BIRTH_YEAR').val(birthDate1);
		$('#FAMILY_BIRTH_MONTH').val(birthDate2);
		$('#FAMILY_BIRTH_DAY').val(birthDate3);
		frm.FAMILY_SCHOOL_NAME.value = schoolNm;
		frm.FAMILY_SCHOOL_GRADE.value = schoolGd;
		
		$('#Family_Add').empty();
		$('#Family_Add').text(html);
		
		$(Obj).remove();
	}
	
	// 등록
	function RegFamily(){
		var trLen = $('#ADD_FAMILY').children('tr').length;
		
		if(trLen > 0){	
			$.ajax({
				type : 'get',
				url  : '<c:url value="/schedules/RegFamily"/>',
				data : encodeURI($('form[name=frmRegFamily]').serialize()),
				dataType : 'json',
				success : function(data){
					if(data.result){
						alert('성공적으로 등록 되었습니다.');
						
						$('.close').click(); 
						$('#ADD_FAMILY').empty();
						FamilList();
					}else{
						alert(data.Msg);
						return;
					}
				}
			});
		}else{
			alert('가족을 한명 이상 추가 하셔야 합니다.');
			return;
		}
	}
	
	function Reset(){
		 document.frmAddFamily.reset();
	}
	
	function resetFamily(){
		$("#ADD_FAMILY").empty();
		Reset();
	}
</script>
