<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<style>
	div.contents span {
		display: inline-block;
	}
</style>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-purple">
		<c:import url="/WEB-INF/views/schedules/detailedNavbar.jsp"/>
	</div>
	
	<div id="schedule-container" class="sub-body">
		<div id="schedule-types">
			<div class="narrow-sub-top schedule">
				<div class="sub-banner-tab-wrapper sub-banner-tab-purple">
					<div class="container">
						<div class="row">
							<div class="
								<c:choose>
									<c:when test="${AcademyClassSize eq '1'}">
										col-xs-6 col-xs-offset-3 col-md-4 col-md-offset-4 
									</c:when>
									<c:when test="${AcademyClassSize eq '4'}">
										col-md-10 col-md-offset-1
									</c:when>
									<c:otherwise>								
										col-md-8 col-md-offset-2
									</c:otherwise>
								</c:choose>
								">
								<ul class="nav nav-tabs">
									<c:forEach items="${AcademyClassList}" var="l" varStatus="status">
										<li class=" 									
											<c:choose>
												<c:when test="${AcademyClassSize eq '1'}">
													col-xs-12 
												</c:when>
												<c:when test="${AcademyClassSize eq '2'}">
													col-xs-12 col-sm-6 col-md-6
												</c:when>
												<c:when test="${AcademyClassSize eq '3'}">
													col-xs-12 col-sm-4 col-md-4
												</c:when>
												<c:when test="${AcademyClassSize eq '4'}">
													col-xs-12 col-sm-3 col-md-3
												</c:when>
												<c:otherwise>
													col-xs-12 col-sm-2 col-md-2
												</c:otherwise>
											</c:choose>
											<c:if test="${ClassCd eq l.CLASS_CD}"> active</c:if>
										">
											<a href="javascript:void(0)" onclick="ChangeClass('${l.CLASS_CD}')" class="item">${l.CLASS_NAME}</a>
										</li>
									</c:forEach>
								</ul>     
							</div>
						</div>			
					</div>
				</div>
			</div>
		</div>
		
		<div class="container margin-bottom30">
			<div id="board-spy" class="row">
				<div class="col-md-12">
					<h3 class="page-title purple">
						<div class="top-line"></div>
						 ${detail.courseName }
					</h3>
				</div>
			</div>
			<div id="board-show" class="board-show-purple">
				<div class="board-show-wrapper row">
			        <div class="col-md-12">
			            <div class="board-body">
			            	<div class="header-item-wrapper">
				                <div class="row">
		                        	<div class="col-md-9">
										<div class="header-item title">
											<div class="row">
												<div class="col-xs-3 col-sm-2 col-md-2">
													<div class="header">
														장소 정보										
													</div>
												</div>
												<div class="col-xs-9 col-sm-10 col-md-10">
													<div class="value">
														${detail.placeInfo }							
													</div>
												</div>
											</div>
										</div>   	
									</div>
									<div class="col-md-3">
										<div class="header-item">
											<div class="row">
												<div class="col-xs-3 col-sm-2 col-md-4">
													<div class="header">
														시설물
													</div>
												</div>
												<div class="col-xs-9 col-sm-10 col-md-8">
													<div class="value">
														${detail.faciltyName }							
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="header-item-wrapper">
				                <div class="row">
		                        	<div class="col-md-9">
										<div class="header-item title">
											<div class="row">
												<div class="col-xs-3 col-sm-2 col-md-2">
													<div class="header">
														장소명										
													</div>
												</div>
												<div class="col-xs-9 col-sm-10 col-md-10">
													<div class="value">
														${detail.placeName }							
													</div>
												</div>
											</div>
										</div>   	
									</div>
									<div class="col-md-3">
										<div class="header-item">
											<div class="row">
												<div class="col-xs-3 col-sm-2 col-md-4">
													<div class="header">
														
													</div>
												</div>
												<div class="col-xs-9 col-sm-10 col-md-8">
													<c:if test="${type != ''}">
														<div class="value">
																<a href="javascript:void(0)"  
																onclick="ShowList('${courseCd}','${SemesterCd}','${type}','${flag }')"
																class="btn btn-purple">예약 바로가기</a>		
														</div>
													</c:if>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
			                    <div class="col-md-12">
									<div class="contents">
			                        	${detail.courseContents }
									</div>
			                    </div>
			                </div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4 class="schedule-title">
						
					</h4>
				</div>
				<div>
					
				</div>
			</div>
		</div>
	</div>
</div>
<form name="frmIndex">
	<input type="hidden" name="ACADEMY_CD" value="${AcademyCd}" />
	<input type="hidden" name="COURSE_CD" value="" />
	<input type="hidden" name="SEMESTER_CD" value="" />
	<input type="hidden" name="TYPE" value="" />
	<input type="hidden" name="FLAG" value="" />
	<input type="hidden" name="CLASS_CD" value="${ClassCd}" />
	<input type="hidden" name="OWNER_LIMIT_FLAG" value="${ownerLimitFlag}" />
</form>
<script type="text/javascript">
function AcademyChoice(Cd){
	var frm = document.frmIndex;
	frm.ACADEMY_CD.value = Cd;
	frm.CLASS_CD.value = "";
	frm.action = '<c:url value="/schedules"/>';
	frm.method = 'GET';
	frm.submit();
}
function ChangeClass(Cd){
	var frm = document.frmIndex;
	frm.CLASS_CD.value = Cd;
	frm.action = '<c:url value="/schedules"/>';
	frm.method = 'GET';
	frm.submit();
}
function ShowList(CourseCd,SemesterCd,Type,flag){
	var frm = document.frmIndex;
	frm.COURSE_CD.value = CourseCd;
	frm.SEMESTER_CD.value = SemesterCd;
	frm.TYPE.value = Type;
	if(flag == 'true')	frm.FLAG.value = "Y";
	else		frm.FLAG.value = "N";
	
	frm.action = '<c:url value="/schedules/Show"/>';
	frm.method = 'GET';
	frm.submit();
	
}
</script>