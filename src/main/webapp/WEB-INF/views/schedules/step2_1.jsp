<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<form name="frmStep2">
	<input type="hidden" name="COMPANY_CD" value="${COMPANY_CD }" />
	<input type="hidden" name="MEMBER_NO" value="${MEMBER_NO }" />
	<input type="hidden" name="COURSE_CD" value="${COURSE_CD }" />
	<input type="hidden" name="APPLY_TYPE"	value="${APPLY_TYPE }" />
	<input type="hidden" name="PRICE_NBR" value="" />
	<input type="hidden" name="OWNER_LIMIT_FLAG" value="" />
	<input type="hidden" name="AGE_FLAG" value="" />
	<input type="hidden" name="AGE_TYPE" value="" />
	<input type="hidden" name="AGE_LIMIT_MIN" value="" />
	<input type="hidden" name="AGE_LIMIT_MAX" value="" />
	<input type="hidden" name="LIMIT_CNT" value="${LIMIT_CNT }" />
	<input type="hidden" name="VACANCY_CNT" value="" />
	<input type="hidden" name="WVACANCY_CNT" value="" />
	<input type="hidden" name="COURSE_PRICE" value="" />
	<input type="hidden" name="DAY" value="" />
	<input type="hidden" name="SCHEDULE_FLAG" value="${SCHEDULE_FLAG}" />
	<input type="hidden" name="EXIST_RECEIPT_FLAG" value="" />
	<input type="hidden" name="EXIST_DATE_FLAG" value="" />
</form>
<div id="step2" class="row reservation-process">
	<div class="col-md-12">
		<div class="process-wrapper clearfix">
			<div class="process-title" >
				02. 수강생 선택 <div class="right" id="Step2Id"></div>
			</div>
			
			<div class="process-contents" style="display: none">
				<div class="process-members">
					
					<!-- 가족 리스트 -->
					<div class="table-responsive">
						<div style="padding: 10px;"><font color="red">연령 제한에 의해 수강신청할 수 없는 경우 수강생이 선택되지 않습니다. 
수강 신청이 되지 않는 경우 가족 또는 수강생의 학년/반을 확인 및 업데이트하여 다시 시도해 주세요.</font><br/>
							<strong>· 단체</strong>
						</div>
						<table class="table centered-table">
							<colgroup>
								<col width="5%">
								<col width="20%">
								<col width="25%">
								<col width="50%">
							</colgroup>
							<thead>
								<tr>
									<th>선택</th>
									<th>이름</th>
									<th>휴대폰</th>
									<th>이메일</th>
								</tr>
							</thead>
							<tbody id="user-infos">
							</tbody>
							<thead>
								<tr>
									<th colspan="2">요금  | 대상</th>
									<td id="user-courseGrade-target"></td>
									<td id="user-family-target" colspan="2"></td>
								</tr>
						</table>
					</div>
					
					<div class="margin-top20 margin-bottom20 center">
						<button class="btn btn-purple member-btn" onclick="FamilyChoise();">
							선택 완료
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript"> 
	
	//버튼 
	function FamilyAdd(){
		FamilList();
	}
	
	// 가족 리스트
	function FamilList(){
		var html ="";
		var friendHtml ="";
		var familyTargetHtml = "";
		var courseGradeHtml = "";
		var targetCnt = 0;
		var frmStep2 = document.frmStep2;
		var familyNo = "";
		var limitLec = "";
	
		$.ajax({
			type : 'get',
			url  : '<c:url value="/schedules/FamilyList"/>?${_csrf.parameterName}=${_csrf.token}',
			data : $('form[name=frmStep2]').serialize(),
			dataType : 'json',
			success : function(data){

				// 가족 리스트
				$.each(data.FamilyList,function(k,v){
					
					limitLec = v.AGE_RESULT;
					
					html += '<tr>';
					if(limitLec == 'Y'){
						html +=	    '<td><input type="checkbox" name="family" id="name_'+v.FRIEND_NO+'" value="'+v.FAMILY_NO+'" checked="checked"></td>';
					}else{
						html +=	    '<td><input type="checkbox" name="family" id="name_'+v.FRIEND_NO+'" value="'+v.FAMILY_NO+'" disabled></td>';
					}
					html += '<td>'+v.FAMILY_NAME+'</td>';
					html += '<td>'+v.FAMILY_CEL+'</td>';
					html += '<td>'+v.MEMBER_EMAIL+'</td>';
					
					html +=	'</td>';
					html += '</tr>';
					
					familyNo = v.FAMILY_NO;
					
				});
				
				$('#user-infos').empty();
				$(html).appendTo('#user-infos');
				
				
				// 요금
				$.each(data.CourseGradeList,function(k,v){

					courseGradeHtml += '<tr><td colspan="2" style="text-align: left;">';
					var coursePrice;
					if(v.COURSE_PRICE == 0){
						coursePrice = '무료';
					}
					else{
						coursePrice = Comma(v.COURSE_PRICE)+'원';
					}
					courseGradeHtml += '<span>'+v.PRICE_NAME+' ['+coursePrice+'] </span></td>';
					courseGradeHtml += '<td><input type="number" id="PEOPLES_'+targetCnt+'" name="PEOPLES" value="0" min="1" max="100" maxlength="3" oninput="maxLengthCheck(this)" style="width:60%;"/> 명'
					courseGradeHtml += '<input type="hidden" id="coursePrice_'+targetCnt+'" name="coursePrice" value="'+v.PRICE_DETAIL_NBR+'">';
					courseGradeHtml += '<input type="hidden" id="coursePricePay_'+targetCnt+'" name="coursePricePay" value="'+coursePrice+'">';
					courseGradeHtml += '<input type="hidden" id="MEMBERSHIP_TYPE_CNT_'+targetCnt+'" name="MEMBERSHIP_TYPE_CNT" value="'+v.CNT+'">';
					courseGradeHtml += '<input type="hidden" id="MEMBERSHIP_TYPE_'+targetCnt+'" name="MEMBERSHIP_TYPE" value="'+v.MEMBERSHIP_TYPE+'">';
					courseGradeHtml += '<input type="hidden" id="PRICE_NAME_'+targetCnt+'" name="PRICE_NAME" value="'+v.PRICE_NAME+'">';
					courseGradeHtml +=	'</td></tr>';
					
					// 대상
					familyTargetHtml += '<tr><td>';
					if(limitLec == 'Y'){
						familyTargetHtml += '<select name="FAMILY_TARGET" id="FAMILY_TARGET_'+targetCnt+'">';
						familyTargetHtml += '<option value="">=== 선택 ===</option>';
						$.each(data.TargetList,function(k,v){
							familyTargetHtml += '<option value="'+v.COMMON_CD+'">'+v.COMMON_NAME+'</option>'
						});
						familyTargetHtml += '</select>';
					}else{
						familyTargetHtml += '<select name="FAMILY_TARGET" id="FAMILY_TARGET_'+targetCnt+'">';
						familyTargetHtml += '<option value="">수강대상 아님</option>';
						familyTargetHtml += '</select>';
					}
					familyTargetHtml +=	'</td></tr>';
					
					
					targetCnt++;
				});
				
				$('#user-courseGrade-target').empty();
				$(courseGradeHtml).appendTo('#user-courseGrade-target');
				
				$('#user-family-target').empty();
				$(familyTargetHtml).appendTo('#user-family-target');
				
			}
		});
	}
	
	//maxlength 체크
	function maxLengthCheck(object){
	    if (object.value.length > object.maxLength){
	    	object.value = object.value.slice(0, object.maxLength);
	    }    
  	}
	
	//꼼마,천단위
	function Comma(n){
		var result = String(n);
		return result.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
	};
	
	function ChkFalse(Obj){
		var id = Obj.id;
		alert('선택한 회원은 나이제한에 의해 수강신청할 수 없습니다.');
		document.getElementById(id).checked = false;
		return;
	}
	
	var peoplesCnt = 0;
	// 체크박스 선택
	function FamilyChoise(){
		var frm = document.frmStep2;
		
		var chkFamily = document.getElementsByName("family");
		var selectFamily = document.getElementsByName("FAMILY_TARGET");
		var peoples = document.getElementsByName("PEOPLES");
		var coursePrice = document.getElementsByName("coursePrice");
		var mebershipType = document.getElementsByName("MEMBERSHIP_TYPE");
		
		var LimitCnt  	= frm.LIMIT_CNT.value;
		var setNo =[];
		var cnt = 0;
		var cnt2 = 0;
		var cnt3 = 0;
		var cnt4 = 0;
		var cnt5 = 0;
		var peopleCnt = 0;
		var peopleFamily = true;
		
// 		$("input:checkbox[name='family']").each(function() {
// 	        if($(this).is(":checked")){
	        	//봉사활동시 옵션정보
	        	if($("#ACADEMY_CD").val() == "ACD004" || $("#SEMESTER_CD").val() == "SM110062162000000041"){
	        		if($("#OPD001").val() == "") {
		        		alert("선택하신 봉사자의 학교를 넣으세요.");
		        		$("#OPD001").focus();
		        		return;
		        	}
		        	if($("#OPD002").val() == "") {
		        		alert("선택하신 봉사자의 학년을 넣으세요.");
		        		$("#OPD002").focus();
		        		return;
		        	}
		        	if($("#OPD003").val() == "") {
		        		alert("선택하신 봉사자의 반을 넣으세요.");
		        		$("#OPD003").focus();
		        		return;
		        	}
		        	if($("#OPD004").val() == "") {
		        		alert("선택하신 봉사자의 번호를 넣으세요.");
		        		$("#OPD004").focus();
		        		return;
		        	}
	        	}
	        	//봉사활동시 옵션정보
	        	
// 	        }
// 	   	});
		
			
		// 가족
		for(var i=0;i < chkFamily.length;i++){
			if(chkFamily[i].checked){
				
				/* var optionList = $("#OPD001").val() + "^"
        		+ $("#OPD002").val() + "^"
        		+ $("#OPD003").val() + "^"
        		+ $("#OPD004").val();
				
				setNo.push(chkFamily[i].value+','+selectFamily[i].value+','+optionList); */
				cnt++;
			} 
		}
		
		for(var k=0;k < selectFamily.length;k++){
			if(selectFamily[k].value != ''){
				cnt2++;
			} else {
				cnt4++;
			}
		}
		
		for(var i=0;i < peoples.length;i++){
			if(peoples[i].value == 0)
				cnt5++;
			
			if(peoples[i].value != ''){
				cnt3++;
				var num = $("#PEOPLES_"+i).val(numberCheck(peoples[i].value));
				peopleCnt = parseInt(peopleCnt) + parseInt(numberCheck(peoples[i].value));
				if(num == 0)
					cnt2 = parseInt(cnt2) + 1;
			} 
		}
		
		for(var k=0;k < mebershipType.length;k++){
			if(mebershipType[k].value != '' && mebershipType[k].value != 'undefined'){
				if(mebershipTypeCheck(k, peoples[k].value)) {
					return;
					break;
				}
			}
		}
		
		if(peopleCnt == 0) {
			alert('1명 이상 선택해 주세요.');
			return;
		}
		
		if(cnt4 == cnt5)
			peopleFamily = false;
		
		if(cnt == 0){
			alert('1명 이상 선택해 주세요.');
			return;
		}
		
		if(cnt3 == 0){
			alert('인원을 확인 하십시오');
			return;
		} else if(cnt3 < peoples.length){
			alert('인원을 확인 하십시오');
			return;
		}
		
		if(cnt2 == 0){
			alert('대상을 선택해 주세요');
			return;
		} else if(cnt2 < selectFamily.length && peopleFamily){
			alert('대상을 확인해 주세요.');
			return;
		} else{
			if(CheckReser(peopleCnt)){
				for(var i=0;i < peoples.length;i++){
					var optionList = $("#OPD001").val() + "^"
	        		+ $("#OPD002").val() + "^"
	        		+ $("#OPD003").val() + "^"
	        		+ $("#OPD004").val();

					setNo.push(chkFamily[0].value+','+selectFamily[i].value+','+optionList+','+peoples[i].value+','+coursePrice[i].value);
				}
				
				StudentChoice(setNo, peopleCnt);
			}
			else{
				alert('잔여인원이 없거나, 신청 가능 인원을 초과했습니다(가능인원: '+peoplesCnt+' 명)');
				return;
			}
		}
	}
	
	function numberCheck(num) {
		if(num == 0 )
			return num;
		else
			return num.replace(/(^0+)/,"");
	}
	
	function mebershipTypeCheck(num, peopleCnt) {
		var idx = "MEMBERSHIP_TYPE_CNT_"+num;
		var idx2 = "PRICE_NAME_"+num;
		
		var mebershipTypeCnt = $("#"+idx).val();
		var priceName = $("#"+idx2).val();
		
		if(mebershipTypeCnt < peopleCnt) {
			alert(priceName + ' 인원을 확인 하십시오(가능인원: '+mebershipTypeCnt+' 명)');
			return true;
		}
		return false;
	}
	
	function CheckReser(cnt){
		
		var frm = document.frmStep2;
		var bl = true;
		
		var ApplyType 	= frm.APPLY_TYPE.value;
		var LimitCnt  	= Number(frm.LIMIT_CNT.value);
		var VacancyCnt  = Number(frm.VACANCY_CNT.value);
		var WVacancyCnt = Number(frm.WVACANCY_CNT.value);
		var Cnt			= Number(cnt);
		
		if(LimitCnt == 0){
			LimitCnt = 999;
		}

		if(ApplyType == 'RESER'){
			if(parseInt(VacancyCnt) >= parseInt(LimitCnt)){
				if(LimitCnt >= Cnt){
					bl = true;
				}
				else{
					bl = false;
					peoplesCnt = LimitCnt;
				}
			}
			else{
				if(VacancyCnt >= Cnt){
					bl = true;
				}
				else{
					bl = false;
					peoplesCnt = VacancyCnt;
				}
			}
		}
		else if(ApplyType == 'WAIT'){
			if(WVacancyCnt >= LimitCnt){
				if(LimitCnt >= Cnt){
					bl = true;
				}
				else{
					bl = false;
					peoplesCnt = LimitCnt;
				}
			}
			else{
				if(WVacancyCnt >= Cnt){
					bl = true;
				}
				else{
					bl = false;
					peoplesCnt = WVacancyCnt;
				}
			}
		}
		
		return bl;
	}
	
	// 요금선택 step3
	function StudentChoice(arr, peopleCnt){
		var frm = document.frmStep3;
		var frm2 = document.frmStep2;
		var Str = '총 '+peopleCnt+'명';
		
		frm.DAY.value = frm2.DAY.value;
		frm.PRICE_NBR.value = frm2.PRICE_NBR.value;
		
		$('#Step2Id').text(Str);
		
		$("#step2").removeClass("active");
		$("#step3").addClass("active");
		$("#step2").css("cursor","pointer");
		
		$('#step3').find('.process-title').attr('onclick','step3Slide()');
		$("#step2 .process-contents").slideUp();
		$("#step3 .process-contents").slideDown();
		
		StudentAjax(arr);
	}
	
</script>