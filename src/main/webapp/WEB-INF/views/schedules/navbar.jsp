<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<li class="dropdown">
	<a class="dropdown-toggle" type="button" data-toggle="dropdown" role="button" 
		aria-haspopup="true" aria-expanded="true">
		<spring:message code="${currentCategory }" text=''/><i class="fa fa-angle-down" aria-hidden="true"></i>
	</a>
	<ul class="dropdown-menu">
		<c:set var="categoryNo" value="5"/>
		<c:forEach var="i" begin="1" end="12">
			<spring:message var="sub_category" code="main.nav.catetory${categoryNo}.sub${i}" scope="application" text='' />
			
			<c:if test="${not empty sub_category }">
				<spring:message var="sub_category_url" code="main.nav.catetory${categoryNo}.sub${i}.url" scope="application" text='' />
				<li>
					<c:choose>
						<c:when test="${sub_category_url eq 'scheduleScript' }">
							<spring:message var="academy_cd" code="main.nav.catetory${categoryNo}.sub${i}.academy_cd" scope="application" text='' />
 							<!-- 예약신청의 javascript를 통해 페이지를 들어가야할 경우 판단 -->
 							<a href="javascript:void(0)" onclick="AcademyChoiceFromTopMenu('${academy_cd}')">
		 						<c:out value="${sub_category }"/>
		 					</a>
						</c:when>
						<c:otherwise>
							<a href="<c:url value="${sub_category_url }" />" >
								<c:out value="${sub_category }" />
							</a>	
						</c:otherwise>
					</c:choose>
					
					<spring:message var="sub_child_category" code="main.nav.catetory${categoryNo}.sub${i }.sub1" scope="application" text='' />
					<c:if test="${sub_child_category != '' }">
						<ul class="right-menus">
							<c:forEach var="j" begin="1" end="8">
								<spring:message var="sub_child_category" code="main.nav.catetory${categoryNo}.sub${i}.sub${j}" scope="application" text='' />
								<c:if test="${sub_child_category != '' }">
									<spring:message var="sub_child_category_url" code="main.nav.catetory${categoryNo}.sub${i}.sub${j}.url" scope="application" text='' />
									<li>
										<a href="<c:url value="${sub_child_category_url }" />">
											${sub_child_category } 
										</a>
									</li>
								</c:if>
							</c:forEach>	
						</ul>
					</c:if>
				</li>
			</c:if>
		</c:forEach>
	</ul>
</li>