<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type="text/javascript">

	function princtReceipt(ApplyUniqueNbr){
		$.ajax({
			type : 'get',
			url  : '<c:url value="/payment/paymentPrint"/>?${_csrf.parameterName}=${_csrf.token}',
			data : 'APPLY_UNIQUE_NBR='+ApplyUniqueNbr,
			dataType : 'json',
			success  : function(data){
				if(data.result){
					ReceiptPrint();
				}else{
					alert(data.Msg);
					return;
				}
			}
		});
	}
	
	function ReceiptPrint(){
		var initBody = document.body.innerHtml;
		window.onbeforeprint = function(){
			document.body.innerHTML = document.getElementById('receiptArea').innerHTML;
		}
		
		window.onafterprint = function(){
			document.body.innerHTML = initBody;
		}
		
		window.print();
	}
	
	function ReserConfirm(MemberNo){
		document.location.href = '<c:url value="/payment/"/>';
	}
</script>

	<div class="container">
		

		<div class="row" id="receiptArea">
			<div class="col-md-12">
				<div class="reservation-receipe-wrapper">
					<div class="receipe-header">
						<h4 class="doc-title">예약증</h4>
					</div>

					<div class="receipe-body margin-bottom20">
						<h2 class="doc-title">${CompleteInfo.COURSE_NAME }</h2>
						<h5 class="doc-title">
							<c:if test="${CompleteInfo.SCHEDULE_FLAG eq 'Y' }">
								<fmt:parseDate value="${CompleteInfo.COURSE_DATE }" var="COURSE_DATE" pattern="yyyyMMdd"/>
								<fmt:formatDate value="${COURSE_DATE }" var="CourseDate" pattern="yyyy년 MM월 dd일"/>
								${CourseDate} (${CompleteInfo.COURSE_WEEK_STR})
							</c:if>
							
							<c:if test="${CompleteInfo.SCHEDULE_FLAG eq 'N' }">
								<fmt:parseDate value="${CompleteInfo.COURSE_START_DATE }" var="COURSE_START_DATE" pattern="yyyyMMdd"/>
								<fmt:formatDate value="${COURSE_START_DATE }" var="CourseStartDate" pattern="yyyy년 MM월 dd일"/>
								<fmt:parseDate value="${CompleteInfo.COURSE_END_DATE }" var="COURSE_END_DATE" pattern="yyyyMMdd"/>
								<fmt:formatDate value="${COURSE_END_DATE }" var="CourseEndDate" pattern="yyyy년 MM월 dd일"/>
								${CourseStartDate}~${CourseEndDate} (${CompleteInfo.COURSE_WEEK_STR})
							</c:if>
				
						</h5>
						<h5 class="doc-title">
							${CompleteInfo.PRICE_NAME } 
							<c:if test="${CompleteInfo.COURSE_START_TIME ne '0000' }">
								<fmt:parseDate value="${CompleteInfo.COURSE_START_TIME }" var="COURSE_START_TIME" pattern="HHmm"/>
								<fmt:formatDate value="${COURSE_START_TIME }" var="CourseStartTime" pattern="HH:mm"/>
								<fmt:parseDate value="${CompleteInfo.COURSE_END_TIME }" var="COURSE_END_TIME" pattern="HHmm"/>
								<fmt:formatDate value="${COURSE_END_TIME }" var="CourseEndTime" pattern="HH:mm"/>
								${CourseStartTime}~${CourseEndTime}
							</c:if>
						</h5>
						<h5 class="doc-title">${CompleteInfo.PLACE_INFO}</h5>
					</div>

					<div class="receipe-info">
						<div class="table-responsive">
						  <table class="table lined-table">
						    <tr>
						    	<th>예약구분</th>
						    	<td>${CompleteInfo.CLASS_NAME}</td>
						    	<th>예약코드</th>
						    	<td>${CompleteInfo.APPLY_UNIQUE_NBR}</td>
						    </tr>
						    <tr>
						    	<th>예약일시</th>
						    	<td>
						    		<fmt:parseDate value="${CompleteInfo.RESER_DATE }" var="RESER_DATE" pattern="yyyyMMdd"/>
									<fmt:formatDate value="${RESER_DATE }" var="ReserDate" pattern="yyyy년 MM월 dd일"/>
									<fmt:parseDate value="${CompleteInfo.RESER_TIME }" var="RESER_TIME" pattern="HHmmss"/>
									<fmt:formatDate value="${RESER_TIME }" var="ReserTime" pattern="HH:mm"/>
									${ReserDate } ${ReserTime }
						    	</td>
						    	<th>예약상태</th>
						    	<td>${CompleteInfo.APPLY_STATUS }</td>
						    </tr>
						    <tr>
						    	<th>예약자명(수강생)</th>
						    	<td>${CompleteInfo.STUDENT_NAME}(${CompleteInfo.STUDENT_NAME2})</td>
						    	<th>연락처</th>
						    	<td>${CompleteInfo.MEMBER_CEL }</td>
						    </tr>
						    <tr>
						    	<th>관람인원</th>
						    	<td>${CompleteInfo.APPLY_CNT}명</td>
						    	<th>결제금액</th>
						    	<td>
						    		<fmt:formatNumber value="${CompleteInfo.TOTAL_PRICE}" var="TOTAL_PRICE" />
						    		${TOTAL_PRICE }원
						    	</td>
						    </tr>
						    <tr>
						    	<th>결제방식</th>
						    	<td colspan="3">
						    		<c:if test="${PaymentInfo.PAYMENT_TYPE eq '000002'}">
						    			${PaymentInfo.PAYMENT_TYPE_NAME }
						    		</c:if>
						    		
						    		<c:if test="${PaymentInfo.PAYMENT_TYPE eq '000003'}">
						    			${PaymentInfo.PAYMENT_TYPE_NAME} (${PaymentInfo.ACCOUNT_NO } / ${PaymentInfo.BANK_NAME}) 
						    			<font color="red">${fn:substring(PaymentInfo.LIMIT_RECEIPT_DATE, 0, 4)} 년 
						    			${fn:substring(PaymentInfo.LIMIT_RECEIPT_DATE, 4, 6)} 월 
						    			${fn:substring(PaymentInfo.LIMIT_RECEIPT_DATE, 6, 8)} 일  24시 전까지 입금하셔야 합니다.</font>
						    		</c:if>						    		
						    	</td>
						    </tr>
						  </table>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
<!-- 			<div class="col-md-12"> -->
<!-- 				<h5 class="doc-title center"> -->
<!-- 					예약이 접수되었습니다. -->
<!-- 				</h5> -->
<!-- 				<h4 class="doc-title center teal"> -->
<!-- 					감사합니다. -->
<!-- 				</h3> -->
<!-- 			</div> -->

			<div class="col-md-12 center margin-bottom20">
				<a href="javascript:void(0)" class="btn btn-teal reverse hidden-xs margin-right20" onclick="princtReceipt('${CompleteInfo.APPLY_UNIQUE_NBR}');">
					예약증 인쇄하기
				</a>
				<a href="javascript:void(0)" class="btn btn-teal" onclick="ReserConfirm('${CompleteInfo.MEMBER_NO}')">
					예약내역 바로가기
				</a>
			</div>
		</div>
	</div>