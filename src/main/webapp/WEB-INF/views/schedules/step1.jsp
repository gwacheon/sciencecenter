<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script type="text/javascript" src="<c:url value='/resources/js/calendar.js'/>"></script>
<form name="frmStep1">
	<input type="hidden" name="SEMESTER_CD" id="frmStep1SEMESTER_CD" value="${SEMESTER_CD }" />
	<input type="hidden" name="COMPANY_CD" value="${COMPANY_CD }" />
	<input type="hidden" name="COURSE_CD" id="frmStep1COURSE_CD" value="${COURSE_CD }" />
	<input type="hidden" name="SALE_TYPE" value="${SALE_TYPE }" />
	<input type="hidden" name="DAY" id="frmStep1DAY" value="" />
</form>
<div id="step1" class="row reservation-process active"> 
	<div class="col-md-12">
		<div class="process-wrapper clearfix">
			<div class="process-title" onclick="step1Slide()">
				01. 날짜 / 회차 선택 <div class="right" id="Step1Id"></div>
			</div>
			<div class="process-contents">
				<div class="row">
					<div class="col-md-6">
						<div id="program-calendar-wrapper"></div>
						<div class="reservation-status-infos right">
							<span class="selected"></span> 선택한 날짜
							<span class="info-item available"></span> 예약가능일
							<span class="info-item waited"></span> 접수예정일
						</div>
					</div>
					
					<div class="col-md-6">
						<div id="program-times">
							<table class="table centered-table">
								<thead>
									<tr>
										<th class="center">회차</th>
										<th class="center">시간</th>
										<th class="center">잔여/정원</th>
										<th></th>
									</tr>
								</thead >
								
								<tbody id="priceList">
									<tr>
										<td colspan="3">날짜를 선택하세요.</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	$(document).ready(function(){
		_init('',''); 
	});
	
	// 숫자만
	function setNumber(TargetObj){
		if(TargetObj != null){
			TargetObj.value = TargetObj.value.replace(/[^0-9]/g, '');
		}
		return TargetObj;
	};
	
	// 해당 달의 강좌 날짜 가져 오기
	function DayAjax(day){
		var frm = document.frmStep1;
		frm.DAY.value = day;
		
		$.ajax({
			type : 'get',
			url  : '<c:url value="/schedules/dayList"/>',
			data : {"SEMESTER_CD":$("#frmStep1SEMESTER_CD").val(),"COURSE_CD":$("#frmStep1COURSE_CD").val(),"DAY":$("#frmStep1DAY").val()},
			dataType : 'json', 
			success : function(data){
				EventDay(data.LectureList);	
			}
		});
	}
	 
	// 해당 날짜의 예약가능 상태 표시
	function EventDay(data){
		var cnt = 0;
		
		for(var i=0;i < data.length; i++){
			
			//금일날짜부터 달력에 표기
			var today = new Date();
			var compareToday = new Date(today.getFullYear(), today.getMonth(), today.getDate());
			var dbDate = new Date(data[i].COURSE_DATE.substring(0,4)+"-"+data[i].COURSE_DATE.substring(4,6)+"-"+data[i].COURSE_DATE.substring(6,8));
			
			if(data[i].ABLE_FLAG == 'Y'){
				
				
				if(compareToday<=dbDate){
					if(cnt == 0){
						$('#'+data[i].COURSE_DATE).click();
					}
					$('#'+data[i].COURSE_DATE).addClass('available');
					cnt++;
				}
			}else if (data[i].ABLE_FLAG == 'N'){
				if(compareToday<=dbDate) $('#'+data[i].COURSE_DATE).addClass('waited');
			}
		}
	}

	// 날짜 클릭시 이벤트
	function CurrentDay(dd,Obj){		
		var frm = document.frmStep1;
		
		$('.ptech-date').removeClass("selected");
		$('#'+Obj.id).addClass('selected');
		frm.DAY.value = dd;
		var classHas = $('#'+Obj.id).hasClass('waited');
		var classHas2 = $('#'+Obj.id).hasClass('available');
		
		if(!classHas && classHas2){
			$.ajax({
				type : 'get', 
				url  : '<c:url value="/schedules/priceList"/>',
				data : {"SEMESTER_CD":$("#frmStep1SEMESTER_CD").val(),"COURSE_CD":$("#frmStep1COURSE_CD").val(),"DAY":$("#frmStep1DAY").val()},
				dataType : 'json',
				success : priceList
			});
		} else if(!classHas && !classHas2) {
			$('#priceList').empty();
			return;
		} else{
			alert('예약 가능한 날이 아닙니다.');
			$('#priceList').empty();
			return;
		}
	}
	
	// 날짜 및 회차 리스트
	function priceList(data){
		var html ='';
		$('#priceList').empty();
		
		for(var i=0; i < data.LectureList.length;i++){
			var startTime = data.LectureList[i].COURSE_START_TIME;
			var endTime	  = data.LectureList[i].COURSE_END_TIME;
			
			if(startTime.length == 4){
				startTime = startTime.substring(0,2)+':'+startTime.substring(2);	
			}
			if(endTime.length == 4){
				endTime = endTime.substring(0,2)+':'+endTime.substring(2);
			}
			
			html += '<tr>';
			html += '<td class="center">'+data.LectureList[i].PRICE_NAME+'</td>';
			html += '<td class="center">'+startTime+'~'+endTime+'</td>';
// 			html += '<td class="center">'+data.LectureList[i].RESER_CNT+'/'+data.LectureList[i].VACANCY_CNT+'</td>';
			html += '<td class="center">'+isMinus(data.LectureList[i].COURSE_CAPACITY - data.LectureList[i].RESER_CNT)+'/'+data.LectureList[i].COURSE_CAPACITY+'</td>';
			html += '<td class="center">';
		    
			if(data.LectureList[i].VACANCY_CNT < 1 && data.LectureList[i].WVACANCY_CNT < 1){
		    	html += '<button type="button" class="btn btn-disabled">';
		    	html += '마감';
		    }else{
		    	html += '<button type="button" class="btn btn-primary reserve-btn"'+
		    	' onclick="PriceChoice('+data.LectureList[i].PRICE_NBR+',\''+data.LectureList[i].OWNER_LIMIT+'\',\''+
		    							 data.LectureList[i].AGE_FLAG+'\',\''+data.LectureList[i].AGE_TYPE+'\',\''+
		    							 data.LectureList[i].AGE_LIMIT_MIN+'\',\''+data.LectureList[i].AGE_LIMIT_MAX+'\',\''+
		    							 data.LectureList[i].COURSE_PRICE+'\',\''+data.LectureList[i].LIMIT_CNT+'\',\''+
		    							 data.LectureList[i].VACANCY_CNT+'\',\''+data.LectureList[i].WVACANCY_CNT+'\',\''+
		    							 data.LectureList[i].COURSE_WEEK_STR+'\',\''+data.LectureList[i].PRICE_NAME+'\',\''+
		    							 data.LectureList[i].COURSE_START_TIME+'\',\''+data.LectureList[i].COURSE_END_TIME+'\')">';
		    	html += '선택';
		    }
			
			html += '</button>';
			html += '</td>';
			html += '</tr>';
		}
		
		$('#program-times').attr('style',' '); 
		$(html).appendTo('#priceList');
	}
	
	// 회차 선택
	function PriceChoice(nbr,ownerLimit,ageFlag,ageType,ageLimitMin,ageLimitMax,coursePrice,limitCnt,vacancyCnt,
						WvacancyCnt,courseWeekStr,priceName,courseStartTime,courseEndTime){
		
		var frm = document.frmStep2;
		var courseDate = document.frmStep1.DAY.value;
		var startTime = courseStartTime.substring(0,2)+':'+courseStartTime.substring(2);
		var endTime = courseEndTime.substring(0,2)+':'+courseEndTime.substring(2);
		
		frm.PRICE_NBR.value = nbr; 
		frm.OWNER_LIMIT_FLAG.value = (typeof ownerLimit =='undefined')?"":ownerLimit;
		frm.AGE_FLAG.value = (ageFlag =='undefined')?"":ageFlag;
		frm.AGE_TYPE.value = (ageType =='undefined')?"":ageType;
		frm.AGE_LIMIT_MIN.value = (ageLimitMin =='undefined')?"":ageLimitMin;
		frm.AGE_LIMIT_MAX.value = (ageLimitMax =='undefined')?"":ageLimitMax; 
		frm.COURSE_PRICE.value  = coursePrice;
		frm.LIMIT_CNT.value 	= limitCnt;
		frm.VACANCY_CNT.value 	= vacancyCnt;
		frm.WVACANCY_CNT.value 	= WvacancyCnt;
		frm.DAY.value			= courseDate;
		
		var today = new Date();
		var compareToday = new Date(courseDate.substring(0,4), Number(courseDate.substring(4,6))-1, courseDate.substring(6,8), startTime.substring(0,2));
		
		console.log(today);
		console.log(compareToday);
		if(today > compareToday){
			alert("시간이 지난 프로그램 입니다.");
			return;
		}
		
		// 선택한 회차 String
		var Str = '';
		courseDate = courseDate.substring(0,4)+'년 '+courseDate.substring(4,6)+'월 '+courseDate.substring(6,8)+'일';
		Str = courseDate+' '+courseWeekStr+'요일 / '+priceName+' '+startTime+' ~ '+endTime;
		
		$('#Step1Id').text(Str);
		
		$("#step1").removeClass("active");
		$("#step1 .process-contents").slideUp();
		$("#step1").css("cursor","pointer");
		
		$('#step2').find('.process-title').attr('onclick','step2Slide()');
		$("#step2").addClass("active");
		$("#step2 .process-contents").slideDown();
		
		FamilyAdd();
	}
	
function isMinus(number){
	if(Number(number) < 0) return 0;
	else return number;
}
</script>


