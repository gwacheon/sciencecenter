<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-purple">
		<c:import url="/WEB-INF/views/schedules/detailedNavbar.jsp"/>
	</div>
	
	<div id="schedule-container" class="sub-body">
		<div class="narrow-sub-top schedule">
			<div class="sub-banner-tab-wrapper sub-banner-tab-purple">
				<div class="container">
					<div class="row">
						<div class="
							<c:choose>
								<c:when test="${AcademyClassSize eq '1'}">
									col-xs-6 col-xs-offset-3 col-md-4 col-md-offset-4 
								</c:when>
								<c:when test="${AcademyClassSize eq '4'}">
									col-md-10 col-md-offset-1
								</c:when>
								<c:otherwise>								
									col-md-8 col-md-offset-2
								</c:otherwise>
							</c:choose>
							">
							<ul class="nav nav-tabs">
								<c:forEach items="${AcademyClassList}" var="l" varStatus="status">
									<li class=" 									
										<c:choose>
											<c:when test="${AcademyClassSize eq '1'}">
												col-xs-12 
											</c:when>
											<c:when test="${AcademyClassSize eq '2'}">
												col-xs-12 col-sm-6 col-md-6
											</c:when>
											<c:when test="${AcademyClassSize eq '3'}">
												col-xs-12 col-sm-4 col-md-4
											</c:when>
											<c:when test="${AcademyClassSize eq '4'}">
												col-xs-12 col-sm-3 col-md-3
											</c:when>
											<c:otherwise>
												col-xs-12 col-sm-2 col-md-2
											</c:otherwise>
										</c:choose>
										<c:if test="${ClassCd eq l.CLASS_CD}"> active</c:if>
									">
										<a href="javascript:void(0)" onclick="ChangeClass('${l.CLASS_CD}')" class="item">${l.CLASS_NAME}</a>
									</li>
								</c:forEach>
							</ul>     
						</div>
					</div>			
				</div>
			</div>
		</div>
		<div class="container margin-bottom30">
			<div class="row">
				<div class="col-md-12">
					<h4 class="schedule-title">
						<c:forEach items="${LectureList }" var="LectureList" begin="1" end="1">
							${LectureList.COURSE_NAME }
						</c:forEach>
					</h4>
				</div>
			</div>
			<c:choose>
				<c:when test="${((MEMBERSHIP_TYPE eq 'VB' ||
							MEMBERSHIP_TYPE eq 'VA' ||
							MEMBERSHIP_TYPE eq 'VE' ||
							MEMBERSHIP_TYPE eq 'VD' ||
							MEMBERSHIP_TYPE eq 'VC' ||
							MEMBERSHIP_TYPE eq 'VF' ||
							MEMBERSHIP_TYPE eq 'SV' ||
							MEMBERSHIP_TYPE eq 'SM' || MEMBERSHIP_TYPE_CNT != '0') && AcademyCd eq 'ACD009') || AcademyCd != 'ACD009'}">
					<c:if test="${SCHEDULE_FLAG eq 'Y' }">
						<c:import url="/WEB-INF/views/schedules/step1.jsp"></c:import>
					</c:if>
					<c:if test="${SCHEDULE_FLAG eq 'N' }">
						<c:import url="/WEB-INF/views/schedules/step1_1.jsp"></c:import>
					</c:if>
					<c:if test="${OWNER_LIMIT_FLAG eq 'N' }">
						<c:import url="/WEB-INF/views/schedules/step2.jsp"></c:import>
					</c:if>
					<c:if test="${OWNER_LIMIT_FLAG eq 'Y' }">
						<c:import url="/WEB-INF/views/schedules/step2_1.jsp"></c:import>
					</c:if>
					<c:import url="/WEB-INF/views/schedules/step3.jsp"></c:import>
					<c:import url="/WEB-INF/views/schedules/step4.jsp"></c:import>
				</c:when>
				<c:otherwise>
					<table class="table centered-table schedule-table responsive-table">
					<tr>
						<td style="height:200px;">후원회원 전용 메뉴입니다.</td>
					</tr>
					</table>
				</c:otherwise>
			</c:choose>
		</div>
	</div>
</div>
<form name="frmIndex">
	<input type="hidden" name="ACADEMY_CD" id="ACADEMY_CD" value="${AcademyCd}" />
	<input type="hidden" name="SEMESTER_CD" id="SEMESTER_CD" value="${SEMESTER_CD}" />
	<input type="hidden" name="CLASS_CD" value="${ClassCd}" />
</form>
<script type="text/javascript">
	// 1step
	function step11Slide(){
		$("#step1").addClass("active");
		$("#step1 .process-contents").slideDown();
		
		$("#step2").removeClass("active");
		$("#step2 .process-contents").slideUp();
		$('#step2 .process-title').attr('onclick','');
		
		$("#step3").removeClass("active");
		$("#step3 .process-contents").slideUp();
		$('#step3 .process-title').attr('onclick','');
		
		$("#step4").removeClass("active");
		$("#step4 .process-contents").slideUp();
		$('#step4 .process-title').attr('onclick','');
		
		$("#step1, #step2, #step3, #step4").css("cursor","auto");
		
		$("#Step2Id").empty();
	}
	
	// 2step
	function step1Slide(){
		$("#step1").addClass("active");
		$("#step1 .process-contents").slideDown();
		
		$("#step2").removeClass("active");
		$("#step2 .process-contents").slideUp();
		$('#step2 .process-title').attr('onclick','');
		
		$("#step3").removeClass("active");
		$("#step3 .process-contents").slideUp();
		$('#step3 .process-title').attr('onclick','');
		
		$("#step4").removeClass("active");
		$("#step4 .process-contents").slideUp();
		$('#step4 .process-title').attr('onclick','');
		
		$("#step2, #step3, #step4").css("cursor","auto");
	} 
	
	// 3step
	function step2Slide(){
		$("#step1").removeClass("active");
		$("#step1 .process-contents").slideUp();
		
		$("#step2").addClass("active");
		$("#step2 .process-contents").slideDown();
		
		$("#step3").removeClass("active");
		$("#step3 .process-contents").slideUp();
		$('#step3 .process-title').attr('onclick','');
		
		$("#step4").removeClass("active");
		$("#step4 .process-contents").slideUp();
		$('#step4 .process-title').attr('onclick','');
		
		$("#step3, #step4").css("cursor","auto");
	} 
	
	// 4step
	function step3Slide(){
		$("#step1").removeClass("active");
		$("#step1 .process-contents").slideUp();
		
		$("#step2").removeClass("active");
		$("#step2 .process-contents").slideUp();
		
		$("#step3").addClass("active");
		$("#step3 .process-contents").slideDown();
		
		$("#step4").removeClass("active");
		$("#step4 .process-contents").slideUp();
		$('#step4 .process-title').attr('onclick','');
		
		$("#step4").css("cursor","auto");
	} 
	
	// 숫자만
	function setNumber(TargetObj){
		if(TargetObj != null){
			TargetObj.value = TargetObj.value.replace(/[^0-9]/g, '');
		}
		return TargetObj;
	};
	
	
	
	function AcademyChoice(Cd){
		var frm = document.frmIndex;
		frm.ACADEMY_CD.value = Cd;
		frm.CLASS_CD.value = "";
		frm.action = '<c:url value="/schedules"/>';
		frm.method = 'GET';
		frm.submit();
	}
	
	function ChangeClass(Cd){
		var frm = document.frmIndex;
		frm.CLASS_CD.value = Cd;
		frm.action = '<c:url value="/schedules"/>';
		frm.method = 'GET';
		frm.submit();
	}
	
</script>

<c:import url="/WEB-INF/views/schedules/addUserModal.jsp"/>
<c:import url="/WEB-INF/views/schedules/familyAddModal.jsp"/>
