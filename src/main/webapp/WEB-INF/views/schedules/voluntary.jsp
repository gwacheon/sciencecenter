<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-purple">
		<c:import url="/WEB-INF/views/schedules/detailedNavbar.jsp"/>
	</div>
	
	<div class="sub-body no-sub-child">
		<div id="volunteer-banner" class="narrow-sub-top schedule">
			<div class="sub-banner-tab-wrapper sub-banner-tab-purple">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<ul class="nav nav-tabs">
								<li class="col-xs-6 active">
									<a class="item" href="#intro">신청안내 (학생/일반)</a>
								</li>
								<li class=" col-xs-6">
									<a href="javascript:void(0)" onclick="ChangeClass('CL4001')" class="item">자원봉사 신청(학생)</a>
								</li>
							</ul>
						</div>							
					</div>
				</div>	
			</div>			
		</div>
	</div>
	
	<div class="container">
		<div id="intro">
			<div id="application-guide-student" class="row">
				<div class="col-md-12">
					<h3 class="page-title purple">
						<div class="top-line"></div>
						신청안내(학생)
					</h3>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<ul class="circle-list purple sub-page-title">
						<li>
							학생 자원봉사자 모집 안내
						</li>
					</ul>
				</div>
			</div>
			
			
			<div class="row">
				<div class="col-md-6 voluntary-info">
					<div class="voluntary-info-title">
						<i class="fa fa-chevron-right"></i> 장소 
					</div>
					<div class="voluntary-info-desc">
						국립과천과학관 (중앙홀, 전시관 및 과학교실 등)
					</div>
				</div>
				
				<div class="col-md-6 voluntary-info">
					<div class="voluntary-info-title">
						<i class="fa fa-chevron-right"></i> 봉사기간
					</div>
					<div class="voluntary-info-desc">
						학기중: 주말 및 공휴일 09:30 ~ 15:30<br>
						방학중: 매주 화~일요일(휴관일 제외) 09:30 ~ 15:30
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-6 voluntary-info">
					<div class="voluntary-info-title">
						<i class="fa fa-chevron-right"></i> 모집대상 
					</div>
					<div class="voluntary-info-desc">
						중&middot;고등학생
					</div>
				</div>
				
				<div class="col-md-6 voluntary-info">
					<div class="voluntary-info-title">
						<i class="fa fa-chevron-right"></i> 신청방법
					</div>
					<div class="voluntary-info-desc">
						홈페이지 접수<br>
						<span class="alert-info">* 신청한 본인 이외에는 봉사활동 확인서 발급이 불가하므로 유의해 주시기 바랍니다.</span> 
					</div>
				</div>
			</div>
			
			<div class="row">
			
				<div class="col-md-6 voluntary-info">
					<div class="voluntary-info-title">
						<i class="fa fa-chevron-right"></i> 봉사참여
					</div>
					<div class="voluntary-info-desc">
						봉사 당일 10분 전까지 '본관 1층중앙안내데스크' 앞에 집합<br>
						<span class="alert-info">* 대공원역 5번 출구</span> 
					</div>
				</div>
				<div class="col-md-6 voluntary-info">
					<div class="voluntary-info-title">
						<i class="fa fa-chevron-right"></i> 봉사내용 
					</div>
					<div class="voluntary-info-desc">
						전시관 안전관리, 질서유지 및 체험보조, 교육관 수업보조 등
					</div>
				</div>
				
			</div>
			
			<div class="row">
			
				<div class="col-md-6 voluntary-info">
					<div class="voluntary-info-title">
						<i class="fa fa-chevron-right"></i> 준비물
					</div>
					<div class="voluntary-info-desc">
						본인확인을 위한 학생증은 반드시 지참(본인 확인이 안 될 경우 참여 불가)<br>
						<span class="alert-info">* 접수 후 사전 연락 없이 불참할 경우 향후 제약을 받을 수 있음</span> 
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="voluntary-note">
						<strong class="title">※ 예약시 유의 사항</strong>
						
						<div class="note-item">
							 - 참여 희망일 기준으로 <span class="alert-info">21일 전 낮 12시(정오) 부터</span> 인터넷 신청이 가능합니다.<br/>
							 * 자원봉사자는 <span class="alert-info">단정한 복장</span>으로 참여해주시기 바랍니다.
						</div>
						<div class="note-item">
							 - 봉사 당일 <span class="alert-info">10분전까지 '본관 1층 중앙 안내데스크' 앞</span> 으로 오시기 바람.(봉사시간 시작 이후에 도착한 학생은 봉사 참여 불가)
						</div>
						<div class="note-item">
							 - 신청시에는 참여 <span class="alert-info">학생의 이름, 학교명, 학년, 반, 번호</span>를 반드시 적어주시기 바랍니다. 
						</div>
						<div class="note-item">
							 - 본인확인을 위하여 자원봉사일에  <span class="alert-info">'학생증'</span> 반드시 지참 바랍니다. (본인 확인이 되지 않는 경우는 참여불가) 
						</div>
						<div class="note-item">
							 - 신청확인과 취소는 홈페이지 상단 '마이페이지'에서 가능하며, 과학관 행사 등으로 일부 시간은 조기 마감될 수 있습니다. 
						</div>
						<div class="note-item">
							 * 확인서 발급 : 로그인 <i class="fa fa-arrow-right" aria-hidden="true"></i> 마이페이지 <i class="fa fa-arrow-right" aria-hidden="true"></i> 이력관리 <i class="fa fa-arrow-right" aria-hidden="true"></i> 자원봉사 <i class="fa fa-arrow-right" aria-hidden="true"></i> 해당 봉사활동 일자 우측의 '발급' 클릭 <i class="fa fa-arrow-right" aria-hidden="true"></i> 인쇄  
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<a href="javascript:void(0)" onclick="ChangeClass('CL4001')" class="btn btn-purple">
						학생 자원봉사 신청 하러가기
					</a>
				</div>
			</div>
			
			<div id="application-guide-regular" class="row margin-top30">
				<div class="col-md-12">
					<h3 class="page-title purple">
						<div class="top-line"></div>
						신청안내(일반)
					</h3>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<ul class="circle-list purple sub-page-title">
						<li>
							일반 자원봉사자 모집 안내
						</li>
					</ul>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-6 voluntary-info">
					<div class="voluntary-info-title">
						<i class="fa fa-chevron-right"></i> 모집기간 
					</div>
					<div class="voluntary-info-desc">
						연도별 모집 원칙(단, 결원 발생시 별도 공지)
					</div>
					
					<div class="voluntary-info-title">
						<i class="fa fa-chevron-right"></i> 모집분야
					</div>
					<div class="voluntary-info-desc">
						<div><span class="numbering">01</span> 안내자원봉사자</div>
						<ul class="dash-list non-padding">
							<li>
								활동내용 : 관람안내 및 질서유지, 전시장 안전 등 업무 지원
							</li>
							<li>
								자격 : 별도 제한 없음
							</li>
						</ul>
						<div><span class="numbering">02</span> 전시자원봉사자</div>
						<ul class="dash-list non-padding">
							<li>
								활동내용 : 전시품해설 및 기타 교육, 전시 프로그램 등 보조
							</li>
							<li>
								자격 : 해당 업무능력 평가 후 배치 협의, 해당 분양 전공자 우대
							</li>
						</ul>
					</div>
					<div class="voluntary-info-title">
						<i class="fa fa-chevron-right"></i> 근무형태
					</div>
					<div class="voluntary-info-desc">
						1일 2교대(오전,오후) / 전일 근무 가능(휴관일 제외)<br>
						(오전 09:30~13:30, 오후 13:30~17:30)<br>
						
						<span class="alert-info">* 자원봉사자가 원할 경우, 전일 근무 가능. </span><br>
						<span class="alert-info">* 주말근무 가능자 우선 선발</span><br/>
						<span class="alert-info">* 업무시간은 기관 사정에 따라 변동 가능</span>
					</div>
				</div>
				
				<div class="col-md-6 voluntary-info">
					<div class="voluntary-info-title">
						<i class="fa fa-chevron-right"></i> 신청방법
					</div>
					<div class="voluntary-info-desc">
						<ul class="dash-list non-padding">
							<li>
								제출서류 : 국립과천과학관 자원봉사자 신청서 1부<br>
								(아래 자원봉사자 신청서를 다운 받아 작성 제출)
							</li>
							<li>
								접수방법 : 이메일, 우편 또는 방문 제출 
							</li>
							<li>
								제출처 : 주소 (427-060) 경기도 과천시 상하벌로 110 국립과천과학관
							</li>
							<li>
								이메일 : sunogy@korea.kr
							</li>
							<li>
								팩스  : (02) 3677-1379
							</li>
						</ul>
						<a href="<c:url value="/voluntary/voluntaryForm"/>" class="btn btn-purple">
							일반 자원봉사 신청서 다운로드
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<form name="frmIndex">
	<input type="hidden" name="ACADEMY_CD" value="ACD004" />
	<input type="hidden" name="COURSE_CD" value="" />
	<input type="hidden" name="SEMESTER_CD" value="" />
	<input type="hidden" name="TYPE" value="" />
	<input type="hidden" name="FLAG" value="" />
	<input type="hidden" name="CLASS_CD" value="${ClassCd}" />
</form>
<script type="text/javascript">
	function ChangeClass(Cd){
		var frm = document.frmIndex;
		frm.CLASS_CD.value = Cd;
		frm.action = '<c:url value="/schedules"/>?${_csrf.parameterName}=${_csrf.token}';
		frm.method = 'get';
		frm.submit();
	}
	function AcademyChoice(Cd){
		var frm = document.frmIndex;
		frm.ACADEMY_CD.value = Cd;
		frm.CLASS_CD.value = "";
		frm.action = '<c:url value="/schedules"/>';
		frm.method = 'GET';
		frm.submit();
	}
</script>