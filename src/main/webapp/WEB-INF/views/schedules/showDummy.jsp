<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<form name="frmMain">
	<input type="hidden" name="APPLY_UNIQUE_NBR" value="${APPLY_UNIQUE_NBR}" />
</form>

<script type="text/javascript">
	$(document).ready(function(){
		var rs = '${result}';
		var msg = '${Msg}';
		var frm = document.frmMain;
		console.log(rs);
		if(rs == 'true'){
			frm.action = '<c:url value="/schedules/CompleteProc"/>?${_csrf.parameterName}=${_csrf.token}';
			frm.method = 'post';
			frm.submit();
		}
		else{
			alert(msg);
			document.location.href = '<c:url value="/payment/"/>';
		}
	});
</script>