<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="friend-modal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">수강생 추가</h4>
			</div>
			<div class="modal-body">
				<div>
					<form name="frmAddFriend">
						<table class="table centered-table">
							<tbody>
								<tr>
									<th>이름(<font color="red">*</font>)</th>
									<td><input type="text" name="FRIEND_NAME" id="FRIEND_NAME" class="form-control"/></td>
								</tr>
								<tr>
									<th>성별(<font color="red">*</font>)</th>
									<td class="left">
										<div class="radio radio-primary left">
											<input name="GENDER" type="radio" id="add-user-sex-male" value="M" checked/>
											<label for="add-user-sex-male">남성</label>
											
											<input name="GENDER" type="radio" id="add-user-sex-female" value="F" />
											<label for="add-user-sex-female">여성</label>
									  	</div>
									</td>
								</tr>
								<tr>
									<th>휴대폰(<font color="red">*</font>)</th>
									<td>
										<div class="row">
											<div class="col-xs-4 col-md-3">
												<select id="FRIEND_CEL1" name="FRIEND_CEL1" class="form-control">
													<option value="010" selected>010</option>
													<option value="011">011</option>
													<option value="016">016</option>
													<option value="017">017</option>
												</select>
												<label for="FRIEND_CEL1" style="display: none"></label>
											</div>
											<div class="col-xs-4 col-md-3">
												<input name="FRIEND_CEL2" type="text" id="FRIEND_CEL2"  maxlength="4"  class="form-control" onblur="setNumber(this)" onkeyup="setNumber(this)"/>
												<label for="FRIEND_CEL1" style="display: none"></label>
											</div>
											<div class="col-xs-4 col-md-3">
												<input name="FRIEND_CEL3" type="text" id="FRIEND_CEL3"  maxlength="4" class="form-control" onblur="setNumber(this)" onkeyup="setNumber(this)"/>
												<label for="FRIEND_CEL3" style="display: none"></label>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<th style="padding-right:5px; padding-left:5px;">생년월일(<font color="red">*</font>)</th>
									<td>
										<div class="row">
											<div class="col-xs-4 col-md-3">
												<select id="FRIEND_BIRTH_YEAR" name="FRIEND_BIRTH_YEAR" class="form-control">
													<c:forEach var="i" begin="1950" end="2050" varStatus="loop">
														<c:if test="${YEAR == (loop.end - i + loop.begin) }">
															<option value='${loop.end - i + loop.begin}' selected="selected">${loop.end - i + loop.begin}년</option>
														</c:if>
														
														<c:if test="${YEAR != (loop.end - i + loop.begin) }">
															<option value='${loop.end - i + loop.begin}'>${loop.end - i + loop.begin}년</option>
														</c:if>
													</c:forEach>
												</select>
												<label for="FRIEND_BIRTH_YEAR" style="display: none"></label>
											</div>
											<div class="col-xs-4 col-md-3">
												<select id="FRIEND_BIRTH_MONTH" name="FRIEND_BIRTH_MONTH" class="form-control">
													<c:forEach var="i" begin="1" end="12" varStatus="loop">
														<c:if test="${i < 10 }">
															<option value='0${i }'>0${i }월</option>
														</c:if>
														
														<c:if test="${i > 9 }">
															<option value='${i }'>${i }월</option>
														</c:if>
													</c:forEach>
												</select>
												<label for="FRIEND_BIRTH_MONTH" style="display: none"></label>
											</div>
											<div class="col-xs-4 col-md-3">
												<select id="FRIEND_BIRTH_DAY" name="FRIEND_BIRTH_DAY" class="form-control">
													<c:forEach var="i" begin="1" end="31" varStatus="loop">
														<c:if test="${i < 10 }">
															<option value='0${i }'>0${i }일</option>
														</c:if>
														
														<c:if test="${i > 9 }">
															<option value='${i }'>${i }일</option>
														</c:if>
													</c:forEach>
												</select>
												<label for="FRIEND_BIRTH_DAY" style="display: none"></label>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<th>
										학교
									</th>
									<td class="left">
										<input type="text" name="FRIEND_SCHOOL_NAME" id="FRIEND_SCHOOL_NAME" class="form-control"/>
										<label for="FRIEND_SCHOOL_NAME" style="display: none"></label>
									</td>
								</tr>
								<tr>
									<td class="center" colspan="2">
										<button type="button" class="add-user-btn btn btn-primary" onclick="AddFriend();" id="Friend_Add">
											추가		
										</button>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
					
					<div id="add-users">
						<div>
							<h5 style="float: left; height: 30px;">수강생 목록</h5>
							<div style="margin-left:200px; float:right;"><button type="button" class="btn btn-white" onclick="resetFriend();" style="float:left;">수강생목록 리셋</button></div>
						</div>
						
						<form name="frmRegFriend">
							<input type="hidden" name="SALE_TYPE"  value="${SALETYPE }" />
							<input type="hidden" name="MEMBER_NO"  value="${MEMBER_NO }" />
							<input type="hidden" name="COMPANY_CD" value="${COMPANY_CD }" />
							<input type="hidden" name="FRIEND_NO"  value="" />
							<input type="hidden" name="COURSE_CD"  value="${COURSE_CD }" />
							
							<table class="table centered-table">
								<thead>
									<tr>
										<th class="center">이름</th>
										<th class="center">성별</th>
										<th class="center">휴대폰</th>
										<th class="center">생년월일</th>
										<th class="center">학교</th>
									</tr>
								</thead>
								<tbody id="ADD_FRIEND">
									
								</tbody>
							</table>
						</form>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal" onclick="RegFreind()">저장</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script type="text/javascript">
	
	function AddFriend(){
		var frm = document.frmAddFriend;
		
		var name = frm.FRIEND_NAME.value;
		var genderLen = frm.GENDER.length;
		var gender ='';
		var genderFlag ='';
		var familyTel = '';
		var birth = '';
		var schoolNm = frm.FRIEND_SCHOOL_NAME.value;
		
		for(var i=0; i < genderLen;i++){
			if(frm.GENDER[i].checked){
				gender = (frm.GENDER[i].value =='M')?'남':'여';
				genderFlag = frm.GENDER[i].value;
			}	
		}
		
		if(name == ''){
			alert('이름을 입력해 주세요');
			return;
		}else if(gender == ''){
			alert('성별을 체크해 주세요');
			return;
		}else if(frm.FRIEND_CEL1.value ==''){
			alert('휴대폰 번호를 입력해 주세요');
			return;
		}else if(frm.FRIEND_CEL2.value ==''){
			alert('휴대폰 번호를 입력해 주세요');
			return;
		}else if(frm.FRIEND_CEL3.value == ''){
			alert('휴대폰 번호를 입력해 주세요');
			return;
		}
		
		familyTel = frm.FRIEND_CEL1.value+frm.FRIEND_CEL2.value+frm.FRIEND_CEL3.value;
		birth = frm.FRIEND_BIRTH_YEAR.value+frm.FRIEND_BIRTH_MONTH.value+frm.FRIEND_BIRTH_DAY.value;
		
		AddTrFriend(name,genderFlag,gender,familyTel,birth,schoolNm);
		
		frm.FRIEND_NAME.value = '';
		frm.FRIEND_CEL2.value = '';
		frm.FRIEND_CEL3.value = '';
		frm.FRIEND_SCHOOL_NAME.value = '';
		frm.FRIEND_BIRTH_YEAR.value  = '${YEAR}';
		frm.FRIEND_BIRTH_MONTH.value  = '01';
		frm.FRIEND_BIRTH_DAY.value   = '01';
	}
	function AddTrFriend(name,genderFlag,gender,familyTel,birth,schoolNm){
		var html ='';
		var trLen = $('#ADD_FRIEND').children('tr');
		
		if(familyTel.length == 11){
			familyTel = familyTel.substring(0,3)+'-'+familyTel.substring(3,7)+'-'+familyTel.substring(7);
		}else if(familyTel.length == 11){
			familyTel = familyTel.substring(0,3)+'-'+familyTel.substring(3,6)+'-'+familyTel.substring(6);
		}
		
		var birthRs = birth;
		if(birth.length == 8){
			birth = birth.substring(0,4)+'.'+birth.substring(4,6)+'.'+birth.substring(6);
		}
		
		html += '<tr onclick="FriendModify(\''+name+'\',\''+genderFlag+'\',\''+familyTel+'\',\''+birthRs+'\',\''+schoolNm+'\',this)">';			
		html += '<input type="hidden" name="FRIEND_NM" value="'+name+'"/>'
		html += '<input type="hidden" name="GENDER_FLAG" value="'+genderFlag+'"/>'
		html += '<input type="hidden" name="FRIEND_CEL" value="'+familyTel+'"/>'
		html += '<input type="hidden" name="BIRTHDAY_FRIEND" value="'+birthRs+'"/>'
		html += '<input type="hidden" name="SCHOOL_NM" value="'+schoolNm+'"/>'
		
		html += '<td class="center">'+name+'</td>';
		html += '<td class="center">'+gender+'</td>';
		html += '<td class="center">'+familyTel+'</td>';
		html += '<td class="center">'+birth+'</td>';
		html += '<td class="center">'+schoolNm+'</td>';
		html += '</tr>';
		
		$(html).appendTo('#ADD_FRIEND');
		$('#Friend_Add').text('추가');
	}
	
	function FriendModify(name,genderFlag,failyTel,birthRs,schoolNm,Obj){
		var frm = document.frmAddFriend;
		var tel = failyTel.split("-");
		var html = "변경";
		var birthDate1 = birthRs.substring(0,4);
		var birthDate2 = birthRs.substring(4,6);
		var birthDate3 = birthRs.substring(6);
		
		frm.FRIEND_NAME.value = name;
		$('#FRIEND_CEL1').val(tel[0])
		frm.FRIEND_CEL2.value = tel[1];
		frm.FRIEND_CEL3.value = tel[2];
		$('#FRIEND_BIRTH_YEAR').val(birthDate1);
		$('#FRIEND_BIRTH_MONTH').val(birthDate2);
		$('#FRIEND_BIRTH_DAY').val(birthDate3);
		frm.FRIEND_SCHOOL_NAME.value = schoolNm;
		
		$('#Friend_Add').empty();
		$('#Friend_Add').text(html);
		
		$(Obj).remove();
	}
	
	// 등록
	function RegFreind(){
		var trLen = $('#ADD_FRIEND').children('tr').length;
		
		if(trLen > 0){	
			$.ajax({
				type : 'get',
				url  : '<c:url value="/schedules/RegFriend"/>',
				data : encodeURI($('form[name=frmRegFriend]').serialize()),
				dataType : 'json',
				success : function(data){
					if(data.result){
						alert('성공적으로 등록 되었습니다.');
						$('.close').click();
						$('#ADD_FRIEND').empty();
						FamilList();
					}else{
						alert(data.Msg);
						return;
					}
				}
			});
		}else{
			alert('하나 이상 추가해 주세요.');
			return;
		}
	}
		
	function ResetFriend(){
		 document.frmAddFriend.reset();
	}
	function resetFriend(){
		$("#ADD_FRIEND").empty();
		ResetFriend();
	}
</script>