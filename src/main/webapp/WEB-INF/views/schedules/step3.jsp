<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<form name="frmStep3">
	<input type="hidden" name="COMPANY_CD" value="${COMPANY_CD }" />
	<input type="hidden" name="MEMBER_NO" value="${MEMBER_NO }" />
	<input type="hidden" name="COURSE_CD" value="${COURSE_CD }" />
	<input type="hidden" name="SALE_TYPE" value="${SALE_TYPE }" />
	<input type="hidden" name="FAMILY_NO" value="" />
	<input type="hidden" name="DAY" value="" />
	<input type="hidden" name="PRIOD" value="" />
	<input type="hidden" name="PRICE_NBR" value="" />
	<input type="hidden" name="TOTAL_PRICE" value="" />
	<input type="hidden" name="APPLY_TYPE"	value="${APPLY_TYPE }" />
	<input type="hidden" name="PRICE_CNT" value="" />
	<input type="hidden" name="TARGET_CD" value="" />
	<input type="hidden" name="SCHEDULE_FLAG" value="${SCHEDULE_FLAG }" />
	<input type="hidden" name="COURSE_START_DATE" value=""/>
	<input type="hidden" name="COURSE_END_DATE" value=""/>
	<input type="hidden" name="OPTION_LIST" value=""/>
	<input type="hidden" name="PEOPLE_CNT" value="" />
	<input type="hidden" name="PRICE_NUM" value=""/>
	
	<div id="step3" class="row reservation-process">
		<div class="col-md-12">
			<div class="process-wrapper clearfix">
				<div class="process-title" >
					03. 요금선택 <div class="right" id="Step3Id"></div>
				</div>
				<div class="process-contents" style="display: none">
					<div class="col-xs-12 col-md-8 col-md-offset-2">
						<table class="table centered-table">
							<thead>
								<tr>
									<th>이름</th>
									<th>요금</th>
									<th>총결제금액</th>
								</tr>
							</thead>
							
							<tbody id="user-pricing-infos">
							
							</tbody>
						</table>
					</div>
					
					<div class="col-xs-12">
						<div class="margin-top20 margin-bottom20 center">
							<button type="button" class="btn btn-purple submit-pricing-btn" onclick="ProcReser();">
								예약하기
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

<script type="text/javascript">
	
	var totalPay = 0;
	var onePay = 0;
	function StudentAjax(arr){
		var frm = document.frmStep3;
		var cnt = 0;
		var splitArr;
		var check = 0;
		var priceNums = 0;
		totalPay = 0;
		onePay = 0;
		$('#user-pricing-infos').empty();	
		
		frm.PRICE_CNT.value =  arr.length;
		
		if(arr != ''){
			for(var i=0;i < arr.length;i++){
				splitArr = arr[i].split(",");
				frm.FAMILY_NO.value = splitArr[0];
				frm.TARGET_CD.value = splitArr[1];
				frm.OPTION_LIST.value = splitArr[2];
				
				if(splitArr[3] != undefined) {
					frm.PEOPLE_CNT.value = splitArr[3]; // 인원
					frm.PRICE_NUM.value = splitArr[4];  // 금액 번호
					check = 1;
				}
					
				$.ajax({
					type : 'get',
					url  : '<c:url value="/schedules/StudentPrice"/>?${_csrf.parameterName}=${_csrf.token}',
					data : encodeURI($('form[name=frmStep3]').serialize()),
					dataType : 'json',
					async: false,
					success : function(data){
						
						if(check == 1) {
							if(cnt == 0){
								Step3_1AddTr(data.courseGradeList,data.memberName,arr.length,cnt,data.TargetCd, data.optionList, data.peopleCnt, data.priceNum);
							}else{
								Step3_1AddTr(data.courseGradeList,data.memberName,'',cnt,data.TargetCd, data.optionList, data.peopleCnt, data.priceNum);
							}
							priceNums = data.priceNum;
							
						} else {
							if(cnt == 0){
								Step3AddTr(data.courseGradeList,data.memberName,arr.length,cnt,data.TargetCd, data.optionList);
							}else{
								Step3AddTr(data.courseGradeList,data.memberName,'',cnt,data.TargetCd, data.optionList);
							}
						}
						
					}
				});
				if(splitArr[3] != undefined) {
					$('#PRICE_'+cnt).val(onePay);
					$('#PRICE_DETAIL_NBR_'+cnt).val(priceNums);
				}
				cnt++;
			}
			
			if(splitArr[3] != undefined) {
				$('#total').empty();
				$('#total').append(Comma(totalPay+'')+'원');
				document.frmStep3.TOTAL_PRICE.value = totalPay;
			}
		}
	}
	
	//꼼마,천단위
	function Comma(n){
		var result = String(n);
		return result.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
	};
	
	function Step3_1AddTr(courseGradeList,memberName,len,id,TargetCd, optionList, peopleCnt, priceNum){

		var pay = 0;
		var html ='';
		html += '<tr>';
		html += '<input type="hidden" id="STUDENT_'+id+'" name="STUDENT_'+id+'" value="'+memberName[0].FAMILY_NO+'" />';
		html += '<input type="hidden" id="PRICE_'+id+'" name="PRICE_'+id+'" value="" />';
		html += '<input type="hidden" id="PRICE_DETAIL_NBR_'+id+'" name="PRICE_DETAIL_NBR_'+id+'" value="" />';
		html += '<input type="hidden" id="TARGET_CD_'+id+'" name="TARGET_CD_'+id+'" value="'+TargetCd+'" />'
		html += '<input type="hidden" id="OPTION_LIST_'+id+'" name="OPTION_LIST_'+id+'" value="'+optionList+'" />'
		html += '<input type="hidden" id="PEOPLE_CNT_'+id+'" name="PEOPLE_CNT_'+id+'" value="'+peopleCnt+'" />'
		html += '<td>'+memberName[0].NAME+'</td>';
		html += '<td>';
		html += '<select id="gradePrice_'+id+'" name="gradePrice" onchange="changePrice(\''+memberName[0].FAMILY_NO+'\',this,\''+id+'\','+peopleCnt+'\,0)">';
		html += '<option value="">== 선택 == </option>';
		
		for(var i=0; i < courseGradeList.length;i++){
			var coursePrice;
			if(courseGradeList[i].COURSE_PRICE == 0){
				coursePrice = '무료';
			}
			else{
				coursePrice = Comma(courseGradeList[i].COURSE_PRICE)+'원 ';
			}
			html += '<option value="'+courseGradeList[i].COURSE_PRICE+','+courseGradeList[i].PRICE_DETAIL_NBR+'">'+courseGradeList[i].PRICE_NAME+' ('+coursePrice+')</option>';
			
			if(priceNum == courseGradeList[i].PRICE_DETAIL_NBR) {
				pay = courseGradeList[i].COURSE_PRICE;
				onePay = (courseGradeList[i].COURSE_PRICE * peopleCnt);
				totalPay = parseInt(totalPay) + parseInt((courseGradeList[i].COURSE_PRICE * peopleCnt));
			}
		}
		
		html += '</select>';
		html += '<label for="gradePrice" style="display:none"></label>';
		html += '<span> x '+ peopleCnt + ' 명 = ' + Comma(onePay)+'원 ' + '</span>';
		html += '</td>';
		if(len != ''){
			html += '<td id="total" rowspan="'+len+'">0원</td>';
		}
				
		$(html).appendTo('#user-pricing-infos');
		
		var selectVal = pay + "," + priceNum;
		$("#gradePrice_"+id).val(selectVal);
		$("#gradePrice_"+id).prop( "disabled", true);
	}
	
	function Step3AddTr(courseGradeList,memberName,len,id,TargetCd, optionList){
		
		var html ='';
		html += '<tr>';
		html += '<input type="hidden" id="STUDENT_'+id+'" name="STUDENT_'+id+'" value="'+memberName[0].FAMILY_NO+'" />';
		html += '<input type="hidden" id="PRICE_'+id+'" name="PRICE_'+id+'" value="" />';
		html += '<input type="hidden" id="PRICE_DETAIL_NBR_'+id+'" name="PRICE_DETAIL_NBR_'+id+'" value="" />';
		html += '<input type="hidden" id="TARGET_CD_'+id+'" name="TARGET_CD_'+id+'" value="'+TargetCd+'" />'
		html += '<input type="hidden" id="OPTION_LIST_'+id+'" name="OPTION_LIST_'+id+'" value="'+optionList+'" />'
		html += '<td>'+memberName[0].NAME+'</td>';
		html += '<td>';
		//html += '<select id="gradePrice_'+id+'" name="gradePrice" onchange="changePrice(\''+memberName[0].FAMILY_NO+'\',this,\''+id+'\')">';
		//html += '<select id="gradePrice_'+id+'" name="gradePrice" onchange="changePrice(\''+memberName[0].FAMILY_NO+'\',this,\''+id+'\','+peopleCnt[0]+'\,'+priceNum[0]+'\)">';
		html += '<select id="gradePrice_'+id+'" name="gradePrice" onchange="changePrice(\''+memberName[0].FAMILY_NO+'\',this,\''+id+'\','+1+'\,0)">';
		html += '<option value="">== 선택 == </option>';
		
// 		html += '<option value="0">무료</option>';

		//courseGradeList[i].MEMBERSHIP_TYPE
		
		var memberType = memberName[0].MEMBERSHIP_TYPE;
		var memberTypeValue = '';
		for(var i=0; i < courseGradeList.length;i++){
			var coursePrice;
			if(courseGradeList[i].COURSE_PRICE == 0){
				coursePrice = '무료';
			}
			else{
				coursePrice = Comma(courseGradeList[i].COURSE_PRICE)+'원 ';
			}
			html += '<option value="'+courseGradeList[i].COURSE_PRICE+','+courseGradeList[i].PRICE_DETAIL_NBR+'">'+courseGradeList[i].PRICE_NAME+' ('+coursePrice+')</option>';
			
			var memberTypeTemp = courseGradeList[i].MEMBERSHIP_TYPE;
			if(memberTypeTemp == undefined) memberTypeTemp = 'FR';
			if(memberType == memberTypeTemp) memberTypeValue = courseGradeList[i].COURSE_PRICE+','+courseGradeList[i].PRICE_DETAIL_NBR;
		}
		
		html += '</select>';
		html += '<label for="gradePrice" style="display:none"></label>';
		html += '</td>';
		if(len != ''){
			html += '<td id="total" rowspan="'+len+'">0원</td>';
		}
				
		$(html).appendTo('#user-pricing-infos');
		
		$("#gradePrice_"+id).val(memberTypeValue);
		changePrice(memberName[0].FAMILY_NO, memberTypeValue, id, 1, true);
		
	}
	
	// 요금 선택
	function changePrice(familyNo,Obj,id, peopleCnt, check){
		var selLen = $('form[name=frmStep3] select').length;
		var rs ='';
		var Price;
		var sum = 0;
		var val = Obj.value;
		var splitVal = [];
		
		if(check) {
			splitVal = Obj.split(",");
		} else {
			if(Obj.value.length > 0){ 
				splitVal = val.split(",");
			}
		}
	
		$('#total').empty();		
		for(var i=0;i < selLen;i++){
			Price = $('select[id=gradePrice_'+i+']').val();
			var splitPrice = [];
			
			if(Price.length > 0){
				splitPrice = Price.split(",");
			}
			
			if(Price != ''){
				sum +=Number(splitPrice[0]);
			}
		}

		sum = sum * peopleCnt;
		rs = Comma(sum+'')+'원';
		
		$('#total').append(rs);
		document.frmStep3.TOTAL_PRICE.value = sum;
		$('#PRICE_'+id).val(splitVal[0]);
		$('#PRICE_DETAIL_NBR_'+id).val(splitVal[1]);
	}
	
	// 선택완료
	function ProcReser(){
		var selLen = $('form[name=frmStep3] select').length;
		var rs = 0;
		
		
		for(var i=0; i < selLen;i++){
			if($('#gradePrice_'+i).val() !=''){
				rs++;
			}
		}
		
		if(rs != selLen){
			alert('가격을 선택해 주세요');
			return;
		} 
		
		var Str = document.frmStep3.TOTAL_PRICE.value;
		Str = '요금합계 '+Comma(Str)+'원';
		AlertPop(Str);
	}
</script>