<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="navbar-detailed">
	<div class="sub-category-wrapper">
		<div class="container">
			<ul>
				<c:forEach items="${AcamedyList}" var="AcamedyList">
					<c:if test="${AcademyCd eq AcamedyList.ACADEMY_CD }">
						<li class="sub-category active">
							<a href="javascript:void(0)" class="sub-item" onclick="AcademyChoice('${AcamedyList.ACADEMY_CD}')">
								${AcamedyList.ACADEMY_NAME}
								<c:set value="${AcamedyList.ACADEMY_NAME}" var="AcademyName" /> 
							</a>				
						</li>
					</c:if>
					<c:if test="${AcademyCd ne AcamedyList.ACADEMY_CD }">
						<li class="sub-category">
							<a href="javascript:void(0)" class="sub-item" onclick="AcademyChoice('${AcamedyList.ACADEMY_CD}')">
								${AcamedyList.ACADEMY_NAME}
							</a>				
						</li>
					</c:if>
				</c:forEach>
			</ul>
		</div>
	</div>
</div>