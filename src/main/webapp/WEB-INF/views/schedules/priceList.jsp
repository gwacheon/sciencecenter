<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="program-times" style="display: none">
	<table class="table centered-table">
		<thead>
			<tr>
				<th class="center">회차</th>
				<th class="center">시간</th>
				<th class="center">잔여/인원</th>
				<th></th>
			</tr>
		</thead>
		
		<tbody>
			
		</tbody>
	</table>
</div>