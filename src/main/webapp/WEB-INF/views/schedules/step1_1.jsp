<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<form name="frmStep11">
	<input type="hidden" name="SEMESTER_CD" value="${SEMESTER_CD }" />
	<input type="hidden" name="COMPANY_CD" value="${COMPANY_CD }" />
	<input type="hidden" name="COURSE_CD" value="${COURSE_CD }" />
	<input type="hidden" name="SALE_TYPE" value="${SALE_TYPE }" />
</form>

	<div id="step1" class="row reservation-process active">
		<div class="col-md-12">
			<div class="process-wrapper clearfix">
				<div class="process-title clearfix" onclick="step11Slide()"> 
					01. 회차 선택	<div class="right" id="Step11Id"></div>
				</div>

				<div class="process-contents program-choice">
					<div class="row">
						<div class="col-md-12 table-responsive">
							<table class="table centered-table">
								<thead>
									<tr>
										<th class="center"></th>
										<th class="center">회차</th>
										<th class="center">기간</th>
										<th class="center">시간</th>
										<th class="center">잔여/정원</th>
									</tr>
								</thead >
								
								<tbody id="courseList">
									<c:forEach items="${LectureList }" var="LectureList" varStatus="status">
											<c:if test="${status.index == 0}">
												<input type="hidden" id="existReceiptFlag" value="${LectureList.EXIST_RECEIPT_FLAG}" />
												<input type="hidden" id="existdateFlag" value="${LectureList.EXIST_DATE_FLAG}" />
											</c:if>
											<c:choose>
												<c:when test="${MEMBERSHIP_PAY_CNT > 0 && LectureList.EXIST_RECEIPT_FLAG == 'Y' && LectureList.EXIST_DATE_FLAG != 'N'}">
													<tr>
												</c:when>
												<c:otherwise>
													<tr <c:if test="${LectureList.NEW_DATE_FLAG == 'N'}">style="color:#DCDCDC"</c:if>>
												</c:otherwise>
											</c:choose>
											<td>
											<c:choose>
												<c:when test="${MEMBERSHIP_PAY_CNT > 0 && LectureList.EXIST_RECEIPT_FLAG == 'Y' && LectureList.EXIST_DATE_FLAG != 'N'}">
														<c:if test="${LectureList.EXIST_RECEIPT_FLAG != 'N'}">
															<input type="radio" name="PROGRAM" value="" onclick="LectureChoice('${LectureList.PRICE_NBR}','${LectureList.OWNER_LIMIT_FLAG}',
																															  '${LectureList.AGE_FLAG}','${LectureList.AGE_TYPE}',
																															  '${LectureList.AGE_LIMIT_MIN}','${LectureList.AGE_LIMIT_MAX}',
																															  '${LectureList.COURSE_PRICE}','${LectureList.LIMIT_CNT}',
																															  '${LectureList.COURSE_CAPACITY - LectureList.RESER_CNT}',
																															  '${LectureList.WAIT_CAPACITY - LectureList.WAIT_CNT}',
																															  '${LectureList.COURSE_START_DATE}','${LectureList.COURSE_END_DATE}',
																															  '${LectureList.COURSE_NAME }',this)"/>
														</c:if>
												</c:when>
												<c:otherwise>
													<c:if test="${LectureList.NEW_DATE_FLAG != 'N'}">
															<input type="radio" name="PROGRAM" value="" onclick="LectureChoice('${LectureList.PRICE_NBR}','${LectureList.OWNER_LIMIT_FLAG}',
																															  '${LectureList.AGE_FLAG}','${LectureList.AGE_TYPE}',
																															  '${LectureList.AGE_LIMIT_MIN}','${LectureList.AGE_LIMIT_MAX}',
																															  '${LectureList.COURSE_PRICE}','${LectureList.LIMIT_CNT}',
																															  '${LectureList.COURSE_CAPACITY - LectureList.RESER_CNT}',
																															  '${LectureList.WAIT_CAPACITY - LectureList.WAIT_CNT}',
																															  '${LectureList.COURSE_START_DATE}','${LectureList.COURSE_END_DATE}',
																															  '${LectureList.COURSE_NAME }',this)"/>
														</c:if>
												</c:otherwise>
											</c:choose>
				    						</td>
											<td>${LectureList.PRICE_NAME}</td>
											<td>
												<fmt:parseDate value="${LectureList.COURSE_START_DATE }" var="COURSE_START_DATE" pattern="yyyyMMdd"/>
												<fmt:formatDate value="${COURSE_START_DATE }" var="CourseStartDate" pattern="yyyy.MM.dd"/>
												<fmt:parseDate value="${LectureList.COURSE_END_DATE }" var="COURSE_END_DATE" pattern="yyyyMMdd"/>
												<fmt:formatDate value="${COURSE_END_DATE }" var="CourseEndDate" pattern="yyyy.MM.dd"/>
												${CourseStartDate}~${CourseEndDate}
											</td>
											<td>
												${LectureList.COURSE_WEEK}<br />
												<fmt:parseDate value="${LectureList.COURSE_START_TIME }" var="COURSE_START_TIME" pattern="HHmm"/>
												<fmt:formatDate value="${COURSE_START_TIME }" var="CourseStartTime" pattern="HH:mm"/>
												<fmt:parseDate value="${LectureList.COURSE_END_TIME }" var="COURSE_END_TIME" pattern="HHmm"/>
												<fmt:formatDate value="${COURSE_END_TIME }" var="CourseEndTime" pattern="HH:mm"/>
												${CourseStartTime}~${CourseEndTime}
											</td>
											<td>
												<c:if test="${(LectureList.COURSE_CAPACITY - LectureList.RESER_CNT) <= 0}"> 
													대기자 접수
													<%--	${LectureList.WAIT_CAPACITY - LectureList.WAIT_CNT} / ${LectureList.WAIT_CAPACITY} --%>
												</c:if> 
												<c:if test="${(LectureList.COURSE_CAPACITY - LectureList.RESER_CNT) > 0}"> 
														${LectureList.COURSE_CAPACITY - LectureList.RESER_CNT} / ${LectureList.COURSE_CAPACITY}
												</c:if> 
											</td>
										</tr>
										
									</c:forEach>
								</tbody>
							</table>
							
							<div class="margin-top20 margin-bottom20 center">
								<button class="btn btn-purple member-btn" onclick="LectureStep2()">
									선택 완료
								</button>
							</div>
						</div>
					</div>
				</div>
			</div> 
		</div>
	</div>
	
	<!-- 국립과천과학관 과학교육프로그램 수강규칙 -->
	<div id="program-agree-modal" class="modal fade" tabindex="-1" role="dialog">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">국립과천과학관 과학교육프로그램 수강규칙</h4>
	      </div>
	      <div class="modal-body">
	        <p>
	        	국립과천과학관 과학교육프로그램 수강규칙</p>
</p>
				 1.수강신청</p>
				   ① 수강신청 시 공지된 수업대상, 내용, 일정 등을 반드시 확인하고 신청합니다.</p>
				   ② 수강은 본인의 이름으로 등록해야 하며, 온라인 접수는 실명인증을 하지 않을 경우 신청이 제한 될 수 있습니다.</p>
				   ③ 1인 당 수강신청은 최대 4과목으로 제한하며, 무분별한 수강신청 및 취소의 경우 과학교육관련 프로그램 참가를 제한 받을 수 있습니다.</p>
				   ④ 연간회원 할인은 수강신청 3일 전까지 가입이 완료되어야 합니다.</p>
				   ⑤ 기한 내 교육비를 납부하지 않을 경우 수강신청은 통보 없이 취소됩니다.</p>
				   ⑥ 각 반별 예정인원의 50% 미만 신청 시 폐강됩니다.</p>
				   ⑦ 대상 학년이 아닌 신청자는 수강이 취소될 수 있습니다.</p>
				   ⑧ 본 규칙을 위반한 수강생은 교육 프로그램 참여가 제한될 수 있습니다.</p>
				</p>
				 2.수강 참여</p>
				   ① 수업 분위기를 저해하는 학생은 수강이 취소 될 수 있습니다.</p>
				   ② 수업은 수강생 이외에는 참여할 수 없으며, 수강증은 양도할 수 없습니다.</p>
				   ③ 과학관은 개인 소지품 분실에 대한 책임이 없습니다. 개인물품 관리에 주의 바랍니다.</p>
				   ④ 강의 일정 및 강사는 사정에 따라 변경 될 수 있습니다.</p>
				</p>
				 3.환불</p>
				   ① 결석에 대한 환불은 하지 않습니다.</p>
				   ② 수강신청기간 중에는 홈페이지의 마이페이지에서 수강을 취소할 수 있습니다.</p>
				   ③ 수강신청기간 이후는 국립과천과학관 교육행정실에 환불신청서를 제출하여야 하며(통장사본과 수강증 지참 필수), 환불기준은 아래와 같습니다.</p>
				     - 15일~3개월 이하</p>
				         ☞ 첫 수업 7일전까지 : 전액 환불 가능</p>
				         ☞ 6일전 ~ 1/3차시 수업당일 : 1/3 공제 후 환불</p>
				         ☞ 1/3차시 수업 다음날 ~ 2/3차시 수업 당일 : 2/3 공제 후 환불</p>
				         ☞ 2/3차시 수업 이후 : 환불 불가</p>
				     - 3일~14일</p>
				         ☞ 수업 7일전까지 전액 환불 가능</p>
				         ☞ 수업 6일전 이후는 환불 불가</p>
				     - 1일~3일</p>
				         ☞ 수업 2일전까지 : 전액 환불 가능</p>
				         ☞ 수업 잔날 이후 : 환불 불가</p>
</p>				      
</p>				
				 4.안전관리  『한 번의 실수도 되돌릴 수 없답니다. 항상 조심 조심!』</p>
				   ① 수강생 준수사항</p>
				     - 실험·실습 전</p>
				         ☞ 실험·실습실 입실은 선생님 지시에 따른다</p>
				         ☞ 선생님의 안전사고 예방교육 내용을 숙지한다</p>
				         ☞ 필요 시 정해진 보호장구(실험복, 보호 안경·장갑)를 착용한다</p>
				         ☞ 긴 머리는 묶거나 핀으로 고정하여 늘어지지 않게 한다</p>
				</p>
				     - 실험·실습 중</p>
				         ☞ 피펫은 입으로 빨지 말고 피펫 필러(사진)를 사용한다</p>
				         ☞ 시약은 독성이 있으므로 함부로 맛을 보거나 직접 냄새를 맡지 않는다</p>
				         ☞ 뜨거운 실험기구를 만질 때에는 반드시 집게나 장갑을 사용한다</p>
				         ☞ 시험관을 가열할 때는 윗부분을 사람이 없는 곳으로 향하게 한다</p>
				         ☞ 시약을 조제하여 보관할 때는 라벨(시약명, 농도, 일자, 제조자)을 붙인다</p>
				         ☞ 시약은 반드시 라벨을 확인한 후 필요한 양만 덜어서 사용한다</p>
				         ☞ 시약병은 바닥에 떨어져 깨지지 않도록 시약장에 안전하게 보관한다</p>
				         ☞ 실험실 사고 발생 시 사소한 사항이라도 지도교사에게 알리고 응급조치를 한다</p>
				</p>
				     - 실험·실습 후</p>
				         ☞ 실험이 끝나면 시약, 기구 등을 제자리에 정리·정돈한다</p>
				         ☞ 수도꼭지, 전원, 가스밸브 등을 점검 및 확인한다</p>
				         ☞ 실험·실습실 퇴실은 지도교사 지시에 따른다</p>
				</p>
				   ② 교사/과학관</p>
				        ☞ 지도교사는 학생들의 실험실 출입을 통제하며 지도한다</p>
				        ☞ 실험·실습 시 약품 사용법과 실험실 안전 응급조치법을 교육한 후 실험하도록 지도한다</p>
				        ☞ 실험·실습 중 학생들이 잡담, 장난 등을 하지 않도록 통제한다</p>
				        ☞ 학생이 임의로 시약을 실험실 밖으로 반출하지 않도록 관리한다</p>
				        ☞ 지도교사는 실험이 끝나면 필요한 대장을 정리하고 잠금장치를 확인한 후 퇴실한다</p>
				        ☞ 실험·실습 중 사고발생 시 응급조치 및 구급실 간호사의 조치에 따른다</p>
				        ☞ 과학관은 지독사에게 안전교육을 실시한다</p>
				        ☞ 과학관은 실험폐수, 시약은 별도보관 후 전문기관에 위탁 처리한다</p>
				        ☞ 과학관은 쓰레기통의 쓰레기는 반드시 위험성을 확인한 후 당일 분리 처리한다</p>
				</p>
				   ③ 실험·실습실 사고 응급조치 방법</p>
				     - 화재</p>
				        ☞ 화재발생 시 행동요령에 따라 조치한다</p>
				        ☞ 옷에 불이 붙었을 시 바닥에 엎드려 불이 꺼질 때까지 뒹군다</p>
				     - 화상</p>
				        ☞ 흐르는 찬물에 열기를 식히면서 구급실 간호사의 지시를 받는다</p>
				        ☞ 화상 부위가 넓을 때는 찬 수건으로 감싸고 즉시 의료기관으로 이송한다</p>
				     - 외상</p>
				        ☞ 상처 부위를 깨끗한 물로 씻고 소독된 거즈를 붙인다</p>
				        ☞ 출혈이 심하면 압박·지혈하고 구급실 간호사의 지시를 받는다</p>
				     - 이물질</p>
				        ☞ 유리나 나무 조각이 박히면 움직이지 말고 구급실 간호사의 지시에 따른다</p>
				     - 산이나 염기가 묻었을 경우</p>
				        ☞ 묻은 부위를 물로 씻어낸 후 구급실 간호사의 지시를 받는다</p>
				        ☞ 눈에 약품이 들어간 경우 눈을 비비지 말고, 신속히 물(생리식염수)로 씻어 낸 후 구급실 간호사의 지시를 받는다</p>
				     - 유독가스·휘발성 액체 증기 흡입</p>
				        ☞ 공기가 신선한 곳으로 옮기고 119에 지원요청을 한다</p>
				        ☞ 호흡이나 심장이 정지된 때에는 즉시 심폐소생술을 실시하고, 119에 지원요청 또는 의료기관으로 이송한다</p>
				</p>
				  ※ 안전사고 발생 시 국립과천과학관이 가입한 책임배상보험사를 통해 처리됩니다. 원활한 처리를 위해 안전사고과정 등을 조사자에게 성실히 답해야 합니다.
	        </p>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-purple reverse" data-dismiss="modal">동의안함</button>
	        <button id="program-agree-btn" type="button" class="btn btn-purple">동의</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	
<script type="text/javascript">
	$(function(){
		$(".reservation-warning-btn").click(function(e){
			$("#reservation-warning-wrapper").toggle();
		});
	});
	
	function LectureChoice(nbr,ownerLimit,ageFlag,ageType,ageLimitMin,ageLimitMax,coursePrice,limitCnt,
						vacancyCnt,WvacancyCnt,courseStartDate,courseEndDate,courseName,th){
		
		var frm = document.frmStep2;
		var Str = courseName;		
		
		frm.PRICE_NBR.value = nbr; 
		frm.OWNER_LIMIT_FLAG.value = (typeof ownerLimit =='undefined')?"":ownerLimit;
		frm.AGE_FLAG.value = (ageFlag =='undefined')?"":ageFlag;
		frm.AGE_TYPE.value = (ageType =='undefined')?"":ageType;
		frm.AGE_LIMIT_MIN.value = (ageLimitMin =='undefined')?"":ageLimitMin;
		frm.AGE_LIMIT_MAX.value = (ageLimitMax =='undefined')?"":ageLimitMax; 
		frm.COURSE_PRICE.value  = coursePrice;
		frm.LIMIT_CNT.value 	= limitCnt;
		frm.VACANCY_CNT.value 	= vacancyCnt;
		frm.WVACANCY_CNT.value 	= WvacancyCnt;
		document.frmStep3.COURSE_START_DATE.value = courseStartDate;
		document.frmStep3.COURSE_END_DATE.value = courseEndDate;
		document.frmStep2.EXIST_RECEIPT_FLAG.value = $("#existReceiptFlag").val();
		document.frmStep2.EXIST_DATE_FLAG.value = $("#existdateFlag").val();
		
		var step11IdVal = $(th).parent().next().html() + "    " + $(th).parent().next().next().next().html().replace("<br>","");
		
		$('#Step11Id').text(step11IdVal);
	}
	
	function LectureStep2(){
		$("#program-agree-modal").modal();
	}
	
	$("#program-agree-btn").click(function(e){
		
		e.preventDefault();
		
		$("#program-agree-modal").modal('hide');
		
		var cnt = 0;
		var chk = document.getElementsByName("PROGRAM");
		
		for(var i = 0;i < chk.length;i++){
			if(chk[i].checked){
				cnt++;
			}
		}
		
		if(cnt ==0){
			alert('프로그램을 선택하셔야 합니다.');
			return;
		}
		
		$("#step1").removeClass("active");
		$("#step1 .process-contents").slideUp();
		$("#step1").css("cursor","pointer");
		
		$('#step2').find('.process-title').attr('onclick','step2Slide()');
		$("#step2").addClass("active");
		$("#step2 .process-contents").slideDown();
		
		FamilyAdd();
	});
</script>
	