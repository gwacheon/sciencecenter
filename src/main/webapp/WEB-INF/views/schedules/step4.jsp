<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<form name="frmStep4">
	<input type="hidden" name="APPLY_UNIQUE_NBR" value="" />
	<input type="hidden" name="MEMBER_NO" value="${MEMBER_NO }" />
	<input type="hidden" name="SALE_TYPE" value="${SALE_TYPE }" />
	<input type="hidden" name="TOTAL_PRICE" value="" />
	<input type="hidden" name="APPLY_CNT" value="" />
	<input type="hidden" name="PAYMENT_AMT" value="" />
	<input type="hidden" name="COURSE_NAME" value="" />
	<input type="hidden" name="CARD_APPROVE_DATE" value="" />
	<input type="hidden" name="CARD_APPROVE_NO" value="" />
	<input type="hidden" name="CARD_TRADE_NO" value="" />
	<input type="hidden" name="CARD_NAME" value="" />
	<input type="hidden" name="PAY_KIND" value="" /> 
	<input type="hidden" name="PAY_TYPE" value="" />
	<input type="hidden" name="VACCOUNT" value="" /> 
	<input type="hidden" name="VACCOUNT_BANK_NAME" value="" />
</form>

<div id="step4" class="row reservation-process">
	<div class="col-xs-12">
		<div class="process-wrapper clearfix">
			<div class="process-title">
				04. 결제하기
			</div>
			
			<div class="pay-contents" style="display: none">					
				<div class="col-xs-12">
					<div class="margin-top20 margin-bottom20 center">
						<input type="radio" value="1" name="payKind" id="payKind1" onclick="payType('1')" checked/><lable for="payKind1">신용카드</lable>
						<c:if test="${ClassCd eq 'CL6003'}">
						<input type="radio" value="2" name="payKind" id="payKind2" onclick="payType('2')"/><lable for="payKind2">가상계좌</lable>
						</c:if>
					</div>
				</div>
				
				<div class="col-xs-12">
					<div class="margin-top20 margin-bottom20 center">
						<button type="button" class="btn btn-purple submit-pricing-btn" onclick="doPay();">
							결제하기
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

	<!-- Modal -->
	<div class="modal fade" id="payment-exec" data-backdrop="static" data-keyboard="false"> 
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h4 class="modal-title" id="myModalLabel">예약이 완료되었습니다. 결제 하시겠습니까?</h4>
	      </div>
	      <div class="modal-footer">
	      	<c:if test="${APPLY_TYPE != 'WAIT'}">
	        	<button type="button" class="btn btn-primary" onclick="CourseApplyPay();">결제하기</button>
	        </c:if>
	        <button type="button" class="btn btn-primary" onclick="CompleteProc();">예약내역확인</button>
	      </div>
	    </div>
	  </div>
	</div>
	
<c:import url="/WEB-INF/views/myPayment/paymentModule.jsp"></c:import>

<script type="text/javascript">
//1. 예약완료 후 결제로 넘어갈지 결정. (강좌예약이므로 개별모듈)
function AlertPop(Str){
	var frm = document.frmStep3;
	var totPrice = Comma(frm.TOTAL_PRICE.value);
	
	$('#step1_slide').attr('id','');
	$('#step1_1_slide').attr('id','');
	$('#step2_slide').attr('id','');
	$('#step3_slide').attr('id','');
	
	$.ajax({ 
		type : 'get',
		url  : '<c:url value="/schedules/Reser"/>?${_csrf.parameterName}=${_csrf.token}',
		data : encodeURI($('form[name=frmStep3]').serialize()),
		dataType : 'json',
		success : function(data){
			
			if(data.rs == '0000'){
				
				$('#Step3Id').text(Str);
				
				$('#step1 .process-title').attr('onclick','');
				$('#step2 .process-title').attr('onclick','');
				$('#step3 .process-title').attr('onclick','');
				
				$("#step1").removeClass("active");
				$("#step2").removeClass("active");
				$("#step3").removeClass("active");
				
				$("#step3").removeClass("active");
				$("#step4").addClass("active");
				$("#step3").css("cursor","pointer");
				
				$('#step4').find('.process-title').attr('onclick','step4Slide()');
				$("#step3 .process-contents").slideUp();
				$("#step4 .process-contents").slideDown();
				
				
				var mallForm = document.mallForm;
				
				mallForm.APPLY_UNIQUE_NBR.value = data.ApplyUniqueNbr;
				mallForm.TOTAL_PRICE.value 		= data.TotalPrice;
				mallForm.APPLY_CNT.value 		= data.ApplyCnt;
				mallForm.PAYMENT_AMT.value 		= data.TotalPrice;
				mallForm.COURSE_NAME.value 		= data.CoureseName;
				
				mallForm.productName.value  = data.CoureseName;
				mallForm.productPrice.value = data.TotalPrice;
				mallForm.productCount.value = data.ApplyCnt;
				mallForm.salesPrice.value   = data.TotalPrice;
				mallForm.oid.value   		= data.ApplyUniqueNbr;
				mallForm.mbrId.value   		= "${pay_mbrInfo.get('MBR_ID')}";
				mallForm.mbrName.value   	= "${pay_mbrInfo.get('MBR_NAME')}";
				mallForm.returnUrl.value   	= "${fn:replace(c2_homepage_url,'https','http')}/payresponse/CourseApplyPay";	//리턴URL로 GUBUN:01-강좌
				
// 				console.log('xid -- ' + mallForm.xid.value);
// 				console.log('eci -- ' + mallForm.eci.value);
// 				console.log('cavv -- ' + mallForm.cavv.value);
// 				console.log('cardno -- ' + mallForm.cardno.value);
// 				console.log('hs_useamt_sh -- ' + mallForm.hs_useamt_sh.value);
// 				console.log('encData -- ' + mallForm.encData.value);
// 				console.log('callbackUrl -- ' + mallForm.callbackUrl.value);
// 				console.log('mbrId -- ' + mallForm.mbrId.value);
// 				console.log('mbrName -- ' + mallForm.mbrName.value);
// 				console.log('payKind -- ' + mallForm.payKind.value);
// 				console.log('buyerName -- ' + mallForm.buyerName.value);
// 				console.log('productName -- ' + mallForm.productName.value);
// 				console.log('productPrice -- ' + mallForm.productPrice.value);
// 				console.log('productCount -- ' + mallForm.productCount.value);
// 				console.log('salesPrice -- ' + mallForm.salesPrice.value);
// 				console.log('oid -- ' + mallForm.oid.value);
// 				console.log('openDate -- ' + mallForm.openDate.value);
// 				console.log('bizNo -- ' + mallForm.bizNo.value);
// 				console.log('returnUrl -- ' + mallForm.returnUrl.value);
// 				console.log('hashValue -- ' + mallForm.hashValue.value);
// 				console.log('version -- ' + mallForm.version.value);
// 				console.log('returnType -- ' + mallForm.returnType.value);
// 				console.log('targetUrl -- ' + mallForm.targetUrl.value);
				
				if	(data.TotalPrice == '0') CompleteProc();
				else {$('#payment-exec').modal();}
			}
			else{
				alert(data.RspMsg);
			}
		}
	});
}

//2. 4단계 결제진행 마지막 단계 ([결제하기] 버튼이 있다면 여기서부터 시작)
//ApplyUniqueNbr:예약한 고유의 값
//TotalPrice:전체가격
//ApplyCnt:수량
//CourseName:상품명
function CourseApplyPay(){
	$("#payment-exec").modal('hide');
	$('.pay-contents').show();
}

// 3. 결제 형태(신용카드:num(1), 가상계좌:num(2))
function payType(num){
	var mallForm = document.mallForm;
	mallForm.payKind.value = num;
}




//5. 리턴 URL에서 결과 저장

//7. 콜백함수에서 완료페이지 호출
function CompleteProc(oid){
// 	alert(oid);
	var frm = document.frmStep4;
	frm.APPLY_UNIQUE_NBR.value = document.mallForm.APPLY_UNIQUE_NBR.value;
	frm.action = '<c:url value="/schedules/CompleteProc"/>?${_csrf.parameterName}=${_csrf.token}';
	frm.method = 'post';
	frm.submit();
}

// var popUrl = "/sciencecenter/resources/payment1.3/paymentCallBack_test.jsp";	//팝업창에 출력될 페이지 URL
// var popOption = "width=370, height=360, resizable=no, scrollbars=no, status=no;";    //팝업창 옵션(optoin)
// 	window.open(popUrl,"",popOption);
	
		
	
</script> 