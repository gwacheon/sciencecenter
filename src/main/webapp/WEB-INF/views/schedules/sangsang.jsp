<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav schedules">
		<c:import url="/WEB-INF/views/schedules/detailedNavbar.jsp"/>
	</div>
	
	<div id="schedule-container" class="sub-body">
		<div class="narrow-sub-top schedule">
		</div>
		
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="schedule-sangsang-item">
						<img alt="무한상상실 장비예약바로가기" src="<c:url value='/img/schedule/sangsang/01.png'/>"
							class="over-img">
							
						<img alt="무한상상실 장비예약바로가기" src="<c:url value='/img/schedule/sangsang/image_01.png'/>"
							class="img-responsive item-img">
							
						<div class="link-for-reserve">
							<a href="#" class="btn btn-purple">
								장비예약 바로가기
							</a>
						</div>
						
						<div class="">
							과천과학관에서 진행되고 있는 다양한 전시물들을<br>
							직접 예약하실 수 있습니다.
						</div>
					</div>
				</div>
				
				<div class="col-md-4">
					<div class="schedule-sangsang-item">
						<img alt="무한상상실 작업실 예약바로가기" src="<c:url value='/img/schedule/sangsang/02.png'/>"
							class="over-img">
							
						<img alt="무한상상실 작업실 예약바로가기" src="<c:url value='/img/schedule/sangsang/image_02.png'/>"
							class="img-responsive item-img">
							
						<div class="link-for-reserve">
							<a href="#" class="btn btn-purple">
								작업실 예약 바로가기
							</a>
						</div>
						
						<div class="">
							과천과학관에서 진행되고 있는 다양한<br>
							전시물들을 직접 예약하실 수 있습니다.
						</div>
					</div>
				</div>
				
				<div class="col-md-4">
					<div class="schedule-sangsang-item">
						<img alt="워크숍 예약 바로가기" src="<c:url value='/img/schedule/sangsang/03.png'/>"
							class="over-img">
							
						<img alt="워크숍 예약 바로가기" src="<c:url value='/img/schedule/sangsang/image_03.png'/>"
							class="img-responsive item-img">
							
						<div class="link-for-reserve">
							<a href="#" class="btn btn-purple">
								워크숍 예약  바로가기
							</a>
						</div>
						
						<div class="">
							과천과학관에서 진행되고 있는 다양한 전시물들을<br>
							직접 예약하실 수 있습니다.
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
    	$('select').material_select();
  	});
</script>