<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-blue">
	   <nav id="scrollspy-nav" class="navbar-default navbar-static has-scroll">
			<div class="container">
				<ul class="nav navbar-nav">
					<c:import url="/WEB-INF/views/education/navbar.jsp"/>
					<li>
						<a href="#board-spy" class="item">
							수업게시판
						</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
				    <li>
						<div class="bread-crumbs">
							과학교육 / 수업게시판
						</div>
				    </li>
				</ul>
			</div>
		</nav>
	</div>
	<div id="support-body" class="sub-body">
		<div id="board-spy" class="narrow-sub-top experience">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h4 class="doc-title">
							수업 게시판
						</h4>
						<h6 class="doc-title exp-desc">
							과학교육 수업들에 대한 신청 안내 주제 변경, 강사변경 및 <br>
							특별 교육과정 등 각종 소식을 접하실 수 있습니다.
						</h6>
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<ul id="community-tabs" class="nav nav-tabs">
									<li class="col-xs-12 col-sm-6 col-md-3">
										<a href="<c:url value='/education/educationGuide' />" class="item">
											교육안내
										</a>
									</li>
									<li class="col-xs-12 col-sm-6 col-md-3">
										<a href="<c:url value='/education/educationNotice' />" class="item">
											교육공지
										</a>
									</li>
									<li class="col-xs-12 col-sm-6 col-md-3">
										<a href="<c:url value='/education/refund' />" class="item">
											환불안내
										</a>
									</li>
									<li class="col-xs-12 col-sm-6 col-md-3 active">
										<a href="#" class="item">
											수업게시판
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title blue">
						<div class="top-line"></div>
						수업게시판
					</h3>
				</div>
			</div>
		 	<div class="row">
		 		<div class="col-md-12">
		 			<div id="board-wrapper" class="table-blue">
			 			<c:url var="currentPath" value="/education/classBoard"/>
			 			
			 			<c:import url="/WEB-INF/views/boards/list.jsp">
			 				<c:param name="path" value="${currentPath }"/>
			 			</c:import>
		 			</div>
		 		</div>
			</div>
		</div>
	</div>
</div>