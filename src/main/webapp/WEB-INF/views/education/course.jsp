<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-blue">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>

	<div id="intro-body" class="sub-body">
		<div class="narrow-sub-top education">
			
		</div>
		<div id="edu-exp-body" class="container">
			<div class="row scrollspy">
				<div class="col-md-6 col-md-offset-3">
					<h3 class="program-title">
						프로그램 안내
					</h3>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12 margin-bottom20">
					<ul class="circle-list exp-sub-title text-color blue">
						<li>
							과정안내
						</li>
					</ul>
				</div>
				<div class="col-md-12">
					<ul class="ptech-ul dash-list">
						<li class="strong margin-bottom20">
							운영 프로그램: 미래유망 12개 직업군, 26개 직종
							
							<ul class="ptech-ul non-bullet">
								<li>① 녹색 에너지 <span class="blue">【신재생에너지 연구‧개발자, 에너지 환경 정책연구자】</span></li>
								<li>② 뇌과학 · 뇌공학 <span class="blue">【뇌분석/뇌질환 전문가,
										감성인식기술 연구‧개발자】</span></li>
								<li>③ 로봇 <span class="blue">【로봇 설계‧개발자, 산업용 로봇
										연구‧개발자】</span></li>
								<li>④ 생명 의공학 <span class="blue">【신약개발 전문가, 의료/실험 장비
										전문가】</span></li>
								<li>⑤ 네트워크 <span class="blue">【모바일콘텐츠 연구‧개발자, 네트워크
										시스템 연구‧개발자】</span></li>
								<li>⑥ 생명과학 <span class="blue">【생물정보 분석전문가, 생물자원
										연구‧개발자】</span></li>
								<li>⑦ 나노 융합 신소재 <span class="blue">【나노신소재 연구‧개발자,
										재료/금속/섬유공학 기술자】</span></li>
								<li>⑧ 정보 시스템 <span class="blue">【시스템 소프트웨어 전문가, 시스템
										통합 전문가】</span></li>
								<li>⑨ 항공우주 <span class="blue">【항공(유/무인)기 연구‧개발자,
										위성항법/궤도제어 전문가】</span></li>
								<li>⑩ 헬스 케어 <span class="blue">【U헬스케어 전문가, 헬스케어
										컨설턴트】</span></li>
								<li>⑪ 법조인과 과학 <span class="blue">【(변호사 (변리사), 과학수사대
										(CSI)】</span></li>
								<li>⑫ 문화 콘텐츠 <span class="blue">【방송 종사자, 공연/영화/애니메이션
										종사자, 게임 종사자, 문화콘텐츠 연구‧개발자】</span></li>
							</ul>
						</li>
						
						<li class="strong">
							운영 시간 : 2016.3.8(화) ~ 2016.12.23(금) / 매주 화~금【오전 10:00~12:00, 오후 13:00~15:00】
						</li>
					</ul>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<ul class="circle-list exp-sub-title text-color blue">
						<li>
							참여 대상 및 프로그램 구성
						</li>
					</ul>
					<div class="table-responsive">
						<table class="centered-table table table-blue">
							<thead>
								<tr>
									<th>대상 및 인원</th>
									<th>프로그램 구성</th>
									<th>접수방법</th>
								</tr>
							</thead>
							
							<tbody>
								<tr>
									<td>
										- 대상 : 중학생 단체 (학교 및 동아리)<br>
										- 인원 : 150명 규모
									</td>
									
									<td>
										<div>전문가 특강(45분) + 직업체험 활동(60분)</div>
										<div>
											<a href="<c:url value="/education/course/download/applicationNotice" />" class="btn btn-blue margin-top10">
												<i class="fa fa-download"></i> 프로그램 신청 시 유의사항
											</a>
											
											<a href="<c:url value="/education/course/download/description" />" class="btn btn-blue margin-top10">
												<i class="fa fa-download"></i> 프로그램 상세안내
											</a>
										</div>
									</td>
									
									<td>
										<div>- 집중 및 상시 접수 기간으로 구분하여 접수</div>
										<div>- 이메일 변경으로 인한 접수 일시 중단</div>
										<a href="<c:url value="/education/course/download/schedule" />" class="btn btn-blue margin-top10">
											<i class="fa fa-download"></i> 집중 및 상시 기간 안내
										</a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<ul class="circle-list exp-sub-title text-color blue">
						<li>
							프로그램 참가 절차
						</li>
					</ul>
					<div class="table-responsive">
						<table class="centered-table table table-blue">
							<thead>
								<tr>
									<th>접수 절차</th>
									<th>신청서 양식</th>
								</tr>
							</thead>
							
							<tbody>
								<tr>
									<td>
										<div>
											전화문의(날짜여부문의) <i class="fa fa-chevron-right"></i> 신청서접수(이메일) 
											<i class="fa fa-chevron-right"></i> 신청확인(접수확인메일발)
										</div>
										<div>
											* 동아리경우 선입금 확인 후 접수완료 됨 <i class="fa fa-chevron-right"></i> 수업일 3주전 확정안내문 발송
										</div>
										<div>
											(인원수, 수업일자/예약 내용 변경시 2주전까지 변경신청서 제출)
										</div>
										<div>
											<i class="fa fa-chevron-right"></i> 교육참가 <i class="fa fa-chevron-right"></i> (요청시) 교육비 납부 공문발송
										</div>
									</td>
									
									<td>
										<a href="<c:url value="/education/course/download/experienceApplication"/>" class="btn btn-blue margin-top10">
											<i class="fa fa-download"></i> 신청서 다운로드
										</a>
										
										<a href="<c:url value="/education/course/download/editApplication"/>" class="btn btn-blue margin-top10">
											<i class="fa fa-download"></i> 변경 및 취소 요청서
										</a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<ul class="circle-list exp-sub-title text-color blue">
						<li>
							교육비(선택) : 진로체험(10,000원/명) / 진로체험+상설전시관관람(11,000원/명)
							
							<ul>
								<li>납부방법 : 현장 카드결제 혹은 계좌입금(입금계좌 : 농협 301-0058-6973-21 / 예금주: 국립과천과학관)</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12 margin-bottom20">
					<ul class="circle-list exp-sub-title text-color blue">
						<li>
							개설주제
						</li>
					</ul>
				</div>
				<div class="col-md-12">
					<c:import url="/WEB-INF/views/education/course_topics.jsp"></c:import>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="call-bottom-guide">
						<div class="call-icon blue"><i class="fa fa-phone"></i></div> 
						<div class="desc">
							사전에 프로그램 내용과 주제에 대하여 숙지하신 후 <span class="blue"><b>교육행정실(02-3677-1493)</b></span>
							로 프로그램 구성 및 교육관 시설사용 가능 여부에 대하여 문의하여 주시기 바랍니다.
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>