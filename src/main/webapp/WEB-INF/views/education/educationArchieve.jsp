<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-blue">
	   <nav id="scrollspy-nav" class="navbar-default navbar-static has-scroll">
			<div class="container">
				<ul class="nav navbar-nav">
					<c:import url="/WEB-INF/views/education/navbar.jsp"/>
					<li>
						<a href="#board-spy" class="item">
							과학교육 자료실
						</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
				    <li>
						<div class="bread-crumbs">
							과학교육 / 과학교육 자료실
						</div>
				    </li>
				</ul>
			</div>
		</nav>
	</div>
	<div id="support-body" class="sub-body">
		<div id="board-spy" class="narrow-sub-top education-archieve no-nav-tabs">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h4 class="doc-title">
							과학교육 자료실
						</h4>
						<h6 class="doc-title exp-desc">
							학기별 새로운 과정을 개설하여 어린이들이 즐겁게 과학관의 현장 전시물 앞에서<br>
							생생한 과학의 흥미, 체험, 탐구, 토론 등을 통해 직접 체험할 수 있는 학교 밖 과학교육프로그램
						</h6>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title blue">
						<div class="top-line"></div>
						과학교육 자료실
					</h3>
				</div>
			</div>
		 	<div class="row">
		 		<div class="col-md-12">
		 			<div id="board-wrapper" class="table-blue">
			 			<c:url var="currentPath" value="/education/educationArchieve"/>
			 			
			 			<c:import url="/WEB-INF/views/boards/list.jsp">
			 				<c:param name="path" value="${currentPath }"/>
			 			</c:import>
		 			</div>
		 		</div>
			</div>
		</div>
	</div>
</div>