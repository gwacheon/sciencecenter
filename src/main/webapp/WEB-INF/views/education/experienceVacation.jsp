<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-blue">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>

	<div class="sub-body">
		<div class="narrow-sub-top education">
			<div class="sub-banner-tab-wrapper sub-banner-tab-blue">
			</div>			
		</div>
		<div id="support-sub-nav">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="sub-nav convenience">
							<div class="row">
								<div class="col-xs-12 col-sm-4 col-md-3">
									<a href="<c:url value='/education/educationGuide' />" class="item active">교육안내</a>
								</div>
								<div class="col-xs-12 col-sm-4 col-md-3">
									<a href="<c:url value='/education/educationNotice'/>" class="item active">교육공지</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="edu-exp-body" class="container tab-content">
			<div class="row">
				<div class="col-md-4 col-md-offset-4">
					<h3 class="program-title">
						프로그램 기본 안내
					</h3>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="program-intro-wrapper">
						<div class="intro">
							<div class="header">
								교육대상
							</div>
							<div class="value">
								유아6, 7세(부모) - 초등 6학년
							</div>
						</div>
						<div class="intro">
							<div class="header">
								교육비
							</div>
							<div class="value">
								<div>
									1주일 4회(2시간)기준 60,000원
								</div>
								<div>
									연간회원(접수시점기준) : 교육비 20% 할인적용(단, 할인금액은 2만 5천원을 초과할 수 없음)
								</div>
								<div>
									*연간회원혜택 (교육비할인) 을 받은 경우 연간회원 환불을 받을 수 없습니다.
								</div>
							</div>
						</div>
						<div class="intro">
							<div class="header">
								접수 및 등록
							</div>
							<div class="value">
								<div>
									과학관 홈페이지 선착순접수 (개강 1개월전 과학관 홈페이지 공지)
								</div>
								<div>
									홈페이지 온라인접수 (전화접수 불가능)
								</div>
								<div>
									접수기간, 추가접수기간 이후 등록 불가능
								</div>
							</div>
						</div>
						<div class="intro">
							<div class="header">
								결제
							</div>
							<div class="value">
								<div>
									신용카드 결제
								</div>
								<div>
									무통장입금 (계좌번호: 농협 353-01-018460, 예금주: 국립과천과학관)
								</div>
								<div>
									예약 당일 미결제시 취소
								</div>
							</div>
						</div>
						<div class="intro">
							<div class="header">
								문의
							</div>
							<div class="value">
								<div>
									과학탐구 교육문의 02-3677-1367
								</div>
								<div>
									연간회원 가입문의 02-3677-1551
								</div>
								<div>
									신용카드결제 오류문의 02-6925-2020
								</div>
							</div>
						</div>
						<div class="intro">
							<div class="header">
								교육참여 시 유의 사항
							</div>
							<div class="value">
								교육을 받으러 오실 때 전시관입구 좌측 <b>"교육관"</b> 입구로 입장하시기 바랍니다.
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="education-transportation public-transportation">
						<h4 class="page-sub-title">
							과학관내에서 교육장소 오시는 길
						</h4>
						<h4 class="page-sub-title">
							<i class="fa fa-car"></i> 자가용 이용시
						</h4>
						<div class="location-info-desc">
							<div>
								<span class="how-to-location-item">
									서쪽주차장
								</span>
								<span class="how-to-location-right-arrow"> <i class="fa fa-chevron-right"></i> </span>
								<span class="how-to-location-item">
									중앙 홀 방향
								</span>
								<span class="how-to-location-right-arrow"> <i class="fa fa-chevron-right"></i> </span>
								<span class="how-to-location-item">
									교육관 입구
								</span>
								<span class="how-to-location-right-arrow"> <i class="fa fa-chevron-right"></i> </span>
								<span class="how-to-location-item">
									엘레베이터 이용 3층
								</span>
							</div>
						</div>
						<h4 class="page-sub-title">
							<i class="fa fa-subway"></i> 지하철 이용시
						</h4>
						
						<div class="location-info-desc">
							<div>
								<span class="how-to-location-item">
									지하철4호선 대공원역 5번 출구
								</span>
								<span class="how-to-location-right-arrow"> <i class="fa fa-chevron-right"></i> </span>
								<span class="how-to-location-item">
									중앙홀 입구 좌측의 특별전시관 지나서
								</span>
								<span class="how-to-location-right-arrow"> <i class="fa fa-chevron-right"></i> </span>
								<span class="how-to-location-item">
									교육관 입구
								</span>
								<span class="how-to-location-right-arrow"> <i class="fa fa-chevron-right"></i> </span>
								<span class="how-to-location-item">
									엘레베이터 이용 3층
								</span>
							</div>
						</div>
						
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>