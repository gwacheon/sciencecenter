<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div id="course-topics-wrapper">

</div>

<script id="course-topic-template" type="text/x-handlebars-template">
<div class="panel-group blue margin-bottom30" id="course-topics" role="tablist"
	aria-multiselectable="true">
	{{#each courseTopics}}
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="heading-{{@index}}">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#course-topics"
						href="#collapse-{{@index}}" aria-expanded="true"
						aria-controls="collapse-{{@index}}" class="{{#compare @index "0" operator="!="}}collapsed{{/compare}}">
						{{title}}
					</a>
				</h4>
			</div>
			<div id="collapse-{{@index}}" class="panel-collapse collapse {{#compare @index "0" operator="=="}} in{{/compare}}"
				role="tabpanel" aria-labelledby="heading-{{@index}}">
				{{#each topics}}
				<div class="panel-body">
					<div class="content-wrapper">
						<div class="content-label topics">
							{{title}}
						</div>
						<div class="content-value">
							{{description}}
						</div>
					</div>
				</div>
				{{/each}}
			</div>
		</div>
	{{/each}}
</div>	
</script>


<script type="text/javascript">
	var courseTopics = [
		{
			title: 	"무한상상실",
		    topics: [
		             {
		            	 title: "TRIZ 발명교실(10명/회, 2시간)",
		            	 description : "창의적인 문제해결 방법인 TRIZ와 아이디어 구현 도구인 리틀비츠를 활용한 아이디어 창작 체험"
		             },
		             {
		            	 title: "디지털 제작교실(10명/회, 2시간)",
		            	 description : "3D프린터, 레이저커터중 한 주제를 선택하여 장비의 원리와 기능을 익히면서 제작해보는 디지털 제작 체험"
		             },
		             {
		            	 title: "소프트웨어 교실(10명/회, 2시간)",
		            	 description : "스크래치 기반의 코딩을 익히고, HW(비트브릭, 컵드론)의 물리적인 입출력 장치와 연결, 제어하는 프로그래밍 체험"
		             },
		             {
		            	 title : "영상 제작교실(10명/회, 2시간)",
		            	 description : "시나리오 기획, 촬영, 편집 등 영상제작 과정을 배워 자신의 이야기를 담은 동영상 제작 체험"
		             }
		             ]
		}	,
		{
			title : "천문우주시설",
			topics : [
			          	{
							title : "천체관측장비활용(30명/회, 2시간)",
							description : "우수한 천체관측장비, 실습장비 및 컴퓨터 프로그램을 활용한 천체관측 실습 프로그램"
			          	},
			          	{
			          		title : "천문우주시설 탐방(30명/회, 2시간)",
			          		description : "스페이스월드, 천체투영관, 천체관측소의 우수한 시설과 전시물을 활용한 수준별 맞춤 프로그램"
			          	}
			          ]
			
		},
		{
			title : "곤충생태관",
			topics: [
			         {
			        	 title : "아름다운 곤충표본 관찰(20명/회, 2시간)",
			        	 description : "곤충을 관찰하고 표본을 통하여 형태와 분류, 생태와 습성을 파악하고 기관별 연관성을 유추해보는 프로그램"
			         }
			         ]
		}
	];
	
	var courseTopicsSource = $("#course-topic-template").html();
	var courseTopicsTemplate = Handlebars.compile(courseTopicsSource);
	var courseTopicsHtml = courseTopicsTemplate({courseTopics: courseTopics});
	$("#course-topics-wrapper").html(courseTopicsHtml);
	
</script>