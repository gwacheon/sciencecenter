<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-blue">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>

	<div id="support-body" class="sub-body">
		<div id="basic-info-spy" class="narrow-sub-top education">
			<div class="sub-banner-tab-wrapper sub-banner-tab-blue">
				<div class="container">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							
						</div>
					</div>
				</div>
			</div>			
		</div>
		<div class="container">
			<div class="section scrollspy">
				<div class="row margin-top20">
					<div class="col-md-12">
						<h3 class="page-title blue">
							<div class="top-line"></div>
							교육과정
						</h3>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<ul class="circle-list blue">
							<li>
								단체
							</li>
						</ul>
					</div>
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-blue program-info-table group focusing">
							    <thead>
							        <tr>
							            <th width="12%" class="first">교육명</th>
							            <th width="14%">대상</th>
							            <th>운영 시간</th>
							            <th>교육비</th>
							            <th>장소</th>
							            <th>접수</th>
							        </tr>
							    </thead>
							    <tbody>
							        <tr>
							          <td>
							          	전시물탐구
							          </td>
							          <td>
							          	유아, 초·중·고등학생
							          </td>
							          <td>
							          	1차시 × 2시간(유아 90분), 주중
							          </td>
							          <td>
							          	10, 000원 (유아 5,000원)
							          </td>
							          <td>
							          	교육관, 전시관
							          </td>
							          <td>
							          	연중상시
							          </td>
						            </tr>
						            <tr>
						            	<td>
						            		과학수사대
						            	</td>
						            	<td>
						            		초·중·고등학생
						            	</td>
						            	<td>
						            		1차시 × 2시간, 주중
						            	</td>
						            	<td>
						            		10,000원
						            	</td>
						            	<td>
						            		교육관
						            	</td>
						            	<td>
						            		연중상시
						            	</td>
						            </tr>
						            <tr>
						            	<td>
						            		발명교실
						            	</td>
						            	<td>
						            		초·중·고등학생 
						            	</td>
						            	<td>
						            		1차시 × 2시간, 주중
						            	</td>
					            		<td>
					            			10,000원
						            	</td>
						            	<td>
						            		교육관
						            	</td>
						            	<td>
						            		연중상시
						            	</td>
						            </tr>
						            <tr>
						            	<td>
						            		자유학기제<br>
											(진로탐색)
						            	</td>
						            	<td>
						            		중학생 
						            	</td>
						            	<td>
						            		1차시 × 2시간, 주중
						            	</td>
						            	<td>
						            		10,000원 
						            	</td>
						            	<td>
						            		교육관
						            	</td>
						            	<td>
						            		연중상시
						            	</td>
						            </tr>
						            <tr>
						            	<td>
						            		자유학기제<br>
											(주제선택)
						            	</td>
						            	<td>
						            		중학생
						            	</td>
						            	<td>
						            		1차시 × 2시간, 주중
						            	</td>
						            	<td>
						            		10,000원
						            	</td>
						            	<td>
						            		무한상상실, 천문우주시설, 곤충생태관
						            	</td>
						            	<td>
						            		연중상시
						            	</td>
						            </tr>
					            </tbody>
				            </table>
			            </div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<ul class="circle-list blue">
							<li>
								개인 (학생)
							</li>
						</ul>
					</div>
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-blue program-info-table group focusing">
							    <thead>
							        <tr>
							            <th width="12%" class="first">교육명</th>
							            <th width="14%">대상</th>
							            <th>운영 시간</th>
							            <th>교육비</th>
							            <th>장소</th>
							            <th>접수</th>
							        </tr>
							    </thead>
							    <tbody>
							        <tr>
							          <td>
							          	과학융합탐구
							          </td>
							          <td>
							          	유아, 초등학생
							          </td>
							          <td>
							          	12차시 × 2시간 (유아90분), 주말
							          </td>
							          <td>
							          	180,000원
							          </td>
							          <td>
							          	교육관
							          </td>
							          <td>
							          	2, 5, 8, 11월 (학기과정) / 7, 12월 (방학과정)
							          </td>
						            </tr>
						            <tr>
						            	<td>
						            		스스로 과학탐구
						            	</td>
						            	<td>
						            		초·중학생 
						            	</td>
						            	<td>
						            		12차시 × 2시간, 주말
						            	</td>
						            	<td>
						            		180,000원 
						            	</td>
						            	<td>
						            		교육관
						            	</td>
						            	<td>
						            		2, 5, 8, 11월
						            	</td>
						            </tr>
						            <tr>
						            	<td>
						            		저자와 함께 체험
						            	</td>
						            	<td>
						            		초등학생 
						            	</td>
						            	<td>
						            		12차시 × 2시간, 주말
						            	</td>
					            		<td>
					            			180,000원 
						            	</td>
						            	<td>
						            		교육관
						            	</td>
						            	<td>
						            		2, 5, 8, 11월
						            	</td>
						            </tr>
						            <tr>
						            	<td>
						            		천문탐구
						            	</td>
						            	<td>
						            		초등학생 
						            	</td>
						            	<td>
						            		1차시 × 50분, 매일
						            	</td>
						            	<td>
						            		3,000원 
						            	</td>
						            	<td>
						            		스페이스월드
						            	</td>
						            	<td>
						            		연중상시
						            	</td>
						            </tr>
						            <tr>
						            	<td>
						            		우주융합탐구
						            	</td>
						            	<td>
						            		초·중학생 
						            	</td>
						            	<td>
						            		1차시 × 90분, 주말
						            	</td>
						            	<td>
						            		5,000원 
						            	</td>
						            	<td>
						            		스페이스월드
						            	</td>
						            	<td>
						            		연중상시
						            	</td>
						            </tr>
						            <tr>
						            	<td>
						            		야간별자리 관측
						            	</td>
						            	<td>
						            		초·중·고등학생·성인 
						            	</td>
						            	<td>
						            		1차시 × 2시간, 매일
						            	</td>
						            	<td>
						            		10,000원 
						            	</td>
						            	<td>
						            		천체관측소
						            	</td>
						            	<td>
						            		연중상시
						            	</td>
						            </tr>
					            </tbody>
				            </table>
			            </div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<ul class="circle-list blue">
							<li>
								성인
							</li>
						</ul>
					</div>
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-blue program-info-table group focusing">
							    <thead>
							        <tr>
							            <th width="12%" class="first">교육명</th>
							            <th width="14%">대상</th>
							            <th>운영 시간</th>
							            <th>교육비</th>
							            <th>장소</th>
							            <th>접수</th>
							        </tr>
							    </thead>
							    <tbody>
							        <tr>
							        	<td>
							          		학부모연수
							        	</td>
							        	<td>
							         		학부형 
							        	</td>
							          	<td>
							          		1차시 × 4시간, 주중
							          	</td>
							          	<td>
							          		10,000원 
							          	</td>
							          	<td>
							          		교육관, 전시관
							          	</td>
							          	<td>
							          		4, 9월
							          	</td>
						            </tr>
						            <tr>
						            	<td>
						            		엄마 과학산책
						            	</td>
						            	<td>
						            		학부형  
						            	</td>
						            	<td>
						            		1차시 × 1시간, 주말
						            	</td>
						            	<td>
						            		무료
						            	</td>
						            	<td>
						            		교육관
						            	</td>
						            	<td>
						            		연중상시
						            	</td>
						            </tr>
						            <tr>
						            	<td>
						            		청춘 과학대학
						            	</td>
						            	<td>
						            		60세 이상 어르신  
						            	</td>
						            	<td>
						            		16차시 × 3시간 , 주중
						            	</td>
					            		<td>
					            			180,000원 
						            	</td>
						            	<td>
						            		교육관, 전시관
						            	</td>
						            	<td>
						            		2, 7, 8월
						            	</td>
						            </tr>
					            </tbody>
				            </table>
			            </div>
					</div>
				</div>
				<div class="row margin-top20">
					<div class="col-md-12">
						<h3 class="page-title blue">
							<div class="top-line"></div>
							교육안내
						</h3>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<ul class="circle-list blue">
							<li>
								2016년도 국립과천과학관 과학교육 연간 일정표
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table id="education-yearly-time-table" class="table table-bordered centered-table">
								<thead>
									<tr>
										<td>
										</td>
										<th colspan="4">
											개인정규 (주말)
										</th>
										<th>
											개인방학
										</th>
										<th>
											단체
										</th>
									</tr>
									<tr>
										<td>
										</td>
										<th>
											봄
										</th>
										<th>
											여름
										</th>
										<th>
											가을
										</th>
										<th>
											겨울
										</th>
										<th>
											주중
										</th>
										<th>
											주중
										</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th>
											1월
										</th>
										<td>
										</td>
										<td>
										</td>
										<td>
										</td>
										<td>
										</td>
										<td>
											<ul>
												<li>
													겨울방학 1-3기 (1/5~1/22)
												</li>
											</ul>
										</td>
										<td>
										</td>
									</tr>
									<tr>
										<th>
											2월
										</th>
										<td>
											<ul>
												<li>
													홈페이지 공지(2/16)
												</li>
												<li>
													수강신청접수(2/23~28)
												</li>
											</ul>
										</td>
										<td>
										</td>
										<td>
										</td>
										<td>
											<ul>
												<li>
													겨울학기 종강(17년 2/26)
												</li>
											</ul>
										</td>
										<td>
										</td>
										<td>
											<ul>
												<li>
													단체 접수 시작(2/16 ~)
												</li>
											</ul>
										</td>
									</tr>
									<tr>
										<th>
											3월
										</th>
										<td>
											<ul>
												<li>
													봄학기 개강 (3/5)
												</li>
											</ul>
										</td>
										<td>
										</td>
										<td>
										</td>
										<td>
										</td>
										<td>
										</td>
										<td>
											<ul>
												<li>
													16년 단체 수업 시작 (~ 17년 2월)
												</li>
											</ul>
										</td>
									</tr>
									<tr>
										<th>
											4월
										</th>
										<td>
										</td>
										<td>
										</td>
										<td>
										</td>
										<td>
										</td>
										<td>
										</td>
										<td>
											<ul>
												<li>
													진로체험 집중운영주간 (4/19~5/13)
												</li>
											</ul>
										</td>
									</tr>
									<tr>
										<th>
											5월
										</th>
										<td>
											<ul>
												<li>
													봄학기 종강 (5/29)
												</li>
											</ul>
										</td>
										<td>
											<ul>
												<li>
													홈페이지 공지(5/10)
												</li>
												<li>
													수강신청접수(5/17~22)
												</li>
											</ul>
										</td>
										<td>
										</td>
										<td>
										</td>
										<td>
										</td>
										<td>
										</td>
									</tr>
									<tr>
										<th>
											6월
										</th>
										<td>
										</td>
										<td>
											<ul>
												<li>
													여름학기 개강(6/4)
												</li>
											</ul>
										</td>
										<td>
										</td>
										<td>
										</td>
										<td>
										</td>
										<td>
										</td>
									</tr>
									<tr>
										<th>
											7월
										</th>
										<td>
										</td>
										<td>
										</td>
										<td>
										</td>
										<td>
										</td>
										<td>
											<ul>
												<li>
													여름방학 공지 (7/5)
												</li>
												<li>
													수강신청접수 (7/12~17)
												</li>
											</ul>
										</td>
										<td>
										</td>
									</tr>
									<tr>
										<th>
											8월
										</th>
										<td>
										</td>
										<td>
											<ul>
												<li>
													여름학기 종강 (8/28)
												</li>
											</ul>
										</td>
										<td>
											<ul>
												<li>
													홈페이지 공지 (8/9)
												</li>
												<li>
													수강신천접수 (8/16~21)
												</li>
											</ul>
										</td>
										<td>
										</td>
										<td>
											<ul>
												<li>
													여름방학 1~3기 (7/26~8/12)
												</li>
											</ul>
										</td>
										<td>
										</td>
									</tr>
									<tr>
										<th>
											9월
										</th>
										<td>
										</td>
										<td>
										</td>
										<td>
											<ul>
												<li>
													가을학기 개강(9/3)
												</li>
											</ul>
										</td>
										<td>
										</td>
										<td>
										</td>
										<td>
										</td>
									</tr>
									<tr>
										<th>
											10월
										</th>
										<td>
										</td>
										<td>
										</td>
										<td>
										</td>
										<td>
										</td>
										<td class="text-left">
										</td>
										<td>
											<ul>
												<li>
													진로체험 집중운영주간 (10/11~11/4)
												</li>
											</ul>
										</td>
									</tr>
									<tr>
										<th>
											11월
										</th>
										<td>
										</td>
										<td>
										</td>
										<td>
											<ul>
												<li>
													가을학기 종강 (11/27)
												</li>
											</ul>
										</td>
										<td>
											<ul>
												<li>
													홈페이지 공지(11/8)
												</li>
												<li>
													수강신청접수 (11/15~20)
												</li>
											</ul>
										</td>
										<td>
										</td>
										<td>
										</td>
									</tr>
									<tr>
										<th>
											12월
										</th>
										<td>
										</td>
										<td>
										</td>
										<td>
										</td>
										<td>
											<ul>
												<li>
													겨울학기 개강 (12/3)
												</li>
											</ul>
										</td>
										<td>
										</td>
										<td>
											<ul>
												<li>
													진로체험 집중운영주간 (12/6~12/16)
												</li>
											</ul>
										</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="7">
											* 과학관 사정에 의해 학기별 공지날짜 및 수강신청 접수기간은 변경될 수 있습니다.
											학기별 정확한 일정은 학기 시작 전달에 홈페이지 공지를 통해 확인하실 수 있습니다.
											(홈페이지 -> 과학교육 -> 교육공지)
										</td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>