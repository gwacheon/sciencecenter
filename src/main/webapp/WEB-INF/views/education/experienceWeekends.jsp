<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-blue">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>

	<div class="sub-body">
		<div class="narrow-sub-top education">
			<div class="sub-banner-tab-wrapper sub-banner-tab-blue">
				
			</div>
		</div>
		<div id="support-sub-nav">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="sub-nav convenience">
							<div class="row">
								<div class="col-xs-12 col-sm-4 col-md-3">
									<a href="<c:url value='/education/educationGuide' />" class="item active">교육안내</a>
								</div>
								<div class="col-xs-12 col-sm-4 col-md-3">
									<a href="<c:url value='/education/educationNotice'/>" class="item active">교육공지</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div id="edu-exp-body" class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<h3 class="program-title">
						2016 가을학기 창의체험 과학교육프로그램
					</h3>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<ul class="circle-list text-color blue">
						<li>
							<h5 class="exp-sub-title">
								스스로 과학탐구(플립드 러닝)(4개 주제, 9개 반, 144명)
							</h5>
						</li>
					</ul>

					<table class="table table-blue program-info-table" summary="스스로 과학탐구">
						<colgroup>
							<col style="width: 9%">
							<col style="width: 19%">
							<col style="width: 60%">
						</colgroup>
						<thead>
							<tr>
								<th class="first">대상</th>
								<th class="col">주제명</th>
								<th class="col">학습 내용</th>
							</tr>
						</thead>
						<tbody>
						    <tr>
						        <td>초3~5</td>
						        <td>금메달 스포츠 사이언스</td>
						        <td class="left-article">
						        	“어떻게 스포츠 경기에서 승리할 수 있을까?” 본 수업에서는 스포츠의 과학 원리와 관련된 여러 가지 미션을 수행하면서 다양한 생각들을 이끌어 내고 이를 구조화하는 연습을 진행한다. 실제로 과학자가 된 것처럼 마치 나뭇가지가 뻗어나가듯 생각을 이어나가다 보면 어느새 해답을 발견하게 되고 생각의 폭을 넓힌다.
						        </td>
						    </tr>
						    <tr>
						        <td>초4~6</td>
						        <td>두 얼굴의 빛</td>
						        <td class="left-article">
						        	"빛은 대체 무엇일까?” 인류는 태초부터 빛이 무엇인지에 대해 궁금해 했다. 빛에 대한 탐구의 역사는 물리학의 역사 그 자체이기도 하다. 본 수업에서는 다양한 광학 실험 장치를 이용하여 다각도로 미션을 수행하면서 그동안 알려진 수많은 과학자들의 생각을 이끌어내며 구조화해본다.
						        </td>
						    </tr>
						    <tr>
						        <td>초5~6</td>
						        <td>과학 도약 Ⅳ</td>
						        <td class="left-article">
						        	수업 전에 미리 조사한 내용을 수업 중에 짝과 함께 정리하고 실험을 진행하여 결과를 함께 나누면서 스스로 공부할 수 있는 창의적 능력을 기른다.
						        </td>
						    </tr>
						    <tr>
						        <td>중1~2</td>
						        <td>
						        	중등과학Ⅳ<br>
						        	㉠ 열과 우리생활<br>
						        	㉡ 지구계와 지권<br>
						        	㉢ 소화, 호흡, 순환, 배설<br>
						        	㉣ 일과 에너지
						        </td>
						        <td class="left-article">
						        	과학적 사고력 향상을 위한 수업으로, 수업 전 미션과 실험을 통해 스스로 탐구하는 힘을 기른다.
						        </td>
						    </tr>   
						</tbody>
					</table>
					
					<ul class="circle-list text-color blue">
						<li>
							<h5 class="exp-sub-title">
								과학융합탐구(25개 주제, 49개 반, 805명)
							</h5>						
						</li>
					</ul>

					<table class="table table-blue program-info-table" summary="과학융합탐구">
						<colgroup>
							<col style="width: 9%">
							<col style="width: 19%">
							<col style="width: 60%">
						</colgroup>
						<thead>
							<tr>
								<th class="first">대상</th>
								<th class="col">주제명</th>
								<th class="col">학습 내용</th>
							</tr>
						</thead>
						<tbody>
						    <tr>
							    <td>6~7세</td>
							    <td>싱싱~ 신나는 생활과학탐구!</td>
							    <td class="left-article">의식주에 담겨있는 과학원리를 알고, 우리 생활 속에 숨어있는 또 다른 예를 찾아 자기 생각을 표현해 본다.</td>
							</tr>
							<tr>
							    <td>6~7세</td>
							    <td>동화 속, 과학 쏙! Ⅱ</td>
							    <td class="left-article">동화 속에서 과학이야기를 찾아내어 과학적 현상을 이해하고 생활에 적용할 수 있는 힘을 기른다.</td>
							</tr>
							<tr>
							    <td>6~7세</td>
							    <td>똑똑~ 수학아 뭐 할까?</td>
							    <td class="left-article">체험하고 탐색하는 과정을 수학의 기본 원리와 개념을 깨우치고 창의적인 사고능력을 기른다.</td>
							</tr>
							<tr>
							    <td>6~7세</td>
							    <td>신기한 곤충나라 Ⅲ</td>
							    <td class="left-article">우리주변에서 흔히 만날 수 있는 곤충들을 직접 관찰하고, 몸을  움직이는 놀이 활동과 오감을 자극하는 다양한 만들기 활동 체험을 한다.</td>
							</tr>
							<tr>
							    <td>초1~2</td>
							    <td>과학 호기심</td>
							    <td class="left-article">과학에 대한 호기심을 느낄 수 있는 수업으로, 암석과 지각변동, 지구생명체, 관성과 빛, 물질의 특성 등을 관찰 및 실험활동을 통해 알아본다.</td>
							</tr>
							<tr>
							    <td>초1~2</td>
							    <td>생명탐험교실</td>
							    <td class="left-article">생물 다양성에 기초하여 다양한 동ㆍ식물에 대한 호기심을 해결하기 위하여 학생들이 직접 관찰과 실험, 융합적인 체험활동을 해본다.</td>
							</tr>
							<tr>
							    <td>초1~2</td>
							    <td>생물박사! 생명지킴이</td>
							    <td class="left-article">다양한 동물과 식물의 구조, 특징, 생활방식 등을 알아보고 각 생물들을 여러 기준에 맞추어 분류해보면서 생물 다양성의 의미와 생명의 소중함을 알아본다.</td>
							</tr>
							<tr>
							    <td>초1~3</td>
							    <td>파브르 탐험대 Ⅲ</td>
							    <td class="left-article">곤충을 관찰하고 탐구하여 과학적 상상력을 키우고 토론과 발표수업을 통해 자기주도적  학습능력을 키울 수 있으며 스스로 준비하는 과정에서 자존감을 높여주는 결과를 얻을 수 있다.</td>
							</tr>
							<tr>
							    <td>초2~3</td>
							    <td>플레이 사이언스</td>
							    <td class="left-article">생활 속 과학원리를 실험하고, 과학사, 스토리텔링을 통해 접근하면서 한편의 과학연극으로 제작 발표해 본다.</td>
							</tr>
							<tr>
							    <td>초1~4</td>
							    <td>생각을 펼치는 수학</td>
							    <td class="left-article">과학관 전시물을 통해 우리주변에 있는 수학에 대해 알아보고, 관련된 교구를 만들어 봄으로써 수학적 흥미와 탐구능력, 창의적 문제 해결력을 기른다.</td>
							</tr>
							<tr>
							    <td>초1~4</td>
							    <td>맛있는 실험실</td>
							    <td class="left-article">우리 주변에서 일어나는 여러 가지 과학현상 및 변화를 쉽게 접할 수 있는 식재료를 이용하여 과학적 호기심을 해결해보고, 터득한 과학 원리를 응용할 수 있는 사고력을 키운다.</td>
							</tr>
							<tr>
							    <td>초2~4</td>
							    <td>우주로 꿈을 쏜다</td>
							    <td class="left-article">인류는 오래전부터 우주에 대한 호기심을 가져왔다. 무한우주를 꿈꾸는 인류가 우주 탐사를 진행하면서 어떤 노력을 하고 있는지 알아본다. </td>
							</tr>
							<tr>
							    <td>초3~4</td>
							    <td>원리를 찾아라!</td>
							    <td class="left-article">공기, 소리, 빛과 관련된 생활 속 물건들의 숨어있는 과학 원리를 찾고 창작아이디어로 새롭게 디자인해 본다.</td>
							</tr>
							<tr>
							    <td>초3~4</td>
							    <td>과학 탐험</td>
							    <td class="left-article">과학의 원리를 탐구하고 탐험할 수 있는 수업으로, 지구와 달, 우리 몸, 기초회로와 빛, 용액과 용해도 등을 이해하기 위한 관찰 및 실험활동을 해본다.</td>
							</tr>
							<tr>
							    <td>초3~4</td>
							    <td>지구생각! 기후변화교실</td>
							    <td class="left-article">자연사관에서 시작하여 기초과학관, 첨단과학관을 아우르며 기후의 기본 과학적 원리와 기후변화 현상과 원인, 대책에 대해 알아본다.</td>
							</tr>
							<tr>
							    <td>초2~5</td>
							    <td>과학의 이론으로 미술에 접근하라!</td>
							    <td class="left-article">명화를 통해 그림을 이해하는 폭을 넓히고 화가의 생각이나 당시 과학의 이해도를 알아보고 그림 속에서 과학원리를 찾아 미술과 과학의 융합사고력을 키운다.</td>
							</tr>
							<tr>
							    <td>초4~5</td>
							    <td>과학 집중</td>
							    <td class="left-article">과학을 집중적으로 탐구할 수 있는 수업으로, 지구환경에너지, 생명과학, 전기, 화학반응 등을 이해하기 위한 관찰 및 실험활동을 해본다.</td>
							</tr>
							<tr>
							    <td>초4~5</td>
							    <td>세포 속으로</td>
							    <td class="left-article">생명공학의 기본이 되는 유전자의 개념과 유전물질의 전달, 발생과정을 탐구해본다.</td>
							</tr>
							<tr>
							    <td>초3~6</td>
							    <td>CSI 과학수사대</td>
							    <td class="left-article">실제 과학수사 장비를 이용하여 과학수사의 과정을 직접 체험하고 과학 수사 안에 숨어있는 과학의 원리를 이해한다.</td>
							</tr>
							<tr>
							    <td>초3~6</td>
							    <td>노벨상과 함께 크는 과학의 꿈</td>
							    <td class="left-article">인류에게 가장 큰 유익을 가져다 준 사람에게 주어진다는 노벨상에 대하여 분야별, 시대별  수상한 과학자들을 알아보고 그들의 연구과정을 체험해본다.</td>
							</tr>
							<tr>
							    <td>초4~6</td>
							    <td>게임으로 배우는 수학 스트레칭</td>
							    <td class="left-article">다양한 게임의 원리를 이해하고 실행하여 수학적 문제 해결력을 향상시킬 수 있고, 게임에 담긴 원리를 응용하여 나만의 새로운 게임을 창의적으로 제작해 본다.</td>
							</tr>
							<tr>
							    <td>초4~6</td>
							    <td>사물을 움직이는 코딩 교실</td>
							    <td class="left-article">컴퓨터 과학 원리를 사용하여 문제를 해결하고 창의력을 증진시키고, 사고의 순서와 흐름을 이해하는 실습을 통해 코딩에 대한 연습과 논리적인 사고력을 기른다.</td>
							</tr>
							<tr>
							    <td>초4~6</td>
							    <td>골드버그교실</td>
							    <td class="left-article">골드버그 장치의 배경 이론과 원리를 이해하고 실제 제작하는 과정을 통하여 창의적 사고능력 증진과 창의적 사고 훈련을 경험해본다.</td>
							</tr>
							<tr>
							    <td>초4~6</td>
							    <td>나도 할 수 있어요! 스마트폰 앱 만들기</td>
							    <td class="left-article">스마트폰의 기능과 여러 가지 어플리케이션에 대하여 이해하고, 앱 인벤터를 사용하여 안드로이드 스마트폰에서 구동되는 다양한 앱을 개발해본다.</td>
							</tr>
							<tr>
							    <td>초4~6</td>
							    <td>로봇을 이용한 프로그래밍 교실</td>
							    <td class="left-article">알고리즘이 무엇인지 알고, 문제 해결을 위한 순서도를 작성할 수 있다. 햄스터 로봇과 스크래치를 이용하여 컴퓨터 프로그래밍을 배우고, 문제 해결에 대한 컴퓨팅 사고력을 키운다.</td>
							</tr>
						</tbody>
					</table>

					<ul class="circle-list text-color blue">
						<li>
							<h5 class="exp-sub-title">
								저자와 함께 체험(3개 반 60명)
							</h5>
						</li>
					</ul>
					<table class="table table-blue program-info-table" summary="저자와 함께 체험">
						<colgroup>

							<col style="width: 25%">
							<col style="width: 6%">
							<col style="width: 57%">

						</colgroup>
						<thead>
							<tr>
								<th class="first">과정명</th>
								<th class="col">대상</th>
								<th class="col">학습 내용</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="first">저자와 함께 체험</td>
								<td class="txtL">초3~6</td>
								<td class="left-article">우수과학도서를 과학교육과 접목시켜 저자 직강과 연계 체험활동을 통해 흥미를
									유발하여 과학도서 읽기로 연결함으로써 과학에 대한 깊은 이해유도</td>
							</tr>

						</tbody>
					</table>

					<ul class="circle-list text-color blue">
						<li>
							<h5 class="exp-sub-title">
								엄마의 과학관 산책(1개반 20명)
							</h5>
						</li>
					</ul>

					<table class="table table-blue program-info-table" summary="엄마 과학 산책">
						<colgroup>
							<col style="width: 25%">
							<col style="width: 6%">
							<col style="width: 57%">
						</colgroup>
						<thead>
							<tr>
								<th class="first">과정명</th>
								<th class="col">대상</th>
								<th class="col">학습 내용</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="first">엄마 과학 산책</td>
								<td class="txtL">성인</td>
								<td class="left-article">자녀들의 과학교육에 과학관을 활용하고자 하는 학부모들을 위해 과학관 전시물과
									연계한 전시해설, 과학체험, 전문가 특강 등을 진행(무료)</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-md-offset-4">
					<h3 class="program-title">
						프로그램 기본 안내
					</h3>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="program-intro-wrapper">
						<div class="intro">
							<div class="header">
								교육대상
							</div>
							<div class="value">
								유아6, 7세(부모) ~ 초등 6학년 / 중등1~2
							</div>
						</div>
						<div class="intro">
							<div class="header">
								교육비
							</div>
							<div class="value">
								<div>
									3개월간 12회(2시간)기준 180,000원
								</div>
								<div>
									연간회원(접수시점기준): 교육비 20% 할인적용(단, 할인금액은 2만 5천원을 초과할 수 없음)
								</div>
								<div>
									※ 연간회원혜택(교육비할인)을 받은 경우 연간회원 환불을 받을 수 없습니다.
								</div>
							</div>
						</div>
						<div class="intro">
							<div class="header">
								접수 및 등록
							</div>
							<div class="value">
								<div>
									과학관 홈페이지 분기별 선착순접수 (개강 1개월전 과학관 홈페이지 공지)
								</div>
								<div>
									홈페이지 온라인접수 (전화접수 불가능)
								</div>
								<div>
									접수기간, 추가접수기간 이후 등록 불가능
								</div>
							</div>
						</div>
						<div class="intro">
							<div class="header">
								결제
							</div>
							<div class="value">
								<div>
									신용카드 결제
								</div>
								<div>
									무통장입금 (계좌번호: 농협 353-01-018460, 예금주: 국립과천과학관)
								</div>
								<div>
									예약 당일 미결제시 취소
								</div>
							</div>
						</div>
						<div class="intro">
							<div class="header">
								문의
							</div>
							<div class="value">
								<div>
									과학탐구 교육문의 02-3677-1367
								</div>
								<div>
									연간회원 가입문의 02-3677-1551
								</div>
								<div>
									신용카드결제  02-6925-2020
								</div>
							</div>
						</div>
						<div class="intro">
							<div class="header">
								교육참여 시 유의 사항
							</div>
							<div class="value">
								교육을 받으러 오실 때 전시관입구 좌측 <b>"교육관"</b> 입구로 입장하시기 바랍니다.
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="education-transportation public-transportation">
						<h4 class="page-sub-title">
							과학관내에서 교육장소 오시는 길
						</h4>
						<h4 class="page-sub-title">
							<i class="fa fa-car"></i> 자가용 이용시
						</h4>
						<div class="location-info-desc">
							<div>
								<span class="how-to-location-item">
									서쪽주차장
								</span>
								<span class="how-to-location-right-arrow"> <i class="fa fa-chevron-right"></i> </span>
								<span class="how-to-location-item">
									중앙 홀 방향
								</span>
								<span class="how-to-location-right-arrow"> <i class="fa fa-chevron-right"></i> </span>
								<span class="how-to-location-item">
									교육관 입구
								</span>
								<span class="how-to-location-right-arrow"> <i class="fa fa-chevron-right"></i> </span>
								<span class="how-to-location-item">
									엘레베이터 이용 3층
								</span>
							</div>
						</div>
						<h4 class="page-sub-title">
							<i class="fa fa-subway"></i> 지하철 이용시
						</h4>
						
						<div class="location-info-desc">
							<div>
								<span class="how-to-location-item">
									지하철4호선 대공원역 5번 출구
								</span>
								<span class="how-to-location-right-arrow"> <i class="fa fa-chevron-right"></i> </span>
								<span class="how-to-location-item">
									중앙홀 입구 좌측의 특별전시관 지나서
								</span>
								<span class="how-to-location-right-arrow"> <i class="fa fa-chevron-right"></i> </span>
								<span class="how-to-location-item">
									교육관 입구
								</span>
								<span class="how-to-location-right-arrow"> <i class="fa fa-chevron-right"></i> </span>
								<span class="how-to-location-item">
									엘레베이터 이용 3층
								</span>
							</div>
						</div>
						
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>