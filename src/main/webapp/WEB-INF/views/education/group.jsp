<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-blue">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>

	<div id="intro-body" class="sub-body">
		<div id="education-group-banner" class="narrow-sub-top education">
			
		</div>
		<div id="edu-exp-body" class="container">
			<div class="row scrollspy">
				<div class="col-md-6 col-md-offset-3">
					<h3 class="program-title">
						프로그램 안내
					</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<ul class="circle-list text-color blue">
						<li>
							<h5 class="exp-sub-title">
								과정안내
							</h5>
						</li>
					</ul>
					
					<div class="table-responsive">
						<table class="table table-bordered centered-table">
							<thead>
								<tr>
									<th>구분</th>
									<th>과정내용</th>
									<th>운영시간</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										전시물탐구
									</td>
									<td>
										과학관 내 4천여개의 우수한 전시물을 테마로 만들기와 실험이 함께하는 융합체험교실
									</td>
									<td rowspan="3">
										화요일 ~ 토요일<br>
										오전 10:00~12:00<br>
										낮 13:00~15:00<br>
										오후 15:30~17:30<br>
										(토요일은 오전만 운영)
									</td>
								</tr>
								<tr>
									<td>
										과학수사대
									</td>
									<td>
										첨단 과학수사(CSI)장비를 활용하여 실제 과학수사 원리를 배워보는 체험교실
									</td>
								</tr>
								<tr>
									<td>
										발명교실
									</td>
									<td>
										다양한 발명원리를 만들기를 통해 습득하고 창의적인 아이디어를 발명·토론하는 미래형 교실
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<div class="table-responsive">
						<table class="table table-blue program-info-table">
							<thead>
								<tr>
									<th>반별구성</th>
									<th>주제구성</th>
									<th>접수안내</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										6,7세 유아, 초등~고등학생(단체)<br>
										1반 15명 정원(과학수사대, 발명교실 20명 정원)<br>
										※주중(화~금) 동시 최대 12개 반(180명) 개설 가능<br>
										단, 토요일 3개 반 개설
									</td>
									<td>
										·반별 세부주제와 인원 선택 후 신청서 작성<br>
										·동일 주제 최대 3반 개설 가능<br>
										·단, 과학수사대 주제별 2반까지 가능
									</td>
									<td>
										이메일 선착순 접수(gnsmedu@korea.kr)<br>
										(매주 토요일 마감)<br><br>
										
										[집중접수기간]<br>
										‘16. 02. 16(화) 10:00~ 02. 28(일)<br>
										[상시접수]<br>
										‘16. 02. 16(화)~상시접수<br>
										※최소 한 달 전 접수
									</td>
									<td style="vertical-align: middle;">
										<a href="<c:url value="/education/download/groupDescription"/>" class="btn btn-blue">
											<i class="fa fa-download"></i> 주제별 강의계획서
										</a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="table-responsive">
						<table class="table table-blue program-info-table">
							<thead>
								<tr>
									<th>접수 절차</th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										[주중 학교 단체]<br>
										①전화 문의 → ②신청서 접수(이메일) → ③신청확인(접수확인) 및 교육일정 확인 → ④교육 참가→⑤참가 확인 교육비 입금<br><br>
										[토요일 동아리]<br>
										①전화 문의 → ②신청서 접수(이메일) → ③신청확인 (접수 확인) 메일 발송 → ④교육비 입금→ ⑤선입금 확인 후 최종 확정메일 발송<br>
									</td>
									<td style="vertical-align: middle;">
										<a href="<c:url value="/education/download/groupApplication"/>" class="btn btn-blue">
											<i class="fa fa-download"></i> 신청서 다운로드
										</a>
									</td>
									<td style="vertical-align: middle;">
										<a href="<c:url value="/education/download/SATApplication"/>" class="btn btn-blue">
											<i class="fa fa-download"></i> 토요일 동아리 신청서 다운로드
										</a>
									</td>
									<td style="vertical-align: middle;">
										<a href="<c:url value="/education/download/groupCancel"/>" class="btn btn-blue">
											<i class="fa fa-download"></i> 변경 및 취소 신청서
										</a>
									</td>
								</tr>								
							</tbody>
						</table>
					</div>
				</div>
				<div class="col-md-12">
					<ul class="circle-list text-color blue">
						<li>
							<h5 class="exp-sub-title">
								교육비
							</h5>
						</li>
					</ul>
					<div class="table-responsive">
						<table class="table table-blue program-info-table">
							<thead>
								<tr>
									<th>대상</th>
									<th>교육비</th>
									<th>비고</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										유아반(6~7세)
									</td>
									<td>
										5,000원/명
									</td>
									<td rowspan="2">
										계좌이체 및 현장 카드결제<br>
										(전시관 입장료 , 주차료 별도)
									</td>
								</tr>
								<tr>
									<td>
										초등~고등반
									</td>
									<td>
										10,000원/명
									</td>
								</tr>
								<tr>
									<td colspan="3" class="center">
										<b>입금계좌: 농협 301-0058-6973-21 / 예금주: 국립과천과학관</b>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<p>
						-학교 단체는 후불 납부, 동아리 단체는 선불 납부를 원칙으로함<br>
						-카드 결제 후 관련 서류 [참가확인증,사업자등록증 및 통장 사본] 필요시 발급 가능(현장에서 요청바람)<br>
						-수업시간 외 과학관 입장료 별도(당일 카드결제 가능)<br>
						-주차료: 당일 주차매표 결제(1일 주차비: 소형 4,000원, 대형 9,000원)<br>
						※월요일은 휴관이니 상담전화는 화-일(9:00~18:00)까지 가능합니다
					</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<h3 class="program-title">
						전시물집중탐구
					</h3>
				</div>
				
				<div class="col-md-12">
					<ul class="circle-list text-color blue">
						<li>
							<h5 class="exp-sub-title">
								개설주제
							</h5>
						</li>
					</ul>
					<div class="table-responsive">
						<table class="table table-blue program-info-table group focusing">
						    <thead>
						        <tr>
						            <th width="12%" class="first">상설 전시관</th>
						            <th width="14%">6~7세</th>
						            <th>초등(1~3)학년</th>
						            <th>초등(4~6)학년</th>
						            <th>중학생</th>
						            <th>고등학생</th>
						        </tr>
						    </thead>
						    <tbody>
						        <tr>
						          <th class="first">어린이<br>탐구<br>체험관</th>
						            <td>
						              · 그림자가 뭐예요?<br>
						              · 우주여행을 떠나요!<br>
						              · 향기 나는 식물의 비밀!<br>
						              · 거미! 곤충 아니니?<br>
						              · 내 모습이 홀쭉~ 길쭉~<br>
						              · 따끈따끈 보들보들
						            </td>
						            <td>
						            · 내가 만든 전압<br>
						            · 살아나는 그림, 3D 입체<br>
						            · 그림자 놀이상자의 비밀<br>
						            · 몸속으로 떠나는 탐험여행<br>
						            · 소리여행을 떠나요!<br>
						            · 덜컹덜컹 움직이는 기계속 비밀<br>
						            · 빛은 마술사! 떠오르는 동전의 비밀<br>
						            · 눈으로 볼까 뇌로 볼까
						            </td>
						            <td>
						            </td>
						            <td>
						            </td>
						            <td>
						            </td>
						          </tr>
						          <tr>
						            <th class="first">기초<br>과학관</th>
						          <td>
						          </td>
						          <td>
						          </td>
						          <td>
						            · 부글부글~ 볼케이노!<br>
						            · 몸속에 뼈가 없다면?<br>
						            · 원소야~ 놀자<br>
						            · 빛과 색의 신비<br>
						            · 빛과 소리의 경주<br>
						            · 소리를 보아요!
						          </td>
						          <td>
						            · 혈액이 흐르는 길<br>
						            · 쭈뼛쭈뼛 내 머리카락!<br>
						            · 전기, 자기장을 제어하다.<br>
						            · 소금으로 빛을 만들자!<br>
						            · 파동과 파동이 만날 때
						          </td>
						          <td>
						            · 노벨상을 버린 테슬라와 에디슨<br>
						            · 트랜지스터가 바꾼 세상<br>
						            · 소금 속 들여다보기<br>
						            · 뉴턴을 향한 반격, 빛의 파동성
						          </td>
						            </tr>
						            <tr>
						                <th class="first">전통과학관</th>
						          <td>
						          </td>
						          <td>
						            · 슝~! 날아라 화살<br>
						            · 태양을 부활시키는 청동거울<br>
						            · 쓰륵쓰륵! 맷돌 만들기<br>
						            · 측우기여, 강우량을 측정하라!
						          </td>
						          <td>
						            · 별나라 임금님은 어디에 살까요?<br>
						            · 나만의 향주머니<br>
						            · 계절을 이해한 우리의 한옥<br>
						            · 한지에 스며드는 빛,청사초롱<br>
						            · 휙휙~ 돌을 날리는 투석기
						          </td>
						          <td>
						            · 음식에 깃든 조상의 지혜<br>
						            · 자연이 만들어낸 종이, 한지
						          </td>
						          <td>
						            · 천연염색, 과학에 물들다<br>
						            · '성덕대왕 신종' 소리의 비밀을 찾아서
						          </td>
						            </tr>
						            <tr>
						                <th class="first">첨단<br>기술관Ⅰ</th>
						          <td>
						          </td>
						          <td>
						            · 맑은 물 주식회사<br>
						            · 에너지를 만들자<br>
						            · 바람이 씽씽, 전기가 팍팍!
						          </td>
						          <td>
						            · 현미경 속 작은세상<br>
						            · 뇌파는 마술사<br>
						            · 로봇의 과거, 현재, 미래<br>
						            · 연료가 된 소금물
						          </td>
						          <td>
						            · 꼭꼭 숨은 에너지를 모아서<br>
						            · 지구를 살리는 적정기술<br>
						            · 생활 속의 방사선
						          </td>
						          <td>
						            · 아인슈타인과 원자력 발전<br>
						            · DNA 이중나선 구조 발견과 함께한 과학자들<br>
						            · 유전자 암호를 해독하라!<br>
						            · 인공태양과 핵발전의 미래<br>
						            · 인류의 궁극적 에너지원! 수소연료전지
						          </td>
						            </tr>
						            <tr>
						                <th class="first">첨단<br>기술관Ⅱ</th>
						          <td>
						          </td>
						          <td>
						          · 날아라! 우주왕복선<br>
						          · 릴리엔탈, 하늘을 날다!<br>
						          · 몽골피에형제의 도전
						          </td>
						          <td>
						          · 우주에 가려면? <br>
						          · 나는야 비행전문가<br>
						          · 포기는 없다! 최초의 비행기
						          </td>
						          <td>
						          · 우주기술 생활로 터치! 터치!<br>
						          · 은밀하게 조용하게<br>
						          · 비행기를 만난 베르누이
						          </td>
						          <td>
						          · 우리는 이제 우주로 간다<br>
						          · 생체모방공학기술<br>
						          · 석유, 고분자로 다시 태어나리<br>
						          · 캐러더스의 실험실<br>
						          · 245톤 항공기가 나는 법
						          </td>
						            </tr>
						            <tr>
						                <th class="first">자연사관</th>
						          <td>
						          </td>
						          <td>
						          · 어류 몸속 대탐험<br>
						          · 최고의 공룡<br>
						          · 애벌레의 꿈
						          </td>
						          <td>
						          · 공룡과 매머드! 누가 이길까?<br>
						          · 나무줄기 속 세상<br>
						          · 침팬지는 인간의 조상일까?<br>
						          · 빛으로 잡아요.<br>
						          · 핀치새의 비밀<br>
						          · 최고의 눈을 찾아라!
						          </td>
						          <td>
						          · 멘델의 법칙 그 다음 이야기<br>
						          · 자연에서 배우는 생활 속 아이디어<br>
						          · 인류의 진화, 증거를 찾아라!
						          </td>
						          <td>
						          · 산소, 지구 생명체를 바꾸다<br>
						          · 아가미와 허파<br>
						          · 유전자변이: DNA가 하나 바뀌면?
						          </td>
						        </tr>
						        <tr>
						            <th class="first">합계</th>
						      <td>6개 주제</td>
						      <td>21개 주제</td>
						      <td>24개 주제</td>
						      <td>16개 주제</td>
						      <td>19개 주제</td>
						        </tr>
						    </tbody>
						</table>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<h3 class="program-title">
						과학수사대
					</h3>
				</div>
				
				<div class="col-md-12">
					<ul class="circle-list text-color blue">
						<li>
							<h5 class="exp-sub-title">
								개설주제
							</h5>
						</li>
					</ul>
					<div class="table-responsive">
						<table class="table table-blue program-info-table group focusing">
							<thead>
								<tr>
									<th>초등(1~3)학년</th>
									<th>초등(4~6)학년</th>
									<th>중학생</th>
									<th>고등학생</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="first" align="left">·[지문] 꼭꼭 숨은 흔적! 쏙쏙 찾아내라!<br />
										·[족적] 작은 흔적도 놓치지 말아라!<br /> ·[위조지폐] 진짜를 찾아서 돈! 돈! 돈!<br />
										·[위조문서] 비밀약속, 비밀편지
		
									</td>
									<td class="txtL">·[지문] 딱 걸렸어~ 니 손가락!<br /> ·[족적] 사라진 운석을
										찾아서<br /> ·[위조지폐] 나는 위조지폐 감별사<br /> ·[위조문서] 종이가 말해준 우정<br />
										·[법곤충학] 곤충은 시간을 말한다.
									</td>
									<td class="txtL">·[지문] 사라지지 않는 증거<br /> ·[족적] 그래도 흔적은 남는다.<br />
										·[위조지폐] 캐치 미 이프 유 캔<br /> ·[위조문서] 진실을 밝히는 과학의 힘<br /> ·[법곤충학]
										곤충이 밝히는 진실<br /> ·[혈흔] 혈흔이 알려주는 진실<br /> ·[유전자분석] 범인 잡은 유전자
									</td>
									<td class="txtL">·[지문] CSI의 치명적 실수<br /> ·[족적] 모든 접촉은 흔적을
										남긴다<br /> ·[혈흔] 용의자 X의 행적<br /> ·[유전자] 유전자는 거짓말을 하지 않는다
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<h3 class="program-title">
						발명교실
					</h3>
				</div>
				
				<div class="col-md-12">
					<ul class="circle-list text-color blue">
						<li>
							<h5 class="exp-sub-title">
								개설주제
							</h5>
						</li>
					</ul>
					<div class="table-responsive">
						<table class="table table-blue program-info-table group focusing">
							<thead>
								<tr>
									<th>초등(3~4)학년</th>
									<th>초등(5~6)학년</th>
									<th>중학생</th>
									<th>고등학생</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="first" align="left">
										·바퀴<br>
										·자석<br>
										·포개기<br>
										·전기
									</td>
									<td class="txtL">
										·전기(전도성펜)<br>
										·센서(기울기)<br>
										·진동원리<br>
										·자가발전<br>
										·3D프린터(3D펜)
									</td>
									<td class="txtL">
										·빛<br>
										·기계장치(캠, 크랭크, 기어)<br>
										·천연자원(물, 바람, 태양)<br>
										·신소재(고무)
									</td>
									<td class="txtL">
										·바이메탈<br>
										·센서와 진동<br>
										·모터<br>
										·트랜지스터<br>
										·물라스틱
									</td>
								</tr>
		
							</tbody>
						</table>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="call-bottom-guide">
						<div class="call-icon blue"><i class="fa fa-phone"></i></div> 
						<div class="desc">
							사전에 프로그램 내용과 주제에 대하여 숙지하신 후 <span class="blue"><b>교육행정실(02-3677-1492)</b></span>
							로 프로그램 구성 및 교육관 시설사용 가능 여부에 대하여 문의하여 주시기 바랍니다.
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>