<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-blue">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>
	<div id="support-body" class="sub-body">
		<div id="basic-info-spy" class="narrow-sub-top education">
			<div class="sub-banner-tab-wrapper sub-banner-tab-blue">
				<div class="container">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
						   
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="support-sub-nav">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="sub-nav convenience">
							<div class="row">
								<div class="col-xs-12 col-sm-4 col-md-3">
									<a href="<c:url value='/education/experienceWeekends' />" class="item active">
										<i class="fa fa-flask" aria-hidden="true"></i> 개인 주말 프로그램
									</a>
								</div>
								<div class="col-xs-12 col-sm-4 col-md-3">
									<a href="<c:url value='/education/experienceVacation' />" class="item active">
										<i class="fa fa-flask" aria-hidden="true"></i> 개인 방학 프로그램
									</a>
								</div>
								<div class="col-xs-12 col-sm-4 col-md-3">
									<a href="<c:url value='/education/group' />" class="item active">
										<i class="fa fa-flask" aria-hidden="true"></i> 단체 주중 프로그램
									</a>  
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title blue">
						<div class="top-line"></div>
						교육 공지사항
					</h3>
				</div>
			</div>
		 	<div class="row">
		 		<div class="col-md-12">
		 			<div id="board-wrapper" class="table-blue">
			 			<c:url var="currentPath" value="/education/educationNotice"/>
			 			
			 			<c:import url="/WEB-INF/views/boards/list.jsp">
			 				<c:param name="path" value="${currentPath }"/>
			 			</c:import>
		 			</div>
		 		</div>
			</div>
		</div>
	</div>
</div>