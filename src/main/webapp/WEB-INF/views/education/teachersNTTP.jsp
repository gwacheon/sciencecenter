<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-blue">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>

	<div class="sub-body">
		<div class="narrow-sub-top education">
			<div class="sub-banner-tab-wrapper sub-banner-tab-blue">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<ul class="nav nav-tabs">
								<li class="col-xs-6">
									<a href="<c:url value='/education/teachersOnSite' />" class="item">교사 현장연수</a>
								</li>
		        				<li class="col-xs-6 active">
		        					<a href="#" class="item">NTTP교사 위탁연수</a>
		       					</li>
							</ul>   
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div id="edu-exp-body" class="container tab-content">
    		<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<h3 class="program-title">
						프로그램 안내
					</h3>
				</div>
			</div>
				
			<div class="row">
				<div class="col-md-12">
					<div class="bordered-div bordered-blue margin-bottom20">
						경기도교육청 우수교사들의 요청에 의한 NTTP(New Teacher Training Program) 교사 위탁연수 지정 기관 수탁을 통해 교사연수 기관으로서의 위탁연수를 실시합니다.<br>
						연구년 신청시 혹은 선발된 연구년 교사들은 지정위탁기관을 국립과천과학관으로 신청하고 관심 연구분야와 관련있는 석박사 학위를 가진 과천과학관의 연구직 공무원을 중심으로 멘토를 지정 신청할 수 있습니다.
					</div>
					<div class="contact-info margin-bottom30">
						<div class="circle-bg circle-bg-blue">
							?
						</div>
						<div class="header">
							문의 :
						</div>
						<div class="detail">
							 과학탐구교육과 
						</div>
						<div class="call-number">
							 <i class="fa fa-phone blue"></i> 02-36770-1521, 1522
						</div>
					</div>
				</div>
			</div>
				
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<h3 class="program-title">
						취지
					</h3>
				</div>
			</div>
				
			<div class="row">
				<div class="col-md-12">
					<div class="bordered-div bordered-blue margin-bottom20">
						학습연구년 교사들을 대상으로 우리관의 첨단과학시설과 환경을 학교밖 과학교육 연구에 활용하고<br>
						연구결과를 관람객들에게 환원할 수 있는 연수프로그램을 위탁연수 기간 동안 운영하여 과천과학관의 교육적 활용 효과를 극대화
					</div>
					<div class="contact-info margin-bottom30">
						<div class="circle-bg circle-bg-blue">
							?
						</div>
						<div class="header">
							문의 :
						</div>
						<div class="detail">
							 과학탐구교육과 
						</div>
						<div class="call-number">
							 <i class="fa fa-phone blue"></i> 02-36770-1521, 1522
						</div>
					</div>
				</div>
			</div>
				
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<h3 class="program-title">
						개요
					</h3>
				</div>
			</div>
				
			<div class="row">
				<div class="col-md-12">
					<div class="bordered-div bordered-blue margin-bottom30">
						<div class="content-wrapper">
							<div class="content-label">
								일시
							</div>
							<div class="content-value">
								매 학년도 3월 1일~이듬해 2월 28일(12개월)
							</div>
						</div>
						<div class="content-wrapper">
							<div class="content-label">
								장소
							</div>
							<div class="content-value">
								연구․실험실 중 배정, 도서쉼터(구 과학기술사료관) 등 이용
							</div>
						</div>
						<div class="content-wrapper">
							<div class="content-label">
								참가대상
							</div>
							<div class="content-value">
								시•도교육청에서 선발된 연구년 교사
							</div>
						</div>
						<div class="content-wrapper">
							<div class="content-label">
								연수비
							</div>
							<div class="content-value">
								<ul class="non-padding">
									<li>위탁 연수 신청자는 소정의 개인당 연수비용을 국립과천과학관에 납부<br> ※ 개인당
										50만원(2013년)을 멘토 지도, 전공 수강 및 시설 사용 비용 명목으로 납부
									</li>

									<li>국내외 유관 기관 탐방, 학회 세미나 참석 등 세부 프로그램 구성 운영에 따른 단체 소요 비용
										필요시<br>시 수익자 부담 원칙으로 운영<br> ※ 전체 연구 활동비 5백만원에 대한
										정산서는 경기도교육청에 개별 제출
									</li>
								</ul>
							</div>
						</div>
						<div class="content-wrapper">
							<div class="content-label">
								지원사항
							</div>
							<div class="content-value">
								<ul class="non-padding">
									<li>연구 세미나용 PC, 프로젝터 등 과천과학관 기자재 활용</li>
									<li>과천과학관 출입카드, 주차시설 사용 허가증 등 제공</li>
									<li>식당 직원용 코너 이용 편의 제공 등</li>
									<li>기타 연구뇬 교사 요청 사항 협의 지원</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
				
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<h3 class="program-title">
						연수 내용
					</h3>
				</div>
				
				<div class="col-md-12">
					<ul class="circle-list blue text-color">
						<li>
							<h5 class="exp-sub-title">
								교수 사사(석․박사급 연구직 멘토링) 지도
							</h5>						
						</li>
					</ul>
					
					<div class="padding-area20">
						<strong>전시연구단 각 과별 소속 전공분야별 석․박사급 연구직 직원을 멘토로 지정하여 1:1 사사 지도</strong><br>
						(붙임 국립과천과학관 연구직 공무원 전공 현황 참조)<br>
						 - 과학교육, 기술교육, 천문, 생명공학, 물리, 지구과학, 환경, 컴퓨터 등 각과 소속 연구직 전공분야별로 위탁연수 신청자가 지정 신청,
						상호 협의하에 1:1 멘토링 지도 가능<br>
						 - 학교밖 과학교육 관련 교수․학습 자료 개발, 첨단 전시물 개발 및 전시기법 관련 분야 멘토링 가능<br>
						 - 자유탐구학습, 과학융합체험학습, 창의적 체험학습, 전시장 특별교육 관련 자료개발 및 설문조사 등을 통한 학교 활용 방안 공동
						연구 가능<br>
					</div>
				</div>
				
				<div class="col-md-12">
					<ul class="circle-list blue text-color">
						<li>
							<h5 class="exp-sub-title">
								전공 강좌 수강
							</h5>
						</li>
					</ul>
					
					<div class="padding-area20">
						<strong>학교밖 교육 선도기관으로서 과천과학관의 첨단 전시시설과 문화공연환경을 활용한 교사 직무연수프로그램 개설 강좌 수강 자격 부여</strong><br>
						 - 초등 교사 대상 30시간 전시물이용지도과정, 초․중등 교사 대상 15시간 자유탐구학습지도과정 등<br>
						<strong>천체투영관 및 천체관측소를 활용한 천문교육프로그램을 전공 강좌로 개설 운영, 수강 자격 부여</strong><br>
						<strong>기타 과천과학관 스승의 날 기념 자체 테마 워크숍 프로그램 등 과천과학관 주관 교사워크숍, 세미나 등에 우선 참여 및 신청 자격 부여</strong><br>
						<strong>과천과학관의 최신 첨단 과학시설 및 전시물 활용 학교 과학교육 개선 또는 혁신방안을 제안할 수 있는 시설 자유 이용 및 연구 기회 제공</strong><br>
						- 기초과학관, 어린이탐구체험관, 첨단기술관Ⅰ&Ⅱ관, 자연사관, 전통과학관, 곤충생태관, 천체투영관, 천체관측소, 생태공원, 야외전시장 등 전시시설<br>
						- 2천여 점의 작동․체험 중심 전시물<br>
						- 첨단 기자재와 과학실험도구를 갖춘 실험실 8개 및 강의실 6개 등 교육시설<br>
					</div>
				</div>
				
				<div class="col-md-12">
					<ul class="circle-list blue text-color">
						<li>
							<h5 class="exp-sub-title">
								과학문화체험/교양 프로그램 참여
							</h5>
						</li>
					</ul>
					
					<div class="padding-area20">
						<strong>학교밖 과학문화체험 활동을 통한 교양 증진이 가능한 솨학문화행사, 특별전시 행사, 과학문화캠프행사 등 과학문화․교양 프로그램을 담당 과별 업무 담당자와 협의하여 참여 프로그램으로 구성 운영</strong><br>
						 - 과학송경연대회, 사이언스데이 과학행사, 뮤지컬공연 등 과학문화행사<br>
						 - 등 과학문화행사<br>
						 -  창의소통 1박 2일 캠프, 과학나눔 캠프 등 과학문화캠프 행사 등<br>
						<strong>위탁 연수 교사들이 개인별 단순 참가 혹은 소속 학교 단체 참여 및 지도 협력활동을 하고자 하는 프로그램에 참여할 수 있는 자젹 및 우선 신청 기회 제공</strong><br>
					</div>
				</div>
				
				<div class="col-md-12">
					<ul class="circle-list blue text-color">
						<li>
							<h5 class="exp-sub-title">
								세미나, 워크숍 참가
							</h5>						
						</li>
					</ul>
					
					<div class="padding-area20">
						<strong>과천과학관 자체 연구직 세미나 개최 시 참석 자격 부여, 의견 교환 및 토의 참여</strong><br>
						<strong>위탁연수 교사 요청시 과학교육 및 관심 분야에 따른 외부 전문가 초청 세미나, 워크숍 개최 및 참가 연수 지원</strong><br>
						<strong>한국과학교육학회 등 과학교육 관련 학회 세미나 참석 및 공동 연구결과 발표 지원</strong><br>
					</div>
				</div>
				
				<div class="col-md-12">
					<ul class="circle-list blue text-color">
						<li>
							<h5 class="exp-sub-title">
								유관기관 탐방 참가
							</h5>						
						</li>
					</ul>
					
					<div class="padding-area20">
						<strong>중앙과학관, 서울과학관, 우석헌자연사박물관, 광릉수목원 등 학교밖 과학교육 기관 탐방 및 협력 활동 프로그램 구성, 지원</strong><br>
						<strong>KIST, 한국천문연구원, 기초자원연구원, 원자력발전소 등 견학 지원</strong><br>
						<strong>대학민국 과학축전, 오사카 과학축전 등 국내․외 과학축전, 과학의 달 행사 참가 등 탐방프로그램 협으 구성 및 지원</strong><br>
					</div>
				</div>
			</div>
				
			<div class="row margin-bottom30">
				<div class="col-md-6 col-md-offset-3">
					<h3 class="program-title">
						연간일정
					</h3>
				</div>
				
				<div class="col-md-12">
					<div class="blue-box">
						연구년 교사들이 지정받은 멘토들과 함께 프로그램 구성 분야별로 세부 내용 및 일정(기간, 시간 수 등)을 협의하여 연간 270시간의 개인 연수계획을 수립 추진(추후 개인별 연구년 계획서 제출)
					</div>
					
					<div class="margin-bottom30">
					 	- 270시간(12개월)(예시) ※ 추후 멘토와의 협의를 통해 세부 내용 및 일정 변경 가능
					</div>
					
					<div class="table-responsive">
						<table class="table lined-table centered-table">
							<thead>
								<tr>
									<th class="first" scope="col">분야</th>
									<th colspan="2" scope="col">프로그램 내용</th>
									<th scope="col">시간수</th>
									<th scope="col">일정</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="" rowspan="2">전공 강좌 수강1<br />(과천과학관 교사직무연수
										과정 이수)
									</td>
									<td class="left">
										<ul class="commonList pTNo pBNo">
											<li>과학관교육 이론</li>
											<li>과학관 시설과 전시물의 이해</li>
											<li>전시물 관련 탐구활동 교수법</li>
											<li>전시물 관련 실험탐구지도</li>
										</ul>
									</td>
									<td class="left">초등교사, 30시간 직무연수과정<br />
									<경기도교육청 율곡교육연수원 지정 의거></td>
									<td rowspan="2">45</td>
									<td rowspan="2">8월</td>
								</tr>
								<tr>
									<td class="left">
										<ul class="commonList pTNo pBNo">
											<li>자유탐구학습 이론</li>
											<li>전시물 이해 및 활용지도</li>
											<li>자유탐구활동지도 실습</li>
											<li>탐구실험 및 실습</li>
										</ul>
									</td>
									<td class="left">초등/중등교사 15시간 직무연수과정<경기도교육청 율곡교육연수원 지정 의거></td>
								</tr>
								<tr>
									<td class="first">개인별 사사(멘토링)<br />지도 연구
									</td>
									<td class="left" colspan="2">
										<ul class="commonList pTNo pBNo">
											<li>과천과학관 천문교육 프로그램 수강<br />- 천체관측 이론 및 망원경 실습 등
											</li>
											<li>상설 전시관 전시물 심층 해설<br />- 기초과학관․첨단기술관․자연사관․전통과학관
											</li>
											<li>과천과학관 교육프로그램 과목 견학</li>
											<li>과천과학관 현장 체험 지도 참여 등</li>
										</ul>
									</td>
									<td>30</td>
									<td>3~12월</td>
								</tr>
								<tr>
									<td class="first">전공 강좌 수강2<br />(과천과학관 시설 체험 및 활용지도의 실제)
									</td>
									<td class="left" colspan="2">
										<ul class="commonList pTNo pBNo">
											<li>멘토와의 면담 및 토의, 공동연구 작업</li>
											<li>과학교육, 전시기법 관련 자료 조사 연구</li>
											<li>전시물 활용 교수․학습 자료 개발</li>
											<li>과천과학관 제반 교육 및 전시 프로그램 효과 분석</li>
										</ul>
									</td>
									<td>60</td>
									<td>3~12월</td>
								</tr>
								<tr>
									<td class="first">유관 기관 탐방 및 체험</td>
									<td class="left" colspan="2">
										<ul class="commonList pTNo pBNo">
											<li>중앙과학관, 서울과학관, 나로우주발사센터 등 방문 협의</li>
											<li>우석헌자연사박물관, 광릉수목원 등 견학</li>
											<li>KIST, 한국천문연구원, 기초자원연구원, 원자력발전소 등 탐방</li>
											<li>국내․외 과학축전 관람</li>
										</ul>
									</td>
									<td>30</td>
									<td>4~9월</td>
								</tr>
								<tr>
									<td class="first">학회 세미나 참석 발표</td>
									<td class="left" colspan="2">
										<ul class="commonList pTNo pBNo">
											<li>과천과학관 연구직 세미나 참석 및 연구 성과물 발표</li>
											<li>관련 학회 및 세미나 참석 및 발표</li>
											<li>교육청 단위 세미나 참석 및 발표 등</li>
										</ul>
									</td>
									<td>15</td>
									<td>수시</td>
								</tr>
								<tr>
									<td class="first">자율 구성 연수</td>
									<td class="left" colspan="2">
										<ul class="commonList pTNo pBNo">
											<li>직무연수<경기도과학교육원 , 율곡교원연수원, 티쳐빌 등></li>
											<li>교육코칭 및 창의적 학습방법<한국리더십센터></li>
											<li>사고력 확장 기법 연수<부잔코리아></li>
										</ul>
									</td>
									<td>45</td>
									<td>4~10월</td>
								</tr>
								<tr>
									<td class="first">기타</td>
									<td class="left" colspan="2">기타 행사 참석 등</td>
									<td>45</td>
									<td>수시</td>
								</tr>
								<tr>
									<td class="first">계</td>
									<td colspan="2"></td>
									<td>270</td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>