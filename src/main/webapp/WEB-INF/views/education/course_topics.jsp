<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div id="course-topics-wrapper">

</div>

<script id="course-topic-template" type="text/x-handlebars-template">
<div class="panel-group blue margin-bottom30" id="course-topics" role="tablist"
	aria-multiselectable="true">
	{{#each courseTopics}}
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="heading-{{@index}}">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#course-topics"
						href="#collapse-{{@index}}" aria-expanded="true"
						aria-controls="collapse-{{@index}}" class="{{#compare @index "0" operator="!="}}collapsed{{/compare}}">
						{{title}}
					</a>
				</h4>
			</div>
			<div id="collapse-{{@index}}" class="panel-collapse collapse {{#compare @index "0" operator="=="}} in{{/compare}}"
				role="tabpanel" aria-labelledby="heading-{{@index}}">
				<div class="panel-body">
					<div class="content-wrapper">
						<div class="content-label">
							세부직종
						</div>
						<div class="content-value">
							{{careerValue}}
						</div>
					</div>
					<div class="content-wrapper">
						<div class="content-label">
							탐구내용
						</div>
						<div class="content-value">
							{{descValue}}
						</div>
					</div>
					<div class="content-wrapper">
						<div class="content-label">
							체험활동
						</div>
						<div class="content-value">
							{{activityValue}}
						</div>
					</div>
				</div>
			</div>
		</div>
	{{/each}}
</div>	
</script>


<script type="text/javascript">
	var courseTopics = [
		{
			title: 	"[과학/기술]녹색에너지",
		    careerValue: "신재생에너지 연구·개발자, 에너지 환경 정책연구자",
		    descValue: "친환경 에너지를 연구하고 생활에 활용할 수 있는 기술을 개발하는 녹색 에너지 분야에 대해서 탐구해본다.",
		    activityValue: "청정에너지 기술시스템 체험, 소금물로 가는 자동차"
		},
		{
			title: 	"[과학/기술]뇌과학·뇌공학",
		    careerValue: "뇌분석·뇌질환 전문가, 감성인식기술 연구·개발자",
		    descValue: "아직 미지의 영역인 뇌에 대한 세계의 관심은 날로 높아지고 있다. 뇌에 대한 연구는 미래 세상을 어떻게 변화시킬까?",
		    activityValue: "양의 뇌 해부 및 관찰, 뇌파 게임"
		},
		{
			title: 	"[과학/기술]로봇",
		    careerValue: "로봇 설계·개발자, 산업용 로봇 연구·개발자",
		    descValue: "사람을 돕고 삶을 더 풍부하게 하는 로봇을 만들기 위해 어떤 연구들이 진행되고 있을까? 로봇은 미래를 어떻게 바꿀까?",
		    activityValue: "로봇 팔 관찰하기, 태양광 로봇 만들기"
		},
		{
			title: 	"[과학/기술]생명의공학	",
		    careerValue: "신약개발 전문가, 의료/실험 장비 전문가",
		    descValue: "인공장기, 의료기기, 신약개발, 백신개발 및 줄기세포로 이루어지는 보건·생명·의공학 분야의 진로를 탐색하고, 조직배양 전문가가 되어 배양이 어려운 식충식물을 인공증식 시켜보자.",
		    activityValue: "조직배양 전문가 체험, 희귀식물 인공배양실험"
		},
		{
			title: 	"[과학/기술]네트워크",
		    careerValue: "모바일콘텐츠 연구·개발자, 네트워크 시스템 연구·개발자",
		    descValue: "가상의 시공간에서 각종 응용분야에 적용할 수 있는 시스템을 설계하고 이용하는 분야에 대해서 알아본다.",
		    activityValue: "가상현실 체험하기, VR 카드보드 제작하기"
		},
		{
			title: 	"[과학/기술]생명과학",
		    careerValue: "생물정보 분석전문가, 생물자원 연구·개발자",
		    descValue: "인류에게 당면한 의약, 식품, 환경, 에너지와 같은 문제를 해결해 주고 삶의 질을 높이는 혜택을 주는 생명과학분야의 진로를 탐색해본다.",
		    activityValue: "유전자 전문가, 친자확인을 위한 유전자 분석하기"
		},
		{
			title: 	"[과학/기술]나노 융합 신소재",
		    careerValue: "나노신소재 연구·개발자, 재료/금속/섬유공학 기술자",
		    descValue: "우주항공, 자동차, 화장품, 에너지, 의료, 바이오, 섬유 등 전 산업 분야에서 다양하게 활용되고 있는 나노기술에 대해서 살펴보고 나노 기술을 바탕으로 만들어진 신소재 분야에 대해서 탐구해본다.",
		    activityValue: "신소재 그래핀 관찰하기, 폴리모프(물라스틱) 체험"
		},
		{
			title: 	"[과학/기술]정보시스템",
		    careerValue: "시스템 소프트웨어 전문가, 시스템 통합 전문가",
		    descValue: "21세기 정보전쟁 속에서 각종 시스템과 소프트웨어를 개발해 정보를 효율적으로 수집, 관리, 활용할 수 있도록 하는 정보시스템 분야에 대해 탐구한다.",
		    activityValue: "컴퓨터 시스템 구성, 프로그램 활용한 코딩 입문"
		},
		{
			title: 	"[과학/기술]항공우주",
		    careerValue: "항공(유/무인)기 연구·개발자, 위성항법/궤도제어 전문가",
		    descValue: "하늘에 대한 상상 어디까지 이뤄질까? 다양한 비행물체에 대한 개발연구와 항공우주기술에 대해서 알아보고, 항공공학분야 및 우주과학 분야에서 주목할 만한 직업에 대해서 살펴본다.",
		    activityValue: "드론 조종하기, 전동 비행 모형 만들기"
		},
		{
			title: 	"[과학/기술]헬스케어",
		    careerValue: "U 헬스케어 전문가, 헬스케어 컨설턴트",
		    descValue: "의학기술과 공학적 지식을 접목하여 좀 더 효과적으로 건강을 관리할 수 있도록 해주는 개인 맞춤형 헬스케어 분야에 대해 알아본다.",
		    activityValue: "혈액분석을 통해 자신의 건강상태 모니터링 해보기, 체 성분 측정 및 분석 시스템 체험"
		},
		{
			title: 	"[인문/예술융합]법조인과 과학",
		    careerValue: "변호사 (변리사), 과학수사대 (CSI)",
		    descValue: "과학은 전공자들만의 전유물인가? 아니다! 절대 그렇지 않다!　날카롭고 냉철한 과학적 사고를 통해 사건을 낱낱이 파헤치는 그들의 직업세계에 대해서 알아본다. 또한, 세상을 살아가는 데 있어 과학적 지식 뿐 만이 아닌 ‘과학적 사고’ 즉 합리적으로 판단하는 사고의 중요성에 대하여 생각해 보는 시간을 가져본다.",
		    activityValue: "거짓말 탐지기를 이용해 사건 수사망 좁혀보기, 사건에 대한 토론을 통해 각자의 판결 내려 보기"
		},
		
	
	];
	
	var courseTopicsSource = $("#course-topic-template").html();
	var courseTopicsTemplate = Handlebars.compile(courseTopicsSource);
	var courseTopicsHtml = courseTopicsTemplate({courseTopics: courseTopics});
	$("#course-topics-wrapper").html(courseTopicsHtml);
	
	
</script>