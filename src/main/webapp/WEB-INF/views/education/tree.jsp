<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-blue">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>
	
	<div class="sub-body">
		<div class="narrow-sub-top education">
			<div class="sub-banner-tab-wrapper sub-banner-tab-blue">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<ul class="nav nav-tabs">
								<li class="col-xs-12 col-sm-6">
									<a href="<c:url value='/education/fusionScience' />" class="item">과학융합탐구</a>
								</li>
		        				<li class="col-xs-12 col-sm-6 active">
		        					<a href="#" class="item">스스로 과학탐구</a>
		       					</li>
							</ul>     
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="support-sub-nav">
			<div class="container">
		   		<div class="sub-nav convenience">
		   			<div class="row">
						<div class="col-xs-6 col-sm-3 col-md-3">
				     		<a href="<c:url value='/education/reverseScience' />"  class="item">거꾸로 과학탐구 교실</a> 
						</div>
						<div class="col-xs-6 col-sm-3 col-md-3">
					      	<a href="#" class="item active">생각트리 과학탐구 교실</a>
						</div>
		   			</div>
				</div>
			</div>
		</div> 
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title blue">
						<div class="top-line"></div>
						생각트리 과학탐구 교실
					</h3>
				</div>
			</div>
			<div class="row">
		 		<div class="col-md-12">
		 			<div id="board-wrapper" class="table-blue">
			 			<c:url var="currentPath" value="/education/tree"/>
			 			
			 			<c:import url="/WEB-INF/views/boards/list.jsp">
			 				<c:param name="path" value="${currentPath }"/>
			 			</c:import>
		 			</div>
		 		</div>
			</div>
		</div>
	</div>
</div>