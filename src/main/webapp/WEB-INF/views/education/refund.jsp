<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-blue">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>

	<div id="intro-body" class="sub-body">
		<div id="basic-info-spy" class="narrow-sub-top education">
			<div class="sub-banner-tab-wrapper sub-banner-tab-blue">
				<div class="container">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
						
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="edu-exp-body" class="container">
			<div class="row margin-top20">
				<div class="col-md-12">
					<ul class="circle-list blue text-color exp-sub-title">
						<li>
							환불 규정
						</li>
					</ul>
					<ul class="dash-list margin-bottom30">
						<li>
							환불은 평생교육법, 학원법 등에 근거하며, 결석에 대한 환불은 하지 않습니다.
						</li>
					</ul>
				</div>
				
				<div class="col-md-6">
					<div class="table-responsive">
						<table class="table table-blue program-info-table">
							<thead>
								<tr>
									<th colspan="2">환불신청 시점에 따른 환불액 안내</th>
								</tr>
								<tr>
									<th class="left">첫 수업 7일전</th>
									<td class="left">전액 환불</td>
								</tr>
								
								<tr>
									<th class="left">첫 수업 6일전 ~ 1/3차시 수업 당일</th>
									<td class="left">1/3 공제 후 환불</td>
								</tr>
								
								<tr>
									<th class="left">1/3차시 수업 다음날 ~ 2/3차시 수업 당일</th>
									<td class="left">2/3 공제 후 환불</td>
								</tr>
								
								<tr>
									<th class="left">2/3차시 수업 이후</th>
									<td class="left">환불 불가</td>
								</tr>
							</thead>
						</table>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="table-responsive">
						<table class="table program-info-table table-blue">
							<thead>
								<tr>
									<th colspan="2">환불신청 방법 안내</th>
								</tr>
								<tr>
									<th class="left">첫 수업 7일전 환불 신청 방법</th>
									<td class="left">국립과천과학관 홈페이지: 마이페이지 > 환불신청</td>
								</tr>
								
								<tr>
									<th rowspan="2" class="left">첫 수업 6일전 ~ 2/3차시 수업 당일</th>
									<td class="left">
										교육관 3층 교육행정실 방문 신청
									</td>
								</tr>
								<tr>
									<td class="left">
										(수감증, 통장사본 필수 지참)
									</td>
								</tr>
								
								<tr>
									<td colspan="2" class="left">
										<ul class="circle-list text-color blue">
											<li>
												환불 처리가 완료되기까지 약 2주일의 기간이 소요됩니다.
											</li>
										</ul>
									</td>
								</tr>
							</thead>
						</table>
					</div>
				</div>
				
				<div class="col-md-12 margin-bottom30">
					<ul class="dash-list">
						<li>
							위 규정은 주말프로그램에 한하여 적용이 되고, 방학프로그램은 첫 수업 시작 7일 전 이후엔 환불이 불가합니다.
						</li>
						<li>
							규정을 벗어난 환불 및 교육비 적립은 없으므로 꼼꼼히 읽은 후 신중한 접수 부탁 드립니다.
						</li>
						<li>
							카드 결제 후 계좌이체로 교육비를 환불하는 경우에는 수수료 차감이 있을 수 있습니다.
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>