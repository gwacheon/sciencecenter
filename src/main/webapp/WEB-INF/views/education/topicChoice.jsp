<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-blue">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>

	<div id="intro-body" class="sub-body">
		<div class="narrow-sub-top education">
			
		</div>
		<div id="edu-exp-body" class="container">
			<div class="row scrollspy">
				<div class="col-md-6 col-md-offset-3">
					<h3 class="program-title">
						프로그램 안내
					</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 margin-bottom20">
					<ul class="circle-list exp-sub-title text-color blue">
						<li>
							과정안내
						</li>
					</ul>
				</div>
				<div class="col-md-12">
					<ul class="ptech-ul dash-list">
						<li class="strong margin-bottom20">
							운영 프로그램 : 무한상상실 4개, 천문우주시설 2개, 곤충생태관 1개
							
							<ul class="ptech-ul non-bullet">
								<li>① 무한상상실(4개 주제) <span class="blue">【TRIZ 발명교실, 디지털 제작교실, 소프트웨어 교실, 영상 제작교실】</span></li>
								<li>② 천문우주시설(2개 주제) <span class="blue">【천체관측장비활용, 천문우주시설 탐방】</span></li>
								<li>③  곤충생태관(1개 주제) <span class="blue">【아름다운 곤충표본 관찰】</span></li>
							</ul>
						</li>
						
						<li class="strong">
							운영 시간 : 2016.3.8(화) ~ 2016.12.23(금) / 매주 화~금 1일 2회【오전 10:00~12:00, 오후 13:00~15:00】
						</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<ul class="circle-list exp-sub-title text-color blue">
						<li>
							참여 대상 및 프로그램 구성
						</li>
					</ul>
					<div class="table-responsive">
						<table class="centered-table table table-blue">
							<thead>
								<tr>
									<th>대상 및 인원</th>
									<th>프로그램 구성</th>
									<th>접수방법</th>
								</tr>
							</thead>
							
							<tbody>
								<tr>
									<td>
										- 대상 : 중학생 단체 (학교 및 동아리)<br>
										- 인원 : 7주제 총 120명
									</td>
									
									<td>
										- 무한상상실(4개), 천문우주시설(2개), 곤충생태관(1개)
									</td>
									
									<td>
										<div>- 전화상담 후 이메일로 신청서 제출/상시접수</div>
										<div><b>※참가접수는 사용 희망일 최소 30일 전까지 접수</b></div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<ul class="circle-list exp-sub-title text-color blue">
						<li>
							프로그램 참가 절차
						</li>
					</ul>
					<div class="table-responsive">
						<table class="centered-table table table-blue">
							<thead>
								<tr>
									<th>프로그램 참가 절차</th>
									<th>참가 신청 (변경·취소)</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										프로그램 확인 <i class="fa fa-chevron-right"></i> 전화 상담 (프로그램별 담당자)
										<i class="fa fa-chevron-right"></i> 신청서 작성 후 이메일 제출 <i class="fa fa-chevron-right"></i> 접수 확인 (과학관에서 접수 승인메일 발송) <i class="fa fa-chevron-right"></i> 교육비 결제(현장 카드결제) <i class="fa fa-chevron-right"></i> 프로그램 참가
									</td>
									<td>
										<a href="<c:url value="/education/course/download/application"/>" class="btn btn-blue margin-top10">
											<i class="fa fa-download"></i> 참가신청서
										</a>
									</td>

								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 margin-bottom20">
					<ul class="circle-list exp-sub-title text-color blue">
						<li>
							교육비 : 10,000원/명(과학관 입장료와 주차료 별도)
							<ul>
								<li>납부방법 : 현장 신용카드 결제</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<ul class="circle-list exp-sub-title text-color blue">
						<li>
							관련문의
						</li>
					</ul>
				</div>
				<div class="col-md-12 margin-bottom20">
					<ul class="ptech-ul non-bullet">
						<li>
							무한상상실: 박남식 연구관(02.3677.1443, nspark7979@korea.kr)
						</li>
						<li>
							천문우주시설 : 조재일 연구사(02.3677.1448, stoneye@korea.kr)
						</li>
						<li>
							곤충생태관 : 손재덕 연구사(02.3677.1447, jedogy0805@korea.kr)
						</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 margin-bottom20">
					<ul class="circle-list exp-sub-title text-color blue">
						<li>
							개설주제
						</li>
					</ul>
				</div>
				<div class="col-md-12">
					<c:import url="/WEB-INF/views/education/topics.jsp"></c:import>
				</div>
			</div>
		</div>
	</div>
</div>