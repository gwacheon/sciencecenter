<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-blue">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>

	<div class="sub-body">
		<div class="narrow-sub-top education">
			
		</div>
		
		<div id="edu-exp-body" class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<h3 class="program-title">
						프로그램 기본 안내
					</h3>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<ul class="circle-list text-color blue">
						<li>
							<h5 class="exp-sub-title">
								교육개요
							</h5>
						</li>
					</ul>
				</div>
			</div>
			
			<div class="row margin-bottom20">
				<div class="col-md-12">
					<div class="program-intro-wrapper">
						<div class="intro">
							<div class="header">
								교육명
							</div>
							<div class="value">
								제2기 『청춘과학대학』
							</div>
						</div>
						<div class="intro">
							<div class="header">
								교육비
							</div>
							<div class="value">
							 	2016. 9. 6 ～ 12. 20 (매주 화요일 13:30～16:30/16주)
							</div>
						</div>
						<div class="intro">
							<div class="header">
								대상
							</div>
							<div class="value">
								60세 이상 어르신 90명 이내
							</div>
						</div>
						<div class="intro">
							<div class="header">
								장소
							</div>
							<div class="value">
								과천과학관 교육동 대강의실, 전시장 일대, 탐방 현장
							</div>
						</div>
						<div class="intro">
							<div class="header">
								내용
							</div>
							<div class="value">
								하단 참고
							</div>
						</div>
						<div class="intro">
							<div class="header">
								교육비
							</div>
							<div class="value">
								180,000원/인(월 45,000원)
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<ul class="circle-list text-color blue">
						<li>
							<h5 class="exp-sub-title">
								접수 안내
							</h5>
						</li>
					</ul>
					<table class="table table-blue program-info-table" summary="스스로 과학탐구">
						<colgroup>
							<col style="width: 20%">
							<col style="width: 60%">
							<col style="width: 20%">
						</colgroup>
						<thead>
							<tr>
								<th class="first">접수기간</th>
								<th class="col">접수방법</th>
								<th class="col">신청서 양식</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="first left-article">
									2016. 7. 8(금) ～ 8. 26(금)
								</td>
								<td class="left-article">
									수강신청서 작성(붙임2), 이메일 또는 팩스 제출(방문, 전화 가능)
									<br>
									<span class="emphasize">
										<i class="fa fa-envelope" aria-hidden="true"></i>  snsmsook@korea.kr
									</span>
									<span class="emphasize">
										<i class="fa fa-fax" aria-hidden="true"></i> 02-3677-1379 
									</span> 
									<br>
									<span class="emphasize">계좌번호 </span>농협 301-0186-0805-41 (예금주: 국립과천과학관)
									<br>
									<span class="emphasize">선착순 </span>접수 후 수강료 계좌 입금 완료 시 선착순 마감
									<br>
									* 수강료는 반드시 본인 이름으로 입금 또는 명기 요망 <br>
									* 환불은 개강일까지에 한함
								</td>
								<td class="">
									<a href="<c:url value='/education/teachersOnSite/download/plan' />" class="btn btn-blue">
										<i class="fa fa-download"></i> 수강 신청서 다운로드
									</a>
								</td>
							</tr>
						</tbody>
					</table>
					
					<ul class="circle-list text-color blue">
						<li>
							<h5 class="exp-sub-title">
								제 2기 청춘과학대학 교육과정 개요
							</h5>						
						</li>
					</ul>

					<table class="table table-blue youth-science-table" summary="과학융합탐구">
						<colgroup>
							<col style="width: 13%">
							<col style="width: 17%">
							<col style="width: 70%">
						</colgroup>
						<thead>
							<tr>
								<th class="first">과정</th>
								<th class="col">주제 및 강사</th>
								<th class="col">강좌 개요</th>
							</tr>
						</thead>
						<tbody>
							<tr class="first-row">
								<td class="first" rowspan="4">건강과학</td>
								<td>소중한 사람과 행복한 소통</td>
								<td class="left-article">
									왜 소통이 어려운가?<br>
									감정으로 소통하는가? 감성으로 소통하는가? 소중한 사람과 잘 소통하는 방법에 대해 알아본다.
								</td>
							</tr>
							<tr>
								<td>과학과 춤<br>(물리학, 운동역학)</td>
								<td class="left-article">
									운동 역학, 무용음악론에 대한 이해를 바탕으로 아르헨티나땅고 춤(커플댄스) 동작의 과학적 이해와 체험을 해본다.
								</td>
							</tr>
							<tr  class="last-row">
								<td>
									건강한 생활과 생태체험 오감활동
								</td>
								<td class="left-article">
									국립생태원, 국립생물자원관<br>(충남, 서천)
								</td>
							</tr>
							<tr>
								<td>해독과 건강 (장이 건강해야 뇌가 건강하다)</td>
								<td class="left-article">
									미세먼지, 스트레스, 오염된 먹거리 등 독성이 어떻게 질병을 일으키며, 그에 대한 해독방법은 무엇인지 알아본다.
								</td>
							</tr>
							<tr class="first-row">
								<td class="first" rowspan="4">
									전통과학
								</td>
								<td>
									전통음료-와인과 과학
								</td>
								<td class="left-article">
									와인 생산지와 자연을 이해하고, 와인과 음식의 매칭, 와인과 건강 등 와인을 활용하고 또한 즐길 수 있는 법을 알아본다.
								</td>
							</tr>
							<tr>
								<td>
									우리나라 한지와 전통공예
								</td>
								<td class="left-article">
									한지의 역사와 종이문화를 이해하고, 한지 원료인 닥나무의 특성과 우수성, 미래산업으로서의 가치를 고찰해 본다.
								</td>
							</tr>
							<tr  class="last-row">
								<td>
									전통생활모습 및 풍물체험
								</td>
								<td class="left-article">
									헤이리 도자기 체험학교, 한국근현대사박물관<br>(경기도 파주)
								</td>
							</tr>
							<tr>
								<td>
									과학으로 풀어보는 민속<br>(농경세시와 마을신앙)
								</td>
								<td class="left-article">
									농경문화와 농경제례, 마을신앙을 생활과학적인 측면에서 살펴보고 그 속에 내재된 우리의 정신문화를 총체적으로 이해한다.
								</td>
							</tr>
							<tr class="first-row">
								<td class="first" rowspan="4">
									첨단과학
								</td>
								<td>
									유비쿼터스와 우리의 미래 사회
								</td>
								<td class="left-article">
									사람, 사물, 공간을 초연결하여 산업구조 및 사회 시스템을 혁신시키는 “4차산업혁명”과 인공지능에 대해 알아본다. 
								</td>
							</tr>
							<tr class="last-row">
								<td>
									첨단 우주과학체험과 허브용품 제작 체험
								</td>
								<td class="left-article">
									포천아트벨리천문과학관, 포천허브아일랜드<br>(경기도 포천)  
								</td>
							</tr>
							<tr>
								<td>
									인공지능 알파고와 바둑기사의 대결
								</td>
								<td class="left-article">
									치매예방에 좋은 바둑의 기초원리를 이해하고 인공지능 알파고의 기보 읽기를 통해 과학과 바둑의 역사적 조우를 되새긴다. 
								</td>
							</tr>
							<tr>
								<td>
									사진으로 느껴보는 우주
								</td>
								<td class="left-article">
									우주는 얼마나 넓고 오래되었을까? 우주의 천문학적인 규모에 대하여 인류가 알고 있는 우주의 끝까지 살펴본다.
								</td>
							</tr>
							<tr class="first-row last-row">
								<td class="first" rowspan="4">
									생활	
								</td>
								<td>
									자생식물의 중요성과 생활 체험
								</td>
								<td class="left-article">
									전주수목원, 전주 한옥마을<br>(전북 전주) 
								</td>
							</tr>
							<tr>
								<td>
									살인사건 수사와 법의학
								</td>
								<td class="left-article">
									조선왕조에서 발생한 살인 범죄에 대해 관리들의 법의학 지식의 수준은 어떠했는지 현대적 관점에서 살펴본다.   
								</td>
							</tr>
							<tr>
								<td>
									인생 100세 시대의 생애설계
								</td>
								<td class="left-article">
									인생100세 시대를 맞아 건강문제, 자녀문제, 퇴직 후에 할 일 등을 종합적으로 점검 대응해 나가는 방법을 소개한다.   
								</td>
							</tr>
							<tr>
								<td>
									날씨가 미쳤다
								</td>
								<td class="left-article">
									날씨가 우리의 생활과 일상에 어떠한 영향을 미쳤고, 현재 그리고 앞으로 어떤 영향을 미칠 것인지 같이 생각해 본다.
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>