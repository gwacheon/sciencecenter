<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-blue">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>

	<div class="sub-body">
		<div class="narrow-sub-top education">
			<div class="sub-banner-tab-wrapper sub-banner-tab-blue">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<ul class="nav nav-tabs">
								<li class="col-xs-6 active">
									<a href="#" class="item">교사 현장연수</a>
								</li>
		        				<li class="tab col-xs-6">
		        					<a href="<c:url value='/education/teachersNTTP' />" class="item">NTTP교사 위탁연수</a>
		       					</li>
							</ul>   
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div id="edu-exp-body" class="container">
			
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<h3 class="program-title">
						프로그램 안내
					</h3>
				</div>
			</div>
				
			<div class="row">
				<div class="col-md-12">
					<ul class="circle-list text-color blue">
						<li>
							<h5 class="exp-sub-title">
								직무연수
							</h5>
						</li>
					</ul>
					
					<div class="bordered-div bordered-blue margin-bottom30">
						<div class="row">
							<div class="col-md-9">
								2016년 하반기 직무연수가 준비 중에 있습니다. 추후 공지하겠습니다. 
							</div>							
						</div>
					</div>
					
					<ul class="circle-list text-color blue">
						<li>
							<h5 class="exp-sub-title">
								현장연수
							</h5>
						</li>
					</ul>
					
					<div class="bordered-div bordered-blue margin-bottom30">
						<div class="row">
							<div class="col-md-12">
								과천과학관의 첨단전시시설 및 기반환경을 교원이 적극적으로 활용할 수 있도록 단기(1일) 연수프로그램을 안내 지원하고 있습니다. 교육청 및 개별학교의 요청에 따라 세부내용(일시 및 시간 등)을 조정하여 연중 수시로 운영됩니다. 

							</div>
						</div>
					</div>
					
					<div class="contact-info margin-bottom30">
						<div class="circle-bg circle-bg-blue">
							?
						</div>
						<div class="header">
							문의사항 :
						</div>
						<div class="detail">
							 과학탐구교육과
						</div>
						<div class="call-number">
							 <i class="fa fa-phone blue"></i> 02-3677-1521
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>