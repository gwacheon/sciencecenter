<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="communication-library">
	<div class="sub-content-nav scrollspy-blue">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
    </div>
	<div id="support-body" class="sub-body">
		<div id="basic-info-spy" class="narrow-sub-top education">
			<div class="sub-banner-tab-wrapper sub-banner-tab-blue">
				<div class="container">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
						  
						</div>
					</div>
				</div>
			</div>
		</div>
		<div  class="container">
			<div id="board-spy" class="row">
				<div class="col-md-12">
					<h3 class="page-title blue">
						<div class="top-line"></div>
						 교육 공지사항
					</h3>
				</div>
			</div>
			<div id="board-show" class="board-show-blue">
			
			</div>
		</div> 
	</div>
</div> 

<c:import url="/WEB-INF/views/boards/show.jsp"></c:import>

<script type="text/javascript">
var board = new Board(
        "#board-show",
        "<c:url value='/education/educationNotice'/>",
        {
            id: <c:out value="${id}"/>,
        }
    );
board.renderBoardShow();
</script>