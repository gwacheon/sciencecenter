<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-green">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
		<%-- <nav id="scrollspy-nav" class="navbar-default navbar-static has-scroll">
			<div class="container">
				<ul class="nav navbar-nav">
					<c:import url="/WEB-INF/views/guide/navbar.jsp"/>
					<li>
						<a href="#recommand-spy" class="item">
							추천관람코스
						</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<div class="bread-crumbs">
							전시관 /  추천관람코스
						</div>
					</li>
				</ul>
			</div>
		</nav> --%>
	</div>
	
	<div id="display-body" class="sub-body">
		<div class="narrow-sub-top guide">
			<div class="sub-banner-tab-wrapper sub-banner-tab-green">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<ul class="nav nav-tabs">
								<li class="col-xs-12 col-sm-6">
									<a href="<c:url value='/guide/totalGuide?scrollspyName=total-map-spy' />" class="item">
										<i class="fa fa-map-o fa-lg" aria-hidden="true"></i> 전시관 지도 보기
									</a>		
								</li>
								<li class="col-xs-12 col-sm-6">
									<a href="<c:url value='/display/download/course'/>" class="item">
										<i class="fa fa-download fa-lg" aria-hidden="true"></i> 전체관람코스 다운로드
									</a>
								</li>
							</ul>  
						</div>
					</div>
				</div>
			</div>			
		</div>
		<div class="container">
			<div class="row">
				<div class="col-xs-4 col-md-2">
					<h3 class="page-title green">
						<div class="top-line"></div>
						추천 관람 코스
					</h3>
				</div>
			</div>
			<div class="row description">
				<div class="col-md-12">
					<ul class="circle-list green">
						<li>
							기본 관람 코스
							<ul>
								<li>
									어린이탐구체험관 <i class="fa fa-arrow-right" aria-hidden="true"></i>  
									기초과학관 <i class="fa fa-arrow-right" aria-hidden="true"></i>
									자연사관 <i class="fa fa-arrow-right" aria-hidden="true"></i>
									전통과학관 <i class="fa fa-arrow-right" aria-hidden="true"></i>
									첨단기술관1 <i class="fa fa-arrow-right" aria-hidden="true"></i>
									첨단기술관2 <i class="fa fa-arrow-right" aria-hidden="true"></i>
									프론티어창작관
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
			<div class="row recommend-choose-box margin-bottom20">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 recommend-choose-wrapper">
					<div class="recommend-choose-detail">
						<div class="header">
							<strong>Step01 시간별</strong> 
						</div>
						<div class="sub-header">
							원하시는 시간코스를 선택해주세요
						</div>
						<div class="row content">
							<div id="recommend-2hours" data-id="1" class="col-xs-4 col-sm-4 recommend-time active">
								     
							</div>
							<div id="recommend-4hours" data-id="2" class="col-xs-4 col-sm-4 recommend-time">
								
							</div>
							<div id="recommend-1day" data-id="3" class="col-xs-4 col-sm-4 recommend-time">
								
							</div>   
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 recommend-choose-wrapper">
					<div class="recommend-choose-detail">
						<div class="header">
							<strong>Step02 대상별</strong> 
						</div>
						<div class="sub-header">
							해당되는 대상을 선택해주세요
						</div> 
						<div class="content">
							<div class="recommend-age-wrapper">
								<div data-id="1" class="recommend-age age-level1 active">				
									<div class="age-box">
									</div>
									<div class="age-label">
										유아
									</div>
								</div>
								<div data-id="2" class="recommend-age age-level2">
									<div class="age-box">
									</div>
									<div class="age-label">
										<div>초등학교</div>
										<div>(~3학년)</div>
									</div>
								</div>
								<div data-id="3" class="recommend-age age-level3">
									<div class="age-box">
									</div>
									<div class="age-label">
										<div>초등학교</div>
										<div>(4학년~중학생)</div>
									</div>
								</div>
								<div data-id="4" class="recommend-age age-level4">
									<div class="age-box">
									</div>
									<div class="age-label">
										고등학생
									</div>
								</div>
								<div data-id="5" class="recommend-age age-level5">
									<div class="age-box">
									</div>
									<div class="age-label">
										성인
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 recommend-arrow-wrapper">
					<div class="left-diagonal-line">
					</div>
					<div class="right-diagonal-line">
					</div>
				</div>
			</div>
			<div id="rec-show-1-1" class="recommend-show-wrapper">
				<div class="col-sm-12 recommend-title">
					<h5 class="doc-title green">
						2시간 유아 코스
					</h5>
				</div>
				<div class="col-sm-12 recommend-sub-title">
					<div class="sub-title-content">
						전시관을 클릭하면 추천전시관의 위치를 알 수 있습니다.
					</div>
					<div class="sub-title-content">
					전시물을 클릭하면 해당 전시품에 대한 설명을 보실 수 있습니다.
					</div>
				</div>
				<div class="col-sm-12 recommend-course-detail-wrapper">
					<div class="row course-name">
						<strong>A 코스</strong>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>	
								<div class="title">
									<a href="">어린이탐구체험관</a>
								</div>
								<div class="detail">
									나를 따르는 나비 - 빛을 만들자 - 신기한 인체나라 - (생각놀이터)오르락 내리락 - (생각놀이터)물의 힘으로 - 창작놀이방 - 3D영상관 - 나는 천하장사
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#exhibitionMapModal" class="modal-trigger" 
										data-url="http://www.sciencecenter.go.kr/gnsm_web/images/favorit/H2/bb_A_2.jpg">
										첨단기술관
									</a>
								</div>
								<div class="detail">
									로봇공연장 - 태양광자동차 - 에너지수확기술
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">자연사관</a>
								</div>
								<div class="detail">
									백악기 아시아의 공룡낙원 - 화려한 지배자 공룡 -사라져버린 대형 포유류 - 신생대의 환경과 생물 - 한반도의 육상생테계 - 한반도의 해양생물 - 영화속 물고기들 - 닥터피쉬
								</div>								
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12 recommend-course-detail-wrapper">
					<div class="row course-name">
						<strong>B 코스</strong>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">어린이탐구체험관</a>
								</div>
								<div class="detail">
									나를 따르는 나비 - 빛을 만들자 - 신기한 인체나라 - (생각놀이터)오르락 내리락 - (생각놀이터)물의 힘으로 - 창작놀이방 - 3D영상관 - 나는 천하장사
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">곤충생태관</a>
								</div>
								<div class="detail">
									육상수서곤충 - 물고기 밥주기 - 곤충소리 듣기 - 곤충체험장(장수풍뎅이,수서곤충체험) - 나비정원 - 거미
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">생태공원</a>   
								</div>
								<div class="detail">
									생태연못 - 정화연못 - 자생야생화원
								</div>								
							</div>
						</div>
					</div>  
				</div>
			</div>
			
			<div id="rec-show-1-2" class="recommend-show-wrapper">
				<div class="col-sm-12 recommend-title">
					<h5 class="doc-title green">2시간 초등학교(~3학년) 코스</h5>
				</div>
				<div class="col-sm-12 recommend-sub-title">
					<div class="sub-title-content">
						전시관을 클릭하면 추천전시관의 위치를 알 수 있습니다.
					</div>
					<div class="sub-title-content">
					전시물을 클릭하면 해당 전시품에 대한 설명을 보실 수 있습니다.
					</div>
				</div>
				<div class="col-sm-12 recommend-course-detail-wrapper has-top-line">
					<div class="top-line green"></div>
					<div class="row course-name">
						<strong>A 코스</strong>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>	
								<div class="title">
									어린이탐구체험관
								</div>
								<div class="detail">
									빛을 만들자 - 감각놀이터 - (생각놀이터)물의 힘으로 - 
									(생각놀이터)기울어진방 - (생각놀이터)아트스튜디오 - 
									(생각놀이터)빛은 마술사 - 나는 천하장사
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>	
								<div class="title">
									기초과학관
								</div>
								<div class="detail">
									테슬라코일 - 과학자가 바꾼 세상 - 파스칼의 삼각형 - 플라즈마의 세계 - 태풍체험 - 지진체험
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">첨단기술관</a>
								</div>
								<div class="detail">
									로봇공연장 - 뇌파는마술사 - 에너지수확기술 - 자기부상열차
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">자연사관</a>
								</div>
								<div class="detail">
									한반도 지질여행 - 백악기 아시아의 공룡낙원 - 화려한 지배자 공룡 - 한반도의 습지생태계 - 
									상류·중류·하류에 사는 물고기 - 한반도의 해양생물 - 닥터피시 - 어둠속의 생물
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">전통과학관</a>
								</div>
								<div class="detail">
									동서양별자리 - 해시계 - 자격루 - 대동여지도 - 기리고차 - 봉수대 - 
									3D거북선 체험 or 학익진 체험 - 디지털 한글놀이 체험
								</div>								
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12 recommend-course-detail-wrapper">
					<div class="row course-name">
						<strong>B 코스</strong>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">곤충생태관</a>
								</div>
								<div class="detail">
									육상·수서곤충 - 물고기 밥주기 - 곤충의 한 살이 - 곤충 표본 - 곤충소리 듣기 - 
									곤충체험장(장수풍뎅이, 수서곤충체험) - 나비정원 - 거미
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">생태공원</a>   
								</div>
								<div class="detail">
									생태연못 - 정화연못 - 자생야생화원
								</div>								
							</div>
						</div>
					</div>  
				</div>
			</div>
			
			<div id="rec-show-1-3" class="recommend-show-wrapper">
				<div class="col-sm-12 recommend-title">
					<h5 class="doc-title green">2시간 초등학교(4학년) ~ 중학생 코스</h5>
				</div>
				<div class="col-sm-12 recommend-sub-title">
					<div class="sub-title-content">
						전시관을 클릭하면 추천전시관의 위치를 알 수 있습니다.
					</div>
					<div class="sub-title-content">
					전시물을 클릭하면 해당 전시품에 대한 설명을 보실 수 있습니다.
					</div>
				</div>
				<div class="col-sm-12 recommend-course-detail-wrapper">
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>	
								<div class="title">
									기초과학관
								</div>
								<div class="detail">
									테슬라코일 - 과학자가 바꾼 세상 - 카오스 수차 - 극성 무극성 공유결합 - 
									생식과 발생 - 지각변동과 판구조론 - 지진체험
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">첨단기술관</a>
								</div>
								<div class="detail">
									로봇공연장 - 뇌파는마술사 - 미래디지털방송 - 에너지수확기술 - 
									항공기비행원리 - 스페이스캠프
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">자연사관</a>
								</div>
								<div class="detail">
									밤하늘의 수수께끼 - 수수께끼의 열쇠 - 운석 - 한반도의 고생대 - 화려한 지배자 공룡 - 
									사라져버린 대형 포유류 - 신생대의 환경과 생물 - 제주도의 인류흔적 - 
									한반도의 특이식물 - 독도의 생태계
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">전통과학관</a>
								</div>
								<div class="detail">
									동서양별자리 - 대동여지도 - 기리고차 - 봉수대 - 성덕대왕신종 - 거중기 - 도량형과 단위 - 
									동서양 노젓기체험 - 3D거북선 체험 - 학익진 체험 - 디지털 한글놀이 체험
								</div>								
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div id="rec-show-1-4" class="recommend-show-wrapper">
				<div class="col-sm-12 recommend-title">
					<h5 class="doc-title green">2시간 고등학생 코스</h5>
				</div>
				<div class="col-sm-12 recommend-sub-title">
					<div class="sub-title-content">
						전시관을 클릭하면 추천전시관의 위치를 알 수 있습니다.
					</div>
					<div class="sub-title-content">
					전시물을 클릭하면 해당 전시품에 대한 설명을 보실 수 있습니다.
					</div>
				</div>
				<div class="col-sm-12 recommend-course-detail-wrapper">
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>	
								<div class="title">
									기초과학관
								</div>
								<div class="detail">
									테슬라코일 - 과학자가 바꾼 세상 - 파스칼의 삼각형 - 안개상자 - 에어테이블 -
									수파발생장치 - 빛의 파동적 성질 - 타코마브릿지 붕괴사건
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">첨단기술관</a>
								</div>
								<div class="detail">
									로봇공연장 - 무균돼지 - 미래디지털방송 - 무선전력송출 - 항공기비행원리 - 
									스페이스캠프 - 로켓엔진 - ISS - 스트레스를 날리자
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">자연사관</a>
								</div>
								<div class="detail">
									밤하늘의 수수께끼 - 별의 일생 - 원시지구의 진화 - 생명의 기원 - 
									한반도의 생성과 진화 - 한반도의 고생대 - 생물의 육상진출 - 
									백악기 아시아의 공룡낙원 - 신생대의 환경과 생물 - 한반도의 신생대 동식물 화석 - 
									인류의 진화 - 우포늪
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">전통과학관</a>
								</div>
								<div class="detail">
									동서양별자리 - 자격루 - 대동여지도 - 기리고차 - 봉수대 - 성덕대왕신종 - 
									동서양노젓기체험 - 3D거북선 체험 - 학익진 체험 - 신기전 - 비거
								</div>								
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div id="rec-show-1-5" class="recommend-show-wrapper">
				<div class="col-sm-12 recommend-title">
					<h5 class="doc-title green">2시간 성인 코스</h5>
				</div>
				<div class="col-sm-12 recommend-sub-title">
					<div class="sub-title-content">
						전시관을 클릭하면 추천전시관의 위치를 알 수 있습니다.
					</div>
					<div class="sub-title-content">
					전시물을 클릭하면 해당 전시품에 대한 설명을 보실 수 있습니다.
					</div>
				</div>
				<div class="col-sm-12 recommend-course-detail-wrapper">
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>	
								<div class="title">
									기초과학관
								</div>
								<div class="detail">
									테슬라코일 - 과학자가 바꾼 세상 - 파스칼의 삼각형 - 플라즈마의 세계 - 안개상자 - 
									수파발생장치 - 에어테이블 - 로켓추진체 - 타코마브릿지 붕괴사건 - 지진체험관
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">첨단기술관</a>
								</div>
								<div class="detail">
									로봇공연장 - - 미래디지털방송 - 무선전력송출 - 핵융합 - 항공기비행원리 - 로켓엔진 - 
									행글라이더 체험 - ISS - 미래선박 - 스트레스를 날리자
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">자연사관</a>
								</div>
								<div class="detail">
									밤하늘의 수수께끼 - 대한민국 남극 군석탐사대 - 생명의 기원 - 한반도의 생성과 진화 - 
									한반도의 고생대 - 생물의 육상진출 - 백악기 아시아의 공룡낙원 - 
									한반도의 신생대 동식물 화석 - 제주도의 인류 흔적 - 한반도의 특이식물 - 
									상류·중류·하류에 사는 물고기 - 한반도의 해양생물 - 닥터피시
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">전통과학관</a>
								</div>
								<div class="detail">
									동서양별자리 - 해시계 - 자격루 - 대동여지도 - 기리고차 - 봉수대 - 맥파분석체험 - 
									성덕대왕신종 - 거중기 - 도량형과 단위 - 동서양노젓기체험 - 학익진 체험 - 
									3D거북선 체험 - 한옥 - 신기전 - 비거
								</div>								
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div id="rec-show-2-1" class="recommend-show-wrapper">
				<div class="col-sm-12 recommend-title">
					<h5 class="doc-title green">4시간 유아 코스</h5>
				</div>
				<div class="col-sm-12 recommend-sub-title">
					<div class="sub-title-content">
						전시관을 클릭하면 추천전시관의 위치를 알 수 있습니다.
					</div>
					<div class="sub-title-content">
					전시물을 클릭하면 해당 전시품에 대한 설명을 보실 수 있습니다.
					</div>
				</div>
				<div class="col-sm-12 recommend-course-detail-wrapper">
					<div class="row course-name">
						<strong>A 코스</strong>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>	
								<div class="title">
									어린이탐구체험관
								</div>
								<div class="detail">
									나를 따르는 나비 - 빛을 만들자 - 신기한 인체나라 - (생각놀이터)창작놀이방 - 
									(생각놀이터)오르락내리락 - (생각놀이터)물의 힘으로 - (생각놀이터)연주가 - 
									3D영상관 - (생각놀이터)아트스튜디오 - (생각놀이터)빛은 마술사 - 
									이상한 목소리로 말해요
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>	
								<div class="title">
									첨단기술관
								</div>
								<div class="detail">
									로봇공연장 - 태양광자동차 - 에너지수확기술 - T-50 - 부활호 - KSR-III - 달착륙선 - ISS
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										자연사관
									</a>
								</div>
								<div class="detail">
									쳉지앙 화석군 - 한반도의 고생대 - 육지로의 진출 - - 백악기 아시아의 공룡낙원 - 
									화려한 지배자 공룡 - 한반도의 육상생태계 - 상류·중류·하류에 사는 물고기 - 
									한반도의 해양생물 - 영화속 물고기들 - 닥터피시 - 어둠속의 생물들
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										전통과학관
									</a>
								</div>
								<div class="detail">
									동서양별자리 - 해시계 - 대동여지도 - 디지털 한지 체험 - 디지털 한글놀이 체험
								</div>								
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12 recommend-course-detail-wrapper">
					<div class="row course-name">
						<strong>B 코스</strong>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">어린이탐구체험관</a>
								</div>
								<div class="detail">
									나를 따르는 나비 - 빛을 만들자 - 신기한 인체나라 - (생각놀이터)창작놀이방 - 
									(생각놀이터)오르락내리락 - (생각놀이터)물의 힘으로 - (생각놀이터)연주가 - 
									3D영상관 - (생각놀이터)아트스튜디오 - (생각놀이터)빛은 마술사 - 
									이상한 목소리로 말해요
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">곤충생태관</a>   
								</div>
								<div class="detail">
									육상·수서곤충 - 물고기 밥주기 - 곤충소리 듣기 - 
									곤충체험장(장수풍뎅이, 수서곤충체험) - 나비정원 - 거미
								</div>								
							</div>
						</div>
					</div> 
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										생태공원
									</a>   
								</div>
								<div class="detail">
									생태연못 - 정화연못 - 자생야생화원
								</div>								
							</div>
						</div>
					</div>  
				</div>
			</div>
			
			<div id="rec-show-2-2" class="recommend-show-wrapper">
				<div class="col-sm-12 recommend-title">
					<h5 class="doc-title green">4시간 초등학생(~3학년) 코스</h5>
				</div>
				<div class="col-sm-12 recommend-sub-title">
					<div class="sub-title-content">
						전시관을 클릭하면 추천전시관의 위치를 알 수 있습니다.
					</div>
					<div class="sub-title-content">
					전시물을 클릭하면 해당 전시품에 대한 설명을 보실 수 있습니다.
					</div>
				</div>
				<div class="col-sm-12 recommend-course-detail-wrapper">
					<div class="row course-name">
						<strong>A 코스</strong>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>	
								<div class="title">
									어린이탐구체험관
								</div>
								<div class="detail">
									인체탐험(4D) - 신기한 인체나라 - 감각놀이터 - 과학과 미술의 만남 - 사진작가 - 
									(생각놀이터)오르락내리락 - (생각놀이터)물의 힘으로 - (생각놀이터)기울어진방 - 
									(생각놀이터)아트 스튜디오 - (생각놀이터)빛은마술사 - 이상한 목소리로 말해요 - 
									깨끗한 숲 만들기
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>	
								<div class="title">
									기초과학관
								</div>
								<div class="detail">
									테슬라코일 - 과학자가 바꾼 세상 - 파스칼의 삼각형 - 플라즈마의 세계 - 
									테슬라 코일 - 로켓추진체 - 생태계와 광합성 - 극지체험 - 태풍체험 - 지진체험
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										첨단기술관
									</a>
								</div>
								<div class="detail">
									로봇공연장 - 뇌파게임 - 초고속카메라 - 태양광자동차 - 무선전력송출 - 
									에너지수확기술 - T-50 - 부활호 - KSR-III - 달착륙선 - ISS
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										자연사관
									</a>
								</div>
								<div class="detail">
									밤하늘의 수수께끼 - 수수께끼의 열쇠 - 운석 - 한반도 지질여행 - 한반도의 고생대 - 
									화려한 지배자 공룡 - 공룡의 이모저모 - 사라져간 대형 포유류 - 신생대의 환경과 생물 - 
									한반도의 습지생태계 - 상류·중류·하류에 사는 물고기 - 한반도의 해양생물 - 
									영화속 물고기들 - 화석물고기 - 닥터피시
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										전통과학관
									</a>
								</div>
								<div class="detail">
									동서양별자리 - 자격루 - 대동여지도 - 기리고차 - 봉수대 - 성덕대왕신종 - 
									동서양노젓기체험 - 3D거북선 체험 - 학익진 체험 - 신기전 - 비거 - 디지털 한지 체험 - 
									디지털 한글놀이 체험
								</div>								
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12 recommend-course-detail-wrapper">
					<div class="row course-name">
						<strong>B 코스</strong>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">어린이탐구체험관</a>
								</div>
								<div class="detail">
									인체탐험(4D) - 신기한 인체나라 - 감각놀이터 - 과학과 미술의 만남 - 사진작가 - 
									(생각놀이터)오르락내리락 - (생각놀이터)물의 힘으로 - (생각놀이터)기울어진방 - 
									(생각놀이터)아트 스튜디오 - (생각놀이터)빛은마술사 - 
									이상한 목소리로 말해요 - 깨끗한 숲 만들기
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">곤충생태관</a>   
								</div>
								<div class="detail">
									육상·수서곤충 - 물고기 밥주기 - 곤충의 한 살이 - 곤충 표본 - 곤충소리 듣기 - 
									곤충체험장(장수풍뎅이, 수서곤충체험) - 나비정원 - 거미
								</div>								
							</div>
						</div>
					</div> 
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										생태공원
									</a>   
								</div>
								<div class="detail">
									생태연못 - 정화연못 - 자생야생화원
								</div>								
							</div>
						</div>
					</div>  
				</div>
			</div>
			
			<div id="rec-show-2-3" class="recommend-show-wrapper">
				<div class="col-sm-12 recommend-title">
					<h5 class="doc-title green">4시간 초등학생(4학년) ~ 중학생  코스</h5>
				</div>
				<div class="col-sm-12 recommend-sub-title">
					<div class="sub-title-content">
						전시관을 클릭하면 추천전시관의 위치를 알 수 있습니다.
					</div>
					<div class="sub-title-content">
					전시물을 클릭하면 해당 전시품에 대한 설명을 보실 수 있습니다.
					</div>
				</div>
				<div class="col-sm-12 recommend-course-detail-wrapper">
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>	
								<div class="title">
									기초과학관
								</div>
								<div class="detail">
									테슬라코일 - 과학자가 바꾼 세상 - 카오스 수차 - 극성 무극성 공유결합 - 
									고전역학자와 함께 - 플라즈마의 세계 - 로켓추진체 - 생식과 발생 - 
									생태계와 광합성 - 지각변동과 판구조론 - 지진체험
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">첨단기술관</a>
								</div>
								<div class="detail">
									로봇공연장 - 뇌파는마술사 - 미래디지털방송 - 컴퓨터와의대화(HCI) - 
									무선전력송출 - 에너지수확기술 - 항공기비행원리 - 항공기시뮬레이터 - 
									우주여행극장 - 스페이스캠프 - ISS
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">자연사관</a>
								</div>
								<div class="detail">
									대한민국 남극운석 탐사대 - 생명의 기원을 찾아서 - 스트로마톨라이트 - 
									한반도의 고생대 - 생물의 육상진출 - - 화려한 지배자 공룡 - 백악기 아시아의 공룡낙원 - 
									사라져간 대형 포유류 - 신생대의 환경과 생물 - 제주도의 인류흔적 - 
									생동하는 지구 SOS(예약필요) - 한반도의 습지생태계 - 상류·중류·하류에 사는 물고기 - 
									한반도의 해양생물 - 독도의 생태계
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">전통과학관</a>
								</div>
								<div class="detail">
									동서양별자리 - 자격루 - 대동여지도 - 기리고차 - 봉수대 - 성덕대왕신종 - 
									식생활 도구 - 3D거북선 체험 - 동서양노젓기체험 - 학익진 체험 - 한옥 - 신기전 - 
									비거 - 디지털 한지 체험 - 디지털 한글놀이 체험
								</div>								
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div id="rec-show-2-4" class="recommend-show-wrapper">
				<div class="col-sm-12 recommend-title">
					<h5 class="doc-title green">4시간 고등학생  코스</h5>
				</div>
				<div class="col-sm-12 recommend-sub-title">
					<div class="sub-title-content">
						전시관을 클릭하면 추천전시관의 위치를 알 수 있습니다.
					</div>
					<div class="sub-title-content">
					전시물을 클릭하면 해당 전시품에 대한 설명을 보실 수 있습니다.
					</div>
				</div>
				<div class="col-sm-12 recommend-course-detail-wrapper">
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>	
								<div class="title">
									기초과학관
								</div>
								<div class="detail">
									카오스의 수차 - 테슬라코일 - 과학자가 바꾼 세상 - 파스칼의 삼각형 - 
									극성 무극성 공유결합 - 플라즈마의 세계 - 안개상자 - 수파발생장치 - 에어테이블 - 
									로켓추진체 - 타코마브릿지 붕괴사건 - 빛의 파동적 성질 - 지각변동과 판구조론
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">첨단기술관</a>
								</div>
								<div class="detail">
									로봇공연장 or 뇌파는마술사 - 무균돼지 - 미래디지털방송 - 컴퓨터와의대화(HCI) - 
									무선전력송출 - 에너지수확기술 - 핵융합 - 항공기비행원리 - 항공기시뮬레이터 - 
									로켓엔진 - 우주여행극장 - 스페이스캠프 - ISS - 미래선박 - 스트레스를 날리자
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">자연사관</a>
								</div>
								<div class="detail">
									수수께끼의 열쇠 - 운석 - 태양계와 지구의 탄생 - 생명의 기원을 찾아서 - 
									스트로마톨라이트 - 한반도의 고생대 - 쳉지앙 화석군 - 어류의 진화 - 양서류의 진화 - 
									생물의 육상진출 - 백악기 아시아의 공룡낙원 - 사라져간 대형 포유류 - 
									신생대의 환경과 생물 - 한반도의 신생대 동식물 화석 - 인류의 진화 - 
									생동하는 지구, SOS(예약필요) - 한반도의 육상생태계 - 상류·중류·하류에 사는 물고기 - 
									한반도의 해양생물 - 독도의 생태계
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">전통과학관</a>
								</div>
								<div class="detail">
									동서양별자리 - 자격루 - 대동여지도 - 기리고차 - 봉수대 - 정낭과 정주석 - 
									한의원 - 법의학 - 맥파분석체험 - 성덕대왕신종 - 천연염색 - 거중기 - 도자기와 가마 - 
									동서양노젓기체험 - 학익진체험 - 3D거북선 체험 - 한옥 - 신기전 - 비거
								</div>								
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div id="rec-show-2-5" class="recommend-show-wrapper">
				<div class="col-sm-12 recommend-title">
					<h5 class="doc-title green">4시간 성인 코스</h5>
				</div>
				<div class="col-sm-12 recommend-sub-title">
					<div class="sub-title-content">
						전시관을 클릭하면 추천전시관의 위치를 알 수 있습니다.
					</div>
					<div class="sub-title-content">
					전시물을 클릭하면 해당 전시품에 대한 설명을 보실 수 있습니다.
					</div>
				</div>
				<div class="col-sm-12 recommend-course-detail-wrapper">
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>	
								<div class="title">
									기초과학관
								</div>
								<div class="detail">
									카오스의 수차 - 테슬라코일 - 과학자가 바꾼 세상 - 파스칼의 삼각형 - 
									극성 무극성 공유결합 - 플라즈마의 세계 - 안개상자 - 수파발생장치 - 에어테이블 - 
									로켓추진체 - 타코마브릿지 붕괴사건 - 생태계와 광합성 - 지진체험관
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">첨단기술관</a>
								</div>
								<div class="detail">
									로봇공연장 - 뇌파는마술사 - 미래디지털방송 - 컴퓨터와의대화(HCI) - 
									무선전력송출 - 에너지수확기술 - 핵융합 - 항공기비행원리 - 행글라이더 체험 - 
									로켓엔진 - ISS - 미래선박 - 스트레스를 날리자
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">자연사관</a>
								</div>
								<div class="detail">
									밤하늘의 수수께끼 - 태양계와 지구의 탄생 - 수수께끼의 열쇠 - 운석 - 
									스트로마톨라이트 - 쳉지앙 화석군 - 한반도의 고생대 - 양서류의 진화 - 
									중생대의 바다 - 백악기 아시아의 공룡낙원 - 사라져간 대형포유류 - 
									신생대의 환경과 생물 - 한반도의 신생대 동식물 화석 - 
									생동하는 지구, SOS(예약필요) - 한반도의 육상생태계 - 한반도의 습지생태계 - 
									상류·중류·하류에 사는 물고기 - 한반도의 해양생물 - 닥터피시 - 독도의 생태계
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">전통과학관</a>
								</div>
								<div class="detail">
									동서양별자리 - 자격루 - 대동여지도 - 기리고차 - 봉수대 - 정낭과 정주석 - 
									한의원 - 법의학 - 맥파분석체험 - 성덕대왕신종 - 식생활 도구 - 천연염색 - 거중기 - 
									도량형과 단위 - 도자기와 가마 - 동서양노젓기체험 - 학익진체험 - 3D거북선 체험 - 
									한옥 - 신기전 - 비거
								</div>								
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div id="rec-show-3-1" class="recommend-show-wrapper">
				<div class="col-sm-12 recommend-title">
					<h5 class="doc-title green">1일 유아 코스</h5>
				</div>
				<div class="col-sm-12 recommend-sub-title">
					<div class="sub-title-content">
						전시관을 클릭하면 추천전시관의 위치를 알 수 있습니다.
					</div>
					<div class="sub-title-content">
					전시물을 클릭하면 해당 전시품에 대한 설명을 보실 수 있습니다.
					</div>
				</div>
				<div class="col-sm-12 recommend-course-detail-wrapper">
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>	
								<div class="title">
									어린이탐구체험관
								</div>
								<div class="detail">
									나를 따르는 나비 - 빛을 만들자 - 신기한 인체나라 - 감각놀이터 - 
									(생각놀이터)창작놀이방 - (생각놀이터)오르락내리락 - (생각놀이터)물의 힘으로 - 
									(생각놀이터)연주가 - (생각놀이터)물속음악대 - 3D영상관 - (생각놀이터)아트스튜디오 - 
									(생각놀이터)빛은마술사 - 이상한 목소리로 말해요 - 깨끗한 숲 만들기
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>	
								<div class="title">
									첨단기술관
								</div>
								<div class="detail">
									로봇공연장 - 태양광자동차 - 에너지수확기술
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										자연사관
									</a>
								</div>
								<div class="detail">
									한반도의 고생대 - 육지로의 진출 - - 백악기 아시아의 공룡낙원 - 화려한 지배자 공룡 - 
									공룡의 이모저모 - 포유류의 시대 - 빙하기의 생물들 - 한반도의 온대림 - 습지 생태계 - 
									담수생태계 - 해양생태계 - 영화속 물고기들 - 닥터피시 - 어둠속의 생물들
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										전통과학관
									</a>
								</div>
								<div class="detail">
									동서양 별자리 - 봉수대 - 3D거북선 - 성덕대왕신종 - 디지털 한지 체험 - 
									디지털 한글놀이 체험
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										곤충생태관
									</a>
								</div>
								<div class="detail">
									육상·수서곤충 - 물고기 밥주기 - 곤충소리 듣기 - 곤충체험장(장수풍뎅이, 수서곤충체험) - 
									나비정원 - 거미
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										생태공원
									</a>
								</div>
								<div class="detail">
									생태연못 - 푸른향기원 - 자원식물원 - 자생야생화원
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										야외전시장
									</a>
								</div>
								<div class="detail">
									나로호 - 공룡동산
								</div>								
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div id="rec-show-3-2" class="recommend-show-wrapper">
				<div class="col-sm-12 recommend-title">
					<h5 class="doc-title green">1일 초등학생(~3학년) 코스</h5>
				</div>
				<div class="col-sm-12 recommend-sub-title">
					<div class="sub-title-content">
						전시관을 클릭하면 추천전시관의 위치를 알 수 있습니다.
					</div>
					<div class="sub-title-content">
					전시물을 클릭하면 해당 전시품에 대한 설명을 보실 수 있습니다.
					</div>
				</div>
				<div class="col-sm-12 recommend-course-detail-wrapper">
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>	
								<div class="title">
									어린이탐구체험관
								</div>
								<div class="detail">
									인체탐험(4D) - 신기한 인체나라 - 감각놀이터 - 과학과 미술의 만남 - 사진작가 - 
									(생각놀이터)오르락내리락 - (생각놀이터)물의 힘으로 - (생각놀이터)물속음악대 - 
									(생각놀이터)연주가 - (생각놀이터)호기심방_교육프로그램 - (생각놀이터)기울어진방 - 
									(생각놀이터)아트 스튜디오 - (생각놀이터)빛은 마술사 - 이상한 목소리로 말해요 - 
									깨끗한 숲 만들기
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>	
								<div class="title">
									기초과학관
								</div>
								<div class="detail">
									테슬라코일 - 과학자가 바꾼 세상 - 파스칼의 삼각형 - 플라즈마의 세계 - 로켓추진체 - 
									생태계와 광합성 - 극지체험 - 태풍체험 - 지진체험
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										첨단기술관
									</a>
								</div>
								<div class="detail">
									로봇공연장 - 뇌파는마술사 - 태양광자동차 - 무선전력송출 - 에너지수확기술 - 
									항공기비행원리 - 행글라이더 체험
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										자연사관
									</a>
								</div>
								<div class="detail">
									밤하늘의 수수께끼 - 수수께끼의 열쇠 - 운석 - 한반도 지질여행 - 한반도의 고생대 - 
									생물의 육상진출 - 백악기 아시아의 공룡낙원 - 화려한 지배자 공룡 - 공룡의 이모저모 - 
									사라져간 대형 포유류 - 신생대의 환경과 생물 - 제주도의 인류흔적 - 
									한반도의 육상생태계 - 한반도의 습지생태계 - 한반도의 특이식물 - 
									상류·중류·하류에 사는 물고기 - 한반도의 해양생물 - 영화속 주인공들 - 화석물고기 - 
									닥터피시 - 독도의 생태계 - 어둠속의 생물들
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										전통과학관
									</a>
								</div>
								<div class="detail">
									앙부일구 - 대동여지도 - 한옥 - 발효과학 - 3D거북선 - 동서양노젓기 체험 - 
									학익진 전법 체험 - 성덕대왕신종 - 디지털 한지 체험 - 디지털 한글놀이 체험
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										곤충생태관
									</a>
								</div>
								<div class="detail">
									육상·수서곤충 - 곤충의 한 살이 - 곤충의 세계 - 곤충소리 듣기 - 
									곤충체험장(장수풍뎅이, 수서곤충체험) - 자연물 곤충만들기 - 나비정원 - 거미
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										생태공원
									</a>
								</div>
								<div class="detail">
									생태연못 - 푸른향기원 - 단풍원 - 이야기원 - 자원식물원 - 자생야생화원
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										야외전시장
									</a>
								</div>
								<div class="detail">
									나로호 - 공룡동산
								</div>								
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div id="rec-show-3-3" class="recommend-show-wrapper">
				<div class="col-sm-12 recommend-title">
					<h5 class="doc-title green">1일 초등학생(4학년) ~ 중학생 코스</h5>
				</div>
				<div class="col-sm-12 recommend-sub-title">
					<div class="sub-title-content">
						전시관을 클릭하면 추천전시관의 위치를 알 수 있습니다.
					</div>
					<div class="sub-title-content">
					전시물을 클릭하면 해당 전시품에 대한 설명을 보실 수 있습니다.
					</div>
				</div>
				<div class="col-sm-12 recommend-course-detail-wrapper">
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>	
								<div class="title">
									기초과학관
								</div>
								<div class="detail">
									테슬라코일 - 과학자가 바꾼 세상 - 달리는 기차안에서 공던지기 - 카오스 수차 - 
									극성 무극성 공유결합 - 고전역학자와 함께 - 플라즈마의 세계 - 로켓추진체 - 
									생식과 발생 - 생태계와 광합성 - 지각변동과 판구조론 - 지진체험
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>	
								<div class="title">
									첨단기술관
								</div>
								<div class="detail">
									로봇공연장 - 뇌파는마술사 - 미래디지털방송 - 컴퓨터와의대화(HCI) - 무선전력송출 - 
									에너지수확기술 - 항공기비행원리 - 항공기시뮬레이터 - 로켓엔진 - 스페이스캠프 등
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										자연사관
									</a>
								</div>
								<div class="detail">
									밤하늘의 수수께끼 - 수수께끼의 열쇠 - 운석 - 생명의 기원을 찾아서 - 
									스트로마톨라이트 - 한반도의 고생대 - 생물의 육상진출 - 중생대의 바다 - 
									화려한 지배자 공룡 - 백악기 아시아의 공룡낙원 - 공룡의 이모저모 - 사라져간 대형 포유류 - 
									생동하는 지구, SOS(예약필요) - 한반도의 육상생태계 - 한반도의 습지생태계 - 
									한반도의 특이식물 - 밤하늘의 수수께끼 - 수수께끼의 열쇠 - 운석 - 한반도 지질여행 - 
									상류·중류·하류에 사는 물고기 - 한반도의 해양생물 - 영화속 주인공들 - 화석물고기 - 
									닥터피시 - 독도의 생태계
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										전통과학관
									</a>
								</div>
								<div class="detail">
									앙부일구 - 봉수대 - 한옥 - 3D거북선 - 동서양노젓기체험 - 학익진전법 체험 - 
									성덕대왕신종 - 화약무기 - 디지털 한지 체험 - 디지털 한글놀이 체험
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										곤충생태관
									</a>
								</div>
								<div class="detail">
									육상·수서곤충 - 곤충의 한 살이 - 곤충의 세계 - 곤충소리 듣기 - 
									곤충체험장(장수풍뎅이, 수서곤충체험) - 나비정원 - 
									거미
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										생태공원
									</a>
								</div>
								<div class="detail">
									생태연못 - 푸른향기원 - 단풍원 - 이야기원 - 자원식물원 - 자생야생화원
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										야외전시장
									</a>
								</div>
								<div class="detail">
									나로호 - 태양광발전기 - 지질동산
								</div>								
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div id="rec-show-3-4" class="recommend-show-wrapper">
				<div class="col-sm-12 recommend-title">
					<h5 class="doc-title green">1일 고등학생 코스</h5>
				</div>
				<div class="col-sm-12 recommend-sub-title">
					<div class="sub-title-content">
						전시관을 클릭하면 추천전시관의 위치를 알 수 있습니다.
					</div>
					<div class="sub-title-content">
					전시물을 클릭하면 해당 전시품에 대한 설명을 보실 수 있습니다.
					</div>
				</div>
				<div class="col-sm-12 recommend-course-detail-wrapper">
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>	
								<div class="title">
									기초과학관
								</div>
								<div class="detail">
									카오스의 수차 - 테슬라코일 - 과학자가 바꾼 세상 - 파스칼의 삼각형 - 
									극성 무극성 공유결합 - 플라즈마의 세계 - 안개상자 - 수파발생장치 - 에어테이블 - 
									로켓추진체 - 타코마브릿지 붕괴사건 - 빛의 파동적 성질 - 지각변동과 판구조론 - 지진체험
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>	
								<div class="title">
									첨단기술관
								</div>
								<div class="detail">
									로봇공연장 - - 무균돼지 - 뇌파는마술사 - 미래디지털방송 - 컴퓨터와의대화(HCI) - 
									무선전력송출 - 신재생에너지 - 핵융합 - 항공기비행원리 - 비행 시뮬레이터 - 
									스페이스캠프 - 발사통제센터 - 자기부상열차
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										자연사관
									</a>
								</div>
								<div class="detail">
									태양계와 지구의 탄생 - 생명의 기원을 찾아서 - 스트로마톨라이트 - 
									한반도의 고생대 - 어류의 진화 - 양서류의 진화 - 백악기 아시아의 공룡낙원 - 
									화려한 지배자 공룡 - 사라져간 대형 포유류 - 신생대의 환경과 생물 - 
									한반도의 신생대 동식물 화석 - 생동하는 지구, SOS(예약필요) - 한반도의 육상생태계 - 
									한반도의 습지생태계 - 한반도의 특이식물 - 상류·중류·하류에 사는 물고기 - 
									한반도의 해양생물 - 영화속 주인공들 - 화석물고기 - 닥터피시 - 독도의 생태계
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										전통과학관
									</a>
								</div>
								<div class="detail">
									동서양 별자리 - 앙부일구 - 자격루 - 대동여지도 - 기리고차 - 봉수대 - 
									성덕대왕신종 - 식생활 도구 - 한옥 - 3D거북선 - 동서양노젓기체험 - 
									학익진전법 체험 - 성덕대왕신종 - 화약무기 - 한옥 - 신기전 - 비거 - 
									디지털 한지체험 - 디지털 한글놀이
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										곤충생태관
									</a>
								</div>
								<div class="detail">
									육상·수서곤충 - 곤충의 세계 - 곤충체험장(장수풍뎅이, 수서곤충체험) - 
									나비정원 - 거미
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										생태공원
									</a>
								</div>
								<div class="detail">
									생태연못 - 푸른향기원 - 단풍원 - 이야기원 - 자원식물원 - 자생야생화원
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										야외전시장
									</a>
								</div>
								<div class="detail">
									나로호 - 역사의 광장 - 지질동산
								</div>								
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div id="rec-show-3-5" class="recommend-show-wrapper">
				<div class="col-sm-12 recommend-title">
					<h5 class="doc-title green">1일 성인 코스</h5>
				</div>
				<div class="col-sm-12 recommend-sub-title">
					<div class="sub-title-content">
						전시관을 클릭하면 추천전시관의 위치를 알 수 있습니다.
					</div>
					<div class="sub-title-content">
					전시물을 클릭하면 해당 전시품에 대한 설명을 보실 수 있습니다.
					</div>
				</div>
				<div class="col-sm-12 recommend-course-detail-wrapper">
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>	
								<div class="title">
									기초과학관
								</div>
								<div class="detail">
									카오스의 수차 - 테슬라코일 - 과학자가 바꾼 세상 - 광속측정 - 시간지연 - 
									파스칼의 삼각형 - 플라즈마의 세계 - 안개상자 - 수파발생장치 - 에어테이블 - 
									로켓추진체 - 타코마브릿지 붕괴사건 - 생태계와 광합성 - 지진체험관
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>	
								<div class="title">
									첨단기술관
								</div>
								<div class="detail">
									로봇공연장 - - 무균돼지 - 뇌파는마술사 - 미래디지털방송 - 컴퓨터와의대화(HCI) - 
									무선전력송출 - 신재생에너지 - 핵융합 - 항공기비행원리 - 행글라이더 체험 - 
									로켓엔진 - 발사통제센터 - 자기부상열차
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										자연사관
									</a>
								</div>
								<div class="detail">
									밤하늘의 수수께끼 - 태양계와 지구의 탄생 - 수수께끼의 열쇠 - 운석 - 
									스트로마톨라이트 - 쳉지앙 화석군 - 한반도의 고생대 - 양서류의 진화 - 
									생물의 육상진출 - - 백악기 아시아의 공룡낙원 - 화려한 지배자 공룡 - 공룡의 이모저모 - 
									사라져간 대형 포유류 - 신생대의 환경과 생물 - 한반도의 신생대 동식물 화석 - 인류의 진화 - 
									제주도의 인류흔적 - 생동하는 지구, SOS(예약필요) - 한반도의 육상생태계 - 한반도의 습지생태계 - 
									한반도의 특이식물 - 상류·중류·하류에 사는 물고기 - 한반도의 해양생물 - 영화속 주인공들 - 
									화석물고기 - 닥터피시 - 독도의 생태계
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										전통과학관
									</a>
								</div>
								<div class="detail">
									앙부일구 - 봉수대 - 맥파분석체험 - 한옥 - 3D거북선 - 동서양노젓기체험 - 
									학익진전법 체험 - 성덕대왕신종 - 신기전 - 비거
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										곤충생태관
									</a>
								</div>
								<div class="detail">
									육상·수서곤충 - 곤충의 한 살이 - 곤충의 세계 - 곤충소리 듣기 - 
									곤충체험장(장수풍뎅이, 수서곤충체험) - 나비정원 - 거미
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										생태공원
									</a>
								</div>
								<div class="detail">
									생태연못 - 푸른향기원 - 단풍원 - 이야기원 - 자원식물원 - 
									자생야생화원
								</div>								
							</div>
						</div>
					</div>
					<div class="row recommend-course-detail">
						<div class="col-sm-12">
							<div class="content">
								<div class="recommend-course-dot">
								</div>
								<div class="title">
									<a href="#">
										야외전시장
									</a>
								</div>
								<div class="detail">
									역사의 광장 - 교통수송 - 지질동산
								</div>								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$( function() {
		// hide all recommend courses.
	    $('.recommend-show-wrapper').hide();   
	    // find current active DIVs or time & age.
		var rec_time_id = $('.recommend-time.active').data('id');
		var rec_age_id = $('.recommend-age.active').data('id');
		// build an id selector for recommend course DIV
		var rec_course_id = "#rec-show-" + rec_time_id + "-" + rec_age_id;
		// show the course DIV
		$(rec_course_id).show();
		
		// click event for recommend time selections
		clickRecommendTime();
		// click event for recommend age selections
		clickRecommendAge();
		
	});
	
	function clickRecommendTime()  {
		$('.recommend-time').on("click", function() {
			
			// get current "active" recommend course and hide it.
			var hide_time_id = $('.recommend-time.active').data('id');
			var age_id = $('.recommend-age.active').data('id');
			// remove 'active' class for current recommend-time DIV.
			$('.recommend-time.active').removeClass('active');
			// build an id selector for current active course DIV
			var hide_course_id = "#rec-show-" + hide_time_id + "-" + age_id;
			// hide the course
			$(hide_course_id).hide();
			
			// add 'active' for current clicked div
			var show_time_id = $(this).data('id');
			$(this).addClass('active');
			// build an id selector for changed recommend course
			var show_course_id = "#rec-show-" + show_time_id + "-" + age_id;
			$(show_course_id).show();
		});
	}
  
	function clickRecommendAge() {
		$('.recommend-age').on("click", function() {
			
			// get current "active" recommend course and hide it.
			var time_id = $('.recommend-time.active').data('id');
			var hide_age_id = $('.recommend-age.active').data('id');
			// remove 'active' class for current recommend-age DIV.
			$('.recommend-age.active').removeClass('active');
			// build an id selector for current active course DIV
			var hide_course_id = "#rec-show-" + time_id + "-" + hide_age_id;
			// hide the course
			$(hide_course_id).hide();
			
			// add 'active' for current clicked div
			var show_age_id = $(this).data('id');
			$(this).addClass('active');
			// build an id selector for changed recommend course
			var show_course_id = "#rec-show-" + time_id + "-" + show_age_id;
			$(show_course_id).show();
		});
  	}
</script>