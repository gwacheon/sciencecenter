<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=460165677508131";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<div id="story-more-modal" class="modal fade story-modal">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" style="color: white; opacity: 1"><span aria-hidden="true">&times;</span></button>
				<h4 id="story-more-title" class="modal-title" id="myModalLabel">${shareUrl }</h4>
			</div>
			<div class="modal-body">
				<div class="option-button-wrapper text-right">
					<a href="#" class="modal-option-btn print-btn"><i class="fa fa-print"></i> 출력하기</a><a href="https://www.facebook.com/sharer/sharer.php?u=${param.shareUrl }"
						class="modal-option-btn share-btn"> <i class="fa fa-facebook-official"></i> 공유하기</a>
				</div>
				<div class="modal-body-inner-wrapper">
					<div id="stories-wrapper">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<c:import url="/WEB-INF/views/display/storyPrintPage.jsp">
	<c:param name="exhibitionName" value="${param.exhibitionName }" />
</c:import>

<script id="story-modal-template" type="text/x-handlebars-template">
{{#each stories}}
<div class="story-swipe-item" data-seq="{{@index}}">
	<div class="story-swipe-menu">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="menu-title">
						<a href="#" id="swipe-left-btn" class="menu-swipe-btn" data-direction="left">
							<i class="material-icons">&#xE314;</i>
						</a>

						<span>{{title}}</span>

						<a href="#" id="swipe-right-btn" class="menu-swipe-btn" data-direction="right">
							<i class="material-icons">&#xE315;</i>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="story-body-wrapper">
		<div class="story-header-desc">
			{{{headerDesc}}}
		</div>
		{{#each stories}}
			<div class="story">
				<div class="story-image">
					<img src="{{imgUrl}}" alt=" " class="img-responsive"/>
					<div class="story-label">
						{{storyLabel}}
					</div>							
				</div>
				<div class="story-desc">
					{{breaklines storyDesc}}
				</div>
			</div>
		{{/each}}
	</div>
</div>
{{/each}}
</script>

<script type="text/javascript">
	$(document).on("click", ".print-btn", function(e){
		window.print();
	});
	
	
	Handlebars.registerHelper('breaklines', function(text) {
	    text = Handlebars.Utils.escapeExpression(text);
	    text = text.toString();
	    text = text.replace(/(\r\n|\n|\r)/gm, '<span></span>');
	    return new Handlebars.SafeString(text);
	});	
	

	var selecedStoryIdx=0;
	var storyCount = stories.length;
	var storyModalSource   = $("#story-modal-template").html();
	var storyModalTemplate = Handlebars.compile(storyModalSource);
	var modalHtml = storyModalTemplate({stories: stories});
	$('#stories-wrapper').html(modalHtml);
	
	var storyModalWidth = $("#story-more-modal .modal-dialog").width();
	$("#stories-wrapper").css('width', storyModalWidth * storyCount);
	$(".story-swipe-item").css("width", storyModalWidth);
	

	$(document).on("click", ".story-more-btn", function(e) {
		e.preventDefault();
		selecedStoryIdx = $(this).data("idx");
		moveStoryModal();
		$("#story-more-modal").modal();
	});
	
	$('#story-more-modal').on('shown.bs.modal', function () {
		$("#stories-wrapper").css("height", $(".story-swipe-item[data-seq='" + selecedStoryIdx + "']").css('height'));
	});
	
	function moveStoryModal(){
		$("#story-more-title").text(stories[selecedStoryIdx].title);
		$("#stories-wrapper").animate({
			marginLeft: (-1 * storyModalWidth * selecedStoryIdx) + "px"
		});
		
		$("#stories-wrapper").css("height", $(".story-swipe-item[data-seq='" + selecedStoryIdx + "']").css('height'));
	}

	$(document).on("click", ".menu-swipe-btn", function(e) {
		e.preventDefault();
		var direction = $(this).data("direction");
		var idx = 0;

		if (direction == "left") {
			if (selecedStoryIdx == 0) {
				selecedStoryIdx = storyCount - 1;
			} else {
				selecedStoryIdx = selecedStoryIdx - 1;
			}
		} else {
			if (selecedStoryIdx == storyCount - 1) {
				selecedStoryIdx = 0;
			} else {
				selecedStoryIdx += 1;
			}
		}
		
		moveStoryModal();
	});
	
	
	var mywindow = $("#story-more-modal .modal-body");
	var mypos = mywindow.scrollTop();
	var up = false;
	var newscroll;
	mywindow.scroll(function () {
	    newscroll = mywindow.scrollTop();
	    if (newscroll > mypos && !up) {
	        $('.option-button-wrapper').stop().fadeOut();
	        up = !up;
	        
	    } else if(newscroll < mypos && up) {
	        $('.option-button-wrapper').stop().fadeIn();
	        up = !up;
	    }
	    else if( mypos == 0 ) {
	    	$('.option-button-wrapper').stop().fadeIn();
	    }
	    mypos = newscroll;
	});

</script>