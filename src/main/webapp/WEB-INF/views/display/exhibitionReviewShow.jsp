<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id = "communication-wrapper">
	<div class="sub-content-nav scrollspy-teal">
		<nav id="scrollspy-nav" class="navbar-default navbar-static has-scroll">
			<div class="container">
				<ul class="nav navbar-nav">
					<c:import url="/WEB-INF/views/display/navbar.jsp"/>
					<li>
						<a href="#board-spy" class="item">
							전시물 리뷰
						</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<div class="bread-crumbs">
							전시관람 / 전시물 리뷰
						</div>					
					</li>
				</ul>
			</div>
		</nav>
	</div>
	<div class="sub-body">
		<div  class="container">
			<div id = "board-spy" class="row">
				<div class="col-md-12">
					<h3 class="page-title teal">
						<div class="top-line"></div>
						 과학관 직원이 소개하는 전시물
					</h3>
				</div>
			</div> 
			<div id="board-show" class="board-show-teal">
			
			</div>
		</div> 
	</div>
</div> 

<c:import url="/WEB-INF/views/boards/show.jsp"></c:import>

<script type="text/javascript">
	var board = new Board(
	        "#board-show",
	        "<c:url value='/display/exhibitionReview'/>",
	        {
	            id: <c:out value="${id}"/>,
	        }
	    );
	board.renderBoardShow();
</script>