<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-teal">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>
	<div id="display-body" class="sub-body">
		<div id="basic-info-spy" class="narrow-sub-top display">
			<div class="sub-banner-tab-wrapper sub-banner-tab-teal">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<ul class="nav nav-tabs">
								<li class="col-xs-12 col-sm-6 col-md-3">
									<a href="<c:url value="/display/frontier/kidsMakerStudio" />" class="item">
										키즈메이커스튜디오
									</a>
								</li>
								<li class="col-xs-12 col-sm-6 col-md-3">
									<a href="<c:url value="/display/frontier/freeMarket" />" class="item">
										메이커프리마켓 
									</a>
								</li>
								<li class="col-xs-12 col-sm-6 col-md-3">
									<a href="<c:url value="/display/frontier/familySLand" />" class="item">
										패밀리창작놀이터
									</a>
								</li>
								<li class="col-xs-12 col-sm-6 col-md-3 active">
									<a href="#" class="item">
										무한상상실
									</a>
								</li>
							</ul>   
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="support-sub-nav">  
			<div class="container">
			   	<div class="sub-nav">
					<div class="row">	
						<div class="col-xs-6 col-md-2-5">
				     		<a href="<c:url value="/display/frontier/infiniteImagination" />" class="item">
								<i class="fa fa-book" aria-hidden="true"></i> 무한상상실 안내
							</a> 
				     		<div class="visible-xs" style="margin-bottom: 15px"></div> 
						</div>
						<div class="col-xs-6 col-md-2-5">
				     		<a href="<c:url value="/sangsang/list/device" />" class="item">
								<i class="fa fa-print" aria-hidden="true"></i> 장비 예약
							</a> 
				     		<div class="visible-xs" style="margin-bottom: 15px"></div> 
						</div>
						<div class="col-xs-6 col-md-2-5">
						    <a href="#" class="item active">
								<i class="fa fa-cube" aria-hidden="true"></i> 메이커인큐베이터 
							</a>
						    <div class="visible-xs" style="margin-bottom: 15px"></div> 
						</div>
						<div class="col-xs-6 col-md-2-5">
						    <a href="javascript:void(0)" class="item" onclick="AcademyChoice('ACD008')">
								<i class="fa fa-calendar-check-o" aria-hidden="true"></i> 워크숍 예약
							</a>
							<div class="visible-xs" style="margin-bottom: 15px"></div> 
						</div>
						<div class="col-xs-6 col-md-2-5">
						    <a href="http://cyber.scientorium.go.kr/museum_2015/vrLoad.jsp?pavilion=indoor&xml_name=img_001.xml&angle=-90"
								class="item">
								<i class="fa fa-desktop" aria-hidden="true"></i> 사이버 전시관
							</a>
						</div>				     
					</div>  
				</div>
			</div>
		</div> 
		<div class="container">
			<div id="incubator-spy" class="scrollspy section margin-bottom20">
				<div class="row">
					<div class="col-xs-6 col-md-3">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							메이커 인큐베이터
						</h3>
					</div>
				</div>
				<div class="row description">
					<div class="col-sm-12 col-md-4 col-lg-4">
						<ul class="circle-list teal text-bold item-margin-bottom">
							<li>
								위치
								<ul>
									<li>
										과학관 입장후 프론티어 창작관 내 메이커 랜드
									</li>
								</ul>
							</li>
							<li>
								권장대상
								<ul>
									<li>
										중학생 이상 2인 이하의 창작팀
									</li>
								</ul>
							</li>
							<li>
								입주기간
								<ul>
									<li>
										공식 입주일로부터 3개월
									</li>
									<li>
										1차 입주기간: ‘16.6.1 ~ 9.31
									</li>
									<li>
										2차 입주기간: ‘16.10.1 ~ 12.31
									</li>
								</ul>
							</li>
							<li>
								신청기간
								<ul>
									<li>
										공식 입주일로부터 6주 전 ~ 입주일 15일 전까지
									</li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="col-sm-12 col-md-8 col-lg-8 text-right">
						<!-- select dropdown : 다른관으로 이동 -->
						<c:import url="/WEB-INF/views/display/displayHallSelect.jsp"/>
						<div>
							<img alt="기초과학관" src="<c:url value='/resources/img/display/map/15.png'/>"
								class="img-responsive margin-top20">
						</div>
					</div>
				</div>
			</div>
			<div id="apply-guide-spy">
				<div class="row">
					<div class="col-xs-6 col-md-3">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							지원 안내
						</h3>
					</div>
				</div>
				<div class="row description">
					<div class="col-md-12">
						<ul class="circle-list teal text-bold item-margin-bottom">
							<li>
								지원내용
								<ul>
									<li>
										 창작공간 및 장비 지원
										 <div class="table-responsive">
											<table class="table table-bordered centered-table">
												<thead>
													<tr>
														<th>
															제공 공간
														</th>
														<th>
															세부내용
														</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>
															개별 작업 공간
														</td>
														<td>
															<ul class="circle-list teal text-left">
																<li>
																	최대 2인 이용가능한 개방형 공간 10개
																</li>
																<li>
																	최대 2인 이용가능한 개방형 공간 10개
																</li>
															</ul>
														</td>
													</tr>
													<tr>
														<td>
															공용 공간
														</td>
														<td>
															<ul class="circle-list teal text-left">
																<li>
																	디지털 제조 장비로 시제품 제작이 가능한 창작 공간
																</li>
																<li>
																	목공(레이저커팅, CNC), 아크릴가공, 전자회로, 3D 프린팅 등
																</li>
															</ul>
														</td>
													</tr>
													<tr>
														<td>
															회의실
														</td>
														<td>
															<ul class="circle-list teal text-left">
																<li>
																	공용 회의실 2개, 오픈미팅룸 1개
																</li>
															</ul>
														</td>
													</tr>
													<tr>
														<td>
															기타
														</td>
														<td>
															<ul class="circle-list teal text-left">
																<li>
																	무선 인터넷, 전기, 음수대 
																</li>
															</ul>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</li>
								</ul>
							</li>
							<li>
								지원방법
								<ul>
									<li>
										 이메일 접수(신청서 작성 후 nspark@korea.kr) 및 서면평가
										 <div class="process-box-wrapper teal">
											 <div class="row">
												<div class="col-sm-12 col-md-6 col-lg-3">								
													<div class="process">
														<div class="text">
															<span class="head">공고 및 접수</span><br>
															모집 공고 및 이메일 접수
														</div>
														<i class="fa fa-angle-right fa-lg" aria-hidden="true"></i>
													</div>
												</div>
												<div class="col-sm-12 col-md-6 col-lg-3">								
													<div class="process ex-color single-line">
														<div class="text">
															<span class="head">서면 평가</span><br>
															내부 위원회 평가
														</div>
														<i class="fa fa-angle-right fa-lg" aria-hidden="true"></i>
													</div>
												</div>
												<div class="col-sm-12 col-md-6 col-lg-3">								
													<div class="process">
														<div class="text">
															<span class="head">결과 통보</span><br>
															평가 결과 및 공식 입주일 통보
														</div>
														<i class="fa fa-angle-right fa-lg" aria-hidden="true"></i>
													</div>
												</div>
												<div class="col-sm-12 col-md-6 col-lg-3">								
													<div class="process ex-color single-line">
														<div class="text">
															<span class="head">입주 완료</span><br>
															창작공간 입주
														</div>
													</div>
												</div>
											</div>
										</div>
									</li>
								</ul>
							</li>
							
							<li>
								평가기준
								<ul>
									<li>
										<div class="table-responsive">
											<table class="table table-bordered centered-table">
												<thead>
													<tr>
														<th>
															제공 공간
														</th>
														<th>
															세부내용
														</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>
															개별 작업 공간
														</td>
														<td>
															<ul class="circle-list teal text-left">
																<li>
																	최대 2인 이용가능한 개방형 공간 10개
																</li>
																<li>
																	최대 2인 이용가능한 개방형 공간 10개
																</li>
															</ul>
														</td>
													</tr>
													<tr>
														<td>
															공용 공간
														</td>
														<td>
															<ul class="circle-list teal text-left">
																<li>
																	디지털 제조 장비로 시제품 제작이 가능한 창작 공간
																</li>
																<li>
																	목공(레이저커팅, CNC), 아크릴가공, 전자회로, 3D 프린팅 등
																</li>
															</ul>
														</td>
													</tr>
													<tr>
														<td>
															회의실
														</td>
														<td>
															<ul class="circle-list teal text-left">
																<li>
																	공용 회의실 2개, 오픈미팅룸 1개
																</li>
															</ul>
														</td>
													</tr>
													<tr>
														<td>
															기타
														</td>
														<td>
															<ul class="circle-list teal text-left">
																<li>
																	무선 인터넷, 전기, 음수대 
																</li>
															</ul>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</li>
								</ul>
							</li>
							<li>
								문의
								<ul>
									<li>
										전화 : 02)509-6925(월요일 휴관)
										
									</li>
									<li>
										e-mail : moohsangsang@gmail.com
									</li>
								</ul>
							</li>
						</ul>
						
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>
<form name="frmIndex">
	<input type="hidden" name="ACADEMY_CD" value="ACD008" />
	<input type="hidden" name="COURSE_CD" value="" />
	<input type="hidden" name="SEMESTER_CD" value="" />
	<input type="hidden" name="TYPE" value="" />
	<input type="hidden" name="FLAG" value="" />
	<input type="hidden" name="CLASS_CD" value="CL8001" />
</form>
<script type="text/javascript">
	function AcademyChoice(Cd){
		var frm = document.frmIndex;
		frm.CLASS_CD.value = Cd;
		frm.action = '<c:url value="/schedules"/>?${_csrf.parameterName}=${_csrf.token}';
		frm.method = 'post';
		frm.submit();
	}
</script>