<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-teal">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>
	<div id="display-body" class="sub-body">
		<div class="narrow-sub-top display">
			<div class="sub-banner-tab-wrapper sub-banner-tab-teal">
				<!-- <div class="container">
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
							<ul class="nav nav-tabs">
								<li class="col-xs-12 active">
									<a href="http://cyber.scientorium.go.kr/museum_2015/vrLoad.jsp?pavilion=indoor&xml_name=img_001.xml&angle=150" class="item">
										<i class="fa fa-desktop fa-lg" aria-hidden="true"></i> 사이버 전시관
									</a>		
								</li>
							</ul>   
						</div>
					</div>
				</div> -->
			</div>			
		</div>
		<div class="container">
			<div id="basic-map-spy" class="section scrollspy">
				<div class="row">
					<div class="col-sm-4 col-md-3">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							노벨상과 나 안내
						</h3>
					</div>
				</div>
				<div class="row description">
					<div class="col-sm-12 col-md-4 col-lg-4">
						<ul class="circle-list teal">
							<li>
								주요전시물
								
								<ul>
									<li>
										<b>「노벨상과 나」</b>는 우리 생활 속에서 만나 볼 수 있는 노벨상 수상자들의 
										연구 성과를 친근하게 체험해 보는 공간이다. 우리 집 거실에 있는 TV와 전화, 
										주방에 있는 프라이팬과 플라스틱 용품, 주차장에 있는 자동차, 방에 있는 컴퓨터와 디지털카메라 
										모두 노벨상 수상자들의 연구 성과물이라 할 수 있다. 이곳에는 집 형태의 5개 코너에 31건의 
										전시물이 놓여 있다. 특히, ‘노벨자동차경주’, ‘DNA 롤링볼’등 4건의 체험물을 이용하며 미래의 노벨상 수상을 꿈꿔보자.
									</li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="col-sm-12 col-md-8 col-lg-8 text-right">
						<!-- select dropdown : 다른관으로 이동 -->
						<c:import url="/WEB-INF/views/display/displayHallSelect.jsp"/>
						<div>
							<img alt="노벨상과나" src="<c:url value='/resources/img/display/map/14.png'/>"
								class="img-responsive margin-top20">
							<%-- <c:import url="/WEB-INF/views/display/mainBuilding/svgs/display_svg_map_modified.jsp"></c:import> --%>
						</div>
					</div>
				</div>
			</div>
			<div id="major-exhibitions-spy" class="section scrollspy">
				<div class="row">
					<div class="col-sm-3 col-md-2 col-lg-2">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							주요전시물					
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="main-exhibition-wrapper">
							<div id="main-exhibitions">
							</div>
						</div>
					</div>
				</div>
				<div id="story-spy" class="story-telling-wrapper">
					<div class="header">
						<div class="row">
							<div class="col-md-6">
								<h3 class="page-title teal">
									<div class="top-line"></div>
									스토리로 읽어 보는 명예의전당&amp;노벨상과나 여행기 
								</h3>
							</div>
						</div>
					</div>
					<div class="description">
					</div>
					
					<div id="stories" class="row">
					</div>
				</div>
				
				<div class="related-photos">
					<div class="title">
						<i class="color-darkgreen material-icons">&#xE413;</i>   관련사진
					</div>
					<div id="image-slider">
						<div id="image-slider-window">
							<img src="<c:url value="/resources/img/display/frontier/nobelPrize/related_photo_01.png"/>" 
								data-url="/resources/img/display/frontier/nobelPrize/related_photo_01.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/frontier/nobelPrize/related_photo_02.png"/>" 
								data-url="/resources/img/display/frontier/nobelPrize/related_photo_02.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/frontier/nobelPrize/related_photo_03.png"/>" 
								data-url="/resources/img/display/frontier/nobelPrize/related_photo_03.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/frontier/nobelPrize/related_photo_04.png"/>" 
								data-url="/resources/img/display/frontier/nobelPrize/related_photo_04.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/frontier/nobelPrize/related_photo_05.png"/>" 
								data-url="/resources/img/display/frontier/nobelPrize/related_photo_05.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/frontier/nobelPrize/related_photo_06.png"/>" 
								data-url="/resources/img/display/frontier/nobelPrize/related_photo_06.png" class="slide"/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script id="story-template" type="text/x-handlebars-template">
{{#each stories}}
<div class="col-xs-12 col-sm-6 col-md-6">
	<div class="display-card story-telling story-more-btn pointer" data-idx="{{@index}}" onclick="">
		<div class="card-image">
			<img src="{{thumbnailUrl}}" class="img-responsive"/>
		</div>
		<div class="card-content">
			<div class="display-card-title">
				{{title}}
			</div>
			<div class="display-card-content">
				{{{desc}}}
			</div>
			
		</div>
	</div>							
</div>
{{/each}}
</script>

<script type="text/javascript">
	var stories = [
		{
			title: "한국을 빛낸 과학자들과 함께하는 시간 [명예의 전당]",
			desc: "15세기 전반 우리 과학기술을 세계 최고 수준으로 끌어 올린 세종대왕! 한글, 측우기, "
				+ "해시계 등을 창조하며 타고난 발명가이자 창의력 대왕이었던 세종대왕은 물론 장영실, 허준, "
				+ "이휘소 등 여러 과학자들의 이야기가 이 곳에 담겨 있습니다.",
			thumbnailUrl: "<c:url value="/resources/img/display/frontier/hallOfFame/story05_01.jpg"/>",
			stories: [
				{
					storyLabel: "명예의 전당",
					storyDesc: "15세기 전반 우리 과학기술을 세계 최고 수준으로 끌어 올린 세종대왕! 한글, 측우기, "
						+ "해시계 등을 창조하며 타고난 발명가이자 창의력 대왕이었던 세종대왕은 물론 장영실, 허준, "
						+ "이휘소 등 여러 과학자들의 이야기가 이 곳에 담겨 있습니다.",
					imgUrl: "<c:url value="/resources/img/display/frontier/hallOfFame/story05_01.jpg"/>"
				},
				{
					storyLabel: "명예의 전당",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/frontier/hallOfFame/story05_02.jpg"/>"
				},
				{
					storyLabel: "명예의 전당",
					storyDesc: "저는 생물학자 석주명 선생의 소개가 가장 흥미로웠는데요. 일제 식민지시기, 모든 것이 "
							+ "자유롭지 못했던 시기에 가장 활발한 연구를 선보이며 우리나라 나비 연구의 기틀을 마련했던 " 
							+ "석주명 선생의 연구 내용을 살펴보며 다시 한번 우리 과학자들의 저력을 느낄 수 있었습니다. \n\n"
							+ "여러분도 명예의 전당을 둘러보시며, 나만의 존경하는 한국 과학자를 꼽아 보시는 건 어떨까요?",
					imgUrl: "<c:url value="/resources/img/display/frontier/hallOfFame/story05_03.jpg"/>"
				},
				
			]
		},
		{
			title: "대한민국의 노벨상 과학 부분 수상을 꿈꾸며 [노벨상과 나]",
			desc: "노벨상 만찬장에서는 어떻게 만찬이 벌어지는 지 궁금하셨나요? 이 곳에서 확인해보세요. "
				+ "그 들이 사용하는 접시부터 음식까지, 노벨상 만찬장의 모든 것을 눈으로 확인할 수 있답니다.\n\n"
				+ "평소 그냥 우리가 무심코 사용했던 스마트폰, 자동차 등에 숨어있는 노벨상 수상자들의 공로도 영상과 글을 통해 세세하게 만나볼 수 있습니다.",
			thumbnailUrl: "<c:url value="/resources/img/display/frontier/nobelPrize/story06_01.jpg"/>",
			stories: [
				{
					storyLabel: "노벨상과 나",
					storyDesc: "노벨상 만찬장에서는 어떻게 만찬이 벌어지는 지 궁금하셨나요? 이 곳에서 확인해보세요. "
						+ "그 들이 사용하는 접시부터 음식까지, 노벨상 만찬장의 모든 것을 눈으로 확인할 수 있답니다.\n\n"
						+ "평소 그냥 우리가 무심코 사용했던 스마트폰, 자동차 등에 숨어있는 노벨상 수상자들의 공로도 영상과 글을 통해 세세하게 만나볼 수 있습니다.",
					imgUrl: "<c:url value="/resources/img/display/frontier/nobelPrize/story06_01.jpg"/>"
				},
				{
					storyLabel: "노벨상과 나",
					storyDesc: "인간의 능력을 확장한 노벨 물리학상, 자연을 대처하는 노벨 화학상, 삶과 생명을 향한 "
						+ "시선을 바꾸는 노벨 생리의학상까지! \n\n"
						+ "아직 대한민국에서는 수상자가 없는 과학분야!\n"
						+ "그 최초의 주인공은 누가 될까요? \n\n" 
						+ "그 주인공은, 오늘 국립과천과학관을 찾아 새로운 상상력과 아이디어, 호기심으로 가득 찬 여러분일 것입니다.",
					imgUrl: "<c:url value="/resources/img/display/frontier/nobelPrize/story06_02.jpg"/>"
				}
			]
		}

	];
	
	var storySource   = $("#story-template").html();
	var storyTemplate = Handlebars.compile(storySource);
	var storyHtml = storyTemplate({stories: stories});
	$("#stories").html(storyHtml);
</script>

<c:url var="shareUrl" value="/display/frontier/nobelPrizeAndI" />
<c:import url="/WEB-INF/views/display/displayDetailModal.jsp">
	<c:param name="shareUrl" value="${shareUrl }"/>
	<c:param name="exhibitionName" value="노벨상과 나" />
</c:import>


<script type="text/javascript">

	$(document).ready( function() {
		// 주요 전시물 클릭시 show hide event
		showHideDetails();
		
		// image slider
		PTechSlider('#image-slider').autoSlide();
		
		// SVG MAP SETTING
		// 전시관 Map 1층으로.
		//setDefaultSVGMap('#basic-map-spy',1);
		// 지도 flash 해당 전시관 표시
		//$('#XMLID_11_').attr('class', 'active');

	});
	
	function showHideDetails()  {
		// hide all details first.
		$('.exhibition-detail-wrapper').hide();
		// show the active detail only.
		var exhibit_active_id = $('.major-exhibition-wrapper.active').data('id');
		$(exhibit_active_id).show();
		
		// click event for major exhibition box
		$('.major-exhibition-wrapper').on('click', function() {
			
			// get current active div & remove active
			var hide_id = $('.major-exhibition-wrapper.active').data('id');
			$('.major-exhibition-wrapper.active').removeClass('active');
			// hide the matching detail
			$(hide_id).hide();
			
			// add 'active' to the current clicked div
			var show_id = $(this).data('id');
			$(this).addClass('active');
			// show the mathcing detail
			$(show_id).show();
			
		});
	}
	
</script>


<!-- Major Exhibition Contents Loading -->
<c:import url="/WEB-INF/views/display/majorExhibitionTemplate.jsp" />
<script type="text/javascript">
	var exhibitions = [
		{
			descriptions : [
				
				{
					title: "노벨상 히어로, 인류의 영웅을 만나다",
					desc : "‘노벨상 히어로’코너에서는 퀴리부인을 비롯해 DNA구조를 발견한 제임스 왓슨, "
						+ "페니실린을 발견한 플레밍과 같은 노벨상 수상자들을 만나볼 수 있다. "
						+ "이들로 인해 우리 인류가 원더우먼, 스파이더맨, 배트맨과 같은 능력을 가지게 되었다는 것을 알 수 있다. "
						+ "퀴즈를 풀고 노벨상 히어로 카드도 받아 가자."
				},
				{
					title: "노벨생리의학상, 가족과 서재에서 만나다",
					desc : "‘노벨생리의학상, 생명을 향한 시선을 바꾸다’코너에서는 백신 발견, 인공비타민 개발, "
						+ "시험관 아기 시술 연구로 인해 우리 가족이 건강을 지키며 행복하게 살고 있다는 것을 "
						+ "‘노벨하우스 가족 이야기’ 스토리텔링으로 보여준다." 
						+ "‘DNA 롤링볼’은 형광 물고기, 파란 장미 등 유전자 합성의 원리를 알려주는 체험전시물이다."
				}
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/frontier/nobelPrize/major_exhibition_01.png' />",
					caption: "[노벨상 히어로]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/frontier/nobelPrize/major_exhibition_04.png' />",
					caption: "[DNA 롤링볼]"
				},
			]
		},
		{
			descriptions : [
				{
					title : "노벨물리학상, 우주와 거실에서 만나다",
					desc : "‘노벨물리학상, 우주의 근원을 밝히다’는 현관으로 들어오면 창밖으로 밤하늘의 별들과 "
						+ "멋진 도시야경이 보이는 코너이다. 밤하늘을 올려다보면 138억 년 전 우주가 어떻게 탄생했는지를 "
						+ "밝혀낸 노벨물리학상 수상 과학자들의 이야기를 볼 수 있다. 창밖의 도시 야경 밑에 설치된 거대입자가속기 "
						+ "모형에서는 가장 작은 입자를 찾으려는 과학자들의 노력을 엿볼 수도 있다."
				},
				{
					title : "",
					desc : "‘노벨물리학상, 인간의 능력을 확장하다’코너는 현관을 지나 거실로 들어서면 보인다. "
						+ "노벨물리학상 수상자 20명의 연구 성과가 들어 있는 스마트폰을 중심으로 거실에 있는 TV, "
						+ "디지털카메라, 전화, 내비게이션, 컴퓨터가 어떻게 발명되었는지를 확인할 수 있다."
				},
				{
					title : "노벨화학상, 부엌과 자동차에서 만나다",
					desc : "‘노벨화학상, 자연을 대체하다’코너에는 부엌 곳곳에 있는 세제, 프라이팬, "
						+ "플라스틱 제품, 인공염료 및 향신료, 발효식품 등 노벨상의 흔적들로 가득하다."
				},
				{
					title : "",
					desc : "‘자동차 속 노벨화학상’에서는 VR로 자동차 게임을 하면서 노벨화학상 연구들이 "
						+ "자동차 발전에 얼마나 많은 기여를 했는지를 체험해 볼 수 있다."
						+ "<div class='additional-info'>(4명/회, 초등 3학년 이상)</div>"
				},
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/frontier/nobelPrize/major_exhibition_02.png' />",
					caption: "[노벨의 부엌]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/frontier/nobelPrize/major_exhibition_03.png' />",
					caption: "[VR 자동차 게임]"
				}
			]
		}
	];
	
	var exhibitionSource   = $("#major-exhibition-template").html();
	var exhibitionTemplate = Handlebars.compile(exhibitionSource);
	var exhibitionHtml = exhibitionTemplate({exhibitions: exhibitions});
	$("#main-exhibitions").html(exhibitionHtml);
	
</script>