<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-teal">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
		<%-- <nav id="scrollspy-nav" class="navbar-default navbar-static has-scroll">
		<div class="container">
			<ul class="nav navbar-nav">
				<c:import url="/WEB-INF/views/display/navbar.jsp"/>
				<li>
					<a href="#basic-info-spy" class="item"> 기본안내 </a> 
				</li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li>
					<div class="bread-crumbs">전시관 / 메이커랜드</div>
				</li>
			</ul>
		</div>
		</nav> --%>
	</div>
	<div id="display-body" class="sub-body">
		<div class="narrow-sub-top display">
			<div class="sub-banner-tab-wrapper sub-banner-tab-teal">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<ul class="nav nav-tabs">
								<li class="col-xs-12 col-sm-6 col-md-3">
									<a href="<c:url value="/display/frontier/kidsMakerStudio" />" class="item">
										키즈메이커스튜디오
									</a>
								</li>
								<li class="col-xs-12 col-sm-6 col-md-3">
									<a href="<c:url value="/display/frontier/freeMarket" />" class="item">
										메이커프리마켓 
									</a>
								</li>
								<!-- <li class="col-xs-12 col-sm-6 col-md-3 active">
									<a href="#" class="item">
										패밀리창작놀이터
									</a>
								</li> -->
								<li class="col-xs-12 col-sm-6 col-md-3">
									<a href="<c:url value="/display/frontier/infiniteImagination" />" class="item">
										무한상상실
									</a>
								</li>
							</ul>   
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div id="kids-studio-spy" class="scrollspy section margin-bottom20">
				<div class="row">
					<div class="col-xs-6 col-md-3">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							패밀리 창작놀이터
						</h3>
					</div>
				</div>
				<div class="row description">
					<div class="col-sm-12 col-md-6">
						<ul class="circle-list teal">
							<li>
								기본 안내 
							</li>
						</ul>
						<div class="desc-text">
							<div class="row">
								<div class="col-xs-3 col-md-2">
									<div class="label-header">
										위치
									</div>
								</div>
								<div class="col-xs-9 col-md-10">
									<div class="label-value">
										과학관 옥외전시장 창작놀이터
									</div>
								</div>
								<div class="col-xs-3 col-md-2">
									<div class="label-header">
										권장대상
									</div>
								</div>
								<div class="col-xs-9 col-md-10">
									<div class="label-value">
										가족 단위(아동1 성인1 이상)
									</div>
								</div>
								<div class="col-xs-3 col-md-2">
									<div class="label-header">
										운영시간
									</div>
								</div>
								<div class="col-xs-9 col-md-10">
									<div class="label-value">
										화요일 ~ 일요일, 10시~12시, 1시~3시, 3~5시(1일 3회)
									</div>
								</div>
								<div class="col-xs-3 col-md-2">
									<div class="label-header">
										이용방법
									</div>
								</div>
								<div class="col-xs-9 col-md-10">
									<div class="label-value">
										과학관 홈페이지 예약 – 창작놀이터 예약
									</div>
								</div>
								<div class="col-xs-3 col-md-2">
									<div class="label-header">
										문의
									</div>
								</div>
								<div class="col-xs-9 col-md-10">
									<div class="label-value">
										전화 : 02)509-6925(월요일 휴관)<br>
										e-mail : moohsangsang@gmail.com
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-6 col-md-3">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							창작프로그램 소개
						</h3>
					</div>
				</div>
				<div class="row description">
					<div class="col-md-3 sland-icons">
						<div class="icon">
							<i class="fa fa-puzzle-piece" aria-hidden="true"></i>
						</div>
						<div>
							문제해결의 열쇠<br>
							<span>호기심과 작동체험</span>
						</div>
					</div>
					<div class="col-md-3 sland-icons odd">
						<div class="icon">
							<i class="fa fa-users" aria-hidden="true"></i>
						</div>
						<div>
							가족, 친구와 함께하는<br>
							<span>팀 창작놀이</span>
						</div>
					</div>
					<div class="col-md-3 sland-icons">
						<div class="icon">
							<i class="fa fa-flask" aria-hidden="true"></i>
						</div>
						<div>
							학문의 경계를 넘어 즐기는<br>
							<span>STEAM 교육</span>
						</div>
					</div>
					<div class="col-md-3 sland-icons odd">
						<div class="icon">
							<i class="fa fa-lightbulb-o" aria-hidden="true"></i>
						</div>
						<div>
							아이디어를 구현하는<br>
							<span>풀뿌리 메이커 육성</span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-6 col-md-3">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							프로그램 구성 안내
						</h3>
					</div>
					<div class="col-sm-12 col-md-12">
						<ul class="circle-list teal">
							<li>
								주중 Create
							</li>
						</ul>
					</div>
				</div>
				
				<div id="current-equipment-spy" class="row description sland">
					<div class="col-md-6">
						<div class="equipment-card sland">
							<div class="row">
								<div class="col-md-4">
									<div class="equipment-label">
										초코릿 3D 프린터
									</div>								
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4 col-md-4">
									<div class="equipment-img">
										<img src="/sciencecenter/resources/img/display/frontier/sland/sland_1.png" class="img-responsive">
									</div>
								</div>
								<div class="col-xs-8 col-md-8">
									<div class="equipment-desc">
										간단한 프로그래밍으로 모델링한 물체를 직접 제작<br>
										<b>제목</b> 3D 프린터로 모형 제작<br>
										<b>목적</b> 3D 프린터 작동원리를 이해하고 아이디어 구성방법 체득<br>
										<b>체험</b> <ul>
										<li>
											영상을 통해 교육(10분)
										</li>
										<li>
											초코릿 3D 프린터의 작동법을 익히고
										</li>
										<li>
											모델링한 파일을 출력하여 모형 제작 
										</li>
										</ul>
									</div>
								</div>
							</div>
						</div>					
					</div>
					<div class="col-md-6">
						<div class="equipment-card sland">
							<div class="row">
								<div class="col-md-4">
									<div class="equipment-label">
										사물인터넷(IoT)
									</div>								
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4 col-md-4">
									<div class="equipment-img">
										<img src="/sciencecenter/resources/img/display/frontier/sland/sland_2.png" class="img-responsive">
									</div>
								</div>
								<div class="col-xs-8 col-md-8">
									<div class="equipment-desc">
										아두이노와 센서를 이용해 사물인터넷 시스템을 구성<br>
										<b>제목</b> 아두이노와 센서를 활용한 사물인터넷 체험<br>
										<b>목적</b> 생활 속 사물인터넷 원리를 이해하고 활용방법 체득 <br>
										<b>체험</b> <ul>
										<li>
											영상을 통해 교육(10분)
										</li>
										<li>
											아두이노와 센서를 이용해 사물인터넷시스템을 만들고, 작동 체험

										</li>
										</ul>
									</div>
								</div>
							</div>
						</div>					
					</div>
					<div class="col-sm-12 col-md-12">
						<ul class="circle-list teal">
							<li>
								주말 Make
							</li>
						</ul>
					</div>
					<div class="col-md-6">
						<div class="equipment-card sland">
							<div class="row">
								<div class="col-md-4">
									<div class="equipment-label">
										LEGO WeDo
									</div>								
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4 col-md-4">
									<div class="equipment-img">
										<img src="/sciencecenter/resources/img/display/frontier/sland/sland_3.png" class="img-responsive">
									</div>
								</div>
								<div class="col-xs-8 col-md-8">
									<div class="equipment-desc">
										LEGO WeDo 키트를 활용한 설계, 제작, 프로그래밍, 미션 수행
										<b>제목</b> LEGO WeDo 로보틱스 미션 해결 <br>
										<b>목적</b> Think, Do, Test의  초급단계 엔지니어링 이해 <br>
										<b>체험</b> <ul>
										<li>
											영상을 통해 교육(10분)
										</li>
										<li>
											Think, Do, Test의 초급단계 엔지니어링 이해
										</li>
										<li>
											주어진 키트를 활용해서 설계, 제작 후, 아이패드로 프로그래밍, 미션 수행
											
										</li>
										</ul>
									</div>
								</div>
							</div>
						</div>					
					</div>
					<div class="col-md-6">
						<div class="equipment-card sland">
							<div class="row">
								<div class="col-md-4">
									<div class="equipment-label">
										VEX IQ
									</div>								
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4 col-md-4">
									<div class="equipment-img">
										<img src="/sciencecenter/resources/img/display/frontier/sland/sland_4.png" class="img-responsive">
									</div>
								</div>
								<div class="col-xs-8 col-md-8">
									<div class="equipment-desc">
										VEX 키트를 활용한 설계, 제작, 프로그래밍, 미션 수행
										<b>제목</b> VEX 로보틱스 미션 해결  <br>
										<b>목적</b> Think, Do, Test의  중급단계 엔지니어링 이해 <br>
										<b>체험</b> <ul>
										<li>
											영상을 통해 교육(10분)
										</li>
										<li>
											VEX 로보틱스 활용법을 익히고,
										</li>
										<li>
											주어진 키트를 활용해서 설계, 제작 후,
										</li>
										<li>
											아이패드로 프로그래밍, 미션 수행
										</li>
										</ul>
									</div>
								</div>
							</div>
						</div>					
					</div>
				</div>
				<div class="row">
					<div class="col-xs-6 col-md-3">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							창작놀이터 참가방법
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-md-4 col-lg-3">								
						<div class="process sland single-line">
							<div class="text">
								인터넷 예약
							</div>
							<i class="fa fa-angle-right fa-lg" aria-hidden="true"></i>
						</div>
					</div>
					<div class="col-sm-12 col-md-4 col-lg-3">								
						<div class="process sland ex-color single-line">
							<div class="text">
								영상교육<br>
								(10~20분)
							</div>
							<i class="fa fa-angle-right fa-lg" aria-hidden="true"></i>
						</div>
					</div>
					<div class="col-sm-12 col-md-4 col-lg-3">								
						<div class="process sland single-line">
							<div class="text">
								팀별 작업 및 창작놀이<br>
								(약 3시간)
							</div>
							<i class="fa fa-angle-right fa-lg" aria-hidden="true"></i>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>