<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-teal">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>
	<div id="display-body" class="sub-body">
		<div class="narrow-sub-top display">
			<div class="sub-banner-tab-wrapper sub-banner-tab-teal">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<ul class="nav nav-tabs">
								<li class="col-xs-12 col-sm-6 col-md-4 active">
									<a href="<c:url value="/display/frontier/kidsMakerStudio" />" class="item">
										키즈메이커스튜디오
									</a>
								</li>
								<li class="col-xs-12 col-sm-6 col-md-4">
									<a href="<c:url value="/display/frontier/freeMarket" />" class="item">
										메이커프리마켓 
									</a>
								</li>
								<%-- <li class="col-xs-12 col-sm-6 col-md-3">
									<a href="<c:url value="/display/frontier/familySLand" />" class="item">
										패밀리창작놀이터
									</a>
								</li> --%>
								<li class="col-xs-12 col-sm-6 col-md-4">
									<a href="<c:url value="/display/frontier/infiniteImagination" />" class="item">
										무한상상실
									</a>
								</li>
							</ul>   
						</div>
					</div>
				</div>
			</div>			
		</div>
		<div class="container">
			<div id="kids-studio-spy" class="scrollspy section margin-bottom20">
				<div class="row">
					<div class="col-xs-6 col-md-3">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							키즈메이커스튜디오
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						「키즈메이커 스튜디오(Kids Maker Studio)」는 어린이들이 직접 만들고 실험하면서 
						과학기술의 원리와 ICT를 경험하는 창작 체험 공간이다. 
						2015년 10월 구글(Google) 후원을 통해 키즈메이커 스튜디오가 새롭게 오픈하였다.
					</div>
				</div>				
				<div class="row description">
					<div class="col-sm-12 col-md-6">
						<ul class="circle-list teal">
							<li>
								기본 안내 
							</li>
						</ul>
						<div class="desc-text">
							<div class="row">
								<div class="col-xs-3 col-md-2">
									<div class="label-header">
										위치
									</div>
								</div>
								<div class="col-xs-9 col-md-10">
									<div class="label-value">
										과학관 정문 입장 후 왼편 무한상상실
									</div>
								</div>
								<div class="col-xs-3 col-md-2">
									<div class="label-header">
										시간
									</div>
								</div>
								<div class="col-xs-9 col-md-10">
									<div class="label-value">
										오전11시 / 오후2시 / 3시 / 4시(1일 4회)
									</div>
								</div>
								<div class="col-xs-3 col-md-2">
									<div class="label-header">
										소요시간
									</div>
								</div>
								<div class="col-xs-9 col-md-10">
									<div class="label-value">
										~30분 *다 만든 학생은 먼저 퇴실 가능
									</div>
								</div>
								<div class="col-xs-3 col-md-2">
									<div class="label-header">
										휴무일
									</div>
								</div>
								<div class="col-xs-9 col-md-10">
									<div class="label-value">
										과학관 휴관일
									</div>
								</div>
								<div class="col-xs-3 col-md-2">
									<div class="label-header">
										연령제한
									</div>
								</div>
								<div class="col-xs-2 col-md-2">
									<div class="label-value">
										없음
									</div>
								</div>
								<div class="col-xs-7 col-md-8">
									<div class="label-value">
										 *6세 이하는 보호자 동반 필수,
										7세 이상이어도 보호자 동반을 권장.
									</div>
								</div>
								<div class="col-xs-3 col-md-2">
									<div class="label-header">
										참여비
									</div>
								</div>
								<div class="col-xs-2 col-md-2">
									<div class="label-value">
										없음
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12 col-md-6">
						<ul class="circle-list teal">
							<li>
								참여 안내
							</li>
						</ul>
						<div class="desc-text">
							<div class="row">
								<div class="col-md-12">
									<div class="label-value">
										<span class="numbering">01</span> 키즈 메이커 스튜디오 방문
									</div>
								</div>
								<div class="col-md-12">
									<div class="label-value">
										<span class="numbering">02</span> 입구에 있는 신청서에 학생 성함 기입
									</div>
								</div>
								<div class="col-md-12">
									<div class="label-value">
										<span class="numbering">03</span> 신청한 시간에 맞춰 재방문
									</div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- kids-studio sub-banner END... -->
			</div>
		</div>
		<div class="container">
			<div id="major-exhibition-spy" class="section scrollspy">
				<div class="row">
					<div class="col-sm-3 col-md-2">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							주요전시물
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="main-exhibition-wrapper">
							<div id="main-exhibitions">
							</div>
							<table class="table table-bordered centered-table">
								<caption>
									[프로그램]
								</caption>
								<thead>
									<tr>
										<th>
											프로그램명
										</th>
										<th>
											내용 (20분 내외, 무료)
										</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											뱅글뱅글 팽이
										</td>
										<td>
											반투명 표지 종이에 대칭된 도형을 그려 이쑤시개를 꽂아 팽이 만들기
										</td>
									</tr>
									<tr>
										<td>
											비행체 만들기
										</td>
										<td>
											색종이, 종이컵 등을 이용해 바람에 뜨도록 만들기
										</td>
									</tr>
									<tr>
										<td>
											구슬길 만들기
										</td>
										<td>
											벽면에 구슬이 굴러갈 수 있도록 길 구성해 보기
										</td>
									</tr>
									<tr>
										<td>
											풍선 자동차
										</td>
										<td>
											풍선 바람의 반작용으로 움직이는 자동차 만들기 
										</td>
									</tr>
									<tr>
										<td>
											구슬 오뚝이
										</td>
										<td>
											구슬이 무게중심이 되어 쓰러지지 않는 오뚝이
										</td>
									</tr>
									<tr>
										<td>
											레버 오토마타
										</td>
										<td>
											지레의 원리를 이용해 날개가 움직이는 종이 인형 만들기
										</td>
									</tr>
									<tr>
										<td>
											종이 전자회로
										</td>
										<td>
											전도성 테이프를 연결해 LED에 불이 들어오도록 만들기
										</td>
									</tr>
									<tr>
										<td>
											그리기 팝업북
										</td>
										<td>
											자르고 붙여 만든 2페이지 팝업북에 팝업된 부분과 연결시켜 그리기
										</td>
									</tr>
									<tr>
										<td>
											암호로 말해요
										</td>
										<td>
											알파벳 0과 1로 암호화하여 털실로 표현한 뱃지 만들기
										</td>
									</tr>
									<tr>
										<td>
											풍선 비행체
										</td>
										<td>
											생김새에 따라 달라지는 낙하 물체를 만들고 관찰하기
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div id="group-reservation-spy" class="section scrollspy margin-bottom20">
				<div class="row">
					<div class="col-xs-6 col-md-3">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							단체예약 안내
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div>
							10명 이상인 경우 단체 예약 가능하며, 학교나 기관이 아닌 개인 10명 이상이 모인 단체도 가능합니다.
							※단, 체험학습으로 별도의 비용을 받고 대신 프로그램을 예약하는 업체의 경우 이용을 제한합니다.
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<ul class="arrow-list teal">
							<li>
								프로그램 : 선택 가능
							</li>
							<li>
								정원 : 회당 20명 (20명 초과인 경우 인원을 회차별로 나누어 예약)
							</li>
							<li>
								예약방법:
								<div class="indent">
									1. 현장 방문
								</div>
								<div class="indent">
									2. 이메일: kidsmakers@gmail.com
								</div>
							</li>
							<li>
								이메일 신청 시 기입 내용
								<div class="indent">
									단체명 / 신청 프로그램 / 참여 학생 연령 / 신청일,시간 / 담당자 연락처<br>
								</div>
								<div class="indent">
									※ 확인 후 <span class="color-red">이메일 회신</span> 또는 <span class="color-red">전화 연락</span> 드립니다.
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div id="open-studio-day-spy" class="section scrollspy margin-bottom20">
				<div class="row">
					<div class="col-xs-6 col-md-3">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							오픈 스튜디오 데이
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="margin-bottom20">
							오픈 스튜디오 데이는 시간대로 나눈 회차 구성 없이 일정시간 상시 개방하는 날입니다.
							선생님의 지도 없이 매뉴얼을 참고하여 자율적으로 제작품을 만들어갑니다. 부모님, 친구와 함께
							스스로 만들어보는 시간이며 비어있는 자리가 있을 때마다 원하는 시간에 자유롭게 참여하실 수 있습니다.
						</div>
					</div>
				</div>
				<div class="row margin-bottom20">
					<div class="col-md-12">
						<ul class="arrow-list teal">
							<li>
								시간 : 오전 10시 ~ 오후 3시
							</li>
							<li>
								날짜 : 매월 3주차 토요일 / 키즈메이커 스튜디오 지정 1일(월별 상이) 
							</li>
							<li>
								참여 인원 : 제한 없음
							</li>
							<li>
								참여 방법 개방 시간 내 선착순 참여
							</li>
						</ul>
					</div>
				</div>
				<div class="row margin-bottom20">
					<div class="col-md-12">
						<ul class="dash-list non-padding color-red">
						<li>
							직접 만든 완성품은 가져가도 되지만, 재료를 가지고 가는 것은 불가합니다.
						</li>
						<li>
							당일 주제에 맞는 재료는 준비되어 있으며, 다른 재료를 직접 가져와서 다른 만들기 활동을 하는 것도 가능합니다.
						</li>
						<li>
							재료 소진 시 행사가 종료 될 수 있습니다.
						</li>
					</ul>
					</div>
				</div>
				
				<%-- <div class="row margin-bottom20">
					<div class="col-md-12">
						<ul class="circle-list teal">
							<li class="calendar-title">
								${sangSangSchedule.title }
								
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 margin-bottom20">
						<ul class="arrow-list teal">
							<li>
								4일(금),5일(토)에는 '소프트웨어 교육 체험주간'으로 기존과 다른 시간으로 운영됩니다.
							</li>
							<li>
								12월 '오픈 스튜디오 데이'는 12/19(토), 12/25(금) 입니다.
							</li>
							<li>
								'오픈 스튜디오 데이'는 일정 시간 동안 (10시~15시) 개방하여,
								신청 없이 비어있는 자리에서 선착순으로 체험합니다.
								운영자의 도움 없이 매뉴얼을 참고해 스스로 만드는 날입니다.
							</li>
						</ul>
					</div>
					<div class="col-md-12 table-responsive">
						<table id="sangsang-schedules" class="table table-bordered centered-table">
							<thead>
								<tr>
									<th>
										일
									</th>
									<th>
										월
									</th>
									<th>
										화
									</th>
									<th>
										수
									</th>
									<th>
										목
									</th>
									<th>
										금
									</th>
									<th>
										토
									</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table> 
					</div>
					<div class="col-md-4">
						
					</div>
				</div>	 --%>
			</div>
		</div>
	</div>
</div>

<script id="sangsang-schedule-template" type="text/x-handlebars-template">
{{#each dates}}
	<tr>
		{{#each this}}
			<td class="date" data-date="{{formatedDate date "YYYY-MM-DD"}}">
				{{#if isInMonth}}
					<div class="day-no">
						{{formatedDate date "DD"}}
					</div>
					<div class="day-infor">
						{{title}}
					</div>
				{{/if}}
			</td>
		{{/each}}
	</tr>
{{/each}}
</script>

<script type="text/javascript">
	Handlebars.registerHelper('formatedDate', function(date, format) {
	  return moment(date).format(format);
	});
	
	var selectedDate = moment("<fmt:formatDate pattern="yyyy-MM-dd" value="${sangSangSchedule.beginMonth }" />", "YYYY-MM-DD").startOf('month');
	var dates = [];
	<c:forEach var="schedule" items="${sangSangSchedule.schedules }">
		dates.push({
			date: moment("<fmt:formatDate pattern="yyyy-MM-dd" value="${schedule.sDate }" />", "YYYY-MM-DD"),
			title: "${schedule.title }",
			isInMonth: true
		});
	</c:forEach>
	
	function renderCalendars(){
		var beginOfMonth = moment(selectedDate).startOf('month');
		var beginDate = moment(beginOfMonth).startOf('week');
		
		var endOfMonth = moment(selectedDate).endOf('month');
		var endDate = moment(endOfMonth).endOf('week');
		
		if(dates.length == 0){
			for(var temp = beginDate; temp.isBefore(endDate); temp.add(1, 'days')){
				var data = {};
				
				if(temp.isBefore(beginOfMonth) || temp.isAfter(endOfMonth)){
					data.isInMonth = false;
				}else{
					data.isInMonth = true;
				}
				
				data.date = temp.clone();
				
				dates.push(data);
			}
		}else{
			var beginTemp = moment(dates[0].date, "YYYY-MM-DD").startOf('week');
			var endTemp = moment(_.last(dates).date, "YYYY-MM-DD");
			
			for( ; beginTemp.isBefore(beginOfMonth); beginTemp.add(1, 'days')){
				dates.unshift({
					isInMonth: false,
					date: beginTemp.clone()
				});
			}
			
			for(endTemp.add(1, 'days') ; endTemp.isBefore(endDate); endTemp.add(1, 'days')){
				dates.push({
					isInMonth: false,
					date: endTemp.clone()
				});
			}
			
			console.log(dates);
		}
		
		resultedDates = _.chunk(dates, 7);
		
		var source = $("#sangsang-schedule-template").html();
		var template = Handlebars.compile(source);
		var html = template({dates: resultedDates});
		$("#sangsang-schedules > tbody").html(html);
	}
	
	renderCalendars();
</script>

<!-- Major Exhibition Contents Loading -->
<c:import url="/WEB-INF/views/display/majorExhibitionTemplate.jsp" />
<script type="text/javascript">
	var exhibitions = [
		{
			descriptions : [
				
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/frontier/kidsMakerStudio/major_exhibition_01.png' />",
					caption: "[키즈메이커스튜디오]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/frontier/kidsMakerStudio/major_exhibition_02.png' />",
					caption: "[키즈메이커스튜디오 수업]"
				},
			]
		},
		{
			descriptions : [
				
				{
					title: "뱅글뱅글 팽이, 회전과 관성을 배우다",
					desc : "색을 혼합하여 착시효과를 낼 수 있는 팽이를 만들어 본다. 팽이 만들기를 통해 회전 운동의 원리와 회전 관성을 이해할 수 있다."
				},
				{
					title: "풍선자동차 만들기, 작용-반작용을 배우다",
					desc : "풍선 바람의 반작용을 통해 움직이는 자동차를 만들어 본다. 움직이는 풍선 자동차 제작을 통해 작용-반작용 법칙을 이해할 수 있다."
				}
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/frontier/kidsMakerStudio/major_exhibition_03.png' />",
					caption: "[뱅글뱅글 팽이]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/frontier/kidsMakerStudio/major_exhibition_04.png' />",
					caption: "[풍선자동차 만들기]"
				},
			]
		},
		{
			descriptions : [
				{
					title : "암호로 말해요, 컴퓨터 작동원리를 배우다",
					desc : "알파벳 문자를 숫자화하고 그 숫자를 이진수로 코딩하는 털실 배지를 제작한다. 배지 제작을 통해 컴퓨팅의 기본 작동원리를 이해할 수 있다."
				},
				{
					title : "이렇게 참여할 수 있어요 ",
					desc : "키즈메이커가 되고 싶다면 과학관 입장 후  1층에 있는 키즈메이커스튜디오를 방문하면 된다. "
						+ "1일 5회(10시, 11시, 14시, 15시, 16시) 키즈메이커를 위한 재미있는 프로그램이 준비되어 있다. "
						+ "원하는 시간대에 예약하고 참 여하면 키즈메이커 되기 완료! 10종의 다양한 체험프로그램이 매일 순환 운영되고 있다. "   
						+ "<div class='additional-info'>(10명 이내/회, 만5세 이상)</div>"  
				}
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/frontier/kidsMakerStudio/major_exhibition_05.png' />",
					caption: "[암호로 말해요]"
				}
			]
		}
	];
	
	var exhibitionSource   = $("#major-exhibition-template").html();
	var exhibitionTemplate = Handlebars.compile(exhibitionSource);
	var exhibitionHtml = exhibitionTemplate({exhibitions: exhibitions});
	$("#main-exhibitions").html(exhibitionHtml);
	
</script>