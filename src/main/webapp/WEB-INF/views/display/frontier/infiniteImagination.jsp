<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-teal">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>
	<div id="display-body" class="sub-body">
		<div class="narrow-sub-top display">
			<div class="sub-banner-tab-wrapper sub-banner-tab-teal">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<ul class="nav nav-tabs">
								<li class="col-xs-12 col-sm-6 col-md-4">
									<a href="<c:url value="/display/frontier/kidsMakerStudio" />" class="item">
										키즈메이커스튜디오
									</a>
								</li>
								<li class="col-xs-12 col-sm-6 col-md-4">
									<a href="<c:url value="/display/frontier/freeMarket" />" class="item">
										메이커프리마켓 
									</a>
								</li>
								<%-- <li class="col-xs-12 col-sm-6 col-md-3">
									<a href="<c:url value="/display/frontier/familySLand" />" class="item">
										패밀리창작놀이터
									</a>
								</li> --%>
								<li class="col-xs-12 col-sm-6 col-md-4 active">
									<a href="#" class="item">
										무한상상실
									</a>
								</li>
							</ul>   
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="support-sub-nav">  
			<div class="container">
			   	<div class="sub-nav">
					<div class="row">
						<div class="col-xs-6 col-md-2-5">
				     		<a href="#" class="item active">
								<i class="fa fa-book" aria-hidden="true"></i> 무한상상실 안내
							</a> 
				     		<div class="visible-xs" style="margin-bottom: 15px"></div> 
						</div>
						<div class="col-xs-6 col-md-2-5">
				     		<a href="<c:url value="/sangsang/list/device" />" class="item">
								<i class="fa fa-print" aria-hidden="true"></i> 장비 예약
							</a> 
				     		<div class="visible-xs" style="margin-bottom: 15px"></div> 
						</div>
						<div class="col-xs-6 col-md-2-5">
						    <a href="<c:url value="/display/frontier/incubator" />" class="item">
								<i class="fa fa-cube" aria-hidden="true"></i> 메이커인큐베이터 
							</a>
						    <div class="visible-xs" style="margin-bottom: 15px"></div> 
						</div>
						<div class="col-xs-6 col-md-2-5">
						    <a href="javascript:void(0)" class="item" onclick="AcademyChoice('ACD008')">
								<i class="fa fa-calendar-check-o" aria-hidden="true"></i> 워크숍 예약
							</a>
							<div class="visible-xs" style="margin-bottom: 15px"></div> 
						</div>
						<div class="col-xs-6 col-md-2-5">
						    <a href="http://cyber.scientorium.go.kr/museum_2015/vrLoad.jsp?pavilion=indoor&xml_name=img_001.xml&angle=-90"
								class="item">
								<i class="fa fa-desktop" aria-hidden="true"></i> 사이버 전시관
							</a>
						</div>				     
					</div>  
				</div>
			</div>
		</div> 
		<div class="container">
			<div id="basic-map-spy" class="section scrollspy">
				<div class="row">
					<div class="col-xs-4 col-md-3">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							무한상상실 안내
						</h3>
					</div>
				</div>
				<div class="row description">
					<div class="col-sm-12 col-md-4 col-lg-4">
						<ul class="circle-list teal">
							<li>
								관람안내
								<ul>
									<li>
										「무한상상실(Idea Factory)」은 상상 속에 존재하던 아이디어를 
										현실 세계로 이끌어내는 창작 공간이다. 전문가와 머리를 맞대고 의논할 수 있으며 
										이를 디지털 제작 장비로 만들어 볼 수 있다. 창의적인 아이디어가 있다면 누구나 와서 
										직접(DIY) 제작해 보자.
									</li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="col-sm-12 col-md-8 col-lg-8 text-right">
						<!-- select dropdown : 다른관으로 이동 -->
						<c:import url="/WEB-INF/views/display/displayHallSelect.jsp"/>
						<div>
							<img alt="무한상상실" src="<c:url value='/resources/img/display/map/08.png'/>"
								class="img-responsive margin-top20">
						</div>
					</div>
				</div>
			</div>
			<div id="basic-guide-spy" class="section scrollspy margin-bottom20">
				<!-- <div class="row">
					<div class="col-xs-6 col-md-3">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							무한상상실 소개
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="row infinite-guide guide1">
							<div class="col-md-3">
								<div class="guide-icon">
									<i class="material-icons">&#xE3E3;</i>
								</div>
							</div>
							<div class="col-md-9">
								<div class="text">
									<div class="header">
										상상노하우
									</div>
									<div class="desc">
										전문가들과 토의하여 아이디어를 어떻게 실현할지 고민하고 만듭니다.
									</div>
									<div class="row">
										<div class="info">
											<div class="col-xs-5 col-md-3">
												<div class="title">
													대상
												</div>
											</div>
											<div class="col-xs-7 col-md-9">
												<div class="value">
													초등 4학년 - 일반인
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="info">
											<div class="col-xs-5 col-md-3">
												<div class="title">
													참여방법
												</div>
											</div>
											<div class="col-xs-7 col-md-9">
												<div class="value">
													e-mail 접수, 상상위원회 선정/개별맨토링
												</div>
											</div>
										</div>									
									</div>
									<div class="row">
										<div class="info">
											<div class="col-xs-5 col-md-3">
												<div class="title">
													문의
												</div>
											</div>
											<div class="col-xs-7 col-md-9">
												<div class="value">
													i_knowhow@naver.com
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="row infinite-guide guide2">
							<div class="col-md-3">
								<div class="guide-icon">
									<i class="material-icons">&#xE90F;</i>
								</div>
							</div>
							<div class="col-md-9">
								<div class="text">
									<div class="header">
										상상반짝
									</div>
									<div class="desc">
										발명의 원리를 이해하고 창이적인 문제해결방법(TRIZ) 기법을 통해
										자신만의 아이디어를 도출합니다.
									</div>
									<div class="row">
										<div class="info">
											<div class="col-xs-5 col-md-3">
												<div class="title">
													대상
												</div>
											</div>
											<div class="col-xs-7 col-md-9">
												<div class="value">
													초등 4학년 - 일반인
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="info">
											<div class="col-xs-5 col-md-3">
												<div class="title">
													참여방법
												</div>
											</div>
											<div class="col-xs-7 col-md-9">
												<div class="value">
													인터넷 예약
												</div>
											</div>
										</div>									
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="row infinite-guide guide3">
							<div class="col-md-3">
								<div class="guide-icon">
									<i class="material-icons">&#xE87B;</i>
								</div>
							</div>
							<div class="col-md-9">
								<div class="text">
									<div class="header">
										상상만들기
									</div>
									<div class="desc">
										디지털 제작 장비를 이용해 아이디어를 직접 제작합니다.
									</div>
									<div class="row">
										<div class="info">
											<div class="col-xs-5 col-md-3">
												<div class="title">
													대상
												</div>
											</div>
											<div class="col-xs-7 col-md-9">
												<div class="value">
													초등 4학년 - 일반인
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="info">
											<div class="col-xs-5 col-md-4">
												<div class="title">
													참여방법
												</div>
											</div>
											<div class="col-xs-7 col-md-8">
												<div class="value">
													인터넷 예약
												</div>
											</div>
										</div>									
									</div>
								</div>
							</div>
						</div>
					</div>
				</div> row END  -->
			</div>
			<div id="major-exhibitions-spy" class="section scrollspy">
				<div class="row">
					<div class="col-sm-3 col-md-2">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							주요전시물
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="main-exhibition-wrapper">
							<div id="main-exhibitions">
							</div>
						</div>
					</div>
				</div>
				<div id="story-spy" class="story-telling-wrapper">
					<div class="header">
						<div class="row">
							<div class="col-md-6">
								<h3 class="page-title teal">
									<div class="top-line"></div>
									스토리로 읽어 보는 무한상상실 여행기   
								</h3>
							</div>
						</div>
					</div>
					<div class="description">
						오늘 국립과천과학관에 오면서 여러분은 어떤 상상들을 했나요?<br> 
						‘하늘을 날아서 오면 과학관에 조금 더 빨리 갈 수 있을 텐데……’<br>
						‘오래오래 과학관에서 친구들과 시간을 보낼 수 있게, 시계가 잠깐 멈췄으면 좋겠다.’<br>
						‘과학관 설명을 아인슈타인이 과거에서 살아와 해준다면 어떨까?’<br><br>
						
						한계나 답이 없이 내 마음대로 그려나갈 수 있어 더욱 즐거운 상상의 세계! <br>
						모든 발명과 과학의 시작은 상상이었습니다. 오늘 무한상상실에서는 여러분의 상상이 현실이 되는 
						신나는 체험을 선물해준다고 하는데요. 어떤 체험들이 기다리고 있는지 발걸음을 서둘러 보겠습니다.
					</div>
					
					<div id="stories" class="row">
					</div>
				</div>
				
				<div class="related-photos">
					<div class="title">
						<i class="color-darkgreen material-icons">&#xE413;</i> 관련사진
					</div>
					<div id="image-slider">
						<div id="image-slider-window">
							<img src="<c:url value="/resources/img/display/frontier/infiniteImagination/related_photo_01.png"/>"
								data-url="/resources/img/display/frontier/infiniteImagination/related_photo_01.png"	class="slide" />
							<img src="<c:url value="/resources/img/display/frontier/infiniteImagination/related_photo_02.png"/>"
								data-url="/resources/img/display/frontier/infiniteImagination/related_photo_02.png"	class="slide" />
							<img src="<c:url value="/resources/img/display/frontier/infiniteImagination/related_photo_03.png"/>"
								data-url="/resources/img/display/frontier/infiniteImagination/related_photo_03.png"	class="slide" />
							<img src="<c:url value="/resources/img/display/frontier/infiniteImagination/related_photo_04.png"/>"
								data-url="/resources/img/display/frontier/infiniteImagination/related_photo_04.png"	class="slide" />
							<img src="<c:url value="/resources/img/display/frontier/infiniteImagination/related_photo_05.png"/>"
								data-url="/resources/img/display/frontier/infiniteImagination/related_photo_05.png"	class="slide" />
							<img src="<c:url value="/resources/img/display/frontier/infiniteImagination/related_photo_06.png"/>"
								data-url="/resources/img/display/frontier/infiniteImagination/related_photo_06.png"	class="slide" />
							<img src="<c:url value="/resources/img/display/frontier/infiniteImagination/related_photo_07.png"/>"
								data-url="/resources/img/display/frontier/infiniteImagination/related_photo_07.png"	class="slide" />
							<img src="<c:url value="/resources/img/display/frontier/infiniteImagination/related_photo_08.png"/>"
								data-url="/resources/img/display/frontier/infiniteImagination/related_photo_08.png"	class="slide" />
							<img src="<c:url value="/resources/img/display/frontier/infiniteImagination/related_photo_09.png"/>"
								data-url="/resources/img/display/frontier/infiniteImagination/related_photo_09.png"	class="slide" />
							<img src="<c:url value="/resources/img/display/frontier/infiniteImagination/related_photo_10.png"/>"
								data-url="/resources/img/display/frontier/infiniteImagination/related_photo_10.png"	class="slide" />
							<img src="<c:url value="/resources/img/display/frontier/infiniteImagination/related_photo_11.png"/>"
								data-url="/resources/img/display/frontier/infiniteImagination/related_photo_11.png"	class="slide" />
							<img src="<c:url value="/resources/img/display/frontier/infiniteImagination/related_photo_12.png"/>"
								data-url="/resources/img/display/frontier/infiniteImagination/related_photo_12.png"	class="slide" />
						</div>
					</div>
				</div>
			</div><!--  basic Guide spy END... -->
		</div><!--  container END...  -->
		
		<!-- 장비 안내 -->
		<div id="current-equipment-spy" class="scrollspy">
			<%-- <div class="sub-banner">
				<div class="container">
					<div class="banner-text-wrapper">
						<div class="row">
							<div class="col-md-12">
								<div class="banner-header">									
									<img src="<c:url value="/resources/img/display/frontier/infiniteImagination/equipment_banner_text.png"/>" class="img-responsive"/>
								</div>								
							</div>
						</div>
						<div class="row">
							<div class="col-md-8">
								<div class="banner-desc">
									대형 CNC라우터, 레이저 커터, 3D 프린터, 비닐커터, 탁상용 CNC조각기 등
									디지털 제작 장비를 보유하고 있습니다.
								</div>
							</div>		
							<div class="col-md-4">
								<div class="banner-btn text-right">
									<a href="<c:url value="/sangsang/list/device" />" >
										장비 예약 바로가기		
									</a>
								</div>
							</div>				
						</div>
					</div>					
				</div>
			</div> --%>
			<div class="container">
				<div class="row">
					<div class="col-xs-6 col-md-3">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							무한상상실 보유장비
						</h3>
					</div>			
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="equipment-card">
							<div class="row">
								<div class="col-md-4">
									<div class="equipment-label">
										01 3D프린터
									</div>								
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4 col-md-4">
									<div class="equipment-img">
										<img src="<c:url value="/resources/img/display/frontier/infiniteImagination/equipment_list01.png"/>" class="img-responsive"/>
									</div>
								</div>
								<div class="col-xs-8 col-md-8">
									<div class="equipment-desc">
										3D 프린터는 이미 많은 사람들이 집에서 사용하고 있는 종이 프린터와
										똑같은 일을 합니다. 2D 프린터는 컴퓨터 화면의 픽셀을 인식해
										2D 매체인 종이에 잉크로 점을 찍어서 인쇄합니다.
										반면 3차원 프린터는 액체 플라스틱을 분사해 쌓아올리는 방식으로
										입체적인 물건을 만듭니다. 또한 레이저를 사용해 액체나 분말상태인 완료를 굳히기도 합니다.
									</div>
								</div>
							</div>
						</div>					
					</div>
					<div class="col-md-6">
						<div class="equipment-card">
							<div class="row">
								<div class="col-md-4">
									<div class="equipment-label">
										02 CNC 라우터
									</div>								
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4 col-md-4">
									<div class="equipment-img">
										<img src="<c:url value="/resources/img/display/frontier/infiniteImagination/equipment_list02.png"/>" class="img-responsive"/>
									</div>
								</div>
								<div class="col-xs-8 col-md-8">
									<div class="equipment-desc">
										CNC 라우터는 플라스틱 나무 금속 블록을 드릴로 깎아 물건을 만듭니다.
										CNC 라우터는 여러 가지 종류가 잇는데, 이불을 누비고 자수를 놓는 기계,
										실크 스크린 인쇄물을 만드는 기계, 수공예에 사용할 종이와 천을 자르는 기계도 있습니다.
										컴퓨터에 의해 조종되기 때문에 매우 다양하고 정밀한 형태로 가공할 수 있습니다.
									</div>
								</div>
							</div>
						</div>					
					</div>
					<div class="col-md-6">
						<div class="equipment-card">
							<div class="row">
								<div class="col-md-4">
									<div class="equipment-label">
										03 CNC 조각기
									</div>								
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4 col-md-4">
									<div class="equipment-img">
										<img src="<c:url value="/resources/img/display/frontier/infiniteImagination/equipment_list03.png"/>" class="img-responsive"/>
									</div>
								</div>
								<div class="col-xs-8 col-md-8">
									<div class="equipment-desc">
										CNC 조각기는 수려한 외관과, 금형소재의 초정밀 가공, 초소형가공 등에 이용할 수 있습니다. 
									</div>
								</div>
							</div>
						</div>					
					</div>
					<div class="col-md-6">
						<div class="equipment-card">
							<div class="row">
								<div class="col-md-4">
									<div class="equipment-label">
										04 비닐 커터
									</div>								
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4 col-md-4">
									<div class="equipment-img">
										<img src="<c:url value="/resources/img/display/frontier/infiniteImagination/equipment_list04.png"/>" class="img-responsive"/>
									</div>
								</div>
								<div class="col-xs-8 col-md-8">
									<div class="equipment-desc">
										비닐 커터는 컴퓨터에 의해 조종되는 가공 기계로 컴퓨터가 예리한 칼날의 움직임을 조종하고,
										이 칼날이 얇은 비닐위에 모양이나 글자등을 절단해 냅니다.
									</div>
								</div>
							</div>
						</div>					
					</div>
					<%-- <div class="col-md-6">
						<div class="equipment-card software">
							<div class="row margin-bottom20">
								<div class="col-md-4">
									<div class="equipment-label">
										05 소프트웨어
									</div>								
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="software-item">
										<div class="item-label">
											추천하는 2차원 드로잉 프로그램
										</div>
										<div class="item-list">
											무료: 잉크스케이프 inkscape (윈도우와 맥 사용 가능)
										</div>
										<div class="item-list">
											유료: 아도브 일러스트레이터 adobe illustrator (왼도우와 맥 사용 가능)
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="software-item">
										<div class="item-label">
											추천하는 3차원 드로잉 프로그램
										</div>
										<div class="item-list">
											무료: 구글 스케치업 오토데스크123D autode 나 
										</div>
										<div class="item-list">
											유료: 아도브 일러스트레이터 adobe illustrator (왼도우와 맥 사용 가능)
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="software-item">
										<div class="item-label">
											추천하는 2차원 드로잉 프로그램
										</div>
										<div class="item-list">
											무료: 잉크스케이프 inkscape (윈도우와 맥 사용 가능)
										</div>
										<div class="item-list">
											유료: 아도브 일러스트레이터 adobe illustrator (왼도우와 맥 사용 가능)
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="software-item">
										<div class="item-label">
											추천하는 2차원 드로잉 프로그램
										</div>
										<div class="item-list">
											무료: 잉크스케이프 inkscape (윈도우와 맥 사용 가능)
										</div>
										<div class="item-list">
											유료: 아도브 일러스트레이터 adobe illustrator (왼도우와 맥 사용 가능)
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="software-item">
										<div class="item-label">
											추천하는 2차원 드로잉 프로그램
										</div>
										<div class="item-list">
											무료: 잉크스케이프 inkscape (윈도우와 맥 사용 가능)
										</div>
										<div class="item-list">
											유료: 아도브 일러스트레이터 adobe illustrator (왼도우와 맥 사용 가능)
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="software-item">
										<div class="item-label">
											추천하는 2차원 드로잉 프로그램
										</div>
										<div class="item-list">
											무료: 잉크스케이프 inkscape (윈도우와 맥 사용 가능)
										</div>
										<div class="item-list">
											유료: 아도브 일러스트레이터 adobe illustrator (왼도우와 맥 사용 가능)
										</div>
									</div>
								</div>
							</div>
						</div>					
					</div>
					<div class="col-md-6">
						<div class="equipment-card facility">
							<div class="row margin-bottom20">
								<div class="col-md-4">
									<div class="equipment-label">
										06 시설
									</div>								
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6 col-md-6">
									<div class="equipment-img">
										<img src="<c:url value="/resources/img/display/frontier/infiniteImagination/facility_01.png"/>" class="img-responsive"/>
									</div>
								</div>
								<div class="col-xs-6 col-md-6">
									<div class="equipment-desc">
										<div class="facility-title">
											상상 토의실
										</div>
										<div class="facility-desc">
											수용인원: 20명
										</div>
										<div class="facility-desc">
											컴퓨터, 빔 프로젝터, 맥킨토시, 전화 등
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6 col-md-6">
									<div class="equipment-img">
										<img src="<c:url value="/resources/img/display/frontier/infiniteImagination/facility_02.png"/>" class="img-responsive"/>
									</div>
								</div>
								<div class="col-xs-6 col-md-6">
									<div class="equipment-desc">
										<div class="facility-title">
											상상 공장실
										</div>
										<div class="facility-desc">
											수용인원: 10명
										</div>
										<div class="facility-desc">
											FAB,LAB. 장비
										</div>
									</div>
								</div>
							</div>
						</div>					
					</div> --%>
				</div>
			</div>
		</div>
			
		<div class="container">
			
		</div>
	</div>
</div>

<form name="frmIndex">
	<input type="hidden" name="ACADEMY_CD" value="ACD008" />
	<input type="hidden" name="COURSE_CD" value="" />
	<input type="hidden" name="SEMESTER_CD" value="" />
	<input type="hidden" name="TYPE" value="" />
	<input type="hidden" name="FLAG" value="" />
	<input type="hidden" name="CLASS_CD" value="CL8001" />
</form>

<script id="sangsang-schedule-template" type="text/x-handlebars-template">
{{#each dates}}
	<tr>
		{{#each this}}
			<td class="date" data-date="{{formatedDate date "YYYY-MM-DD"}}">
				{{#if isInMonth}}
					<div class="day-no">
						{{formatedDate date "DD"}}
					</div>
					<div class="day-infor">
						{{title}}
					</div>
				{{/if}}
			</td>
		{{/each}}
	</tr>
{{/each}}
</script>

<script type="text/javascript">
	Handlebars.registerHelper('formatedDate', function(date, format) {
	  return moment(date).format(format);
	});
	
	var selectedDate = moment("<fmt:formatDate pattern="yyyy-MM-dd" value="${sangSangSchedule.beginMonth }" />", "YYYY-MM-DD").startOf('month');
	var dates = [];
	<c:forEach var="schedule" items="${sangSangSchedule.schedules }">
		dates.push({
			date: moment("<fmt:formatDate pattern="yyyy-MM-dd" value="${schedule.sDate }" />", "YYYY-MM-DD"),
			title: "${schedule.title }",
			isInMonth: true
		});
	</c:forEach>
	
	function renderCalendars(){
		var beginOfMonth = moment(selectedDate).startOf('month');
		var beginDate = moment(beginOfMonth).startOf('week');
		
		var endOfMonth = moment(selectedDate).endOf('month');
		var endDate = moment(endOfMonth).endOf('week');
		
		if(dates.length == 0){
			for(var temp = beginDate; temp.isBefore(endDate); temp.add(1, 'days')){
				var data = {};
				
				if(temp.isBefore(beginOfMonth) || temp.isAfter(endOfMonth)){
					data.isInMonth = false;
				}else{
					data.isInMonth = true;
				}
				
				data.date = temp.clone();
				
				dates.push(data);
			}
		}else{
			var beginTemp = moment(dates[0].date, "YYYY-MM-DD").startOf('week');
			var endTemp = moment(_.last(dates).date, "YYYY-MM-DD");
			
			for( ; beginTemp.isBefore(beginOfMonth); beginTemp.add(1, 'days')){
				dates.unshift({
					isInMonth: false,
					date: beginTemp.clone()
				});
			}
			
			for(endTemp.add(1, 'days') ; endTemp.isBefore(endDate); endTemp.add(1, 'days')){
				dates.push({
					isInMonth: false,
					date: endTemp.clone()
				});
			}
			
			console.log(dates);
		}
		
		resultedDates = _.chunk(dates, 7);
		
		var source = $("#sangsang-schedule-template").html();
		var template = Handlebars.compile(source);
		var html = template({dates: resultedDates});
		$("#sangsang-schedules > tbody").html(html);
	}
	
	renderCalendars();
</script>


<script id="story-template" type="text/x-handlebars-template">
{{#each stories}}
<div class="col-xs-12 col-sm-6 col-md-6">
	<div class="display-card story-telling story-more-btn pointer" data-idx="{{@index}}" onclick="">
		<div class="card-image">
			<img src="{{thumbnailUrl}}" class="img-responsive"/>
		</div>
		<div class="card-content">
			<div class="display-card-title">
				{{title}}
			</div>
			<div class="display-card-content">
				{{{desc}}}
			</div>
			
		</div>
	</div>							
</div>
{{/each}}
</script>

<script type="text/javascript">
	var stories = [
		{
			title: "상상력이 필요한 체험들",
			desc: "뚝딱뚝딱 공작실에서는 ‘구슬길 만들기’와 같이 다양한 공작 활동을 통해 상상력을 자극할 수 있습니다. "
				+ "친구들과 함께 만들기를 해보면서 머릿속 준비 운동을 해보세요! 앞으로 상상력이 필요한 많은 체험들이 기다리고 있으니까요.",
			thumbnailUrl: "<c:url value="/resources/img/display/frontier/infiniteImagination/story07_01.JPG"/>",
			stories: [
				{
					storyLabel: "뚝딱뚝딱 공작실",
					storyDesc: "뚝딱뚝딱 공작실에서는 ‘구슬길 만들기’와 같이 다양한 공작 활동을 통해 상상력을 자극할 수 있습니다. "
						+ "친구들과 함께 만들기를 해보면서 머릿속 준비 운동을 해보세요! 앞으로 상상력이 필요한 많은 체험들이 기다리고 있으니까요.",
					imgUrl: "<c:url value="/resources/img/display/frontier/infiniteImagination/story07_01.jpg"/>"
				},
				{
					storyLabel: "상상노하우실",
					storyDesc: "SF 영화를 만드는 원리를 볼 수 있는 SF 스튜디오를 지나면 상상 노하우실이 나타납니다. "
						+ "상상하는 것도 우리 몸에 근육이 붙는 것과 같아서 연습하면 연습할수록 더 많은, "
						+ "또 더 다양한 상상을 할 수 있다고 하는데요. 이 곳을 찾은 학생들의 모습이 무척 진지해 보입니다. "
						+ "더 자유롭고 창의적으로 상상할 수 있도록 열심히 노력하는 모습들을 보니 우리나라 과학의 미래가 기대됩니다.",
					imgUrl: "<c:url value="/resources/img/display/frontier/infiniteImagination/story07_02.jpg"/>"
				},
				{
					storyLabel: "디자인실",
					storyDesc: "자! 상상력 훈련을 한 학생들의 얼굴이 기대감과 호기심으로 가득해 보입니다. "
							+ "이제부터, 머릿속에 있는 생각들을 눈 앞에 물건으로 만들어 볼 것이라고 하는데요. "
							+ "모두 바쁘게 디자인실로 이동합니다. 이 곳에서는 머릿속 상상을 공간기계로 설계하고, "
							+ "이 것을 USB에 담아 볼 수 있습니다. 주차할 때마다 곤란한 아빠를 위해, 주머니에 "
							+ "넣고 다니다가 타기 전에 크게 확대할 수 있는 자동차를 상상한 학생은 자신의 손바닥 만한 "
							+ "자동차를 설계했네요. 설계 데이터를 USB에 잘 담아 다음 공간으로 이동하고 있습니다. "
							+ "어떤 모습의 자동차가 나올지 한번 함께 따라가 볼까요?",
					imgUrl: "<c:url value="/resources/img/display/frontier/infiniteImagination/story07_03.jpg"/>"
				},
				{
					storyLabel: "3D프린터실",
					storyDesc: "재료를 설계도에 따라 자르고 파낼 수 있는 레이져커터실과 측정, "
						+ "조명기구 등의 재료와 물품을 내가 생각한대로 만들 수 있는 ICT 소프트웨어실을 구경한 학생은 3D 프린터실에 들어섰습니다. "
						+ "USB에 담아왔던 자동차를 3D 프린터로 완성해 볼 예정인대요. \n"
						+ "자, 작은 선들이 쌓여 서서히 그 모습이 드러나고 있습니다. 자동차를 설계한 학생도, "
						+ "선물 받을 아빠도 모두 기대감에 가득 찬 얼굴입니다. 그리고 드디어 학생의 손바닥만한 "
						+ "멋진 자동차가 완성되었습니다. 자동차를 주머니에 넣어보니 한 번에 쏙! 상상을 완성한 "
						+ "기분이 어떤지 한 번 들어보았습니다. \n\n"
						+ "“상상은 아무나 할 수 없다고 생각했는데, 상상을 하는 것만으로도 즐겁고 "
						+ "그 것을 눈으로 확인하니까 더 신났던 것 같아요. 앞으로도 사람들에게 도움을 줄 수 있는 "
						+ "많은 상상을 하고 싶어요!”\n\n" 
						+ "실제로 이런 휴대용 자동차가 만들어진다면 성형실과 페인트실을 거쳐 더 멋진 모습으로 탄생할 수 있겠죠?\n\n" 
						+ "상상은 모두에게 주어진 큰 선물입니다. 조금 더 나은 내가 되고, 조금 더 살기 좋은 세상이 "
						+ "될 수 있도록 여러분의 상상을 무한대로 펼쳐보세요. 그리고 그 시작이 무한상상실이 될 수 " 
						+ "있으면 좋겠습니다. 그럼 이 곳에서 품은 상상을 조금 더 큰 생각으로 키우기 위해 과학관 여행을 이어가겠습니다.",
					imgUrl: "<c:url value="/resources/img/display/frontier/infiniteImagination/story07_04.jpg"/>"
				}
			]
		}

	];
	
	var storySource   = $("#story-template").html();
	var storyTemplate = Handlebars.compile(storySource);
	var storyHtml = storyTemplate({stories: stories});
	$("#stories").html(storyHtml);
</script>

<c:url var="shareUrl" value="/display/frontier/infiniteImagination" />
<c:import url="/WEB-INF/views/display/displayDetailModal.jsp">
	<c:param name="shareUrl" value="${shareUrl }"/>
	<c:param name="exhibitionName" value="무한상상실" />
</c:import>
<script type="text/javascript">
	$(document).ready( function() {
		// 주요 전시물 클릭시 show hide event
		//showHideDetails();

		// image slider
		PTechSlider('#image-slider').autoSlide();
		
		// 전시관 Map 1층으로.
		//setDefaultSVGMap('#basic-map-spy',1);
		// 지도 flash 해당 전시관 표시
		//$('#XMLID_2_').attr('class', 'active');
	});

	function showHideDetails() {
		// hide all details first.
		$('.exhibition-detail-wrapper').hide();
		// show the active detail only.
		var exhibit_active_id = $('.major-exhibition-wrapper.active')
				.data('id');
		$(exhibit_active_id).show();

		// click event for major exhibition box
		$('.major-exhibition-wrapper').on('click', function() {

			// get current active div & remove active
			var hide_id = $('.major-exhibition-wrapper.active').data('id');
			$('.major-exhibition-wrapper.active').removeClass('active');
			// hide the matching detail
			$(hide_id).hide();

			// add 'active' to the current clicked div
			var show_id = $(this).data('id');
			$(this).addClass('active');
			// show the mathcing detail
			$(show_id).show();

		});
	}
	
	function AcademyChoice(Cd){
		var frm = document.frmIndex;
		frm.ACADEMY_CD.value = Cd;
		frm.CLASS_CD.value = "";
		frm.action = '<c:url value="/schedules"/>';
		frm.method = 'GET';
		frm.submit();
	}

</script>

<!-- Major Exhibition Contents Loading -->
<c:import url="/WEB-INF/views/display/majorExhibitionTemplate.jsp" />
<script type="text/javascript">
	var exhibitions = [
		{
			descriptions : [
				
				{
					title: "3D 프린터실, 입체적인 물건을 인쇄하다",
					desc : "상상한 아이디어를 설계한 후 3D 프린터로 출력하면 플라스틱 소재의 입체적인 물건을  만들어 볼 수 있다. "
						+ "작은 컵에서부터 탈 수 있는 자전거까지 다양한 형태로 출력이 가능하다. 3D 프린터실에서 원하는 물건을 직접 "
						+ "만들며 스티브 잡스가 되는 경험을 해보자."
				},
				{
					title: "SF 스튜디오, 이제 나도 스티븐 스필버그",
					desc : "공룡이 뛰어 다니고 외계인과 만나는 스토리를 구성하고, 직접 영상을 촬영해 편집하는 방법을 배운다. "
						+ "나만의 스토리가 담긴 SF 영상 제작을 통해 미래의 스티븐 스필버그가 되는 경험을 해볼 수 있다."
				}
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/frontier/infiniteImagination/major_exhibition_01.png' />",
					caption: "[3D 프린터실]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/frontier/infiniteImagination/major_exhibition_02.png' />",
					caption: "[SF 스튜디오 영상 제작]"
				},
			]
		},
		{
			descriptions : [
				{
					title : "ICT 소프트웨어실, 알파고의 시작",
					desc : "인공지능 로봇, 무인 자동차와 같은 최신 기술을 사용한 전자 제품의 기초 작동원리를 학습한다. 자기가 생각한대로 작동하는 시제품을 만들어 보면서 새로운 미래를 만드는 에디슨이 되는 경험을 해보자."
				},
				{
					title : "장비워크숍, 누구나 메이커(Maker)가 될 수 있다",
					desc : "누구나 상상했던 아이디어를 현실에서 직접 실현했던 스티븐잡스, 에디슨이 될 수 있다. "
						+ "3D 프린터, 손바닥 컴퓨터, 레이저커터, 영상 등 다양한 디지털 제조 장비의 작동방법을 친절하게 "
						+ "알려주는 장비워크숍에 참여해보자. 그리고 상상한 아이디어를 직접 제작해보자. "
						+ "어느 순간 메이커가 되어 있는 나를 발견할 것이다."  
						+ "<div class='additional-info'>(10명 이내/회, 중학생 이상)</div>"
				}
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/frontier/infiniteImagination/major_exhibition_03.png' />",
					caption: "[ICT 소프트웨어실]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/frontier/infiniteImagination/major_exhibition_04.png' />",
					caption: "[다양한 디지털 제조 장비]"
				}
			]
		}
	];
	
	var exhibitionSource   = $("#major-exhibition-template").html();
	var exhibitionTemplate = Handlebars.compile(exhibitionSource);
	var exhibitionHtml = exhibitionTemplate({exhibitions: exhibitions});
	$("#main-exhibitions").html(exhibitionHtml);
	
</script>