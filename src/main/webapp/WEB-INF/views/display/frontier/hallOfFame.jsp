<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-teal">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>
	<div id="display-body" class="sub-body">
		<div class="narrow-sub-top display">
			<!-- <div class="sub-banner-tab-wrapper sub-banner-tab-teal">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
							<ul class="nav nav-tabs">
								<li class="col-xs-12 active">
									<a href="http://cyber.scientorium.go.kr/museum_2015/vrLoad.jsp?pavilion=indoor&xml_name=img_001.xml&angle=-150" class="item">
										<i class="fa fa-desktop fa-lg" aria-hidden="true"></i> 사이버 전시관
									</a>  		
								</li>
							</ul>   
						</div>
					</div>
				</div>
			</div>	 -->		
		</div>
		<div class="container">
			
			<div id="basic-map-spy" class="section scrollspy">
				<div class="row">
					<div class="col-sm-3 col-md-2">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							명예의전당 안내					
						</h3>
					</div>
				</div>
				<div class="row description">
					<div class="col-sm-12 col-md-4 col-lg-4">
						<ul class="circle-list teal">
							<li>
								주요전시물
								<ul>
									<li>
										<b>「명예의 전당」</b>은 과학기술 발전에 기여한 과학자의 업적을 기리고, 
										우리나라 과학기술의 우수 성과와 발전과정을 살펴보는 공간이다. 
										장영실, 허준, 이휘소, 석주명 등 31인의 명예로운 과학자가 헌정되어 있다. 
										이곳에는 ‘나도 명예로운 과학자’ 등 2건의 체험물을 포함한 총 
										35건의 전시물로 구성되어 있다. 과학자들의 업적을 스토리텔링 형식으로 
										단장시킨 품격 있는 전시관을 둘러보며 한국 과학의 자부심을 느껴보자.
									</li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="col-sm-12 col-md-8 col-lg-8 text-right">
						<!-- select dropdown : 다른관으로 이동 -->
						<c:import url="/WEB-INF/views/display/displayHallSelect.jsp"/>
						<div>
							<img alt="명예의전당" src="<c:url value='/resources/img/display/map/13.png'/>"
								class="img-responsive margin-top20">
							<%-- <c:import url="/WEB-INF/views/display/mainBuilding/svgs/display_svg_map_modified.jsp"></c:import> --%>
						</div>
					</div>
				</div>
			</div>
			<div id="major-exhibitions-spy" class="section scrollspy">
				<div class="row">
					<div class="col-sm-3 col-md-2">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							주요전시물
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="main-exhibition-wrapper">
							<div id="main-exhibitions">
							</div>
							<table id="hall-of-fame-list" class="table table-bordered centered-table table-teal">
								<caption>
									명예의 전당 코너 및 헌정자
								</caption>
								<thead>
									<tr>
										<th>
											시대
										</th>
										<th>
											코너
										</th>
										<th>
											헌정 과학자
										</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="2">
											전통시대
										</td>
										<td>
											조선전기: 전통과학의 체계적 종합과 한국적 응용(14~15C)
										</td>
										<td>
											세종대왕, 최무선, 이순지, 장영실, 이천
										</td>
									</tr>
									<tr>
										<td>
											조선후기: 서양과학의 전래와 전통과학의 새로운 발전(17~18C)
										</td>
										<td>
											허준, 최석정, 홍대용, 서호수, 정약전, 김정호
										</td>
									</tr>
									<tr>
										<td rowspan="4">
											전통시대
										</td>
										<td>
											과학기술선구자: 한국 과학기술의 여명을 열다
										</td>
										<td>
											김점동, 이원철, 윤일선, 조백현
										</td>
									</tr>
									<tr>
										<td>
											과학기술기반구축: 한국 과학기술의 기틀을 닦다
										</td>
										<td>
											안동혁, 김동일, 최형섭, 김재근, 한만춘
										</td>
									</tr>
									<tr>
										<td>
											앎의 지평을 확장시킨 연구: 지식의 지평을 넓히다
										</td>
										<td>
											이태규, 김순경, 이임학, 조순탁, 이휘소
										</td>
									</tr>
									<tr>
										<td>
											한국인의 삶을 변화시킨 연구: 우리 삶을 풍요롭게 하다
										</td>
										<td>
											우장춘, 석주명, 장기려, 현신규, 허문회, 이호왕
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div id="story-spy" class="story-telling-wrapper">
					<div class="header">
						<div class="row">
							<div class="col-md-6">
								<h3 class="page-title teal">
									<div class="top-line"></div>
									스토리로 읽어 보는 명예의전당&amp;노벨상과나 여행기  
								</h3>
							</div>
						</div>
					</div>
					<div class="description">
					</div>
					
					<div id="stories" class="row">
					</div>
				</div>
				<div class="related-photos">
					<div class="title">
						<i class="color-darkgreen material-icons">&#xE413;</i>   관련사진
					</div>
					<div id="image-slider">
						<div id="image-slider-window">
							<img src="<c:url value="/resources/img/display/frontier/hallOfFame/related_photo_01.png"/>" 
								data-url="/resources/img/display/frontier/hallOfFame/related_photo_01.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/frontier/hallOfFame/related_photo_02.png"/>" 
								data-url="/resources/img/display/frontier/hallOfFame/related_photo_02.png" class="slide"/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script id="story-template" type="text/x-handlebars-template">
{{#each stories}}
<div class="col-xs-12 col-sm-6 col-md-6">
	<div class="display-card story-telling story-more-btn pointer" data-idx="{{@index}}" onclick="">
		<div class="card-image">
			<img src="{{thumbnailUrl}}" class="img-responsive"/>
		</div>
		<div class="card-content">
			<div class="display-card-title">
				{{title}}
			</div>
			<div class="display-card-content">
				{{{desc}}}
			</div>
			
		</div>
	</div>							
</div>
{{/each}}
</script>

<script type="text/javascript">
	var stories = [
		{
			title: "한국을 빛낸 과학자들과 함께하는 시간 [명예의 전당]",
			desc: "15세기 전반 우리 과학기술을 세계 최고 수준으로 끌어 올린 세종대왕! 한글, 측우기, "
				+ "해시계 등을 창조하며 타고난 발명가이자 창의력 대왕이었던 세종대왕은 물론 장영실, 허준, "
				+ "이휘소 등 여러 과학자들의 이야기가 이 곳에 담겨 있습니다.",
			thumbnailUrl: "<c:url value="/resources/img/display/frontier/hallOfFame/story05_01.jpg"/>",
			stories: [
				{
					storyLabel: "명예의 전당",
					storyDesc: "15세기 전반 우리 과학기술을 세계 최고 수준으로 끌어 올린 세종대왕! 한글, 측우기, "
						+ "해시계 등을 창조하며 타고난 발명가이자 창의력 대왕이었던 세종대왕은 물론 장영실, 허준, "
						+ "이휘소 등 여러 과학자들의 이야기가 이 곳에 담겨 있습니다.",
					imgUrl: "<c:url value="/resources/img/display/frontier/hallOfFame/story05_01.jpg"/>"
				},
				{
					storyLabel: "명예의 전당",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/frontier/hallOfFame/story05_02.jpg"/>"
				},
				{
					storyLabel: "명예의 전당",
					storyDesc: "저는 생물학자 석주명 선생의 소개가 가장 흥미로웠는데요. 일제 식민지시기, 모든 것이 "
							+ "자유롭지 못했던 시기에 가장 활발한 연구를 선보이며 우리나라 나비 연구의 기틀을 마련했던 " 
							+ "석주명 선생의 연구 내용을 살펴보며 다시 한번 우리 과학자들의 저력을 느낄 수 있었습니다. \n\n"
							+ "여러분도 명예의 전당을 둘러보시며, 나만의 존경하는 한국 과학자를 꼽아 보시는 건 어떨까요?",
					imgUrl: "<c:url value="/resources/img/display/frontier/hallOfFame/story05_03.jpg"/>"
				},
				
			]
		},
		{
			title: "대한민국의 노벨상 과학 부분 수상을 꿈꾸며 [노벨상과 나]",
			desc: "노벨상 만찬장에서는 어떻게 만찬이 벌어지는 지 궁금하셨나요? 이 곳에서 확인해보세요. "
				+ "그 들이 사용하는 접시부터 음식까지, 노벨상 만찬장의 모든 것을 눈으로 확인할 수 있답니다.\n\n"
				+ "평소 그냥 우리가 무심코 사용했던 스마트폰, 자동차 등에 숨어있는 노벨상 수상자들의 공로도 영상과 글을 통해 세세하게 만나볼 수 있습니다.",
			thumbnailUrl: "<c:url value="/resources/img/display/frontier/nobelPrize/story06_01.jpg"/>",
			stories: [
				{
					storyLabel: "노벨상과 나",
					storyDesc: "노벨상 만찬장에서는 어떻게 만찬이 벌어지는 지 궁금하셨나요? 이 곳에서 확인해보세요. "
						+ "그 들이 사용하는 접시부터 음식까지, 노벨상 만찬장의 모든 것을 눈으로 확인할 수 있답니다.\n\n"
						+ "평소 그냥 우리가 무심코 사용했던 스마트폰, 자동차 등에 숨어있는 노벨상 수상자들의 공로도 영상과 글을 통해 세세하게 만나볼 수 있습니다.",
					imgUrl: "<c:url value="/resources/img/display/frontier/nobelPrize/story06_01.jpg"/>"
				},
				{
					storyLabel: "노벨상과 나",
					storyDesc: "인간의 능력을 확장한 노벨 물리학상, 자연을 대처하는 노벨 화학상, 삶과 생명을 향한 "
						+ "시선을 바꾸는 노벨 생리의학상까지! \n\n"
						+ "아직 대한민국에서는 수상자가 없는 과학분야!\n"
						+ "그 최초의 주인공은 누가 될까요? \n\n" 
						+ "그 주인공은, 오늘 국립과천과학관을 찾아 새로운 상상력과 아이디어, 호기심으로 가득 찬 여러분일 것입니다.",
					imgUrl: "<c:url value="/resources/img/display/frontier/nobelPrize/story06_02.jpg"/>"
				}
			]
		}

	];
	
	var storySource   = $("#story-template").html();
	var storyTemplate = Handlebars.compile(storySource);
	var storyHtml = storyTemplate({stories: stories});
	$("#stories").html(storyHtml);
</script>

<c:url var="shareUrl" value="/display/frontier/hallOfFame" />
<c:import url="/WEB-INF/views/display/displayDetailModal.jsp">
	<c:param name="shareUrl" value="${shareUrl }"/>
	<c:param name="exhibitionName" value="명예의전당" />
</c:import>


<script type="text/javascript">

	$(document).ready( function() {
		// 주요 전시물 클릭시 show hide event
		showHideDetails();
	
		// image slider
		PTechSlider('#image-slider');
		
		// SVG MAP SETTING
		// 전시관 Map 1층으로.
		//setDefaultSVGMap('#basic-map-spy',1);
		// 지도 flash 해당 전시관 표시
		//$('#XMLID_3_').attr('class', 'active');
	});
	
	function showHideDetails()  {
		// hide all details first.
		$('.exhibition-detail-wrapper').hide();
		// show the active detail only.
		var exhibit_active_id = $('.major-exhibition-wrapper.active').data('id');
		$(exhibit_active_id).show();
		
		// click event for major exhibition box
		$('.major-exhibition-wrapper').on('click', function() {
			
			// get current active div & remove active
			var hide_id = $('.major-exhibition-wrapper.active').data('id');
			$('.major-exhibition-wrapper.active').removeClass('active');
			// hide the matching detail
			$(hide_id).hide();
			
			// add 'active' to the current clicked div
			var show_id = $(this).data('id');
			$(this).addClass('active');
			// show the mathcing detail
			$(show_id).show();
			
		});
	}
	
</script>




<!-- Major Exhibition Contents Loading -->
<c:import url="/WEB-INF/views/display/majorExhibitionTemplate.jsp" />
<script type="text/javascript">
	var exhibitions = [
		{
			descriptions : [
				
				{
					title: "전통시대 명예로운 과학자, 찬란한 과거를 만나다",
					desc : "우리나라 최초로 화약무기를 개발한 최무선, 조선시대 대표적인 기계기술자 장영실을 비롯하여 " 
						+ "14~15세기 과학기술 강국을 이끌었던 주역들의 찬란했던 과학기술 업적을 만날 수 있다. "
						+ "이어서 허준, 홍대용을 포함해 17~18세기 우리나라 과학기술의 르네상스를 이끌었던 과학자들의 업적과 유물을 살펴볼 수 있다."
				},
				{
					title: "근현대 명예로운 과학자, 아픔을 딛고 일어서다",
					desc : "일제강점기와 6‧25전쟁의 아픔을 이기고, 세계 10대 과학기술 강국으로 발돋움하는데 "
						+ "기여한 근현대의 명예로운 과학자들이 모셔져 있다. 업적에 따라 ‘과학기술 선구자’, "
						+ "‘과학기술 기반 구축’, ‘앎의 지평을 확장시킨 연구’, ‘한국인의 삶을 변화시킨 연구’로 "
						+ "스토리를 따라 전시를 볼 수 있다."
				}
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/frontier/hallOfFame/major_exhibition_01.png' />",
					caption: "[전통시대 명예로운 과학자]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/frontier/hallOfFame/major_exhibition_02.png' />",
					caption: "[근현대 명예로운 과학자]"
				},
			]
		},
		{
			descriptions : [
				{
					title : "디지털 아카이브, 한 눈에 되돌아보다",
					desc : "‘우리나라의 위대한 과학의 역사’는 아카이브 테이블을 통해 14세기부터 현재까지 역사 속 명예로운 과학자들의 위치와 업적을 연대적으로 살펴볼 수 있다."
				},
				{
					title : "나도 명예로운 과학자, 과학자의 꿈을 키우다",
					desc : "관람객들이 명예로운 과학자의 꿈을 키울 수 있도록 자신이 성취하고 싶은 과학적 업적과 자신의 얼굴 부조를 전시할 수 있는 포토존이다."
				},
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/frontier/hallOfFame/major_exhibition_03.png' />",
					caption: "[나도 명예로운 과학자]"
				}
			]
		}
	];
	
	var exhibitionSource   = $("#major-exhibition-template").html();
	var exhibitionTemplate = Handlebars.compile(exhibitionSource);
	var exhibitionHtml = exhibitionTemplate({exhibitions: exhibitions});
	$("#main-exhibitions").html(exhibitionHtml);
	
</script>