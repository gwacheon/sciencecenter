<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-teal">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>
	<div id="display-body" class="sub-body">
		<div class="narrow-sub-top display">

			<div class="sub-banner-tab-wrapper sub-banner-tab-teal">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<ul class="nav nav-tabs">
								<li class="col-xs-12 col-sm-6 col-md-4">
									<a href="<c:url value="/display/frontier/kidsMakerStudio" />" class="item">
										키즈메이커스튜디오
									</a>
								</li>
								<li class="col-xs-12 col-sm-6 col-md-4 active">
									<a href="#" class="item">
										메이커프리마켓 
									</a>
								</li>
								<%-- <li class="col-xs-12 col-sm-6 col-md-3">
									<a href="<c:url value="/display/frontier/familySLand" />" class="item">
										패밀리창작놀이터
									</a>
								</li> --%>
								<li class="col-xs-12 col-sm-6 col-md-4">
									<a href="<c:url value="/display/frontier/infiniteImagination" />" class="item">
										무한상상실
									</a>
								</li>
							</ul>   
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div id="kids-studio-spy" class="scrollspy section margin-bottom20">
				<div class="row">
					<div class="col-xs-6 col-md-3">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							메이커프리마켓
						</h3>
					</div>
				</div>
				<div class="row description">
					<div class="col-sm-12 col-md-6">
						<ul class="circle-list teal">
							<li>
								기본 안내 
							</li>
						</ul>
						<div class="desc-text">
							<div class="row">
								<div class="col-xs-3 col-md-2">
									<div class="label-header">
										위치
									</div>
								</div>
								<div class="col-xs-9 col-md-10">
									<div class="label-value">
										과학관 입장 후 프론티어 창작관 내 메이커랜드
									</div>
								</div>
								<div class="col-xs-3 col-md-2">
									<div class="label-header">
										입점기간
									</div>
								</div>
								<div class="col-xs-9 col-md-10">
									<div class="label-value">
										입점 1주일 전 수요일 ~ 개최 주 수요일 오후 3시까지
									</div>
								</div>
								<div class="col-xs-3 col-md-2">
									<div class="label-header">
										신청방법
									</div>
								</div>
								<div class="col-xs-9 col-md-10">
									<div class="label-value">
										최초 입점희망자 등록(e-mail) 후 과천과학관 홈페이지 간이신청
									</div>
								</div>
								<div class="col-xs-3 col-md-2">
									<div class="label-header">
										결과발표 
									</div>
								</div>
								<div class="col-xs-9 col-md-10">
									<div class="label-value">
										개최 주 목요일 15시 홈페이지를 통해 선발결과 공지
									</div>
								</div>
								<div class="col-xs-3 col-md-2">
									<div class="label-header">
										운영시간  
									</div>
								</div>
								<div class="col-xs-9 col-md-10">
									<div class="label-value">
										토요일 ~ 일요일, 09:30 ~ 17:30 (매주 월요일 휴관) 
									</div>
								</div>
								<div class="col-xs-3 col-md-2">
									<div class="label-header">
										이용방법   
									</div>
								</div>
								<div class="col-xs-9 col-md-10">
									<div class="label-value">
										(창작자) 과학관 홈페이지 예약 – 메이커 프리마켓 입점<br>
					                     (관람자) 과천과학관 출입 후 메이커 프리마켓 방문
 
									</div>
								</div>
								<div class="col-xs-3 col-md-2">
									<div class="label-header">
										유의사항   
									</div>
								</div>
								<div class="col-xs-9 col-md-10">
									<div class="label-value">
 										등록 가능 : 스스로 창작한 작품, 차별화된 작품, 명확한 스토리텔링이 있는 작품<br>
								   		등록 불가 : 다른 사람이 만들었거나 반제품 비율이 50%이상일 경우, 환경을 고려하지 않은 작품이나 작업방식, 만드는 방법이 일반적인 기성품의 경우(단순비누 등), 식음료를 포함한 모든 음식물 등
									</div>
								</div>
								<div class="col-xs-3 col-md-2">
									<div class="label-header">
										문의   
									</div>
								</div>
								<div class="col-xs-9 col-md-10">
									<div class="label-value">
										   전화 : 02)509-6925(월요일 휴관)<br>
										   e-mail : moohsangsang@gmail.com

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>