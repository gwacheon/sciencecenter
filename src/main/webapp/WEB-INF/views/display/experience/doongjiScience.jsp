<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-teal">
		<nav id="scrollspy-nav" class="navbar-default navbar-static has-scroll">
			<div class="container">
				<ul class="nav navbar-nav">
					<c:import url="/WEB-INF/views/display/navbar.jsp"/>
					<li>
						<a href="#basic-info-spy" class="item">
							둥지과학놀이 
						</a>
					</li>
					<li>
						<a href="#reservation-guide-spy" class="item">
							예약 안내
						</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<div class="bread-crumbs">
							전시관람 / 전시체험예약 
						</div>
					</li>
				</ul>
			</div>
		</nav>
	</div>
	<div id="display-body" class="sub-body">
		<div id="basic-info-spy" class="scrollspy">
			<div class="narrow-sub-top doongji-science">
				<div class="container">
					<h4 class="doc-title">둥지과학놀이</h4>
					<h6 class="doc-title exp-desc">
						과학놀이터인 전시장에서 생명공학 실험장비 및 로봇 전시물 등 각종 전시물과 연계한 체험 활동을 통해<br>
						과학에 대한 흥미를 높이고 미래의 과학기술자의 꿈을 키울 수 있는 프로그램입니다.
					</h6>
					<div class="banner-link">
						<a href="javascript:void(0)" onclick="ChangeClass('CL6001')" class="btn btn-teal btn-wide">
							예약하러 가기
						</a>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-xs-4 col-md-3">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							둥지과학놀이 프로그램
						</h3>
					</div>
				</div>
				<div class="row description">
					<div class="col-md-12">
						<ul class="circle-list teal">
							<li>
								프로그램 소개
								<ul>
									<li>
										국립과천과학관의 '둥지과학놀이'는 주말에 이용할 수 있는 과학체험프로그램으로
										과학놀이터인 전시장에서 생명공학 실험장비 및 로봇 전시물 등 각종 전시물과 연계한 체험 활동을 통해
										과학에 대한 흥미를 높이고 미래의 과학기술자의 꿈을 키울 수 있는 프로그램 입니다.
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
		<div id="reservation-guide-spy">
			<div class="container">
				<div class="row">
					<div class="col-xs-4 col-md-2">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							예약 안내
						</h3>
					</div>
				</div>
				<div class="row description">
					<div class="col-md-12">
						<ul class="circle-list teal">
							<li>
								예약 방법
								<ul>
									<li>
										<b>해당 프로그램을 예약하려면 카테고리 <span class="color-darkgreen">예약/신청</span> 페이지에서 예약이 가능합니다.</b>
										<a href="javascript:void(0)" onclick="ChangeClass('CL6001')" class="btn btn-teal right">
											해당 예약화면으로 바로가기
										</a>	
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>

<form name="frmIndex">
	<input type="hidden" name="ACADEMY_CD" value="ACD006" />
	<input type="hidden" name="COURSE_CD" value="" />
	<input type="hidden" name="SEMESTER_CD" value="" />
	<input type="hidden" name="TYPE" value="" />
	<input type="hidden" name="FLAG" value="" />
	<input type="hidden" name="CLASS_CD" value="CL7002" />
</form>

<script type="text/javascript">
	function ChangeClass(Cd){
		var frm = document.frmIndex;
		frm.CLASS_CD.value = Cd;
		frm.action = '<c:url value="/schedules"/>?${_csrf.parameterName}=${_csrf.token}';
		frm.method = 'post';
		frm.submit();
	}
</script>