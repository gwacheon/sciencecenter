<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-teal">
		<nav id="scrollspy-nav" class="navbar-default navbar-static has-scroll">
			<div class="container">
				<ul class="nav navbar-nav">
					<c:import url="/WEB-INF/views/display/navbar.jsp"/>
					<li>
						<a href="#basic-info-spy" class="item">
							무한상상실프로그램
						</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<div class="bread-crumbs">
							전시관람 / 무한상상실프로그램
						</div>
					</li>
				</ul>
			</div>
		</nav>
	</div>
	<div id="display-body" class="sub-body">
		<div class="container">
			<div id="basic-map-spy" class="section scrollspy">
				<div class="row">
					<div class="col-sm-3 col-md-2 col-lg-2">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							무한상상실프로그램
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<c:import url="/WEB-INF/views/underConstruction/underConstruction.jsp"/>
					</div>
				</div>
			</div>			
		</div>
	</div>
</div>
