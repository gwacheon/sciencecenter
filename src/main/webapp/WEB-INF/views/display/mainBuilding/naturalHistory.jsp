<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-teal">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
		<%-- <nav id="scrollspy-nav" class="navbar-default navbar-static has-scroll">
			<div class="container">
				<ul class="nav navbar-nav">
					<c:import url="/WEB-INF/views/display/navbar.jsp"/>
					<li>
						<a href="#basic-info-spy" class="item">
							기본 안내 
						</a>
					</li>
					<li>
						<a href="#basic-map-spy" class="item">
							자연사관 안내
						</a>
					</li>
					<li>
						<a href="#major-exhibitions-spy" class="item">
							주요전시물
						</a>
					</li>
					<li>
						<a href="#story-spy" class="item">
							스토리 여행기
						</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<div class="bread-crumbs">
							전시관 / 자연사관
						</div>
					</li>					
				</ul>
			</div>
		</nav> --%>
	</div>
	<div id="display-body" class="sub-body">
		<div class="narrow-sub-top display">
			<div class="sub-banner-tab-wrapper sub-banner-tab-teal">
				<!-- <div class="container">
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
							<ul class="nav nav-tabs">
								<li class="col-xs-12 active">
									<a href="http://cyber.scientorium.go.kr/museum_2015/vrLoad.jsp?pavilion=indoor&xml_name=nat_001.xml&angle=-90" class="item">
										<i class="fa fa-desktop fa-lg" aria-hidden="true"></i> 사이버 전시관
									</a>  
								</li>
							</ul>   
						</div>
					</div>
				</div> -->
			</div>			
		</div>
		<div class="container">
			<div id="basic-map-spy" class="section scrollspy">
				<div class="row">
					<div class="col-sm-3 col-md-2 col-lg-2">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							자연사관 안내					
						</h3>
					</div>
				</div>
				<div class="row description">
					<div class="col-sm-12 col-md-4 col-lg-4">
						<ul class="circle-list teal">
							<li>
								주요전시물
								<ul>
									<li>
										<b>「자연사관(Natural History)」</b>은 138억 년 전 우주 생성부터 시작해 
										46억 년 전 지구 탄생에 이어 현재의 인간과 자연의 모습을 이루기까지의 모든 과정을 다루는 공간이다. 
										‘자연사’가 다소 생소한 용어이지만, 현재 지구에 살고 있는 모든 것들의 근간을 이해하는 데에 매우 
										중요한 개념이다. 이곳에는 총 115건의 전시물이 있으며 이 중 체험물은 23건이다. 
										특히 ‘생동하는 지구 SOS’는 사람들의 시선을 단번에 사로잡는 자연사관의 대표적인 체험물이다.
									</li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="col-sm-12 col-md-8 col-lg-8 text-right">
						<c:import url="/WEB-INF/views/display/displayHallSelect.jsp"/>
						<div>
							<img alt="자연사관" src="<c:url value='/resources/img/display/map/07.png'/>"
								class="img-responsive margin-top20">
						</div>
					</div>
				</div>
			</div>
			<div id="major-exhibitions-spy" class="section scrollspy">
				<div class="row">
					<div class="col-sm-3 col-md-2 col-lg-2">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							주요전시물						
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="main-exhibition-wrapper">
							<div id="main-exhibitions">
							</div>
						</div>
					</div>
				</div>

				<div id="story-spy" class="story-telling-wrapper">
					<div class="header">
						<div class="row">
							<div class="col-md-6">
								<h3 class="page-title teal">
									<div class="top-line"></div>
									스토리로 읽어 보는 자연사관 여행기  
								</h3>
							</div>
						</div>
					</div>
					<div class="description">
						“공룡 보러 왔어요.”<br>
						“이 곳에서 할머니의 할머니, 할머니의 할머니보다도 더 오래 전 사람들을 볼 수 있다고 하던데요!”<br>
						“지구의 역사를 볼 수 있다고 해서 기대하고 있습니다.”
					</div>
					
					<div id="stories" class="row">
					</div>
				</div>
				<div class="related-photos">
					<div class="title">
						<i class="color-darkgreen material-icons">&#xE413;</i>   관련사진
					</div>
					<div id="image-slider">
						<div id="image-slider-window">
							<img src="<c:url value="/resources/img/display/mainBuilding/naturalHistory/related_photo_01.png"/>" 
								data-url="/resources/img/display/mainBuilding/naturalHistory/related_photo_01.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/naturalHistory/related_photo_02.png"/>" 
								data-url="/resources/img/display/mainBuilding/naturalHistory/related_photo_02.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/naturalHistory/related_photo_03.png"/>" 
								data-url="/resources/img/display/mainBuilding/naturalHistory/related_photo_03.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/naturalHistory/related_photo_04.png"/>" 
								data-url="/resources/img/display/mainBuilding/naturalHistory/related_photo_04.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/naturalHistory/related_photo_05.png"/>" 
								data-url="/resources/img/display/mainBuilding/naturalHistory/related_photo_05.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/naturalHistory/related_photo_06.png"/>" 
								data-url="/resources/img/display/mainBuilding/naturalHistory/related_photo_06.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/naturalHistory/related_photo_07.png"/>" 
								data-url="/resources/img/display/mainBuilding/naturalHistory/related_photo_07.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/naturalHistory/related_photo_08.png"/>" 
								data-url="/resources/img/display/mainBuilding/naturalHistory/related_photo_08.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/naturalHistory/related_photo_09.png"/>" 
								data-url="/resources/img/display/mainBuilding/naturalHistory/related_photo_09.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/naturalHistory/related_photo_10.png"/>" 
								data-url="/resources/img/display/mainBuilding/naturalHistory/related_photo_10.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/naturalHistory/related_photo_11.png"/>" 
								data-url="/resources/img/display/mainBuilding/naturalHistory/related_photo_11.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/naturalHistory/related_photo_12.png"/>" 
								data-url="/resources/img/display/mainBuilding/naturalHistory/related_photo_12.png" class="slide"/>
						</div> 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script id="story-template" type="text/x-handlebars-template">
{{#each stories}}
<div class="col-xs-12 col-sm-6 col-md-6">
	<div class="display-card story-telling story-more-btn pointer" data-idx="{{@index}}" onclick="">
		<div class="card-image">
			<img src="{{thumbnailUrl}}" class="img-responsive"/>
		</div>
		<div class="card-content">
			<div class="display-card-title">
				{{title}}
			</div>
			<div class="display-card-content">
				{{{desc}}}
			</div>
			
		</div>
	</div>							
</div>
{{/each}}
</script>

<script type="text/javascript">
	var stories = [
		{
			title: "지구의 역사를 경험하다 [탄생의 장] & [변화의 장]",
			desc: "여행을 떠나기 전 만나본 관람객들의 이야기를 들어보니 이 곳이 어디인지 아시겠나요? \n"
				+ "맞습니다! 이번 여행지는 자연사관입니다. 여러분은 지금부터 저와 함께 공룡부터 동식물들의 역사는 "
				+ "물론 인류의 기원까지 국립과천과학관이 자랑하는 대규모의 자연사 전시물들을 만나보게 될 것입니다. 자! 그럼 다 함께 자연과 생물의 역사를 찾아가 볼까요?",
			thumbnailUrl: "<c:url value="/resources/img/display/mainBuilding/naturalHistory/story09_01.jpg"/>",
			stories: [
				{
					storyLabel: "자연사관",
					storyDesc: "여행을 떠나기 전 만나본 관람객들의 이야기를 들어보니 이 곳이 어디인지 아시겠나요? \n"
						+ "맞습니다! 이번 여행지는 자연사관입니다. 여러분은 지금부터 저와 함께 공룡부터 동식물들의 역사는 "
						+ "물론 인류의 기원까지 국립과천과학관이 자랑하는 대규모의 자연사 전시물들을 만나보게 될 것입니다. 자! 그럼 다 함께 자연과 생물의 역사를 찾아가 볼까요?",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/naturalHistory/story09_01.jpg"/>"
				},
				{
					storyLabel: "종려나무잎의 화석",
					storyDesc: "자연사관의 입구에는 성인 키보다도 훨씬 큰 잎사귀 화석이 보입니다. "
							+ "듣기만 해도 너무 큰 숫자라서 어느 정도 과거인지 쉽게 감조차 오지 않는 5000만 년 전! "
							+ "그 시기에 살았던 종려나무잎의 화석입니다. 세계 최대 크기의 화석의 표본을 보고 있으니, "
							+ "‘이 잎사귀가 달려있던 나무의 전체 크기는 얼마나 컸을까?’하는 생각이 드네요.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/naturalHistory/story09_02.jpg"/>"
				},
				{
					storyLabel: "지구와 별의 탄생기",
					storyDesc: "지구 생물의 이야기를 시작하기 전 관람객들은 지구와 별의 탄생기를 만날 수 있습니다.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/naturalHistory/story09_03.jpg"/>"
				},
				{
					storyLabel: "생동하는 지구, SOS",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/naturalHistory/story09_04_01.jpg"/>"
				},
				{
					storyLabel: "생동하는 지구, SOS",
					storyDesc: "특히 생동하는 지구, SOS는 놓치지 말아야 할 전시물입니다. "
							+ "단독 전시관안으로 들어가면 커다란 지구 모형이 하나 보이는데요. "
							+ "모형 위로 무엇인가 끊임없이 움직이고 있습니다. 이 것은 지구환경 변화 관측 시스템 일명 SOS라고 불리는 것인데요. "
							+ "미국 해양 대기청에서 들여온 것으로 인공위성에서 관측한 환경자료를 축소하여 지구 모형에서 직접 볼 수 있도록 해 놓은 전시입니다. "
							+ "이 전시를 보고 있으니 마치 커다란 거인이 되어 우주에서 지구를 내려다보는 듯한 기분을 느낄 수 있습니다. "
							+ "또, 우리는 의식하지 못하는 거대한 우주의 흐름을 통해 경이로움도 느껴지네요.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/naturalHistory/story09_04_02.jpg"/>"
				},
				{
					storyLabel: "지질여행",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/naturalHistory/story09_05_01.jpg"/>"
				},
				{
					storyLabel: "지질여행",
					storyDesc: "지구 이야기가 끝나면 한반도 이야기가 여러분을 기다리고 있습니다. "
							+ "한반도의 생성과 진화의 모습 그리고 한반도의 지질 이야기를 다양한 체험과 전시로 만나 볼 수 있습니다. "
							+ "두 대의 자동차가 보이시죠? 이 것은 자동차를 타고 한반도 지질 여행을 떠나는 것인데, "
							+ "학생들에게 무척 인기가 많습니다. 신나게 한반도를 달리며 우리나라 구석구석 지질의 차이점을 꼭 느껴보시기 바랍니다.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/naturalHistory/story09_05_02.jpg"/>"
				}
			]
		},
		{
			title: "공룡이 살아나는 시간 [진화의 장]",
			desc: "전시관을 가득 채운 공룡 모형들, 마치 공룡이 살던 시대로 돌아가 그 모습들을 엿보고 있는 듯한 "
				+ "기분에 빠져들게 됩니다. 가장 눈에 띄는 공룡 화석은 ‘에드몬토사우르스’의 화석입니다. 원형 보존율 "
				+ "90% 이상인 이 화석을 보고 있으면 정말 살아 있는 공룡을 보고 있는 것 같습니다. 자세히 보니 이빨 자국이 보이는데요. "
				+ "이 화석은 척추뼈에 티라노사우르스에게 물린 이빨 자국이 아직도 남아 있다고 합니다. 눈을 크게 뜨고 이빨 자국을 한번 찾아보세요. "
				+ "당시 긴박했던 두 공룡간 싸움의 현장이 눈 앞에 그려질 테니까요.",
			thumbnailUrl: "<c:url value="/resources/img/display/mainBuilding/naturalHistory/story09_06.jpg"/>",
			headerDesc:"학생들의 발걸음이 빨라집니다. 아빠의 걸음을 재촉하는 아이들의 모습도 자주 눈에 띄고, "
				+ "아이보다 더 즐거워 보이는 선생님들도 보입니다. 이렇게 전시관 내 사람들의 얼굴이 흥분되어 "
				+ "보이는 이유는 이제, 공룡을 만날 수 있기 때문입니다. 먼저, 어류부터 파충류까지 다양한 생물들의 "
				+ "화석을 보고 나면 여러분은 공룡이 사는 먼 과거로 떠나게 됩니다.",
			stories: [
				{
					storyLabel: "에드몬토사우르스",
					storyDesc: "전시관을 가득 채운 공룡 모형들, 마치 공룡이 살던 시대로 돌아가 그 모습들을 엿보고 있는 듯한 "
						+ "기분에 빠져들게 됩니다. 가장 눈에 띄는 공룡 화석은 ‘에드몬토사우르스’의 화석입니다. 원형 보존율 "
						+ "90% 이상인 이 화석을 보고 있으면 정말 살아 있는 공룡을 보고 있는 것 같습니다. 자세히 보니 이빨 자국이 보이는데요. "
						+ "이 화석은 척추뼈에 티라노사우르스에게 물린 이빨 자국이 아직도 남아 있다고 합니다. 눈을 크게 뜨고 이빨 자국을 한번 찾아보세요. "
						+ "당시 긴박했던 두 공룡간 싸움의 현장이 눈 앞에 그려질 테니까요.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/naturalHistory/story09_06.jpg"/>"
				},
				{
					storyLabel: "천장 공룡 화석",
					storyDesc: "여기서 잠깐! 자연사관, 진화의 장을 더욱 흥미롭게 즐길 수 있는 방법을 하나 알려드리겠습니다. "
							+ "자연사관의 진화의 장 코너에서는 앞만 보고 이동하지 마세요. 천장에도 익룡의 화석이 " 
							+ "달려 있습니다. 마치 하늘에서 아직도 날개짓을 하고 있는 것 같은데요. 여러분의 머리 위로 "
							+ "보이는 익룡은 프테라노돈 입니다. 이 밖에도 대형 포식자 틸로사우르스, "
							+ "대형 수장룡 엘리스모사우르스 등 수 많은 공룡 화석들이 여러분의 호기심과 동심을 자극합니다.\n\n"
							+ "우리나라에도 공룡이 있었던 사실 알고 계시죠? 보성에서 발견되었던 공룡 골격화석이 이 곳에 전시되어 있습니다. "
							+ "이 화석을 보면서 내가 살고 있는 도시에 공룡이 살았던 모습을 한 번 상상해보세요.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/naturalHistory/story09_07.jpg"/>"
				},
				{
					storyLabel: "인류의 기원",
					storyDesc: "공룡과 함께 했던 흥미로운 여행이 끝나면 인류의 기원이 여러분을 기다리고 있습니다. "
							+ "이름만 들어보았던 고인류의 키와 생김새를 눈으로 확인할 수 있고, 인류 진화의 모습을 한 눈에 " 
							+ "볼 수 있습니다. 두뇌와 척추, 그들이 사용했던 도구까지 지금 인류와의 다른 점도 꼼꼼하게 찾아보시고요.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/naturalHistory/story09_08.jpg"/>"
				},
				{
					storyLabel: "인류",
					storyDesc: "그럼 제가 질문을 하나 던져보겠습니다. 가장 미성숙한 모습으로 태어나는 동물은 무엇일까요?\n" 
							+ "바로, 인류입니다. 만물의 영장이라고 하는 인류가 가장 미성숙한 모습으로 태어나는 이유를 " 
							+ "이 곳 자연사관에서 찾을 수 있습니다. 인간은 좁은 산도를 통과해야 하는 반면, 큰 두뇌를 가지고 있습니다. "
							+ "이러한 이유로 눈도 못 뜨고, 목도 못 가누는 미성숙한 모습으로 탄생하게 되는 것이죠.\n"   
							+ "작은 생물은 물론 공룡부터 인류까지 진화의 역사를 한 눈에 볼 수 있는 자연사관은 한 동안 국립과천과학관을 대표하는 명물로 기억될 것 같습니다.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/naturalHistory/story09_09.jpg"/>"
				}
				
				
			]
		}
		
	];
	
	var storySource   = $("#story-template").html();
	var storyTemplate = Handlebars.compile(storySource);
	var storyHtml = storyTemplate({stories: stories});
	$("#stories").html(storyHtml);
</script>

<c:url var="shareUrl" value="/display/mainBuilding/naturalHistory" />
<c:import url="/WEB-INF/views/display/displayDetailModal.jsp">
	<c:param name="shareUrl" value="${shareUrl }"/>
	<c:param name="exhibitionName" value="자연사관" />
</c:import>



<script type="text/javascript">

	$(document).ready( function() {
		// 주요 전시물 클릭시 show hide event
		//showHideDetails();
		
		// image slider
		PTechSlider('#image-slider').autoSlide();
		
		// 전시관 지도 flash 2층으로 전환.
		//setDefaultSVGMap('#basic-map-spy',2);
		// 지도 flash 해당 전시관 표시
		//$('#XMLID_156_').attr('class', 'active');
	});
	
	function showHideDetails()  {
		// hide all details first.
		$('.exhibition-detail-wrapper').hide();
		// show the active detail only.
		var exhibit_active_id = $('.major-exhibition-wrapper.active').data('id');
		$(exhibit_active_id).show();
		
		// click event for major exhibition box
		$('.major-exhibition-wrapper').on('click', function() {
			
			// get current active div & remove active
			var hide_id = $('.major-exhibition-wrapper.active').data('id');
			$('.major-exhibition-wrapper.active').removeClass('active');
			// hide the matching detail
			$(hide_id).hide();
			
			// add 'active' to the current clicked div
			var show_id = $(this).data('id');
			$(this).addClass('active');
			// show the mathcing detail
			$(show_id).show();
			
		});
	}
	
</script>

<!-- Major Exhibition Contents Loading -->
<c:import url="/WEB-INF/views/display/majorExhibitionTemplate.jsp" />
<script type="text/javascript">
	var exhibitions = [
		{
			descriptions : [
				{
					title : "입구에서 관람객을 맞이하는 종려나뭇잎 화석",
					desc : "5천만 년 전에 형성된 미국 그린리버 지층에서 발견된 것으로, "
						+ "세계 최대 크기(가로 2.4m, 세로 4.3m)를 자랑한다. 당시의 종려나뭇잎 화석이 "
						+ "대개 2m이내인 것에 반해 이 화석은 나뭇잎 하나의 길이가 무려 4m에 이른다. 현재 "
						+ "종려나뭇잎의 크기와 나무 높이 비율이 약 5배 이상인 것으로 미루어 보았을 때, 나무 높이는 "
						+ "20m보다 컸을 것으로 추정된다. "
						+ "4종 10마리의 물고기 화석도 함께 발견되어 화석이 만들어지는 과정과 당시의 환경을 가늠하게 해 준다."
				},
				{
					title: "탄생의 장, 운석을 통해 지구의 기원을 느끼다",
					desc : "아무도 경험해 보지 못한 지구의 기원을 어떻게 알아낼까? 지구와 동시에 태어나 당시의 흔적을 "
						+ "그대로 가지고 있는 운석에 해답이 있다. 무려 44kg에 달하는 철질 운석을 직접 만져보며 우주를 느껴보자. " 
				}
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/mainBuilding/naturalHistory/major_exhibition_01.png' />",
					caption: "[세계 최대의 종려나뭇잎 화석]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/mainBuilding/naturalHistory/major_exhibition_02.png' />",
					caption: "[운석]"
				}
			]
		},
		{
			descriptions : [
				{
					title : "진화의 장, 화석으로 지구의 과거를 파헤치다",
					desc : "선캄브리아대, 고생대, 중생대, 신생대의 대표 화석을 전시하며 지질시대의 생물 및 환경 변화를 "
						+ "설명한다. 특히 공룡 코너로 접어들면, ‘에드몬토사우루스’가 위용을 자랑한다. "
						+ "대개 공룡화석이 20~30%의 진품 비율을 보유하는 데 반해, 90%이상이 진품으로 "
						+ "이루어져 매우 가치가 높다. 또한 꼬리 쪽에는 육식공룡에게 물린 자국도 있어 공룡 생태 연구에 학술적으로도 가치가 크다."
				},
				{
					title : "",
					desc : "또한 인간이란 무엇인가에 대한 질문으로 시작하여 인류의 기원과 진화 과정을 살펴보는 "
						+ "체험형 전시코너 ‘인류의 진화’가 있다. 인간이 현재 왜 이러한 모습을 가지게 되었는지에 대해 탐구해 볼 수 있다."
				},
				{
					title: "생동의 장, 생생한 지구의 모습을 감상한다",
					desc : "한 번 체험하고 나면 그 매력에 흠뻑 빠지는 과학관의 대표 체험물, "
						+ "‘생동하는 지구, SOS’. 해설요원들의 알차고 맛깔 나는 설명과 함께 지구의 다양한 모습을 "
						+ "3D 구형 스크린을 통해 감상할 수 있다. 미국 해양대기청(NOAA)의 인공위성으로부터 "
						+ "매일 전송받는 500여 개의 영상콘텐츠를 보유하고 있으며, 주요 영상들을 하나의 스토리로 "
						+ "구성하여 해설을 제공한다. 실시간 구름 이동, 세계 지진 발생 현황, 밤에 본 지구, "
						+ "비행기 이동 경로, 6억 년 간 대륙 이동 모습은 꾸준한 인기를 얻는 영상들이다." 
						+ "<div class='additional-info'>(40명/회, 초등4학년 이상)</div>" 
				}
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/mainBuilding/naturalHistory/major_exhibition_03.png' />",
					caption: "[에드몬토사우루스]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/mainBuilding/naturalHistory/major_exhibition_04.png' />",
					caption: "[생동하는 지구 SOS]"
				}
			]
		},
		{
			descriptions : [
				{
					title : "생명의 장, 살아있는 생물들을 직접 만나다",
					desc : "‘라이브진화센터’에는 상어, 문어, 농게를 비롯하여 살아있는 생물들이 가득하다. "
						+ "자연선택과 진화의 원리를 생물 관찰을 통해 터득하게끔 구성하였다. 또한 수족관에는 "
						+ "하천과 바다에 사는 다양한 물고기들이 화려함을 자랑하며 관람객의 시선을 끈다."
				},
				{
					title: "",
					desc : "일반 관람객은 평소 접근할 수 없는 수족관 관리실을 공개하고, "
						+ "전시장에서 볼 수 없는 신기한 물고기를 만나볼 수 있는 기회가 있다. "
						+ "‘수족관 뒤에는 무슨 일이?’에는 주말과 공휴일에만 운영되는 특별 체험 프로그램이다. "
						+ "전시관에서 근무하는 수족관 관리사가 직접 생생한 이야기를 들려준다."
						+ "<div class='additional-info'>(15명/회, 초등 1학년 이상)</div>" 
				}
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/mainBuilding/naturalHistory/major_exhibition_05.png' />",
					caption: "[문어]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/mainBuilding/naturalHistory/major_exhibition_06.png' />",
					caption: "[수족관 뒤에는 무슨일이?]"
				}
			]
		}
	];
	
	var exhibitionSource   = $("#major-exhibition-template").html();
	var exhibitionTemplate = Handlebars.compile(exhibitionSource);
	var exhibitionHtml = exhibitionTemplate({exhibitions: exhibitions});
	$("#main-exhibitions").html(exhibitionHtml);
	
</script>