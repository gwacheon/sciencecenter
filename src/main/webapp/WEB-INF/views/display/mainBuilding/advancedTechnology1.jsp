<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-teal">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
		<%-- <nav id="scrollspy-nav" class="navbar-default navbar-static has-scroll">
			<div class="container">
				<ul class="nav navbar-nav">
					<c:import url="/WEB-INF/views/display/navbar.jsp"/>
					<li>
						<a href="#basic-info-spy" class="item">
							기본 안내 
						</a> 
					</li>
					<li>
						<a href="#basic-map-spy" class="item">
							첨단기술관1 안내
						</a>
					</li>
					<li>
						<a href="#major-exhibitions-spy" class="item">
							주요전시물
						</a>
					</li>
					<li>
						<a href="#story-spy" class="item">
							스토리 여행기
						</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<div class="bread-crumbs">
							전시관 / 첨단기술관1
						</div>						
					</li>
				</ul>
				
			</div>
		</nav> --%>
	</div>
	<div id="display-body" class="sub-body">
		<div class="narrow-sub-top display">
			<div class="sub-banner-tab-wrapper sub-banner-tab-teal">
				<!-- <div class="container">
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
							<ul class="nav nav-tabs">
								<li class="col-xs-12 active">
									<a href="http://cyber.scientorium.go.kr/museum_2015/vrLoad.jsp?pavilion=indoor&xml_name=ce1_002.xml&angle=-90" class="item">
										<i class="fa fa-desktop fa-lg" aria-hidden="true"></i> 사이버 전시관
									</a>		
								</li>
							</ul>   
						</div>
					</div>
				</div> -->
			</div>			
		</div>
		<div class="container">
			<div id="basic-map-spy" class="section scrollspy">
				<div class="row">
					<div class="col-md-12">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							첨단기술관1 안내						
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-md-4 col-lg-4">
						<ul class="circle-list teal">
							<li>
								주요전시물
								<ul>
									<li>
										<b>「첨단기술1관」</b>은 인류 미래를 열어 가는데 중요한 최신 과학기술을 전시하는
										 공간이다. 이 곳에는 로봇, 생명공학, 정보통신, 에너지·환경 
										 분야의 과학기술을 보여주는 총 125건의 전시물이 있다. 
										 이 중 체험물은 54건으로 ‘뇌파는 마술사’, ‘알파경주’, ‘마인드 레이싱카’등이 대표적이다. 
										 이곳에서 평소 접하기 어려운 첨단 기술을 체험하며 우리의 미래를 상상해보자.
									</li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="col-sm-12 col-md-8 col-lg-8 text-right">
						<!-- select dropdown : 다른관으로 이동 -->
						<c:import url="/WEB-INF/views/display/displayHallSelect.jsp"/>
						<div>
							<img alt="첨단기술관1" src="<c:url value='/resources/img/display/map/02.png'/>"
								class="img-responsive margin-top20">
						</div>
					</div>	
				</div>
			</div>
			<div id="major-exhibitions-spy" class="section scrollspy">
				<div class="row">
					<div class="col-sm-3 col-md-2 col-lg-2">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							주요전시물
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="main-exhibition-wrapper">
							<div id="main-exhibitions">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="story-spy" class="story-telling-wrapper">
				<div class="header">
					<div class="row">
						<div class="col-md-6">
							<h3 class="page-title teal">
								<div class="top-line"></div>
								스토리로 읽어 보는 첨단기술관1 여행기
							</h3>
						</div>
					</div>
				</div>
				<div class="description">
					우리는 내일의 기술이 오늘의 기술이 되는 속도가 점점 짧아지고 있는 세상에 살고 있습니다.
					몇 년 전 SF 영화에서 봤던 기술들이 이미 오늘날 상용화 되어있기도 하죠. 오늘 첨단기술관
					1 에서의 경험들이 여러분이 그려갈 미래의 모습을 더 빛나게 하는데 큰 도움이 될 것이라고
					확신합니다. 그럼, 우리의 다같이 첨단기술관 1로 여행을 떠나볼까요?
				</div>
				
				<div id="stories" class="row">
				</div>
			</div>
			<div class="related-photos">
				<div class="title">
					<i class="color-darkgreen material-icons">&#xE413;</i>   관련사진
				</div>
				<div id="image-slider">
					<div id="image-slider-window">
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_01.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_01.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_02.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_02.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_03.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_03.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_04.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_04.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_05.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_05.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_06.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_06.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_07.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_07.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_08.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_08.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_09.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_09.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_10.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_10.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_11.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_11.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_12.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_12.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_13.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_13.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_14.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_14.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_15.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_15.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_16.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_16.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_17.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_17.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_18.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_18.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_19.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_19.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_20.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_20.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_21.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_21.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_22.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_22.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_23.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_23.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_24.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_24.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_26.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_26.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_27.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_27.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_28.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_28.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_29.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_29.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_30.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_30.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_31.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_31.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_32.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_32.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_33.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_33.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_34.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_34.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_36.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology1/related_photo_36.png" class="slide"/>								

					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script id="story-template" type="text/x-handlebars-template">
{{#each stories}}
<div class="col-xs-12 col-sm-6 col-md-2-5">
	<div class="display-card story-telling story-more-btn pointer" data-idx="{{@index}}" onclick="">
		<div class="card-image">
			<img src="{{thumbnailUrl}}" class="img-responsive"/>
		</div>
		<div class="card-content">
			<div class="display-card-title">
				{{title}}
			</div>
			<div class="display-card-content">
				{{{desc}}}
			</div>
			
		</div>
	</div>							
</div>
{{/each}}
</script>

<script type="text/javascript">
	var stories = [
		{
			title: "인간의 가능성을 넓히다[로봇]",
			desc: "어릴 때 가지고 놀던 로봇, 기억하시나요?"
				+ "팔, 다리가 움직이는 게 고작이지만 작은 로봇 하나면 하루 가는 줄 모르고 놀던 어린 시절, "
				+ "눈으로 직접 움직이는 로봇을 본다면 얼마나 좋을까 생각했던 적 모두 있으실 겁니다. "
				+ "첨단기술관 1에서는 상상 속에만 있던 많은 로봇들을 만날 수 있습니다. "
				+ "먼저, 로봇공연장을 찾아가 보겠습니다. 음악 소리와 함께 춤을 추는 로봇이 있네요! "
				+ "마치 나의 친구처럼 두 발로 걷고, 악수를 하고, "
				+ "물건을 알아보고 반응하는 로봇을 보니 우리와 로봇이 함께 살게 될 날도 멀지 않은 것 같습니다. "
				+ "로봇의 춤이 끝나자 아이들의 박수소리가 크게 들립니다.",
			thumbnailUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/story02_01.jpg"/>",
			stories: [
				{
					storyLabel: "로봇 공연장",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/story02_01.jpg"/>"
				},
				{
					storyLabel: "로봇 공연장",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/story02_02.jpg"/>"
				},
				{
					storyLabel: "로봇 공연장",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/story02_03.jpg"/>"
				},
				{
					storyLabel: "반응하는 로봇",
					storyDesc: "어릴 때 가지고 놀던 로봇, 기억하시나요?"
						+ "팔, 다리가 움직이는 게 고작이지만 작은 로봇 하나면 하루 가는 줄 모르고 놀던 어린 시절, "
						+ "눈으로 직접 움직이는 로봇을 본다면 얼마나 좋을까 생각했던 적 모두 있으실 겁니다. "
						+ "첨단기술관 1에서는 상상 속에만 있던 많은 로봇들을 만날 수 있습니다. "
						+ "먼저, 로봇공연장을 찾아가 보겠습니다. 음악 소리와 함께 춤을 추는 로봇이 있네요! "
						+ "마치 나의 친구처럼 두 발로 걷고, 악수를 하고, "
						+ "물건을 알아보고 반응하는 로봇을 보니 우리와 로봇이 함께 살게 될 날도 멀지 않은 것 같습니다. "
						+ "로봇의 춤이 끝나자 아이들의 박수소리가 크게 들립니다.", 
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/story02_04.jpg"/>"
				},
				{
					storyLabel: "로봇공연장_인터뷰",
					storyDesc: "“로봇이 춤을 추는 게 너무 자연스러워서 너무 신기했어요. 표정까지 우리와 똑같이 짓고 "
						 + "말도 할 수 있게 된다면 로봇과 친구가 될 수 있을 것 같아요.” "
						 + "공연을 본 한 어린이의 소감을 들어보았는데요. 이 어린이가 어른이 되었을 때는 어린이의 "
						 + "꿈이 이루어져 있을 것 같은 기분 좋은 예감이 들었습니다.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/story02_05_interview.jpg"/>"
				},
				{
					storyLabel: "동작하는 로봇",
					storyDesc: "이 밖에도 로봇 관련 전시물 곳곳에서는 다양한 형태의 로봇들을 만날 수 있었습니다. "
						+ "내가 조작하는 대로 움직이거나, 표정이 바뀌고, 동작을 따라 하는 로봇 등. " 
						+ "이미 우리 생활 속으로 깊숙하게 들어온 로봇을 만나는 기회를 놓치지 말고 경험해 보세요. ", 
						imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/story02_06.jpg"/>"
				}
			]
		},
		{
			title: "내일의 발명왕에게! [발명특허]",
			desc: "삼각팬티와 반창고가 어떻게 발명되었는지 아시나요? 발명은 특별한 사람만 한다는 " 
				+ "생각을 하고 계셨다면 첨단기술관 1 입구에서 만날 수 있는 발명 이야기들을 읽어보세요." 
				+ "할머니의 사랑에서, 아내를 생각하는 남편의 마음에서 발명된 삼각팬티와 반창고의 이야기를 "  
				+ "읽다 보면, 발명이 어려운 것이 아니라는 생각을 하게 될 것입니다.",
			thumbnailUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/story02_07.jpg"/>",
			stories: [
				{
					storyLabel: "발명",
					storyDesc: "삼각팬티와 반창고가 어떻게 발명되었는지 아시나요? 발명은 특별한 사람만 한다는 " 
						+ "생각을 하고 계셨다면 첨단기술관 1 입구에서 만날 수 있는 발명 이야기들을 읽어보세요. " 
						+ "할머니의 사랑에서, 아내를 생각하는 남편의 마음에서 발명된 삼각팬티와 반창고의 이야기를 "  
						+ "읽다 보면, 발명이 어려운 것이 아니라는 생각을 하게 될 것입니다." + "\n\n"
						+ "오늘 날, 우리 나라도 아이디어에 대한 가치를 중요하게 생각하는 사회로 변해가고 있습니다."
						+ "발명 역시 크고 작은 아이디어의 싸움이기에 그 가치를 법적으로 보호하고 인정해주는 " 
						+ "문화가 반드시 필요한데요. 첨단기술관 1에서는 발명가가 되기 위한 방법과 발명의 방법, "
						+ "생활 속 발명품 그리고 그 발명품과 발명 아이디어를 보호받을 수 있는 방법까지 모두 " 
						+ "안내 되어 있습니다. " 
						+ "발명왕을 꿈꾸고 있는 사람이라면 반드시 이 곳을 관람해보세요.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/story02_07.jpg"/>"
				}
				
			]
		},
		{
			title: "생로병사 한계를 무너뜨리다[생명과학]",
			desc: "100세 시대, 생명공학은 이제 선택이 아닌 필수입니다. "
				+"우리의 건강부터 먹거리까지 생명과학이 영향을 안 끼치는 분야가 없다고 해도 과언이 아닌데요. "
				+"그런 의미에서 생명과학 전시를 둘러보는 것은 무척 의미있는 체험이 될 것 같습니다.",
			thumbnailUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/story02_08.jpg"/>",
			stories: [
				{
					storyLabel: "DNA",
					storyDesc: "100세 시대, 생명공학은 이제 선택이 아닌 필수입니다. "
						+"우리의 건강부터 먹거리까지 생명과학이 영향을 안 끼치는 분야가 없다고 해도 과언이 아닌데요. "
						+"그런 의미에서 생명과학 전시를 둘러보는 것은 무척 의미있는 체험이 될 것 같습니다.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/story02_08.jpg"/>"
				},
				{
					storyLabel: "커다란 식물공장",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/story02_09.jpg"/>"
				},
				{
					storyLabel: "커다란 식물공장",
					storyDesc: "생명공학에 대한 전시는 DNA 이야기부터 시작됩니다. 그 동안 많이 얘기해왔지만 어렵기만 했던 DNA와 "
						+ "생명공학에 대한 전반적인 이야기들을 쉽고 즐겁게 만나고 나면 커다란 식물공장 앞에 서게 됩니다.\n" 
						+ "실내 안에 농장이라니 다소 생소한 기분이 드시죠? 실내에서도 야외의 자연환경에서처럼 "
						+ "식물이 잘 자랄 수 있도록 환경을 만들어 준 농장입니다. 이 안에서는 조직배양, 유전자 재조합, "
						+ "세포 융합 등의 기술로 만들어진 식물들이 자라고 있는데요. 이제는 농업도 전통적인 "
						+ "방식만을 고수하는 것이 아니라 첨단 과학과 만나 미래의 경쟁력으로 새롭게 자리하고 " 
						+ "있습니다. 우리나라의 농업도 생명공학의 융합으로 새로운 청사진을 그려가고 있고요.\n"
						+ "여기서 잠깐! 웃고 갈 수 있는 전시가 있어 소개해드립니다. 파란색 가지가 있다면 어떤 모습일까요?"
						+ "식물 공장에서 본 다양한 유전자 재조합 식물들을 떠올리며 나만의 식물을 만들어 볼 수 있는 전시가 있습니다. "
						+ "내가 원하는 색깔과 모양, 채소들을 선택해서 새로운 식물을 만들어 보세요!",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/story02_10.jpg"/>"
				},
				{
					storyLabel: "미니 무균돼지",
					storyDesc: "생명공학은 식물뿐 아니라 인체와 동물에까지 그 분야가 확대되어가고 있습니다. 요즘은 평균 "
						+ "수명이 늘어나고 건강한 삶에 대한 관심이 높아지면서 대체 장기에 대한 이야기를 많이 " 
						+ "접할 수 있는데요. 이 곳에서는 대체 장기에 대한 다양한 정보도 제공되고 있습니다.\n" 
						+ "미니 무균돼지 이야기를 들어보셨나요?\n" 
						+ "인간의 장기 이식을 위해 무균 돼지를 연구 개발하여 장기를 배양하는 모습을 눈으로 직접 볼 수 있답니다.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/story02_11.jpg"/>"
				},
				{
					storyLabel: "암",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/story02_12.jpg"/>"
				},
				{
					storyLabel: "암",
					storyDesc: "첨단기술관 1을 관람하다 보면, 과학뿐 아니라 나의 생활도 돌아볼 수 있는 기회가 주어지게 됩니다. "
						+ "스트레스가 많은 현대인에게 암은 언제나 조심해야 할 적입니다. "
						+ "그런 의미에서 암 검진과 암 예방 역시 생명과학의 중요한 영역입니다." 
						+ "내가 암을 예방하기 위해 어떤 생활을 하고 있는지 또 나에게 어떤 노력이 필요한 지를 이 곳에 전시된 정보와 체험들을 통해 꼭 체크해보세요.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/story02_13.jpg"/>"
				},
				{
					storyLabel: "뇌파를 이용한 자동차",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/story02_14.jpg"/>"
				},
				{
					storyLabel: "뇌파를 이용한 자동차",
					storyDesc: "눈에 보이는 것뿐 아니라 뇌파 역시 새롭게 연구해가야 할, 무궁무진한 가능성을 지닌 연구 분야입니다. "
						+ "뇌파의 의미가 어렵다면 뇌파로 게임을 할 수 있는 공간을 통해 쉽게 뇌파의 존재를 체험해보세요. "
						+ "자! 여기 친구들이 뇌파를 이용해 자동차 경주를 한 참 즐겁게 하고 있는대요. "
						+ "눈에 보이지 않는 뇌파로 게임을 할 수 있다니 믿어지시나요? "
						+ "이 게임을 하고 나면, 뇌파를 이해할 수 있는 것은 물론, 나의 집중력과 뇌 기능을 활성화 할 수 있다고 하니 "
						+ "이 곳은 절대 그냥 지나갈 수 없겠네요!\n\n"
						+ "이 곳의 전시들을 통해 더 많은 관람객들이 어렵게만 느껴졌던 생명공학을 조금 더 쉽게 느낄 수 있는 시간이 되면 좋겠습니다.", 
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/story02_15.jpg"/>"
				},
			]
		},
		{
			title: "에너지의 모든 것[에너지환경]",
			desc: "“이렇게 다양한 에너지가 있었나요?”\n" 
				+ "“원자력 에너지에 대한 오해를 풀 수 있었어요.”\n" 
				+ "“우리나라의 에너지 현황에 대해서 다시 알아볼 수 있는 시간이었어요.”\n\n"
				+ "첨단기술관 1의 에너지 관련 전시를 본 사람들의 이야기입니다. 여러분은 에너지에 대해 "
				+ "얼마나 많이 알고 계신가요? 최근 각광받고 있는 원자력, 수력 에너지부터 신재생 에너지까지! "
				+ "우리가 꼭 알아야 할 에너지에 대한 다양한 설명과 이야기가 이 곳에 전시되어 있습니다. " 
				+ "또 그로 인해 발생하는 다양한 형태의 오염과 그에 대한 우리의 대안들을 생각해볼 수 있는 "
				+ "전시도 마련되어 있고요.",
			thumbnailUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/story02_16.jpg"/>",
			stories: [
				{
					storyLabel: "원자력,수력 에너지등",
					storyDesc: "“이렇게 다양한 에너지가 있었나요?”\n" 
						+ "“원자력 에너지에 대한 오해를 풀 수 있었어요.”\n" 
						+ "“우리나라의 에너지 현황에 대해서 다시 알아볼 수 있는 시간이었어요.”\n\n"
						+ "첨단기술관 1의 에너지 관련 전시를 본 사람들의 이야기입니다. 여러분은 에너지에 대해 "
						+ "얼마나 많이 알고 계신가요? 최근 각광받고 있는 원자력, 수력 에너지부터 신재생 에너지까지! "
						+ "우리가 꼭 알아야 할 에너지에 대한 다양한 설명과 이야기가 이 곳에 전시되어 있습니다. " 
						+ "또 그로 인해 발생하는 다양한 형태의 오염과 그에 대한 우리의 대안들을 생각해볼 수 있는 "
						+ "전시도 마련되어 있고요.",
					imgUrl: "#"
				},
				{
					storyLabel: "꼭꼭 숨은 에너지를 찾아라",
					storyDesc: "우리가 직접 에너지를 만들어 볼 수 있는 체험들도 마련되어 있습니다. "
						+ "에너지 수확기술 ‘꼭꼭 숨은 에너지를 찾아라!' 코너는 내가 직접 움직여서 "
						+ "숨겨진 에너지들을 찾아볼 수 있는 공간으로 무심코 그냥 지나쳤던 생활 속 에너지들을 "
						+ "찾아볼 수 있는 기회를 만날 수 있습니다. 이 곳의 경험을 바탕으로 여러분도 에너지 절약의 리더가 되어 보는 게 어떨까요?",
					imgUrl: "#"
				},
				{
					storyLabel: "압전 발판 체험",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/story02_16.jpg"/>"
				},
				{
					storyLabel: "압전 발판 체험",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/story02_17.jpg"/>"
				},
				{
					storyLabel: "압전 발판 체험",
					storyDesc: "요즘 밤하늘에서 별을 본적 있나요?\n" 
						+ "에너지를 절약하고, 신재생 에너지에 대한 연구를 계속 이어간다면 "
						+ "공해로 사라졌던 반짝이는 별들을 다시 만날 수 있을 것입니다. "
						+ "이러한 기대를 담아 제작된 ‘압전 발판 체험’도 놓치지 말고 경험해보세요. "
						+ "발을 굴러 밤하늘의 별을 켜보는 경험으로 우리의 아름다운 환경을 지키겠다는 의지를 다져볼 수 있을 것입니다.\n\n" 
						+ "에너지에 대한 이야기! 생각보다 훨씬 많은 재미와 정보들이 담겨 있답니다.\n" 
						+ "꼼꼼하게 둘러보고, 기억하고, 실천해주세요!",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/story02_18.jpg"/>"
				}
			]
		},
		{
			title: "미래와 우리를 잇는 다리[정보통신]",
			desc: "이제, 손 안에 누구나 작은 컴퓨터를 들고 다니는 세상!\n" 
				+ "컴퓨터는 어디에서 시작되었고, 또 어떻게 발전되어 왔을까요? 그에 대한 답이 이 곳에 " 
				+ "있습니다. 컴퓨터의 언어인 코드를 직접 텍스트를 입력해서 변환해보며 확인해볼 수 있고 " 
				+ "기기간의 대화를 통신망에 연결된 정보교류를 통해 경험해볼 수 있는 장소도 마련되어 있습니다.",
			thumbnailUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/story02_19.jpg"/>",
			headerDesc: "이제, 손 안에 누구나 작은 컴퓨터를 들고 다니는 세상!\n" 
				+ "컴퓨터는 어디에서 시작되었고, 또 어떻게 발전되어 왔을까요? 그에 대한 답이 이 곳에 " 
				+ "있습니다. 컴퓨터의 언어인 코드를 직접 텍스트를 입력해서 변환해보며 확인해볼 수 있고 " 
				+ "기기간의 대화를 통신망에 연결된 정보교류를 통해 경험해볼 수 있는 장소도 마련되어 있습니다.",
			stories: [
				{
					storyLabel: "나와 닮은 과학자",
					storyDesc: "평소 아인슈타인 닮아 다는 말 들어보셨나요? 아뇨~ 똑똑한 두뇌 말고 바로 얼굴 말이에요!\n" 
							+ "눈이 휘둥그래지는 미래의 기술을 만나기 전, 잠깐 웃으며 쉬어가는 공간인 ‘나와 닮은 과학자는?’코너! "
							+ "얼굴감지와 얼굴자세 측정, 얼굴부분 감지 등을 통해 과학 역사를 새롭게 써온 과학자들 중 나와 닮은 과학자를 찾아보세요!",
					imgUrl: "#"
				},
				{
					storyLabel: "가상 스튜디오",
					storyDesc: "가장 눈에 띄는 것은 미래 디지털 방송 전시입니다. "
							+ "방송 가상 스튜디오가 마련되어 있어 가상의 물체를 실제처럼 표현하고 조작하는 체험을 할 수 있으며, "
							+ "눈으로 인식 못하는 장면을 순간 포착하는 영상 기술을 직접 확인해볼 수 있습니다.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/story02_19.jpg"/>"
				},
				{
					storyLabel: "영상 복원",
					storyDesc: "소리를 변화시켜 영상을 복원하는 기술, 상상해본 적 있으세요?\n"
						+ "소리를 변화시켜 물방울의 위치와 크기를 조절하고, "
						+ "그 곳에 숨겨진 영상을 복원시키는 체험을 할 수 있는데 영상을 복원하는 재미뿐 아니라 "
						+ "내 스스로 미래의 기술을 실현하고 있다는 사실만으로도 큰 보람을 느낄 수 있을 것입니다.",
					imgUrl: "#"
				},
				{
					storyLabel: "디지털 스트리트",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/story02_20.jpg"/>"
				},
				{
					storyLabel: "디지털 스트리트",
					storyDesc: "첨단기술관 1에서 디지털 스트리트는 꼭 걸어봐야 할 명소입니다. "
							+ "내가 지나가는 곳마다 나에게 필요한 광고 정보가 따라 온다면? "
							+ "영화 속에서나 봤던 이러한 기술을 이 곳에서 체험해볼 수 있는 공간이 바로 이 곳에 마련되어 있습니다. "
							+ "내가 움직이는 대로 광고가 따라오고, 미래 애완 로봇이 이에 맞는 반응 들을 하면서 상상 속 미래 거리를 눈 앞에서 실현해볼 수 있습니다.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/story02_21.jpg"/>"
				},
				{
					storyLabel: "LED 드럼",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/story02_22.jpg"/>"
				},
				{
					storyLabel: "줄없는 하프",
					storyDesc: "탁 트인 공간에 마련된 디지털 타운도 놓치지 마세요! 내가 움직이는 대로 음악이 되는 " 
							+ "LED 드럼과 에어 하프는 물론 움직이는 대로 화면에 재미있는 반응을 확인할 수 있는 "
							+ "디지털 타운에서는 친구, 가족, 연인 모두가 웃음꽃을 피우고 있답니다.\n\n"
							+ "백문이불여일견! \n"
							+ "아직 보지 못한 귀로만 들어왔던 미래의 기술! 이제는 참지 말고 눈으로 직접 확인해보세요. "
							+ "자세한 정보와 재미있는 체험들로 미래의 기술을 만나고 나면 나의 경쟁력도 한 층 높아져 "
							+ "있을 것이니까요. 첨단기술관 1은 호기심 가득한 여러분을 기다리고 있습니다!",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology1/story02_23.jpg"/>"
				}
				
			]
		}
	];
	
	var storySource   = $("#story-template").html();
	var storyTemplate = Handlebars.compile(storySource);
	var storyHtml = storyTemplate({stories: stories});
	$("#stories").html(storyHtml);
</script>

<c:url var="shareUrl" value="/display/mainBuilding/advancedTechnology1" />
<c:import url="/WEB-INF/views/display/displayDetailModal.jsp">
	<c:param name="shareUrl" value="${shareUrl }"/>
	<c:param name="exhibitionName" value="첨단기술관1" />
</c:import>


<script type="text/javascript">

	$(document).ready( function() {
		// 주요 전시물 클릭시 show hide event
		//showHideDetails();
		
		// image slider
		PTechSlider('#image-slider').autoSlide();
		
		// 전시관 Map 1층으로.
		//setDefaultSVGMap('#basic-map-spy',1);
		// 지도 flash 해당 전시관 표시
		//$("g[id='XMLID_1_']").attr('class', 'active');
	});
	
	function showHideDetails()  {
		// hide all details first.
		$('.exhibition-detail-wrapper').hide();
		// show the active detail only.
		var exhibit_active_id = $('.major-exhibition-wrapper.active').data('id');
		$(exhibit_active_id).show();
		
		// click event for major exhibition box
		$('.major-exhibition-wrapper').on('click', function() {
			
			// get current active div & remove active
			var hide_id = $('.major-exhibition-wrapper.active').data('id');
			$('.major-exhibition-wrapper.active').removeClass('active');
			// hide the matching detail
			$(hide_id).hide();
			
			// add 'active' to the current clicked div
			var show_id = $(this).data('id');
			$(this).addClass('active');
			// show the mathcing detail
			$(show_id).show();
			
		});
	}
	
</script>

<!-- Major Exhibition Contents Loading -->
<c:import url="/WEB-INF/views/display/majorExhibitionTemplate.jsp" />
<script type="text/javascript">
	var exhibitions = [
		{
			descriptions : [
				
				{
					title: "생명공학, 유전자 기술의 현재와 미래를 보다",
					desc : "꽁꽁 얼어붙을 정도로 추운 남극 세종기지에서는 어떻게 신선한 야채를 먹을 수 있을까? "
						+ "식물공장에서는 인공 양육베드, 온·습도 제어 장치, LED 조명장치를 통해 인공적으로 식물의 "
						+ "생육 환경을 조절할 수 있다. 첨단농업기술의 집성체인 식물공장은 앞으로 지구가 아닌 다른 "
						+ "행성에서도 신선한 야채를 길러 먹을 가능성을 상상하게 해 준다. "
						+ "<br>이 외에도 DNA의 구조와 기능, 유전자를 이용한 다양한 첨단기술의 원리와 활용 방안, "
						+ "의학에 적용되는 생명과학적 기술들을 한 자리에서 살펴볼 수 있다. "
				},
				{
					title: "",
					desc : "‘뇌파는 마술사’, ‘마인드 레이싱카’, ‘알파 경주’로 이루어진 뇌파 체험 3총사는 빼놓을 수 없는 체험물이다. "
						+ "뇌와 기계를 연결하는 장치(BCI)를 통해 뇌파를 측정하고, 뇌파를 이용해 자동차 경주와 구슬 굴리기 게임을 즐겨볼 수 있다. "
				}
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/mainBuilding/advancedTechnology1/major_exhibition_02.png' />",
					caption: "[식물공장]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/mainBuilding/advancedTechnology1/major_exhibition_03.png' />",
					caption: "[마인드레이싱카]"
				},
			]
		},
		{
			descriptions : [
				{
					title : "로봇체험관, 인류의 동반자 로봇을 만나다",
					desc : "‘로봇(robot)’은 1920년 카렐 차페크의 소설에서 처음으로 언급된 지 불과 100년 만에, "
						+ "사람을 대신하여 자동차를 생산하고 자유롭게 이동하며 위험 지역을 점검하기도 한다. "
						+ "이러한 로봇의 발전 역사와 함께 표정을 따라하는 로봇, 블록을 쌓는 로봇, 대화를 나눌 "
						+ "수 있는 로봇을 만나볼 수도 있다. 로봇공연장에서는 25cm 크기의 휴머노이드형 미니 "
						+ "로봇이 노래에 맞춰 춤을 추며, 넘어지면 스스로 일어나기도 한다. 생명공학, 인공지능, "
						+ "정보통신의 발전과 함께 지능형 로봇이나 인간을 닮은 휴머노이드 로봇으로 발전하여 로봇이 인류의 "
						+ "동반자가 될 날도 그다지 멀지 않은 것 같다."
						+ "<br><div class='additional-info'>(로봇댄스공연 : 250명/회, 누구나)</div>"
				},
				{
					title: "정보통신, 언제 어디서나 정보를 얻는다",
					desc : "“하와이”라고 이야기하면 컴퓨터가 알아서 비행기 시간표와 가격, 숙박시설, 대표 여행지의 정보, "
						+ "현지 날씨까지 스스로 찾아 제공해 준다. IPv6로 자신만의 주소를 부여 받은 사물의 컴퓨터들은 자신이 "
						+ "수집한 정보를 쏟아내고, 이 수많은 정보는 차세대 휴대기기에 저장되어 개인 비서처럼 사용자의 취향에 맞게 검색되어 제공된다. "
						+ "작은 진공 유리관 속 전극 두 개의 움직임으로 시작된 컴퓨터와 이를 연결한 네트워크는 시간과 장소에 상관없이 "
						+ "정보통신망을 이용해 원하는 정보를 얻을 수 있는 세상을 만들고 있다." 
				},
				{
					title: "에너지‧환경, 깨끗하고 풍요로운 세상을 만들다",
					desc : "삶을 유지하는 데 에너지 자원을 얻는 것은 필수적이다. 그러나 현재 인류가 주로 의존하고 있는 "
						+ "화석에너지는 환경오염과 지구온난화, 자원고갈 문제로 우리를 위협하고 있다. 이에 ‘신‧재생에너지’는 "
						+ "보다 똑똑하게 에너지를 구하고 사용하는 방법에 대해 알아볼 수 있다. 풍력, 태양광, 조력, 연료전지에서 핵융합에 "
						+ "이르기까지 다양한 신‧재생에너지의 종류와 원리, 구조와 기능 및 생활에 적용할 수 있는 과학 기술을 체험해 볼 수 있다. "
						+ "또 스마트그리드와 같은 정보통신기술을 이용한 에너지의 효율적 활용을 통해 청정에너지의 미래 모습을 그려볼 수 있다. "
						+ "‘원자력에너지’는 한국수력원자력의 후원으로 만들어진 코너로서, 상대성 이론에서부터 원자력발전소의 구조와 원리에 이르기까지 "
						+ "단계적으로 원자력에 대해 학습할 수 있다. " 
				},
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/mainBuilding/advancedTechnology1/major_exhibition_01.png' />",
					caption: "[로봇댄스공연]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/mainBuilding/advancedTechnology1/major_exhibition_04.png' />",
					caption: "[신재생에너지]"
				}
			]
		}
	];
	
	var exhibitionSource   = $("#major-exhibition-template").html();
	var exhibitionTemplate = Handlebars.compile(exhibitionSource);
	var exhibitionHtml = exhibitionTemplate({exhibitions: exhibitions});
	$("#main-exhibitions").html(exhibitionHtml);
	
</script>