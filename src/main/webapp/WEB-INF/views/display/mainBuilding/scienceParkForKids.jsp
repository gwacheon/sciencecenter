<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-teal">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>
	<div id="display-body" class="sub-body">
		<div class="narrow-sub-top display">
			<div class="sub-banner-tab-wrapper sub-banner-tab-teal">
				<!-- <div class="container">
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
							<ul class="nav nav-tabs">
								<li class="col-xs-12 active">
									<a href="http://cyber.scientorium.go.kr/museum_2015/vrLoad.jsp?pavilion=indoor&xml_name=kid_001.xml&angle=90" class="item">
										<i class="fa fa-desktop fa-lg" aria-hidden="true"></i> 사이버 전시관
									</a>		
								</li>
							</ul>   
						</div>
					</div>
				</div> -->
			</div>
		</div>
		<div class="container">
			<div id="basic-map-spy" class="section scrollspy">
				<div class="row">
					<div class="col-md-12">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							어린이탐구체험관 안내
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-md-4 col-lg-4">
						<ul class="circle-list teal">
							<li class="margin-bottom20">
								유의사항
								
								<ul>
									<li>대상: 유아 ~ 초등학교 3학년 어린이</li>
									<li>
										어린이들의 안전을 위해 시간별 입장인원을 제한하고 있으므로 평일 단체관람객(10인 이상)은 사전 예약이 필요합니다.
									</li>
									<li>
										주말 및 가족,개인 관람객은 별도의 예약 없이 현장 상황에 따라 입장하실 수 있습니다.
									</li>
									<li>
										사전예약을 하신 경우에도 과학관 입장권을 구매하셔야 어린이탐구체험관 입장이 가능합니다.
									</li>
									<li>
										단체예약은 온라인 예약으로 입장 가능합니다.
									</li>
									<li>
										단체 예약이 안되어 있을경우, 입장이 제한됩니다.
									</li>
								</ul>
							</li>
							<li>
								주요전시물
								<ul>
									<li>
										<b>「어린이탐구체험관(Children's Hall)」</b>은 유아 및 아동기의 어린이가 과학적 놀이를 통해 
										성장 발달의 기초가 되는 신체, 정서, 언어, 인지, 사회성 등을 발달시킬 수 있는 공간이다. 
										총 44건의 전시물로 구성되어 있으며 이 중 체험물이 34건을 차지한다. 
										이 곳은 ‘인체탐험 매직버스’, ‘3D 영상실’ 등 감각적으로 경험할 수 있는 재미있는 체험물로 가득하다. 
									</li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="col-sm-12 col-md-8 col-lg-8 text-right">
						<!-- select dropdown : 다른관으로 이동 -->
						<c:import url="/WEB-INF/views/display/displayHallSelect.jsp"/>
						<div>
							<img alt="어린이탐구체험관1" src="<c:url value='/resources/img/display/map/03.png'/>"
								class="img-responsive margin-top20">
							<img alt="어린이탐구체험관2" src="<c:url value='/resources/img/display/map/04.png'/>"
								class="img-responsive margin-top20">
						</div>
					</div>
				</div>
			</div>
			<div id="major-exhibitions-spy" class="section scrollspy">
				<div class="row">
					<div class="col-md-12">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							주요전시물
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="main-exhibition-wrapper">
							<div id="main-exhibitions">
							</div>
						</div>
					</div>
				</div>
			</div>
				
			<!--  story telling -->
			<div id="story-spy" class="story-telling-wrapper">
				<div class="header">
					<div class="row">
						<div class="col-md-12">
							<h3 class="page-title teal">
								<div class="top-line"></div>
								스토리로 읽어 보는 어린이탐구체험관 여행기
							</h3>
						</div>
					</div>
				</div>
				<div class="description">
					아이들의 상상력이 스마트폰과 컴퓨터에 갇혀 천편일률적인 생각에만 머물고 있는 것이 안타깝다면, 
					이 곳을 놓쳐서는 안됩니다. 어린이탐구체험관은 개성강한 아이들의 상상력을 자극하고, 
					생각 주머니의 크기를 넓혀주는 체험이 가득합니다. 
					오감을 자극하는 다양한 전시 안에서 즐겁게 뛰어 놀면서 아이들은 나도 모르게 성장해 있을 것입니다. 
					그럼 아이들의 웃음소리를 따라 어린이탐구체험관을 만나보시죠.
				</div>
				
				<div id="stories" class="row">
				</div>
			</div>
			<div class="related-photos">
				<div class="title">
					<i class="color-darkgreen material-icons">&#xE413;</i>   관련사진
				</div>
				<div id="image-slider">
					<div id="image-slider-window">
						<img src="<c:url value="/resources/img/display/mainBuilding/sciencePark/related_photo_01.png"/>" 
							data-url="/resources/img/display/mainBuilding/sciencePark/related_photo_01.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/sciencePark/related_photo_02.png"/>" 
							data-url="/resources/img/display/mainBuilding/sciencePark/related_photo_02.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/sciencePark/related_photo_03.png"/>" 
							data-url="/resources/img/display/mainBuilding/sciencePark/related_photo_03.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/sciencePark/related_photo_04.png"/>" 
							data-url="/resources/img/display/mainBuilding/sciencePark/related_photo_04.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/sciencePark/related_photo_05.png"/>" 
							data-url="/resources/img/display/mainBuilding/sciencePark/related_photo_05.png" class="slide"/>
					</div>  
				</div>
			</div>
		</div>
	</div>
</div>


<script id="story-template" type="text/x-handlebars-template">
{{#each stories}}
<div class="col-xs-12 col-sm-6 col-md-6">
	<div class="display-card story-telling story-more-btn pointer" data-idx="{{@index}}" onclick="">
		<div class="card-image">
			<img src="{{thumbnailUrl}}" class="img-responsive"/>
		</div>
		<div class="card-content">
			<div class="display-card-title">
				{{title}}
			</div>
			<div class="display-card-content">
				{{{desc}}}
			</div>
			
		</div>
	</div>							
</div>
{{/each}}
</script>

<script type="text/javascript">
	var stories = [
		{
			title: "신나는 인체 탐험",
			desc: "“사람의 몸 속은 어떻게 생겼나요?” \n\n"			
				+ "어린이의 질문에 대답하려면, 큐씨, 엔씨의 매직버스를 타야겠네요! "
				+ "잠이 들어버린 박사님의 몸 속을 국립과천과학관의 마스코트들과 4D 영상물을 "
				+ "통해 탐험해보는 시간이 마련되어 있답니다. 큐씨, 엔씨와 함께 신나는 인체 탐험을 시작해보세요!",
			thumbnailUrl: "<c:url value="/resources/img/display/mainBuilding/sciencePark/story04_01.jpg"/>",
			stories: [
				{
					storyLabel: "매직버스",
					storyDesc: "“사람의 몸 속은 어떻게 생겼나요?” \n\n"			
						+ "어린이의 질문에 대답하려면, 큐씨, 엔씨의 매직버스를 타야겠네요! "
						+ "잠이 들어버린 박사님의 몸 속을 국립과천과학관의 마스코트들과 4D 영상물을 "
						+ "통해 탐험해보는 시간이 마련되어 있답니다. 큐씨, 엔씨와 함께 신나는 인체 탐험을 시작해보세요!",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/sciencePark/story04_01.jpg"/>"
				},
				{
					storyLabel: "감각놀이터",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/sciencePark/story04_02.jpg"/>"
				},
				{
					storyLabel: "그물놀이",
					storyDesc: "신나게 뛰어 노는 것을 좋아하는 어린이라면 감각놀이터를 그냥 지나칠 수 없겠죠? "
							+ "그물놀이와 파이프 실로폰 등 아이들이 좋아하는 신체활동과 놀이가 곳곳에 숨어있습니다. "
							+ "아이들은 신나게 놀면서 자연스럽게 사고력과 감수성을 키울 수 있답니다. 친구들과 함께 감각놀이터에서 즐거운 시간을 보내보세요!",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/sciencePark/story04_03.jpg"/>"
				},
				{
					storyLabel: "창작놀이방",
					storyDesc: "창작놀이방에서도 즐거운 생각은 멈추지 않습니다. 상상한 것들을 블록을 쌓아 만들며 창의력을 키울 수 있어요. "
							+ "여기저기 블록으로 자동차를 만들고, 집을 만들고, 발명품을 만드는 어린이들이 보이네요. "
							+ "어린이 한 명, 한 명이 발명가가 되고 과학자가 되는 의미 있는 시간입니다.", 
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/sciencePark/story04_04.jpg"/>"
				},
				{
					storyLabel: "물의 힘으로",
					storyDesc: "물은 부드럽고 정해진 모양도 없지만, 큰 힘을 가지고 있습니다. "
							+ "우리도 물이 없으면 단 하루도 살 수 없으니까요. 물의 특성을 잘 활용하면 물체의 "
							+ "움직임을 만들 수 있어요. 물을 이용해서 여러 색깔의 공을 공중에 띄우고, "
							+ "레일 앞으로 보내는 활동을 하다 보면 호기심과 사고력이 쑥쑥 자라나게 될 것입니다.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/sciencePark/story04_05.jpg"/>"
				},
				{
					storyLabel: "오르락내리락도르래",
					storyDesc: "아까부터 두 명의 친구가 무언가를 열심히 당기고 있는데요. 친구들의 이야기를 한 번 들어보겠습니다.\n" 
							+ "“이 걸 당기니까 인형이 위로 올라갔다 내려갔다 해요.”\n"
							+ "“친구랑 함께 하니까 힘을 많이 들이지 않고도 인형을 움직일 수 있어요.”\n\n"
							+ "오르락, 내리락 도르래로 인형을 움직이는 놀이를 통해 아이들은 작용, 반작용의 원리를 자연스럽게 배워볼 수 있게 됩니다. "
							+ "어린이들이 쉽게 과학을 만날 수 있는 좋은 기회이겠네요.", 
						imgUrl: "<c:url value="/resources/img/display/mainBuilding/sciencePark/story04_06.jpg"/>"
				},
				{
					storyLabel: "깨끗한 숲 만들기",
					storyDesc: "다양한 착시 그림들과 어질어질 기울어진 방을 지나고 나면, 깨끗한 숲 만들기 체험공간이 나타납니다. "
							+ "어린이들도 엄마의 분리수거를 도와주면서 많은 생활 용품들이 재활용이 될 수 있다는 사실은 이미 알고 있죠? "
							+ "무심코 버렸던 이 생활용품들이 자연으로 분해되어 돌아가는 대는 생각보다 무척 긴 시간이 필요하다고 합니다. "
							+ "모형 놀이를 통해 생활용품들이 분해되는 시간을 직접 체험해보고, 자연 보호에 대한 필요성을 아이들이 직접 체험해볼 수 있는 기회를 만나보시기 바랍니다.", 
						imgUrl: "<c:url value="/resources/img/display/mainBuilding/sciencePark/story04_07.jpg"/>"
				},
				{
					storyLabel: "어린이 다양한 표정들",
					storyDesc: "여기저기 아이들의 다양한 표정들이 보입니다. 처음 보는 신기한 현상들에 눈이 동그래진 아이, "
							+ "친구들과 함께하는 놀이가 신이나 활짝 웃는 아이, 눈 앞에 펼쳐지는 놀라운 광경들을 하나라도 놓칠까 진지한 "
							+ "얼굴로 집중하는 아이, 머릿속 상상력들이 마구 움직이며 새로운 아이디어로 가득 차 흥분된 아이! \n\n" 
							+ "아이들과 함께 자라는 과학! 놀며 배우는 재미있는 과학!\n" 
							+ "어린이들의 가능성을 키워주는 곳, 어린이탐구체험관입니다.", 
						imgUrl: "<c:url value="/resources/img/display/mainBuilding/sciencePark/story04_08.jpg"/>"
				}
			]
		}

	];
	
	var storySource   = $("#story-template").html();
	var storyTemplate = Handlebars.compile(storySource);
	var storyHtml = storyTemplate({stories: stories});
	$("#stories").html(storyHtml);
</script>

<c:url var="shareUrl" value="/display/mainBuilding/scienceParkForKids" />
<c:import url="/WEB-INF/views/display/displayDetailModal.jsp">
	<c:param name="shareUrl" value="${shareUrl }"/>
	<c:param name="exhibitionName" value="어린이탐구체험관" />
</c:import>


<script type="text/javascript">

	$(document).ready( function() {
		// 주요 전시물 클릭시 show hide event
		//showHideDetails();
		
		// image slider
		PTechSlider('#image-slider').autoSlide();
		
		// 전시관 Map 1층으로.
		//setDefaultSVGMap('#basic-map-spy',1);
		// 지도 flash 해당 전시관 표시
		//$("g[id='XMLID_15_']").attr('class', 'active');
	});
	
	function showHideDetails()  {
		// hide all details first.
		$('.exhibition-detail-wrapper').hide();
		// show the active detail only.
		var exhibit_active_id = $('.major-exhibition-wrapper.active').data('id');
		$(exhibit_active_id).show();
		
		// click event for major exhibition box
		$('.major-exhibition-wrapper').on('click', function() {
			
			// get current active div & remove active
			var hide_id = $('.major-exhibition-wrapper.active').data('id');
			$('.major-exhibition-wrapper.active').removeClass('active');
			// hide the matching detail
			$(hide_id).hide();
			
			// add 'active' to the current clicked div
			var show_id = $(this).data('id');
			$(this).addClass('active');
			// show the mathcing detail
			$(show_id).show();
			
		});
	}

</script>


<!-- Major Exhibition Contents Loading -->
<c:import url="/WEB-INF/views/display/majorExhibitionTemplate.jsp" />
<script type="text/javascript">
	var exhibitions = [
		{
			descriptions : [
				{
					title : "인체탐험 매직버스, 몸 속 탐험 시작~",
					desc : "전시관 입구에 있는 노란색 버스에 탑승하면 먼지만큼 작아진 채로 우리 몸속에 들어가 "
						+ "폐, 심장, 소장, 뇌를 탐험해 볼 수 있다. 몸속이 어떻게 생겼는지 궁금한 어린이 친구들은 "
						+ "한 번 도전해 보자."
						+ "<br><div class='additional-info'>(20명/회, 초등 1~3학년)</div>"
				},
				{
					title: "감각놀이터, 보자마자 달려달려~",
					desc : "감각놀이터는 알록달록하며 울퉁불퉁한 비탈길과 미끄럼틀이 3차원 구조물로 이루어져 "
						+ "색 인지 감각과 공간 지각 능력을 발달시킬 수 있는 고난이도 체험 공간이다. "
						+ "신나는 미끄럼틀로 세 가지 길(그물 속 통과하기, 계단길 오르기, 미로찾기)을 찾아 "
						+ "가는 놀이 방식이다. 어린이가 스스로 체험에 빠져드는 매력이 있어 인기가 높은 전시물이다. " 
				},
				{
					title : "생각놀이터, 내 힘으로 도전~",
					desc : "생각놀이터는 자신의 신체와 주변 사물에 대해 지속적인 관심을 가지고 탐구 과정을 "
						+ "실현해 볼 수 있는 공간이다. 다양한 물놀이 체험기구를 사용해 물의 힘으로 공을 "
						+ "움직이고(‘물의 힘으로’), 도르래를 움직여 인형을 높이 올리면서(‘어떻게 올라가지?’) "
						+ "여러 가지 사물이나 자연현상에 대해 호기심을 갖고 알아간다."
				},
				{
					title: "",
					desc : "어린이들의 탐구 본능을 지속적으로 이끌어주면서 과학적 기초 지식을 알아가게 하는 것은 매우 중요하다. "
						+ "이러한 과정을 통해 유아들은 자연 세계를 알아가고 발전된 과학기술을 생활 속에서 활용할 수 있는 것은 물론, "
						+ "자연과 인간이 더불어 살아가는 것이 중요하다는 것을 경험하게 된다. " 
				}
				
				
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/mainBuilding/sciencePark/major_exhibition_01.png' />",
					caption: "[그물 속 통과하기]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/mainBuilding/sciencePark/major_exhibition_02.png' />",
					caption: "[오르락! 내리락!]"
				},
			]
		},
		{
			descriptions : [
				{
					title : "인간의 감각, 어? 아하!",
					desc : "우리는 눈, 귀, 손과 같은 감각을 통해 바깥세상과 접촉하고, 그 정보를 뇌가 받아들여 인식하게 된다. "
						+ "이 과정에서 착각 현상들이 생기기도 하는데, ‘인간의 감각’ 코너에서는 14종의 감각체험형 전시물을 통해 "
						+ "재미있는 착각들을 체험해 보고, 그 원인을 과학적으로 배울 수 있다. 우리의 뇌가 어떻게 보는지, "
						+ "어떻게 듣는지, 어떻게 느끼는지에 대한 궁금증을 함께 풀어보자."
				},
				{
					title: "3D 영상실, 신나는 영화 속으로~",
					desc : "매 차시마다 다양한 주제의 3D 영상을 관람할 수 있는 영상실이다. 현재 총 3개의 영상이 상영되고 있다. "
						+ "‘샌디의 공룡알 구출작전’에는 타임머신 로봇이 공룡이 살던 과거 시대로 시간여행을 하며 아기 트리케라톱스를 지켜내는 "
						+ "여정을 담았다. ‘쌀 왕국과 황금들판’은 우리가 매일 먹는 밥의 재료인 쌀과 친환경 농법에 대해 다루며, 주인공이 쌀 왕국에 "
						+ "가서 해충을 물리치는 내용이다. ‘로켓의 발사’는 지구에 추락한 주인공이 로켓을 연구하고 우주선을 고치는 미션을 수행하며 천문·우주에 대한 흥미를 가지게 한다."
						+ "<br><div class='additional-info'>(30명/회, 만 5세~초등 3학년)</div>" 
				}
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/mainBuilding/sciencePark/major_exhibition_03.png' />",
					caption: "[인간의 감각]"
				}
			]
		}
	];
	
	var exhibitionSource   = $("#major-exhibition-template").html();
	var exhibitionTemplate = Handlebars.compile(exhibitionSource);
	var exhibitionHtml = exhibitionTemplate({exhibitions: exhibitions});
	$("#main-exhibitions").html(exhibitionHtml);
	
</script>