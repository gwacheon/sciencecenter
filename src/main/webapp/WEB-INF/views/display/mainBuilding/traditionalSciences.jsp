<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-teal">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
		<%-- <nav id="scrollspy-nav" class="navbar-default navbar-static has-scroll">
			<div class="container">
				<ul class="nav navbar-nav">
					<c:import url="/WEB-INF/views/display/navbar.jsp"/>
					<li>
						<a href="#basic-info-spy" class="item">
							기본 안내 
						</a>
					</li>
					<li>
						<a href="#basic-map-spy" class="item">
							전통과학관 안내
						</a>
					</li>
					<li>
						<a href="#major-exhibitions-spy" class="item">
							주요전시물
						</a>
					</li>
					<li>
						<a href="#story-spy" class="item">
							스토리 여행기
						</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<div class="bread-crumbs">
							전시관 / 전통과학관
						</div>
					</li>
				</ul>
			</div>
		</nav> --%>
	</div>
	<div id="display-body" class="sub-body">
		<div class="narrow-sub-top display">
			<div class="sub-banner-tab-wrapper sub-banner-tab-teal">
				<!-- <div class="container">
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
							<ul class="nav nav-tabs">
								<li class="col-xs-12 active">
									<a href="http://cyber.scientorium.go.kr/museum_2015/vrLoad.jsp?pavilion=indoor&xml_name=tra_002.xml&angle=180" class="item">
										<i class="fa fa-desktop fa-lg" aria-hidden="true"></i> 사이버 전시관
									</a>		
								</li>
							</ul>   
						</div>
					</div>
				</div> -->
			</div>			
		</div>
		<div class="container">
			<div id="basic-map-spy" class="section scrollspy">
				<div class="row">
					<div class="col-sm-3 col-md-2 col-lg-2">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							전통과학관 안내
						</h3>
					</div>
				</div>
				<div class="row description">
					<div class="col-sm-12 col-md-4 col-lg-4">
						<ul class="circle-list teal">
							<li>
								주요전시물
								<ul>
									<li>
										<b>「전통과학관」</b>은 우리 조상들이 살아오면서 깨달은 과학기술의 
										지혜와 슬기를 배우는 공간이다. 하늘, 땅, 사람, 생활, 응용과학 분야의 전시물 총 
										80건으로 구성되어 있다. 이 중 16건의 체험물이 있으며, ‘사상체질진단’, ‘맥파체험’, 
										‘동서양 노젓기’등이 대표적이다. 이러한 전통 과학기술을 체험하고 배움으로써 계승·발전시켜 
										나가는 것을 목적으로 한다.
									</li>
								</ul>					
							</li>
						</ul>
					</div>     
					<div class="col-sm-12 col-md-8 col-lg-8 text-right">
						<!-- select dropdown : 다른관으로 이동 -->
						<c:import url="/WEB-INF/views/display/displayHallSelect.jsp"/>
						<div>
							<img alt="전통과학관" src="<c:url value='/resources/img/display/map/06.png'/>"
								class="img-responsive margin-top20">
						</div>
					</div>
				</div>
			</div>
			<div id="major-exhibitions-spy" class="section scrollspy">
				<div class="row">
					<div class="col-sm-3 col-md-2 col-lg-2">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							주요전시물
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="main-exhibition-wrapper">
							<div id="main-exhibitions">
							</div>
						</div>
					</div>
				</div>
				<div id="story-spy" class="story-telling-wrapper">
				<div class="header">
					<div class="row">
						<div class="col-md-6">
							<h3 class="page-title teal">
								<div class="top-line"></div>
								스토리로 읽어 보는 전통과학관 여행기
							</h3>
						</div>
					</div>
				</div>
				<div class="description">
					타임머신을 타고 과거로 가볼 수 있다면, 어떤 순간으로 여행을 떠나보고 싶으세요?<br> 
					저는 이순신 장군님이 멋지게 거북선을 타고 적군을 무찌르는 그 순간으로 시간 여행을 해보고 싶습니다. <br>
					오직 리더십과 전략, 과학적으로 고안된 무기만으로 수적으로 열세인 상황에서 거둔 승리! 여러분도 선조들이 이룬
					빛나는 업적들을 직접 눈으로 보고 싶으시죠? 그런 분들께 전통과학관으로의 여행을 추천합니다. 바로 지금, 함께 국립과천과학관이라는
					타임머신을 타고 선조들이 남긴 위대한 과학유산들을 만나러 떠나보시죠!
				</div>
				
				<div id="stories" class="row">
				</div>
			</div>
				<div class="related-photos">
					<div class="title">
						<i class="color-darkgreen material-icons">&#xE413;</i>관련사진
					</div>
					<div id="image-slider">
						<div id="image-slider-window">
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_01.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_01.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_02.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_02.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_03.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_03.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_04.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_04.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_05.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_05.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_06.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_06.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_07.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_07.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_08.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_08.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_09.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_09.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_10.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_10.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_11.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_11.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_12.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_12.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_13.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_13.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_14.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_14.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_15.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_15.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_16.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_16.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_17.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_17.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_18.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_18.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_19.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_19.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_20.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_20.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_21.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_21.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_22.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_22.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_23.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_23.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_24.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_24.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_25.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_25.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_26.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_26.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_27.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_27.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_28.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_28.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_29.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_29.png" class="slide"/>
							<img src="<c:url value="/resources/img/display/mainBuilding/traditionalSciences/related_photo_30.png"/>" 
								data-url="/resources/img/display/mainBuilding/traditionalSciences/related_photo_30.png" class="slide"/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script id="story-template" type="text/x-handlebars-template">
{{#each stories}}
<div class="col-xs-12 col-sm-6 col-md-2-5">
	<div class="display-card story-telling story-more-btn pointer" data-idx="{{@index}}" onclick="">
		<div class="card-image">
			<img src="{{thumbnailUrl}}" class="img-responsive"/>
		</div>
		<div class="card-content">
			<div class="display-card-title">
				{{title}}
			</div>
			<div class="display-card-content">
				{{{desc}}}
			</div>
			
		</div>
	</div>							
</div>
{{/each}}
</script>

<script type="text/javascript">
	var stories = [
		{
			title: "하늘은 스스로 연구하는 자를 돕는다 [하늘의 과학]",
			desc: "하늘은 동서고금을 막론하고 언제나 과학자들의 가장 큰 연구과제였습니다. "
				+ "수 많은 질서와 과학 원리들을 밝혀냈지만, 아직도 무궁무진한 가능성과 풀어야 할 숙제들을 안고 있는 하늘! "
				+ "우리 선조들도 오래 전부터 하늘에 관심을 갖고 많은 발명품들을 만들어 냈습니다.",
			thumbnailUrl: "<c:url value="/resources/img/display/mainBuilding/traditionalSciences/story08_02.jpg"/>",
			headerDesc : "하늘은 동서고금을 막론하고 언제나 과학자들의 가장 큰 연구과제였습니다. 수 많은 질서와 " 
				+ "과학 원리들을 밝혀냈지만, 아직도 무궁무진한 가능성과 풀어야 할 숙제들을 안고 있는 하늘! " 
				+ "우리 선조들도 오래 전부터 하늘에 관심을 갖고 많은 발명품들을 만들어 냈습니다.",
			stories: [
				{
					storyLabel: "천상열차분야지도",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/traditionalSciences/story08_01_01.jpg"/>"
				},
				{
					storyLabel: "천상열차분야지도",
					storyDesc: "세계에서 두 번째로 오래된 천문도! 무엇인지 알고 계신가요? 바로 제가 지금 앞에 서 있는" 
						+ "천상열차분야지도입니다. 1395년, 우리나라에서 바라본 하늘의 형상을 적도 좌표계를 기준으로 하여 둥근 평면에 투영하여 표현한 천문도로, 1,467개의 별과 282개의 별자리가 " 
						+ "담겨 있습니다. 왠지 낯이 익다고요? 그럼 지금 지갑에 있는 만원권 지폐를 꺼내보세요. 만원권 지폐 뒷면에 그려진 별자리가 바로 이 천상열차분야지도입니다. " 
						+ "지폐 이미지는 더 많은 사람이 기억했으면 좋겠다고 생각하는 유산들과 인물들을 선정하여 새겨놓는 것을 고려해보면, "
						+ "천상열차분야지도가 우리 민족에게 얼마나 중요한 것인지를 알 수 있습니다. "
						+ "변변한 천체망원경도 없던 시절 이토록 정교한 천문도를 완성했다는 것은 지금 봐도 너무 놀랍고 자랑스러운 사실입니다. \n\n"
						+ "세계 수준의 우리나라 천문 기술을 볼 수 있는 것은 이뿐만이 아닙니다. 모두 경주 가보셨죠? " 
						+ "학교에서 현장학습을 떠나거나 가족들과 함께 휴가로 많이 찾는 신라시대 역사의 보고 경주에서 본 문화재 중 기억에 남는 것은 무엇이었나요? "
						+ "석가탑만 떠올리고 계시다면 다음 경주 여행 때 꼭 첨성대를 찾아가보세요. 첨성대는 동양에서 가장 오래된 천문대로 그냥 쌓여진 탑이 "
						+ "아니라 네모의 받침대에 원의 몸통, 우물 정 모양의 상단부로 각각 만들어져 신비함을 더합니다. 그 시절부터 별을 보고 날씨를 예측하고, "
						+ "하늘을 연구했다는 것만으로도 첨성대는 자랑스러운 우리의 유산입니다. 그냥 문화재로만 생각하고 지나쳤던 첨성대에 대해 자세한 정보를 "
						+ "얻고, 다시 한번 기술을 되돌아 볼 수 있는 전시물이었습니다. \n\n"
						+ "여러 가지 과거 천문 자료들을 지나와, 여기 또 신기한 시계가 보입니다. 해시계인 것 같네요. " 
						+ "이름은 양부일구라고 쓰여있고요. 24절기와 시각을 예측할 수 있을 뿐 아니라 더 많은 백성들이 농사와 생활에 필요한 정보를 "
						+ "얻었으면 하는 마음을 담아 글을 모르는 사람도 볼 수 있도록 설계 되었습니다. 지금이 몇 시인지 친구들과 함께 양부일구로 맞춰보는 것도 참 재미있겠네요.\n\n"
						+ "매일 누구에게나 똑같이 보여지는 밤하늘! 그 곳에서 새로운 지식과 정보를 찾고, "
						+ "우리 삶에 꼭 필요한 생활품을 만들어 온 우리 선조들의 노력을 보니 어떤 생각이 드시나요? 여러분도 " 
						+ "누구에게나 주어지는 일상 속에서, 아무나 찾을 수 없는 숨겨진 지혜와 지식들을 찾아보세요! 우리의 후손들에게 꼭 남겨주고 싶은 위대한 유산이 될지도 모르니까요.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/traditionalSciences/story08_01_02.jpg"/>"
				}
			]
		},
		{
			title: "지혜로 삶의 질을 높이다 [응용과학]",
			desc: "지금은 찾아 볼 수 없지만, 생활 적재적소에 이용되며 선조들의 삶의 질을 높였던 과학 결과물들도 많이 있습니다. "
				+ "이제는 사극이나 영화에서밖에 볼 수 없는 봉수대가 보입니다. 위험의 순간, 신속하게 상황을 전달하기 위해 만들어진 "
				+ "봉수대를 이 곳 전통과학관에서 간접적으로 체험해볼 수 있습니다. 당시 수원화성에 위치했던 봉수대를 모형으로 제작한 "
				+ "이 체험물은 구조, 형태, 당시 봉화를 올렸던 방법과 횟수까지 상세하게 체험해볼 수 있으니 놓치지 말고 경험해보세요."
				+ "“이게 뭐예요?”\n"
				+ "“꼭 악기처럼 생겼어요. 흔들면 소리가 나나요?”\n\n"
				+ "주판 앞에 서있는 어린 학생들의 질문이 재미있네요. 그러고 보니 지금은 나이가 아주 많은 어르신들이 "
				+ "사용하는 것 말고는 찾아보기 힘든 주판은 어린 학생들이 보기엔 정말 신기한 물건 일 것 같네요. "
				+ "삼국시대부터 이어져온 우리의 계산도구, 주판과 산가지로 오늘 과학관에 오기까지 사용했던 용돈들을 "
				+ "어떻게 계산하면 좋을지 한 번 생각해보세요. 주판과 산가지를 만든 선조들의 지혜가 더 가깝게 느껴질 것입니다.",
			thumbnailUrl: "<c:url value="/resources/img/display/mainBuilding/traditionalSciences/story08_02.jpg"/>",
			stories: [
				{
					storyLabel: "봉수대",
					storyDesc: "지금은 찾아 볼 수 없지만, 생활 적재적소에 이용되며 선조들의 삶의 질을 높였던 과학 결과물들도 많이 있습니다. "
						+ "이제는 사극이나 영화에서밖에 볼 수 없는 봉수대가 보입니다. 위험의 순간, 신속하게 상황을 전달하기 위해 만들어진 "
						+ "봉수대를 이 곳 전통과학관에서 간접적으로 체험해볼 수 있습니다. 당시 수원화성에 위치했던 봉수대를 모형으로 제작한 "
						+ "이 체험물은 구조, 형태, 당시 봉화를 올렸던 방법과 횟수까지 상세하게 체험해볼 수 있으니 놓치지 말고 경험해보세요."
						+ "“이게 뭐예요?”\n"
						+ "“꼭 악기처럼 생겼어요. 흔들면 소리가 나나요?”\n\n"
						+ "주판 앞에 서있는 어린 학생들의 질문이 재미있네요. 그러고 보니 지금은 나이가 아주 많은 어르신들이 "
						+ "사용하는 것 말고는 찾아보기 힘든 주판은 어린 학생들이 보기엔 정말 신기한 물건 일 것 같네요. "
						+ "삼국시대부터 이어져온 우리의 계산도구, 주판과 산가지로 오늘 과학관에 오기까지 사용했던 용돈들을 "
						+ "어떻게 계산하면 좋을지 한 번 생각해보세요. 주판과 산가지를 만든 선조들의 지혜가 더 가깝게 느껴질 것입니다.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/traditionalSciences/story08_02.jpg"/>"
				}
				
			]
		},
		{
			title: "우주의 원리로 인간을 읽다 [사람의 과학]",
			desc: "이제부터는 조금 편안하고 가벼운 마음으로 함께 해주시면 좋겠네요. "
				+ "자연과 우주의 섭리 속에서 건강한 몸의 비결을 찾았던 한의학에 대해 알아볼 것인데요. "
				+ "건강한 마음에서 건강한 몸이 시작되는 것은 알고 계시죠? " 
				+ "자 그럼 건강한 마음을 가지고 건강한 몸을 연구하는 한의학의 세계로 떠나보겠습니다.\n\n" 
				+ "태음인, 태양인, 소음인, 소양인! 체질에 따라 네 가지로 나눈 사상의학은 한 번쯤은 들어보셨죠?" 
				+ "저는 하체가 발달하고 땀이 적은 소음인인데요. 아직 자신이 어떤 체질인지 모르셨던 분들도 걱정하지 마세요. "
				+ "전통체험관에서 간단한 문항을 통해 여러분의 체질을 예측해볼 수 있답니다.",
			thumbnailUrl: "<c:url value="/resources/img/display/mainBuilding/traditionalSciences/story08_03.jpg"/>",
			headerDesc: "이제부터는 조금 편안하고 가벼운 마음으로 함께 해주시면 좋겠네요. "
				+ "자연과 우주의 섭리 속에서 건강한 몸의 비결을 찾았던 한의학에 대해 알아볼 것인데요. "
				+ "건강한 마음에서 건강한 몸이 시작되는 것은 알고 계시죠? " 
				+ "자 그럼 건강한 마음을 가지고 건강한 몸을 연구하는 한의학의 세계로 떠나보겠습니다.\n\n" 
				+ "태음인, 태양인, 소음인, 소양인! 체질에 따라 네 가지로 나눈 사상의학은 한 번쯤은 들어보셨죠?" 
				+ "저는 하체가 발달하고 땀이 적은 소음인인데요. 아직 자신이 어떤 체질인지 모르셨던 분들도 걱정하지 마세요. "
				+ "전통체험관에서 간단한 문항을 통해 여러분의 체질을 예측해볼 수 있답니다.",
			stories: [
				{
					storyLabel: "맥파분석체험",
					storyDesc: "우리의 전통 의학은 지금도 회자가 될 만큼 그 깊이가 깊고, 현대 의학에서도 활용 가능한 "
							+ "치료법들이 많이 있습니다. 전통과학관에서는 이렇게 신비한 한의학을 조금 더 쉽고 가깝게 " 
							+ "느낄 수 있습니다. 약재의 냄새를 맡아볼 수 도 있고, 1분간 오른손 검지에 센서를 부착해서 혈관 "
							+ "나이와 나의 건강상태를 살펴보는 맥파분석체험을 경험할 수 있습니다. 또 목소리로 나의 체질을 테스트해보는 "
							+ "음성 체질 진단기도 있어 재미를 더합니다. 아까부터 열심히 테스트를 해보고 있는 한 어린이가 있어 이야기를 잠시 들어보겠습니다.\n\n"
							+ "“저는 소양인이 나왔어요! 친구는 태음인이 나왔고요. 우리가 친구인데 왜 이렇게 다른 " 
							+ "것이 많은지 오늘 알 수 있게 되었어요. 또 어떻게 하면 친구를 잘 이해할 수 있을지 알게 되어서 기뻐요.”\n\n" 
							+ "우주를 통해 나를 알고, 자연 속 인간의 섭리를 이해함으로써 건강한 나를 지켜갈 수 있는 방법을 알려주었던 한의학의 세계! "
							+ "흥미롭게 한의학의 역사와 깊이를 보여주는 전시를 통해 건강한 일상을 찾아보시기 바랍니다.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/traditionalSciences/story08_03.jpg"/>"
				}
			]
		},
		{
			title: "하나를 알면 열을 알게 되는 [생활과학]",
			desc: "과학은 먼 곳이 아니라 우리의 삶 속에 있다는 가장 평범한 진리를 우리 전통 과학 속에서도 "
				+ "쉽게 찾을 수 있습니다. 아직도 이어져오고 있는 생활 속 조상들의 과학 기술과 지식이 무엇이 있는지 둘러보도록 하겠습니다."
				+ "귓가에 들려오는 종소리를 따라 저는 지금 성덕대왕 신종 모형으로 만들어진 전시물 앞에 서 있습니다. "
				+ "성덕대왕 신종은 우리나라에서 가장 큰 종이자 2톤 가량의 구리로 34년간 만든 종이라고 하는데요. "
				+ "이 곳을 둘러보니 이렇게 아름다운 종소리를 멀리까지 오랫동안 들려주는 신종의 비밀이 더 궁금해집니다. "
				+ "성덕대왕 신종은 소리를 모아주고 키워주는 장치와 잡음을 줄여주는 장치가 종 내부에 만들어져 있고, "
				+ "진동수가 다른 진동파를 이용한 멱놀이 현상이 적용된 과학 기술의 집결체였기에 이런 아름다운 종소리가 가능했다고 하네요. "
				+ "평소 그냥 역사 속 의미 있는 종이라고만 알았던 성덕대왕 신종이 이렇게 오랫동안 사람들에게 기억되고 있는 이유를 "
				+ "과학적 사실 속에서 찾을 수 있었습니다. 이 것이 과학관을 찾는 또 다른 재미가 아닐까 생각합니다.",
			thumbnailUrl: "<c:url value="/resources/img/display/mainBuilding/traditionalSciences/story08_04.jpg"/>",
			headerDesc: "과학은 먼 곳이 아니라 우리의 삶 속에 있다는 가장 평범한 진리를 우리 전통 과학 속에서도 "
					+ "쉽게 찾을 수 있습니다. 아직도 이어져오고 있는 생활 속 조상들의 과학 기술과 지식이 무엇이 있는지 둘러보도록 하겠습니다.",
			stories: [
				{
					storyLabel: "성덕대왕 신종 모형",
					storyDesc: "귓가에 들려오는 종소리를 따라 저는 지금 성덕대왕 신종 모형으로 만들어진 전시물 앞에 서 있습니다. "
							+ "성덕대왕 신종은 우리나라에서 가장 큰 종이자 2톤 가량의 구리로 34년간 만든 종이라고 하는데요. "
							+ "이 곳을 둘러보니 이렇게 아름다운 종소리를 멀리까지 오랫동안 들려주는 신종의 비밀이 더 궁금해집니다. "
							+ "성덕대왕 신종은 소리를 모아주고 키워주는 장치와 잡음을 줄여주는 장치가 종 내부에 만들어져 있고, "
							+ "진동수가 다른 진동파를 이용한 멱놀이 현상이 적용된 과학 기술의 집결체였기에 이런 아름다운 종소리가 가능했다고 하네요. "
							+ "평소 그냥 역사 속 의미 있는 종이라고만 알았던 성덕대왕 신종이 이렇게 오랫동안 사람들에게 기억되고 있는 이유를 "
							+ "과학적 사실 속에서 찾을 수 있었습니다. 이 것이 과학관을 찾는 또 다른 재미가 아닐까 생각합니다.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/traditionalSciences/story08_04.jpg"/>"
				},
				{
					storyLabel: "선조들의 주거 환경",
					storyDesc: "신종을 나와 둘러보면 선조들의 주거 환경을 재연한 전시물이 있습니다. "
							+ "선조들이 삶의 지혜를 가득 담아 만들어 살았던 집 구석구석을 살펴보고 나면 커다란 항아리들이 보입니다. "
							+ "고추장, 된장, 김치까지- 우리 음식에 없어서는 안될 재료들이 만들어지는 곳이죠. 우리가 매일 먹고 있는 "
							+ "발효식품들이 어떤 원리로 만들어지는지 자세하게 살펴볼 수 있는 전시가 되겠네요. 여기저기 숨어져 있는 발효에 "
							+ "대한 지식과 정보들을 놓치지 말고 찾아보세요! 오늘부터 먹는 음식들이 더욱 건강하게 느껴질 것입니다.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/traditionalSciences/story08_05.jpg"/>"
				}
			]
		},
		{
			title: "세계를 놀라게 한 아이디어",
			desc: "전통과학관을 둘러보다 보면, 그 동안 서양의 과학 기술을 한 발 늦게 답습해 온 줄만 알았던 " 
				+ "우리 과학기술에 대한 자부심이 높아지는 것 같습니다. 지금부터 보게 될 전시들이 특히 더 " 
				+ "큰 자부심을 선물해주는 전시들인데요.",
			thumbnailUrl: "<c:url value="/resources/img/display/mainBuilding/traditionalSciences/story08_06.jpg"/>",
			headerDesc: "전통과학관을 둘러보다 보면, 그 동안 서양의 과학 기술을 한 발 늦게 답습해 온 줄만 알았던 " 
				+ "우리 과학기술에 대한 자부심이 높아지는 것 같습니다. 지금부터 보게 될 전시들이 특히 더 " 
				+ "큰 자부심을 선물해주는 전시들인데요.",
			stories: [
				{
					storyLabel: "거중기",
					storyDesc: "여기 무거운 물건을 들어올리는데 이용되었던 거중기가 보입니다. "
							+ "이 거중기는 화성을 세울 때 건축 기간을 무려 10년에서 2.5년으로 단축시켰던 조선시대 정약용 "
							+ "선생이 개발한 거중기 를 만든 전시물입니다. 조선시대 과학 발전의 중요한 역할을 했던 거중기를 "
							+ "이렇게 실제로 보니 당시 기술로 어떻게 이렇게 정교한 기기를 만들어 냈는지 너무나 놀라울 따름입니다. "
							+ "그리고 조금 더 둘러보면, 세계 최초의 인공온실도 보입니다. 독일 하이델베르크에 있는 온실보다 "
							+ "무려 240년이나 앞섰던 것이라고 하니 자랑스러운 맘에 괜히 어깨가 펴지는 것 같네요.\n\n"  
							+ "디지털 한지 만들기 등의 체험공간을 지나면 거대한 비행선이 눈 앞에 보입니다. 20세기 초 "
							+ "서양의 비행기보다 300년이나 앞섰던 우리나라의 비행선 ‘비거’ 입니다. 가죽주머니에 압축공기를 넣고, " 
							+ "그 공기가 빠져나올 때 하늘을 날게 되는 구조로 설계되었다고 합니다. 오늘 관람을 통해 우리가 서양보다 한 발 "
							+ "앞서 비행기의 기본을 만들었다는 정말 자랑스러운 사실을 알게 되었네요.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/traditionalSciences/story08_06.jpg"/>"
				},
				{
					storyLabel: "이순신 장군의 전투",
					storyDesc: "전통과학관을 들어오기 전에 꼭 만나 뵙고 싶었던 이순신 장군의 전투도 전통과학관에서 "
							+ "만날 수 있습니다. 당시 전투에 이용되었던 과학 기술과 무기들을 볼 수 있는 전시가 다양한 "
							+ "체험들과 함께 마련되어 있습니다. 거북선 조종과 학익진 전법을 체험을 하고 나면 당신 "
							+ "전투 현장에 가있는 듯한 기분이 들기도 합니다. 이순신 장군의 업적을 100인치 스크린에 "
							+ "영상 시물레이터를 통해서도 확인할 수 있다니 평소 이순신 장군을 존경했던 관람객들이라면 꼭 들러봐야겠네요.\n\n" 
							+ "섬세한 손길로 만든 다양한 금속들과 금속 제작에 필수 장비인 풀무까지 그 "
							+ "실물과 정보를 접하고 나니 다시 한번 지혜로운 선조의 아이디어에 감탄 할 수 밖에 없습니다.\n\n"
							+ "그 동안 몰랐던 한국 전통 과학의 유구한 역사와 놀라운 잠재력을 만날 수 있는 시간이었습니다. "
							+ "건강하고 느린 삶, 자연과 더불어 사는 킨포크 인생을 지향하는 현대인들에게 전통과학관에서 접했던 선조들의 "
							+ "지혜는 더욱 의미가 있는 것 같은데요. 이 곳에서 인상적이었던 지혜들을 나만의 아이디어에 적용해 보는 것도 "
							+ "무척 의미 있는 일이 될 것 같습니다. 여러분도 잠시 눈을 감고 전통 과학이 숨쉬는 역사 속으로 여행을 떠나 보세요! ",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/traditionalSciences/story08_07.jpg"/>"
				}
			]
		}
	];
	
	var storySource   = $("#story-template").html();
	var storyTemplate = Handlebars.compile(storySource);
	var storyHtml = storyTemplate({stories: stories});
	$("#stories").html(storyHtml);
</script>

<c:url var="shareUrl" value="/display/mainBuilding/traditionalSciences" />
<c:import url="/WEB-INF/views/display/displayDetailModal.jsp">
	<c:param name="shareUrl" value="${shareUrl }"/>
	<c:param name="exhibitionName" value="전통과학관" />
</c:import>


<script type="text/javascript">

	$(document).ready( function() {
		// 주요 전시물 클릭시 show hide event
		//showHideDetails();
		
		// image slider
		PTechSlider('#image-slider').autoSlide();
		
		// 전시관 지도 flash 2층으로 전환.
		//setDefaultSVGMap('#basic-map-spy',2);
		// 지도 flash 해당 전시관 표시
		//$('#XMLID_159_').attr('class', 'active');
	});
	
	function showHideDetails()  {
		// hide all details first.
		$('.exhibition-detail-wrapper').hide();
		// show the active detail only.
		var exhibit_active_id = $('.major-exhibition-wrapper.active').data('id');
		$(exhibit_active_id).show();
		
		// click event for major exhibition box
		$('.major-exhibition-wrapper').on('click', function() {
			
			// get current active div & remove active
			var hide_id = $('.major-exhibition-wrapper.active').data('id');
			$('.major-exhibition-wrapper.active').removeClass('active');
			// hide the matching detail
			$(hide_id).hide();
			
			// add 'active' to the current clicked div
			var show_id = $(this).data('id');
			$(this).addClass('active');
			// show the mathcing detail
			$(show_id).show();
			
		});
	}
	
</script>


<!-- Major Exhibition Contents Loading -->
<c:import url="/WEB-INF/views/display/majorExhibitionTemplate.jsp" />
<script type="text/javascript">
	var exhibitions = [
		{
			descriptions : [
				
				{
					title: "하늘의 과학, 천문학의 우수성을 느끼다",
					desc : "첨성대, 앙부일구, 자격루, 천상열차분야지도를 포함해 전통시대 우리나라 천문학의 "
						+ "우수성을 느낄 수 있는 전시물들이 가득하다. 동서양 별자리 전시를 통해 우리나라 "
						+ "별자리와 서양 별자리의 차이를 배울 수 있고, 앙부일구를 통해 시간과 절기를 동시에 정확히 "
						+ "측정한 조상의 슬기로움도 엿볼 수 있다. 천상분야열차지도는 1395년 제작되어 세계 두 번째로 "
						+ "오래된 천문도로 282개의 별자리와 1,467개의 별이 기록되어 있다."
				},
				{
					title: "사람의 과학, 한의학의 과학성을 배우다",
					desc : "‘사상체질진단’에서는 몸의 체질을 4가지 유형으로 나누어 병을 진단, "
						+ "치료하는 사상의학에 따라 직접 자신의 체질을 진단해 볼 수 있다." 
						+ "1분간 오른손 검지에 센서를 부착해 맥파를 검출하여 혈관 나이를 측정하는 ‘맥파체험’도 있다. "
						+ "이를 통해 한의학의 원리를 체험해 보고 한의학의 과학성을 배울 수 있다. "
						+ "<div class='additional-info'>(5명/회, 중학생 이상)</div>"
				}
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/mainBuilding/traditionalSciences/major_exhibition_01.png' />",
					caption: "[동서양 별자리]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/mainBuilding/traditionalSciences/major_exhibition_04.png' />",
					caption: "[맥파 체험]"
				},
			]
		},
		{
			descriptions : [
				{
					title : "땅의 과학, 정밀함을 자랑하다",
					desc : "1861년 김정호가 제작한 4m×7m 크기의 22첩 지도인 대동여지도는 "
						+ "현대의 위성지도와 비교해도 그 정밀도가 결코 떨어지지 않는다. 이외에도 "
						+ "자동 거리측정기인 기리고차, 전통시대의 전국 통신네트워크인 봉수대도 볼 수 있다."
				},
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/mainBuilding/traditionalSciences/major_exhibition_02.png' />",
					caption: "[땅의 과학]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/mainBuilding/traditionalSciences/major_exhibition_03.png' />",
					caption: "[대동여지도]"
				}
			]
		},
		{
			descriptions : [
				{
					title : "생활과학, 의식주 속의 지혜를 찾다",
					desc : "전통 옷감과 천연 염색을 통해 의복 속의 과학 지혜, 김치를 비롯한 발효음식에 숨은 과학의 원리, "
						+ "한옥과 거중기를 통한 주거 속 과학 기술을 찾아볼 수 있다. 최초의 바닥 난방 방식인 온돌, "
						+ "나무끼리 잇는 결구법, 황토벽 등 친환경적인 건축 과학의 산물인 한옥을 관람하며 현대의 친환경 "
						+ "건축과학자를 꿈꿔보거나, 김치 속 발효 원리를 응용하여 개발된 김치냉장고의 과학 원리를 이해해 볼 수 있다. "
				},
				{
					title : "응용과학, 곳곳에 숨은 전통과학을 발견하다",
					desc : "디지털 한지 만들기, 한글 체험, 동서양 노젓기, 거북선과 학익진 체험, "
						+ "뱃전 만들기와 같은 다양한 체험전시물을 통해 우리 생활 곳곳에 숨어 있는 전통 "
						+ "응용과학을 발견하고 즐길 수 있다. 세계 최초 돌격용 장갑선인 거북선의 3D 시뮬레이터를 통해서 "
						+ "거북선 내부 구조와 조선 기술을 살펴볼 수 있다."
				},
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/mainBuilding/traditionalSciences/major_exhibition_05.png' />",
					caption: "[동서양 노젓기]"
				}
			]
		}
	];
	
	var exhibitionSource   = $("#major-exhibition-template").html();
	var exhibitionTemplate = Handlebars.compile(exhibitionSource);
	var exhibitionHtml = exhibitionTemplate({exhibitions: exhibitions});
	$("#main-exhibitions").html(exhibitionHtml);
	
</script>