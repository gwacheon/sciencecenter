<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-teal">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
		<%-- <nav id="scrollspy-nav" class="navbar-default navbar-static has-scroll">
			<div class="container">
				<ul class="nav navbar-nav">
					<c:import url="/WEB-INF/views/display/navbar.jsp"/>
					<li>
						<a href="#basic-info-spy" class="item">
							기본 안내 
						</a>
					</li>
					<li>
						<a href="#basic-map-spy" class="item">
							첨단기술관2 안내
						</a>
					</li>
					<li>
						<a href="#major-exhibitions-spy" class="item">
							주요전시물
						</a>
					</li>
					<li>
						<a href="#story-spy" class="item">
							스토리 여행기
						</a>
					</li>		
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<div class="bread-crumbs">
							전시관 / 첨단기술관2
						</div>
					</li>
				</ul>
			</div>
		</nav> --%>
	</div>
	<div id="display-body" class="sub-body">
		<div class="narrow-sub-top display">
			<div class="sub-banner-tab-wrapper sub-banner-tab-teal">
				<!-- <div class="container">
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
							<ul class="nav nav-tabs">
								<li class="col-xs-12 active">
									<a href="http://cyber.scientorium.go.kr/museum_2015/vrLoad.jsp?pavilion=indoor&xml_name=ce2_002.xml&angle=-90" class="item">
										<i class="fa fa-desktop fa-lg" aria-hidden="true"></i> 사이버 전시관
									</a>
								</li>
							</ul>   
						</div>
					</div>
				</div> -->
			</div>			
		</div>
		<div class="container">
			<div id="basic-map-spy" class="section scrollspy">
				<div class="row">
					<div class="col-sm-4 col-md-3 col-lg-3">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							첨단기술관2 안내					
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-md-4 col-lg-4">
						<ul class="circle-list teal">
							<li>
								주요전시물
								<ul>
									<li>
										<b>「첨단기술2관」</b>은 우리 생활 속에서 사용되어 온 과학의 진보된 기술을 
										전시한 공간이다. 항공, 우주, ICT, 소재 분야의 총 130건의 전시물로 
										구성되어 있다. 이 곳에서는 ‘우주여행극장’, ‘자이로스코프’, ‘월면점프’, ‘유인조종장치’등 
										53건의 체험물을 이용할 수 있다. 다양한 분야를 다루는 만큼 한 눈 팔지 말고 
										곳곳에 설치된 전시물을 살펴보도록 하자.
									</li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="col-sm-12 col-md-8 col-lg-8 text-right">
						<!-- select dropdown : 다른관으로 이동 -->
						<c:import url="/WEB-INF/views/display/displayHallSelect.jsp"/>
						<div>
							<img alt="첨단기술관2" src="<c:url value='/resources/img/display/map/05.png'/>"
								class="img-responsive margin-top20">
						</div>
					</div>	
				</div>
			</div>
			<div id="major-exhibitions-spy" class="section scrollspy">
				<div class="row">
					<div class="col-sm-3 col-md-2 col-lg-2">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							주요전시물
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="main-exhibition-wrapper">
							<div id="main-exhibitions">
							</div>
						</div>
					</div>
				</div>
				<div id="story-spy" class="story-telling-wrapper">
					<div class="header">
						<div class="row">
							<div class="col-md-6">
								<h3 class="page-title teal">
									<div class="top-line"></div>
									스토리로 읽어 보는 첨단기술관2 여행기
								</h3>
							</div>
						</div>
					</div>
					<div class="description">
						우주인을 꿈꾸는 사람들, 미래 생활을 디자인하고 싶은 사람들이라면 첨단기술관 2를 
						놓치지 마세요! 첨단기술관 2에서는 상상으로만 그려봤던 우주인의 생활과 미래 도시인의 삶을 체험해볼 수 있으니까요. 
						자, 그럼 잊을 수 없는 경험을 선물할 첨단기술관 2로 출발하겠습니다.
					</div>
					
					<div id="stories" class="row">
					</div>
				</div>
			</div>
			<div class="related-photos">
				<div class="title">
					<i class="color-darkgreen material-icons">&#xE413;</i>   관련사진
				</div>
				<div id="image-slider">
					<div id="image-slider-window">
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_01.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_01.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_02.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_02.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_03.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_03.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_04.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_04.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_05.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_05.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_06.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_06.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_07.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_07.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_08.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_08.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_09.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_09.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_10.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_10.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_11.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_11.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_12.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_12.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_13.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_13.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_14.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_14.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_15.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_15.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_16.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_16.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_17.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_17.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_18.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_18.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_19.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_19.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_20.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_20.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_21.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_21.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_22.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_22.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_23.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_23.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_24.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_24.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_25.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_25.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_26.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_26.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_27.png"/>" 
							data-url="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_27.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_28.png"/>"
							data-url="/resources/img/display/mainBuilding/advancedTechnology2/related_photo_28.png" class="slide"/>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script id="story-template" type="text/x-handlebars-template">
{{#each stories}}
<div class="col-xs-12 col-sm-6 col-md-6">
	<div class="display-card story-telling story-more-btn pointer" data-idx="{{@index}}" onclick="">
		<div class="card-image">
			<img src="{{thumbnailUrl}}" class="img-responsive"/>
		</div>
		<div class="card-content">
			<div class="display-card-title">
				{{title}}
			</div>
			<div class="display-card-content">
				{{{desc}}}
			</div>
			
		</div>
	</div>							
</div>
{{/each}}
</script>

<script type="text/javascript">
	var stories = [
		{
			title: "미래 생활의 주인공이 되다[ICT]",
			desc: "이 곳에서는 수증기가 스크린이 되기도 하죠. 수증기에 반사된 영상을 손으로 만져보면, "
				+ "영상이 다양한 모양으로 변하게 됩니다. 수증기에 손을 가져다 대는 사람들 모두 "
				+ "안개 속에 펼쳐진 영화 속 주인공이 된 듯한 기분을 느끼고 있네요.",
			thumbnailUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/story03_02.jpg"/>",
			stories: [
				{
					storyLabel: "ICT 융합 체험존",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/story03_01.jpg"/>"
				},
				{
					storyLabel: "수증기 스크린 홀로그램",
					storyDesc: "이 곳에서는 수증기가 스크린이 되기도 하죠. 수증기에 반사된 영상을 손으로 만져보면, "
						+ "영상이 다양한 모양으로 변하게 됩니다. 수증기에 손을 가져다 대는 사람들 모두 "
						+ "안개 속에 펼쳐진 영화 속 주인공이 된 듯한 기분을 느끼고 있네요.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/story03_02.jpg"/>"
				},
				{
					storyLabel: "투명 스크린 홀로그램",
					storyDesc: "투명스크린 홀로그램도 인기가 많은 전시물입니다. "
						+ "마치 관람객과 영상이 함께 있는 듯 합성해서 보여주는 투명스크린 홀로그램은 "
						+ "특히 호기심이 많은 아이들에게 인기가 많습니다.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/story03_03.jpg"/>"
				},
				{
					storyLabel: "나만의 자동차와 도시",
					storyDesc: "이제, 상상하는 대로 나만의 자동차와 도시를 만들어 볼 수 있는 시간입니다. "
						+ "첨단기술관 2에서는 내가 원하는 색깔로 자동차를 칠하고 머리로만 그려왔던 도시를 "
						+ "증강현실로 건설해볼 수도 있습니다. 관람객마다 개성 따라 다양하고 멋진 도시와 자동차가 완성되고 있네요.", 
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/story03_04.jpg"/>"
				},
				{
					storyLabel: "스마트봇 홀로그램",
					storyDesc: "나의 일상을 도와주는 로봇이 있다면 삶이 훨씬 스마트 해지지 않을까요? 여러분의 상상이 " 
						+ "곧 현실이 됩니다. 바로, 스마트봇 홀로그램이 있으니까요. 나의 손가락 위치와 움직임에 따라 " 
						+ "스마트봇 각 부분의 기능을 살펴볼 수 있답니다.\n\n"
						+ "다음 기술은 어떤 것이 될까요? 각자 다음 미래의 기술을 상상해보세요. "
						+ "그리고 가까운 미래에, 첨단기술관 2의 한 곳을 채울 기술을 여러분 스스로 만들어보세요. 벌써부터 기대되네요.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/story03_05.jpg"/>"
				}
			]
		},
		{
			title: "디테일로 패러다임을 바꾸다[기계소재]",
			desc: "“철도 위를 떠서 달리는 이 기차는 뭐예요? 칙칙폭폭 소리도 안 나고 엄청 빨라요.” \n\n"
				+ "미래에는 소재 하나에도 다양한 기술이 담겨 있게 됩니다. 여기 땅에 닿지 않고 달리는 자기부상 열차가 있습니다. "
				+ "첨단기술관 2에는 자기부상열차를 모형으로 볼 수 있는 전시가 마련되어 있는대요. "
				+ "자기의 힘으로 열차를 철로 위로 띄어서 빠르고 안정적으로 기차를 움직일 수 있게 하는 자기부상열차! "
				+ "비용도 줄이고, 승차감도 개선할 수 있다니 우리 생활 속에서 볼 날이 무척 기다려집니다. "
				+ "몇 년 후 명절에는 자기부상열차를 타고 할머니, 할아버지를 만나러 갈 수 있다면 정말 좋겠네요. "
				+ "더 빠르고 안전하게, 보고 싶은 가족들을 만날 수 있을테니까요.\n\n"
				+ "이 밖에도 " 
				+ "손으로만 만져졌던 소재의 한계를 벗어난 다양하고 신기한 기계 소재들을 만날 수 있습니다." 
				+ "뉴스에서만 접했던 다양한 기술과 소재들을 친구들과 함께 만지고 경험하며 그 매력에 빠져 보세요.",
			thumbnailUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/story03_06.jpg"/>",
			stories: [
				{
					storyLabel: "자기부상열차",
					storyDesc: "“철도 위를 떠서 달리는 이 기차는 뭐예요? 칙칙폭폭 소리도 안 나고 엄청 빨라요.” \n\n"
						+ "미래에는 소재 하나에도 다양한 기술이 담겨 있게 됩니다. 여기 땅에 닿지 않고 달리는 자기부상 열차가 있습니다. "
						+ "첨단기술관 2에는 자기부상열차를 모형으로 볼 수 있는 전시가 마련되어 있는대요. "
						+ "자기의 힘으로 열차를 철로 위로 띄어서 빠르고 안정적으로 기차를 움직일 수 있게 하는 자기부상열차! "
						+ "비용도 줄이고, 승차감도 개선할 수 있다니 우리 생활 속에서 볼 날이 무척 기다려집니다. "
						+ "몇 년 후 명절에는 자기부상열차를 타고 할머니, 할아버지를 만나러 갈 수 있다면 정말 좋겠네요. "
						+ "더 빠르고 안전하게, 보고 싶은 가족들을 만날 수 있을테니까요.\n\n"
						+ "이 밖에도 디지털로 구현되는 휘어지는 전자 종이, 형상기억합금, 압전소자, 색변환 섬유 등 " 
						+ "손으로만 만져졌던 소재의 한계를 벗어난 다양하고 신기한 기계 소재들을 만날 수 있습니다." 
						+ "뉴스에서만 접했던 다양한 기술과 소재들을 친구들과 함께 만지고 경험하며 그 매력에 빠져 보세요.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/story03_06.jpg"/>"
				},
				{
					storyLabel: "우주헬멧",
					storyDesc: "이제, 우주를 여행할 시간입니다. 우주인이 될 준비가 되셨나요?" 
						+ "상상력과 창의력, 우주에 대한 호기심을 가득 장착해주세요. 그러면 이제 출발합니다. 다함께 우주로 떠나보겠습니다.\n\n"
						+ "우주에서는 어떤 옷을 입고, 어떤 음식을 먹을까요? "
						+ "또, 화장실은 어떻게 가고, 어디에서 잠을 잘까요? "
						+ "첨단기술관 2에 모든 정답이 숨겨져 있습니다. "
						+ "이 곳에서는 우주인들의 눈을 보호해주고, 착용상태에서 식사와 통신이 가능한 우주헬멧을 "
						+ "체험해볼 수 있는 곳이 있습니다. 우주헬멧을 직접 쓰고 우주인이 된 기분을 느껴보세요!",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/story03_07.jpg"/>"
				},
				{
					storyLabel: "스페이스 캠프",
					storyDesc: "우주 영화를 보면 우주에서 몸이 무중력상태로 자유롭게 움직이는 것을 볼 수 있는데요. "
						+ "궁금했던 무중력 상태를 체험해볼 수 있는 공간도 마련되어 있답니다. 방향감과 무게감을 "
						+ "상실하게 되는 우주 공간! 우주인들의 훈련장치인 무중력 체험공간인 스페이스 캠프에서 "
						+ "나의 우주인 능력을 시험해보세요!",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/story03_08.jpg"/>"
				},
				{
					storyLabel: "월면 점프 코너",
					storyDesc: "매일 밤 보지만, 언제나 상상의 공간으로 머물러 있던 달! 달은 지구 중력의 6분의 1밖에 " 
						+ "안 된다는 사실은 알고 계시죠? 하지만 달의 중력이 정확히 어느 정도의 힘인지 느껴보고 싶다면 월면 점프 코너를 지나치지 마세요. "
						+ "달에 있다는 생각으로 점프를 해보면 적은 힘으로도 지구에서보다 높이 점프할 수 있답니다.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/story03_09.jpg"/>"
				},
				{
					storyLabel: "항공관제시스템",
					storyDesc: "이제, 우주에서 지구의 하늘로 가보겠습니다. 항공기가 가득한 이 곳을 하늘이라고 상상해보세요. "
						+ "여러 모형 항공기를 구경하다 보면 항공관제시스템을 체험해볼 수 있는 공간이 등장합니다. "
						+ "1일 관제사가 되어 비행기의 이착륙을 도우며 승객들의 안전한 여행을 책임져보세요.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/story03_10.jpg"/>"
				},
				{
					storyLabel: "항공기시뮬레이터",
					storyDesc: "이번에는 조종사가 될 시간입니다. 항공기의 이륙부터 비행, 착륙까지 모든 과정을 시뮬레이터로 "
						+ "체험해볼 수 있는 공간으로 하늘을 나는 듯한 기분을 느낄 수 있습니다. \n\n"
						+ "우주와 하늘을 날아오르고, 체험하며 어떤 꿈과 미래를 그려보셨나요? 그 동안 꿈으로만 가지고 있던 항공인과 우주인의 꿈을 "
						+ "이 곳에서 종이비행기에 담아 날려보세요. 간절했던 소원이 하늘높이, 우주까지 전해질 수 있을 것입니다. \n\n" 
						+ "첨단기술관 2는 누구에게나 꿈이 가득한 공간으로 기억될 것입니다. 공상과학 영화에서나 " 
						+ "볼 수 있었던 기술은 물론 미지의 세계로 남아있던 하늘과 우주까지 궁금했던 많은 부분들의 "
						+ "답을 얻을 수 있는 곳, 첨단기술관 2! 이 곳을 경험하는 것 만으로 여러분의 꿈도 커지게 될 것입니다.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/advancedTechnology2/story03_11.jpg"/>"
				}
				
				
			]
		}
	];
	
	var storySource   = $("#story-template").html();
	var storyTemplate = Handlebars.compile(storySource);
	var storyHtml = storyTemplate({stories: stories});
	$("#stories").html(storyHtml);
</script>

<c:url var="shareUrl" value="/display/mainBuilding/advancedTechnology2" />
<c:import url="/WEB-INF/views/display/displayDetailModal.jsp">
	<c:param name="shareUrl" value="${shareUrl }"/>
	<c:param name="exhibitionName" value="첨단기술관2" />
</c:import>



<script type="text/javascript">

	$(document).ready( function() {
		
		// 주요 전시물 클릭시 show hide event
		//showHideDetails();
		
		// image slider
		PTechSlider('#image-slider').autoSlide();
		
		// 전시관 Map 2층으로.
		//setDefaultSVGMap('#basic-map-spy',2);
		// 지도 flash 해당 전시관 표시
		//$("g[id='XMLID_155_']").attr('class', 'active');
	});
	
	function showHideDetails()  {
		// hide all details first.
		$('.exhibition-detail-wrapper').hide();
		// show the active detail only.
		var exhibit_active_id = $('.major-exhibition-wrapper.active').data('id');
		$(exhibit_active_id).show();
		
		// click event for major exhibition box
		$('.major-exhibition-wrapper').on('click', function() {
			
			// get current active div & remove active
			var hide_id = $('.major-exhibition-wrapper.active').data('id');
			$('.major-exhibition-wrapper.active').removeClass('active');
			// hide the matching detail
			$(hide_id).hide();
			
			// add 'active' to the current clicked div
			var show_id = $(this).data('id');
			$(this).addClass('active');
			// show the mathcing detail
			$(show_id).show();
			
		});
	}

</script>

<!-- Major Exhibition Contents Loading -->
<c:import url="/WEB-INF/views/display/majorExhibitionTemplate.jsp" />
<script type="text/javascript">
	var exhibitions = [
		{
			descriptions : [
				
				{
					title: "CCD와 LED, 생활 속 기술을 만나다",
					desc : "첨단기술2관을 들어가기에 앞서 엄청 큰 카메라와 스크린이 우리를 기다린다. "
						+ "일상에서 자주 사용하는 CCD 카메라와 LED 디스플레이의 원리를 설명하는 전시물이다. "
						+ "직접 모델이 되어 카메라 촬영을 하며 전기와 빛의 변환에 대해 배울 수 있다."
				},
				{
					title: "ICT융합체험존, 몸짓, 손짓으로 가상을 즐긴다",
					desc : "페인트 없이 자동차를 내 마음대로 색칠해 보기, 손대지 않고 물건을 조종해서 자동차 정비하기, "
					 	+ "가상의 댄서와 함께 무대의 주인공이 되어 춤 추기, 수증기 위에 비춰진 바다 속과 우주를 배경으로 뛰어놀기, "
					 	+ "TV 속 아바타로 스포츠 즐기기. 이것은 영화에 나오는 이야기가 아니다. 모두 ICT 융합체험존에서 직접 경험할 "
					 	+ "수 있는 전시물들이다. 이들 코너에서는 허공을 향해 손짓, 몸짓을 하는 친구들을 만나더라도 당황하지 말 것! "
					 	+ "그리고 함께 즐길 것!"
				}
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/mainBuilding/advancedTechnology2/major_exhibition_01.png' />",
					caption: "[LED 게임]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/mainBuilding/advancedTechnology2/major_exhibition_02.png' />",
					caption: "[수중기 스크린 홀로그램]"
				},
			]
		},
		{
			descriptions : [
				{
					title : "석탄을 입다, 옷에서 석유의 향기가 난다",
					desc : "옷은 무엇으로 만들까? 옷의 재료인 섬유는 목화나 누에, 양털을 통해 얻기도 하지만, "
						+ "현대에는 대부분 석유로부터 얻는다. 석유화학기술을 통해 고분자로 중합된 합성물질이어서 ‘합성섬유’라고 부른다. "
						+ "이들은 때론 비단처럼 부드럽고, 강철만큼 강하며, 솜털보다 가볍기도 하다. 만약 우리가 합성섬유를 만들지 못했다면 "
						+ "이 세상은 어떠했을까? 합성섬유의 대표적인 제품인 나일론을 개발한 미국의 화학자 캐러더스는 이렇게 "
						+ "바뀐 세상을 상상할 수 있었을까? 그는 불행히도 찬란한 미래를 보지 못한 채 세상을 떠났지만, 석탄과 석유로부터 "
						+ "만들어진 나일론은 인류 문명사에 가장 혁신적인 발명품으로 남았다. 고분자 합성물질이 실이 되는 과정을 실제 반응기를 통해 "
						+ "직접 눈으로, 손으로 확인해 보며 나일론 섬유를 발견하게 된 이야기, 석유 증류 공정, 미래의 섬유에 대해 알아보자."
				}
				
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/mainBuilding/advancedTechnology2/major_exhibition_03.png' />",
					caption: "[뜨개질]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/mainBuilding/advancedTechnology2/major_exhibition_04.png' />",
					caption: "[방사기]"
				}
			]
		},
		{
			descriptions : [
				{
					title : "우주 코너, 나도 우주인이 될 수 있다",
					desc : "우주인이 되려면 어떤 준비가 필요할까? 3명씩 짝을 이뤄 빙글빙글 돌아가는 "
						+ "‘자이로스코프’를 타면 우주에서의 무중력을 느껴볼 수 있고, 우주 왕복선에 장착된 ‘유인조종장치’를 "
						+ "이용하여 우주작업을 체험할 수 있다. 작은 힘으로 높이 뛸 수 있는 ‘월면점프’는 달 표면에서의 중력을 경험할 수도 있다. "
						+ "우주비행사 훈련에 사용되는 이들 장치를 스페이스캠프에서 모두 체험해 볼 수 있다. "
						+ "<br><div class='additional-info'>(8~9명/회, 초등 1학년 이상)</div>"
				},
				{
					title : "",
					desc : "‘우주여행극장’에서는 4D 영상 체험을 통해 우주인이 되어 볼 수 있다. "
						+ "국제우주정거장이 우주쓰레기와 충돌하는 위험한 상황을 해결하기 위해 로켓을 타고 "
						+ "우주로 나갔다 오는 과정을 실감나게 경험할 수 있다. " 
						+ "<br><div class='additional-info'>(30명/회, 초등 1학년 이상)</div>"
				},
				{
					title : "항공 코너, 비행의 원리를 체험으로 이해한다",
					desc : "무거운 비행기가 어떻게 공중에 뜰까? ‘비행기는 어떻게 나는가’에서 단계적으로 그 해답을 찾아볼 수 있다. "
						+ "날개 주위의 압력차와 공기 흐름을 눈으로 볼 수 있게 증강현실로 표현하여 물체의 이동 원리를 이해할 수 있다. "
						+ "또 라이트 형제가 비행기 날개를 만들기 위해 실험했던 풍동 장치로 어떤 날개 모양이 잘 날 수 있는지 탐구할 수 있다. "
						+ "그럼 이제 직접 비행기를 만들어 볼까? 종이비행기를 접어 날려보는 전시물은 어린이들에게는 꿈과 희망을, "
						+ "어른들에게는 어린 시절 추억을 불러일으킨다."
				},
				{
					title : "",
					desc : "비행 원리를 이해했다면 ‘항공기 시뮬레이터’로 비행기를 직접 조종해 보자. 자칫 한 눈 팔면 땅에 충돌할 수 있으니 주의해야 한다. "
						+ "<br><div class='additional-info'>(6명/회, 초등 1학년 이상)</div>"
						+ "<br>이번에는 관제탑에 들어가 조종사와 교신하며 여객기를 이착륙시켜 보자. 이 모든 체험들을 완수했다면 어느새 항공 전문가가 되어 있을 것이다."
				}
				
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/mainBuilding/advancedTechnology2/major_exhibition_05.png' />",
					caption: "[자이로스코프]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/mainBuilding/advancedTechnology2/major_exhibition_06.png' />",
					caption: "[항공기시뮬레이터]"
				}
			]
		}
	];
	
	var exhibitionSource   = $("#major-exhibition-template").html();
	var exhibitionTemplate = Handlebars.compile(exhibitionSource);
	var exhibitionHtml = exhibitionTemplate({exhibitions: exhibitions});
	$("#main-exhibitions").html(exhibitionHtml);
	
</script>