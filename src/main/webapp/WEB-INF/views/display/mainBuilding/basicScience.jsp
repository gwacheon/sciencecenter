<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-teal">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
		<%-- <nav id="scrollspy-nav" class="navbar-default navbar-static has-scroll">
			<div class="container">
				<ul class="nav navbar-nav">
					<c:import url="/WEB-INF/views/display/navbar.jsp"/>
					<li>
						<a href="#basic-info-spy" class="item">
							기본 안내 
						</a>				
					</li>
					<li>
						<a href="#basic-map-spy" class="item">
							기초과학관 안내
						</a>				
					</li>
					<li>
						<a href="#major-exhibitions-spy" class="item">
							주요 전시물
						</a>
					</li>
					<li>
						<a href="#story-spy" class="item">
							스토리 여행기
						</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<div class="bread-crumbs">
							전시관 / 기초과학관
						</div>
					</li>
				</ul>
			</div>
		</nav> --%>
	</div>

	<div id="display-body" class="sub-body">
		<div id="basic-info-spy" class="narrow-sub-top display">
			<div class="sub-banner-tab-wrapper sub-banner-tab-teal">
				<!-- <div class="container">
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
							<ul class="nav nav-tabs">
								<li class="col-xs-12 active">
									<a href="http://cyber.scientorium.go.kr/museum_2015/vrLoad.jsp?pavilion=indoor&xml_name=bas_002.xml&angle=180" class="item">
										<i class="fa fa-desktop fa-lg" aria-hidden="true"></i> 사이버 전시관
									</a>		
								</li>
							</ul>   
						</div>
					</div>
				</div> -->
			</div>
		</div>
		<div class="container">
			<div id="basic-map-spy" class="section scrollspy">
				<div class="row">
					<div class="col-md-12">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							기초과학관 안내
						</h3>
					</div>
				</div>
				<div class="row description">
					<div class="col-sm-12 col-md-4 col-lg-4">
						<ul class="circle-list teal">
							<li class="margin-bottom20">
								유의사항
								<ul>
									<li>대상: 초등학생 이상</li>
									<li>
										(초등학생 이상의 학생이 이해할 수 있도록 전시되어 있어 미취학 아동의 입장을 제한하오니 양해하여 주시기 바랍니다. 단, 보호자가 동반할 경우는 입장가능하며, 유치원 단체 관람은 불가)
									</li>	
								</ul>
							</li>
							
							<li>
								주요전시물
								<ul>
									<li>
										<b>「기초과학관(Basic Science Hall)」</b>은 자연 과학의 기초 원리와 이론을 눈과 귀, 
										손과 몸으로 배울 수 있는 공간이다. 물리·화학·생물·지구과학의 4개 코너에 총 110건의 
										전시물로 구성되어 있다. 이 중 체험물은 76건이 있으며 특히, ‘지진체험’, 
										‘태풍체험’, ‘흔들흔들 혈관탐험’, ‘물방울 여행’등이 인기가 있다. 이곳에서 다양한 과학 
										체험을 하며 우리 일상 가까이에 숨어있는 과학적 원리를 발견해 보자.  
									</li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="col-sm-12 col-md-8 col-lg-8 text-right">
						<!-- select dropdown : 다른관으로 이동 -->
						<c:import url="/WEB-INF/views/display/displayHallSelect.jsp"/>
						<div>
							<img alt="기초과학관" src="<c:url value='/resources/img/display/map/01.png'/>"
								class="img-responsive margin-top20">
						</div>
					</div>
				</div>
			</div>
			<div id="major-exhibitions-spy" class="section scrollspy">
				<div class="row">
					<div class="col-md-12">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							주요전시물
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="main-exhibition-wrapper">
							<div id="main-exhibitions">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id = "story-spy" class="story-telling-wrapper">
				<div class="header">
					<div class="row">
						<div class="col-md-12">
							<h3 class="page-title teal">
								<div class="top-line"></div>
								스토리로 읽어 보는 기초과학관 여행기
							</h3>
						</div>
					</div>
				</div>
				<div class="description">
					'과학'하면 어떤 생각이 떠오르시나요? 어렵고, 복잡하고, 나와는 먼 얘기라고만 생각하고 있지 않나요?
					하지만 과학은 바로, 우리의 '생활'이라고 할 수 있습니다. 내가 사는 세상은 물론
					내 몸으로 보고, 만지고, 듣고 느기는 모든 것이 과학이니까요. 믿기 힘들다고요? 
					그렇다면 지금부터 과학이 얼마나 우리 가까이에 있는지 얼마나 쉽고 재미있는지 확인하기 위해
					기초과학관으로 함께 떠나보겠습니다!
				</div>
				
				<div id="stories" class="row">
				</div>
			</div>
			<div class="related-photos">
				<div class="title">
					<i class="color-darkgreen material-icons">&#xE413;</i>    관련사진
				</div>
				<div id="image-slider">
					<div id="image-slider-window">
						<img src="<c:url value="/resources/img/display/mainBuilding/basicScience/related_photo_01.png"/>" 
							data-url="/resources/img/display/mainBuilding/basicScience/related_photo_01.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/basicScience/related_photo_02.png"/>" 
							data-url="/resources/img/display/mainBuilding/basicScience/related_photo_02.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/basicScience/related_photo_03.png"/>" 
							data-url="/resources/img/display/mainBuilding/basicScience/related_photo_03.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/basicScience/related_photo_04.png"/>" 
							data-url="/resources/img/display/mainBuilding/basicScience/related_photo_04.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/basicScience/related_photo_05.png"/>" 
							data-url="/resources/img/display/mainBuilding/basicScience/related_photo_05.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/basicScience/related_photo_06.png"/>" 
							data-url="/resources/img/display/mainBuilding/basicScience/related_photo_06.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/mainBuilding/basicScience/related_photo_07.png"/>" 
							data-url="/resources/img/display/mainBuilding/basicScience/related_photo_02.png" class="slide"/>			
					</div>
				</div><!--  image-slider END ... -->
			</div>
		</div>
	</div>
</div>


<script id="story-template" type="text/x-handlebars-template">
{{#each stories}}
<div class="col-xs-12 col-sm-6 col-md-2-5">
	<div class="display-card story-telling story-more-btn pointer" data-idx="{{@index}}" onclick="">
		<div class="card-image">
			<img src="{{thumbnailUrl}}" class="img-responsive"/>
		</div>
		<div class="card-content">
			<div class="display-card-title">
				{{title}}
			</div>
			<div class="display-card-content">
				{{desc}}
			</div>
			
		</div>
	</div>							
</div>
{{/each}}
</script>

<script type="text/javascript">
	var stories = [
		{
			title: "생활 속 사고의 깊이를 더하다 [물리]",
			desc: "제일 먼저, 우리의 생활 속 숨어있는 물리를 만나보겠습니다. 빛, 원자, 관성… 책으로만 접했던 과학 정보들이 이제 손에 잡히는 즐거운 지식으로 바뀌는 경험을 하게 될 것입니다."
				+"기초과학관의 물리 영역을 관람 하다 보면, 그 전시 크기에 압도되는 커다란 물건을 보게 됩니다. 이 커다란 물건은 바로 ‘테슬라코일!’ 테슬라가 에디슨과 동시대에 개발한 고주파 진동전류로 고전압을 발생시키는 변압기인데요!"
				+"나이아가라 폭포에 세워지는 세계 최초의 수력발전소가 이 교류방식을 채택하면서 유명해졌다고 하죠. 테슬라코일에 전류가 흐르게 되면 전시물 앞에 모든 사람들이 한 마음이 되어 “와~!!”하고 탄성을 지릅니다." 
				+"테슬라코일 주변에 전기에 대한 지식들을 읽고, 체험하다 보면 평소 아무런 생각 없이 사용했던 전기에 대해 많은 호기심과 의문들이 생겨나게 될 것입니다. 바로 그때부터 여러분도 과학을 향한 첫 발을 내 딛게 되는 것이고요.",
			thumbnailUrl: "<c:url value="/resources/img/display/mainBuilding/basicScience/story01_01.jpg"/>",
			stories: [
				{
					storyLabel: "테슬라코일",
					storyDesc: "제일 먼저, 우리의 생활 속 숨어있는 물리를 만나보겠습니다. 빛, 원자, 관성… 책으로만 접했던 과학 정보들이 이제 손에 잡히는 즐거운 지식으로 바뀌는 경험을 하게 될 것입니다."
						+"기초과학관의 물리 영역을 관람 하다 보면, 그 전시 크기에 압도되는 커다란 물건을 보게 됩니다. 이 커다란 물건은 바로 ‘테슬라코일!’ 테슬라가 에디슨과 동시대에 개발한 고주파 진동전류로 고전압을 발생시키는 변압기인데요!"
						+"나이아가라 폭포에 세워지는 세계 최초의 수력발전소가 이 교류방식을 채택하면서 유명해졌다고 하죠. 테슬라코일에 전류가 흐르게 되면 전시물 앞에 모든 사람들이 한 마음이 되어 “와~!!”하고 탄성을 지릅니다." 
						+"테슬라코일 주변에 전기에 대한 지식들을 읽고, 체험하다 보면 평소 아무런 생각 없이 사용했던 전기에 대해 많은 호기심과 의문들이 생겨나게 될 것입니다. 바로 그때부터 여러분도 과학을 향한 첫 발을 내 딛게 되는 것이고요.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/basicScience/story01_01.jpg"/>"
				},
				{
					storyLabel: "과학자가바꾼세상",
					storyDesc: "저는 지금 ‘과학자가 바꾼 세상!’ 전시물 앞에 서있습니다. 뉴턴, 아인슈타인…… 천재 과학자들이 만들어 놓은 업적들은 언제 봐도 신기합니다."
						+ "지금은 너무나 당연하게 알고 있는 지식들! 그들의 창의적인 생각이 세상의 상식을 만들었다는 생각을 하면 그저 가벼운 생각으로 지나칠 수 없는 전시물들입니다.", 

					imgUrl: "<c:url value="/resources/img/display/mainBuilding/basicScience/story01_02.jpeg"/>"
				},
				{
					storyLabel: "타코마다리붕괴",
					storyDesc: "자, 이번엔 두 개의 막대기가 눈 앞에 보입니다. 이 두 개의 막대기 안에 1940년, "
						+"미국에서 일어난 끔찍했던 사건의 비밀이 담겨 있다고 하는데요. 세상에서 가장 아름다운 다리이자, 세 번째로 길었던 타코마 다리 붕괴의 비밀이 여기에 숨겨져 있습니다. "
						+"타코마 다리는 바람이 불어 무너져버린 것으로 많이들 알고 있는데, 사실은 바람의 진동수에 그 원인이 있었습니다. 여러분도 이 곳에서 두 개의 막대기만으로 역사 속 현장으로 들어가, 진동의 무서운 힘을 느껴볼 수 있을 것입니다",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/basicScience/story01_03.jpg"/>"
				}
			]
		},
		{
			title: "나를 알면 과학도 백전백승 [생물]",
			desc: "나의 몸과 생태계! 가장 가까이에서 접하지만, 가장 소홀하게 지나칠 수 있는 것들을 오늘은 조금 자세히 살펴보는 시간을 갖게 됩니다. "
				+"나는 어떻게 태어나게 되었고, 식물은 어떻게 만들어지고, 자라나게 되는지 자세히 알 수 있는 곳입니다." 
				+"무엇보다 그 동안 당연하게 받아들였던 말하기, 보기, 숨쉬기 같은 것들을 새롭게 볼 수 있는 기회를 가질 수 있습니다. ‘말하기와 뇌의 혼란 체험’은 의식 없이 했던 말하기라는 "
				+"행위를 내가 말하고 듣는 시간을 조금 늦춰줌으로써 뇌의 신비함을 다시 한 번 느끼게 되는 체험입니다.",
			thumbnailUrl: "<c:url value="/resources/img/display/mainBuilding/basicScience/story01_05.jpg"/>",
			stories: [
				{
					storyLabel: "말하기와 뇌의 혼란 체험",
					storyDesc: "나의 몸과 생태계! 가장 가까이에서 접하지만, 가장 소홀하게 지나칠 수 있는 것들을 오늘은 조금 자세히 살펴보는 시간을 갖게 됩니다. "
						+"나는 어떻게 태어나게 되었고, 식물은 어떻게 만들어지고, 자라나게 되는지 자세히 알 수 있는 곳입니다." 
						+"무엇보다 그 동안 당연하게 받아들였던 말하기, 보기, 숨쉬기 같은 것들을 새롭게 볼 수 있는 기회를 가질 수 있습니다. ‘말하기와 뇌의 혼란 체험’은 의식 없이 했던 말하기라는 "
						+"행위를 내가 말하고 듣는 시간을 조금 늦춰줌으로써 뇌의 신비함을 다시 한 번 느끼게 되는 체험입니다.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/basicScience/story01_05.jpg"/>"
				},
				{
					storyLabel: "흔들흔들혈관탐험",
					storyDesc: "혈액도 평소 우리가 인식하지 못하고 잊고 사는 것이죠! 하지만 혈액도 우리의 삶에 너무나 중요한 것이랍니다. "
							+"과학의 발전으로 만들어진 수많은 발명품이 있지만, 우리의 혈액만큼은 어떤 과학 기술로도 만들 수 없다는 것을 보면, 혈액은 정말 너무나 신비로운 영역입니다." 
							+"자! 여기 우리의 혈관 속을 볼 수 있는 기회가 있습니다. 탐험선을 타고 흔들흔들 혈관 속을 구경하며, 혈액 속의 물질들과 혈관의 생김새를 살펴볼 수 있는 특별한 경험을 절대 놓치지 마세요!",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/basicScience/story01_06.jpg"/>"
				},
				{
					storyLabel: "가상헌혈체험",
					storyDesc: "헌혈이 무섭기만 하다고요. 그렇다면 가상 헌혈체험으로 무서움을 없애보세요. 혈액은 매일"
							+"새롭게 만들어지고, 우리의 피 한 방울로 소중한 생명을 살릴 수도 있으니까요.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/basicScience/story01_07.jpg"/>"
				},
				{
					storyLabel: "다양한체험",
					storyDesc: "기초과학관에서만 만날 수 있는 혈액에 대한 다양한 체험들은 아이, 어른 모두의 발길을 잡게 됩니다."
							+"누구나 어린이의 상상력과 호기심을 다시 갖게 하는 것, 이 것이 바로 과학의 힘이 아닐까요?",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/basicScience/story01_08.jpg"/>"
				}
			]
		},
		{
			title: "논리적으로 즐거워지는 시간[수학]",
			desc: "수학은 많은 학문의 기초가 되지만, 또 많은 사람들에게 어려운 학문이기도 합니다. 하지만 "
				+"오늘은 수학에 대한 선입견을 내려놓고 수학으로 놀아보겠다는 생각으로 함께 해주세요." 
				+"가벼운 수학 이야기부터, 수학을 기반으로 만들어진 회전체 제품까지 다양한 수학의 세계를 만날 준비가 되셨나요? 그럼 떠나보겠습니다!",
			thumbnailUrl: "<c:url value="/resources/img/display/mainBuilding/basicScience/story01_09.jpg"/>",
			stories: [
				{
					storyLabel: "내 얼굴에 수학공식",
					storyDesc: "수학은 많은 학문의 기초가 되지만, 또 많은 사람들에게 어려운 학문이기도 합니다. 하지만 "
							+"오늘은 수학에 대한 선입견을 내려놓고 수학으로 놀아보겠다는 생각으로 함께 해주세요." 
							+"가벼운 수학 이야기부터, 수학을 기반으로 만들어진 회전체 제품까지 다양한 수학의"
							+"세계를 만날 준비가 되셨나요? 그럼 떠나보겠습니다!\n"
							+"어린이들의 얼굴에 수학공식이 보이네요. 내 얼굴에 수학공식을 넣어 벽에 있는 화면으로" 
							+"그 모습을 확인할 수 있는 체험 공간이 있네요. 수학공식을 쉽고 즐겁게 체험하면서 수학과 친해지는 시간을 가질 수 있었습니다."
							+"평소 추리 소설이나 탐정 영화를 좋아한다고요? 그렇다면 ‘암호를 풀어라!’체험을 놓치지 마세요! 화면의 문제를 암호를 알아내서 "
							+"풀어보는 경험을 통해 기초과학관에서 체험했던 다양한 지식에 대한 확인도 할 수 있고, 암호를 푸는 재미를 쏠쏠하게 느낄 수 있으니까요!"
							+"수학이 없었더라면, 지금 우리가 즐기는 음악도, 사용하는 가전 제품들도 모두 존재할 수 없었겠죠? 집에 돌아가면 우리 일상 속 숨어있는 수학을 찾아보는 것도 정말 즐거울 것 같습니다.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/basicScience/story01_09.jpg"/>"
				}
			]
		},
		{
			title: "너와 나,우리의 지구 이야기[지구과학]",
			desc: "요즘 들어, 여기 저기서 지구가 들썩거리는 이야기를 많이 듣게 됩니다. "
				+"지진이나 태풍의 영향으로 국가적인 피해를 입게 되는 나라가 늘어나고, 그 나라들이 우리와 가까이 있어 우리 역시 "
				+"그 피해에서 자유롭지만은 않다는 사실! 모두 알고 계실 겁니다. 그래서 오늘은, 어쩌면 우리의 눈 앞에 있을지도 모르는 "
				+"위험에 대해 직접 몸으로 느껴보고 다시 생각할 수 있는 기회를 가져볼까 합니다.\n" 
				+"저기, 바람에 잔뜩 날린 머리로 체험장을 나오는 어린이가 보입니다. 젖은 발을 닦는 학생도 엄마와 아이도 보이네요. "
				+"온 몸이 흔들리는 경험을 했는지 어질어질한 기분을 잠재우기 위해 잠시 앉아 쉬는 아빠도 보입니다." 
				+"이 분들은 모두 기초과학관에서 지진, 태풍, 회오리 등 자연현상을 잠시나마 온 몸으로 체험해본 사람들입니다.",
			thumbnailUrl: "<c:url value="/resources/img/display/mainBuilding/basicScience/story01_10.jpeg"/>",
			stories: [
				{
					storyLabel: "지진체험",
					storyDesc: "‘서울에서 지진이 발생한다면?’이라는 가제로 만들어진 4D 체험물을 통해 지진을 경험해볼 수 있어 "
								+"내가 살아가는 도시가 위험에 빠질 수도 있다는 위기 의식을 다시 한번 느낄 수 있는 공간입니다.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/basicScience/story01_10.jpeg"/>"
				},
				{
					storyLabel: "태풍",
					storyDesc: "태풍은 뉴스에서 세기에 대해서는 수치로 많이 들어보았지만, 그 위력이 어느 정도인지는 잘 몰라 궁금하셨죠? "
								+"이름도 세기도 다양한 태풍의 위력이 어느 정도인지 직접 체험하고 나면, "
								+"앞으로 뉴스를 볼 때 태풍의 세기만 들어도 등골이 오싹해지는 경험을 하게 될 것입니다. "
								+"자연 재해는 완벽하게 막을 수는 없지만 그래도 스스로 알고 대비할 수 있다면 피해를 최소화할 수 있겠죠? "
								+"매일 뉴스로만 접했던 크고 작은 자연재해들을 이제는 온 몸으로 직접 체험해보고 언제라도 위험에 대비하려는 마음가짐을 가져보면 좋겠습니다.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/basicScience/story01_11.jpeg"/>"
				},
				{
					storyLabel: "물방울",
					storyDesc: "이 밖에도 너무 작아 지나치는 물방울부터 너무 커서 볼 수 없었던 바닷속을 들여다보면서 과학의 무한한 영역을 느껴보세요."
								+"오늘 여러분 모두, 과학을 통해 세상을 보는 눈이 한층 더 깊어지게 되겠네요!",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/basicScience/story01_12.jpg"/>"
				}
			]
		},
		{
			title: "보이지 않는 세계를 탐험하다[화학]",
			desc: "복잡한 원소기호와 화학식! 이 것들이 화학의 전부라고 생각하셨다면 화학을 제대로 오해하고 계신 겁니다. "
					+"이제, 그 오해를 풀 수 있는 즐거운 화학 이야기가 시작되니 기대해주세요! ",
			thumbnailUrl: "<c:url value="/resources/img/display/mainBuilding/basicScience/story01_13.jpg"/>",
			stories: [
				{
					storyLabel: "플라즈마",
					storyDesc: "‘플라즈마’라고 들어보셨나요? 기체의 온도를 계속 높이면 제 4의 물질상태로 변하는 데 그것을 ‘플라즈마’라고 합니다. "
								+"플라즈마! 말로만 들어보았다면 이제 눈으로 직접 확인해봐야겠죠?"
								+"마치 레이저쇼를 보는 듯 화려하고 짜릿한 플라즈마의 세계! 손을 대면 여러 가지 색깔의 플라즈마가 눈 앞에 펼쳐집니다. "
								+"빨간색, 초록색, 파란색! 내 손으로 직접 만들어내는 플라즈마!" 
								+"화려한 플라즈마 세계에 빠져 있는 동안 물질은 고체, 액체, 기체가 전부라고 생각했던 우리의 상식 수준을 자연스럽게 한 층 높일 수 있을 것입니다.",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/basicScience/story01_13.jpg"/>"
				},
				{
					storyLabel: "전기",
					storyDesc: "평소에 전기 제품은 물이 묻은 손으로 만지면 절대 안 된다는 말은 많이 들어보셨죠? "
								+"그 위험 수준이 어느 정도인지 궁금하지 않으셨나요? 기초과학관에서는 땀에 젖은 손으로 전기제품을 만졌을 때 "
								+"어느 정도의 전류가 흐르게 되는지 확인할 수 있습니다. 그 위험 정도를 직접 경험해보고, 주변 가깝고 소중한 "
								+"사람들에게 평소 전기 사용 시 안전이 얼마나 중요한지 꼭 알려주세요.\n"
								+"화학은 우리 생활에 꼭 필요한 상식이자 삶의 지혜입니다. 오늘 기초과학관에서 화학에 대한"
								+"모든 오해를 풀고, 생활 속에서 조금 더 가까이 화학을 만날 수 있었으면 좋겠네요.\n" 
								+"기초과학관, 즐겁게 관람하셨나요?" 
								+"“과학책을 쉽게 재미있게 읽은 기분이었어요.”"
								+"“시간이 어떻게 갔는지 모르겠어요.”"
								+"“저보다 아빠가 더 좋아하는 것 같아요.”" 
								+"기초과학관에서 만난 관람객들의 이야기인대요. 여러분의 과학에 대한 생각을 바꾸고, 과학에" 
								+"대한 애정을 깊게 하는 시간이 되었다니 무척 기쁩니다. 자! 즐거운 마음으로" 
								+"다음 과학 여행을 이어가 볼까요?",
					imgUrl: "<c:url value="/resources/img/display/mainBuilding/basicScience/story01_14.jpg"/>"
				}
			]
		}
	];
	
	var storySource   = $("#story-template").html();
	var storyTemplate = Handlebars.compile(storySource);
	var storyHtml = storyTemplate({stories: stories});
	$("#stories").html(storyHtml);
</script>


<c:url var="shareUrl" value="/display/mainBuilding/basicScience" />
<c:import url="/WEB-INF/views/display/displayDetailModal.jsp">
	<c:param name="shareUrl" value="${shareUrl }"/>
	<c:param name="exhibitionName" value="기초과학관" />
</c:import>

<script type="text/javascript">

	$(document).ready( function() {
		// 주요 전시물 클릭시 show hide event
		//showHideDetails();
		// 이미지 슬라이더
		//PTechSlider('#image-slider').autoSlide();
		PTechSlider('#image-slider',
			{
				slidesPerScreen: 4,
				indicators: true
			}		
		);
		
		// 전시관 Map 1층으로.
		//setDefaultSVGMap('#basic-map-spy',1);
		// 지도 flash 해당 전시관 표시
		//$('#XMLID_219_').attr('class', 'active');
		
	});
	
	function showHideDetails()  {
		// hide all details first.
		$('.exhibition-detail-wrapper').hide();
		// show the active detail only.
		var exhibit_active_id = $('.major-exhibition-wrapper.active').data('id');
		$(exhibit_active_id).show();
		
		// click event for major exhibition box
		$('.major-exhibition-wrapper').on('click', function() {
			
			// get current active div & remove active
			var hide_id = $('.major-exhibition-wrapper.active').data('id');
			$('.major-exhibition-wrapper.active').removeClass('active');
			// hide the matching detail
			$(hide_id).hide();
			
			// add 'active' to the current clicked div
			var show_id = $(this).data('id');
			$(this).addClass('active');
			// show the mathcing detail
			$(show_id).show();
			
		});
	}

</script>


<!-- Major Exhibition Contents Loading -->
<c:import url="/WEB-INF/views/display/majorExhibitionTemplate.jsp" />
<script type="text/javascript">
	var exhibitions = [
		{
			descriptions : [
				{
					title : "물리, 세상을 이루는 기본적인 힘",
					desc : "‘전기문명은 이렇게 시작되었다’는 전기와 자기가 서로 만나 세상을 어떻게 바꾸어 왔는지를 "
						+ "보여주고, ‘파동이 바꾼 세상’은 빛과 소리로 세상을 보고 듣는 원리를 일깨워준다. "
						+ "이 두 곳은 대표적인 주제 기반 스토리텔링형 전시 코너이다. " 
						+ "과학 개념의 발견과 연구의 역사적 과정을 작동형 전시물로 탐구해 볼 수 있고, " 
						+ "과학의 개념이 응용기술을 통해 우리 생활에 어떻게 적용되는지 한 자리에서 볼 수 있다."
				},
				{
					title: "",
					desc : "매시 15분이면 전시장을 뒤흔드는 굉음! 바로 ‘테슬라코일’이 그 주인공이다. " 
						+ "이것은 220V의 전기를 400만V로 승압시켜 강력한 스파크와 굉음을 발생시키는 " 
						+ "3.1m 높이의 대형 교류발전기이다. 그 주변에 선 사람들은 테슬라코일이 발생시키는 " 
						+ "전류에 영향을 받아 손에 쥔 형광등에서 불이 켜지는 마술 같은 경험을 할 수 있다." 
				}
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/mainBuilding/basicScience/major_exhibition_01.png' />",
					caption: "[파동이 바꾼 세상]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/mainBuilding/basicScience/major_exhibition_02.png' />",
					caption: "[테슬라코일]"
				}
			]
		},
		{
			descriptions : [
				{
					title : "화학, 세상을 이루는 다양한 물질",
					desc : "생명과 사물을 이루는 물질을 탐구하는 학문인 화학을 배울 수 있는 전시 코너이다. " 
						+ "지금까지 발견된 원소들을 특징별로 배열해 만든 ‘주기율표’를 살펴볼 수 있다. " 
						+ "또 직접 전류를 흘려보며 금속원소의 전도성을 측정해 보고, " 
						+ "비눗방울 놀이를 통해 분자 구조와 화학결합에 대해 알아볼 수 있다."
				},
				{
					title: "생물, 세상을 이루는 소중한 생명",
					desc : "우리 몸을 구성하는 다양한 기관들(순환계, 신경계, 생식계, 골격계, 감각계)을 탐구하고, "
						+ "우리 주변을 이루는 동·식물과 생태계에 대해 알아보는 코너이다. " 
						+ "‘ABO 혈액탐구’는 대한적십자사 후원으로 제작된 공간으로서, 혈액의 성분과 기능, " 
						+ "혈액형의 개념을 배울 수 있다. 또 직접 자신의 혈압을 측정해 보고 가상 헌혈 체험도 해볼 수 있다." 
				}
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/mainBuilding/basicScience/major_exhibition_03.png' />",
					caption: "[화학 - 주기율표]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/mainBuilding/basicScience/major_exhibition_04.png' />",
					caption: "[생물 - 28주된 태아]"
				}
			]
		},
		{
			descriptions : [
				{
					title : "지구과학, 세상을이루는 신기한 자연현상",
					desc : "우리가 살고 있는 곳은 어디일까? 지구! 그런데 사실 우리는 겨우 지구의 땅 위에서만 살 뿐이다. " 
						+ "지구 표면의 70%를 덮고 있는 바다, 무려 6,400km의 깊이를 가지는 지구의 땅 속, 그리고 드높은 하늘. " 
						+ "지구의 이 모든 곳에서 일어나는 다양한 현상들을 탐구해 보는 곳이 바로 이곳이다."
				},
				{
					title: "",
					desc : "‘지진체험’은 과학관에서 가장 인기 있는 체험물로서, 실제 지진이 일어난 상황을 4D로 실감나게 체험하며 " 
						+ "지진 발생 시 대처하는 법을 배울 수 있다."
						+ "<div class='additional-info'>(15명/회, 초등 1학년 이상)</div>" 
				},
				{
					title: "",
					desc : "‘태풍체험’은 비옷을 입고 온몸으로 비바람을 느껴볼 수 있는 체험물이다. " 
						+ "얼굴과 발이 흠뻑 젖지만 체험자 모두 신나게 즐긴다. "
						+ "<div class='additional-info'>(15명/회, 초등 1학년 이상)</div>" 
				},
				{
					title: "",
					desc : "극지체험실에서는 매월 2, 4번째 일요일 오전 10시 반에 남극세종기지 연구원들과 " 
						+ "화상통화를 할 기회가 주어진다. 연구원들에게 평소 궁금했던 질문을 직접 물어보고 그 답을 들어볼 수 있다. "
						+ "<div class='additional-info'>(10명/회, 누구나)</div>" 
				}
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/mainBuilding/basicScience/major_exhibition_05.png' />",
					caption: "[태풍체험]"
				}
			]
		}
	];
	
	var exhibitionSource   = $("#major-exhibition-template").html();
	var exhibitionTemplate = Handlebars.compile(exhibitionSource);
	var exhibitionHtml = exhibitionTemplate({exhibitions: exhibitions});
	$("#main-exhibitions").html(exhibitionHtml);
	
</script>