<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id = "communication-wrapper" >
	<div class="sub-content-nav scrollspy-teal">
		<nav id="scrollspy-nav" class="navbar-default navbar-static has-scroll">
			<div class="container">
				<ul class="nav navbar-nav">
					<c:import url="/WEB-INF/views/display/navbar.jsp"/>
					<li>
						<a href="#board-spy" class="item">
							전시물 리뷰
						</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<div class="bread-crumbs">
							전시관람 / 전시물 리뷰
						</div>					
					</li>
				</ul>
			</div>
		</nav>
	</div>
	<div id="display-body" class="sub-body">
		<div  class="container">
			<div id = "board-spy" class="row">
				<div class="col-md-12">
					<h3 class="page-title teal">
						<div class="top-line"></div>
						 과학관 직원이 소개하는 전시물
					</h3>
					<p>
						국립과천과학관에는 다양한 볼거리들로 가득합니다.<br>
						이 코너를 활용하시면 국립과천과학관에서 새롭게 여러분들을 기다릴 신규전시물에 대해 확인하실 수 있습니다.
					</p>
				</div>
			</div> 
			<div class="row">
				<div class="col-md-12">
					<div id="board-wrapper" class="table-teal">
						<c:url var="currentPath" value="/display/exhibitionReview"/>
			 			
			 			<c:import url="/WEB-INF/views/boards/gallery.jsp">
			 				<c:param name="path" value="${currentPath }"/>
			 			</c:import>
		 			</div>
	 			</div>
			</div>
		</div> 
	</div>
</div> 