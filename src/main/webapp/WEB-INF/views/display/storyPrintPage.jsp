<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<!-- Story Print Modal -->
<div id="story-page-wrapper">   
	<div class="story-page-header">
		<h4 class="story-page-title">${param.exhibitionName } 여행기 출력</h4>	
	</div>	
	 <div id="stories-print-wrapper">
	 </div>
</div>

<script id="story-print-modal-template" type="text/x-handlebars-template">
{{#each stories}}
<div class="story-print-item">
	
	<div class="stories-title">
		{{title}}
	</div>

	<div class="story-print-body-wrapper">
		{{#each stories}}
			<div class="story">
				<div class="story-label">
					{{storyLabel}}
				</div>							
				<div class="story-image">
					<img src="{{imgUrl}}" alt=" " class="img-responsive"/>
				</div>
				<div class="story-desc">
					{{breaklines storyDesc}}
				</div>
			</div>
		{{/each}}
	</div>
</div>
{{/each}}
</script>


<script type="text/javascript">

	Handlebars.registerHelper('breaklines', function(text) {
	    text = Handlebars.Utils.escapeExpression(text);
	    text = text.toString();
	    text = text.replace(/(\r\n|\n|\r)/gm, '<span></span>');
	    return new Handlebars.SafeString(text);
	});	

	var storyPrintModalSource   = $("#story-print-modal-template").html();
	var storyPrintModalTemplate = Handlebars.compile(storyPrintModalSource);
	var storyPrintModalHtml = storyPrintModalTemplate({stories: stories});
	$('#stories-print-wrapper').html(storyPrintModalHtml);
	
	
	$('#stories-print-wrapper .story-image img').each( function() {
		var imgSrcValue = $(this).attr('src');
		if( imgSrcValue == "#") {
			$(this).remove();
		}
	});
	
	
</script>