<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-teal">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>
	<div id="display-body" class="sub-body">
		<div class="narrow-sub-top display">
			<div class="sub-banner-tab-wrapper sub-banner-tab-teal">
				<!-- <div class="container">
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
							<ul class="nav nav-tabs">
								<li class="col-xs-12 active">
									<a href="http://cyber.scientorium.go.kr/museum_2015/vrLoad.jsp?pavilion=roadview&xml_name=otr_008.xml&angle=180" class="item">
										<i class="fa fa-desktop fa-lg" aria-hidden="true"></i> 사이버 전시관
									</a>  
								</li>
							</ul>   
						</div>
					</div>
				</div> -->
			</div>			
		</div>
		<div class="container">
			<div id="basic-map-spy" class="section scrollspy">
				<div class="row">
					<div class="col-sm-3 col-md-2 col-lg-2">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							옥외전시장 안내
						</h3>
					</div>
				</div>
				<div class="row description">
					<div class="col-sm-12 col-md-4 col-lg-4">
						「옥외전시장」은 현장체험학습을 통해 교육효과를 증대시키기 위한 공간이다. 이곳 야외에서 직접 관찰·체험함으로써, 실내에서 느낄 수 없는 자연 속 전시환경과 교감할 수 있으며 과학적 원리를 스스로 체험할 수 있다.
					</div>
					<div class="col-sm-12 col-md-8 col-lg-8 text-right">
						<!-- select dropdown : 다른관으로 이동 -->
						<c:import url="/WEB-INF/views/display/displayHallSelect.jsp"/>
						<div>
							<c:import url="/WEB-INF/views/display/mainBuilding/svgs/display_svg_map_modified.jsp"></c:import>
						</div>
					</div> 
				</div>
			</div>
			<div id="major-exhibitions-spy" class="scrollspy">
				<div class="row">
					<div class="col-sm-3 col-md-2 col-lg-2">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							주요 전시물
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="main-exhibition-wrapper">
							<div id="main-exhibitions">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="related-photos">
				<div class="title">
					<i class="color-darkgreen material-icons">&#xE413;</i>   관련사진
				</div>
				<div id="image-slider">
					<div id="image-slider-window">
						<img src="<c:url value="/resources/img/display/outdoorEcological/outdoor/related_photo_01.png"/>" 
							data-url="/resources/img/display/outdoorEcological/outdoor/related_photo_01.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/outdoorEcological/outdoor/related_photo_02.png"/>" 
							data-url="/resources/img/display/outdoorEcological/outdoor/related_photo_02.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/outdoorEcological/outdoor/related_photo_03.png"/>" 
							data-url="/resources/img/display/outdoorEcological/outdoor/related_photo_03.png "class="slide"/>
						<img src="<c:url value="/resources/img/display/outdoorEcological/outdoor/related_photo_04.png"/>" 
							data-url="/resources/img/display/outdoorEcological/outdoor/related_photo_04.png" class="slide"/>												
					</div>
				</div><!--  image-slider END ... -->
			</div>
			<div id="playroom-map-spy" class="section scrollspy">
				<div class="row">
					<div class="col-md-12">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							과학문화광장 안내
						</h3>
					</div>
				</div>
				<div class="row description">
					<div class="col-sm-12 col-md-4 col-lg-4">
						<ul class="circle-list teal">
							<li class="margin-bottom20">
								안내
								<ul>
									<li>과학기술과 환경, 철학, 경제, 예술 등 다양한 분야의 사람들이 함께 만나고 의사소통할 수 있는 과학문화광장입니다. </li>
									<li>
										국립과천과학관의 과학문화광장에서는 다양한 과학문화행사를 개최합니다.
									</li>	
								</ul>
							</li>
						</ul>
					</div> 
					<div class="col-md-12 col-md-8 col-lg-8 text-right convenience">
						<c:import url="/WEB-INF/views/display/mainBuilding/svgs/display_svg_map_modified.jsp"></c:import>
					</div>
				</div>
			</div>		
			
			<div id="science-playroom-spy" class="section scrollspy">
				<div class="row">
					<div class="col-sm-3 col-md-2 col-lg-2">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							과학놀이터
						</h3>
					</div>
				</div>
				<div class="row description">
					<div class="col-md-12">
						<ul class="circle-list teal">
							<li>
								이용 안내
								<ul>
									<li>
										아이들의 놀이터에서도 과학을 느낄 수 있습니다.
									</li>  
									<li>
										소리의 반사경 ‘파라볼라’, 주파수를 배울 수 있는 ‘돌하프와 악기놀이’, 
										빛의 분산 및 굴절을 확인할 수 있는 ‘물 프리즘’, 작용-반작용의 움직임의 원리가 보이는 
										’소용돌이’, 눈으로 확인하는 ‘이중나선형 분자구조’, 소리의 진동 전화놀이와 수평잡기를 배우는 ‘시소’, 
										성덕대왕신종의 현대적 재해석품 ‘나래쇠북’까지 설치하여 쉽고 재미있게 과학과 만날 수 있습니다.
									</li>								
								</ul>
							</li>
						</ul>
					</div>
				</div>
				
				<div class="convenience-detail">
					<div class="row">
						<div class="col-xs-6 col-sm-6 col-md-6 detail-img-box">
							<img src="<c:url value='/resources/img/support/convenience/playground01.png'/>" class="img-responsive"/>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 detail-img-box">
							<img src="<c:url value='/resources/img/support/convenience/playground02.png'/>" class="img-responsive"/>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 detail-img-box">
							<img src="<c:url value='/resources/img/support/convenience/playground03.png'/>" class="img-responsive"/>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 detail-img-box">
							<img src="<c:url value='/resources/img/support/convenience/playground04.png'/>" class="img-responsive"/>
						</div>
					</div>
				</div>
			</div>
			
			
			<div id="culture-square-spy" class="section scrollspy">
				<div class="row">
					<div class="col-sm-3 col-md-2 col-lg-2">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							과학문화광장
						</h3>
					</div>
				</div>
				<div class="row description margin-bottom20">
					<div class="col-md-12">
						<ul class="circle-list teal">
							<li>
								이용 안내
								<ul>
									<li>
										과학기술과 환경, 철학, 경제, 예술 등 다양한 분야의 사람들이 함께 만나고 의사소통할 수 있는 
										과학광장, 과학문화광장, 과학조각공원 등 부대시설이 곳곳에 있습니다.
									</li>  
								</ul>
							</li>
						</ul>
					</div>
				</div>
				
				<div class="convenience-detail">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="display-card">
							<div class="card-image">
								<img src="<c:url value='/resources/img/support/convenience/facility01.png'/>" class="img-responsive"/>							
							</div>
							<div class="card-content">
								<div class="display-card-title">
									과학광장
								</div>
								<div class="display-card-content">
									국립과천과학관은 과학광장 및 과학문화광장, 과학조각공원 등 다양한 문화행사를 개최할 수 있는 야외활동 공간을 확보하고 있습니다.								
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="display-card">
							<div class="card-image">
								<img src="<c:url value='/resources/img/support/convenience/facility02.png'/>" class="img-responsive"/>							
							</div>
							<div class="card-content">
								<div class="display-card-title">
									과학문화광장
								</div>
								<div class="display-card-content">
									국립과천과학관은 과학광장 및 과학문화광장, 과학조각공원 등 다양한 문화행사를 개최할 수 있는 야외활동 공간을 확보하고 있습니다.
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="display-card">
							<div class="card-image">
								<img src="<c:url value='/resources/img/support/convenience/facility03.png'/>" class="img-responsive"/>							
							</div>
							<div class="card-content">
								<div class="display-card-title">
									과학조각공원
								</div>
								<div class="display-card-content">
									국립과천과학관은 과학광장 및 과학문화광장, 과학조각공원 등 다양한 문화행사를 개최할 수 있는 야외활동 공간을 확보하고 있습니다.
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="display-card">
							<div class="card-image">
								<img src="<c:url value='/resources/img/support/convenience/facility04.png'/>" class="img-responsive"/>							
							</div>
							<div class="card-content">
								<div class="display-card-title">
									과학캠프장
								</div>
								<div class="display-card-content">
									국립과천과학관은 천체관측소와 함께 학생들의 다양한 활동을 지원할 수 있는 과학캠프장을 운영하고 있습니다. (캠프장 숙소 운영)
								</div>
							</div>							
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="display-card">
							<div class="card-image">
								<img src="<c:url value='/resources/img/support/convenience/facility05.png'/>" class="img-responsive"/>							
							</div>
							<div class="card-content">
								<div class="display-card-title">
									노천극장
								</div>
								<div class="display-card-content">
									국립과천과학관은 자연 속에서 다양한 문화행사 및 공연을 개최할 수 있는 노천극장을 운영하고 있습니다.<br>
									*수용인원: 약 520명 / 무대: 면적 93.6M, 지붕설치 / 객석: 면적 527M, 스탠드 설치
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	$(document).ready( function() {
		// 이미지 슬라이더
		//PTechSlider('#image-slider').autoSlide();
		PTechSlider('#image-slider',
			{
			slidesPerScreen: 4,
			indicators: true,
			height: 150
			}		
		);
		
		// 전시관 지도 flash outdoor로 전환.
		setDefaultSVGMap('#basic-map-spy',3);
		// 지도 flash 해당 전시관 표시
		$('#XMLID_103_').attr('class', 'active');	// 우주항공
		$("g[id='XMLID_109_']").attr('class', 'active');	//에너지
		$("g[id='XMLID_28_']").attr('class', 'active');	// 교통&수송
		$("g[id='XMLID_99_']").attr('class', 'active'); // 기타 부대시설 - 과학캠프장
		$("g[id='XMLID_105_']").attr('class', 'active'); // 기타 부대시설 - 노천극장
	});
	
	
</script>



<!-- Major Exhibition Contents Loading -->
<c:import url="/WEB-INF/views/display/majorExhibitionTemplate.jsp" />
<script type="text/javascript">
	var exhibitions = [
		{
			descriptions : [
				
				{
					title: "우주 전시, 로켓과 나로호를 만나다",
					desc : "무궁화 위성 1호를 쏘아올린 델타로켓의 모형과 우리나라의 첫 우주발사체인 "
						+ "나로호 모형은 과학관 서쪽 출입문에서 바로 보이는 곳에 위치해 있다. "
						+ "실제 크기로 설치되어 있어 우주탐사에 대한 실감나는 현장 체험과 우리나라의 "
						+ "노력과 결실을 알아보며 체험할 수 있다."
				},
				{
					title: "교통 전시, 철로 위 기차를 만나다",
					desc : "교통 전시장은 상설전시관 후문에 위치해 있는 코너이다. "
						+ "산업기술의 발달과정을 보여주는 육상 운송수단인 기차와 철로가 실물로 "
						+ "설치되어 있으며, 포토존과 체험학습공간으로 사용할 수 있다."
				}
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/outdoorEcological/outdoor/major_exhibition_01.png' />",
					caption: "[나로호]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/outdoorEcological/outdoor/major_exhibition_02.png' />",
					caption: "[기차와 철도]"
				}
			]
		}
		
	];
	
	var exhibitionSource   = $("#major-exhibition-template").html();
	var exhibitionTemplate = Handlebars.compile(exhibitionSource);
	var exhibitionHtml = exhibitionTemplate({exhibitions: exhibitions});
	$("#main-exhibitions").html(exhibitionHtml);
	setDefaultSVGMap("#basic-map-spy", 3);
	
	$('#basic-map-spy').find("g[id='XMLID_142_']").attr('class', 'active'); // 과학놀이터
	$('#basic-map-spy').find("g[id='XMLID_586_']").attr('class', 'active'); // 기타 부대시설 - 과학광장
	$('#basic-map-spy').find("g[id='XMLID_278_']").attr('class', 'active'); // 기타 부대시설 - 과학문화광장
	$('#basic-map-spy').find("g[id='XMLID_140_']").attr('class', 'active'); // 기타 부대시설 - 과학조각공원
</script>