<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-teal">
		<nav id="scrollspy-nav" class="navbar-default navbar-static has-scroll">
			<div class="container">
				<ul class="nav navbar-nav">
					<c:import url="/WEB-INF/views/display/navbar.jsp"/>
					<li>
						<a href="#basic-info-spy" class="item">
							기본안내
						</a>
					</li>
					<li>
						<a href="#basic-map-spy" class="item">
							과학문화광장 안내
						</a>
					</li>
					<li>
						<a href="#science-playroom-spy" class="item">
							과학놀이터
						</a>
					</li>
					<li>
						<a href="#culture-square-spy" class="item">
							과학문화광장
						</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<div class="bread-crumbs">
							전시관람 / 과학문화광장
						</div>
					</li>
				</ul>
			</div>
		</nav>
	</div>
	<div id="display-body" class="sub-body">
		<div id="basic-info-spy" class="scrollspy exhibition-sub-banner culture-square">
			<div class="banner-content-box">
				<div class="title">
					과학문화광장  
				</div>  
				<div class="detail">
					생활 속 과학을 발견하는 곳
				</div>
			</div>
		</div>
	
		<div class="container">
			
				
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function() {
		
	});
</script>