<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-teal">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>
	<div id="display-body" class="sub-body">
		<div class="narrow-sub-top display">
			<div class="sub-banner-tab-wrapper sub-banner-tab-teal">
				<!-- <div class="container">
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
							<ul class="nav nav-tabs">
								<li class="col-xs-12 active">
									<a href="http://cyber.scientorium.go.kr/museum_2015/vrLoad.jsp?pavilion=roadview&xml_name=otr_008.xml&angle=180" class="item">
				                        <i class="fa fa-desktop fa-lg" aria-hidden="true"></i> 사이버 전시관
				                    </a>  
								</li>
							</ul>   
						</div>
					</div>
				</div> -->
			</div>			
		</div>
		<div class="container">
			<div id="basic-map-spy" class="section scrollspy">
				<div class="row">
					<div class="col-xs-4 col-md-2">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							생태공원 안내
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<p>
							「생태공원」은 과학관 전시장 밖 뒤뜰에 있는 도심 속 작은 공원이다. 연못 안에는 물고기들이, 
							풀밭에는 닭과 오리, 꽃밭에는 알록달록한 꽃들이 우리를 반긴다. 
							언덕 위와 연못 주위 곳곳에 마련한 쉼터에서는 잠시 마음의 여유를 찾을 수 있다. 
						</p>
						<ul class="circle-list teal">
							<li>
								주요전시물
								<ul>
									<li>
										자생력있는 생태환경을 구현하여 생물군의 서식처 태계를 직접 관찰하고 체험하는 생태교육장.
									</li>
									<li>
										자연 속에서 휴식할 수 있는 공간 제공
									</li>
									<li>
										다양한 생태체험학습프로그램 운영
									</li>
								</ul>
							</li>
						</ul>
					</div>     
					<div class="col-md-8 text-right">
						<!-- select dropdown : 다른관으로 이동 -->
						<c:import url="/WEB-INF/views/display/displayHallSelect.jsp"/>
						<div>
							<img alt="생태공원" src="<c:url value='/resources/img/display/map/12.png'/>"
								class="img-responsive margin-top20">
						</div>
					</div>
				</div>
			</div>
			<div id="major-exhibitions-spy" class="section scrollspy">
				<div class="row">
					<div class="col-xs-4 col-md-2">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							주요전시물					
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="main-exhibition-wrapper">
							<div id="main-exhibitions">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="story-spy" class="story-telling-wrapper">
				<div class="header">
					<div class="row">
						<div class="col-md-6">
							<h3 class="page-title teal">
								<div class="top-line"></div>
								스토리로 읽어 보는 생태공원 여행기 
							</h3>
						</div>
					</div>
				</div>
				<div class="description">
					야외 공간으로 나오면 과학관 내부 전시관에서 봤던 주제들을 함축하는 전시물들이 곳곳에 
					보입니다. 함께 온 친구, 가족들과 함께 야외 공간을 거닐다 보면 우주 여행을 떠날 수 있는 
					로켓과 실물크기의 기차들도 볼 수 있습니다. 또, 공룡 동산에서는 다양한 공룡들이 과학관을 산책 하는 듯 
					생동감 넘치게 전시되어 있습니다. 
					과학관에서 봤던 전시물 이야기를 나누며 야외 전시물을 천천히 감상해보세요.
				</div>
				<div id="stories" class="row">
				</div>
			</div>
			<div class="related-photos">
				<div class="title">
					<i class="color-darkgreen material-icons">&#xE413;</i>관련사진
				</div>
				<div id="image-slider">
					<div id="image-slider-window">
						<img src="<c:url value="/resources/img/display/outdoorEcological/ecoPark/related_photo_01.png"/>" 
							data-url="/resources/img/display/outdoorEcological/ecoPark/related_photo_01.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/outdoorEcological/ecoPark/related_photo_02.png"/>" 
							data-url="/resources/img/display/outdoorEcological/ecoPark/related_photo_02.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/outdoorEcological/ecoPark/related_photo_03.png"/>" 
							data-url="/resources/img/display/outdoorEcological/ecoPark/related_photo_03.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/outdoorEcological/ecoPark/related_photo_04.png"/>" 
							data-url="/resources/img/display/outdoorEcological/ecoPark/related_photo_04.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/outdoorEcological/ecoPark/related_photo_05.png"/>" 
							data-url="/resources/img/display/outdoorEcological/ecoPark/related_photo_05.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/outdoorEcological/ecoPark/related_photo_06.png"/>" 
							data-url="/resources/img/display/outdoorEcological/ecoPark/related_photo_06.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/outdoorEcological/ecoPark/related_photo_07.png"/>" 
							data-url="/resources/img/display/outdoorEcological/ecoPark/related_photo_07.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/outdoorEcological/ecoPark/related_photo_08.png"/>" 
							data-url="/resources/img/display/outdoorEcological/ecoPark/related_photo_08.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/outdoorEcological/ecoPark/related_photo_09.png"/>" 
							data-url="/resources/img/display/outdoorEcological/ecoPark/related_photo_09.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/outdoorEcological/ecoPark/related_photo_10.png"/>" 
							data-url="/resources/img/display/outdoorEcological/ecoPark/related_photo_10.png" class="slide"/>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script id="story-template" type="text/x-handlebars-template">
{{#each stories}}
<div class="col-xs-12 col-sm-6 col-md-6">
	<div class="display-card story-telling story-more-btn pointer" data-idx="{{@index}}" onclick="">
		<div class="card-image">
			<img src="{{thumbnailUrl}}" class="img-responsive"/>
		</div>
		<div class="card-content">
			<div class="display-card-title">
				{{title}}
			</div>
			<div class="display-card-content">
				{{{desc}}}
			</div>
			
		</div>
	</div>							
</div>
{{/each}}
</script>

<script type="text/javascript">
	var stories = [
		{
			title: "[곤충생태관]",
			desc: "곤충 하면 눈살을 찌푸리는 엄마도, 삭막한 도시 공간에서 살아있는 곤충을 보기 힘든 아이도, "
				+ "동심으로 돌아가 곤충과 함께하는 시간이 그리운 아빠도 이 곳에서는 함께 웃을 수 있습니다. "
				+ "곤충생태관을 들어서면 시골마을을 여행하는 것 같은 기분이 듭니다.",
			thumbnailUrl: "<c:url value="/resources/img/display/outdoorEcological/insectarium/story18_02.jpg"/>",
			stories: [
				{
					storyLabel: "곤충생태관",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/outdoorEcological/insectarium/story18_01.jpg"/>"
				},
				{
					storyLabel: "곤충생태관",
					storyDesc: "풀벌레 소리가 들리시나요? 아직 이 정도로 감동하기는 이릅니다. "
						+ "그 동안 벌을 보면 쏘일 까봐 도망 다니셨죠? 곤충생태관에선 물지 않는 벌이 있어 직접 만져볼 수 있고, "
						+ "촉감만으로 어떤 곤충인지 만져보고 맞춰볼 수 있는 공간도 있어 재미를 더해줍니다. 육상곤충, 수서곤충, 각종 벌레 들의 "
						+ "실제 모습과 사라져 가는 곤충들을 보며 곤충과 더 가까워질 수 있는 시간을 가질 수도 있습니다.",
					imgUrl: "<c:url value="/resources/img/display/outdoorEcological/insectarium/story18_02.jpg"/>"
				},
				{
					storyLabel: "곤충생태관",
					storyDesc: "풀벌레 소리가 들리시나요? 아직 이 정도로 감동하기는 이릅니다. "
						+ "그 동안 벌을 보면 쏘일 까봐 도망 다니셨죠? 곤충생태관에선 물지 않는 벌이 있어 직접 만져볼 수 있고, "
						+ "촉감만으로 어떤 곤충인지 만져보고 맞춰볼 수 있는 공간도 있어 재미를 더해줍니다. 육상곤충, 수서곤충, 각종 벌레 들의 "
						+ "실제 모습과 사라져 가는 곤충들을 보며 곤충과 더 가까워질 수 있는 시간을 가질 수도 있습니다.",
					imgUrl: "<c:url value="/resources/img/display/outdoorEcological/insectarium/story18_03.jpg"/>"
				},
				{
					storyLabel: "나비의 생애",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/outdoorEcological/insectarium/story18_04.jpg"/>"
				},
				{
					storyLabel: "나비의 생애",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/outdoorEcological/insectarium/story18_05.jpg"/>"
				},
				{
					storyLabel: "나비의 생애",
					storyDesc: "나비의 생애를 한 눈에 볼 수 있는 곳도 있으니 절대 놓치지 마세요. "
							+ "애벌레가 나비가 되기까지 모든 과정을 보고, 화려하게 날개 짓을 하는 나비를 본다면 "
							+ "예전의 감동보다 더 큰 감동을 느낄 수 있을 테니까요. 나비 날개 돋이 하는 모습, 긴 대롱을 "
							+ "이용해 꿀을 마시는 모습을 직접 눈으로 보는 것은 곤충생태관에서만 선물 받을 수 있는 소중한 경험이 될 것입니다.",
					imgUrl: "<c:url value="/resources/img/display/outdoorEcological/insectarium/story18_06.jpg"/>"
				}
				
			]
		},
		{
			title: "생태공원",
			desc: "생태 공원은 과학과 함께 휴식을 느낄 수 있는 공간으로 구성되어 있습니다. 학생들은 "
				+ "실물보다 훨씬 큰 곤충 모형들을 보며 웃음을 터뜨리고, 가족들은 찰스 다윈상과 알프레드 "
				+ "러셀 월리스, 그레고어 멘델과 기념 사진을 찍기도 하네요! 실제 인물이냐고요? 물론 아니죠! "
				+ "과학자들의 모습을 그대로 청동으로 만들어 놓은 동상이니까 함께 기념 사진을 남겨보세요. "
				+ "원두막, 물레방아, 디딜방아 같은 우리 선조들의 지혜가 담긴 옥외 전시물들도 놓치지 말고 구경해보세요.",
			thumbnailUrl: "<c:url value="/resources/img/display/outdoorEcological/ecoPark/story19_01.jpg"/>",
			stories: [
				{
					storyLabel: "생태공원",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/outdoorEcological/ecoPark/story19_01.jpg"/>"
				},
				{
					storyLabel: "생태공원",
					storyDesc: "생태 공원은 과학과 함께 휴식을 느낄 수 있는 공간으로 구성되어 있습니다. 학생들은 "
						+ "실물보다 훨씬 큰 곤충 모형들을 보며 웃음을 터뜨리고, 가족들은 찰스 다윈상과 알프레드 "
						+ "러셀 월리스, 그레고어 멘델과 기념 사진을 찍기도 하네요! 실제 인물이냐고요? 물론 아니죠! "
						+ "과학자들의 모습을 그대로 청동으로 만들어 놓은 동상이니까 함께 기념 사진을 남겨보세요. "
						+ "원두막, 물레방아, 디딜방아 같은 우리 선조들의 지혜가 담긴 옥외 전시물들도 놓치지 말고 구경해보세요.",
					imgUrl: "<c:url value="/resources/img/display/outdoorEcological/ecoPark/story19_02.jpg"/>"
				}
			]
		}
	];
	
	var storySource   = $("#story-template").html();
	var storyTemplate = Handlebars.compile(storySource);
	var storyHtml = storyTemplate({stories: stories});
	$("#stories").html(storyHtml);
</script>

<c:url var="shareUrl" value="/display/outdoorEcological/ecoPark" />
<c:import url="/WEB-INF/views/display/displayDetailModal.jsp">
	<c:param name="shareUrl" value="${shareUrl }"/>
	<c:param name="exhibitionName" value="생태공원" />
</c:import>

<script type="text/javascript">

	$(document).ready( function() {
		
		// image slider
		PTechSlider('#image-slider').autoSlide();
		
		// 전시관 지도 flash outdoor로 전환.
		//setDefaultSVGMap('#basic-map-spy',3);
		// 지도 flash 해당 전시관 표시
		//$("g[id='XMLID_98_']").attr('class', 'active');
	});
	
	function showHideDetails()  {
		// hide all details first.
		$('.exhibition-detail-wrapper').hide();
		// show the active detail only.
		var exhibit_active_id = $('.major-exhibition-wrapper.active').data('id');
		$(exhibit_active_id).show();
		
		// click event for major exhibition box
		$('.major-exhibition-wrapper').on('click', function() {
			
			// get current active div & remove active
			var hide_id = $('.major-exhibition-wrapper.active').data('id');
			$('.major-exhibition-wrapper.active').removeClass('active');
			// hide the matching detail
			$(hide_id).hide();
			
			// add 'active' to the current clicked div
			var show_id = $(this).data('id');
			$(this).addClass('active');
			// show the mathcing detail
			$(show_id).show();
			
		});
	}
	
</script>


<!-- Major Exhibition Contents Loading -->
<c:import url="/WEB-INF/views/display/majorExhibitionTemplate.jsp" />
<script type="text/javascript">
	var exhibitions = [
		{
			descriptions : [
				
				{
					title: "대형곤충모형, 숨은 곤충을 찾아라",
					desc : "곤충생태관 옆을 지나면 대형 사마귀 모형이 있어 여기가 생태공원의 입구라는 것을 알려준다. "
						+ "생태공원 곳곳에는 메뚜기, 무당벌레, 하늘소, 사슴벌레의 대형 모형이 숨어 있다. "
						+ "산책을 하다 이들을 발견하더라도 놀라지 마시라."
				},
				{
					title: "생태연못, 다양한 생물들이 모여 사는 곳",
					desc : "공원 입구의 출수부에서 나오는 물은 연못으로 이어지는데, 이 안에는 현재 9종의 물고기가 살고 있다. "
						+ "왜가리, 직박구리, 곤줄박이와 같은 다양한 조류들도 살고 있어 주의를 기울이면 보다 많은 생물들을 발견할 수 있다. "
						+ "연못에서 관람객들을 향해 뒤뚱거리며 다가오는 오리들은 생태공원의 가장 큰 인기 스타이다."
				}
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/outdoorEcological/ecoPark/major_exhibition_01.png' />",
					caption: "[대형곤충모형]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/outdoorEcological/ecoPark/major_exhibition_02.png' />",
					caption: "[생태연못]"
				},
			]
		},
		{
			descriptions : [
				
				{
					title: "세상을 바꾼 생태학자들, 공원에서 만나는 위인들",
					desc : "밤낮으로 생태공원을 지키는 사람들이 있다. 바로 유전학을 수학적으로 표현한 멘델, "
						+ "진화론을 주창한 다윈과 윌리스가 그 주인공이다. 그들이 왜 생태공원에 서 있는지 그 이야기를 들어보자."
				},
				{
					title: "생태텃밭, 농사의 소중함을 배우는 곳",
					desc : "생태공원의 가장 북쪽에는 생태텃밭과 푸른 향기원이 있어 문명이 만들어낸 인공적인 경작에 대한 것을 설명한다. "
						+ "농사의 의미와 작물들에 대해 배우고, 도시텃밭을 관찰하며 체험할 수 있다. 5월부터는 텃밭에서 감자와 옥수수, "
						+ "그리고 이 작물들에 얽힌 이야기를 볼 수 있을 것이다."
				}
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/outdoorEcological/ecoPark/major_exhibition_03.png' />",
					caption: "[유전학자 멘델]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/outdoorEcological/ecoPark/major_exhibition_04.png' />",
					caption: "[생태텃밭]"
				},
			]
		}
		
	];
	
	var exhibitionSource   = $("#major-exhibition-template").html();
	var exhibitionTemplate = Handlebars.compile(exhibitionSource);
	var exhibitionHtml = exhibitionTemplate({exhibitions: exhibitions});
	$("#main-exhibitions").html(exhibitionHtml);
	
</script>