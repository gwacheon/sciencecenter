<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-teal">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
		<%-- <nav id="scrollspy-nav" class="navbar-default navbar-static has-scroll">
			<div class="container">
				<ul class="nav navbar-nav">
					<c:import url="/WEB-INF/views/display/navbar.jsp"/>
					<li>
						<a href="#basic-info-spy" class="item">
							기본 안내 
						</a>
					</li>
					<li>
						<a href="#basic-map-spy" class="item">
							곤충생태관 안내
						</a>
					</li>
					<li>
						<a href="#major-exhibitions-spy" class="item">
							주요전시물
						</a>
					</li>			
					<li>
						<a href="#story-spy" class="item">
							스토리 여행기
						</a>
					</li>		
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<div class="bread-crumbs">
							전시관 / 곤충생태관
						</div>
					</li>
				</ul>
			</div>
		</nav> --%>
	</div>
	<div id="display-body" class="sub-body">
		<div class="narrow-sub-top display">
			<div class="sub-banner-tab-wrapper sub-banner-tab-teal">
				<!-- <div class="container">
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
							<ul class="nav nav-tabs">
								<li class="col-xs-12 active">
									<a href="http://cyber.scientorium.go.kr/museum_2015/vrLoad.jsp?pavilion=roadview&xml_name=otr_008.xml&angle=180" class="item">
				                        <i class="fa fa-desktop fa-lg" aria-hidden="true"></i> 사이버 전시관
				                    </a>  	
								</li>
							</ul>   
						</div>
					</div>
				</div> -->
			</div>			
		</div>
		<div class="container">
			<div id="basic-map-spy" class="section scrollspy">
				<div class="row">
					<div class="col-xs-4 col-md-3">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							곤충생태관 안내
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-md-4 col-lg-4">
						<p>
							<b>「곤충(Insect)생태관」</b>은 무궁무진한 곤충의 세계를 자세히 살펴볼 수 있는 공간이다. 
							곤충은 전체 동물의 3/4을 차지할 정도로 그 수가 많고 종류가 매우 다양하다. 
							만약 곤충을 작고 징그러운 벌레로만 생각하는 사람이 있다면 이곳에서 곤충이 얼마나 
							치열하게 살아가고 있는지 그들만의 세계를 들여다보자. 
						</p>
						<ul class="circle-list teal">
							<li>
								주요전시물
								<ul>
									<li>곤충생태실: 육상과 수서곤충, 나비, 거미의 생태환경 및 실제 곤충을 직접 체험</li>
									<li>
										곤충표본실: 곤충 표본과 생태디오라마 전시
									</li>
									<li>
										살아 있는 곤충 체험: 장수풍뎅이, 수서곤충 등
									</li>
								</ul>
							</li>
						</ul>
					</div>     
					<div class="col-sm-12 col-md-8 col-lg-8 text-right">
						<!-- select dropdown : 다른관으로 이동 -->
						<c:import url="/WEB-INF/views/display/displayHallSelect.jsp"/>
						<div>
							<img alt="곤충생태관" src="<c:url value='/resources/img/display/map/11.png'/>"
								class="img-responsive margin-top20">
						</div>
					</div>
				</div>
			</div>
			<div id="major-exhibitions-spy" class="section scrollspy">
				<div class="row">
					<div class="col-xs-4 col-md-2">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							주요전시물					
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="main-exhibition-wrapper">
							<div id="main-exhibitions">
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div id="story-spy" class="story-telling-wrapper">
				<div class="header">
					<div class="row">
						<div class="col-md-6">
							<h3 class="page-title teal">
								<div class="top-line"></div>
								스토리로 읽어 보는 생태관 여행기 
							</h3>
						</div>
					</div>
				</div>
				<div class="description">
					야외 공간으로 나오면 과학관 내부 전시관에서 봤던 주제들을 함축하는 전시물들이 곳곳에 
					보입니다. 함께 온 친구, 가족들과 함께 야외 공간을 거닐다 보면 우주 여행을 떠날 수 있는 
					로켓과 실물크기의 기차들도 볼 수 있습니다. 또, 공룡 동산에서는 다양한 공룡들이 과학관을 산책 하는 듯 
					생동감 넘치게 전시되어 있습니다. 
					과학관에서 봤던 전시물 이야기를 나누며 야외 전시물을 천천히 감상해보세요.
				</div>
				
				<div id="stories" class="row">
				</div>
			</div>
				
			<div class="related-photos">
				<div class="title">
					<i class="color-darkgreen material-icons">&#xE413;</i>관련사진
				</div>
				<div id="image-slider">
					<div id="image-slider-window">
						<img src="<c:url value="/resources/img/display/outdoorEcological/insectarium/related_photo_01.png"/>" 
							data-url="/resources/img/display/outdoorEcological/insectarium/related_photo_01.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/outdoorEcological/insectarium/related_photo_02.png"/>" 
							data-url="/resources/img/display/outdoorEcological/insectarium/related_photo_02.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/outdoorEcological/insectarium/related_photo_03.png"/>" 
							data-url="/resources/img/display/outdoorEcological/insectarium/related_photo_03.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/outdoorEcological/insectarium/related_photo_04.png"/>" 
							data-url="/resources/img/display/outdoorEcological/insectarium/related_photo_04.png" class="slide"/>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script id="story-template" type="text/x-handlebars-template">
{{#each stories}}
<div class="col-xs-12 col-sm-6 col-md-6">
	<div class="display-card story-telling story-more-btn pointer" data-idx="{{@index}}" onclick="">
		<div class="card-image">
			<img src="{{thumbnailUrl}}" class="img-responsive"/>
		</div>
		<div class="card-content">
			<div class="display-card-title">
				{{title}}
			</div>
			<div class="display-card-content">
				{{{desc}}}
			</div>
			
		</div>
	</div>							
</div>
{{/each}}
</script>

<script type="text/javascript">
	var stories = [
		{
			title: "[곤충생태관]",
			desc: "곤충 하면 눈살을 찌푸리는 엄마도, 삭막한 도시 공간에서 살아있는 곤충을 보기 힘든 아이도, "
				+ "동심으로 돌아가 곤충과 함께하는 시간이 그리운 아빠도 이 곳에서는 함께 웃을 수 있습니다. "
				+ "곤충생태관을 들어서면 시골마을을 여행하는 것 같은 기분이 듭니다.",
			thumbnailUrl: "<c:url value="/resources/img/display/outdoorEcological/insectarium/story18_02.jpg"/>",
			stories: [
				{
					storyLabel: "곤충생태관",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/outdoorEcological/insectarium/story18_01.jpg"/>"
				},
				{
					storyLabel: "곤충생태관",
					storyDesc: "풀벌레 소리가 들리시나요? 아직 이 정도로 감동하기는 이릅니다. "
						+ "그 동안 벌을 보면 쏘일 까봐 도망 다니셨죠? 곤충생태관에선 물지 않는 벌이 있어 직접 만져볼 수 있고, "
						+ "촉감만으로 어떤 곤충인지 만져보고 맞춰볼 수 있는 공간도 있어 재미를 더해줍니다. 육상곤충, 수서곤충, 각종 벌레 들의 "
						+ "실제 모습과 사라져 가는 곤충들을 보며 곤충과 더 가까워질 수 있는 시간을 가질 수도 있습니다.",
					imgUrl: "<c:url value="/resources/img/display/outdoorEcological/insectarium/story18_02.jpg"/>"
				},
				{
					storyLabel: "곤충생태관",
					storyDesc: "풀벌레 소리가 들리시나요? 아직 이 정도로 감동하기는 이릅니다. "
						+ "그 동안 벌을 보면 쏘일 까봐 도망 다니셨죠? 곤충생태관에선 물지 않는 벌이 있어 직접 만져볼 수 있고, "
						+ "촉감만으로 어떤 곤충인지 만져보고 맞춰볼 수 있는 공간도 있어 재미를 더해줍니다. 육상곤충, 수서곤충, 각종 벌레 들의 "
						+ "실제 모습과 사라져 가는 곤충들을 보며 곤충과 더 가까워질 수 있는 시간을 가질 수도 있습니다.",
					imgUrl: "<c:url value="/resources/img/display/outdoorEcological/insectarium/story18_03.jpg"/>"
				},
				{
					storyLabel: "나비의 생애",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/outdoorEcological/insectarium/story18_04.jpg"/>"
				},
				{
					storyLabel: "나비의 생애",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/outdoorEcological/insectarium/story18_05.jpg"/>"
				},
				{
					storyLabel: "나비의 생애",
					storyDesc: "나비의 생애를 한 눈에 볼 수 있는 곳도 있으니 절대 놓치지 마세요. "
							+ "애벌레가 나비가 되기까지 모든 과정을 보고, 화려하게 날개 짓을 하는 나비를 본다면 "
							+ "예전의 감동보다 더 큰 감동을 느낄 수 있을 테니까요. 나비 날개 돋이 하는 모습, 긴 대롱을 "
							+ "이용해 꿀을 마시는 모습을 직접 눈으로 보는 것은 곤충생태관에서만 선물 받을 수 있는 소중한 경험이 될 것입니다.",
					imgUrl: "<c:url value="/resources/img/display/outdoorEcological/insectarium/story18_06.jpg"/>"
				}
				
			]
		},
		{
			title: "[생태공원]",
			desc: "생태 공원은 과학과 함께 휴식을 느낄 수 있는 공간으로 구성되어 있습니다. 학생들은 "
				+ "실물보다 훨씬 큰 곤충 모형들을 보며 웃음을 터뜨리고, 가족들은 찰스 다윈상과 알프레드 "
				+ "러셀 월리스, 그레고어 멘델과 기념 사진을 찍기도 하네요! 실제 인물이냐고요? 물론 아니죠! "
				+ "과학자들의 모습을 그대로 청동으로 만들어 놓은 동상이니까 함께 기념 사진을 남겨보세요. "
				+ "원두막, 물레방아, 디딜방아 같은 우리 선조들의 지혜가 담긴 옥외 전시물들도 놓치지 말고 구경해보세요.",
			thumbnailUrl: "<c:url value="/resources/img/display/outdoorEcological/ecoPark/story19_01.jpg"/>",
			stories: [
				{
					storyLabel: "생태공원",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/outdoorEcological/ecoPark/story19_01.jpg"/>"
				},
				{
					storyLabel: "생태공원",
					storyDesc: "생태 공원은 과학과 함께 휴식을 느낄 수 있는 공간으로 구성되어 있습니다. 학생들은 "
						+ "실물보다 훨씬 큰 곤충 모형들을 보며 웃음을 터뜨리고, 가족들은 찰스 다윈상과 알프레드 "
						+ "러셀 월리스, 그레고어 멘델과 기념 사진을 찍기도 하네요! 실제 인물이냐고요? 물론 아니죠! "
						+ "과학자들의 모습을 그대로 청동으로 만들어 놓은 동상이니까 함께 기념 사진을 남겨보세요. "
						+ "원두막, 물레방아, 디딜방아 같은 우리 선조들의 지혜가 담긴 옥외 전시물들도 놓치지 말고 구경해보세요.",
					imgUrl: "<c:url value="/resources/img/display/outdoorEcological/ecoPark/story19_02.jpg"/>"
				}
			]
		}
	];
	
	var storySource   = $("#story-template").html();
	var storyTemplate = Handlebars.compile(storySource);
	var storyHtml = storyTemplate({stories: stories});
	$("#stories").html(storyHtml);
</script>

<c:url var="shareUrl" value="/display/outdoorEcological/insectarium" />
<c:import url="/WEB-INF/views/display/displayDetailModal.jsp">
	<c:param name="shareUrl" value="${shareUrl }"/>
	<c:param name="exhibitionName" value="곤충생태관" />
</c:import>


<script type="text/javascript">

	$(document).ready( function() {

		// image slider
		PTechSlider('#image-slider').autoSlide();
		
		// 전시관 지도 flash outdoor로 전환.
		//setDefaultSVGMap('#basic-map-spy',3);
		// 지도 flash 해당 전시관 표시
		//$("g[id='XMLID_97_']").attr('class', 'active');
	});
	
	function showHideDetails()  {
		// hide all details first.
		$('.exhibition-detail-wrapper').hide();
		// show the active detail only.
		var exhibit_active_id = $('.major-exhibition-wrapper.active').data('id');
		$(exhibit_active_id).show();
		
		// click event for major exhibition box
		$('.major-exhibition-wrapper').on('click', function() {
			
			// get current active div & remove active
			var hide_id = $('.major-exhibition-wrapper.active').data('id');
			$('.major-exhibition-wrapper.active').removeClass('active');
			// hide the matching detail
			$(hide_id).hide();
			
			// add 'active' to the current clicked div
			var show_id = $(this).data('id');
			$(this).addClass('active');
			// show the mathcing detail
			$(show_id).show();
			
		});
	}

</script>


<!-- Major Exhibition Contents Loading -->
<c:import url="/WEB-INF/views/display/majorExhibitionTemplate.jsp" />
<script type="text/javascript">
	var exhibitions = [
		{
			descriptions : [
				
				{
					title: "수서곤충과 육상곤충, 인간과 함께 살아가다",
					desc : "수서곤충은 일생의 전부 또는 그 일부를 물에서 생활하는 곤충을, 육상곤충은 땅 위에서 생활하는 곤충을 말한다. "
						+ "이들은 천적으로부터 몸을 보호하는 등 자신의 생존을 위해 여러 가지로 적응해 가는 모습을 보인다. "
						+ "곤충생태관에서는 곤충이 살아 움직이는 모습을 눈으로 관찰할 수 있으며, 이름과 관련된 여러 가지 "
						+ "특징에 대해 찾아보는 재미도 있다."
				},
				{
					title: "꿀벌, 맛있는 꿀을 전해줘요",
					desc : "귀엽고 친숙한 곤충으로 알려진 꿀벌을 직접 눈으로 확인해 볼 수 있다. 벌에게 쏘일까봐 두려워 벌집을 "
						+ "멀리서 구경했던 경험이 있다면 이곳에서는 그런 걱정을 할 필요가 없다. 여기서는 벌들이 꿀을 찾아 "
						+ "이동하는 모습과 벌집을 짓는 모습, 또 그들이 지은 벌집을 매우 가까이에서 자세히 관찰할 수 있다."
				}
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/outdoorEcological/insectarium/major_exhibition_01.png' />",
					caption: "[곤충생태관 전경]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/outdoorEcological/insectarium/major_exhibition_02.png' />",
					caption: "[꿀벌 전시대]"
				},
			]
		},
		{
			descriptions : [
				
				{
					title: "장수풍뎅이, 숲 속의 천하장사",
					desc : "장수풍뎅이와 애벌레를 직접 만져볼 수 있다. 단단한 등껍질과 투구를 씌운 듯한 장수풍뎅이를 직접 만져보면, "
						+ "왜 장수풍뎅이가 곤충의 세계에서 돋보이는 힘꾼인지 알 수 있다. 꿈틀꿈틀 움직이는 애벌레를 손으로 만져보자. "
						+ "그들의 강한 생명력을 느낄 수 있을 것이다."
				},
				{
					title: "나비정원, 팔랑팔랑~ 나비를 만날 수 있는 곳",
					desc : "따뜻한 날씨가 되면 하늘에 수를 놓은 듯 날아다니는 나비를 사계절 만나볼 수 있다. 나비정원에는 다양한 나비가 살고 있는데, "
						+ "그들이 아름답게 날고 있는 모습을 가까이에서 볼 수 있다. 애벌레가 인내의 시간을 겪고 아름다운 나비로 변화하는 과정을 "
						+ "직접 눈으로 관찰함으로써 생명의 소중함과 강인함을 느낄 수 있다."
				}
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/outdoorEcological/insectarium/major_exhibition_03.png' />",
					caption: "[장수풍뎅이 체험]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/outdoorEcological/insectarium/major_exhibition_04.png' />",
					caption: "[나비정원]"
				},
			]
		},
		{
			descriptions : [
				
				{
					title: "곤충사육실, 사육실 현장의 모습을 감상한다",
					desc : "곤충들의 사육과정을 눈으로 확인할 수 있다. 곤충사육실 안에서 곤충들이 어떻게 자라나고 "
						+ "있는지를 유리창 너머로 관찰해보는 것도 하나의 흥밋거리이다."
				},
				{
					title: "곤충관찰노트, 곤충생태관만의 차별적 전시해설",
					desc : "매달 하나의 곤충을 선정하여 진행되는 ‘곤충관찰노트’는 우리가 잘 알지 못했던 곤충을 "
						+ "눈으로 직접 관찰하고 만져도 보는 전시해설 프로그램이다."          
                        + "<div class='additional-info'>(20명/회, 누구나)</div>"
				}
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/outdoorEcological/insectarium/major_exhibition_05.png' />",
					caption: "[곤충사육실]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/outdoorEcological/insectarium/major_exhibition_06.png' />",
					caption: "[곤충관찰노트]"
				},
			]
		}
		
	];
	
	var exhibitionSource   = $("#major-exhibition-template").html();
	var exhibitionTemplate = Handlebars.compile(exhibitionSource);
	var exhibitionHtml = exhibitionTemplate({exhibitions: exhibitions});
	$("#main-exhibitions").html(exhibitionHtml);
	
</script>