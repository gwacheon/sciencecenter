<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-teal">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>
	<div id="display-body" class="sub-body">
		<div class="narrow-sub-top display">
			<div class="sub-banner-tab-wrapper sub-banner-tab-teal">
				<!-- <div class="container">
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
							<ul class="nav nav-tabs">
								<li class="col-xs-12 active">
									<a href="http://cyber.scientorium.go.kr/museum_2015/vrLoad.jsp?pavilion=roadview&xml_name=otr_008.xml&angle=180" class="item">
										<i class="fa fa-desktop fa-lg" aria-hidden="true"></i> 사이버 전시관
									</a>  
								</li>
							</ul>   
						</div>
					</div>
				</div> -->
			</div>			
		</div>
		<div class="container">
			<div id="basic-map-spy" class="section scrollspy">
				<div class="row">
					<div class="col-sm-3 col-md-2 col-lg-2">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							공룡역사광장 안내
						</h3>
					</div>
				</div>
				<div class="row description">
					<div class="col-sm-12 col-md-4 col-lg-4">
						<ul class="circle-list teal">
							<li>
								공룡동산
								<ul>
									<li>
										공룡동산은 중생대의 대표적인 공룡(7종) 모형을 실물 크기로 제작 전시하여
										휴식과 학습을 겸할 수 있는 테마파크입니다. 공룡알과 공룡 발자국은 공룡이 살아 뛰노는 듯한
										생생한 현장감과 아이들의 상상력을 무한히 자극합니다.
									</li>
								</ul>
							</li>
							<li>
								역사의광장
								<ul>
									<li>
										역사의 광장역사의 광장에는 고천문과 과학기술 선현을 통해 
										천문학, 기계기술, 지리학 등 9개 분야에 과학 선현의 정보가 기술되어 있고, 
										천문관련 기록 및 기구들에 대한 모형을 실제크기로 재현, 
										전시하여 원리를 이해할  수 있으며 대리석에 음각으로 조각된 전시물을 통해 정보를 얻을 수 있습니다.
									</li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="col-sm-12 col-md-8 col-lg-8 text-right">
						<!-- select dropdown : 다른관으로 이동 -->
						<c:import url="/WEB-INF/views/display/displayHallSelect.jsp"/>
						<div>
							<c:import url="/WEB-INF/views/display/mainBuilding/svgs/display_svg_map_modified.jsp"></c:import>
						</div>
					</div> 
				</div>
			</div>
			<div id="major-exhibitions-spy" class="scrollspy">
				<div class="row">
					<div class="col-sm-3 col-md-2 col-lg-2">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							주요 전시물
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="main-exhibition-wrapper">
							<div id="main-exhibitions">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	$(document).ready( function() {
		// 이미지 슬라이더
		//PTechSlider('#image-slider').autoSlide();
		PTechSlider('#image-slider',
			{
			slidesPerScreen: 4,
			indicators: true,
			height: 150
			}		
		);
		
		// 전시관 지도 flash outdoor로 전환.
		setDefaultSVGMap('#basic-map-spy',3);
		// 지도 flash 해당 전시관 표시
		$("g[id='XMLID_120_']").attr('class', 'active');	//공룡지질동산
		$("g[id='XMLID_112_']").attr('class', 'active');	//역사의 광장
	});
	
	
</script>



<!-- Major Exhibition Contents Loading -->
<c:import url="/WEB-INF/views/display/majorExhibitionTemplate.jsp" />
<script type="text/javascript">
	var exhibitions = [
		{
			descriptions : [
				
				{
					title: "공룡·지질동산, 과거를 만나다",
					desc : "공룡동산은 과학관 동쪽 출입문에서 바로 보인다. 중생대의 대표적인 공룡 7종과 "
						+ "공룡알 및 발자국 화석을 실물 모습 그대로 복원해 놓았다. 당시와 유사한 모습으로 "
						+ "숲을 함께 조성하여 휴식과 학습을 겸할 수 있으며, 생생한 현장감과 아이들의 상상력을 "
						+ "자극할 수 있는 공간이다."
				},
				{
					title: "",
					desc : "지질동산은 공룡동산 옆에 있다. 죽어서 돌이 된 나무, 용암이 만든 아름다운 조형미의 주상절리, "
						+ "한반도 진화의 역사를 간직한 지층을 통해 지층과 암석의 생성이라는 신비한 지질현상을 탐구할 수 있다."
				},
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/outdoorEcological/dinosaurAndHistory/major_exhibition_01.png' />",
					caption: "[공룡동산]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/outdoorEcological/dinosaurAndHistory/major_exhibition_02.png' />",
					caption: "[지질동산]"
				}
			]
		},
		{
			descriptions : [
				
				{
					title: "역사광장, 선현의 과학기술을 만나다",
					desc : "옥외전시장 중앙에 넓게 자리 잡은 역사광장은 우리나라 고천문학과 과학기술을 살펴볼 "
						+ "수 있는 공간이다. 천문학, 기계기술, 지리학을 포함한 9개 분야가 전시되어 있으며, "
						+ "특히 규표와 앙부일구를 통해 15세기에 자주적으로 개발한 조선시대의 시간을 확인할 수 있다."
				},
				{
					title: "",
					desc : "이 외에도 도예작품을 감상하고 직접 만들어 보면서 그 안에 담긴 과학 원리를 배우는 도예체험공방, "
						+ "다양한 놀이시설을 체험하면서 원심력, 도르래, 관성의 원리를 깨우치는 과학놀이터가 야외 공간에 펼쳐져 있다."
				},
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/outdoorEcological/dinosaurAndHistory/major_exhibition_03.png' />",
					caption: "[앙부일구]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/outdoorEcological/dinosaurAndHistory/major_exhibition_04.png' />",
					caption: "[도예체험공방]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/outdoorEcological/dinosaurAndHistory/major_exhibition_05.png' />",
					caption: "[도예체험공방]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/outdoorEcological/dinosaurAndHistory/major_exhibition_06.png' />",
					caption: "[도예체험공방]"
				}
			]
		}
		
	];
	
	var exhibitionSource   = $("#major-exhibition-template").html();
	var exhibitionTemplate = Handlebars.compile(exhibitionSource);
	var exhibitionHtml = exhibitionTemplate({exhibitions: exhibitions});
	$("#main-exhibitions").html(exhibitionHtml);
	
</script>