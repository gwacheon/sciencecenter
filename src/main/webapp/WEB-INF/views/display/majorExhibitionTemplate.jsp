<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script id="major-exhibition-template" type="text/x-handlebars-template">
{{#each exhibitions}}
<div class="exhibition margin-bottom20">
	{{#each descriptions}}	
	<div class="description">
		<div class="title">
			{{title}}
		</div>
		<div class="desc">
			{{{emphasizeQuote desc}}}
		</div>
	</div>
	{{/each}}
	
	<div class="image-wrapper">
		<div class="row">
			{{#each images}}
			<div class="col-sm-12 col-md-6">
				<div class="image">
					<img src="{{imageUrl}}" alt="{{caption}}"/>
					<div class="caption">
						{{{caption}}}
					</div>
				</div>
			</div>
			{{/each}}
		</div>
	</div>
</div>
{{/each}}


</script>

<script type="text/javascript">
// 설명 중에 ‘ ’ 안에 포함되는 문자열은 <b></b> 를 추가하여, bold 체로 변환.
Handlebars.registerHelper('emphasizeQuote', function(text) {
    //text = Handlebars.Utils.escapeExpression(text);
    text = text.toString();
    text = text.replace(/[‘]/g,'<b>‘');
    text = text.replace(/[’]/g,'’</b>');
    return new Handlebars.SafeString(text);
});
</script>