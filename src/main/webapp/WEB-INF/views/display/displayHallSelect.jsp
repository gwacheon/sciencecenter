<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="dropdown hall-select hallselect-teal">
	<div class="visible-sm visible-xs margin-top20">
	</div>
	<a class="dropdown-toggle" type="button" data-toggle="dropdown" role="button" 
		aria-haspopup="true" aria-expanded="true">
		다른 관으로 이동<i class="fa fa-angle-down fa-lg" aria-hidden="true"></i>
	</a>
	<ul class='dropdown-menu'>
	 	<c:forEach var="j" begin="1" end="5">
			<spring:message var="sub_category" code="main.nav.catetory2.sub${j}" scope="application" text='' />
			<spring:message var="sub_category_url" code="main.nav.catetory2.sub${j}.url" scope="application" text='' />
			<c:if test="${not empty sub_category}">
				<li class="optgroup">
					<span><c:out value='${sub_category}'/></span>
				</li>
				<c:forEach var="k" begin="1" end="9">
					<spring:message var="sub_item" code="main.nav.catetory2.sub${j}.sub${k}" scope="application" text='' />
					<spring:message var="sub_item_url" code="main.nav.catetory2.sub${j}.sub${k}.url" scope="application" text='' />
					<c:if test="${not empty sub_item}">
						<li>
							<a href="<c:url value='${sub_item_url }' />">
								<c:out value="${sub_item }"/>
							</a>
						</li>					
					</c:if>
				</c:forEach>
			</c:if>
		</c:forEach>
	</ul>
</div>