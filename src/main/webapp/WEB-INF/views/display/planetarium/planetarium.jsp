<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-teal">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
		<%-- <nav id="scrollspy-nav"
			class="navbar-default navbar-static has-scroll">
			<div class="container">
				<ul class="nav navbar-nav">
					<c:import url="/WEB-INF/views/display/navbar.jsp" />
					<li><a href="#basic-info-spy" class="item"> 기본 안내 </a></li>
					<li><a href="#basic-map-spy" class="item"> 천체투영관 안내 </a></li>
					<li><a href="#major-exhibitions-spy" class="item"> 주요전시물 </a>
					</li>
					<li><a href="#story-spy" class="item"> 스토리 여행기 </a></li>
					<li><a href="#basic-guide-spy" class="item"> 천체투영관 이용안내 </a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<div class="bread-crumbs">전시관 / 천체투영관</div>
					</li>
				</ul>
			</div>
		</nav> --%>
	</div>
	<div id="display-body" class="sub-body">
		<div class="narrow-sub-top display">
			<div class="sub-banner-tab-wrapper sub-banner-tab-teal">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
							<ul class="nav nav-tabs">
								<li class="col-xs-12">
									<a href="javascript:void(0)" onclick="ChangeClass('CL7001')" class="item">
										<i class="fa fa-calendar-check-o fa-lg" aria-hidden="true"></i> 천체투영관 예약하기
									</a>
								</li>
							</ul>   
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div id="basic-map-spy" class="section scrollspy">
				<div class="row">
					<div class="col-sm-3 col-md-2 col-lg-2">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							천체투영관 안내
						</h3>
					</div>
				</div>
				<div class="row description">
					<div class="col-sm-12 col-md-4 col-lg-4">
						<p>지름 25m 돔스크린에 재현한 밤하늘 입체형상 속으로 떠나는 과학의 세계</p>
						<ul class="circle-list teal">
							<li class="margin-bottom20">유의사항
								<ul>
									<li>대상 : 만 5세 이상(5세 미만 유아(2013~2016년생)는 상영관 입장이 절대 불가합니다.)</li>
								</ul>
							</li>
							<li>주요전시물
								<ul>
									<li><b>「천체투영관」</b>은 별과 천체들을 투영하여 밤하늘을 연출하고 천문 분야를 비롯하여 다양한
										주제의 영상을 상영하는 돔 상영관이다. 직경 25m의 국내 최대 돔 스크린에 광학식 투영기와 디지털 투영
										시스템을 갖추고 있다. 이곳에서는 별자리 해설을 비롯해 ‘비행의 꿈’, ‘우주로의 여행’등 총 9개 영상을
										상영하고 있다. <br>(250명/회, 만 5세 이상, 유료)</li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="col-sm-12 col-md-8 col-lg-8 text-right">
						<!-- select dropdown : 다른관으로 이동 -->
						<c:import url="/WEB-INF/views/display/displayHallSelect.jsp" />
						<div>
							<img alt="천체투영관"
								src="<c:url value='/resources/img/display/map/10.png'/>"
								class="img-responsive margin-top20">
						</div>
					</div>
				</div>
			</div>
			<div id="major-exhibitions-spy" class="section scrollspy">
				<div class="row">
					<div class="col-sm-3 col-md-2 col-lg-2">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							주요전시물
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="main-exhibition-wrapper">
							<div id="main-exhibitions"></div>
						</div>
					</div>
				</div>
			</div>
			<div id="story-spy" class="story-telling-wrapper">
				<div class="header">
					<div class="row">
						<div class="col-md-6">
							<h3 class="page-title teal">
								<div class="top-line"></div>
								스토리로 읽어 보는 천문관 여행기
							</h3>
						</div>
					</div>
				</div>
				<div class="description">우리는 언제쯤 방학에 우주로 여행을 떠날 수 있을까요? 외계인과
					친구처럼 메신저를 주고 받을 수 있는 날은 언제가 될까요? 잘 알 수 없기에 더 궁금하고 신비로운 우주! 국립과천과학관에
					오면 다양한 모습의 우주를 만날 수 있습니다. 본관을 나와 넓은 야외 마당에 위치한 천체투영관부터 여행을 떠나보시죠!</div>

				<div id="stories" class="row"></div>
			</div>
			<div class="related-photos">
				<div class="title">
					<i class="color-darkgreen material-icons">&#xE413;</i> 관련사진
				</div>
				<div id="image-slider">
					<div id="image-slider-window">
						<img
							src="<c:url value="/resources/img/display/planetarium/planetarium/related/1.png"/>"
							data-url="/resources/img/display/planetarium/planetarium/related/1.png"
							class="slide" /> 
						<img
							src="<c:url value="/resources/img/display/planetarium/planetarium/related/2.png"/>"
							data-url="/resources/img/display/planetarium/planetarium/related/2.png"
							class="slide" /> 
						<img
							src="<c:url value="/resources/img/display/planetarium/planetarium/related/3.png"/>"
							data-url="/resources/img/display/planetarium/planetarium/related/3.png"
							class="slide" /> 
						<img
							src="<c:url value="/resources/img/display/planetarium/planetarium/related/4.png"/>"
							data-url="/resources/img/display/planetarium/planetarium/related/4.png"
							class="slide" />
							<img
							src="<c:url value="/resources/img/display/planetarium/planetarium/related/5.png"/>"
							data-url="/resources/img/display/planetarium/planetarium/related/5.png"
							class="slide" />
							<img
							src="<c:url value="/resources/img/display/planetarium/planetarium/related/6.png"/>"
							data-url="/resources/img/display/planetarium/planetarium/related/6.png"
							class="slide" />
					</div>
				</div>
				<!--  image-slider END ... -->
			</div>

			<script type="text/javascript">
				// 이미지 슬라이더
				//PTechSlider('#image-slider').autoSlide();
				PTechSlider('#image-slider', {
					slidesPerScreen : 4,
					indicators : true,
					height : 150
				});
			</script>

			<div id="basic-guide-spy" class="section scrollspy">
				<div class="row">
					<div class="col-xs-6 col-sm-4 col-md-3">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							천체투영관 이용안내
						</h3>
					</div>
				</div>

				<div class="display-guide-wrapper">
					<div class="sub-nav">
						<div class="row">
							<div class="col-xs-12 col-md-3">
								<a href="#" class="item" data-name="program">천체투영관 프로그램 안내</a>
							</div>
							<div class="col-xs-12 col-md-3">
								<a href="#" class="item" data-name="reservation">일반예약 안내</a>
							</div>
							<div class="col-xs-12 col-md-3">
								<a href="#" class="item" data-name="group-reservation">단체예약
									안내</a>
							</div>
							<div class="col-xs-12 col-md-3">
								<a href="#" class="item" data-name="movie-event">천체투영관 영화제</a>
							</div>
						</div>
					</div>

					<div id="program" class="guide-item">
						<div class="sub-section">
							<ul class="circle-list teal">
								<li class="sub-section-title">
									천체투영관 정규 프로그램 영상물 소개									
								</li>
							</ul>
							<div class="content">
								<div class="row">
									<div class="image-overlay-wrapper">
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
											<div class="image-item">
												<img
													src="<c:url value='/resources/img/display/planetarium/planetarium/program_01.png' />"
													class="img-responsive " />
												<div class="caption">우주로의 여행</div>
												<div class="overlay">
													<div class="title">우주로의 여행</div>
													<div class="desc">지구에서 우주 끝까지 다녀오는 가상 우주여행을 떠납니다. 우주가
														얼마나 크고 광대한지 느껴보세요.</div>
													<div class="info">
														<div class="header">상영시간</div>
														<div class="text">10분</div>
													</div>
													<div class="info">
														<div class="header">난이도</div>
														<div class="text">5세 이상 모든 연령대 관람 추천</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
											<div class="image-item">
												<img
													src="<c:url value='/resources/img/display/planetarium/planetarium/program_02.png' />"
													class="img-responsive " />
												<div class="caption">SETI: 외계생명체를 찾아서</div>
												<div class="overlay">
													<div class="title">SETI: 외계생명체를 찾아서</div>
													<div class="desc">지적 외계생명체 탐사 프로젝트 세티(Search for
														Extra-Terrestrial Intelligence)에 대해 들어봤나요? 외계인들이 존재할지, 있다면
														어떻게 생겼을지 함께 생각해봐요.</div>
													<div class="info">
														<div class="header">상영시간</div>
														<div class="text">25분</div>
													</div>
													<div class="info">
														<div class="header">난이도</div>
														<div class="text">초등생 이상 관람 추천</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
											<div class="image-item">
												<img
													src="<c:url value='/resources/img/display/planetarium/planetarium/program_03.png' />"
													class="img-responsive " />
												<div class="caption">코코몽의 우주탐험</div>
												<div class="overlay">
													<div class="title">코코몽의 우주탐험</div>
													<div class="desc">토성의 위성 타이탄에 사는 핼리와 지구의 코코몽이 친구가 되어
														세균킹을 무찌르는 우주탐험에 초대합니다.</div>
													<div class="info">
														<div class="header">상영시간</div>
														<div class="text">15분</div>
													</div>
													<div class="info">
														<div class="header">난이도</div>
														<div class="text">5세 이상 초등 저학년 관람 추천</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
											<div class="image-item">
												<img
													src="<c:url value='/resources/img/display/planetarium/planetarium/program_04.png' />"
													class="img-responsive " />
												<div class="caption">골디락스를 위한 행성</div>
												<div class="overlay">
													<div class="title">골디락스를 위한 행성</div>
													<div class="desc">생명체가 살기에 ‘딱 적당한’ 환경을 갖춘 외계행성을 찾기 위한
														과학자들의 노력과 결실을 알아보아요.</div>
													<div class="info">
														<div class="header">상영시간</div>
														<div class="text">25분</div>
													</div>
													<div class="info">
														<div class="header">난이도</div>
														<div class="text">중등 이상 관람 추천</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
											<div class="image-item">
												<img
													src="<c:url value='/resources/img/display/planetarium/planetarium/program_05.png' />"
													class="img-responsive " />
												<div class="caption">비행의 꿈</div>
												<div class="overlay">
													<div class="title">비행의 꿈</div>
													<div class="desc">빛조차 빠져나올 수 없다는 블랙홀의 정체와 블랙홀을 탐사하는
														방법을 알아보아요.</div>
													<div class="info">
														<div class="header">상영시간</div>
														<div class="text">25분</div>
													</div>
													<div class="info">
														<div class="header">난이도</div>
														<div class="text">중등 이상 관람 추천</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
											<div class="image-item">
												<img
													src="<c:url value='/resources/img/display/planetarium/planetarium/program_06.png' />"
													class="img-responsive " />
												<div class="caption">Back to the Moon</div>
												<div class="overlay">
													<div class="title">Back to the Moon</div>
													<div class="desc">가장 친숙한 천체, 지구와 달, 그리고 태양의 진실을 알아가는
														코요테의 생각여행에 함께해요.</div>
													<div class="info">
														<div class="header">상영시간</div>
														<div class="text">25분</div>
													</div>
													<div class="info">
														<div class="header">난이도</div>
														<div class="text">초등생 이상 관람 추천</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
											<div class="image-item">
												<img
													src="<c:url value='/resources/img/display/planetarium/planetarium/program_07.png' />"
													class="img-responsive " />
												<div class="caption">별을 향하여</div>
												<div class="overlay">
													<div class="title">별을 향하여</div>
													<div class="desc">캄캄한 밤하늘에 아름답게 빛나는 별들이 보여주는 탄생과 진화,
														죽음의 장면들을 함께 감상해보아요.</div>
													<div class="info">
														<div class="header">상영시간</div>
														<div class="text">25분</div>
													</div>
													<div class="info">
														<div class="header">난이도</div>
														<div class="text">중등 이상 관람 추천</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
											<div class="image-item">
												<img
													src="<c:url value='/resources/img/display/planetarium/planetarium/program_08.png' />"
													class="img-responsive " />
												<div class="caption">미션: 태양을 감시하라!</div>
												<div class="overlay">
													<div class="title">미션: 태양을 감시하라!</div>
													<div class="desc">우리의 에너지원인 태양이 생명을 위협하는 활동을 한다는데, 그런
														태양을 감시하는 과학자들의 이야기를 들어보세요</div>
													<div class="info">
														<div class="header">상영시간</div>
														<div class="text">12분</div>
													</div>
													<div class="info">
														<div class="header">난이도</div>
														<div class="text">초등생 이상 관람 추천</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
											<div class="image-item">
												<img
													src="<c:url value='/resources/img/display/planetarium/planetarium/program_09.png' />"
													class="img-responsive " />
												<div class="caption">태양계 여행</div>
												<div class="overlay">
													<div class="title">태양계 여행</div>
													<div class="desc">태양계 8개 행성과 소천체들 사이를 여행하며 태양계 구성원들의
														멋진 모습과 특징을 알아보아요.</div>
													<div class="info">
														<div class="header">상영시간</div>
														<div class="text">13분</div>
													</div>
													<div class="info">
														<div class="header">난이도</div>
														<div class="text">초등생 이상 관람 추천</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="sub-section">
							<div class="row">
								<div class="col-sm-12">
									<ul class="circle-list teal">
										<li>
											<span class="sub-section-title">
												천체투영관
											</span>
											<ul>
												<li>
													천체투영관은 하늘의 별과 별자리에 얽힌 이야기, 태양계 행성들의 모습, 우주의 탄생과 진화 등 신비한
													천문현상들을 전문가의 설명과 함께 감상하는 곳입니다.	
												</li>
												<li>
													(천체투영관 예약 변경 사항 안내)
													<ul class="arrow-list teal">
														<li>
															* 5세 미만 유아(2013~2016년생)는 상영관 입장이 절대 불가합니다.
														</li>
														<li>
															* 7월 16일(토)부터 8월 27일(토)까지 매주 토요일에는 릴레이 저자특강이 진행되며, 이로인해 6회(16:20) 프로그램이 운영되지 않습니다.(8월 13일(토) 제외)
														</li>
														<li>
															천체투영관 이용에 참고하시기 바랍니다.
														</li>
													</ul>
												</li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="sub-section">
							<div class="row">
								<div class="col-sm-12">
									<ul class="circle-list teal">
										<li class="sub-section-title">
											상영 일정 
										</li>
									</ul>
								</div>
							</div>
							<div class="content table-responsive">
								<table
									class="table table-bordered centered-table margin-bottom20">
									<thead>
										<tr>
											<th>회차</th>
											<th>화 목 토</th>
											<th>수 금 일</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1회(10:00 ~ 10:40)</td>
											<td>
												<div>숨은 별자리 찾기</div>
												<div>+</div>
												<div>별을 향하여</div>
											</td>
											<td>
												<div>숨은 별자리 찾기</div>
												<div>+</div>
												<div>SETI(외계생명체를 찾아서)</div>
											</td>
										</tr>
										<tr>
											<td>2회(11:00 ~ 11:40)</td>
											<td>
												<div>숨은 별자리 찾기</div>
												<div>+</div>
												<div>태양계 여행</div>
											</td>
											<td>
												<div>숨은 별자리 찾기</div>
												<div>+</div>
												<div>코코몽 우주탐험</div>
											</td>
										</tr>
										<tr>
											<td>3회(13:00 ~ 13:40)</td>
											<td>
												<div>숨은 별자리 찾기</div>
												<div>+</div>
												<div>우주로의 여행</div>
											</td>
											<td>
												<div>숨은 별자리 찾기</div>
												<div>+</div>
												<div>우주로의 여행</div>
											</td>
										</tr>
										<tr>
											<td>4회(14:00 ~ 14:40)</td>
											<td>
												<div>숨은 별자리 찾기</div>
												<div>+</div>
												<div>미션:태양을 감시하라</div>
											</td>
											<td>
												<div>숨은 별자리 찾기</div>
												<div>+</div>
												<div>골디락스를 위한 행성</div>
											</td>
										</tr>
										<tr>
											<td>5회(15:00 ~ 15:40)</td>
											<td>
												<div>숨은 별자리 찾기</div>
												<div>+</div>
												<div>코코몽 우주탐험</div>
											</td>
											<td>
												<div>숨은 별자리 찾기</div>
												<div>+</div>
												<div>Back to the Moon</div>
											</td>
										</tr>
										<tr>
											<td>6회(16:20 ~ 17:00)</td>
											<td>
												<div>비행의 꿈</div>
												<div>(숨은 별자리 찾기 운영없음)</div>
											</td>
											<td>
												<div>비행의 꿈</div>
												<div>(숨은 별자리 찾기 운영없음)</div>
											</td>
										</tr>
									</tbody>
								</table>
								※ 6회(16:20)프로그램은 '숨은 별자리 찾기'가 운영되지 않습니다.<br>
								※ 프로그램은 사정에 따라 변경될 수 있습니다.
							</div>
						</div>
						<!-- sub-section END ... -->
						<div class="sub-section">
							<div class="row">
								<div class="col-sm-12">
									<ul class="circle-list teal">
										<li class="sub-section-title">
											입장료
										</li>
									</ul>
								</div>
							</div>
							<div class="content table-responsive">
								<table
									class="table table-bordered centered-table margin-bottom20">
									<thead>
										<tr>
											<th colspan="2">구분</th>
											<th>요금</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>어른</td>
											<td>20 ~ 64세</td>
											<td>2,000원</td>
										</tr>
										<tr>
											<td>청소년 및 어린이</td>
											<td>5 ~ 19세</td>
											<td>1,000원</td>
										</tr>
										<tr>
											<td>노인</td>
											<td>65세 이상</td>
											<td>1,000원</td>
										</tr>
										<tr>
											<td rowspan="2" style="vertical-align: middle;">장애인 및
												국가유공자</td>
											<td>20세 이상</td>
											<td>1,000원</td>
										</tr>
										<tr>
											<td>5 ~ 19세 이상</td>
											<td>500원</td>
										</tr>
									</tbody>
								</table>
								<div class="notice">※5세 미만은 입장할 수 없습니다.</div>
								<div class="notice">※과학관 연간회원은 50% 할인 적용</div>
								<div class="notice">※중증 장애인(1급~3급) 동반 1인은 장애인 요금제 적용.</div>
								<div class="notice">※천체투영관은 무료입장 대상이 없습니다. (인솔교사 등)</div>
							</div>
						</div>
						<!-- sub-section END ... -->
						<div class="sub-section">
							<div class="row">
								<div class="col-sm-12">
									<ul class="circle-list teal">
										<li>
											<span class="sub-section-title">
												천체투영관 관람좌석 배정 안내
											</span>
											<ul class="arrow-list teal">
												<li>
													평일 배정
													<div class="table-responsive">
														<table
															class="table table-bordered centered-table margin-bottom20">
															<thead>
																<tr>
																	<th>단체예매석</th>
																	<th>일반(인터넷)예매석</th>
																	<th>현장구매석</th>
																	<th>총 좌석</th>
																</tr>
															</thead>
															<tbody>
																<tr>
																	<td>120</td>
																	<td>90</td>
																	<td>60</td>
																	<td>270</td>
																</tr>
															</tbody>
														</table>
													</div>
												</li>
												<li>
													주말 배정
													<table
														class="table table-bordered centered-table margin-bottom20">
														<thead>
															<tr>
																<th>단체예매석</th>
																<th>일반(인터넷)예매석</th>
																<th>현장구매석</th>
																<th>총 좌석</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>0</td>
																<td>150</td>
																<td>120</td>
																<td>270</td>
															</tr>
														</tbody>
													</table>
												</li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<!-- sub-section END ... -->
						<div class="sub-section">
							<div class="row">
								<div class="col-sm-12">
									<ul class="circle-list teal">
										<li>
											<span class="sub-section-title">
												입장권 예약 및 구매방법
											</span>
											<ul>
												<li>
													상단의 일반예약 안내와 단체예약 안내 페이지를 참고하세요.
												</li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<!--  sub-section END ... -->
						<div class="sub-section">
							<div class="row">
								<div class="col-sm-12">
									<ul class="circle-list teal">
										<li>
											<span class="sub-section-title">
												유의사항 및 문의
											</span>
											<ul class="arrow-list teal">
												<li>7월 16일(토)부터 8월 27일(토)까지 매주 토요일에는 릴레이 저자특강이 진행되며, 이로인해 6회(16:20) 프로그램이 운영되지 않습니다.(8월 13일(토) 제외)</li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
							<div class="detail">
								<ul class="green-arrow-list">
									
								</ul>

							</div>
						</div>
						<!--  sub-section END ... -->

					</div>
					<!--  id="program" END ... -->

					<div id="reservation" class="guide-item">
						<div class="sub-section">
							<div class="row">
								<div class="col-sm-12">
									<ul class="circle-list teal">
										<li>
											<span class="sub-section-title">
												천체투영관 일반 예약 변경 사항 안내
											</span>
											<ul class="arrow-list teal">
												<li>5세 미만 유아(2013~2016년생)는 상영관 입장이 절대 불가합니다.</li>
												<li>7월 16일(토)부터 8월 27일(토)까지 매주 토요일에는 릴레이 저자특강이 진행되며, 이로인해 6회(16:20) 프로그램이 운영되지 않습니다.(8월 13일(토) 제외)</li>
												<li>천체투영관 이용에 유의하시기 바랍니다.</li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<!--  sub-section END ... -->
						<div class="sub-section">
							<div class="row">
								<div class="col-sm-12">
									<ul class="circle-list teal">
										<li>
											<span class="sub-section-title">
												인터넷 예약 방법
											</span>
											<ul class="arrow-list teal">
												<li>
													인터넷 예약은 국립과천과학관 홈페이지에서만 가능
												</li>
												<li>
													예약을 희망하는 날짜와 시간을 선택하신 후, 관람료를 결제하시면 예약이 완료됩니다. (단 결제는 신용카드만 가능)
												</li>
												<li>
													관람일 기준으로 1인당 90장까지 구매 가능합니다. (주말 및 공휴일인 경우, 최대 150장)
												</li>
												<li>
													인터넷 예약은 오시려는 날 기준으로 2주전(14일) 오전 09시부터 하루 전(24:00시) 까지 가능합니다.
												</li>
												<li>
													예약 후 예약하신 관람 일에 매표소를 방문하셔서 예약을 확인하신 후 관람권을 수령하시고 입장하시면 됩니다.
												</li>
												<li>
													예약확인 시에는 예약자 성명, 예약번호, 회원가입 시 입력하신 휴대전화번호 확인이 필요하오니 예약증을 출력해 오시면 빠르게 티켓으로 교환하실 수 있습니다.
												</li>
												<li>
													예약결제 후 부분취소는 불가능 합니다. 날짜 및 예약인원을 변경하고 싶을 경우, 관람일 전날(24:00)까지 예약내용을 취소하시고 다시 예약을 하셔야 합니다.
												</li>
												<li>
													예약 확인 및 취소는 나의 예매내역에서 가능합니다.
													<ul>
														<li>관람일 전일 24:00까지 나의 예매 내역에서 취소 및 환불이 가능합니다.<br>
															예) 관람 일이 10일일 경우 9일 24:00 까지 취소 및 환불 가능
														</li>
														<li>관람일 당일에는 관람시간 30분전까지 과학관 매표소에서 직접 취소 또는 전화로 취소하시면
															과학관 매표소에서 직접 환불 처리해 드립니다.(인터넷 취소 불가)
														</li>
													</ul>
													<div class="notice">
														※관람시간 이후에는 취소 및 환불은 불가능하오니 주의하시기 바랍니다.
													</div>
												</li>
												<li>
													법인카드 사용 안내
													<ul>
														<li>
															개인 카드로 결제한 후 당일(오시는 날) 상영시간 30분전까지 과학관 매표소에서 '법인카드' 로 결제 수단을 변경하시면 됩니다.
														</li>
													</ul>
												</li>
											</ul>											
										</li>
									</ul>
								</div>
							</div>
						</div>
						<!--  sub-section END ... -->
					</div>
					<!--  id="reservation" END ... -->

					<div id="group-reservation" class="guide-item">
						<div class="sub-section">
							<div class="row">
								<div class="col-sm-12">
									<ul class="circle-list teal">
										<li>
											<span class="sub-section-title">
												천체투영관 단체 예약 변경 사항 안내
											</span>
											<ul class="arrow-list teal">
												<li>5세 미만 유아(2013~2016년생)는 단체일 경우에도 상영관 입장이 절대 불가합니다. </li>
												<li>천체투영관은 무료입장 대상이 없습니다(인솔교사 등).</li>
												<li>천체투영관 2016년 2학기(9월 ~ 12월)평일 단체예약은 7월 19일(화)부터 가능합니다. </li> 
												<li>10월 28일(금) ~ 11월 6일(일)은 국제천체투영관 영화제 기간으로 단체예약을 받지 않습니다.</li>
												<li>단, 일반예약(인터넷예약)이 가능한 날짜에는 전화예약이 불가합니다.</li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<!--  sub-section END ... -->
						<div class="sub-section">
							<div class="row">
								<div class="col-sm-12">
									<ul class="circle-list teal">
										<li>
											<span class="sub-section-title">
												단체예약 가능 범위 및 대상
											</span>
											<ul class="arrow-list teal">
												<li>초, 중, 고교, 대학생 및 교사(성인, 어린이집 5세반 이상 포함)</li>
												<li>50인 이상부터 120인 이하의 단체</li>
												<li>50인 미만, 주말(토,일), 1&dot;2&dot;8월(방학) 방문 시 일반예약(인터넷예약) 이용바랍니다.</li>
												<li>단, 일반예약(인터넷예약)이 가능한 날짜에는 전화예약이 불가합니다.</li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<!--  sub-section END ... -->
						<div class="sub-section">
							<div class="row">
								<div class="col-sm-12">
									<ul class="circle-list teal">
										<li>
											<span class="sub-section-title">
												상영 일정
											</span>
										</li>
									</ul>
								</div>
							</div>
							<div class="content table-responsive">
								<table
									class="table table-bordered centered-table margin-bottom20">
									<thead>
										<tr>
											<th>회차</th>
											<th>화 목 토</th>
											<th>수 금 일</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1회(10:00 ~ 10:40)</td>
											<td>
												<div>숨은 별자리 찾기</div>
												<div>+</div>
												<div>별을 향하여</div>
											</td>
											<td>
												<div>숨은 별자리 찾기</div>
												<div>+</div>
												<div>SETI(외계생명체를 찾아서)</div>
											</td>
										</tr>
										<tr>
											<td>2회(11:00 ~ 11:40)</td>
											<td>
												<div>숨은 별자리 찾기</div>
												<div>+</div>
												<div>태양계 여행</div>
											</td>
											<td>
												<div>숨은 별자리 찾기</div>
												<div>+</div>
												<div>코코몽 우주탐험</div>
											</td>
										</tr>
										<tr>
											<td>3회(13:00 ~ 13:40)</td>
											<td>
												<div>숨은 별자리 찾기</div>
												<div>+</div>
												<div>우주로의 여행</div>
											</td>
											<td>
												<div>숨은 별자리 찾기</div>
												<div>+</div>
												<div>우주로의 여행</div>
											</td>
										</tr>
										<tr>
											<td>4회(14:00 ~ 14:40)</td>
											<td>
												<div>숨은 별자리 찾기</div>
												<div>+</div>
												<div>미션:태양을 감시하라</div>
											</td>
											<td>
												<div>숨은 별자리 찾기</div>
												<div>+</div>
												<div>골디락스를 위한 행성</div>
											</td>
										</tr>
										<tr>
											<td>5회(15:00 ~ 15:40)</td>
											<td>
												<div>숨은 별자리 찾기</div>
												<div>+</div>
												<div>코코몽 우주탐험</div>
											</td>
											<td>
												<div>숨은 별자리 찾기</div>
												<div>+</div>
												<div>Back to the Moon</div>
											</td>
										</tr>
										<tr>
											<td>6회(16:20 ~ 17:00)</td>
											<td>
												<div>비행의 꿈</div>
												<div>(숨은 별자리 찾기 운영없음)</div>
											</td>
											<td>
												<div>비행의 꿈</div>
												<div>(숨은 별자리 찾기 운영없음)</div>
											</td>
										</tr>
									</tbody>
								</table>
								※ 6회(16:20)프로그램은 '숨은 별자리 찾기'가 운영되지 않습니다.<br>
								※ 프로그램은 사정에 따라 변경될 수 있습니다.
							</div>
						</div>
						<!--  sub-section END ... -->
						<div class="sub-section">
							<div class="row">
								<div class="col-sm-12">
									<ul class="circle-list teal">
										<li>
											<span class="sub-section-title">
												유의사항
											</span>
											<ul class="arrow-list teal">
												<li>상영 시작 이후 입장 절대 불가 (상영시작 20분전까지 천체투영관 입구로 모이시기 바랍니다.)</li>
												<li>상영관 내 음식 반입 및 핸드폰, 카메라 사용금지</li>			
											</ul>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<!--  sub-section END ... -->
						<div class="sub-section">
							<div class="row">
								<div class="col-sm-12">
									<ul class="circle-list teal">
										<li>
											<span class="sub-section-title">
												기타 문의
											</span>
											<ul class="circle-list teal">
												<li>
													02-3677-1561(문의시간 : 09:30~17:30, 월요일 휴관)
												</li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<!--  sub-section END ... -->
					</div>
					<!--  id="group-reservation" END ... -->
					<div id="movie-event" class="guide-item">
						<div class="sub-section">
							<div class="row">
								<div class="col-md-12">
									<ul class="circle-list teal">
					                	<li>
					                    	<span class="sub-section-title">제 4회 국제천체투영관영화제</span>
					                    	<ul>
						                        <li>
						                        	사람들이 영화제를 찾는 이유 중 하나는 평소 만나볼 수 없는 특별한 영화들을 통해서 추억을 만들어 볼 수 있기 때문이죠. 돔 영화를 즐길 수 있는 천체투영관은 과천과학관의 전시관 중 관람객들이 좋아하는 베스트 10 중에서 하나로 꼽히는 전시코스입니다. 별과 은하수 및 별자리를 볼 수 있는 광학식 천체 투영기, 시뮬레이션 및 영상물을 상영할 수 있도록 하는 디지털 돔 영상장치, 천장을 바라볼 수 있도록 젖혀지는 특수 좌석 등이 겸비된 특수 상영관으로 전시장을 찾은 관객은 신비로운 천체 현상을 보며 우주체험을 할 수 있습니다. 
						                        </li>
						                        <li>
						                        	국제천체투영관영화제는 2010년 제1회를 시작으로 격년 개최하고 있는데요. 4번째를 맞이하는 이번 영화제에선 국내에서 상영된 적이 없는 우수 SF영화 11편을 선정하여 상영할 예정입니다. 2014년에는 5회가 상영되었지만 이번 2016년 영화제에선 총 7회로 회차가 추가되어 더욱더 다양한 영화를 만나볼 수 있습니다. 이번 영화제를 뜨겁게 달굴 만한 핫한 영상물들이 지금 여러분 곁을 찾아갑니다. 
						                        </li>
						                    </ul>
						                    <ul class="text-bold">
						                        <li>
						                        	상영 안내 
							                        <ul>
							                        	<li>
															기 간 : 2016년 10월 28일(금) ~ 11월 6일(일), 10일간
															<ul class="color-red">
																<li>
																	개막식: 2016년 10월 28일(금)
																</li>
																<li>
																	10:00 ~ 12:30 오로라 돔 영화 상영 및 촬영 제작 과정 강연 (권오철 작가)
																</li>
																<li>
																	14:00 ~ 17:00 영화제 출품작 상영
																</li>
															</ul>
							                            </li>
							                            <li>
							                              상영 시간 : 7회 / 일 (10시, 11시, 12시, 1시, 2시, 3시, 4시)
							                            </li>
							                            <li>
							                              관람료 : 성인 3000원, 아동 1500원 (특별요금으로 연간, 우대 등 할인 적용 없습니다.)
							                            </li>
							                            <li>  
							                              장 소 : 천체투영관
							                            </li>
							                       	</ul> 
												</li>
											</ul>
											<ul>
												<li>
													상영시간표
													<table class="table centered-table table-bordered">
														<thead>
															<tr>
																<th rowspan="2">
																	회차
																</th>
																<th rowspan="2">
																	프로그램 시간
																</th>
																<th colspan="2">
																	요일별 상영 정보
																</th>
															</tr>
															<tr>
																<th>
																	화․목․토
																</th>
																<th>
																	수․금․일
																</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>
																	1
																</td>
																<td>
																	10:00~10:30
																</td>
																<td>
																	Aurora : Lights of Wonder<br>
																	생명의 빛 오로라(수화영상제공)
																</td>
																<td>
																	Capturing the Cosmos<br>
																	우주를 점령하다
																</td>
															</tr>
															<tr>
																<td>
																	2
																</td>
																<td>
																	10:50~11:20
																</td>
																<td>
																	Capturing the Cosmos<br>
																	우주를 점령하다
																</td>
																<td>
																	Aurora : Lights of Wonder<br>
																	생명의 빛 오로라(수화영상제공)
																</td>
															</tr>
															<tr>
																<td>
																	3
																</td>
																<td>
																	11:40~12:10
																</td>
																<td>
																	Solar Superstorms<br>
																	솔라 슈퍼스톰
																</td>
																<td>
																	We Are Stars<br>
																	우리는 별이다
																</td>
															</tr>
															<tr>
																<td>
																	4
																</td>
																<td>
																	13:30~14:00
																</td>
																<td>
																	POLARIS<br>폴라리스
																</td>
																<td>
																	The Secrets of Gravity<br>중력의 비밀
																</td>
															</tr>
															<tr>
																<td>
																	5
																</td>
																<td>
																	14:30~15:00
																</td>
																<td>
																	The Secrets of Gravity<br>중력의 비밀
																</td>
																<td>
																	POLARIS<br>폴라리스
																</td>
															</tr>
															<tr>
																<td>
																	6
																</td>
																<td>
																	15:30~16:00
																</td>
																<td>
																	Remains of the Dinosaurs<br>공룡의 기억
																</td>
																<td>
																	The Hot and Energetic Universe<br>뜨겁고 강렬한 우주 
																</td>
															</tr>
															<tr>
																<td>
																	7
																</td>
																<td>
																	16:30~17:00
																</td>
																<td>
																	World 2 War<br>제2차 세계 대전
																</td>
																<td>
																	Flower Universe 플라워 유니버스, <br>Hoshimiru Ozisan 별을 보는 할아버지 
																</td>
															</tr>
														</tbody>
													</table>
												</li>
											</ul>
					                    </li>
									</ul>
								</div>
							</div>
						</div>
						<!--  sub-section END ... -->
						
						<div class="sub-section">
							<div class="content">
								<div class="row">
									<div class="image-overlay-wrapper movie">
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
											<div class="image-item">
												<img
													src="<c:url value='/resources/img/display/planetarium/planetarium/movie_07.png' />"
													class="img-responsive " />
												<div class="overlay">
													<div class="title">Aurora : Lights of Wonder</div>
													<div class="desc">인간이 자연에서 경험할 수 있는 최고의 경이로움인 오로라를 세계최초로 4K real time VR 영상으로 담았어요. 천체사진작가인 권오철 작가가 2015년 캐나다 옐로우 나이프에서 촬영한 오로라 영상을 느낄 수 있답니다. </div>
													<div class="info">
														<div class="header">
															장르 
														</div>
														<div class="text">
															자연
														</div>
													</div>
													<div class="info">
														<div class="header">
															제작사 
														</div>
														<div class="text">
															헉미디어
														</div>
													</div>
													<div class="info">
														<div class="header">
															시간 
														</div>
														<div class="text">
															29분
														</div>
													</div>
													<div class="info">
														<div class="header">
															국가 
														</div>
														<div class="text">
															한국
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
											<div class="image-item">
												<img
													src="<c:url value='/resources/img/display/planetarium/planetarium/movie_08.png' />"
													class="img-responsive " />
												<div class="overlay">
													<div class="title">Flower Universe</div>
													<div class="desc"> 마코토 아즈마는 플라워 아티스트에요. 그는 독특하고 상상력 넘치는 표현력으로 온갖 식물을 예술에 접목시키며 식물의 아름다움을 포착했어요. 이 쇼는 강렬한 fulldome 영상을 통해 신비한 세계를 경험할 수 있답니다.</div>
													<div class="info">
														<div class="header">
															장르 
														</div>
														<div class="text">
															자연
														</div>
													</div>
													<div class="info">
														<div class="header">
															제작사 
														</div>
														<div class="text">
															Winner Pictures
														</div>
													</div>
													<div class="info">
														<div class="header">
															시간 
														</div>
														<div class="text">
															10분
														</div>
													</div>
													<div class="info">
														<div class="header">
															국가 
														</div>
														<div class="text">
															일본
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
											<div class="image-item">
												<img
													src="<c:url value='/resources/img/display/planetarium/planetarium/movie_09.png' />"
													class="img-responsive " />
												<div class="overlay">
													<div class="title">Hoshimiru Ozisan</div>
													<div class="desc">
														"호시미루 아저씨“는 춘분(3월 21일 무렵)과 관련한 내용을 담은 공상과학 이야기에요.  
													</div>
													<div class="info">
														<div class="header">
															장르 
														</div>
														<div class="text">
															공상과학, 모험(애니메이션)
														</div>
													</div>
													<div class="info">
														<div class="header">
															제작사 
														</div>
														<div class="text">
															YUKO Namiki
														</div>
													</div>
													<div class="info">
														<div class="header">
															시간 
														</div>
														<div class="text">
															12분
														</div>
													</div>
													<div class="info">
														<div class="header">
															국가 
														</div>
														<div class="text">
															일본
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
											<div class="image-item">
												<img
													src="<c:url value='/resources/img/display/planetarium/planetarium/movie_10.png' />"
													class="img-responsive " />
												<div class="overlay">
													<div class="title">World 2 War</div>
													<div class="desc">
														2차 세계대전과 관련된 내용으로 액션이 풍부한 다큐멘터리에요. 기존 영상 자료와 사진 그리 고 극장식 전투 장면을 통해 생생하게 제2차 세계대전을 전해줍니다. 
													</div>
													<div class="info">
														<div class="header">
															장르 
														</div>
														<div class="text">
															액션
														</div>
													</div>
													<div class="info">
														<div class="header">
															제작사 
														</div>
														<div class="text">
															FullDome Lab
														</div>
													</div>
													<div class="info">
														<div class="header">
															시간 
														</div>
														<div class="text">
															29분
														</div>
													</div>
													<div class="info">
														<div class="header">
															국가 
														</div>
														<div class="text">
															네덜란드
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
											<div class="image-item">
												<img
													src="<c:url value='/resources/img/display/planetarium/planetarium/movie_01.png' />"
													class="img-responsive " />
												<div class="overlay">
													<div class="title">The Secrets of Gravity</div>
													<div class="desc">
														왜 물건들은 아래로 떨어질까요? 마법에 관심이 없는 젊은 마법사가 우주와 별의 매혹적인 비밀에 대해 들려주는 이야기입니다. 그는 그의 궁금증을 풀기 위해 아인슈타인 박물관을 몰래 침입합니다. 그 곳에서 영리한 로봇 Alby와 함께 시간과 공간을 넘나들며, 중력의 수수께끼를 파헤치며 모험을 떠나게 됩니다.
													</div>
													<div class="info">
														<div class="header">
															장르 
														</div>
														<div class="text">
															공상과학, 모험(애니메이션)
														</div>
													</div>
													<div class="info">
														<div class="header">
															제작사 
														</div>
														<div class="text">
															Softmachine
														</div>
													</div>
													<div class="info">
														<div class="header">
															시간 
														</div>
														<div class="text">
															28분
														</div>
													</div>
													<div class="info">
														<div class="header">
															국가 
														</div>
														<div class="text">
															독일
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
											<div class="image-item">
												<img
													src="<c:url value='/resources/img/display/planetarium/planetarium/movie_02.png' />"
													class="img-responsive " />
												<div class="overlay">
													<div class="title">We Are Stars</div>
													<div class="desc">
														우주 여행을 따라 우주의 기원과 생명 그 자체의 기원을 찾아가는 모험을 담고 있어요. 골룸의 목소리로 유명한, 배우 Andy Serkis의 나래이션으로 이야기는 펼쳐진답니다.
													</div>
													<div class="info">
														<div class="header">
															장르 
														</div>
														<div class="text">
															공상과학, 모험(애니메이션)
														</div>
													</div>
													<div class="info">
														<div class="header">
															제작사 
														</div>
														<div class="text">
															NSC Creative
														</div>
													</div>
													<div class="info">
														<div class="header">
															시간 
														</div>
														<div class="text">
															25분
														</div>
													</div>
													<div class="info">
														<div class="header">
															국가 
														</div>
														<div class="text">
															영국
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
											<div class="image-item">
												<img
													src="<c:url value='/resources/img/display/planetarium/planetarium/movie_02.png' />"
													class="img-responsive " />
												<div class="overlay">
													<div class="title">POLARIS</div>
													<div class="desc">
														남극에서 온 펭귄 제임스와 북극의 곰 블라디미르는 북극 빙하에서 만나게 됩니다. 그들은 친구가 되어 자기가 살고 있는 지역을 소개하고 별을 함께 관측하며 지구에서 극지의 밤이 왜 긴지 궁금해 합니다. 기발한 방법으로 관측소도 세우고 우주선도 만들어 우주여행을 하며 각 행성의 공통점과 차이점을 발견하게 됩니다.
													</div>
													<div class="info">
														<div class="header">
															장르 
														</div>
														<div class="text">
															공상과학, 모험(애니메이션)
														</div>
													</div>
													<div class="info">
														<div class="header">
															제작사 
														</div>
														<div class="text">
															Planetarium de Saint-Etienne
														</div>
													</div>
													<div class="info">
														<div class="header">
															시간 
														</div>
														<div class="text">
															29분
														</div>
													</div>
													<div class="info">
														<div class="header">
															국가 
														</div>
														<div class="text">
															프랑스
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
											<div class="image-item">
												<img
													src="<c:url value='/resources/img/display/planetarium/planetarium/movie_03.png' />"
													class="img-responsive " />
												<div class="overlay">
													<div class="title"> Remains of the Dinosaurs</div>
													<div class="desc">
														46억년 전 행성이 되지 못한 소행성들이 태양주변을 돌며 지구위로 떨어지는 운석이 되었어요. 지금부터 66만년전 지구에 떨어진 거대한 운석으로 지구 대부분 생물이 멸종하는 큰 사건이 일어나게 됩니다. 이때 멸망했다고 알려진 어떤 생물의 "진실"에 관한 이야기! 그 비밀스러운 이야기를 자세히 들여다볼까요?	
													</div>
													<div class="info">
														<div class="header">
															장르 
														</div>
														<div class="text">
															생명(다큐멘터리) 
														</div>
													</div>
													<div class="info">
														<div class="header">
															제작사 
														</div>
														<div class="text">
															KONICA MINOLTA
														</div>
													</div>
													<div class="info">
														<div class="header">
															시간 
														</div>
														<div class="text">
															25분
														</div>
													</div>
													<div class="info">
														<div class="header">
															국가 
														</div>
														<div class="text">
															일본
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
											<div class="image-item">
												<img
													src="<c:url value='/resources/img/display/planetarium/planetarium/movie_04.png' />"
													class="img-responsive " />
												<div class="overlay">
													<div class="title">The Hot and Energetic Universe</div>
													<div class="desc">
														뜨겁고 활동적인 우주는 현대 천문학이 어디까지 왔는지 보여주며 전자기복사의 기초 원리와 고에너지 천체물리학과 관련된 자연 현상을 보여줍니다. 
													</div>
													<div class="info">
														<div class="header">
															장르 
														</div>
														<div class="text">
															우주 
														</div>
													</div>
													<div class="info">
														<div class="header">
															제작사 
														</div>
														<div class="text">
															Planetarium production
														</div>
													</div>
													<div class="info">
														<div class="header">
															시간 
														</div>
														<div class="text">
															30분
														</div>
													</div>
													<div class="info">
														<div class="header">
															국가 
														</div>
														<div class="text">
															그리스
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
											<div class="image-item">
												<img
													src="<c:url value='/resources/img/display/planetarium/planetarium/movie_05.png' />"
													class="img-responsive " />
												<div class="overlay">
													<div class="title">Capturing the Cosmos</div>
													<div class="desc">
														과학자들은 우주를 매우 큰 단위로 연구합니다. 그들의 궁극적인 목적은 우주의 신비를 풀어내기 위해 여러 조각들을 퍼즐 맞추 듯 모으는 것입니다. 아카데미상을 수상한 배우 Geoffrey Rush의 내레이션으로 호주의 밤하늘을 감상하며 코스모스에 대한 새로운 것을 또 배우게 될지 기대하게 만듭니다.
													</div>
													<div class="info">
														<div class="header">
															장르 
														</div>
														<div class="text">
															우주 
														</div>
													</div>
													<div class="info">
														<div class="header">
															제작사 
														</div>
														<div class="text">
															Melbourne Planetarium
														</div>
													</div>
													<div class="info">
														<div class="header">
															시간 
														</div>
														<div class="text">
															26분
														</div>
													</div>
													<div class="info">
														<div class="header">
															국가 
														</div>
														<div class="text">
															호주
														</div>
													</div>
												</div>
											</div>
										</div>
										
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
											<div class="image-item">
												<img
													src="<c:url value='/resources/img/display/planetarium/planetarium/movie_11.png' />"
													class="img-responsive " />
												<div class="overlay">
													<div class="title">Solar Superstorms</div>
													<div class="desc">태양은 지구와 가장 가까운 행성이며, 지구 생명의 원천이에요. 태양 활동에 의해 표면의 거대한 폭발현상인 플레어 등과 같은 현상이 만들어지고, 특히 태양으로부터 불어오는 입자들의 바람인 태양풍은 지구에 큰 영향을 미치게 됩니다. 이러한 태양의 활발한 활동이 지구에 어떤 변화를 일으키는지 한번 들여다볼까요?</div>
													<div class="info">
														<div class="header">
															장르 
														</div>
														<div class="text">
															우주 
														</div>
													</div>
													<div class="info">
														<div class="header">
															제작사 
														</div>
														<div class="text">
															Spitz
														</div>
													</div>
													<div class="info">
														<div class="header">
															시간 
														</div>
														<div class="text">
															25분
														</div>
													</div>
													<div class="info">
														<div class="header">
															국가 
														</div>
														<div class="text">
															미국
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
						<!--  sub-section END ... -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- Major Exhibition Contents Loading -->
<c:import url="/WEB-INF/views/display/majorExhibitionTemplate.jsp" />
<script type="text/javascript">
	var exhibitions = [
			{
				descriptions : [

				{
					title : "별자리 해설, 오늘 밤하늘을 미리 만나다",
					desc : "바로 오늘 밤하늘의 모습을 똑같이 재현해 시간대별 밤하늘의 별자리 위치와 그에 얽힌 신화 이야기를 전문 요원이 들려준다. "
				}],
				images : [
						{
							imageUrl : "<c:url value='/resources/img/display/planetarium/planetarium/major_exhibition_01.png' />",
							caption : "[천체투영관 전경]"
						},
						{
							imageUrl : "<c:url value='/resources/img/display/planetarium/planetarium/major_exhibition_02.png' />",
							caption : "[별자리 해설]"
						}, ]
			},

	];

	var exhibitionSource = $("#major-exhibition-template").html();
	var exhibitionTemplate = Handlebars.compile(exhibitionSource);
	var exhibitionHtml = exhibitionTemplate({
		exhibitions : exhibitions
	});
	$("#main-exhibitions").html(exhibitionHtml);
</script>


<script id="story-template" type="text/x-handlebars-template">
{{#each stories}}
<div class="col-xs-12 col-sm-4 col-md-4">
	<div class="display-card story-telling story-more-btn pointer" data-idx="{{@index}}" onclick="">
		<div class="card-image">
			<img src="{{thumbnailUrl}}" class="img-responsive"/>
		</div>
		<div class="card-content">
			<div class="display-card-title">
				{{title}}
			</div>
			<div class="display-card-content">
				{{{desc}}}
			</div>
			
		</div>
	</div>							
</div>
{{/each}}
</script>

<script type="text/javascript">
	var stories = [
			{
				title : "밤하늘 이야기를 보다 [천체투영관]",
				desc : "영화를 보는 듯 커다란 돔 스크린이 있는 천체투영관의 상영관에 들어서면 눈 앞에 광활한 하늘이 펼쳐집니다. "
						+ "여러분은 모두 자신의 별자리를 알고 있죠? 그 이야기의 주인공인 별자리를 실제 밤하늘에서 눈으로 확인하기는 "
						+ "쉽지 않은데요. 오늘은 화면 속 밤하늘에서 궁금했던 "
						+ "별자리의 모습을 자세하게 볼 수 있답니다. "
						+ "불이 꺼지고 반짝이는 별과 다양한 하늘 이야기가 담긴 영상이 펼쳐지자 여기저기서 "
						+ "탄성이 터져나옵니다. 영상이 끝난 후 관람객들의 얼굴을 보니 공기 좋고 경치 좋은 곳에서 "
						+ "별이 가득한 밤하늘을 구경하고 온 듯 편안하고 즐거워 보이네요.",
				thumbnailUrl : "<c:url value="/resources/img/display/planetarium/planetarium/story15_01_01.jpg"/>",
				stories : [
						{
							storyLabel : "별자리의 모습",
							storyDesc : "",
							imgUrl : "<c:url value="/resources/img/display/planetarium/planetarium/story15_01_01.jpg"/>"
						},
						{
							storyLabel : "별자리의 모습",
							storyDesc : "",
							imgUrl : "<c:url value="/resources/img/display/planetarium/planetarium/story15_01_02.jpg"/>"
						},
						{
							storyLabel : "별자리의 모습",
							storyDesc : "영화를 보는 듯 커다란 돔 스크린이 있는 천체투영관의 상영관에 들어서면 눈 앞에 광활한 하늘이 펼쳐집니다. "
									+ "여러분은 모두 자신의 별자리를 알고 있죠? 그 이야기의 주인공인 별자리를 실제 밤하늘에서 눈으로 확인하기는 "
									+ "쉽지 않은데요. 오늘은 화면 속 밤하늘에서 궁금했던 "
									+ "별자리의 모습을 자세하게 볼 수 있답니다. "
									+ "불이 꺼지고 반짝이는 별과 다양한 하늘 이야기가 담긴 영상이 펼쳐지자 여기저기서 "
									+ "탄성이 터져나옵니다. 영상이 끝난 후 관람객들의 얼굴을 보니 공기 좋고 경치 좋은 곳에서 "
									+ "별이 가득한 밤하늘을 구경하고 온 듯 편안하고 즐거워 보이네요.",
							imgUrl : "<c:url value="/resources/img/display/planetarium/planetarium/story15_01_03.jpg"/>"
						},
						{
							storyLabel : "별들의 삷",
							storyDesc : "상영관을 나오면 여러 장의 별자리 사진이 전시되어 있는데요. "
									+ "형형색색의 아름다운 별자리 마다 붙여진 ‘심장이 뛰는 태양’, ‘숨은 아기별 찾기’, "
									+ "‘남반구 밤하늘의 크리스마스 리스’, ‘빛의 메아리’와 같은 재미있는 이름을 보면서 별자리를 관찰해보는 "
									+ "것도 색다른 재미가 있을 것입니다. 여러분도 별자리 사진을 보면서 여러분만의 제목을 붙여보세요.\n\n"
									+ "다음 전시관으로 이동하면서 여러분이 오늘 밤하늘에서 보고 싶은 별과 재미있게 봤던 별자리 이야기를 친구, \n"
									+ "가족들과 나눠보세요. 오랜 시간 후에도 떠 올릴 반짝반짝 빛나는 추억과 이야기가 훨씬 많아졌을 것입니다.",
							imgUrl : "<c:url value="/resources/img/display/planetarium/planetarium/story15_02.jpg"/>"
						}

				]
			},
			{
				title : "남녀노소 누구에게나 웃음을 선물하는 곳[천체관측소]",
				desc : "우주선을 타지 않고, 우주를 볼 수 있는 가장 빠른 방법은 망원경으로 우주의 모습을 관찰하는 것이겠죠! "
						+ "천체관측소에는 일반인이 관측 가능한 최대 구경의 반사망원경이 있습니다. 이 망원경은 사람의 눈보다 2만 "
						+ "배 밝게 볼 수 있어서 낮에도 금성을 볼 수 있다고 해요. 금성을 본 친구의 이야기를 들어볼까요?\n\n"
						+ "“책에서만 보던 금성을 이렇게 볼 수 있는 게 너무 신기해요!\n"
						+ "외계인도 볼 수 있는 망원경이 있다면 빨리 나왔으면 좋겠어요.\n"
						+ "친구들이라 꼭 다시 한번 과학관에 와서 외계인도 보고 인사를 하고 싶어요.”",
				thumbnailUrl : "<c:url value="/resources/img/display/planetarium/observation/story16_01_01.jpg"/>",
				stories : [
						{
							storyLabel : "천체관측소",
							storyDesc : "",
							imgUrl : "<c:url value="/resources/img/display/planetarium/observation/story16_01_01.jpg"/>"
						},
						{
							storyLabel : "천체관측소",
							storyDesc : "우주선을 타지 않고, 우주를 볼 수 있는 가장 빠른 방법은 망원경으로 우주의 모습을 관찰하는 것이겠죠! "
									+ "천체관측소에는 일반인이 관측 가능한 최대 구경의 반사망원경이 있습니다. 이 망원경은 사람의 눈보다 2만 "
									+ "배 밝게 볼 수 있어서 낮에도 금성을 볼 수 있다고 해요. 금성을 본 친구의 이야기를 들어볼까요?\n\n"
									+ "“책에서만 보던 금성을 이렇게 볼 수 있는 게 너무 신기해요!\n"
									+ "외계인도 볼 수 있는 망원경이 있다면 빨리 나왔으면 좋겠어요.\n"
									+ "친구들이라 꼭 다시 한번 과학관에 와서 외계인도 보고 인사를 하고 싶어요.”",
							imgUrl : "<c:url value="/resources/img/display/planetarium/observation/story16_01_02.jpg"/>"
						},
						{
							storyLabel : "천체관측소",
							storyDesc : "",
							imgUrl : "<c:url value="/resources/img/display/planetarium/observation/story16_02.jpg"/>"
						},
						{
							storyLabel : "천체관측소",
							storyDesc : "어른, 아이 할 것 없이 처음 보는 우주의 모습에 무척 들뜬 표정들 입니다. "
									+ "천체관측소에는 반사망원경 말고도 다양한 구경과 형식의 천체 망원경이 있어 행성과 위성, "
									+ "은하 등을 관찰할 수 있답니다. 우주를 만나는 가장 빠른 공간! 천체관측소를 절대 지나쳐서는 안되겠죠.\n"
									+ "계절별로 이 곳에서 관측할 수 있는 별들도 다르다고 하니까 미리 확인해보고, 꼭 보고 싶은 별이 보이는 날 방문해보는 것도 좋을 것 같네요!",
							imgUrl : "<c:url value="/resources/img/display/planetarium/observation/story16_03.jpg"/>"
						}]
			},
			{
				title : "외계인과의 만남, 우주탐사를 떠나다[스페이스 월드]",
				desc : "지금 제 눈 앞에 있는 스페이스 월드는 마치 우주 영화 속 주인공이 "
						+ "우주여행을 떠나기 전 훈련을 받는 공간 같은 외관을 자랑하고 있습니다.",
				thumbnailUrl : "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_01.jpg"/>",
				stories : [
						{
							storyLabel : "스페이스 월드",
							storyDesc : "지금 제 눈 앞에 있는 스페이스 월드는 마치 우주 영화 속 주인공이 "
									+ "우주여행을 떠나기 전 훈련을 받는 공간 같은 외관을 자랑하고 있습니다.",
							imgUrl : "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_01.jpg"/>"
						},
						{
							storyLabel : "스페이스 월드",
							storyDesc : "여러분도 스페이스 월드에 들어서기 전 우주여행을 준비하고 있는 우주 "
									+ "비행사가 된 기분으로 관람을 시작해보세요. 훨씬 많은 상상력들이 자극되어 여러분의 스페이스 월드 여행을 즐겁게 해줄 것입니다.",
							imgUrl : "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_02.jpg"/>"
						},
						{
							storyLabel : "우주로 보내는 메세지",
							storyDesc : "",
							imgUrl : "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_03_01.jpg"/>"
						},
						{
							storyLabel : "우주로 보내는 메세지",
							storyDesc : "입구에는 우주와 첫 인사를 하는 공간이 마련되어 있습니다. "
									+ "150억 년전의 과거로 시공간 우주 여행을 떠날 수 있는 전시물을 체험하고 나면, "
									+ "우주로 메시지를 보낼 수 있는 전시물이 있습니다. 40여 년 전 우주로 보낸 아레시보 메시지처럼, "
									+ "전파를 통해 직접 메시지를 보낼 수 있는 전시물이죠. 여러분도 우주 어딘가에서 살고 있는 외계인들에게 반가움의 마음을 전해보세요.",
							imgUrl : "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_03_02.jpg"/>"
						},
						{
							storyLabel : "우주여행 시작",
							storyDesc : "이제 본격적인 우주 여행을 시작해보겠습니다. 여러분은 외계인의 존재를 믿고 있나요? "
									+ "저는 광활한 우주 어딘가에 우리와 같이 공동체를 이루고 살아가는 생명들의 행성이 분명이 "
									+ "있다고 믿고 있습니다. 만약 믿지 않는 친구들이라도 이 곳에서만큼은 열린 마음으로 어딘가에 "
									+ "존재하는 외계인들을 만나러 간다고 생각해보세요. 그럼 출발해볼까요?\n\n"
									+ "이 곳에서는 지구와 같이 생명이 사는 엘피스 행성 속 생명의 나무를 살리는 협동 미션을 "
									+ "수행하게 됩니다. 함께 온 친구들과 신나는 체험을 통해 우주 여행을 떠나게 되는 것이죠.",
							imgUrl : "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_04.jpg"/>"
						},
						{
							storyLabel : "갤럭시 스테이션",
							storyDesc : "",
							imgUrl : "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_05_01.jpg"/>"
						},
						{
							storyLabel : "갤럭시 스테이션",
							storyDesc : "제가 지금 도착한 곳은 갤럭시 스테이션입니다. 우주대원들이 외계 생명체에게 보내고 "
									+ "받은 전파 신호를 검토하고, 외계 생명체의 존재를 확인하게 되는 체험을 할 수 있는 곳이죠. "
									+ "우주를 연상하게 하는 거대한 공간에 600인치 초대형 스크린으로 보는 미디어 쇼는 "
									+ "정말 한 편의 우주쇼를 보는 듯 환상적입니다. 정말 우주가 우리를 환영해주는 파티를 "
									+ "열어주는 것 같네요.",
							imgUrl : "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_05_02.jpg"/>"
						},
						{
							storyLabel : "스타쉽 전시관",
							storyDesc : "",
							imgUrl : "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_06_01.jpg"/>"
						},
						{
							storyLabel : "스타쉽 전시관",
							storyDesc : "이제, 생명의 나무를 살리기 위한 본격적인 협동 미션을 수행하게 됩니다. 스타쉽 전시관 "
									+ "안으로 들어가면 모션 라이더와 3D 영상으로 광속 여행과 초신성 폭발, 유사 지구인 엘피스 "
									+ "행성의 모습을 체험할 수 있습니다. 여러분이 상상 속에 그려보았던 우주와 비슷한 모습인가요? "
									+ "실제로 우주 탐사를 하는 듯 짜릿하고 긴장감 넘치는 체험이 가득한 곳입니다.",
							imgUrl : "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_06_02.jpg"/>"
						},
						{
							storyLabel : "우주공작실",
							storyDesc : "",
							imgUrl : "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_07_01.jpg"/>"
						},
						{
							storyLabel : "우주공작실",
							storyDesc : "모든 우주 탐사가 끝나면 우주공작실과 우주 자료실에서 여러분이 가졌던 궁금증과 호기심, "
									+ "창작에 대한 꿈을 펼쳐보세요. 그리고 앞으로 여러분이 실제로 떠나게 될 멀지 않은 미래의 "
									+ "우주의 모습을 마음 속에 그려보세요.\n\n"
									+ "우주는 이미 여러분을 기다리고 있습니다. 여러분이 우주에 더 많은 관심을 갖고, 질문을 "
									+ "던진다면 무궁무진한 재미있는 이야기들이 여러분에게 다가올 것입니다. 국립과천과학관의 "
									+ "천문관 전시들이 여러분에게 우주로 가는 문을 열어주었으면 좋겠습니다.",
							imgUrl : "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_07_02.jpg"/>"
						}]
			}];

	var storySource = $("#story-template").html();
	var storyTemplate = Handlebars.compile(storySource);
	var storyHtml = storyTemplate({
		stories : stories
	});
	$("#stories").html(storyHtml);
</script>

<c:url var="shareUrl" value="/display/planetarium/planetarium" />
<c:import url="/WEB-INF/views/display/displayDetailModal.jsp">
	<c:param name="shareUrl" value="${shareUrl }" />
	<c:param name="exhibitionName" value="천체투영관" />
</c:import>

<form name="frmIndex">
	<input type="hidden" name="ACADEMY_CD" value="ACD007" />
	<input type="hidden" name="COURSE_CD" value="" />
	<input type="hidden" name="SEMESTER_CD" value="" />
	<input type="hidden" name="TYPE" value="" />
	<input type="hidden" name="FLAG" value="" />
	<input type="hidden" name="CLASS_CD" value="CL7002" />
</form>

<script type="text/javascript">
	$(document).ready(function() {
		// 전시관 지도 flash outdoor로 전환.
		//setDefaultSVGMap('#basic-map-spy',3);
		// 지도 flash 해당 전시관 표시
		//$("g[id='XMLID_110_']").attr('class', 'active');

		// 이용안내 tab click event
		changeGuideTab();
		// 이용안내 첫 tab click
		$("a[data-name='program']").click();

	});

	function changeGuideTab() {
		$('.display-guide-wrapper .sub-nav .item').on('click', function() {

			var id = $(this).data('name');
			$('.display-guide-wrapper .sub-nav .item').removeClass('active');
			$(this).addClass('active');
			$('.display-guide-wrapper .guide-item').removeClass('active');
			$('#' + id + '.guide-item').addClass('active');
			event.preventDefault();
		});
	}
	
	function ChangeClass(Cd){
		var frm = document.frmIndex;
		frm.CLASS_CD.value = Cd;
		frm.action = '<c:url value="/schedules"/>?${_csrf.parameterName}=${_csrf.token}';
		frm.method = 'post';
		frm.submit();
	}
</script>


