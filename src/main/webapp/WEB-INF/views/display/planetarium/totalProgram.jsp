<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-blue">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>
	<div id="support-body" class="sub-body">
		<div class="narrow-sub-top education">
			<div class="sub-banner-tab-wrapper sub-banner-tab-blue">
				<div class="container">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<ul class="nav nav-tabs">
								<li class="col-xs-12 col-sm-6 col-md-3">
									<a href="<c:url value="/display/planetarium"/>" class="item">
										<i class="fa fa-star fa-lg" aria-hidden="true"></i> 천체투영관
									</a>
								</li>
		        				<li class="col-xs-12 col-sm-6 col-md-3">
		        					<a href="<c:url value="/display/planetarium/observation"/>" class="item">
										<i class="fa fa-star fa-lg" aria-hidden="true"></i> 천체관측소
									</a>
		       					</li>
		       					<li class="col-xs-12 col-sm-6 col-md-3">
		       						<a href="<c:url value="/display/planetarium/spaceWorld"/>" class="item">
										<i class="fa fa-star fa-lg" aria-hidden="true"></i> 스페이스월드
									</a>
		       					</li>
		       					<li class="col-xs-12 col-sm-6 col-md-3">
		       						<a href="http://cyber.scientorium.go.kr/museum_2015/vrLoad.jsp?pavilion=roadview&xml_name=otr_008.xml&angle=180" class="item">
				                        <i class="fa fa-desktop fa-lg" aria-hidden="true"></i> 사이버 전시관
				                    </a>  
		       					</li>
							</ul>   
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="program-guide-spy">
			<div class="container">
				<div class="row">
					<div class="col-xs-5 col-md-3">
						<h3 class="page-title blue">
							<div class="top-line"></div>
							프로그램 구성
						</h3>
					</div>
				</div>
				<div class="row description">
					<div class="col-xs-12 col-sm-6 col-md-4">
						<div class="display-card program">
							<div class="card-image">
								<img src="<c:url value="/resources/img/display/experience/planetariumProgram/program_01.png" />" class="img-responsive"/>
 							</div>
							<div class="card-content">
								<div class="display-card-title">
									천문우주아카데미
								</div>
								<div class="display-card-content">
									<div>
										참가대상의 특성에 맞는 전문&middot;특성화된 맞춤형 천문우주캠프프로그램
									</div>
									<div>
										우주상상탐험대(초등) / 천문우주캠프(중&middot;고등) / Astronomy Concert(성인)
									</div>
								</div>
								<div class="footer blue">
									<i class="fa fa-phone" aria-hidden="true"></i> 문의 02-3677-1393
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-4">
						<div class="display-card program">
							<div class="card-image">
								<img src="<c:url value="/resources/img/display/experience/planetariumProgram/program_02.png" />" class="img-responsive"/>
 							</div>
							<div class="card-content">
								<div class="display-card-title">
									청소년 서포터즈 'Astro Youth'
								</div>
								<div class="display-card-content">
									<div>
										자기주도형 자원봉사활동의 일환으로 천문우주과학에 관심이 있는
										청소년들이 진행하는 눈높이 천문우주 전시해설 프로그램
									</div>
									<div>
										모집: 3월&middot;10월 2회 모집 / 홈페이지공지
									</div>
								</div>
								<div class="footer blue">
									<i class="fa fa-phone" aria-hidden="true"></i> 문의 02-3677-1393
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-4">
						<div class="display-card program">
							<div class="card-image">
								<img src="<c:url value="/resources/img/display/experience/planetariumProgram/program_03.png" />" class="img-responsive"/>
 							</div>
							<div class="card-content">
								<div class="display-card-title">
									과학토크콘서트
								</div>
								<div class="display-card-content">
									<div>
										최신 과학 이슈를 천체투영관 영상을 배경으로 작가, 영화감독, 예술가 등
										다양한 분야의 패널들과 과학자가 대화하는 형식으로 진행되는
										과학관의 대표적 과학문화 프로그램
									</div>
									<div>
										운영 : 5~12월 중 5회
									</div>
									<div>
										예약 : 온라인 사전예약
									</div>
									<div>
										요금 : 5000원/1인
									</div>
								</div>
								<div class="footer blue">
									<i class="fa fa-phone" aria-hidden="true"></i> 문의 02-3677-1561
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-4">
						<div class="display-card program">
							<div class="card-image">
								<img src="<c:url value="/resources/img/display/experience/planetariumProgram/program_04.png" />" class="img-responsive"/>
 							</div>
							<div class="card-content">
								<div class="display-card-title">
									천문학 특강 및 대중강연
								</div>
								<div class="display-card-content">
									<div>
										국내 최고의 천문학자들이 연구중인 주제를 쉽고도 깊이 있게
										해설하는 특강과 대중의 관심이 모이는 천문우주 관련 이슈를 전문가의 정확한
										정보와 해설을 통해 만나볼 수 있는 강연 프로그램
									</div>
									<div>
										운영 : 월 1~2회
									</div>
									<div>
										예약 : 온라인 사전예약
									</div>
									<div>
										요금 : 2000원/1인
									</div>
								</div>
								<div class="footer blue">
									<i class="material-icons">&#xE0CD;</i>문의 02-3677-1561
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-4">
						<div class="display-card program">
							<div class="card-image">
								<img src="<c:url value="/resources/img/display/experience/planetariumProgram/program_05.png" />" class="img-responsive"/>
 							</div>
							<div class="card-content">
								<div class="display-card-title">
									토요관측회
								</div>
								<div class="display-card-content">
									<div>
										생활 속 천문학 소재를 이야기 방식으로 배워 보는 스토리 나잇,
										밤하늘을 1m 망원경과 중소형 망원경을 통해 관측해 보는 별 엿보기와
										가상별자리 여행으로 구성된 누구나 참여가능한 공개 관측행사
									</div>
									<div>
										운영 : 월1~2회 운영
									</div>
									<div>
										예약 : 온라인 사전예약(300명)
									</div>
									<div>
										요금 : 1000원/1인
									</div>
								</div>
								<div class="footer blue">
									<i class="material-icons">&#xE0CD;</i>문의 02-3677-1395
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-4">
						<div class="display-card program">
							<div class="card-image">
								<img src="<c:url value="/resources/img/display/experience/planetariumProgram/program_06.png" />" class="img-responsive"/>
 							</div>
							<div class="card-content">
								<div class="display-card-title">
									특별 공개관측회
								</div>
								<div class="display-card-content">
									<div>
										일식, 월식, 유성우, 혜성출연 등 특이 처눈현상 발생 시 밤하늘에 펼쳐지는
										신비한 현상을 이해하고 이를 직접관측 해보는 개방형 관측행사
									</div>
									<div>
										운영 : 특이현상 발생 시 운영
									</div>
									<div>
										예약 : 온라인 사전예약 또는 현장 접수(누구나)
									</div>
									<div>
										요금 : 무료
									</div>
								</div>
								<div class="footer blue">
									 <i class="fa fa-phone" aria-hidden="true"></i> 문의 02-3677-1395
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>