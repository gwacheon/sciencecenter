<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-teal">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
		<%-- <nav id="scrollspy-nav" class="navbar-default navbar-static has-scroll">
			<div class="container">
				<ul class="nav navbar-nav">
					<c:import url="/WEB-INF/views/display/navbar.jsp"/>
					<li>
						<a href="#basic-info-spy" class="item">
							기본 안내 
						</a>
					</li>
					<li>
						<a href="#basic-map-spy" class="item">
							전시 구성
						</a>
					</li>
					<li>
						<a href="#major-exhibitions-spy" class="item">
							주요전시물
						</a>
					</li>
					<li>
						<a href="#story-spy" class="item">
							스토리 여행기
						</a>
					</li>	
					<li>
						<a href="#basic-guide-spy" class="item">
							이용안내
						</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<div class="bread-crumbs">
							전시관 / 스페이스월드
						</div>
					</li>
				</ul>					
			</div>
		</nav> --%>
	</div>
	<div id="display-body" class="sub-body">
		<div class="narrow-sub-top display">
			<div class="sub-banner-tab-wrapper sub-banner-tab-teal">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
							<ul class="nav nav-tabs">
								<li class="col-xs-12">
									<a href="javascript:void(0)" onclick="ChangeClass('CL7002')" class="item">
				                        <i class="fa fa-calendar-check-o fa-lg" aria-hidden="true"></i> 스페이스월드 예약하기
				                    </a>
								</li>
							</ul>   
						</div>
					</div>
				</div>
			</div>			
		</div>
		<div class="container">
			<div id="basic-map-spy" class="section scrollspy">
				<div class="row">
					<div class="col-xs-4 col-md-2">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							전시 구성
						</h3>
					</div>
				</div>
				<div class="row description">
				   	<div class="col-sm-12 col-md-4 col-lg-4">
				   		<ul class="circle-list teal">
				   			<li>유의사항
					   			<ul>
					   				<li>
					   					대상 : 6세 이하 유아(2011~2016년생)는 체험프로그램을 이용하실 수 없습니다.
					   				</li>
					   			</ul>
				   			</li>
				   		</ul>
					    <p>
							<b>「스페이스 월드(Space World)」</b>는 과학적 사실을 기반으로 구성된 스토리텔링형 극장이다. 
							입체영상물과 각종 효과 및 디지털 정보기술을 응용한 인터렉티브 환경을 통해 외계 생명체와 우주탐사에 대한 
							호기심을 키워나갈 수 있는 우주과학 체험공간이다.  
						</p>
					    <div>
							<span class="green-circle-bg">F1</span> 갤럭시 스테이션 (Galaxy Station) 
						</div>
						<div>
							<span class="green-circle-bg">B1</span> 갤럭시 아카데미 (Galaxy Academy)
						</div>
						
					</div>
					<div class="col-sm-12 col-md-8 col-lg-8 text-right">
						<!-- select dropdown : 다른관으로 이동 -->
						<c:import url="/WEB-INF/views/display/displayHallSelect.jsp"/>
						<div>
							<img alt="스페이스월드" src="<c:url value='/resources/img/display/map/09.png'/>"
								class="img-responsive margin-top20">
						</div>
					</div>
				</div>
			</div>
			
			<div id="major-exhibitions-spy" class="section scrollspy">
				<div class="row">
					<div class="col-xs-4 col-md-2">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							주요전시물
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="main-exhibition-wrapper">
							<div id="main-exhibitions">
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div id="story-spy" class="story-telling-wrapper">
				<div class="header">
					<div class="row">
						<div class="col-md-6">
							<h3 class="page-title teal">
								<div class="top-line"></div>
								스토리로 읽어 보는 천문관 여행기 
							</h3>
						</div>
					</div>
				</div>
				<div class="description">
					우리는 언제쯤 방학에 우주로 여행을 떠날 수 있을까요? 
					외계인과 친구처럼 메신저를 주고 받을 수 있는 날은 언제가 될까요? 
					잘 알 수 없기에 더 궁금하고 신비로운 우주! 국립과천과학관에 오면 
					다양한 모습의 우주를 만날 수 있습니다.
				</div>
				
				<div id="stories" class="row">
				</div>
			</div>
			
			<div class="related-photos">
				<div class="title">
					<i class="color-darkgreen material-icons">&#xE413;</i>  관련사진
				</div>
				<div id="image-slider">
					<div id="image-slider-window">
						<img src="<c:url value="/resources/img/display/planetarium/spaceWorld/related_photo_01.png"/>" 
							data-url="/resources/img/display/planetarium/spaceWorld/related_photo_01.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/planetarium/spaceWorld/related_photo_02.png"/>" 
							data-url="/resources/img/display/planetarium/spaceWorld/related_photo_02.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/planetarium/spaceWorld/related_photo_03.png"/>" 
							data-url="/resources/img/display/planetarium/spaceWorld/related_photo_03.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/planetarium/spaceWorld/related_photo_04.png"/>" 
							data-url="/resources/img/display/planetarium/spaceWorld/related_photo_04.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/planetarium/spaceWorld/related_photo_05.png"/>" 
							data-url="/resources/img/display/planetarium/spaceWorld/related_photo_05.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/planetarium/spaceWorld/related_photo_06.png"/>" 
							data-url="/resources/img/display/planetarium/spaceWorld/related_photo_06.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/planetarium/spaceWorld/related_photo_07.png"/>" 
							data-url="/resources/img/display/planetarium/spaceWorld/related_photo_07.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/planetarium/spaceWorld/related_photo_08.png"/>" 
							data-url="/resources/img/display/planetarium/spaceWorld/related_photo_08.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/planetarium/spaceWorld/related_photo_09.png"/>" 
							data-url="/resources/img/display/planetarium/spaceWorld/related_photo_09.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/planetarium/spaceWorld/related_photo_10.png"/>" 
							data-url="/resources/img/display/planetarium/spaceWorld/related_photo_10.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/planetarium/spaceWorld/related_photo_11.png"/>" 
							data-url="/resources/img/display/planetarium/spaceWorld/related_photo_11.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/planetarium/spaceWorld/related_photo_12.png"/>" 
							data-url="/resources/img/display/planetarium/spaceWorld/related_photo_12.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/planetarium/spaceWorld/related_photo_13.png"/>" 
							data-url="/resources/img/display/planetarium/spaceWorld/related_photo_13.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/planetarium/spaceWorld/related_photo_14.png"/>" 
							data-url="/resources/img/display/planetarium/spaceWorld/related_photo_14.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/planetarium/spaceWorld/related_photo_15.png"/>" 
							data-url="/resources/img/display/planetarium/spaceWorld/related_photo_15.png" class="slide"/>
						<img src="<c:url value="/resources/img/display/planetarium/spaceWorld/related_photo_16.png"/>" 
							data-url="/resources/img/display/planetarium/spaceWorld/related_photo_16.png" class="slide"/>
					</div>
				</div><!--  image-slider END ... -->
			</div>
			<div id="basic-guide-spy" class="section scrollspy">
				<div class="row">
					<div class="col-xs-4 col-md-2">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							이용안내
						</h3>
					</div>
				</div>
				
				<div class="display-guide-wrapper">
					<div class="sub-nav">
						<div class="row">
							<div class="col-md-6">
								<a href="#"  class="item" data-name="program">스페이스월드 이용안내</a> 
							</div>				
							<div class="col-md-6">
						      	<a href="#" class="item" data-name="group-reservation">단체예약 안내</a>
							</div>
						</div>						
					</div>
					  
					<div id="program" class="guide-item">
						<div class="sub-section">
							<div class="row margin-bottom20">
                            	<div class="col-md-12 text-right">
                            		<a href="javascript:void(0)" onclick="ChangeClass('CL7002')" class="reservation-btn">
                            			예약 및 확인(취소) 바로가기
                            		</a>
                            	</div>                            	
                            	
                            </div>
							<div class="row">
								<div class="col-md-12">
									<ul class="circle-list teal">
										<li>
											<span class="sub-section-title">
												스페이스월드
											</span>
											<ul class="arrow-list teal">
												<li>
													체험코스
													<ul>
														<li>
															과학과 상상이 결합된 국내 최초 스토리텔링 형식의 감성형 체험교육! 무한한 상상력으로 우주여행을 떠나보세요.
														</li>
													</ul>
												</li>
												<li>
													상설 전시
													<ul>
														<li>
															외계행성을 찾기위한 KMTnet망원경, 시공간 우주여행등 첨단 우주 과학 전시물들을 보고 창의적인 미래를 그려보세요.
														</li>
													</ul>
												</li>
												<li>
													우주 융합 교실
													<ul>
														<li>
															한걸음 더 다가가는 스페이스월드 만의 특별한 시간, 매주 주말에 만나는 천문 융합 프로그램을 체험해보세요.
														</li>
													</ul>
												</li>
											</ul>
										</li>
									</ul>
								</div>  
							</div>
						</div>
						<div class="sub-section">
							<div class="row">
								<div class="col-md-12">
									<ul class="circle-list teal">
										<li>
											<span class="sub-section-title">
												운영시간 안내	
											</span>
										</li>
									</ul>
								</div>
							</div>
							<div class="table-responsive">
								<table class="table table-teal centered-table margin-bottom20">
									<thead>
										<tr>
											<th>코스명</th>
											<th>운영시간</th>
											<th>참가비</th>
											<th>소요시간</th>
											<th>인원수</th>
											<th>대상</th>
											<th>비고</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>
												체험코스
											</td>
											<td>
												<div>
													1회(10:00)
												</div>
												<div>
													2회(11:00)
												</div>
												<div>
													3회(13:00)
												</div>
												<div>
													4회(14:00)
												</div>
												<div>
													5회(15:00)
												</div>
												<div>
													6회(16:00)
												</div>
												
											</td>
											<td>
												2,000원 
											</td>
											<td>
												50분 
											</td>
											<td>
												40 
											</td>
											<td>
												7세이상<br>
												(7세, 보호자 동반) 
											</td>
											<td>
												 
											</td>
										</tr>
										<tr>
											<td>
												교육코스
											</td>
											<td>
												<div>
													1회(10:00)
												</div>
												<div>
													2회(14:00)
												</div>
												<div>
													3회(15:00)
												</div>
												
											</td>
											<td>
												5,000원 
											</td>
											<td>
												50분 
											</td>
											<td>
												40 
											</td>
											<td>
												초1~고3 
											</td>
											<td>
												<span class="color-red">(평일 운영)단체만 가능</span><br>
												<span class="color-red">사전 전화예약</span><br>
												02)3677-1402
												
											</td>
										</tr>
										<tr>
											<td>
												오픈해설
											</td>
											<td>
												<div>1회(11:00)</div>
												<div>2회(14:00)</div> 
											</td>
											<td>
												
												무료
											</td>
											<td>
												10~30분 
											</td>
											<td>
												30 
											</td>
											<td>
												누구나
											</td>
											<td>
												
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div><!-- sub-section END ... -->
						<div class="sub-section">
							<div class="row">
								<div class="col-md-12">
									<ul class="circle-list teal">
										<li>
											<span class="sub-section-title">
												요금 안내
											</span>
										</li>
									</ul>  
								</div>
							</div>
							<div class="table-responsive">
								<table class="table table-bordered centered-table margin-bottom20">
									<thead>
										<tr>
											<th rowspan="2">구분</th>
											<th rowspan="2">개인</th>
											<th colspan="3">할인</th>
											<th rowspan="2">비고</th>
										</tr>
										<tr>
											<th>단체</th>
											<th>연간회원</th>
											<th>우대고객</th>
											
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>
												체험코스
											</td>
											<td>
												2,000원
											</td>
											<td>
												1,500원 
											</td>
											<td>
												1,500원 
											</td>
											<td>
												1,000원 
											</td>
											<td>
												<div class="color-red">
													7세이상
												</div>
												<div class="color-red">
													(7세, 보호자 동반)
												</div>
											</td>
										</tr>
										<tr>
											<td>
												교육코스
											</td>
											<td>
												
											</td>
											<td>
												5,000원 
											</td>
											<td>
												 
											</td>
											<td>
												 
											</td>
											<td>
												단체(평일운영)<br> 
												전화문의<br>
												02)3677-1402
											</td>
										</tr>
										<tr>
											<td>
												전시해설
											</td>
											<td>
												무료
											</td>
											<td>
												 
											</td>
											<td>
												 
											</td>
											<td>
												 
											</td>
											<td>
											</td>
										</tr>
										
									</tbody>
								</table>
							</div>  
							<div class="color-red notice">
								※6세 이하 유아는 체험코스 입장이 절대 불가합니다.
							</div>
							<div class="notice">
								※우대고객: 경로우대자, 장애인, 국가유공자 및 기초생활수급자, 중증장애인(1~3급) 동반 1인은 장애인 요금 적용
							</div>
						</div><!-- sub-section END ... -->
						<div class="sub-section">
							<div class="row">
								<div class="col-md-12">
									<ul class="circle-list teal">
										<li>
											<span class="sub-section-title">
												인터넷 예약 방법 및 결제 안내
											</span>
											<ul class="arrow-list teal">
												<li>
													인터넷 예약은 국립과천과학관 홈페이지에서만 가능합니다.
												</li>
												<li>
													관람을 희망하는 날짜와 시간을 선택 후, 관람료를 결제 하시면 예약 완료됩니다.
													<div>
														(단, 결제는 신용카드만 가능 / 당일 현장 잔여석 발생 시 카드, 현금 결제 가능)
													</div>
												</li>
												<li>
													관람일 기준으로 1인당 19장까지 구매 가능합니다.
												</li>
												<li>
													인터넷 예약은 관람일 기준으로 2주전(14일) 오전 09시부터 하루 전(24:00)까지 가능합니다.
												</li>
												<li>
													예약 후 관람 일에 과학관 매표소를 방문하셔서 예약을 확인하신 후 관람권을 수령하시고 입장하시면 됩니다.
												</li>
												<li>
													예약 환인 시에는 예약자 성명, 예약번호, 회원가입 시 입력하신 휴대전화번호 확인이 필요하오니 예약증을 출력해 오시면 빠르게 티켓으로 교환하실 수 있습니다.
												</li>
												<li>
													예약 결제 후 부분취소는 불가능합니다.
												</li>
												<li class="color-red">
													인터넷 결제 취소 및 환불은 관람 전일 24시까지 가능하며, 현장 또는 전화상으로는 체험 시작 30분 전까지 취소 또는 교환할 수 있습니다.
												</li>
												<li class="color-red">
													체험 시작 5분 이후에는 안전상의 이유로 절대 입장하실 수 없습니다.
												</li>
											</ul>
										</li>
									</ul>  
								</div>  
							</div>
						</div><!-- sub-section END ... -->

					</div><!--  id="program" END ... -->
					
					<div id="group-reservation" class="guide-item">
						<div class="sub-section">
							 <div class="row margin-bottom20">
                            	<div class="col-md-12 text-right">
                            		<a href="javascript:void(0)" onclick="ChangeClass('CL7002')" class="reservation-btn">
                            			예약 및 확인(취소) 바로가기
                            		</a>
                            	</div>                            	                            	
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                	<ul class="circle-list teal">
										<li>
											<span class="sub-section-title">
												단체 예약 안내
											</span>
											<ul class="arrow-list teal">
				                                <li>
				                                    해당 개월 2개월 전 1일 부터 유선상 단체 예약가능합니다.<br>
				                                    (ex, 11월 10일 -> 9월 1일 부터 예약 가능)   
				                                </li>
				                                <li>
				                                    6세 이하 유아는 단체일지라도 체험이 불가합니다.
				                                </li>
												
											</ul>
										</li>
									</ul>  
                                </div>  
                            </div>
                        </div><!--  sub-section END ... -->
                        <div class="sub-section">
                            <div class="row">
                                <div class="col-md-12">
                                	<ul class="circle-list teal">
										<li>
											<span class="sub-section-title">
												단체예약 가능 범위 및 대상
											</span>
											<ul class="arrow-list teal">
				                                <li>
				                                    초, 중, 고등학교 20인 이상 단체
				                                    <div class="color-red">
				                                    	 (20인 미만일 경우 단체적용 및 종합 프로그램 이용이 불가합니다)
				                                    </div> 
				                                </li>
				                                <li>
				                                    주말,공휴일을 제외한 화~금요일에 한함
				                                </li>
											</ul>
										</li>
									</ul>  
                                </div>  
                            </div>
                        </div><!--  sub-section END ... -->
                        <div class="sub-section">
                            <div class="row">
                                <div class="col-md-12">
                                	<ul class="circle-list teal">
										<li>
											<span class="sub-section-title">
												 단체예약 방법
											</span>
											<ul class="arrow-list teal">
				                                <li>
				                                	스페이스월드 안내데스크(02-3677-1402)로 사전 전화 예약 후 당일 현장 결제<br>
				                                	※ 스페이스월드 안내데스크에서 카드, 현금 결제
				                                </li>
											</ul>
										</li>
									</ul>  
                                </div>  
                            </div>
                        </div><!--  sub-section END ... -->
                        <div class="sub-section">
                            <div class="row">
                                <div class="col-md-12">
                                	<ul class="circle-list teal">
										<li>
											<span class="sub-section-title">
												 프로그램 안내
											</span>
											<ul class="arrow-list teal">
				                                <li>
				                                	코스1 : 체험프로그램
				                                	<ul>
				                                		<li>
				                                			감성형 우주체험으로 미디어쇼,4D 영상등을 이용한 새로운 형식의 가상우주여행 (소요시간 약 50분)
				                                		</li>
				                                	</ul>
				                                </li>
				                                <li>
				                                	코스2 : 교육 프로그램
				                                	<ul>
				                                		<li>
						                                     천문우주 관련 교육 (소요시간 약 50분)
						                                </li>
						                                <li>
						                                     교육 프로그램 교육 주제
						                                </li>
				                                	</ul>
				                                </li>
											</ul>
										</li>
									</ul>  
                                </div>  
                            </div>
                           	<div class="table-responsive">
                            	<table class="table table-bordered centered-table margin-bottom20">
                                	<thead>
                                    	<tr>
                                        	<th>구분</th>
	                                        <th>프로그램명</th>
	                                        <th>프로그램 내용</th>
	                                        <th>비고</th>
                                    	</tr>
                                	</thead>
                               		<tbody>
	                                    <tr>
	                                        <td>
	                                            초등 저(1~3학년)
	                                        </td>
	                                        <td>
	                                            달의 비밀
	                                        </td>
	                                        <td>
	                                            달의 특징과 위상 변화에 대해 알아보기
	                                        </td>
	                                        <td>
	                                            
	                                            달팽이,<br/>
	                                            달지도
	                                             
	                                        </td>
	                                    </tr>
	                                    <tr>
	                                        <td>
	                                            초등 고(4~6학년) 
	                                        </td>
	                                        <td>
	                                            이해가 가요
	                                        </td>
	                                        <td>
	                                            해시계의 작동 원리를 이해하고 만들어 보는 체험 
	                                        </td>
	                                        <td>
                                       			해시계
	                                        </td>
	                                    </tr>
	                                    <tr>
	                                        <td>
	                                            중, 고등 
	                                        </td>
	                                        <td>
	                                            별에서 온 그대
	                                        </td>
	                                        <td>
	                                            분광기를 사용하여 빛이 나뉘는 원리를 탐구 
	                                        </td>
	                                        <td>
                                       			분광기
	                                        </td>
	                                    </tr>
                                	</tbody>
	                        	</table>
                           	</div>
                        </div><!--  sub-section END ... -->
                        <div class="sub-section">
                            <div class="row">
                                <div class="col-md-12">
                                	<ul class="circle-list teal">
										<li>
											<span class="sub-section-title">
		                                        단체예약 및 기타문의
		                                    </span>
		                                    <ul class="arrow-list teal">
				                                <li>
				                                	02-3677-1402(문의시간 : 09:30~17:30, 월요일 휴관)
				                                </li>		                                    
		                                    </ul>
		                                </li>
									</ul>  
                                </div>  
                            </div>
                        </div><!--  sub-section END ... -->
					</div><!--  id="group-reservation" END ... -->
				</div>
			</div>
		</div>
	</div>
</div>


<script id="story-template" type="text/x-handlebars-template">
{{#each stories}}
<div class="col-xs-12 col-sm-4 col-md-4">
	<div class="display-card story-telling story-more-btn pointer" data-idx="{{@index}}" onclick="">
		<div class="card-image">
			<img src="{{thumbnailUrl}}" class="img-responsive"/>
		</div>
		<div class="card-content">
			<div class="display-card-title">
				{{title}}
			</div>
			<div class="display-card-content">
				{{{desc}}}
			</div>
			
		</div>
	</div>							
</div>
{{/each}}
</script>

<script type="text/javascript">
	var stories = [
		{
			title: "밤하늘 이야기를 보다 [천체투영관]",
			desc: "영화를 보는 듯 커다란 돔 스크린이 있는 천체투영관의 상영관에 들어서면 눈 앞에 광활한 하늘이 펼쳐집니다. "
				+ "여러분은 모두 자신의 별자리를 알고 있죠? 그 이야기의 주인공인 별자리를 실제 밤하늘에서 눈으로 확인하기는 "
				+ "쉽지 않은데요. 오늘은 화면 속 밤하늘에서 궁금했던 " 
				+ "별자리의 모습을 자세하게 볼 수 있답니다. " 
				+ "불이 꺼지고 반짝이는 별과 다양한 하늘 이야기가 담긴 영상이 펼쳐지자 여기저기서 "
				+ "탄성이 터져나옵니다. 영상이 끝난 후 관람객들의 얼굴을 보니 공기 좋고 경치 좋은 곳에서 "
				+ "별이 가득한 밤하늘을 구경하고 온 듯 편안하고 즐거워 보이네요.",
			thumbnailUrl: "<c:url value="/resources/img/display/planetarium/planetarium/story15_01_01.jpg"/>",
			stories: [
				{
					storyLabel: "별자리의 모습",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/planetarium/planetarium/story15_01_01.jpg"/>"
				},
				{
					storyLabel: "별자리의 모습",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/planetarium/planetarium/story15_01_02.jpg"/>"
				},
				{
					storyLabel: "별자리의 모습",
					storyDesc: "영화를 보는 듯 커다란 돔 스크린이 있는 천체투영관의 상영관에 들어서면 눈 앞에 광활한 하늘이 펼쳐집니다. "
						+ "여러분은 모두 자신의 별자리를 알고 있죠? 그 이야기의 주인공인 별자리를 실제 밤하늘에서 눈으로 확인하기는 "
						+ "쉽지 않은데요. 오늘은 화면 속 밤하늘에서 궁금했던 " 
						+ "별자리의 모습을 자세하게 볼 수 있답니다. " 
						+ "불이 꺼지고 반짝이는 별과 다양한 하늘 이야기가 담긴 영상이 펼쳐지자 여기저기서 "
						+ "탄성이 터져나옵니다. 영상이 끝난 후 관람객들의 얼굴을 보니 공기 좋고 경치 좋은 곳에서 "
						+ "별이 가득한 밤하늘을 구경하고 온 듯 편안하고 즐거워 보이네요.",
					imgUrl: "<c:url value="/resources/img/display/planetarium/planetarium/story15_01_03.jpg"/>"
				},
				{
					storyLabel: "별들의 삷",
					storyDesc: "상영관을 나오면 여러 장의 별자리 사진이 전시되어 있는데요. "
						+ "형형색색의 아름다운 별자리 마다 붙여진 ‘심장이 뛰는 태양’, ‘숨은 아기별 찾기’, "
						+ "‘남반구 밤하늘의 크리스마스 리스’, ‘빛의 메아리’와 같은 재미있는 이름을 보면서 별자리를 관찰해보는 "
						+ "것도 색다른 재미가 있을 것입니다. 여러분도 별자리 사진을 보면서 여러분만의 제목을 붙여보세요.\n\n"
						+ "다음 전시관으로 이동하면서 여러분이 오늘 밤하늘에서 보고 싶은 별과 재미있게 봤던 별자리 이야기를 친구, \n"
						+ "가족들과 나눠보세요. 오랜 시간 후에도 떠 올릴 반짝반짝 빛나는 추억과 이야기가 훨씬 많아졌을 것입니다.",
					imgUrl: "<c:url value="/resources/img/display/planetarium/planetarium/story15_02.jpg"/>"
				}
				
			]
		},
		{
			title: "남녀노소 누구에게나 웃음을 선물하는 곳[천체관측소]",
			desc: "우주선을 타지 않고, 우주를 볼 수 있는 가장 빠른 방법은 망원경으로 우주의 모습을 관찰하는 것이겠죠! "
				+ "천체관측소에는 일반인이 관측 가능한 최대 구경의 반사망원경이 있습니다. 이 망원경은 사람의 눈보다 2만 "
				+ "배 밝게 볼 수 있어서 낮에도 금성을 볼 수 있다고 해요. 금성을 본 친구의 이야기를 들어볼까요?\n\n"
				+ "“책에서만 보던 금성을 이렇게 볼 수 있는 게 너무 신기해요!\n"
				+ "외계인도 볼 수 있는 망원경이 있다면 빨리 나왔으면 좋겠어요.\n" 
				+ "친구들이라 꼭 다시 한번 과학관에 와서 외계인도 보고 인사를 하고 싶어요.”",
			thumbnailUrl: "<c:url value="/resources/img/display/planetarium/observation/story16_01_01.jpg"/>",
			stories: [
				{
					storyLabel: "천체관측소",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/planetarium/observation/story16_01_01.jpg"/>"
				},
				{
					storyLabel: "천체관측소",
					storyDesc: "우주선을 타지 않고, 우주를 볼 수 있는 가장 빠른 방법은 망원경으로 우주의 모습을 관찰하는 것이겠죠! "
						+ "천체관측소에는 일반인이 관측 가능한 최대 구경의 반사망원경이 있습니다. 이 망원경은 사람의 눈보다 2만 "
						+ "배 밝게 볼 수 있어서 낮에도 금성을 볼 수 있다고 해요. 금성을 본 친구의 이야기를 들어볼까요?\n\n"
						+ "“책에서만 보던 금성을 이렇게 볼 수 있는 게 너무 신기해요!\n"
						+ "외계인도 볼 수 있는 망원경이 있다면 빨리 나왔으면 좋겠어요.\n" 
						+ "친구들이라 꼭 다시 한번 과학관에 와서 외계인도 보고 인사를 하고 싶어요.”",
					imgUrl: "<c:url value="/resources/img/display/planetarium/observation/story16_01_02.jpg"/>"
				},
				{
					storyLabel: "천체관측소",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/planetarium/observation/story16_02.jpg"/>"
				},
				{
					storyLabel: "천체관측소",
					storyDesc: "어른, 아이 할 것 없이 처음 보는 우주의 모습에 무척 들뜬 표정들 입니다. "
						+ "천체관측소에는 반사망원경 말고도 다양한 구경과 형식의 천체 망원경이 있어 행성과 위성, "
						+ "은하 등을 관찰할 수 있답니다. 우주를 만나는 가장 빠른 공간! 천체관측소를 절대 지나쳐서는 안되겠죠.\n"
						+ "계절별로 이 곳에서 관측할 수 있는 별들도 다르다고 하니까 미리 확인해보고, 꼭 보고 싶은 별이 보이는 날 방문해보는 것도 좋을 것 같네요!",
					imgUrl: "<c:url value="/resources/img/display/planetarium/observation/story16_03.jpg"/>"
				}
			]
		},
		{
			title: "외계인과의 만남, 우주탐사를 떠나다[스페이스 월드]",
			desc: "지금 제 눈 앞에 있는 스페이스 월드는 마치 우주 영화 속 주인공이 "
				+ "우주여행을 떠나기 전 훈련을 받는 공간 같은 외관을 자랑하고 있습니다.",
			thumbnailUrl: "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_01.jpg"/>",
			stories: [
				{
					storyLabel: "스페이스 월드",
					storyDesc: "지금 제 눈 앞에 있는 스페이스 월드는 마치 우주 영화 속 주인공이 "
						+ "우주여행을 떠나기 전 훈련을 받는 공간 같은 외관을 자랑하고 있습니다.",
					imgUrl: "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_01.jpg"/>"
				},
				{
					storyLabel: "스페이스 월드",
					storyDesc: "여러분도 스페이스 월드에 들어서기 전 우주여행을 준비하고 있는 우주 "
						+ "비행사가 된 기분으로 관람을 시작해보세요. 훨씬 많은 상상력들이 자극되어 여러분의 스페이스 월드 여행을 즐겁게 해줄 것입니다.",
					imgUrl: "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_02.jpg"/>"
				},
				{
					storyLabel: "우주로 보내는 메세지",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_03_01.jpg"/>"
				},
				{
					storyLabel: "시공간 우주여행",
					storyDesc: "입구에는 우주와 첫 인사를 하는 공간이 마련되어 있습니다. "
						+ "138억 년전의 과거로 시공간 우주 여행을 떠날 수 있는 전시물을 체험하고 나면, "
						+ "우주로 메시지를 보낼 수 있는 전시물이 있습니다. 40여 년 전 우주로 보낸 아레시보 메시지처럼, "
						+ "전파를 통해 직접 메시지를 보낼 수 있는 전시물이죠. 여러분도 우주 어딘가에서 살고 있는 외계인들에게 반가움의 마음을 전해보세요.",
					imgUrl: "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_03_02.jpg"/>"
				},
				{
					storyLabel: "우주여행 시작",
					storyDesc: "이제 본격적인 우주 여행을 시작해보겠습니다. 여러분은 외계인의 존재를 믿고 있나요? "
							+ "저는 광활한 우주 어딘가에 우리와 같이 공동체를 이루고 살아가는 생명들의 행성이 분명이 " 
							+ "있다고 믿고 있습니다. 만약 믿지 않는 친구들이라도 이 곳에서만큼은 열린 마음으로 어딘가에 "
							+ "존재하는 외계인들을 만나러 간다고 생각해보세요. 그럼 출발해볼까요?\n\n" 
							+ "이 곳에서는 지구와 같이 생명이 사는 엘피스 행성 속 생명의 나무를 살리는 협동 미션을 " 
							+ "수행하게 됩니다. 함께 온 친구들과 신나는 체험을 통해 우주 여행을 떠나게 되는 것이죠.",
					imgUrl: "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_04.jpg"/>"
				},
				{
					storyLabel: "갤럭시 스테이션",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_05_01.jpg"/>"
				},
				{
					storyLabel: "갤럭시 스테이션",
					storyDesc: "제가 지금 도착한 곳은 갤럭시 스테이션입니다. 우주대원들이 외계 생명체에게 보내고 "
						+ "받은 전파 신호를 검토하고, 외계 생명체의 존재를 확인하게 되는 체험을 할 수 있는 곳이죠. " 
						+ "우주를 연상하게 하는 거대한 공간에 600인치 초대형 스크린으로 보는 미디어 쇼는 " 
					 	+ "정말 한 편의 우주쇼를 보는 듯 환상적입니다. 정말 우주가 우리를 환영해주는 파티를 "
						+ "열어주는 것 같네요.",
					imgUrl: "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_05_02.jpg"/>"
				},
				{
					storyLabel: "스타쉽 전시관",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_06_01.jpg"/>"
				},
				{
					storyLabel: "스타쉽 전시관",
					storyDesc: "이제, 생명의 나무를 살리기 위한 본격적인 협동 미션을 수행하게 됩니다. 스타쉽 전시관 "
						+ "안으로 들어가면 모션 라이더와 3D 영상으로 광속 여행과 초신성 폭발, 유사 지구인 엘피스 "
						+ "행성의 모습을 체험할 수 있습니다. 여러분이 상상 속에 그려보았던 우주와 비슷한 모습인가요? "
						+ "실제로 우주 탐사를 하는 듯 짜릿하고 긴장감 넘치는 체험이 가득한 곳입니다.",
					imgUrl: "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_06_02.jpg"/>"
				},
				{
					storyLabel: "우주자료실",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_07_01.jpg"/>"
				},
				{
					storyLabel: "우주공작실",
					storyDesc: "모든 우주 탐사가 끝나면 우주공작실과 우주 자료실에서 여러분이 가졌던 궁금증과 호기심, "
						+ "창작에 대한 꿈을 펼쳐보세요. 그리고 앞으로 여러분이 실제로 떠나게 될 멀지 않은 미래의 " 
						+ "우주의 모습을 마음 속에 그려보세요.\n\n" 
						+ "우주는 이미 여러분을 기다리고 있습니다. 여러분이 우주에 더 많은 관심을 갖고, 질문을 " 
						+ "던진다면 무궁무진한 재미있는 이야기들이 여러분에게 다가올 것입니다. 국립과천과학관의 " 
						+ "천문관 전시들이 여러분에게 우주로 가는 문을 열어주었으면 좋겠습니다.",
					imgUrl: "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_07_02.jpg"/>"
				}
			]
		}
	];
	
	var storySource   = $("#story-template").html();
	var storyTemplate = Handlebars.compile(storySource);
	var storyHtml = storyTemplate({stories: stories});
	$("#stories").html(storyHtml);
</script>

<c:url var="shareUrl" value="/display/planetarium/spaceWorld" />
<c:import url="/WEB-INF/views/display/displayDetailModal.jsp">
	<c:param name="shareUrl" value="${shareUrl }"/>
	<c:param name="exhibitionName" value="스페이스월드" />
</c:import>

<script type="text/javascript">

	$(document).ready( function() {
		
		// 이미지 슬라이더
		//PTechSlider('#image-slider').autoSlide();
		PTechSlider('#image-slider',
			{
			slidesPerScreen: 4,
			indicators: true,
			height: 150
			}		
		);
		
		// 전시관 지도 flash outdoor로 전환.
		//setDefaultSVGMap('#basic-map-spy',3);
		// 지도 flash 해당 전시관 표시
		//$("g[id='XMLID_100_']").attr('class', 'active');
				
		// 이용안내 tab click event
		changeGuideTab();
		// 이용안내 첫 tab click
		$("a[data-name='program']").click();

	});
	
	function changeGuideTab() {
		$('.display-guide-wrapper .sub-nav .item').on('click', function() {
			
			var id = $(this).data('name');
			$('.display-guide-wrapper .sub-nav .item').removeClass('active');
			$(this).addClass('active');
			$('.display-guide-wrapper .guide-item').removeClass('active');
			$('#'+id+'.guide-item').addClass('active');
			event.preventDefault();
		});		
	}
	
	function showHideDetails()  {
		// hide all details first.
		$('.exhibition-detail-wrapper').hide();
		// show the active detail only.
		var exhibit_active_id = $('.major-exhibition-wrapper.active').data('id');
		$(exhibit_active_id).show();
		
		// click event for major exhibition box
		$('.major-exhibition-wrapper').on('click', function() {
			
			// get current active div & remove active
			var hide_id = $('.major-exhibition-wrapper.active').data('id');
			$('.major-exhibition-wrapper.active').removeClass('active');
			// hide the matching detail
			$(hide_id).hide();
			
			// add 'active' to the current clicked div
			var show_id = $(this).data('id');
			$(this).addClass('active');
			// show the mathcing detail
			$(show_id).show();
			
		});
	}
	
</script>


<form name="frmIndex">
	<input type="hidden" name="ACADEMY_CD" value="ACD007" />
	<input type="hidden" name="COURSE_CD" value="" />
	<input type="hidden" name="SEMESTER_CD" value="" />
	<input type="hidden" name="TYPE" value="" />
	<input type="hidden" name="FLAG" value="" />
	<input type="hidden" name="CLASS_CD" value="CL7002" />
</form>



<!-- Major Exhibition Contents Loading -->
<c:import url="/WEB-INF/views/display/majorExhibitionTemplate.jsp" />
<script type="text/javascript">
	var exhibitions = [
		{
			descriptions : [
				
				{
					title: "갤럭시 스테이션, 외계행성으로 여행을 떠나다",
					desc : "지상 1~2층의 원통형 디지털 미디어 쇼 공간으로, 32m의 초대형 스크린과 무빙 프로젝터, "
						+ "특수조명 연출이 갖춰져 있다. 엘피스 행성에서 지구로 보내진 우주 메시지를 확인하며 우주여행을 "
						+ "떠나는 스토리가 펼쳐진다." 
						+ "1997년 8월 15일 지구로 도달한 외계신호는 결국 지구 밖에 있는 또다른 지적 생명체, "
						+ "‘엘피스’의 신호로 밝혀지게 된다. 그들이 보내온 정보로 만들어진 갤럭시 스테이션 속에는 "
						+ "엘피스인들의 역사와 함께 도움을 요청하는 목소리가 담겨져 있는데... 과연 우주대원들은 "
						+ "사라져가는 엘피스의 문명을 지켜낼 수 있을까? 영상에서 확인해 보자. " 
				        + "<div class='additional-info'>(40명/회, 7세 이상)</div>"
				},
				{
					title: "KMTNET, 외계행성 사냥꾼",
					desc : "한국천문연구원에서 개발 중인 외계행성 탐색전용 망원경으로 미시 중력렌즈 효과를 포착하여 "
						+ "외계행성의 존재를 발견하는 망원경이다. 남반구의 호주, 칠레, 남아프리카공화국 세 나라에 "
						+ "설치되었으며 은하중심부의 별들을 24시간 내내 관측한다. 1.6m 구경과 고해상도의 CCD 카메라 "
						+ "덕분에 사람 눈으로 볼 수 있는 가장 어두운 별보다 약 52,000배나 더 어두운 별을 관측할 수 있다."
				},
				{
					title: "전자기파와 천체망원경, 빛으로 우주를 파헤치다",
					desc : "광학망원경으로 관찰한 우주는 빈 어둠이지만 전파망원경으로 보면 우주 모든 방향으로부터 같은 "
						+ "세기로 뿜어져 나오는 초단파가 관찰된다. 이것은 빅뱅 당시 퍼져 나온 빛의 흔적이 전 우주에 걸쳐 남아 "
						+ "있어 오늘날 관측되는 것으로 ‘우주배경복사’라고 한다. 우주배경복사처럼 우주에 있는 수많은 천체들은 다양한 "
						+ "파장의 전자기파를 방출한다. 여러 종류의 전자기파로 우주를 관측하면 가시광선을 통한 관측보다 더욱 많은 "
						+ "정보(천체의 특징과 생성, 소멸, 폭발 등)를 얻을 수 있다."
				}
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/planetarium/spaceWorld/major_exhibition_01.png' />",
					caption: "[갤럭시 스테이션]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/planetarium/spaceWorld/major_exhibition_02.png' />",
					caption: "[KMTNET 외계행성탐색전용망원경]"
				},
			]
		}
		
	];
	
	var exhibitionSource   = $("#major-exhibition-template").html();
	var exhibitionTemplate = Handlebars.compile(exhibitionSource);
	var exhibitionHtml = exhibitionTemplate({exhibitions: exhibitions});
	$("#main-exhibitions").html(exhibitionHtml);
	
	function ChangeClass(Cd){
		var frm = document.frmIndex;
		frm.CLASS_CD.value = Cd;
		frm.action = '<c:url value="/schedules"/>?${_csrf.parameterName}=${_csrf.token}';
		frm.method = 'post';
		frm.submit();
	}
</script>