<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-teal">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
		<%-- <nav id="scrollspy-nav"
			class="navbar-default navbar-static has-scroll">
			<div class="container">
				<ul class="nav navbar-nav">
					<c:import url="/WEB-INF/views/display/navbar.jsp"/>
					<li><a href="#basic-info-spy" class="item"> 기본 안내 </a></li>
					<li><a href="#basic-map-spy" class="item"> 천체관측소 안내 </a></li>
					<li>
						<a href="#major-exhibitions-spy" class="item">
							주요전시물
						</a>
					</li>
					<li>
						<a href="#story-spy" class="item">
							스토리 여행기
						</a>
					</li>
					<li><a href="#basic-guide-spy" class="item"> 프로그램 안내 </a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<div class="bread-crumbs">전시관 / 천체관측소</div>
					</li>
				</ul>

			</div>
		</nav> --%>
	</div>
	<div id="display-body" class="sub-body">
		<div class="narrow-sub-top display">
			<div class="sub-banner-tab-wrapper sub-banner-tab-teal">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
							<ul class="nav nav-tabs">
								<li class="col-xs-12">
									<a href="javascript:void(0)" onclick="ChangeClass('CL7003')" class="item">
										<i class="fa fa-calendar-check-o fa-lg" aria-hidden="true"></i> 천체관측소 예약하기
									</a> 		
								</li>
							</ul>   
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div id="basic-map-spy" class="section scrollspy">
				<div class="row">
					<div class="col-md-3">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							천체관측소 안내
						</h3>
					</div>
				</div>
				<div class="row description">
					<div class="col-sm-12 col-md-4 col-lg-4">
						<p>대형 천체망원경으로 보는 우주의 신비 전파망원경을 이용한 외계지적생명체 탐사</p>
						<ul class="circle-list teal">
							<li>유의사항
								<ul>
									<li>대상 : 초등학생 이상 (초, 중,고교 학생 단체는
										20명 이하로 교사동반 필수)</li>
										<li class="margin-bottom20">
											2009년생부터 참여 가능합니다. 미취학 아동은 공개관측을 이용하시기 바랍니다.
										</li>
								</ul>
							</li>
							<li>주요전시물
								<ul>
									<li>
										<b>「천체관측소」</b>는 1m 구경의 반사망원경을 비롯해 다양한 굴절 및 
										반사망원경으로 천체를 관측하는 공간이다. 
										낮에는 태양의 흑점, 홍염, 스펙트럼을 볼 수 있으며, 
										밤에는 다양한 망원경으로 계절별 대표 천체를 관측할 수 있다. 
									</li>
								</ul>
							</li>
						</ul>
					</div>

					<div class="col-sm-12 col-md-8 col-lg-8 text-right">
						<!-- select dropdown : 다른관으로 이동 -->
						<c:import url="/WEB-INF/views/display/displayHallSelect.jsp"/>
						<div>
							<c:import
								url="/WEB-INF/views/display/mainBuilding/svgs/display_svg_map_modified.jsp"></c:import>
						</div>
					</div>
				</div>
			</div>
			<div id="major-exhibitions-spy" class="section scrollspy">
				<div class="row">
					<div class="col-sm-3 col-md-2 col-lg-2">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							주요전시물					
						</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="main-exhibition-wrapper">
							<div id="main-exhibitions">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="story-spy" class="story-telling-wrapper">
				<div class="header">
					<div class="row">
						<div class="col-md-6">
							<h3 class="page-title teal">
								<div class="top-line"></div>
								스토리로 읽어 보는 천문관 여행기  
							</h3>
						</div>
					</div>
				</div>
				<div class="description">
					우리는 언제쯤 방학에 우주로 여행을 떠날 수 있을까요? 
					외계인과 친구처럼 메신저를 주고 받을 수 있는 날은 언제가 될까요? 
					잘 알 수 없기에 더 궁금하고 신비로운 우주! 국립과천과학관에 오면 
					다양한 모습의 우주를 만날 수 있습니다.
				</div>
				
				<div id="stories" class="row">
				</div>
			</div>

			<div class="related-photos">
				<div class="title">
					<i class="color-darkgreen material-icons">&#xE413;</i> 관련사진
				</div>
				<div id="image-slider">
					<div id="image-slider-window">
						<img
							src="<c:url value="/resources/img/display/planetarium/observation/related/1.png"/>"
							data-url="/resources/img/display/planetarium/observation/related/1.png"
							class="slide" /> 
						<img
							src="<c:url value="/resources/img/display/planetarium/observation/related/2.png"/>"
							data-url="/resources/img/display/planetarium/observation/related/2.png"
							class="slide" /> 
						<img
							src="<c:url value="/resources/img/display/planetarium/observation/related/3.png"/>"
							data-url="/resources/img/display/planetarium/observation/related/3.png"
							class="slide" /> 
						<img
							src="<c:url value="/resources/img/display/planetarium/observation/related/4.png"/>"
							data-url="/resources/img/display/planetarium/observation/related/4.png"
							class="slide" />
							<img
							src="<c:url value="/resources/img/display/planetarium/observation/related/5.png"/>"
							data-url="/resources/img/display/planetarium/observation/related/5.png"
							class="slide" />
							<img
							src="<c:url value="/resources/img/display/planetarium/observation/related/6.png"/>"
							data-url="/resources/img/display/planetarium/observation/related/6.png"
							class="slide" />
					</div>
				</div>
				<!--  image-slider END ... -->
			</div>
			<div id="basic-guide-spy" class="section scrollspy">
				<div class="row">
					<div class="col-xs-6 col-md-3">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							천체관측소 이용안내
						</h3>

					</div>
				</div>
				<div class="display-guide-wrapper">
					<div class="sub-nav">
						<div class="row">
							<div class="col-xs-12 col-md-6">
								<a href="#" class="item" data-name="program">천체관측소 프로그램 안내</a>
							</div>
							<div class="col-xs-12 col-md-6">
								<a href="#" class="item" data-name="group-reservation">단체예약
									안내</a>
							</div>
						</div>
					</div>

					<div id="program" class="guide-item">
						<div class="row margin-bottom20">
							<div class="col-md-12 text-right">
								<a href="javascript:void(0)" onclick="ChangeClass('CL7003')" class="reservation-btn"> 예약 및 확인(취소) 바로가기 </a>
							</div>
						</div>
						<div class="sub-section">
							<div class="row">
								<div class="col-md-12">
									<ul class="circle-list teal">
										<li>
											<span class="sub-section-title">
												천체관측소
											</span>
											<ul>
												<li>
													천체관측소는 일반인이 관측 가능한 최대 구경(1M)의 반사망원경과 다양한 종류의 천체망원경을 통해 천체를 관측하고
													천문학에 대한 과학적 지식을 배우는 곳입니다.												
												</li>
												<li>
													(천체관측소 예약 안내)
													<ul class="arrow-list teal">
														<li> 2015년 10월 20일부터 2016년 3월까지 태양관측1회(10:00-10:40)에 태양관측이 불가능하여 운영되지 않습니다.
														</li>
														<li> 12월 1일(화)~12월 13일(일) 동안 천체투영관 공사로 야간프로그램이 기존과 다르게 진행됩니다. 12월 참여 예정 관람객은 추후 공지되는 변경 프로그램을 확인 후 예약하시기 바랍니다.</li>
														<li> 12월 15일(화)~12월 20일(일)까지'겨울맞이 천문우주 특별행사'가 진행됩니다. 위 기간동안 천체관측소 모든 프로그램이 운영되지 않습니다.</li>
														<li> 천체관측소 예약 및 방문에 참고하시기 바랍니다.</li>
													</ul>
												</li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
						</div>

						<div class="sub-section">
							<div class="row">
								<div class="col-md-12">
									<ul class="circle-list teal">
										<li>
											<span class="sub-section-title">
												상영 일정 (~11월 말까지)
											</span>
											<ul class="arrow-list green">
												<li>
													프로그램에 대한 자세한 내용은 '수강신청 바로가기'를 통해 확인하시기 바랍니다.
												</li>
												<li>
													주간 프로그램
													<div class="table-responsive">
														<table
															class="table table-teal centered-table margin-bottom20">
															<thead>
																<tr>
																	<th>이름</th>
																	<th>시간</th>
																	<th>참가비</th>
																	<th>소요시간</th>
																	<th>인원수</th>
																	<th>비고</th>
																	<th>대상</th>
																</tr>
															</thead>
															<tbody>
																<tr>
																	<td>태양관측 1회</td>
																	<td>10:00 ~ 10:40</td>
																	<td>무료</td>
																	<td>40분</td>
																	<td>30</td>
																	<td>단체 전화예약</td>
																	<td>초등이상</td>
																</tr>
																<tr>
																	<td>태양관측 2회</td>
																	<td>11:00 ~ 11:40</td>
																	<td>무료</td>
																	<td>40분</td>
																	<td>30</td>
																	<td>단체 전화예약</td>
																	<td>초등이상</td>
																</tr>
																<tr>
																	<td>태양스케치</td>
																	<td>13:00 ~ 13:40</td>
																	<td>무료</td>
																	<td>40분</td>
																	<td>20</td>
																	<td>인터넷 선착순 예약</td>
																	<td>초등이상</td>
																</tr>
																<tr>
																	<td>공개관측</td>
																	<td>14:00 ~ 16:30</td>
																	<td>무료</td>
																	<td>10분</td>
																	<td>제한없음</td>
																	<td>공개관측(자유관람)</td>
																	<td>제한없음</td>
																</tr>
																<tr>
																	<td>전시해설</td>
																	<td>
																		<div>13:00 ~ 13:40</div>
																		<div>16:00 ~ 16:40</div>
																	</td>
																	<td>유료</td>
																	<td>40분</td>
																	<td>15</td>
																	<td>인터넷 선착순 예약</td>
																	<td>초등 5학년 이상</td>
																</tr>
															</tbody>
														</table>
													</div>
													<div class="notice">※태양관측 2회의 경우 단체예약이 없을 시나 일요일(공휴일)은
														개인 인터넷 20명 선착순 예약 진행됩니다.
													</div>
													<div class="notice">※오전 태양관측 1,2회와 오후 태양스케치는 교육내용이 동일하니
														중복예약을 피하시기 바랍니다.
													</div>
												</li>
												<li>
													야간 프로그램
													<div class="table-responsive">
														<table class="table table-teal centered-table margin-bottom20">
															<thead>
																<tr>
																	<th>이름</th>
																	<th>시간</th>
																	<th>참가비</th>
																	<th>소요시간</th>
																	<th>인원수</th>
																	<th>비고</th>
																	<th>대상</th>
																</tr>
															</thead>
															<tbody>
																<tr>
																	<td>야간천체관측</td>
																	<td>19:30 ~ 21:00(9~4월) 20:00 ~ 21:30(5~8월)</td>
																	<td>1만원/1인</td>
																	<td>90분</td>
																	<td>30</td>
																	<td>인터넷 선착순 예약</td>
																	<td>초등이상</td>
																</tr>
																<tr>
																	<td>토요관측회</td>
																	<td>토요일(시간 별도 공지)</td>
																	<td>1천원/1인</td>
																	<td>30~90분</td>
																	<td>300</td>
																	<td>인터넷 선착순 예약</td>
																	<td>제한없음</td>
																</tr>
																<tr>
																	<td>특별공개관측회</td>
																	<td>(시간 별도 공지)</td>
																	<td>무료</td>
																	<td>별도공지</td>
																	<td>별도공지</td>
																	<td>인터넷 선착순 예약</td>
																	<td>제한없음</td>
																</tr>
															</tbody>
														</table>
													</div>
													<div class="notice">
														※초등학생 이상 참여 프로그램의 경우 미취학 아동은 보호자 동반하여도 입장 불가능합니다.
													</div>
												</li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<!-- sub-section END ... -->

						<div class="sub-section">
							<div class="row">
								<div class="col-md-12">
									<ul class="circle-list teal">
										<li class="sub-section-title">
											상영 일정 (~11월 말까지)
										</li>
									</ul>
								</div>
							</div>
							<div class="content table-responsive">
								<table class="table table-teal centered-table margin-bottom20">
									<thead>
										<tr>
											<th>프로그램</th>
											<th>예약가능일</th>
											<th>예약장소</th>
											<th>1인당 예약 인원</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>주간프로그램</td>
											<td>참가예정일 7일전 09시<br> (예: 10일 예정->3일 09시 예약 시작)
											</td>
											<td>천체관측소 예약페이지</td>
											<td>1인( ID 1개)당 5명</td>
										</tr>
										<tr>
											<td>야간프로그램</td>
											<td>참가예정일 3일전 09시<br> (예: 5일 예정->2일 09시 예약 시작)
											</td>
											<td>천체관측소 예약페이지</td>
											<td>1인( ID 1개)당 5명</td>
										</tr>
										<tr>
											<td>토요(특별)관측회</td>
											<td>공지일부터 관측회 전날까지</td>
											<td>천체관측소 예약페이지</td>
											<td>1인( ID 1개)당 10명</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						<div class="sub-section">
							<div class="row">
								<div class="col-md-12">
									<ul class="circle-list teal">
										<li class="sub-section-title">
											프로그램 내용
										</li>
									</ul>
								</div>
							</div>
							<div class="content table-responsive">
								<table
									class="table table-bordered centered-table margin-bottom20">
									<thead>
										<tr>
											<th rowspan="2">프로그램</th>
											<th colspan="3">내용</th>
										</tr>
										<tr>
											<th><i class="material-icons sunny">&#xE430;</i> 맑은 날</th>
											<th><i class="material-icons cloudy">&#xE42D;</i> 흐린 날</th>
											<th>공통</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td width="30%">낮 프로그램</td>
											<td class="left">
												<ul class="black-dash-list">
													<li>태양관측 (흑점, 홍염, 스펙트럼 관찰)</li>
												</ul>
											</td>
											<td class="left">
												<ul class="black-dash-list">
													<li>태양의 물리적 성질 학습</li>
												</ul>
											</td>
											<td class="left">
												<ul class="black-dash-list">
													<li>망원경 설명 (망원경의 종류 및 원리)</li>
													<li>주망원경 관람, 관측 기본과정 설명</li>
												</ul>
											</td>
										</tr>
										<tr>
											<td>밤 프로그램</td>
											<td class="left">
												<ul class="black-dash-list">
													<li>천체관측 (별자리, 행성, 성단 등)</li>
													<li>관측한 천체에 대한 학습</li>
												</ul>
											</td>
											<td class="left">
												<ul class="black-dash-list">
													<li>망원경 설명 (종류 및 원리)</li>
													<li>전파망원경 기초 원리</li>
												</ul>
											</td>
											<td class="left">
												<ul class="black-dash-list">
													<li>천문 영상 관람 + 별자리 설명 (천체투영관에서 진행)</li>
													<li>망원경 실습 (작동 원리 이해, 지향 실습)</li>
													<li>주망원경 관람, 관측 기본과정 설명</li>
												</ul>
											</td>
										</tr>
										<tr>
											<td>토요(특별)관측회</td>
											<td class="left">
												<ul class="black-dash-list">
													<li>계절별자리 안내</li>
													<li>천체 망원경을 통한 관측체험</li>
												</ul>
											</td>
											<td class="left">
												<ul class="black-dash-list">
													<li>천체투영관 가상별자리 안내</li>
												</ul>
											</td>
											<td>우천시에도 흐린날과 동일하게 진행됩니다.</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!--  id= program END ... -->

					<div id="group-reservation" class="guide-item">
						<div class="sub-section">
							<div class="row">
								<div class="col-md-12">
									<ul class="circle-list teal">
										<li>
											<span class="sub-section-title">
												단체예약 대상
											</span>
											<ul class="arrow-list teal">
												<li>
													초, 중, 고교, 대학생 및 성인
												</li>
												<li>
													주말, 공휴일, 방학(1~2월, 7~8월) 예약 불가
												</li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="sub-section">
							<div class="row">
								<div class="col-md-12">
									<ul class="circle-list teal">
										<li>
											<span class="sub-section-title">
												단체예약 프로그램별 가능 범위
											</span>
											<ul class="arrow-list teal">
												<li>
													프로그램 별 예약방법 및 참여가능 대상이 다르기 때문에 확인 후 예약하시기 바랍니다.
												</li>
												<li>
													프로그램 상세내용은 예약페이지에서 확인 가능합니다.
												</li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
							<div class="table-responsive">
								<table class="table table-bordered centered-table">
									<thead>
										<tr>
											<th>프로그램명</th>
											<th>시간</th>
											<th>예약방법</th>
											<th>대상</th>
											<th>예약 가능 범위</th>
											<th>예약 가능 요일</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>태양관측 1회<br> (무료 프로그램)
											</td>
											<td>10:00 ~ 10:40<br> (40분 소요)
											</td>
											<td rowspan="2">전화 예약</td>
											<td rowspan="2">초등학교 이상<br> <span class="color-red">(미취학
													아동 불가)</span>
											</td>
											<td rowspan="2">20명 ~ 50명<br> (30명 이상 예약시 태양교육 제외)
											</td>
											<td rowspan="3">화요일 ~ 토요일<br> (월요일 휴관)
											</td>
										</tr>
										<tr>
											<td>태양관측 2회<br> (무료 프로그램)
											</td>
											<td>11:00 ~ 11:40<br> (40분 소요)
											</td>
										</tr>
										<tr>
											<td>공개 관측<br> (무료 프로그램)
											</td>
											<td>14:00 ~ 16:30<br> (1분 소요/1인)
											</td>
											<td>전화 예약</td>
											<td>연령제한 없음</td>
											<td>10명 ~ 60명</td>
										</tr>
										<tr>
											<td>야간천체관측<br> (유료 프로그램)
											</td>
											<td>19:20 ~ 21:00<br> (90분 소요)
											</td>
											<td>전화 문의 후 참가신청서 접수</td>
											<td>초등학교 이상<br> <span class="color-red">(미취학
													아동 불가)</span>
											</td>
											<td>15명 ~ 30명</td>
											<td>화, 수, 목요일</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="sub-section">
							<div class="row">
								<div class="col-md-7">
									<ul class="circle-list teal">
										<li>
											<span class="sub-section-title">
												단체예약 방법
											</span>
											<ul class="arrow-list teal">
												<li>
													천체관측소(02-3677-1565)로 전화예약
												</li>
												<li>
													태양관측 1,2회는 인원수가 30명 이상일 경우 태양교육 제외(전화 상세문의)
												</li>
												<li>
													야간천체관측은 전화로 참여예정일 예약가능 여부 확인 후 참가신청서 접수
												</li>
											</ul>
										</li>
									</ul>
									<span class="visible-xs visible-sm margin-bottom20"></span>
								</div>
								<!-- 현재 기존사이트의 단체 참가신청서 버튼 없음. 확인 필요. -->
								<!-- <div class="col-md-5 text-right">
									<a href="#" class="download-btn"> <i class="material-icons">&#xE2C4;</i>
										야간 천체관측소 단체 참가신청서
									</a>
								</div> -->
							</div>
						</div>
						<div class="sub-section">
							<div class="row">
								<div class="col-md-12">
									<ul class="circle-list teal">
										<li>
											<span class="sub-section-title">
												유의사항
											</span>
											<ul class="arrow-list teal">
												<li>
													프로그램 시작 5분 전까지 천체관측소로 도착해주시기 바랍니다.
												</li>
												<li>
													<span class="color-red">프로그램 시작시간까지 미도착시 자동 취소</span>
												</li>
												<li>
													관측소 특성상 인솔자가 1명 이상 포함되어 있어야 합니다.
												</li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="sub-section">
							<div class="row">
								<div class="col-md-12">
									<ul class="circle-list teal">
										<li>
											<span class="sub-section-title">
												기타문의
											</span>
											<ul class="arrow-list teal">
												<li>
													02-3677-1565
												</li>
												<li>
													문의시간: 09:30 ~ 17:30
												</li>
												<li>
													월요일 휴관
												</li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>


<script id="story-template" type="text/x-handlebars-template">
{{#each stories}}
<div class="col-xs-12 col-sm-4 col-md-4">
	<div class="display-card story-telling story-more-btn pointer" data-idx="{{@index}}" onclick="">
		<div class="card-image">
			<img src="{{thumbnailUrl}}" class="img-responsive"/>
		</div>
		<div class="card-content">
			<div class="display-card-title">
				{{title}}
			</div>
			<div class="display-card-content">
				{{{desc}}}
			</div>
			
		</div>
	</div>							
</div>
{{/each}}
</script>

<script type="text/javascript">
	var stories = [
		{
			title: "밤하늘 이야기를 보다 [천체투영관]",
			desc: "영화를 보는 듯 커다란 돔 스크린이 있는 천체투영관의 상영관에 들어서면 눈 앞에 광활한 하늘이 펼쳐집니다. "
				+ "여러분은 모두 자신의 별자리를 알고 있죠? 그 이야기의 주인공인 별자리를 실제 밤하늘에서 눈으로 확인하기는 "
				+ "쉽지 않은데요. 오늘은 화면 속 밤하늘에서 궁금했던 " 
				+ "별자리의 모습을 자세하게 볼 수 있답니다. " 
				+ "불이 꺼지고 반짝이는 별과 다양한 하늘 이야기가 담긴 영상이 펼쳐지자 여기저기서 "
				+ "탄성이 터져나옵니다. 영상이 끝난 후 관람객들의 얼굴을 보니 공기 좋고 경치 좋은 곳에서 "
				+ "별이 가득한 밤하늘을 구경하고 온 듯 편안하고 즐거워 보이네요.",
			thumbnailUrl: "<c:url value="/resources/img/display/planetarium/planetarium/story15_01_01.jpg"/>",
			stories: [
				{
					storyLabel: "별자리의 모습",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/planetarium/planetarium/story15_01_01.jpg"/>"
				},
				{
					storyLabel: "별자리의 모습",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/planetarium/planetarium/story15_01_02.jpg"/>"
				},
				{
					storyLabel: "별자리의 모습",
					storyDesc: "영화를 보는 듯 커다란 돔 스크린이 있는 천체투영관의 상영관에 들어서면 눈 앞에 광활한 하늘이 펼쳐집니다. "
						+ "여러분은 모두 자신의 별자리를 알고 있죠? 그 이야기의 주인공인 별자리를 실제 밤하늘에서 눈으로 확인하기는 "
						+ "쉽지 않은데요. 오늘은 화면 속 밤하늘에서 궁금했던 " 
						+ "별자리의 모습을 자세하게 볼 수 있답니다. " 
						+ "불이 꺼지고 반짝이는 별과 다양한 하늘 이야기가 담긴 영상이 펼쳐지자 여기저기서 "
						+ "탄성이 터져나옵니다. 영상이 끝난 후 관람객들의 얼굴을 보니 공기 좋고 경치 좋은 곳에서 "
						+ "별이 가득한 밤하늘을 구경하고 온 듯 편안하고 즐거워 보이네요.",
					imgUrl: "<c:url value="/resources/img/display/planetarium/planetarium/story15_01_03.jpg"/>"
				},
				{
					storyLabel: "별들의 삷",
					storyDesc: "상영관을 나오면 여러 장의 별자리 사진이 전시되어 있는데요. "
						+ "형형색색의 아름다운 별자리 마다 붙여진 ‘심장이 뛰는 태양’, ‘숨은 아기별 찾기’, "
						+ "‘남반구 밤하늘의 크리스마스 리스’, ‘빛의 메아리’와 같은 재미있는 이름을 보면서 별자리를 관찰해보는 "
						+ "것도 색다른 재미가 있을 것입니다. 여러분도 별자리 사진을 보면서 여러분만의 제목을 붙여보세요.\n\n"
						+ "다음 전시관으로 이동하면서 여러분이 오늘 밤하늘에서 보고 싶은 별과 재미있게 봤던 별자리 이야기를 친구, \n"
						+ "가족들과 나눠보세요. 오랜 시간 후에도 떠 올릴 반짝반짝 빛나는 추억과 이야기가 훨씬 많아졌을 것입니다.",
					imgUrl: "<c:url value="/resources/img/display/planetarium/planetarium/story15_02.jpg"/>"
				}
				
			]
		},
		{
			title: "남녀노소 누구에게나 웃음을 선물하는 곳[천체관측소]",
			desc: "우주선을 타지 않고, 우주를 볼 수 있는 가장 빠른 방법은 망원경으로 우주의 모습을 관찰하는 것이겠죠! "
				+ "천체관측소에는 일반인이 관측 가능한 최대 구경의 반사망원경이 있습니다. 이 망원경은 사람의 눈보다 2만 "
				+ "배 밝게 볼 수 있어서 낮에도 금성을 볼 수 있다고 해요. 금성을 본 친구의 이야기를 들어볼까요?\n\n"
				+ "“책에서만 보던 금성을 이렇게 볼 수 있는 게 너무 신기해요!\n"
				+ "외계인도 볼 수 있는 망원경이 있다면 빨리 나왔으면 좋겠어요.\n" 
				+ "친구들이라 꼭 다시 한번 과학관에 와서 외계인도 보고 인사를 하고 싶어요.”",
			thumbnailUrl: "<c:url value="/resources/img/display/planetarium/observation/story16_01_01.jpg"/>",
			stories: [
				{
					storyLabel: "천체관측소",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/planetarium/observation/story16_01_01.jpg"/>"
				},
				{
					storyLabel: "천체관측소",
					storyDesc: "우주선을 타지 않고, 우주를 볼 수 있는 가장 빠른 방법은 망원경으로 우주의 모습을 관찰하는 것이겠죠! "
						+ "천체관측소에는 일반인이 관측 가능한 최대 구경의 반사망원경이 있습니다. 이 망원경은 사람의 눈보다 2만 "
						+ "배 밝게 볼 수 있어서 낮에도 금성을 볼 수 있다고 해요. 금성을 본 친구의 이야기를 들어볼까요?\n\n"
						+ "“책에서만 보던 금성을 이렇게 볼 수 있는 게 너무 신기해요!\n"
						+ "외계인도 볼 수 있는 망원경이 있다면 빨리 나왔으면 좋겠어요.\n" 
						+ "친구들이라 꼭 다시 한번 과학관에 와서 외계인도 보고 인사를 하고 싶어요.”",
					imgUrl: "<c:url value="/resources/img/display/planetarium/observation/story16_01_02.jpg"/>"
				},
				{
					storyLabel: "천체관측소",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/planetarium/observation/story16_02.jpg"/>"
				},
				{
					storyLabel: "천체관측소",
					storyDesc: "어른, 아이 할 것 없이 처음 보는 우주의 모습에 무척 들뜬 표정들 입니다. "
						+ "천체관측소에는 반사망원경 말고도 다양한 구경과 형식의 천체 망원경이 있어 행성과 위성, "
						+ "은하 등을 관찰할 수 있답니다. 우주를 만나는 가장 빠른 공간! 천체관측소를 절대 지나쳐서는 안되겠죠.\n"
						+ "계절별로 이 곳에서 관측할 수 있는 별들도 다르다고 하니까 미리 확인해보고, 꼭 보고 싶은 별이 보이는 날 방문해보는 것도 좋을 것 같네요!",
					imgUrl: "<c:url value="/resources/img/display/planetarium/observation/story16_03.jpg"/>"
				}
			]
		},
		{
			title: "외계인과의 만남, 우주탐사를 떠나다[스페이스 월드]",
			desc: "지금 제 눈 앞에 있는 스페이스 월드는 마치 우주 영화 속 주인공이 "
				+ "우주여행을 떠나기 전 훈련을 받는 공간 같은 외관을 자랑하고 있습니다.",
			thumbnailUrl: "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_01.jpg"/>",
			stories: [
				{
					storyLabel: "스페이스 월드",
					storyDesc: "지금 제 눈 앞에 있는 스페이스 월드는 마치 우주 영화 속 주인공이 "
						+ "우주여행을 떠나기 전 훈련을 받는 공간 같은 외관을 자랑하고 있습니다.",
					imgUrl: "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_01.jpg"/>"
				},
				{
					storyLabel: "스페이스 월드",
					storyDesc: "여러분도 스페이스 월드에 들어서기 전 우주여행을 준비하고 있는 우주 "
						+ "비행사가 된 기분으로 관람을 시작해보세요. 훨씬 많은 상상력들이 자극되어 여러분의 스페이스 월드 여행을 즐겁게 해줄 것입니다.",
					imgUrl: "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_02.jpg"/>"
				},
				{
					storyLabel: "우주로 보내는 메세지",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_03_01.jpg"/>"
				},
				{
					storyLabel: "우주로 보내는 메세지",
					storyDesc: "입구에는 우주와 첫 인사를 하는 공간이 마련되어 있습니다. "
						+ "150억 년전의 과거로 시공간 우주 여행을 떠날 수 있는 전시물을 체험하고 나면, "
						+ "우주로 메시지를 보낼 수 있는 전시물이 있습니다. 40여 년 전 우주로 보낸 아레시보 메시지처럼, "
						+ "전파를 통해 직접 메시지를 보낼 수 있는 전시물이죠. 여러분도 우주 어딘가에서 살고 있는 외계인들에게 반가움의 마음을 전해보세요.",
					imgUrl: "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_03_02.jpg"/>"
				},
				{
					storyLabel: "우주여행 시작",
					storyDesc: "이제 본격적인 우주 여행을 시작해보겠습니다. 여러분은 외계인의 존재를 믿고 있나요? "
							+ "저는 광활한 우주 어딘가에 우리와 같이 공동체를 이루고 살아가는 생명들의 행성이 분명이 " 
							+ "있다고 믿고 있습니다. 만약 믿지 않는 친구들이라도 이 곳에서만큼은 열린 마음으로 어딘가에 "
							+ "존재하는 외계인들을 만나러 간다고 생각해보세요. 그럼 출발해볼까요?\n\n" 
							+ "이 곳에서는 지구와 같이 생명이 사는 엘피스 행성 속 생명의 나무를 살리는 협동 미션을 " 
							+ "수행하게 됩니다. 함께 온 친구들과 신나는 체험을 통해 우주 여행을 떠나게 되는 것이죠.",
					imgUrl: "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_04.jpg"/>"
				},
				{
					storyLabel: "갤럭시 스테이션",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_05_01.jpg"/>"
				},
				{
					storyLabel: "갤럭시 스테이션",
					storyDesc: "제가 지금 도착한 곳은 갤럭시 스테이션입니다. 우주대원들이 외계 생명체에게 보내고 "
						+ "받은 전파 신호를 검토하고, 외계 생명체의 존재를 확인하게 되는 체험을 할 수 있는 곳이죠. " 
						+ "우주를 연상하게 하는 거대한 공간에 600인치 초대형 스크린으로 보는 미디어 쇼는 " 
					 	+ "정말 한 편의 우주쇼를 보는 듯 환상적입니다. 정말 우주가 우리를 환영해주는 파티를 "
						+ "열어주는 것 같네요.",
					imgUrl: "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_05_02.jpg"/>"
				},
				{
					storyLabel: "스타쉽 전시관",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_06_01.jpg"/>"
				},
				{
					storyLabel: "스타쉽 전시관",
					storyDesc: "이제, 생명의 나무를 살리기 위한 본격적인 협동 미션을 수행하게 됩니다. 스타쉽 전시관 "
						+ "안으로 들어가면 모션 라이더와 3D 영상으로 광속 여행과 초신성 폭발, 유사 지구인 엘피스 "
						+ "행성의 모습을 체험할 수 있습니다. 여러분이 상상 속에 그려보았던 우주와 비슷한 모습인가요? "
						+ "실제로 우주 탐사를 하는 듯 짜릿하고 긴장감 넘치는 체험이 가득한 곳입니다.",
					imgUrl: "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_06_02.jpg"/>"
				},
				{
					storyLabel: "우주공작실",
					storyDesc: "",
					imgUrl: "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_07_01.jpg"/>"
				},
				{
					storyLabel: "우주공작실",
					storyDesc: "모든 우주 탐사가 끝나면 우주공작실과 우주 자료실에서 여러분이 가졌던 궁금증과 호기심, "
						+ "창작에 대한 꿈을 펼쳐보세요. 그리고 앞으로 여러분이 실제로 떠나게 될 멀지 않은 미래의 " 
						+ "우주의 모습을 마음 속에 그려보세요.\n\n" 
						+ "우주는 이미 여러분을 기다리고 있습니다. 여러분이 우주에 더 많은 관심을 갖고, 질문을 " 
						+ "던진다면 무궁무진한 재미있는 이야기들이 여러분에게 다가올 것입니다. 국립과천과학관의 " 
						+ "천문관 전시들이 여러분에게 우주로 가는 문을 열어주었으면 좋겠습니다.",
					imgUrl: "<c:url value="/resources/img/display/planetarium/spaceWorld/story17_07_02.jpg"/>"
				}
			]
		}
	];
	
	var storySource   = $("#story-template").html();
	var storyTemplate = Handlebars.compile(storySource);
	var storyHtml = storyTemplate({stories: stories});
	$("#stories").html(storyHtml);
</script>

<c:url var="shareUrl" value="/display/planetarium/observation" />
<c:import url="/WEB-INF/views/display/displayDetailModal.jsp">
	<c:param name="shareUrl" value="${shareUrl }"/>
	<c:param name="exhibitionName" value="천체관측소" />
</c:import>


<script type="text/javascript">
	$(document).ready(function() {
		// 주요 전시물 클릭시 show hide event
		showHideDetails();
		// 이미지 슬라이더
		//PTechSlider('#image-slider').autoSlide();
		PTechSlider('#image-slider', {
			slidesPerScreen : 4,
			indicators : true,
			height : 150
		});

		// 전시관 지도 flash outdoor로 전환.
		setDefaultSVGMap('#basic-map-spy', 3);
		// 지도 flash 해당 전시관 표시
		$("g[id='XMLID_108_']").attr('class', 'active');

		// 이용안내 tab click event
		changeGuideTab();
		// 이용안내 첫 tab click
		$("a[data-name='program']").click();

	});

	function showHideDetails() {
		// hide all details first.
		$('.exhibition-detail-wrapper').hide();
		// show the active detail only.
		var exhibit_active_id = $('.major-exhibition-wrapper.active')
				.data('id');
		$(exhibit_active_id).show();

		// click event for major exhibition box
		$('.major-exhibition-wrapper').on('click', function() {

			// get current active div & remove active
			var hide_id = $('.major-exhibition-wrapper.active').data('id');
			$('.major-exhibition-wrapper.active').removeClass('active');
			// hide the matching detail
			$(hide_id).hide();

			// add 'active' to the current clicked div
			var show_id = $(this).data('id');
			$(this).addClass('active');
			// show the mathcing detail
			$(show_id).show();

		});
	}

	function changeGuideTab() {
		$('.display-guide-wrapper .sub-nav .item').on('click', function() {

			var id = $(this).data('name');
			$('.display-guide-wrapper .sub-nav .item').removeClass('active');
			$(this).addClass('active');
			$('.display-guide-wrapper .guide-item').removeClass('active');
			$('#' + id + '.guide-item').addClass('active');
			event.preventDefault();
		});
	}
</script>

<form name="frmIndex">
	<input type="hidden" name="ACADEMY_CD" value="ACD007" />
	<input type="hidden" name="COURSE_CD" value="" />
	<input type="hidden" name="SEMESTER_CD" value="" />
	<input type="hidden" name="TYPE" value="" />
	<input type="hidden" name="FLAG" value="" />
	<input type="hidden" name="CLASS_CD" value="CL7002" />
</form>


<!-- Major Exhibition Contents Loading -->
<c:import url="/WEB-INF/views/display/majorExhibitionTemplate.jsp" />
<script type="text/javascript">
	var exhibitions = [
		{
			descriptions : [
				
				{
					title: "1m 반사망원경, 어두운 천체를 파헤치다",
					desc : "천체관측소의 1m 반사망원경은 국내에서 두 번째로 크며, 일반인이 관측할 수 있는 최대 구경의 망원경이다. "
						+ "이 망원경으로는 사람의 눈보다 2만 배 밝게 볼 수 있으므로 아주 어두운 성단이나 성운을 관측할 수 있고, "
						+ "밝은 낮에도 별을 볼 수 있다. 아늑한 원형 돔 안에서 우주로 여행을 떠나는 건 어떨까."
				}
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/planetarium/observation/major_exhibition_01.png' />",
					caption: "[천체관측소 전경]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/planetarium/observation/major_exhibition_02.png' />",
					caption: "[1m 반사망원경]"
				},
			]
		},
		{
			descriptions : [
				{
					title: "보조 망원경, 다양한 망원경으로 별 엿보기",
					desc : "천체관측소의 보조관측실에는 다양한 굴절 및반사망원경 7대가 고정되어 있다. "
						+ "이 망원경으로 주간에는 태양의 흑점, 홍염, 스펙트럼을, 야간에는 달, 행성, 별들을 관측한다."
				},
				{
					title: "태양의 암호, 태양을 통해 밝혀진 사실을 알아보자",
					desc : "천체관측소 1층에 위치한 이 전시물은 고대의 아리스타르코스에서 중세의 갈릴레오를 "
						+ "거쳐 현대의 아인슈타인에 이르기까지 6명의 과학자가 태양을 통해 알아낸 사실들을 직접 "
						+ "실험하면서 알아가는 체험형 전시물이다." 
						+ "<div class='addtional-info'>(15명/회, 초등 5학년 이상, 유료)</div>"
				}
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/planetarium/observation/major_exhibition_03.png' />",
					caption: "[보조 망원경]"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/planetarium/observation/major_exhibition_04.png' />",
					caption: "[태양의 암호]"
				},
			]
		},
		{
			descriptions : [
				{
					title: "주간관측 프로그램, 태양을 그려보자",
					desc : "오후 1시 ‘태양스케치’는 다양한 태양망원경으로 태양의 흑점과 홍염 및 태양 스펙트럼을 "
						+ "관측하여 직접 그려볼 수 있다. 오후 2시부터 4시 30분까지는 예약 없이 누구나 참여 가능한 "
						+ "공개관측이 진행된다. 1m 반사망원경을 통해 낮에 볼 수 있는 1등성 별과 금성 관측이 가능하다."
						+ "<div class='addtional-info'>(20명/회, 초등 1학년 이상)</div>"
				},
				{
					title: "야간관측 프로그램, 별나라로 여행을 떠나자",
					desc : "매일 저녁 천체투영관에서는 망원경 관련 영상을 감상할 수 있고 가상 별자리에 대한 설명을 들을 수 있다. "
						+ "천체관측소에서는 계절별 대표 천체 관측과 실습용 소형망원경 조작으로 별을 직접 찾아볼 수 있다. "
						+ "관측소에서 인기를 끌고 있는 블루마블 여권에 관측한 천체 도장을 받아보자. 사계절 도장을 모두 받은 "
						+ "참가자에게는 소정의 상품이 주어진다." 
						+ "<div class='addtional-info'>(30명/회, 초등 1학년 이상, 유료)</div>"
				}
			],
			images: [
				{
					imageUrl: "<c:url value='/resources/img/display/planetarium/observation/major_exhibition_05.png' />",
					caption: "[태양 스케치]-관측된 태양 모습"
				},
				{
					imageUrl: "<c:url value='/resources/img/display/planetarium/observation/major_exhibition_06.png' />",
					caption: "[야간 관측 프로그램]"
				},
			]
		}
		
	];
	
	var exhibitionSource   = $("#major-exhibition-template").html();
	var exhibitionTemplate = Handlebars.compile(exhibitionSource);
	var exhibitionHtml = exhibitionTemplate({exhibitions: exhibitions});
	$("#main-exhibitions").html(exhibitionHtml);
	
	function ChangeClass(Cd){
		var frm = document.frmIndex;
		frm.CLASS_CD.value = Cd;
		frm.action = '<c:url value="/schedules"/>?${_csrf.parameterName}=${_csrf.token}';
		frm.method = 'post';
		frm.submit();
	}
</script>