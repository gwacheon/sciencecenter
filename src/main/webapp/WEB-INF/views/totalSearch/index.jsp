<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-teal">
	   <nav id="scrollspy-nav" class="navbar-default navbar-static has-scroll">
			<div class="container">
				<ul class="nav navbar-nav">
					<c:import url="/WEB-INF/views/totalSearch/navbar.jsp"/>
					<li>
						<a href="#total-search" class="item">
							검색 결과
						</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
				    <li>
						<div class="bread-crumbs">
							검색 결과
						</div>
				    </li>
				</ul>
			</div>
		</nav>
	</div>
	<div id="total-search" class="sub-body">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title teal">
						<div class="top-line"></div>
						&quot;<c:out value="${searchKey }"/>&quot; 검색 결과
					</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="count-box">
						총 <span class="count"><c:out value='${totalCount }' /></span> 건 검색되었습니다.					
					</div>
					<div class="result-wrapper">
						<table class="table table-teal">
							<thead>
								<tr>
									<th rowspan="2">
										출처
									</th>
									<th>
										제목 / 내용
									</th>
									<th rowspan="2">
										작성자
									</th>
									<th rowspan="2">
										등록일
									</th>											
								</tr>
							</thead>
							<tbody>
								<c:forEach var="result" items="${searchResults}">
									<tr>
										<td rowspan="2">
											<spring:message code='board.${result.key.boardType }'/>
										</td>
										<td class="title">
											<a href="<c:url value='/${result.value.category }/${result.value.name }/${result.key.id }' />">
												<c:out value='${result.key.title }' />
											</a><br>
										</td>
										<td rowspan="2">
											<c:out value='${result.key.member.memberName }' />
										</td>
										<td rowspan="2">
											<fmt:formatDate value="${result.key.regDttm}" pattern="yyyy-MM-dd"/>
										</td>
									</tr>
									<tr>
										<td class="content">
											<c:out value="${result.key.content }" escapeXml="false"/>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<c:import url="/WEB-INF/views/layouts/pagination.jsp"></c:import>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('.result-wrapper .content').each( function() {
		var val = $(this).text();
		$(this).text(val);
	});
	
	searchHighlight('.result-wrapper .title', '${searchKey}');
	searchHighlight('.result-wrapper .content', '${searchKey}');
	
	function searchHighlight( container, searchKey ) {
		$(container).each( function() {
			 var src_str = $(this).html();
			 searchKey = searchKey.replace(/(\s+)/,"(<[^>]+>)*$1(<[^>]+>)*");
			 var pattern = new RegExp("("+searchKey+")", "gi");
			 src_str = src_str.replace(pattern, "<mark>$1</mark>");
			 src_str = src_str.replace(/(<mark>[^<>]*)((<[^>]+>)+)([^<>]*<\/mark>)/,"$1</mark>$2<mark>$4");

			 $(this).html(src_str);
		});
	} 
</script>

