<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<li class="dropdown">
	<a class="dropdown-toggle" type="button" data-toggle="dropdown" role="button" 
		aria-haspopup="true" aria-expanded="true">
		카테고리 이동<i class="fa fa-angle-down"></i>
	</a>
	<ul class="dropdown-menu">
		<c:set var="categoryNo" value="7"/>
		<c:forEach var="i" begin="1" end="8">
			<spring:message var="main_category" code="main.nav.catetory${i}" scope="application" text='' />
			
			<c:if test="${not empty main_category }">
				<spring:message var="main_category_url" code="main.nav.catetory${i}.url" scope="application" text='' />
				<li>
					<a href="<c:url value="${main_category_url }" />" >
						<c:out value="${main_category }" />
					</a>
				</li>
			</c:if>
		</c:forEach>
	</ul>
</li>