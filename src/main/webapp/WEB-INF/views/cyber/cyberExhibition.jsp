<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-green">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>
	<%-- <div class="sub-content-nav scrollspy-green">
	   <nav id="scrollspy-nav" class="navbar-default navbar-static has-scroll">
            <div class="container">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
						<a class="dropdown-toggle" type="button" data-toggle="dropdown" role="button" 
							aria-haspopup="true" aria-expanded="true">
							<spring:message code="${currentCategory }" text=''/><i class="material-icons">&#xE313;</i>
						</a>
						<ul class="dropdown-menu">
							<c:forEach var="i" begin="1" end="9">
								<c:set var="main_category" value="main.nav.catetory${i }"/>
								<spring:message var="main_category_url" code="main.nav.catetory${i }.url" scope="application"/>
								<c:if test="${not empty main_category && main_category eq currentCategory}">
								 	<c:forEach var="j" begin="1" end="12">
										<spring:message var="sub_category" code="main.nav.catetory${i}.sub${j}" scope="application" text='' />
										<spring:message var="sub_category_url" code="main.nav.catetory${i}.sub${j}.url" scope="application" text='' />
										<c:if test="${not empty sub_category }">
											<li>
												<a href="<c:url value="${sub_category_url }" />" >
													<c:out value="${sub_category }" />
												</a>
											</li>
										</c:if>
									</c:forEach>
								</c:if>
							</c:forEach>
						</ul>
					</li>
                    <li><a href="#basic-info-spy" class="item"> 사이버 전시관 </a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <div class="bread-crumbs">사이버과학관 / 사이버 전시관</div>
                    </li>
                </ul>
            </div>
        </nav>
	</div> --%>
	<div id="cyber-body" class="sub-body">
		<div id="basic-info-spy"class="container">
			<div class="row">
				<div class="col-md-4">
					<h3 class="page-title green">
						<div class="top-line"></div>
						사이버 전시관
					</h3>
				</div>
			</div>
			<div class="row margin-bottom30">
				<div class="col-xs-6 col-md-6">
					<div>
						사이버 전시관에서 모션시뮬레이터와 3D입체영상을 통해 전시물을
						체험하실 수 있습니다.
					</div>
					<ul class="circle-list green">
						<li>사이버 전시관 지도</li>
					</ul>
				</div>
				<div class="col-xs-6 col-md-6 text-right">
					<div class="banner-link">
						<a href="http://cyber.scientorium.go.kr/museum_renewal/index.jsp#b">
							사이버전시관 바로가기 </a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div id="map-wrapper-outdoor" class="map-wrapper">
						<c:import url="/WEB-INF/views/cyber/svgs/cyber_svg_map.jsp"></c:import>
					</div>
				</div>
				<div class="col-md-12">
					<div id="map-wrapper-floor1" class="map-wrapper">
						<c:import url="/WEB-INF/views/cyber/svgs/cyber_svg_map.jsp"></c:import>
					</div>
				</div>
				<div class="col-md-12">
					<div id="map-wrapper-floor2" class="map-wrapper">
						<c:import url="/WEB-INF/views/cyber/svgs/cyber_svg_map.jsp"></c:import>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$( function() {
		
		// create selectors
		var page_outdoor_wrapper = $('#map-wrapper-outdoor').find('#svg-page-3-wrapper');
		var page_1_wrapper = $('#map-wrapper-floor1').find('#svg-page-1-wrapper');
		var page_2_wrapper = $('#map-wrapper-floor2').find('#svg-page-2-wrapper');
		// 각 층에 맞는 map 을 보여준다.
		page_outdoor_wrapper.show();
		page_1_wrapper.show();
		page_2_wrapper.show();

		// ** 야외전시관 맵  추가 설정 **
		// 야외전시관 맵의 녹색바탕에 해당하는 이름 tooltip 만 표시.
		$('.st-o-6').parent().siblings().find($('.st1, .st3')).css("fill-opacity", "1");
		// 캠프장 숙소(녹색) 은 OFF.
		$("g[id='XMLID_104_']").find('.st-o-6').css("fill-opacity", "0");
		$("g[id='XMLID_104_']").find('.st1, .st3').css("fill-opacity", "0");
		// 전시관 이외의 장소는 hover 나 active 시에도 표시되지 않도록 함. (보라색)
		$('.st-o-0, .st-o-5').css("fill-opacity", "0");	// background
		$('.st-o-0, .st-o-5').parent().siblings().find($('.st1, .st3')).css("fill-opacity", "0"); // tooltip
		// 전시관 이외의 장소는 hover 나 active 시에도 표시되지 않도록 함. (남색)
		$('.st-o-4, .st-o-7').css("fill-opacity", "0");	// background
		$('.st-o-4, .st-o-7').parent().siblings().find($('.st1, .st3')).css("fill-opacity", "0"); // tooltip
		// 남색 건물들중 야외 전시관에 해당하는 것들은 다시 ON.
		// 역사의 광장, 공룡동산&지질동산, 생태공원, 에너지 의 4개의 건물이 해당됨.
		var extra_buildings = $("g[id='XMLID_112_'], g[id='XMLID_120_'], g[id='XMLID_98_'], g[id='XMLID_109_']");
		extra_buildings.each( function() {
			// background-color
			$(this).find($('.st-o-4, .st-o-7'))
				.css("fill-opacity","0.25")
				.css("transition","fill-opacity .5s ease-in-out");
			// tooltip always on
			$(this).find($('.st1, .st3')).css("fill-opacity", "1");		
		});
		// 위의 4개 건물들에 대한 hover event
		extra_buildings.each( function() {
			$(this).hover( 
				function() {	// hover in
					$(this).find($('.st-o-4, .st-o-7')).css("fill-opacity", "0.7");
				},
				function() {	// hover out
					$(this).find($('.st-o-4, .st-o-7')).css("fill-opacity", "0.25");
				}	
			);
			
		});
		
		
		// ** 1층 전시관 맵  추가 설정 **
		// 1층 전시관의 각 전시장 tooltip 표시.
		$('.st5').parent().siblings().find($('.st1, .st3')).css("fill-opacity", "1");
		// 전시관 이외의 장소는 hover 나 active 시에도 표시되지 않도록 함.
		$('.st8').css("fill-opacity", "0"); // background color
		$('.st8').parent().siblings().find($('.st1, .st3')).css("fill-opacity", "0"); // tooltip
		// 창조홀&상상홀, 어울림홀 OFF
		$("g[id='XMLID_40_']").find('.st5').css('fill-opacity', '0');
		$("g[id='XMLID_40_']").find('.st1, .st3').css('fill-opacity', '0');
		$("g[id='XMLID_217_']").find('.st5').css('fill-opacity', '0');
		$("g[id='XMLID_217_']").find('.st1, .st3').css('fill-opacity', '0');
		
		// ** 2층 전시관 맵  추가 설정 **
		// 각 전시장 tooltip 표시.
		$('.st4').parent().siblings().find($('.st1, .st3')).css("fill-opacity", "1");
		// 2층 전시관 이외의 장소는 hover 나 active 시에도 표시되지 않도록 함.
		$('.st0').css("fill-opacity", "0"); // background color
		$('.st0').parent().siblings().find($('.st1, .st3')).css("fill-opacity", "0"); // tooltip
	});
</script>