<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-green">
	   <nav id="scrollspy-nav"
            class="navbar-default navbar-static has-scroll">
            <div class="container">
                <ul class="nav navbar-nav">
                	<li class="dropdown">
						<a class="dropdown-toggle" type="button" data-toggle="dropdown" role="button" 
							aria-haspopup="true" aria-expanded="true">
							<spring:message code="${currentCategory }" text=''/><i class="material-icons">&#xE313;</i>
						</a>
						<ul class="dropdown-menu">
							<c:forEach var="i" begin="1" end="9">
								<c:set var="main_category" value="main.nav.catetory${i }"/>
								<spring:message var="main_category_url" code="main.nav.catetory${i }.url" scope="application"/>
								<c:if test="${not empty main_category && main_category eq currentCategory}">
								 	<c:forEach var="j" begin="1" end="12">
										<spring:message var="sub_category" code="main.nav.catetory${i}.sub${j}" scope="application" text='' />
										<spring:message var="sub_category_url" code="main.nav.catetory${i}.sub${j}.url" scope="application" text='' />
										<c:if test="${not empty sub_category }">
											<li>
												<a href="<c:url value="${sub_category_url }" />" >
													<c:out value="${sub_category }" />
												</a>
											</li>
										</c:if>
									</c:forEach>
								</c:if>
							</c:forEach>
						</ul>
					</li>
                    <li><a href="#basic-info-spy" class="item"> 과학놀이터 </a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <div class="bread-crumbs">사이버과학관 / 과학놀이터</div>
                    </li>
                </ul>
            </div>
        </nav>
	</div>
	<div id="cyber-body" class="sub-body">
		<div id="basic-info-spy" class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title green">
						<div class="top-line"></div>
						과학놀이터
					</h3>
				</div>		
			</div>
			<div class="row">
				<div class="col-md-12">
					<ul class="circle-list green">
						<li>
							과학여행
						</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div id="board-wrapper" class="table-green">
						<c:url var="currentPath" value="/cyber/sciencePlay"/>
			 			
			 			<c:import url="/WEB-INF/views/boards/gallery.jsp">
			 				<c:param name="path" value="${currentPath }"/>
			 			</c:import>
		 			</div>
	 			</div>
			</div>
		</div>
	</div>
</div>