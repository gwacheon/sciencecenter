<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-green">
		<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
	</div>
	
	<div id="structure-wrapper" class="sub-body">
		<div class="narrow-sub-top guide">
			
		</div>
		
		<div class="container">
			<div id="rent-guide-spy">
				<div class="row">
					<div class="col-md-12">
						<h3 class="page-title green">
							<div class="top-line"></div>
						시설 대관 안내</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<ul class="circle-list green">
							<li class="margin-bottom20">
								대관 대상 활동
								<ul class="structure-activity">
									<li>과학기술문화 확산을 위한 교육 &middot; 학술활동 &middot; 전시회 또는 행사</li>
									<li>과학기술과 관련된 국가적인 행사</li>
									<li>청소년의 정서함양과 건전한 가치관의 형성과 관련된 행사</li>
								</ul>
							</li>
							<li>
								대관 절차
							</li>
						</ul>
					</div>
				</div>
			
				<div id="structure-process-wrapper" class="row">
					<div class="col-md-3 col-xs-6">
						<div class="structure-process">
							<div class="number">01</div>
							<div class="title">
								대관 가능 날짜 확인
							</div>
							<div class="desc">
								대관 캘린더 보기
							</div>
						</div>
					</div>
					
					<div class="col-md-3 col-xs-6">
						<div class="structure-process">
							<div class="number">02</div>
							<div class="title">
								대관 신청서 작성
							</div>
							<div class="center">
								<a href="<c:url value='/structures/applicationForm'/>" class="btn btn-green">신청서 다운로드</a>
							</div>
						</div>
					</div>
					
					<div class="col-md-3 col-xs-6">
						<div class="structure-process">
							<div class="number">03</div>
							<div class="title">
								신청서 제출
							</div>
							<div class="desc">
								(이메일, FAX, 방문)<br>
								<strong>*대관희망일 60 ~ 15일 전까지 신청 가능</strong>
							</div>
						</div>
					</div>
					
					<div class="col-md-3 col-xs-6">
						<div class="structure-process">
							<div class="number">04</div>
							<div class="title">
								심사 및 승인
							</div>
							<div class="desc">
								추후 개별 연락
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="price-guide-spy">
				<div class="row">
					<div class="col-md-12">
						<h3 class="page-title green">
							<div class="top-line"></div>
						대관시설별 대관금액 기준표</h3>	
					</div>
				</div>
			
		
				<div class="row">
					<div class="col-md-12">
						<ul id="structure-tabs" class="nav nav-tabs green">
							<li class="tab active">
								<a href="#display" class="green" data-toggle="tab"><spring:message code="structure.category.display"/></a>
							</li>
							<li class="tab">
								<a href="#camp" class="green" data-toggle="tab"><spring:message code="structure.category.camp"/></a>
							</li>
							<li class="tab">
								<a href="#classroom" class="green" data-toggle="tab"><spring:message code="structure.category.classroom"/></a>
							</li>
							<li class="tab">
								<a href="#lab" class="green" data-toggle="tab"><spring:message code="structure.category.lab"/></a>
							</li>
						</ul>
					</div>
					<!-- Tab panes -->
					<div class="tab-content">
						<div id="display" class="tab-pane active col-md-12">Test 1</div>
						<div id="camp" class="tab-pane fade col-md-12">Test 2</div>
						<div id="classroom" class="tab-pane fade col-md-12">Test 3</div>
						<div id="lab" class="tab-pane fade col-md-12">Test 4</div>
					</div>
				</div>
		
				<div class="row margin-bottom30">
					<div class="col-md-12">
					<ul class="circle-list green">
						<li class="margin-bottom20">
							소관부서
							<ul class="structure-activity">
								<li>상상홀, 창조홀, 어울림홀, 창조홀로비 : 과학문화진흥과</li>
								<li>강의실, 실험실, 캠프장 : 과학탐구교육과</li>
								<li>천체투영관 교육실 : 창조전시관리팀</li>
								<li>기타 과학관내 시설 및 부지 : 업무와 관련된 사업부서</li>
							</ul>
						</li>
						<li class="margin-bottom20">
							대관안내 및 문의
							<ul class="dashList">
								<li class="margin-bottom10">
									어울림홀(과학문화진흥과 김미란 주무관)<br/>
									전화 : 02-3677-1428 / 팩스 : 02-3677-1399 / 이메일 (nanyang99@korea.kr)
								</li>
								<li class="margin-bottom10">
									어울림홀 로비, 상상홀 및 로비, 창조홀(과학문화진흥과 윤덕호 주무관)<br/>
									전화 : 02-3677-1429 / 팩스 : 02-3677-1399 / 이메일 (nsmyun@korea.kr)
								</li>
								<li class="margin-bottom10">
									강의실, 강사토론실, 실험실, 캠프장(과학탐구교육과 김완숙 주무관)<br/>
									전화 : 02-3677-1524 / 팩스 : 02-3677-1379 / 이메일 (snsmsook@korea.kr)
								</li>
								<li class="margin-bottom10">
									천체투영관 교육실(창조전시관리팀 조재일 연구사)<br/>
									전화 : 02-3677-1448 / 팩스 : 02-3677-1399 / 이메일 (stoneye@korea.kr)
								</li>
								<li class="margin-bottom10">
									기초과학관 실험실(전시운영총괄과 김광연 주무관)<br/>
									전화 : 02-3677-1376 / 팩스 : 02-3677-1399 / 이메일 (kky226@korea.kr)
								</li>
								<li class="margin-bottom10">
									전통과학관 배움터(전시운영총괄과 오연숙 주무관)<br/>
									전화 : 02-3677-1372 / 팩스 : 02-3677-1399 / 이메일 (ysukoh@korea.kr)
								</li>
								<li class="margin-bottom10">
									자연사관 탐구교실(전시운영총괄과 정원영 연구사)<br/>
									전화 : 02-3677-1375 / 팩스 : 02-3677-1399 / 이메일 (narubua@korea.kr)
								</li>								
							</ul>
						</li>
						<li class="margin-bottom20">
							사용료부과 방법
							<ul class="structure-activity">
								<li>
									대관료는 다음 산출근거에 의하고 과학관 휴관일을 포함하여 적용한다.
									<ul>
										<li>
											시설사용료 = 국유재산법 시행령 제29조의 하한 사용료율(50/1000) × 재산가액
										</li>
										<li>
											재산가액(건물의 경우) = 건물가액 + 부지가액
										</li>
									</ul>
								</li>
								<li>
									공공요금은 과학관 전체 사용량에 대한 시설 사용량을 면적대비로 산출한다. 다만 별도 산출이 가능한 경우 그 산출량에 의하며, 대관시 별도로 반입한 장비 및 전열기구에 대해서는 추가 부과한다.
								</li>
								<li>
									대관기간에는 대관준비 및 철거기간을 포함하며 대관료 부과단위는 1일 단위로 한다. 다만 캠프장숙소의 경우에는 숙박일을 기준으로 대관일수를 산정하며, 강의실·실험실의 경우에는 4시간 미만 사용시 50%를 적용할 수 있다.
								</li>
								<li>
									총 대관일수가 15일 이내의 경우 기본료를 적용하며, 16일 이상 30일 이하인 경우 초과일수(15일)에 대해 10%를, 31일 이상 60일 이하인 경우 초과일수(30일)에 대해 20%를, 61일 이상인 경우 초과사용일수(61일)부터 30%를 할인할 수 있다.
								</li>
								<li>
									캠프장숙소의 개인·가족단위 대관 시, 신청자의 편의를 위하여 별도 대관료 납부계좌에 입금하게 하여 관리할 수 있다.
								</li>
								<li>
									환경예치금은 대관료 납부 시 별도 예치금 현금계좌에 입금한다.
								</li>
								<li>
									사용료의 경우 매년 공시지가에 따라서 변동 가능하다.
								</li>
								<li>
									계절별 기준은 다음과 같다.
									<ol>
										<li>
											겨울철 : 11, 12, 1, 2월
										</li>
										<li>
											 여름철 : 7, 8월
										</li>
										<li>
											 봄·가을철 : 3, 4, 5, 6, 9, 10월
										</li>
									</ol>
								</li>
							</ul>
						</li>
						<li class="margin-bottom20">
							어울림홀, 상상홀, 창조홀 사용시 유의사항
							<ul class="structure-activity">
								<li>
									홀, 로비 등 실내에서는 식사가 불가능하며 구내식당 이용시 과학관 입장권을 구매하셔야 합니다.(구내식당은 상설전시장 안에 위치)
								</li>
								<li>
									현수막 등 홍보물 설치는 지정된 장소만 가능하며 담당자와 협의 후 가능
								</li>
							</ul>
						</li>
					</ul>				
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="<c:url value='/js/ext/velocity.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/structure.js'/>"></script>
<script type="text/javascript">
<c:forEach var="s" items="${structures }">
	<c:url var="pictureUrl" value="${baseFileUrl }${s.picture }"/>

	if(Structures["${s.category }"] == null){
		Structures["${s.category }"] = [];
	}
	
	Structures["${s.category }"].push(
		new Structure("${s.id }", "${s.title }", "${pictureUrl }", "${s.area }", 
			"<fmt:formatNumber value="${s.priceSpring}" />", 
			"<fmt:formatNumber value="${s.priceSummer}" />", 
			"<fmt:formatNumber value="${s.priceWinter}" />", 
			"${s.person }")
	);
</c:forEach>

$(function(){
	var structureTypes = ["display", "camp", "classroom", "lab"];
	var source   = $("#structure-template").html();
	var template = Handlebars.compile(source);

	_.forEach(structureTypes, function(type, i){
		var data = {structures: Structures[type]};
		var html    = template(data);
		if(type == "display"){
			
			$("#" + type).html("<div class='col-xs-12 margin-top10 margin-bottom10'>"
					+"<a href='"+baseUrl+"structures/download/tech' class='btn btn-green'>어울림홀 기술정보 </a> "
					+"<a href='"+baseUrl+"structures/download/floor' class='btn btn-green'>어울림홀 극장도면  </a> "
					+"<a href='"+baseUrl+"structures/download/map' class='btn btn-green'>어울림홀 좌석배치도</a> "
					+"<a href='"+baseUrl+"structures/download/manual' class='btn btn-green'>어울림홀 공연장 안전매뉴얼 </a>"
					+"</div>");
			
			$("#" + type).append(html);
		}else{
			$("#" + type).html(html);
		}
	});
	
	$(document).on("click", ".materialboxed", function(e){
		var overlay = $('<div id="full-image-overlay"></div>');
		var image = $(this);
		
		$(image).addClass("full-screen-image");
		
		if($("#full-image-overlay").length == 0){
			$("body").append(overlay);
			$(overlay).append();
		}
	});
	
	$(document).on("click", "#full-image-overlay, img.full-screen-image", function(e){
		$("#full-image-overlay").remove();
		$("img.full-screen-image").removeClass("full-screen-image");
	});
});

$(document).ready(function(){
	$('ul.tabs').tabs();
});
</script>
<script id="structure-template" type="text/x-handlebars-template">
<div class="row margin-top30">

<div class="col-xs-12">
	<div class="table-responsive">
<table class="table centered-table lined-table margin-bottom30 structure-price-table">
	<thead>
		<tr>
			<th rowspan="2" class="center" width="15%">사진</th>
			<th rowspan="2" class="center">시설명</th>
			<th rowspan="2" class="center">면적(m<sup>2</sup>)</th>
			<th class="center" colspan="3">대관료 최소금액</th>
			<th rowspan="2" class="center">담당자</th>
			<th rowspan="2" class="center"></th>
		</tr>
		
		<tr>
			<th>봄ㆍ가을철</th>
			<th>여름철</th>
			<th>겨울철</th>
		</tr>
	</thead>
	
	{{#each structures}}
		<tr>
			<td class="center">
				<img alt="{{title}}" src="{{picture}}" class="img-responsive materialboxed"
					data-caption="{{title}}">
			</td>
			<td class="center">
				{{title}}
			</td>
			<td class="center">
				{{area}}
			</td>
			<td class="center">
				{{priceSpring}}
			</td>
			<td class="center">
				{{priceSummer}}
			</td>
			<td class="center">
				{{priceWinter}}
			</td>
			<td class="center">
				{{person}}
			</td>
			<td class="center">
				<a href="<c:url value='/structures/{{id}}/calendars'/>"
					class="btn btn-green">
					일정확인
				</a>
			</td>
		</tr>	
	{{/each}}
</table>
</div>
</div>
</div>
</script>