<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="sub-content-nav scrollspy-green">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>


<div id="sub-content-wrapper">
	<div id="structure-wrapper" class="sub-body">
		<div id="basic-info-spy" class="narrow-sub-top guide">
			<div class="sub-banner-tab-wrapper sub-banner-tab-green">
				<div class="container">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<h3 class="page-title green">
				<div class="top-line"></div>
				대관 예약
			</h3>
			<div class="row margin-bottom30">
				<div class="col-md-12">
					<table class="table table-purple lined-table">
						<tr>
							<th class="center">구분</th>
							<td>${structure.title }</td>
							<th class="center">면적</th>
							<td>${structure.area } (m<sup>2</sup>)</td>
							<th class="center">담당자</th>
							<td>${structure.person }</td>
						</tr>
						<tr>
							<th class="center">비고</th>
							<td colspan="5">${structure.etc }</td>
						</tr>
					</table>
				</div>
			</div>
			
			<div class="row margin-bottom30">
				<div class="col-md-7">
					<div id="structure-unavailable-calendars">
					
					</div>
					<div>
						<a href="<c:url value='/structures'/>" class="btn btn-green reverse">시설 대관 리스트</a>
						<a href="<c:url value='/structures/applicationForm'/>" class="btn btn-green right">신청서 다운로드</a>
					</div>
				</div>
				<div class="col-md-5 margin-top30">
					<ul class="circle-list green">
						<li class="margin-bottom20">
							대관 신청은 금일 기준으로 15일에서 60일 사이에만 가능합니다.
							<ul>
								<li>대관 신청이 가능한 날짜는 <span class="green">연두색</span>으로 표시 되어 있습니다.</li>
								<li>대관 신청이 완료된 이후에는 대관 사용 시간이 해당 날짜 하단에 표시됩니다.</li>	
							</ul>
						</li>
						<li class="margin-bottom20">
							원하는 날짜의 대관 가능여부를 확인 후 신청서를 작성하여 이메일, FAX 또는 방문하여 제출하시면 됩니다. 
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		var structureCalendar = new StructureCalendar({
			date: moment(new Date()),
			container: "#structure-unavailable-calendars",
			template: "#structure-calendar-template",
			url: "<c:url value='/structures/${structure.id}/calendars/dates'/>"
		});
	});	
</script>

<script id="structure-calendar-template"
	type="text/x-handlebars-template">

<div class="month-selector" style="text-align: center">
	<a id="prev-month" class="month-selector green" href="#" data-date="{{prevMonth}}">
		&lt;
	</a>

	<div id="selected-month" style="display: inline-block">
		{{formatedDate selectedDate "YYYY.MM"}}
	</div>

	<a id="next-month" href="#" class="month-selector green" data-date="{{nextMonth}}">
		&gt;
	</a>
</div>

<div id="structure-calendar" class="calendar-area">
	<table id="calendar-table" class="ui celled table unstackable">
		<thead>
			<tr>
				<th class="center">일</th>
				<th class="center">월</th>
				<th class="center">화</th>
				<th class="center">수</th>
				<th class="center">목</th>
				<th class="center">금</th>
				<th class="center">토</th>
			</tr>
		</thead>
		<tbody class="calendar-body">
			{{#each dates}}
				<tr>
					{{#each this}}
						<td class="center date {{#if this.disable}}disabled{{else}}available{{/if}}" data-date="{{formatedDate this.date "YYYY-MM-DD"}}">
							<div class="circle">
								{{formatedDate this.date "DD"}}
							</div>
							<div class="unavailable-times">
								
							</div>
						</td>
					{{/each}}
				</tr>
			{{/each}}
		</tbody>
	</table>
</div>
</script>