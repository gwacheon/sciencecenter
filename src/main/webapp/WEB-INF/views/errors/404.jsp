<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
			<div class="error-box-wrapper">
				<div class="error-box">
					<div class="title">
						<i class="fa fa-exclamation-circle" aria-hidden="true"></i> ERROR 404 - Page Not Found	
					</div>
					<div class="message">
						<div class="text">
							죄송합니다. 요청하신 페이지를 찾을 수 없습니다.<br>
							일시적인 장애로 인해 요청하신 페이지를 열 수 없으니,
							잠시 후 다시 이용해 주시기 바랍니다.<br>
							빠른 시간 내 정상적인 서비스를 제공하도록 하겠습니다. 감사합니다.
						</div>
						<div class="contact">
							홈페이지 이용관련 문의사항 :  <i class="fa fa-phone" aria-hidden="true"></i> 02-3677-1500
						</div>
					</div>
					<div class="footer">
						<div class="row">
							<div class="col-xs-6 col-sm-6 col-md-6 text-left">
								<a id="back-btn" class="link-btn">
									<i class="fa fa-angle-left" aria-hidden="true"></i> 이전 페이지
								</a>						
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 text-right">
								<a href="<c:url value='/' />" class="link-btn">
									메인 페이지 <i class="fa fa-angle-right" aria-hidden="true"></i> 
								</a>						
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$("#back-btn").on("click", function() {
		window.history.back();
	});
</script>