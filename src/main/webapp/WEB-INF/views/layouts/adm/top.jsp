<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<style>
	#admin-top-container {
		border-top: 1px solid #ddd;
		border-bottom: 1px solid #ddd;
	}
	
	#admin-top.navbar-nav > li > a {
		padding-left: 5px;
		padding-right: 5px;
	}
	#admin-top.navbar-nav > li.active,
	#admin-top.navbar-nav > li:hover {
		background-color: #26a69a;
	}
	#admin-top.navbar-nav > li:hover > a {
		background-color: #26a69a;
		color: #fff;
	}
	#admin-top.navbar-nav > li.active > a {
		color: #fff;
	}
	#admin-logout-top {
		padding: 5px 0;
		text-align: right;
		border-bottom: 1px solid #eee;
	}
	#admin-logout-top  a {
		padding: 5px 10px;
	}
	.navbar-toggle {
		border-color: #ddd;
	}
	.navbar-toggle .icon-bar {
		background-color: #3d959e;
	}
	
</style>

<header class="navbar navbar-static-top bs-docs-nav" id="top"
	role="banner">
	<div id="admin-logout-top">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<a href='<c:url value="/admin"/>'>관리자</a>				
					<a href="<c:url value="/admin/logout"/>" id="logout-btn">Logout</a>
					
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="navbar-header">
			<button class="navbar-toggle collapsed" type="button"
				data-toggle="collapse" data-target="#bs-navbar"
				aria-controls="bs-navbar" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a href='<c:url value="/"/>' class="brand-logo">
				<img src="<c:url value="/resources/img/logo.png"/>" id="logo-img"
							class="responsive-img" alt="국립과천과학관" />
			</a>
		</div>
		
	</div>
	
	<div id="admin-top-container">
		<div class="container">
			<nav id="bs-navbar" class="collapse navbar-collapse">
				<ul id="admin-top" class="nav navbar-nav">
					
						<li class="<c:if test="${currentAdminCategory eq 'mainPage' }"> active</c:if>">
							<a href='<c:url value="/admin/mainPage/banners"/>'>
								메인페이지
							</a>
						</li>
						<li>
							<a href='<c:url value="/admin/structures"/>'>
								대관
							</a>
						</li>
						<li>
							<a href='<c:url value="/admin/career"/>'>
								진로탐구
							</a>
						</li>
						<li>
							<a href='<c:url value="/admin/voluntaries"/>'>
								봉사활동
							</a>
						</li>
		 				<li>
		 					<a class="dropdown-toggle" href="#" data-toggle="dropdown">
		 						무한상상실 <i class="fa fa-caret-down"></i>
							</a>
							<ul class="dropdown-menu">
								<li><a href="<c:url value="/admin/sangsang/device"/>">장비 관리</a></li>
								<li>
									<a href="<c:url value="/admin/sangsang/building"/>">대관 관리</a>
								</li>
								<li>
									<a href="<c:url value="/admin/sangsang/schedules/manage"/>">일정 관리</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="<c:url value='/admin/orgPrograms'/>">
								단체예약
							</a>
						</li>
						<li>
							<a href='<c:url value="/admin/surveys"/>'>
								설문
							</a>
						</li>
						<li class="<c:if test="${currentAdminCategory eq 'events' }"> active</c:if>">
							<a href='<c:url value="/admin/events/list/culture"/>'>
								문화행사
							</a>
						</li>
						<li class="<c:if test="${currentAdminCategory eq 'mainEvents' }"> active</c:if>">
							<a href='<c:url value="/admin/mainEvents"/>'>
								대표문화행사
							</a>
						</li>
						<li class="<c:if test="${currentAdminCategory eq 'board' }"> active</c:if>">
							<a href='<c:url value="/admin/boards"/>'>
								게시판
							</a>
						</li>
						<li class="<c:if test="${currentAdminCategory eq 'supportEvaluation' }"> active</c:if>">
							<a href='<c:url value="/admin/supportEvaluation"/>'>
								고객서비스평가
							</a>
						</li>
						<li class="<c:if test="${currentAdminCategory eq 'policies' }"> active</c:if>">
							<a href='<c:url value="/admin/policies"/>'>
								약관/방침
							</a>
						</li>
						<li class="<c:if test="${currentAdminCategory eq 'dinnerApply' }"> active</c:if>">
							<a href='<c:url value="/admin/dinnerApply"/>'>
								석식신청
							</a>
						</li>
						<li class="<c:if test="${currentAdminCategory eq 'dinnerApply' }"> active</c:if>">
							<a href='<c:url value="/admin/membershipLog"/>'>
								유료회원입장
							</a>
						</li>
					
					<li>
	 					<a class="dropdown-toggle" href="#" data-toggle="dropdown">
	 						회원/강좌 <i class="fa fa-caret-down"></i>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a href="<c:url value="/admin/legacy/?url=/masterpage/member/&tmn=${TA_MANAGER_NO}"/>">회원 관리</a>
							</li>
							<li><a href="<c:url value="/admin/legacy/?url=/masterpage/etc/&tmn=${TA_MANAGER_NO}"/>">강좌 관리</a></li>
							<li>
								<a href="<c:url value="/admin/legacy/?url=/masterpage/code/&tmn=${TA_MANAGER_NO}"/>">코드 관리</a>
							</li>
							<li>
								<a href="<c:url value="/admin/legacy/?url=/masterpage/siteManager/&tmn=${TA_MANAGER_NO}"/>">환경 설정</a>
							</li>
						</ul>
					</li>
				</ul>
			</nav>
		</div>
	</div>
</header>

<script type="text/javascript">
	$('.dropdown-toggle').dropdown();
	$("#logout-btn").click(function() {
		$("#logout-form").submit();
	});


	$('.sub-category .sub-title')
			.click(
					function(e) {

						// grab the current clicked main-category id
						var mainCategoryId = $(this).data('main-category-id');
						// grab the current clicked sub-category id
						var subCategoryId = $(this).data('sub-category-id');

						// if there is no sub-item
						// keep the anchor event active
						var subItemSelector = $(".sub-category .sub-item-wrapper[data-main-category-id='"
								+ mainCategoryId
								+ "'][data-sub-category-id='"
								+ subCategoryId + "'] .sub-item");
						if (subItemSelector.length == "0") {
							return true;
						}
						// if there is an sub-item, DO NOT TRIGGER THE anchor event, show the sub-item links.
						else {
							e.preventDefault();
							// remove all the sub-title active class first
							$('.sub-category .sub-title').removeClass('active');
							// hide all the sub-category-wrapper first.
							$(".sub-category .sub-item-wrapper").removeClass(
									'active');

							// active the current sub-title
							$(this).addClass('active');
							// show the child sub-category-wrapper of this clicked sub-title
							$(
									".sub-category .sub-item-wrapper[data-main-category-id='"
											+ mainCategoryId
											+ "'][data-sub-category-id='"
											+ subCategoryId + "']").addClass(
									'active');

						}
					});
</script>
