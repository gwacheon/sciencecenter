<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<c:url var="rootUrl" value="/"/>

<div class="container">
	<div id="family-sites">
	</div>
</div>

<div id="footer">
	<div class="footer-bottom">
		<div class="container">
			<div class="row">
				<div class="col-md-2 hidden-xs hidden-sm">
					<img src="<c:url value="/resources/img/footer_logo_kr.png"/>" 
						class="img-responsive footer-logo-img margin-top20"/>
				</div>
				<div class="col-sm-12 col-md-10">
					<div class="row footer-links">
						<div class="col-xs-12 col-md-8 guide">
							<a href="<c:url value='/communication/policies' />">
								이용약관
							</a>
							<a href="<c:url value='/communication/policies?type=privacy' />">
								개인정보 취급방침
							</a>
							<a href="/gnsm_web/?sub_num=602">
								이메일 무단수신거부
							</a>
							<a href="/gnsm_web/popup/email/email.jsp" target="_blank">
									운영자에게
							</a>
						</div>
						<div class="col-xs-12 col-md-4 item">
							<div class="dropup">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									관련 사이트 이동
									<i class="fa fa-angle-up"></i>
								</a>
								<ul class="dropdown-menu">
									<li>
										<a href="http://www.president.go.kr" target="_blank">청와대</a>
									</li>
									<li>
										<a href="http://www.msip.go.kr" target="_blank">미래창조과학부</a>
									</li>
									<li>
										<a href="http://www.science.go.kr" target="_blank">국립중앙과학관</a>
									</li>
									<li>
										<a href="http://www.ssm.go.kr/v2/kor/index.asp" target="_blank">국립서울과학관</a>
									</li>
									<li>
										<a href="http://www.nssc.go.kr" target="_blank">원자력안전위원회</a>
									</li>
									<li>
										<a href="http://www.copyright.or.kr" target="_blank">한국저작권위원회</a>
									</li>
									<li>
										<a href="http://www.ibs.re.kr" target="_blank">기초과학연구원</a>
									</li>
									<li>
										<a href="http://www.isbb.or.kr" target="_blank">국제과학비지니스벨트</a>
									</li>
									<li>
										<a href="http://www.ntis.go.kr" target="_blank">국가과학기술지식정보서비스</a>
									</li>
									<li>
										<a href="http://www.now.go.kr" target="_blank">과학기술정책정보서비스</a>
									</li>
									<li>
										<a href="http://www.korea.kr/newsWeb/index.jsp" target="_blank">공감코리아</a>
									</li>
								</ul>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="address">
								<!-- 13817 경기도 과천시 상하별로 110 국립과천과학관<br>
								<strong style="margin-right: 10px">대표전화</strong> 02-2677-1500 <strong style="margin-right: 10px;margin-left: 30px">Fax</strong> 02-3677-1328 -->
								<strong style="margin-right: 10px">관람시간</strong>  오전 9:30 ~ 오후 5:30 (<b>입장마감</b> 오후 4:30)  <b>휴관일</b> 1월 1일, 매주 월요일<br> 
								<strong style="margin-right: 10px">주소</strong>  13817 경기도 과천시 상하벌로 110 국립과천과학관 <br>
								<strong style="margin-right: 10px">대표전화</strong>  02-3677-1500   <strong style="margin-right: 10px; margin-left: 10px;">Fax</strong>  02-3677-1328
							</div>
						</div>

						<div class="col-md-6">
							<div class="row">
								<div class="col-xs-4 col-md-4">
									<div class="footer-link-images">
										<img src="<c:url value="/resources/img/main/footer-link-image1.png" />"
											alt="" class="visible-lg"/>
										<div class="footer-link-texts">
											<a href="http://www.sstm.org.cn/kjg_web/html/kjg_korea/portal/index/index.htm" target="_blank">
												<div class="sub-title">상해과학 기술관</div>
												<div class="sub-text">해외과학 기술관</div>
											</a>
										</div>
									</div>
								</div>
								<div class="col-xs-4 col-md-4">
									<div class="footer-link-images">
										<img src="<c:url value="/resources/img/main/footer-link-image2.png" />"
											alt="" class="visible-lg"/>
										<div class="footer-link-texts">
											<a href="http://60.247.10.155:8007/portal/findportal.do?portalid=orga2e3e19908bfa" target="_blank">
												<div class="sub-title">중국과학 기술관</div>
												<div class="sub-text">해외협력 과학관</div>
											</a>
										</div>
									</div>
								</div>
								<div class="col-xs-4 col-md-4">
									<div class="footer-link-images">
										<img src="<c:url value="/resources/img/main/footer-link-image3.png" />"
											alt="" class="visible-lg"/>
										<div class="footer-link-texts">
											<a href="http://www.miraikan.jst.go.jp/ko/" target="_blank">
												<div class="sub-title">일본과학 미래관</div>
												<div class="sub-text">해외협력 과학관</div>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>




<script type="text/javascript" src="<c:url value='/js/responsibility.js'/>"></script>
<script id="responsibilityTemplate" type="text/x-handlebars-template">
	
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="sub-info-wrapper">
						<div class="sub-info">
							<div class="title">
								<i class="fa fa-desktop"></i> 담당부서 
							</div>
							<div class="content">
								{{dept}}
							</div>
						</div>
						<div class="sub-info">
							<div class="title">
								<i class="fa fa-user"></i> 담당자 
							</div>
							<div class="content">
								{{member}}
							</div>
						</div>
						<div class="sub-info">
							<div class="title">
								<i class="fa fa-phone"></i> 내선번호
							</div>
							<div class="content">
								{{phone}}
							</div>					
						</div>   
					</div>
				</div>	
			</div>
		</div>
	
</script>

<script type="text/javascript">
	var responsibilities = new Responsibilities();
	var currentResponsibility;
	
	$(function(){
		var tabName = $('ul.tabs.page-tabs > .tab > a:first').attr('href') || "";
		
		<c:forEach var="responsibility" items="${responsibilities }">
			responsibilities.add(new Responsibility(
				"${responsibility.requestURI}",
				"${responsibility.dept}",
				"${responsibility.member}",
				"${responsibility.phone}",
				"${responsibility.tabName}"
			));
		</c:forEach>
		
		if(!responsibilities.isEmpty()){
			//화면탭이 존재할 경우
			if($('ul.tabs.page-tabs > .tab > a').length > 0){
				tabName = $('ul.tabs.page-tabs > .tab > a:first').attr('href');
				
				currentResponsibility = responsibilities.getByTabName(tabName);
				
				if(currentResponsibility != null){
					currentResponsibility.render();
				}
			}else{
				currentResponsibility = responsibilities.get(0);
				currentResponsibility.render();
			}
		}
		
		<c:if test="${not empty TA_MANAGER_NO }">
			if(currentResponsibility != null){
				currentResponsibility.fillForm();
			}
			
			$("#submit-responsibility-btn").click(function(e){
				e.preventDefault();
				
				if(currentResponsibility == null){
					currentResponsibility = new Responsibility(
						$('#requestURI').val(),
						$('#dept').val(),
						$('#member').val(),
						$('#phone').val(),
						$('#tabName').val()
					);	
				} else{
					currentResponsibility.requestURI = $('#requestURI').val();
					currentResponsibility.tabName = $('#tabName').val();
					currentResponsibility.dept = $('#dept').val();
					currentResponsibility.member = $('#member').val();
					currentResponsibility.phone = $('#phone').val();
				}
				
				currentResponsibility.submitData();
			});
		</c:if>
		
		$('ul.tabs.page-tabs > .tab > a').click(function(e){
			e.preventDefault();
			var tabName = $(this).attr('href');
			
			currentResponsibility = responsibilities.getByTabName(tabName);
			
			if(currentResponsibility != null){
				currentResponsibility.render();
				
				<c:if test="${not empty TA_MANAGER_NO }">
					currentResponsibility.fillForm();
				</c:if>
			}else {
				$("#responsibility-wrapper").html('');
			}
			
			if($("#tabName").length == 1){
				$("#tabName").val(tabName);	
			}
		});
	});
</script>

<script id="familySitesTemplate" type="text/x-handlebars-template">

	<div id="family-slides-window">
		<div id="family-slides-wrapper">
			{{#each familyLinkSlides}}
				<div class="slide">
					<a target="_blank" href="{{linkUrl}}">
						<img src="{{imageUrl}}"/>
					</a>
				</div>			
			{{/each}}
		</div>	
	</div>
	<div class="slide-btn prev" data-direction="left"><i class="fa fa-angle-left"></i></div>
	<div class="slide-btn next" data-direction="right"><i class="fa fa-angle-right"></i></div>		

</script>


<script type="text/javascript">
	var imageBaseUrl = "<c:url value="/resources/img/main_a/familysite/" />"; 
	var familyLinkSlides = [
		{
			imageUrl: imageBaseUrl + "familysite_01.png",
			linkUrl: "http://www.president.go.kr"
		},
		{
			imageUrl: imageBaseUrl + "familysite_02.png",
			linkUrl: "http://www.msip.go.kr"
		},
		{
			imageUrl: imageBaseUrl + "familysite_03.png",
			linkUrl: "http://www.science.go.kr"
		},
		{
			imageUrl: imageBaseUrl + "familysite_04.png",
			linkUrl: "http://www.ssm.go.kr/"
		},
		{
			imageUrl: imageBaseUrl + "familysite_05.png",
			linkUrl: "http://www.nssc.go.kr/"
		},
		{
			imageUrl: imageBaseUrl + "familysite_06.png",
			linkUrl: "http://www.copyright.or.kr/"
		},
		{
			imageUrl: imageBaseUrl + "familysite_07.png",
			linkUrl: "http://www.ibs.re.kr/"
		},
		{
			imageUrl: imageBaseUrl + "familysite_08.png",
			linkUrl: "http://www.isbb.or.kr/"
		},
		{
			imageUrl: imageBaseUrl + "familysite_09.png",
			linkUrl: "http://www.ntis.go.kr/"
		},
		{
			imageUrl: imageBaseUrl + "familysite_10.png",
			linkUrl: "http://www.now.go.kr/"
		},
		{
			imageUrl: imageBaseUrl + "familysite_11.png",
			linkUrl: "http://www.korea.kr/"
		}
	];
	
	var familySitesSource = $("#familySitesTemplate").html();
	var familySitesTemplate = Handlebars.compile(familySitesSource);
	var familySitesHtml = familySitesTemplate({familyLinkSlides: familyLinkSlides});
	$("#family-sites").html(familySitesHtml);
	
</script>

<script type="text/javascript">
	var slides = $('#family-sites .slide');
	var slidesCount = slides.length;
	var slidesPerScreen = 4;
	
	if( $(window).width() >768 && $(window).width() <= 992 ) {
		slidesPerScreen = 3;
	}
	else if( $(window).width() <= 768 ) {
		slidesPerScreen = 2;
	}
	$("#family-slides-window").width( ($('#family-sites').width() - 80) + "px");
	slideWidth = ($("#family-slides-window").width()) / slidesPerScreen;
	slides.width(slideWidth + "px");
	$('#family-slides-wrapper').css('width', slideWidth * slidesCount + "px");
	
	var beginIdx = 0;
	var endIdx = beginIdx + slidesPerScreen - 1;
	
	function moveFamilyLinkSlide() {
		$('#family-slides-wrapper').animate({
			marginLeft: (-1 * slideWidth * beginIdx) + "px"
		});
	}
	
	$('.slide-btn').on("click", function(e) {
		e.preventDefault();
			
		var direction = $(this).data("direction");
		
		if( direction == "left" ) {
			if( beginIdx == 0 ) {
				beginIdx = slidesCount - slidesPerScreen;
			}	
			else {
				beginIdx = beginIdx - 1;
			}
		}
		else {
			if( beginIdx + slidesPerScreen - 1 == slidesCount - 1 ) {
				beginIdx = 0;
			}
			else {
				beginIdx = beginIdx + 1;
			}
		}
		moveFamilyLinkSlide();
	});
	
	$(window).resize( function() {
		if( $(window).width() > 992 ) {
			slidesPerScreen = 4;
			slideWidth = "50%";
		}
		else if( $(window).width() >768 && $(window).width() <= 992 ) {
			slidesPerScreen = 3;
			slideWidth = "25%";
		}
		else if( $(window).width() <= 768 ) {
			slidesPerScreen = 2;
			slideWidth = "50%";
		}	
		
		$("#family-slides-window").width( ($('#family-sites').width() - 80) + "px");
		slideWidth = ($("#family-slides-window").width()) / slidesPerScreen;
		slides.width(slideWidth + "px");
		$('#family-slides-wrapper').css('width', slideWidth * slidesCount + "px");
	});
	
	window.setInterval( function() {
		if( beginIdx + slidesPerScreen - 1 == slidesCount - 1 ) {
			beginIdx = 0;
		}
		else {
			beginIdx = beginIdx + 1;
		}
		moveFamilyLinkSlide();
	}, 3000);
	
</script>

