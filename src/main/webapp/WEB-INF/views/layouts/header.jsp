<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<sec:csrfMetaTags />

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<title>국립과천과학관</title>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<link rel="shortcut icon" href="<c:url value='/img/favicon.ico'/>"> 

<!-- CSS -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/external.css'/>">
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/ptech.css'/>">
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/application.css'/>">
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/reservation.css'/>">
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/user.css'/>">


<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/picker.css'/>">
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/picker.date.css'/>">
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/picker.time.css'/>">

<script type="text/javascript" src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/webfont/1.6.26/webfontloader.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.15.35/js/bootstrap-datetimepicker.min.js"></script>

<!--[if lt IE 9]>
  <script type="text/javascript" src="<c:url value='/resources/js/ext/es5-shim.js'/>"></script>
  <script type="text/javascript" src="<c:url value='/resources/js/ext/es5-sham.js'/>"></script>
<![endif]-->

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/3.10.1/lodash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/picker.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/picker.date.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/picker.time.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/legacy.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/3.0.3/handlebars.min.js"></script>
<script type="text/javascript" src="<c:url value='/resources/js/common.js'/>"></script>
<script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>

<script type="text/javascript" src="<c:url value='/resources/js/ptech/ptech-calendar.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/program.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/ptech/ptech-slider.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/structureUnavailable.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/ext/Chart.min.js'/>"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="<c:url value='/resources/js/ext/html5shiv.js'/>"></script>
  <script src="<c:url value='/resources/js/ext/respond.js'/>"></script>
<![endif]-->

<script>
 
</script>

<!-- base URL for script -->
<spring:eval expression="@propertyConfigurer.getProperty('webroot.relativePath')" var="fileRelativePath" />
<spring:eval expression="@propertyConfigurer.getProperty('legacy.companycd')" var="companycd" />
<c:set var="baseFileUrl" value="/${fileRelativePath}${companycd}/" scope='application'/>
<script type="text/javascript">
	var baseUrl = "<c:url value="/"/>";
	var baseFileUrl = "<c:url value="${fileRelativePath}${companycd}/"/>";
</script>
