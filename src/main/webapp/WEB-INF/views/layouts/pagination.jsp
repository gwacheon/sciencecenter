<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="pagination-wrapper">
	<ul class="pagination ${page.className}">
		<c:if test="${page.beginPage > 1}">
			<li>
				<a href="<c:url value='${page.path}page=1'/>" class="page-link ${page.className}" data-page="1">
					<!-- &laquo; 첫페이지 -->
					1
				</a>
			</li>
			<li>
				<a href="<c:url value='${page.path}page=${page.beginPage-1}'/>" 
					class="page-link ${page.className}" data-page="<c:out value='${page.beginPage-1}'/>">
					<i class="fa fa-angle-left"></i>
				</a>
			</li>	
		</c:if>
		
		<c:forEach begin="${page.beginPage}" end="${page.endPage}" var="i">
			<c:choose>
				<c:when test="${page.currentPage eq i}">
					<li class="active">
						<a href="#">${i} <span class="sr-only"></span></a>
					</li>
				</c:when>
				<c:otherwise>
					<li>
						<a href="<c:url value='${page.path}page=${i}'/>"
                  			class="page-link ${page.className}" data-page="<c:out value='${i}'/>">${i}</a>
					</li>
				</c:otherwise>
			</c:choose>
		</c:forEach>
           
		<c:if test="${page.endPage < page.totalPages}">
			<li>
				<a href="<c:url value='${page.path}page=${page.endPage+1}'/>"
					class="page-link ${page.className}" data-page="<c:out value='${page.endPage+1}'/>">
					<i class="fa fa-angle-right"></i>
				</a>
			</li>
			<li>
				<a href="<c:url value='${page.path}page=${page.totalPages}'/>"
					class="page-link ${page.className}" data-page="<c:out value='${page.totalPages}'/>">
					<!-- 마지막페이지 &raquo; -->
					<c:out value="${page.totalPages }" />
				</a>
			</li>
		</c:if>
	</ul>
</div>

<script id="paginationTemplate" class="${page.className }" type="text/x-handlebars-template">
		{{#compare beginPage 1 operator=">"}}
			<li>
				<a href="#" data-page="1" class="page-link {{className}}">&laquo; <spring:message code="page.first"/></a>
			</li>
			<li>
				<a href="#" data-page={{beforePage beginPage}} class="page-link {{className}}">...</a>
			</li>	
		{{/compare}}
		
		{{#for beginPage endPage 1}}
			{{#compare this ../currentPage operator="=="}}
				<li class="active">
					<a href="#" data-page="{{this}}" class="page-link {{../../className}}">{{this}}</a>
				</li>
			{{else}}
				<li>
					<a href="#" data-page="{{this}}" class="page-link {{../../className}}">{{this}}</a>
				</li>
			{{/compare}}
		{{/for}}
		
		{{#compare endPage totalPages operator="<"}}
			<li>
				<a href="#" data-page="{{nextPage endPage}}" class="page-link {{className}}">...</a>
			</li>
			<li>
				<a href="#" data-page={{totalPages}} class="page-link {{className}}"><spring:message code="page.last"/> &raquo;</a>
			</li>
		{{/compare}}
</script>