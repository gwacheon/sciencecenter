<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE HTML>
<html>
	<head>
		<t:insertAttribute name="header"/>
	</head>
	
	<body>
		<t:insertAttribute name="top"/>
		
		<div id="content-body">
			<t:insertAttribute name="content"/>
		</div>
		
		<t:insertAttribute name="footer"/>
		
		<div id="ajaxLoader">
			<img src="<c:url value='/img/ajax-loader.gif'/>" alt="ajax-loader"/>
		</div>
		
		<script type="text/javascript">
			function ajaxLoading(){
				$("body").css("opacity", "0.5");
				$("#ajaxLoader").show();
			}
			
			function ajaxUnLoading(){
				$("body").css("opacity", "1");
				$("#ajaxLoader").hide();
			}
		</script>
		
		<script type="text/javascript">

		    $(function(){
		    	$('body').scrollspy({ 
					target: '#scrollspy-nav',
					offset: 230
				});
		    	
		    	$("#scrollspy-nav.has-scroll a.item").click(function(e){
		    		(e.preventDefault) ? e.preventDefault() : (e.returnValue = false);
					
					var target = this.hash;
				    var $target = $(target);
				    
				    console.log(target);
				    console.log($target);
				    
					$('html, body').stop().animate({
				        'scrollTop': $target.offset().top - 150
				    }, 500, 'swing', function () {
				        //window.location.hash = target;
				    });
					
				});
		    	
		    	
				$('.dropdown-button').dropdown({
				    inDuration: 300,
				    outDuration: 225,
				    constrain_width: false, // Does not change width of dropdown to that of the activator
				    gutter: 0, // Spacing from edge
				    belowOrigin: false // Displays dropdown below the button
				  }
				);
				
				// scrollspyName 파라메터와 함께 현재페이지가 load 될 때,
				// 해당 scroll spy div 로 스크롤한다.
				var scrollspyName = "<c:out value='${scrollspyName}' />";
				if( scrollspyName != null ) {
					// scroll a little bit to the top
					var spySelector = "#" + scrollspyName;
					scrollspyToElement($(spySelector));
				}
		    });
		    
		    function scrollspyToElement(ele) {
		    	if( ele.length ) {
				    $(window).scrollTop(ele.offset().top - 280);		    		
		    	}
			}
		</script>
	</body>
</html>