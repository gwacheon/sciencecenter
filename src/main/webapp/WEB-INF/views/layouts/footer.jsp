<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<c:url var="rootUrl" value="/"/>
<c:if test="${rootUrl !=  requestScope['javax.servlet.forward.request_uri']}">
	<div class="sub-footer">
		<div id="responsibility-wrapper">
			
		</div>
	</div>
</c:if>

<c:if test="${not empty TA_MANAGER_NO }">
	<div class="container">
		<div class="responsibility-form">			
			<c:url var = "action" value='/admin/responsibilities'/>
			
			<form id="responsibility-form" action="${action }" method="POST" class="form-inline">
				<input type="hidden" id="requestURI" name="requestURI" value="${responsibilityRequestURI }"/>
				<input type="hidden" id="tabName" name="tabName" value=""/>

				<div class="form-group">
					<label for="dept" class="control-label">담당부서</label>
					<input id="dept" type="text" name="dept" class="validate form-control"
						value="">
				</div>
				<div class="form-group">
					<label for="member" class="control-label">담당자</label>
					<input id="member" type="text" name="member" class="validate form-control"
						value=""> 
				</div>
				<div class="form-group">
					<label for="phone" class="control-label">내선번호</label>
					<input id="phone" type="text" name="phone" class="validate form-control"
						value=""> 
				</div>
				<div class="form-group">
					<button id="submit-responsibility-btn" type="button" class="btn btn-primary">저장</button>
				</div>				
			</form>
		</div>
	</div>
</c:if>

<!-- <div id="family-sites-wrapper">
	<div class="container">
		<div id="family-sites">
		</div>
	</div>
</div> -->
<div class="container hidden-xs" style="margin-bottom: 20px;margin-top:20px;">
	<div class="row">
		<div class="col-md-2 col-sm-3">
			<a target="_blank" href="http://www.president.go.kr">
				<img src="<c:url value="/resources/img/main_c/family_sites/familysite_01.png"/>"   class="img-responsive"/>
			</a>
		</div>
		<div class="col-md-2 col-sm-3">
			<a target="_blank" href="http://www.msip.go.kr">
				<img src="<c:url value="/resources/img/main_c/family_sites/familysite_02.png"/>"   class="img-responsive"/>
			</a>
		</div>
		<div class="col-md-2 col-sm-3">
			<a target="_blank" href="http://www.science.go.kr">
				<img src="<c:url value="/resources/img/main_c/family_sites/familysite_03.png"/>"   class="img-responsive"/>
			</a>
		</div>
		<div class="col-md-2 col-sm-3">
			<a target="_blank" href="http://www.ssm.go.kr/">
				<img src="<c:url value="/resources/img/main_c/family_sites/familysite_04.png"/>"   class="img-responsive"/>
			</a>
		</div>
		<div class="col-md-2 col-sm-3">
			<a target="_blank" href="http://www.nssc.go.kr/">
				<img src="<c:url value="/resources/img/main_c/family_sites/familysite_05.png"/>"   class="img-responsive"/>
			</a>
		</div>
		<div class="col-md-2 col-sm-3">
			<a target="_blank" href="http://www.scienlove.or.kr/">
				<img src="<c:url value="/resources/img/main_c/family_sites/familysite_06.png"/>"   class="img-responsive"/>
			</a>
		</div>
	</div>
</div>

<!--  bottom-quick-link -->
<!--  메인페이지에서만 과학교육 카테고리 메뉴 노출 -->
<c:if test="${not empty mainEducationMenuFlag && mainEducationMenuFlag eq true }">
<div id="bottom-quick-link">
	<div  class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="sub-menus">
					<%-- <div class="row">
						<c:set var="categoryNo" value="3"/>
						<c:forEach var="i" begin="1" end="12">
							<spring:message var="sub_category" code="main.nav.catetory${categoryNo}.sub${i}" scope="application" text='' />
							
							<c:if test="${not empty sub_category }">
								<spring:message var="sub_category_url" code="main.nav.catetory${categoryNo}.sub${i}.url" scope="application" text='' />
								<div class="col-xs-4 col-sm-2 sub-menu-item">
									<a href="<c:url value="${sub_category_url }" />" >
										<c:out value="${sub_category }" />
									</a>
									<spring:message var="sub_child_category" code="main.nav.catetory${categoryNo}.sub${i }.sub1" scope="application" text='' />
									<c:if test="${sub_child_category != '' }">
										<ul class="child-menus">
											<c:forEach var="j" begin="1" end="8">
												<spring:message var="sub_child_category" code="main.nav.catetory${categoryNo}.sub${i}.sub${j}" scope="application" text='' />
												<c:if test="${sub_child_category != '' }">
													<spring:message var="sub_child_category_url" code="main.nav.catetory${categoryNo}.sub${i}.sub${j}.url" scope="application" text='' />
													<li>
														<a href="<c:url value="${sub_child_category_url }" />">
															${sub_child_category } 
														</a>
													</li>
												</c:if>
											</c:forEach>	
										</ul>
									</c:if>
								</div>
							</c:if>
						</c:forEach>
					</div> --%>
					<div class="row">
						<div class="col-xs-4 col-sm-2 sub-menu-item">
							<a href="<c:url value="" />" >
								관람안내
							</a>
							
							<ul class="child-menus">
								<li>
									<a href="<c:url value="/guide/totalGuide" />">
										이용안내 
									</a>
								</li>
								<li>
									<a href="<c:url value="/guide/fundMember" />">
										회원제안내
									</a>
								</li>
								<li>
									<a href="<c:url value="/guide/convenience" />">
										편의시설 
									</a>
								</li>
								<li>
									<a href="<c:url value="/guide/food" />">
										식음시설 
									</a>
								</li>
								<li>
									<a href="<c:url value="/introduce/location/#parking-spy" />">
										주차안내 
									</a>
								</li>
							</ul>
						</div>
						<div class="col-xs-4 col-sm-2 sub-menu-item">
							<a href="<c:url value="" />" >
								전시관
							</a>
							
							<ul class="child-menus">
								<li>
									<a href="<c:url value="/display/mainBuilding/basicScience" />">
										상설전시관 
									</a>
								</li>
								<li>
									<a href="<c:url value="/display/frontier/hallOfFame" />">
										프론티어창작관 
									</a>
								</li>
								<li>
									<a href="<c:url value="/display/planetarium" />">
										천문우주관
									</a>
								</li>
								<li>
									<a href="<c:url value="/display/outdoorEcological/insectarium" />">
										야외전시관 
									</a>
								</li>
							</ul>
						</div>
						<div class="col-xs-4 col-sm-2 sub-menu-item">
							<a href="<c:url value="" />" >
								과학교육
							</a>
							
							<ul class="child-menus">
								<li>
									<a href="<c:url value="/education/experienceWeekends" />">
										창의체험교실
									</a>
								</li>
								<li>
									<a href="<c:url value="/education/course" />">
										자유학기제
									</a>
								</li>
								<li>
									<a href="<c:url value="/events/list/camp" />">
										과학캠프
									</a>
								</li>
								<li>
									<a href="<c:url value="/education/youthScience" />">
										청춘과학대학 
									</a>
								</li>
								<li>
									<a href="<c:url value="/introduce/notice/2960" />">
										학부모연수
									</a>
								</li>
							</ul>
						</div>
						<div class="col-xs-4 col-sm-2 sub-menu-item">
							<a href="<c:url value="" />" >
								행사특별전시
							</a>
							<ul class="child-menus">
								<li>
									<a href="<c:url value="/events/" />">
										행사안내 
									</a>
								</li>
								<li>
									<a href="#">
										과학문화행사 
									</a>
								</li>
								<li>
									<a href="<c:url value="/events/list/play" />">
										문화공연 
									</a>
								</li>
							</ul>
						</div>
						<div class="col-xs-4 col-sm-2 sub-menu-item">
							<a href="<c:url value="" />" >
								고객소통
							</a>
							
							<ul class="child-menus">
								<li>
									<a href="<c:url value="/communication/charter" />">
										고객서비스 
									</a>
								</li>
								<li>
									<a href="<c:url value="/communication/evalResult" />">
										평가결과 
									</a>
								</li>
								<li>
									<a href="<c:url value="/communication/safety#evacuation-spy" />">
										관람객대피
									</a>
								</li>
							</ul>
						</div>
						<div class="col-xs-4 col-sm-2 sub-menu-item">
							<a href="<c:url value="" />" >
								과학관소개
							</a>
							
							<ul class="child-menus">
								<li>
									<a href="<c:url value="/introduce/history" />">
										기관연혁 
									</a>
								</li>
								<li>
									<a href="<c:url value="/introduce/location" />">
										찾아오시는길 
									</a>
								</li>
								<li>
									<a href="<c:url value="/introduce/location/#parking-spy" />">
										주차안내
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				
			</div>
		</div>
	</div>
</div>
</c:if>
				<%-- <div class="link-item first first-row">
					<a href="<c:url value='/events/list/event' />">
						문화행사
					</a>
				</div>
				<div class="link-item first-row">
					<a href="#">
						SF
					</a>
				</div>
				<div class="link-item first-row">
					<a href="<c:url value='/display/frontier/infiniteImagination' />">
						무한상상실
					</a>
				</div>
				<div class="visible-xs">
					<div class="link-item first">
						<a href="<c:url value='/education/experienceWeekends' />">
							창의체험
						</a>
					</div>
				</div>
				<div class="link-item hidden-xs">
					<a href="<c:url value='/education/experienceWeekends' />">
						창의체험
					</a>
				</div>
				<div class="link-item">
					<a href="<c:url value='/education/course' />">
						자유학기제
					</a>
				</div>
				<div class="link-item">
					<a href="<c:url value='/voluntary' />">
						자원봉사
					</a>
				</div> --%>



<div id="footer">
	<div class="footer-bottom">
		<div class="container">
			<div class="row">
				<div class="col-md-2 hidden-xs hidden-sm">
					<img src="<c:url value="/resources/img/footer_logo_kr.png"/>" 
						class="img-responsive footer-logo-img margin-top20"/>
				</div>
				<div class="col-sm-12 col-md-10">
					<div class="row footer-links">
						<div class="col-xs-12 col-md-8 guide">
							<div class="row">
								<a href="<c:url value='/communication/policies' />" class="link-item">
									이용약관
								</a>
								<a href="<c:url value='/communication/policies?type=privacy' />" class="link-item">
									개인정보 취급방침
								</a>
								<a href="#" data-toggle="modal" data-target="#emailPolicyModal" class="link-item">
									이메일 무단수신거부
								</a>
								<!-- <a href="#" data-toggle="modal" data-target="#contactUsEmailModal" class="link-item">
									운영자에게
								</a>		 -->						
							</div> 
						</div>
						<div class="col-xs-12 col-md-4 item ">
							<div class="dropup">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									관련 사이트 이동
									<i class="fa fa-angle-up"></i>
								</a>
								<ul class="dropdown-menu">
									<li>
										<a href="http://www.president.go.kr" target="_blank">청와대</a>
									</li>
									<li>
										<a href="http://www.msip.go.kr" target="_blank">미래창조과학부</a>
									</li>
									<li>
										<a href="http://www.science.go.kr" target="_blank">국립중앙과학관</a>
									</li>
									<li>
										<a href="http://www.ssm.go.kr/v2/kor/index.asp" target="_blank">국립서울과학관</a>
									</li>
									<li>
										<a href="http://www.nssc.go.kr" target="_blank">원자력안전위원회</a>
									</li>
									<li>
										<a href="http://www.copyright.or.kr" target="_blank">한국저작권위원회</a>
									</li>
									<li>
										<a href="http://www.ibs.re.kr" target="_blank">기초과학연구원</a>
									</li>
									<li>
										<a href="http://www.isbb.or.kr" target="_blank">국제과학비지니스벨트</a>
									</li>
									<li>
										<a href="http://www.ntis.go.kr" target="_blank">국가과학기술지식정보서비스</a>
									</li>
									<li>
										<a href="http://www.now.go.kr" target="_blank">과학기술정책정보서비스</a>
									</li>
									<li>
										<a href="http://www.korea.kr/newsWeb/index.jsp" target="_blank">공감코리아</a>
									</li>
								</ul>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="address">
								<!-- 13817 경기도 과천시 상하별로 110 국립과천과학관<br>
								<strong style="margin-right: 10px">대표전화</strong> 02-2677-1500 <strong style="margin-right: 10px;margin-left: 30px">Fax</strong> 02-3677-1328 -->
								<strong style="margin-right: 10px">관람시간</strong>  오전 9:30 ~ 오후 5:30 (<b>입장마감</b> 오후 4:30)  <b>휴관일</b> 1월 1일, 매주 월요일<br> 
								<strong style="margin-right: 10px">주소</strong>  13817 경기도 과천시 상하벌로 110 국립과천과학관 <br>
								<strong style="margin-right: 10px">대표전화</strong>  02-3677-1500   <strong style="margin-right: 10px; margin-left: 10px;">Fax</strong>  02-3677-1328
							</div>
						</div>

						<div class="col-md-6">
							<div class="row">
								<div class="col-xs-4 col-md-4">
									<div class="footer-link-images">
										<img src="<c:url value="/resources/img/main/footer-link-image1.png" />"
											alt="" class="visible-lg"/>
										<div class="footer-link-texts">
											<a href="http://www.sstm.org.cn/kjg_web/html/kjg_korea/portal/index/index.htm" target="_blank">
												<div class="sub-title">상해과학 기술관</div>
												<div class="sub-text">해외과학 기술관</div>
											</a>
										</div>
									</div>
								</div>
								<div class="col-xs-4 col-md-4">
									<div class="footer-link-images">
										<img src="<c:url value="/resources/img/main/footer-link-image2.png" />"
											alt="" class="visible-lg"/>
										<div class="footer-link-texts">
											<a href="http://60.247.10.155:8007/portal/findportal.do?portalid=orga2e3e19908bfa" target="_blank">
												<div class="sub-title">중국과학 기술관</div>
												<div class="sub-text">해외협력 과학관</div>
											</a>
										</div>
									</div>
								</div>
								<div class="col-xs-4 col-md-4">
									<div class="footer-link-images">
										<img src="<c:url value="/resources/img/main/footer-link-image3.png" />"
											alt="" class="visible-lg"/>
										<div class="footer-link-texts">
											<a href="http://www.miraikan.jst.go.jp/ko/" target="_blank">
												<div class="sub-title">일본과학 미래관</div>
												<div class="sub-text">해외협력 과학관</div>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- 이메일 무단수집거부 Modal -->
<div id="emailPolicyModal" class="modal fade" tabindex="-1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">
					이메일 무단수집 거부
				</h4>
			</div>
			<div id="support-body" class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="charter-wrapper">
							국립과천과학관 홈페이지에 게시된 <b>이메일주소</b>가 전자우편수집프로그램이나
							그밖의 기술적장치를이용하여 무단으로 수집되는 것을 거부하며,
							이를 위반시 <b>개인정보 보호법에 의해 형사 처벌됨을 유념</b>하시기 바랍니다.
						</div>											
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<h3 class="page-title teal">
							<div class="top-line"></div>
							개인정보 보호법
						</h3>
						<ul class="circle-list teal item-margin-bottom text-bold">
							<li>
								제15조(개인정보의 수집ㆍ이용)
								<ul class="non-bullet item-margin-bottom">
									<li>
										① 개인정보처리자는 다음 각 호의 어느 하나에 해당하는 경우에는 개인정보를 수집할 수 있으며 그 수집 목적의 범위에서 이용할 수 있다.
										<ul>
											<li>
												1. 정보주체의 동의를 받은 경우
											</li>
											<li>
												2. 법률에 특별한 규정이 있거나 법령상 의무를 준수하기 위하여 불가피한 경우
											</li>
											<li>
												3. 공공기관이 법령 등에서 정하는 소관 업무의 수행을 위하여 불가피한 경우
											</li>
											<li>
												4. 정보주체와의 계약의 체결 및 이행을 위하여 불가피하게 필요한 경우
											</li>
											<li>
												5. 정보주체 또는 그 법정대리인이 의사표시를 할 수 없는 상태에 있거나 주소불명 등으로 사전 동의를 받을 수 없는 경우로서 명백히 정보주체 또는 제3자의 급박한 생명, 신체, 재산의 이익을 위하여 필요하다고 인정되는 경우
											</li>
											<li>
												6. 개인정보처리자의 정당한 이익을 달성하기 위하여 필요한 경우로서 명백하게 정보주체의 권리보다 우선하는 경우. 이 경우 개인정보처리자의 정당한 이익과 상당한 관련이 있고 합리적인 범위를 초과하지 아니하는 경우에 한한다.
											</li>
										</ul>
									</li>
									<li>
										② 개인정보처리자는 제1항제1호에 따른 동의를 받을 때에는 다음 각 호의 사항을 정보주체에게 알려야 한다. 다음 각 호의 어느 하나의 사항을 변경하는 경우에도 이를 알리고 동의를 받아야 한다.
										<ul>
											<li>
												1. 개인정보의 수집·이용 목적
											</li>
											<li>
												2. 수집하려는 개인정보의 항목
											</li>
											<li>
												3. 개인정보의 보유 및 이용 기간
											</li>
											<li>
												4. 동의를 거부할 권리가 있다는 사실 및 동의 거부에 따른 불이익이 있는 경우에는 그 불이익의 내용
											</li>
										</ul>
									</li>
								</ul>
							</li>
							<li>
								제16조(개인정보의 수집 제한)
								<ul class="non-bullet item-margin-bottom">
									<li>
										① 개인정보처리자는 제15조제1항 각 호의 어느 하나에 해당하여 개인정보를 수집하는 경우에는 그 목적에 필요한 최소한의 개인정보를 수집하여야 한다. 이 경우 최소한의 개인정보 수집이라는 입증책임은 개인정보처리자가 부담한다.
									</li>
									<li>
										② 개인정보처리자는 정보주체의 동의를 받아 개인정보를 수집하는 경우 필요한 최소한의 정보 외의 개인정보 수집에는 동의하지 아니할 수 있다는 사실을 구체적으로 알리고 개인정보를 수집하여야 한다. <신설 2013.8.6.>
									</li>
									<li>
										③ 개인정보처리자는 정보주체가 필요한 최소한의 정보 외의 개인정보 수집에 동의하지 아니한다는 이유로 정보주체에게 재화 또는 서비스의 제공을 거부하여서는 아니 된다. <개정 2013.8.6.>
									</li>
								</ul>
							</li>
							<li>
								제17조(개인정보의 제공)
								<ul class="non-bullet item-margin-bottom">
									<li>
										① 개인정보처리자는 다음 각 호의 어느 하나에 해당되는 경우에는 정보주체의 개인정보를 제3자에게 제공(공유를 포함한다. 이하 같다)할 수 있다.
										<ul>
											<li>
												1. 정보주체의 동의를 받은 경우
											</li>
											<li>
												2. 제15조제1항제2호·제3호 및 제5호에 따라 개인정보를 수집한 목적 범위에서 개인정보를 제공하는 경우
											</li>
										</ul>
									</li>
									<li>
										② 개인정보처리자는 제1항제1호에 따른 동의를 받을 때에는 다음 각 호의 사항을 정보주체에게 알려야 한다. 다음 각 호의 어느 하나의 사항을 변경하는 경우에도 이를 알리고 동의를 받아야 한다.
										<ul>
											<li>
												1. 개인정보를 제공받는 자
											</li>
											<li>
												2. 개인정보를 제공받는 자의 개인정보 이용 목적
											</li>
											<li>
												3. 제공하는 개인정보의 항목
											</li>
											<li>
												4. 개인정보를 제공받는 자의 개인정보 보유 및 이용 기간
											</li>
											<li>
												5. 동의를 거부할 권리가 있다는 사실 및 동의 거부에 따른 불이익이 있는 경우에는 그 불이익의 내용
											</li>
										</ul>
									</li>
									<li>
										③ 개인정보처리자가 개인정보를 국외의 제3자에게 제공할 때에는 제2항 각 호에 따른 사항을 정보주체에게 알리고 동의를 받아야 하며, 이 법을 위반하는 내용으로 개인정보의 국외 이전에 관한 계약을 체결하여서는 아니 된다.
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div><!--  modal-body END ... -->
			<div class="modal-footer">
				<button id="modalCloseBtn" data-dismiss="modal" class="btn btn-primary">
					닫기
				</button>
			</div>
		</div>
	</div>
</div>


<!-- 운영자 이메일 Modal -->
<div id="contactUsEmailModal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">이메일 문의</h4>
			</div>
			<c:url var = "action" value="/contactUsEmail"/>
			<form id="contactUsEmailForm" name="contactUsEmailForm" action="${action }" method="POST" >
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-8">
							<div class="form-group">
								<label for="name">이름</label>
								<input id="name" type="text" class="form-control contact-email-value" name="name" data-name="이름"/>
							</div>	
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label for="email">이메일</label>
								<input type="email" id="email" class="form-control contact-email-value" name="email" data-name="이메일"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label for="title">제목</label>
								<input type="text" id="title" class="form-control contact-email-value" name="title" data-name="제목"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label for="content">내용</label>
								<textarea class="form-control contact-email-value" rows="5" id="content" name="content" data-name="내용"></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-teal reverse" data-dismiss="modal">취소</button>
					<button id="submitContactUsEmail" type="button" class="btn btn-teal">전송</button>
				</div>
			</form>
		</div>
	</div>
</div>


<script type="text/javascript" src="<c:url value='/js/responsibility.js'/>"></script>
<script id="responsibilityTemplate" type="text/x-handlebars-template">
	
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="sub-info-wrapper">
						<div class="sub-info">
							<div class="title">
								<i class="fa fa-desktop"></i> 담당부서 
							</div>
							<div class="content">
								{{dept}}
							</div>
						</div>
						<div class="sub-info">
							<div class="title">
								<i class="fa fa-user"></i> 담당자 
							</div>
							<div class="content">
								{{member}}
							</div>
						</div>
						<div class="sub-info">
							<div class="title">
								<i class="fa fa-phone"></i> 내선번호
							</div>
							<div class="content">
								{{phone}}
							</div>					
						</div>   
					</div>
				</div>	
			</div>
		</div>
	
</script>

<script type="text/javascript">
	var responsibilities = new Responsibilities();
	var currentResponsibility;
	
	$(function(){
		var tabName = $('ul.tabs.page-tabs > .tab > a:first').attr('href') || "";
		
		<c:forEach var="responsibility" items="${responsibilities }">
			responsibilities.add(new Responsibility(
				"${responsibility.requestURI}",
				"${responsibility.dept}",
				"${responsibility.member}",
				"${responsibility.phone}",
				"${responsibility.tabName}"
			));
		</c:forEach>
		
		if(!responsibilities.isEmpty()){
			//화면탭이 존재할 경우
			if($('ul.tabs.page-tabs > .tab > a').length > 0){
				tabName = $('ul.tabs.page-tabs > .tab > a:first').attr('href');
				
				currentResponsibility = responsibilities.getByTabName(tabName);
				
				if(currentResponsibility != null){
					currentResponsibility.render();
				}
			}else{
				currentResponsibility = responsibilities.get(0);
				currentResponsibility.render();
			}
		}
		
		<c:if test="${not empty TA_MANAGER_NO }">
			if(currentResponsibility != null){
				currentResponsibility.fillForm();
			}
			
			$("#submit-responsibility-btn").click(function(e){
				e.preventDefault();
				
				if(currentResponsibility == null){
					currentResponsibility = new Responsibility(
						$('#requestURI').val(),
						$('#dept').val(),
						$('#member').val(),
						$('#phone').val(),
						$('#tabName').val()
					);	
				} else{
					currentResponsibility.requestURI = $('#requestURI').val();
					currentResponsibility.tabName = $('#tabName').val();
					currentResponsibility.dept = $('#dept').val();
					currentResponsibility.member = $('#member').val();
					currentResponsibility.phone = $('#phone').val();
				}
				
				currentResponsibility.submitData();
			});
		</c:if>
		
		$('ul.tabs.page-tabs > .tab > a').click(function(e){
			e.preventDefault();
			var tabName = $(this).attr('href');
			
			currentResponsibility = responsibilities.getByTabName(tabName);
			
			if(currentResponsibility != null){
				currentResponsibility.render();
				
				<c:if test="${not empty TA_MANAGER_NO }">
					currentResponsibility.fillForm();
				</c:if>
			}else {
				$("#responsibility-wrapper").html('');
			}
			
			if($("#tabName").length == 1){
				$("#tabName").val(tabName);	
			}
		});
	});
</script>

<script id="familySitesTemplate" type="text/x-handlebars-template">

	<div id="family-slides-window">
		<div id="family-slides-wrapper">
			{{#each familyLinkSlides}}
				<div class="slide">
					<a target="_blank" href="{{linkUrl}}">
						<img src="{{imageUrl}}"/>
					</a>
				</div>			
			{{/each}}
		</div>	
	</div>
	<div class="slide-btn prev" data-direction="left"><i class="fa fa-angle-left"></i></div>
	<div class="slide-btn next" data-direction="right"><i class="fa fa-angle-right"></i></div>		

</script>


<script type="text/javascript">
	var imageBaseUrl = "<c:url value="/resources/img/main_a/familysite/" />"; 
	var familyLinkSlides = [
		{
			imageUrl: imageBaseUrl + "familysite_01.png",
			linkUrl: "http://www.president.go.kr"
		},
		{
			imageUrl: imageBaseUrl + "familysite_02.png",
			linkUrl: "http://www.msip.go.kr"
		},
		{
			imageUrl: imageBaseUrl + "familysite_03.png",
			linkUrl: "http://www.science.go.kr"
		},
		{
			imageUrl: imageBaseUrl + "familysite_04.png",
			linkUrl: "http://www.ssm.go.kr/"
		},
		{
			imageUrl: imageBaseUrl + "familysite_05.png",
			linkUrl: "http://www.nssc.go.kr/"
		},
		{
			imageUrl: imageBaseUrl + "familysite_06.png",
			linkUrl: "http://www.copyright.or.kr/"
		},
		{
			imageUrl: imageBaseUrl + "familysite_07.png",
			linkUrl: "http://www.ibs.re.kr/"
		},
		{
			imageUrl: imageBaseUrl + "familysite_08.png",
			linkUrl: "http://www.isbb.or.kr/"
		},
		{
			imageUrl: imageBaseUrl + "familysite_09.png",
			linkUrl: "http://www.ntis.go.kr/"
		},
		{
			imageUrl: imageBaseUrl + "familysite_10.png",
			linkUrl: "http://www.now.go.kr/"
		},
		{
			imageUrl: imageBaseUrl + "familysite_11.png",
			linkUrl: "http://www.korea.kr/"
		}
	];
	
	var familySitesSource = $("#familySitesTemplate").html();
	var familySitesTemplate = Handlebars.compile(familySitesSource);
	var familySitesHtml = familySitesTemplate({familyLinkSlides: familyLinkSlides});
	$("#family-sites").html(familySitesHtml);
	
</script>

<script type="text/javascript">
	var slides = $('#family-sites .slide');
	var slidesCount = slides.length;
	var slidesPerScreen = 4;
	
	if( $(window).width() >768 && $(window).width() <= 992 ) {
		slidesPerScreen = 3;
	}
	else if( $(window).width() <= 768 ) {
		slidesPerScreen = 2;
	}
	$("#family-slides-window").width( ($('#family-sites').width() - 80) + "px");
	slideWidth = ($("#family-slides-window").width()) / slidesPerScreen;
	slides.width(slideWidth + "px");
	$('#family-slides-wrapper').css('width', slideWidth * slidesCount + "px");
	
	var beginIdx = 0;
	var endIdx = beginIdx + slidesPerScreen - 1;
	
	function moveFamilyLinkSlide() {
		$('#family-slides-wrapper').animate({
			marginLeft: (-1 * slideWidth * beginIdx) + "px"
		});
	}
	
	$('.slide-btn').on("click", function(e) {
		e.preventDefault();
			
		var direction = $(this).data("direction");
		
		if( direction == "left" ) {
			if( beginIdx == 0 ) {
				beginIdx = slidesCount - slidesPerScreen;
			}	
			else {
				beginIdx = beginIdx - 1;
			}
		}
		else {
			if( beginIdx + slidesPerScreen - 1 == slidesCount - 1 ) {
				beginIdx = 0;
			}
			else {
				beginIdx = beginIdx + 1;
			}
		}
		moveFamilyLinkSlide();
	});
	
	$(window).resize( function() {
		if( $(window).width() > 992 ) {
			slidesPerScreen = 4;
			slideWidth = "50%";
		}
		else if( $(window).width() >768 && $(window).width() <= 992 ) {
			slidesPerScreen = 3;
			slideWidth = "25%";
		}
		else if( $(window).width() <= 768 ) {
			slidesPerScreen = 2;
			slideWidth = "50%";
		}	
		
		$("#family-slides-window").width( ($('#family-sites').width() - 80) + "px");
		slideWidth = ($("#family-slides-window").width()) / slidesPerScreen;
		slides.width(slideWidth + "px");
		$('#family-slides-wrapper').css('width', slideWidth * slidesCount + "px");
	});
	
	window.setInterval( function() {
		if( beginIdx + slidesPerScreen - 1 == slidesCount - 1 ) {
			beginIdx = 0;
		}
		else {
			beginIdx = beginIdx + 1;
		}
		moveFamilyLinkSlide();
	}, 3000);
	
</script>

<script type="text/javascript">
	$('#submitContactUsEmail').click( function() {
		var isEmpty = false;
		$('.contact-email-value').each( function() {
			var inputValue = $(this).val();
			if( inputValue == null || inputValue == "") {
				
				isEmpty = true;
				alert($(this).data("name") + "을 입력해주세요.");
				$(this).focus();
				return false;
			}
		});
		if( isEmpty ) {
			return;
		}
		else {
			var contactUsEmail = {
	    		"name" : $('#contactUsEmailForm #name').val(),
	    		"email" : $('#contactUsEmailForm #email').val(),
	    		"title" : $('#contactUsEmailForm #title').val(),
	    		"content" : $('#contactUsEmailForm #content').val()
	    	};
		    	
			var token = $("meta[name='_csrf']").attr("content");
			var header = $("meta[name='_csrf_header']").attr("content");
			
			$.ajax({
				url: baseUrl + "/contactUsEmail",
				type: "POST",
				contentType : "application/json; charset=utf-8",
				dataType : "json",
				data: JSON.stringify({contactUsEmail: contactUsEmail}),
				beforeSend : function(xhr) {
					$('#contactUsEmailModal').modal('hide');
					ajaxLoading();
					xhr.setRequestHeader(header, token);
				},
				success: function(data){
					ajaxUnLoading();
					if(!data.error) {
						alert(data.message);						
					}
				},
				error: function(err){
					console.log(err);
				}
			});
		}
	});

</script>

<script type="text/javascript">
	$( function()  {
		var subChild = "<spring:message code="${crumb.subCategory }.sub1" scope="application" text='' />";
		if( subChild == null || subChild == "") {
			$('.sub-body').addClass('no-sub-child');
		}
	});
</script>