<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE HTML>
<html>
	<head>
		<t:insertAttribute name="header"/>
	</head>
	
	<body>
		<div id="">
			<t:insertAttribute name="content"/>
		</div>

		<div id="ajaxLoader">
			<img src="<c:url value='/img/ajax-loader.gif'/>" alt="ajax-loader"/>
		</div>
		
		<script type="text/javascript">
			function ajaxLoading(){
				$("body").css("opacity", "0.5");
				$("#ajaxLoader").show();
			}
			
			function ajaxUnLoading(){
				$("body").css("opacity", "1");
				$("#ajaxLoader").hide();
			}
		</script>
		
		<script type="text/javascript">

		    $(function(){
		    	$('body').scrollspy({ 
					target: '#scrollspy-nav',
					offset: 230
				});
		    	
		    	$("#scrollspy-nav.has-scroll a.item").click(function(e){
					e.preventDefault();
					
					var target = this.hash;
				    var $target = $(target);
				    
					$('html, body').stop().animate({
				        'scrollTop': $target.offset().top - 150
				    }, 500, 'swing', function () {
				        window.location.hash = target;
				    });
				});
		    	
		    	
				$('.dropdown-button').dropdown({
				    inDuration: 300,
				    outDuration: 225,
				    constrain_width: false, // Does not change width of dropdown to that of the activator
				    gutter: 0, // Spacing from edge
				    belowOrigin: false // Displays dropdown below the button
				  }
				);
		    });
		</script>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-66853433-1', 'auto');
		  ga('send', 'pageview');
		</script>
	</body>
</html>