
<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<div id="nav-top" class="">
	<div id="main-top-wrapper" class="navbar-fixed-top">
	 	<nav id="main-top" class="navbar navbar-default">
			<div id="main-top-menu">
				<div class="container">
					<div class="row">
						<div class="hidden-xs col-sm-5 col-md-4">
							<div class="basic-text">
								미래를 상상하며 행복을 주는 "과학체험관"
							</div>
							
							<!--  외부 링크 임시 제거. -->
							<%-- <a href="<c:url value='/display/frontier/kidsMakerStudio' />" class="quick-link first">
								메이커랜드
							</a>
							<a href="http://cyber.scientorium.go.kr/museum_renewal/index.jsp" class="quick-link" target="_blank">
								사이버과학관
							</a>
							<a href="http://www.scienlove.or.kr/" class="quick-link" target="_blank">
								(사)과학사랑희망키움
							</a> --%>
						</div>
						<div class="col-sm-7 col-md-6 text-right">
							<c:choose>
								<c:when test="${not empty currentMemberNo }">
									<c:url value="/users/logout" var="logoutUrl" />
									<form id="logout-form" action="${logoutUrl}" method="post" style="display: none">
									  <input type="submit" value="Log out" />
									  <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
									</form>
									<a href="<c:url value='/mypage'/>">
										<spring:message code="mypage" />
									</a>
									<a href="#" id="logout-btn"><spring:message code="logout" /></a>
								</c:when>
								<c:otherwise>
									<a href="<c:url value="/users/login"/>"> 
										<spring:message code="login" />
									</a>
									<a href="<c:url value="/users/signupIntro"/>">
										회원가입
									</a>
								</c:otherwise>
							</c:choose>
							
							<a href="<c:url value='/introduce/siteMap'/>">
								<spring:message code="sitemap" />
							</a>
							<%-- <div id="language-dropdown" class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" 
									aria-haspopup="true" aria-expanded="false">
									Language
									<span class="caret"></span>
								</a>
								<ul class="dropdown-menu">
									
									<li><a href="<c:url value="/?lang=kr"/>">한국어</a></li>
									<li><a href="<c:url value="/?lang=en"/>">English</a></li>
									<li><a href="#!">日本語</a></li>
									<li><a href="#!">中文 (简体字)</a></li>
								</ul>							
							</div> --%>
							<span class="search hidden-xs">
								<input id="totalSearchText" type="text"  placeholder="검색어를 입력하세요" >
								<i class="fa fa-search"></i>
							</span>
							<c:url var="totalSearchUrl" value="/totalSearch"/>
							<form id="totalSearchForm" action="${totalSearchUrl }" method="GET">
								<%-- <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/> --%>
								<input id="searchKey" name="searchKey" type="hidden" />
							</form>
							
						</div>
						<div class="col-md-2 text-right hidden-xs hidden-sm">
							<div class="sns-link">
								<a href="http://nsm2010.blog.me/" target="_blank">
									<img src="<c:url value='/img/main/naverBlog_logo.png' />" class="img-responsive" alt="Naver Blog"/>
								</a>
								<a href="https://twitter.com/scientorium?lang=ko" target="_blank">
									<img src="<c:url value='/img/main/twitter_logo.png' />" class="img-responsive" alt="Twitter"/>
								</a>
								<a href="https://www.facebook.com/scientorium/" target="_blank">
									<img src="<c:url value='/img/main/facebook_logo.png' />" class="img-responsive" alt="Facebook"/>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div> <!--  main-top-menu END... -->
			 <div class="nav-wrapper">
			 	<div class="container">
					<!-- <a href="#" data-activates="mobile-demo" class="button-collapse"><i
					class="mdi-navigation-menu"></i></a> -->
					<!-- Brand and toggle get grouped for better mobile display -->
				    <div class="navbar-header">
				    	<!-- <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mobile-side-navbar" aria-expanded="false"> -->
						<button type="button" class="navbar-toggle collapsed">
						  <span class="sr-only">Toggle navigation</span>
						  <span class="icon-bar"></span>
						  <span class="icon-bar"></span>
						  <span class="icon-bar"></span>
						</button>
						<a id="main-logo" class="navbar-brand" href="<c:url value='/'/>" >
							<img src="<c:url value="/resources/img/main_a/logo_kr.png"/>" 
									class="img-responsive" alt="국립과천과학관" />
						</a>
				    </div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="navbar-desktop">
				   	<!-- <ul class="right hide-on-med-and-down"> -->
					   	<ul class="nav navbar-nav navbar-right">
							<c:forEach var="i" begin="1" end="8">
								<spring:message var="main_category" code="main.nav.catetory${i }" scope="application"/>
								<spring:message var="currentNav" code="${crumb.mainCategory }" text=''/>
								<c:if test="${not empty main_category}">
									<li data-seq="${i}" class="main-nav-item <c:choose><c:when test="${ i == 1 }">first </c:when><c:when test="${ i == 8 }">last</c:when></c:choose> <c:if test="${currentNav eq main_category }">active</c:if>">
										<a href="#" data-seq="${i}">
											<c:out value="${main_category }"/>
										</a>
										<div class="main-sub-nav-wrapper">
											<div class="container">
									 			<div class="row">
									 				<div class="col-md-9 col-md-offset-3">
														<div class="main-sub-nav">
										 					<div class="row">
										 						<div class="col-sm-2">
										 							<div class="main-category-name">
											 							<c:out value="${main_category }"/>
										 							</div>
										 						</div>
										 						<div class="col-sm-10">
												 					<div class="row main-nav-wrapper first">
																	    <c:forEach var="j" begin="1" end="12">
																	        <spring:message var="sub_category" code="main.nav.catetory${i}.sub${j}" scope="application" text='' />
																	        <spring:message var="sub_category_url" code="main.nav.catetory${i}.sub${j}.url" scope="application" text='' />
																	        <c:if test="${not empty sub_category}">
																	            <div class="col-sm-3 sub-category <c:if test="${j == 1 }" >first</c:if>">
																	                <c:choose>
																	                    <c:when test="${sub_category eq '#' }" >
																	                        <a>&nbsp;</a>
																	                    </c:when>
																	                    <c:when test="${sub_category_url eq 'scheduleScript' }">
																	                        <spring:message var="academy_cd" code="main.nav.catetory${i}.sub${j}.academy_cd" scope="application" text='' />
																	                        <!-- 예약신청의 javascript를 통해 페이지를 들어가야할 경우 판단 -->
																	                        <a href="javascript:void(0)" onclick="AcademyChoiceFromTopMenu('${academy_cd}')" class="sub-title">
																	                            <c:out value="${sub_category }"/>
																	                        </a>
																	                    </c:when>
																	                    <c:otherwise>
																	                        <a href="<c:url value='${sub_category_url }' />" class="sub-title" data-sub-category-id="${j }">
																	                            <c:out value="${sub_category }"/>
																	                        </a>
																	                    </c:otherwise>
																	                </c:choose>
																	        
																	                <!-- sub category 가 '대표/기타 문화행사일 경우' -->
																	                <!--문화/행사 중 '골드버그대회'등 main events dynamic menu -->
																	                <!-- 그렇지 않을 경우에는 message_kr 에서 가져온다.  -->
																	                <div class="sub-item-wrapper" data-sub-category-id="${j }">
																	              
																	                    <c:if test="${ i == 4 && j == 2 }">
																	                        <c:forEach items="${navMainEvents }" var="mainEvent">
																	                        	<c:choose>
																		                        	<c:when test="${mainEvent.subType eq 'link' }">
																		                                <a href="<c:url value='${mainEvent.linkUrl }' />" class="sub-item" target="_blank">
																		                                    <c:out value ="${mainEvent.name }"/>
																		                                </a>
																		                        	</c:when>
																		                        	<c:otherwise>
																		                        		<a href="<c:url value="/mainEvents/${ mainEvent.id }" />" class="sub-item">
																		                                    <c:out value ="${mainEvent.name }"/>
																		                                </a>
																		                        	</c:otherwise>
																	                        	</c:choose>
																	                        </c:forEach>
																	                    </c:if>

																	                    <c:forEach var="k" begin="1" end="9">
																	                        <spring:message var="sub_item" code="main.nav.catetory${i}.sub${j}.sub${k}" scope="application" text='' />
																	                        <spring:message var="sub_item_url" code="main.nav.catetory${i}.sub${j}.sub${k}.url" scope="application" text='' />
																	                        <c:if test="${not empty sub_item && sub_item ne 'dynamicEvents'}">
																	                            <c:choose>
																	                                <c:when test="${sub_item_url eq 'scheduleScript' }">
																	                                    <spring:message var="academy_cd" code="main.nav.catetory${i}.sub${j}.sub${k}.academy_cd" scope="application" text='' />
																	                                    <!-- 예약신청의 javascript를 통해 페이지를 들어가야할 경우 판단 -->
																	                                    <a href="javascript:void(0)" onclick="AcademyChoiceFromTopMenu('${academy_cd}')" class="sub-item">
																	                                        <c:out value="${sub_item }"/>
																	                                    </a>
																	                                </c:when>
																	                                <c:otherwise>
																	                                    <a href="<c:url value="${sub_item_url }"/>" class="sub-item" >
																	                                        <c:out value="${sub_item }"/>
																	                                    </a>
																	                                </c:otherwise>
																	                            </c:choose>
																	                        </c:if>
																	                    </c:forEach>
																	                </div>
																	            </div>
																	                                    
																	            <!--  6번째 Sub Category 가 끝나면, 강제로 row 를 닫고 새로 열어준다. -->
																	            <c:if test="${j==4 }">
																	                </div> <!--  row END ... -->
																	                <div class="row main-nav-wrapper second">
																	            </c:if>
																	        </c:if>
																	    </c:forEach>
																	</div>					
										 						</div>
										 					</div>
										 				</div>
										 			</div>
											 	</div>
											</div>
										</div>
									</li>
								</c:if>
							</c:forEach>
						</ul>
					</div> <!--  collapse navbar-collapse END... -->
				</div> <!--  container END ... -->
			 </div> <!-- nav-wrapper END ... -->
			 
			 <!--  mobile collapse navbar -->
			<div id="mobile-side-navbar" class="visible-xs">
		   		<!-- <ul class="right hide-on-med-and-down"> -->
			   	<ul class="nav navbar-nav mobile">
					<c:forEach var="i" begin="1" end="8">
						<spring:message var="main_category" code="main.nav.catetory${i }" scope="application"/>
						<spring:message var="main_category_url" code="main.nav.catetory${i }.url" scope="application"/>
						<spring:message var="currentNav" code="${currentCategory }" text=''/>
						<c:if test="${not empty main_category}">
							<c:choose>
								<c:when test="${ i == 1 }">
									<li class="main-nav-item first <c:if test="${currentNav eq main_category }">active</c:if>">
								</c:when>
								<c:when test="${ i == 9 }">
									<li class="main-nav-item last <c:if test="${currentNav eq main_category }">active</c:if>">
								</c:when>
								<c:otherwise>
									<li class="main-nav-item <c:if test="${currentNav eq main_category}">active</c:if>">
								</c:otherwise>
							</c:choose>
							
								<a href="<c:url value='${main_category_url }' />" data-seq="${i }">
									<c:out value="${main_category }"/>
								</a>
								<div class="main-sub-nav" data-seq="${i }">
							 		
						 			<div class="main-nav-wrapper">
						 				<c:forEach var="j" begin="1" end="12">
						 					<spring:message var="sub_category" code="main.nav.catetory${i}.sub${j}" scope="application" text='' />
											<spring:message var="sub_category_url" code="main.nav.catetory${i}.sub${j}.url" scope="application" text='' />
						 					<c:if test="${not empty sub_category}">
							 					<div class="sub-category" data-main-category-id="${i }" data-sub-category-id="${j }">
							 						
							 						<c:choose>
								 						<c:when test="${sub_category eq '#' }" >
								 							<a>&nbsp;</a>
								 						</c:when>
								 						<c:when test="${sub_category_url eq 'scheduleScript' }">
								 							<spring:message var="academy_cd" code="main.nav.catetory${i}.sub${j}.academy_cd" scope="application" text='' />
								 							<!-- 예약신청의 javascript를 통해 페이지를 들어가야할 경우 판단 -->
								 							<a href="javascript:void(0)" onclick="AcademyChoiceFromTopMenu('${academy_cd}')" class="sub-title">
										 						<c:out value="${sub_category }"/>
										 					</a>
								 						</c:when>
								 						<c:otherwise>
										 					<a href="<c:url value='${sub_category_url }' />" class="sub-title">
										 						<c:out value="${sub_category }"/>
										 					</a>
								 						</c:otherwise>
							 						</c:choose>
							 					</div>
					 							<!-- sub category 가 '대표문화행사일 경우' -->
												<!--문화/행사 중 '골드버그대회'등 main events dynamic menu -->
												<!-- 그렇지 않을 경우에는 message_kr 에서 가져온다.  -->
												<spring:message var="sub_item" code="main.nav.catetory${i}.sub${j}.sub1" scope="application" text='' />
												<c:if test="${sub_item != '' }">
													<div class="sub-item-wrapper" data-main-category-id="${i }" data-sub-category-id="${j }">
														<c:if test="${ i == 4 && j == 2 }">
															<c:forEach items="${navMainEvents }" var="mainEvent">
								 								<c:choose>
										                        	<c:when test="${mainEvent.subType eq 'link' }">
										                                <a href="<c:url value='${mainEvent.linkUrl }' />" class="sub-item" target="_blank">
										                                    <c:out value ="${mainEvent.name }"/>
										                                </a>
										                        	</c:when>
										                        	<c:otherwise>
										                        		<a href="<c:url value="/mainEvents/${ mainEvent.id }" />" class="sub-item">
										                                    <c:out value ="${mainEvent.name }"/>
										                                </a>
										                        	</c:otherwise>
									                        	</c:choose>
															</c:forEach>
														</c:if>
														
									 					<c:forEach var="k" begin="1" end="9">
									 						<spring:message var="sub_item" code="main.nav.catetory${i}.sub${j}.sub${k}" scope="application" text='' />
															<spring:message var="sub_item_url" code="main.nav.catetory${i}.sub${j}.sub${k}.url" scope="application" text='' />
															<c:if test="${not empty sub_item && sub_item ne 'dynamicEvents' }">
																<a href="<c:url value="${sub_item_url }"/>" class="sub-item" >
																	<c:out value="${sub_item }"/>
									 							</a>	
															</c:if>
									 					</c:forEach>
											 				
														
													</div>
												</c:if>
							 				</c:if>
						 				</c:forEach>
						 			</div><!--  main-nav-wrapper END ... -->
							 	</div><!--  main-sub-nav END ... -->
							</li>
						</c:if>
					</c:forEach>
				</ul>
			</div><!-- mobile-side-navbar END... -->
			 
		</nav>	<!--  main-top END ... -->
	</div>
</div>

<!-- 예약/신청의 메뉴 이동을 위한 form tag -->
<form name="topMenuScheduleForm">
	<input type="hidden" name="ACADEMY_CD" value="${AcademyCd}" />
	<input type="hidden" name="COURSE_CD" value="" />
	<input type="hidden" name="SEMESTER_CD" value="" />
	<input type="hidden" name="TYPE" value="" />
	<input type="hidden" name="FLAG" value="" />
	<input type="hidden" name="CLASS_CD" value="${ClassCd}" />
</form>

<script type="text/javascript">
	$('.dropdown-toggle').dropdown();
	$("#logout-btn").click(function(){
		$("#logout-form").submit();
	});
	
	function AcademyChoiceFromTopMenu(Cd){
		var frm = document.topMenuScheduleForm;
		frm.ACADEMY_CD.value = Cd;
		frm.CLASS_CD.value = "";
		frm.action = '<c:url value="/schedules"/>';
		frm.method = 'get';
		frm.submit();
	}
	
	// desktop navbar click event
	$("#navbar-desktop .main-nav-item > a").click(function(e){
		e.preventDefault();
		
		// grab the data-seq
		var id = $(this).data('seq');
		
		// make selectors for convenience
		var itemSelector = $("#navbar-desktop .main-nav-item[data-seq=" + id + "]");
		
		// if the cliked item is already opened,
		if( itemSelector.hasClass("opened") ) {
			itemSelector.removeClass("opened");
			$('#navbar-desktop-overlay').remove();
			$('html body').css('overflow', 'scroll');
		}
		else {
			// deactivate all first
			$("#navbar-desktop .main-nav-item").removeClass("opened");			
			itemSelector.addClass("opened");
			$('html body').css('overflow', 'hidden');
			$('html body').append('<div id="navbar-desktop-overlay"></div>');
			$desktopOverlay = $('#navbar-desktop-overlay');
			$desktopOverlay.css('z-index', '1002').css('display','block').css('opacity', '0.8');
		}
		// desktop overlay click event only when the navbar-desktop item is clicked.
		$('#navbar-desktop-overlay').on('click', function() {
			
			if( itemSelector.hasClass("opened") ) {
				itemSelector.removeClass("opened");
				$('#navbar-desktop-overlay').remove();
				$('html body').css('overflow', 'scroll');
			}
		});
		

	});
	
	
	$(".mobile .main-nav-item > a").click(function(e){
		e.preventDefault();
		if( $(this).parent().hasClass('active') ) {
			$(this).parent().removeClass('active');
			$(this).parent().find('.main-sub-nav').removeClass("active");			
			$(this).parent().find('.main-sub-nav').find('.sub-category').removeClass("active");
			$(this).parent().find('.main-sub-nav').find('.sub-item-wrapper').removeClass("active");
		}
		else {
			var seq = $(this).data('seq');
			$(".main-nav-item").removeClass('active');
			$('.main-sub-nav').removeClass('active');
			$('.sub-category').removeClass('active');
			$('.sub-item-wrapper').removeClass('active');
			$(this).parent().addClass('active');
			$(this).parent().find('.main-sub-nav').addClass('active');
			
		}
	});
	
	$('.navbar-toggle').click( function(e) {
		if( $('#mobile-side-navbar').hasClass('active') ) {
			$('#mobile-side-navbar').removeClass('active');
			$('#mobile-nav-background-overlay').remove();
			$('html body').css('overflow','scroll');
		}
		else {
			$('#mobile-side-navbar').addClass('active');
			// make the background page blur
			$('html body').css('overflow','hidden');
			$('html body').append('<div id="mobile-nav-background-overlay"></div>');
			$overlay = $('#mobile-nav-background-overlay'); 
			$overlay.css('z-index','1002').css('display','block').css('opacity','0.8');
		}
		
		// side nav closing action while nav is opened.
		$('#mobile-nav-background-overlay').on('click', function() {
			if( $('#mobile-side-navbar').hasClass('active') ) {
				$('#mobile-nav-background-overlay').remove();
				$('html body').css('overflow','scroll');		
				$('#mobile-side-navbar').removeClass('active');
			}
		});
		
	});
	
	
	
	$('.sub-category').click( function(e) {
		// grab the current clicked main-category id
		var mainCategoryId = $(this).data('main-category-id');
		// grab the current clicked sub-category id
		var subCategoryId = $(this).data('sub-category-id');
		
		// if there is no sub-item
		// keep the anchor event active
		var subItemSelector = $(".sub-item-wrapper[data-main-category-id='" + mainCategoryId + "'][data-sub-category-id='" + subCategoryId + "'] .sub-item");
		if( subItemSelector.length == "0" ) {
			return true;	
		}
		// if there is an sub-item, DO NOT TRIGGER THE anchor event, show the sub-item links.
		else {
			e.preventDefault();
			if( $(this).hasClass('active') ) {
				$(this).removeClass('active');		
				$(".sub-item-wrapper[data-main-category-id='" + mainCategoryId + "'][data-sub-category-id='" + subCategoryId + "']")
					.removeClass('active');
			}
			else {
				// remove all the sub-title active class first
				$('.sub-category').removeClass('active');
				// hide all the sub-item-wrapper first.
				$(".sub-item-wrapper").removeClass('active');
				
				// active the current sub-category
				$(this).addClass('active');
				// show the child sub-item-wrapper of this clicked sub-title
				$(".sub-item-wrapper[data-main-category-id='" + mainCategoryId + "'][data-sub-category-id='" + subCategoryId + "']")
					.addClass('active');
				
			}
		}			

	});
	
	
	// keyword search
	$("#totalSearchText").keypress( function(e) {
		
		var keyCode = (event.keyCode ? event.keyCode : event.which);
		if( keyCode == '13') {
			var searchText = $(this).val();
			$('#searchKey').val(searchText);
			$("form[id='totalSearchForm']").submit();
		}
	});
	
</script>
