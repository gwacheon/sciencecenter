<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="pagination-wrapper">
	<ul class="pagination">
		<c:if test="${page.beginPage > 1 }">
			<li>
				<a href="${page.path }page=1" data-page="1" class="page-link {{page.className}}">
					<i class="fa fa-chevron-left"></i>
				</a>
			</li>
			<li>
				<a href="${page.path }page=${page.beginPage - 1}" data-page="${page.beginPage - 1}" class="page-link"><i class="fa fa-ellipsis-h"></i></a>
			</li>
		</c:if>
		
		<c:forEach var="i" begin="${page.beginPage }" end="${page.endPage }" varStatus="loop">
			<c:choose>
				<c:when test="${i == page.currentPage }">
					<li class="active">
						<a href="#" class="page-link">${i }</a>
					</li>
				</c:when>
				<c:otherwise>
					<li>
						<a href="${page.path }page=${i }" class="page-link">${i }</a>
					</li>
				</c:otherwise>
			</c:choose>
		</c:forEach>
		
		<c:if test="${page.endPage < page.totalPages }">
			<li>
				<a href="${page.path }page=${page.endPage + 1}" data-page="${page.endPage + 1}" class="page-link"><i class="fa fa-ellipsis-h"></i></a>
			</li>
			<li>
				<a href="${page.path }page=${page.totalPages }" data-page="${page.totalPages }" class="page-link"><spring:message code="page.last" text=""/>
					<i class="fa fa-chevron-right"></i>
				</a>
			</li>
		</c:if>
	</ul>
</div>