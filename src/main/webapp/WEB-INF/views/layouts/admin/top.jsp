<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<div id="nav-top" class="hide-on-small-only">
	<div class="container">
		<div class="row">
			<div class="col s12">
				<div class="nav-top-wrapper">
					<a href='<c:url value="/"/>' class="brand-logo">
						<img src="<c:url value="/resources/img/logo.png"/>" id="logo-img"
							class="responsive-img" alt="국립과천과학관" />
					</a> 
					<ul class="right">
						<li>
							<a href='<c:url value="/admin"/>'>관리자</a>
						</li>
						<li>
							<a href="<c:url value="/admin/logout"/>" id="logout-btn">Logout</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<nav id="admin-nav" style="min-height: 64px; height: auto;">
	<div class="nav-wrapper">
		<div class="container">
			<a href="#" data-activates="mobile-demo" class="button-collapse">
				<i class="mdi-navigation-menu"></i>
			</a>
			<ul id="dropdown1" class="dropdown-content">
			  <li><a href="<c:url value="/admin/sangsang/device"/>">장비 관리</a></li>
			  <li><a href="<c:url value="/admin/sangsang/building"/>">대관 관리</a></li>
			  <li><a href="<c:url value="/admin/sangsang/schedules/manage"/>">일정 관리</a></li>
			</ul>
			<ul id="dropdown2" class="dropdown-content">
			  <li><a href="<c:url value="/admin/legacy/?url=/masterpage/member/"/>">회원 관리</a></li>
			  <li><a href="<c:url value="/admin/legacy/?url=/masterpage/etc/"/>">강좌 관리</a></li>
			  <li><a href="<c:url value="/admin/legacy/?url=/masterpage/code/"/>">코드 관리</a></li>
			  <li><a href="<c:url value="/admin/legacy/?url=/masterpage/siteManager/"/>">환경 설정</a></li>
			</ul>
			<ul class="main-nav hide-on-med-and-down" style="overflow: auto;">
				<li class="<c:if test="${currentAdminCategory eq 'mainPage' }"> active</c:if>">
					<a href='<c:url value="/admin/mainPage/banners"/>'>
						메인페이지
					</a>
				</li>
				<li>
					<a href='<c:url value="/admin/structures"/>'>
						대관
					</a>
				</li>
				<li>
					<a href='<c:url value="/admin/career"/>'>
						진로탐구
					</a>
				</li>
				<li>
					<a href='<c:url value="/admin/voluntaries"/>'>
						봉사활동
					</a>
				</li>
 				<li>
 					<a class="dropdown-button" href="#!" data-activates="dropdown1">
 						무한상상실<i class="material-icons right">arrow_drop_down</i>
					</a>
				</li>
				<li>
					<a href="<c:url value='/admin/orgPrograms'/>">
						단체예약
					</a>
				</li>
				<li>
					<a href='<c:url value="/admin/surveys"/>'>
						설문
					</a>
				</li>
				<li class="<c:if test="${currentAdminCategory eq 'events' }"> active</c:if>">
					<a href='<c:url value="/admin/events/list/event"/>'>
						문화행사
					</a>
				</li>
				<li class="<c:if test="${currentAdminCategory eq 'mainEvents' }"> active</c:if>">
					<a href='<c:url value="/admin/mainEvents"/>'>
						대표&기타 문화행사
					</a>
				</li>
				<li class="<c:if test="${currentAdminCategory eq 'board' }"> active</c:if>">
					<a href='<c:url value="/admin/boards"/>'>
						게시판
					</a>
				</li>
				<li class="<c:if test="${currentAdminCategory eq 'supportEvaluation' }"> active</c:if>">
					<a href='<c:url value="/admin/supportEvaluation"/>'>
						고객서비스평가
					</a>
				</li>
				<li>
					<a href='<c:url value="/admin/policies"/>'>
						약관/방침
					</a>
				</li>
				<li class="<c:if test="${currentAdminCategory eq 'dinnerApply' }"> active</c:if>">
					<a href='<c:url value="/admin/dinnerApply"/>'>
						석식신청
					</a>
				</li>
				<li>
 					<a class="dropdown-button" href="#!" data-activates="dropdown2">
 						회원/강좌<i class="material-icons right">arrow_drop_down</i>
					</a>
				</li>
			</ul>
		</div>
	</div>
</nav>

<script type="text/javascript">
	$( function() {
		$(".button-collapse").sideNav();
	});
</script>