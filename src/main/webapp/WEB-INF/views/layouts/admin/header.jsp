<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<sec:csrfMetaTags />

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<title>국립과천과학관</title>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<link rel="shortcut icon" href="<c:url value='/img/favicon.ico'/>">	
	
<!-- CSS -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<c:choose>
	<c:when test="${environmentMode == 'production'}">
		<link rel="stylesheet" type="text/css" href="<c:url value='/assets/all.css'/>" />
		<script type="text/javascript" src="<c:url value='/assets/all.js'/>"></script>
	</c:when>
	
	<c:otherwise>
		<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/admin.css'/>">
		
		<script type="text/javascript" src="<c:url value='/resources/js/ext/jquery-1.11.3.min.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/resources/js/ext/jquery-ui.1.11.4.min.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/resources/js/ext/materialize.min.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/resources/js/ext/lodash-3.10.1.min.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/resources/js/ext/pickadate/picker.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/resources/js/ext/pickadate/picker.date.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/resources/js/ext/pickadate/picker.time.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/resources/js/ext/pickadate/legacy.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/resources/js/ext/handlebars.3.0.3.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/resources/js/ext/moment.min.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/resources/js/common.js'/>"></script>
		<script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>
		<script type="text/javascript" src="<c:url value='/resources/js/ptech/ptech-calendar.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/resources/js/program.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/resources/js/ptech/ptech-slider.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/resources/js/structureUnavailable.js'/>"></script>
	</c:otherwise>
</c:choose>




<!-- SCRIPT -->


<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="<c:url value='/resources/js/html5shiv.js'/>"></script>
	<script src="<c:url value='/resources/js/ext/respond.js'/>"></script>
<![endif]-->

<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js" type="text/javascript"></script>
<![endif]-->

<!-- base URL for script -->
<!-- base URL for script -->
<spring:eval expression="@propertyConfigurer.getProperty('webroot.relativePath')" var="fileRelativePath" />
<spring:eval expression="@propertyConfigurer.getProperty('legacy.companycd')" var="companycd" />
<c:set var="baseFileUrl" value="/${fileRelativePath}${companycd}/" scope='application'/>
<script type="text/javascript">
	var baseUrl = "<c:url value="/"/>";
	var baseFileUrl = "<c:url value="${fileRelativePath}${companycd}/"/>";
</script>
