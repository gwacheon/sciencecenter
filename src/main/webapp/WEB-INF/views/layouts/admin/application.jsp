<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE HTML>
<html>
	<head>
		<t:insertAttribute name="header"/>
	</head>
	
	<body>
	
		<t:insertAttribute name="top"/>
		<t:insertAttribute name="content"/>
		<t:insertAttribute name="footer"/>
		
		<div id="ajaxLoader">
			<img src="<c:url value='/img/ajax-loader.gif'/>" alt="ajax-loader"/>
		</div>
		
		<script type="text/javascript">
			function ajaxLoading(){
				$("body").css("opacity", "0.5");
				$("#ajaxLoader").show();
			}
			
			function ajaxUnLoading(){
				$("body").css("opacity", "1");
				$("#ajaxLoader").hide();
			}
		</script>
		
		<script type="text/javascript">
		    $(function(){
		      $(".button-collapse").sideNav();
		      
		      $('.dropdown-button').dropdown({
		          inDuration: 300,
		          outDuration: 225,
		          constrain_width: false, // Does not change width of dropdown to that of the activator
		          gutter: 0, // Spacing from edge
		          belowOrigin: false // Displays dropdown below the button
		        }
		      );
		    });
		</script>
	</body>
</html>