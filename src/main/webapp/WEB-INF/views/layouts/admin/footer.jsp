<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<div id="footer" class="admin-footer">
	<div class="footer-bottom">
		<div class="container">
			<div class="row">
				<div class="col s12 m2 hide-on-med-and-down">
					<img src="<c:url value="/resources/img/footer_logo_kr.png"/>" 
						class="responsive-img footer-logo-img margin-top20"/>
				</div>
				<div class="col s12 m10">
					<div class="row footer-links">
						<div class="col s12 m12 guide">
							<a href="<c:url value='/support/policies' />">
								이용약관
							</a>
							<a href="<c:url value='/support/policies?type=privacy' />">
								개인정보 취급방침
							</a>
							<a href="/gnsm_web/?sub_num=602">
								이메일 무단수신거부
							</a>
							<a href="/gnsm_web/popup/email/email.jsp" target="_blank">
									운영자에게
							</a>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m6">
							<div class="address">
								<!-- 13817 경기도 과천시 상하별로 110 국립과천과학관<br>
								<strong style="margin-right: 10px">대표전화</strong> 02-2677-1500 <strong style="margin-right: 10px;margin-left: 30px">Fax</strong> 02-3677-1328 -->
								<strong style="margin-right: 10px">관람시간</strong>  오전 9:30 ~ 오후 5:30 (<b>입장마감</b> 오후 4:30)  <b>휴관일</b> 1월 1일, 매주 월요일<br> 
								<strong style="margin-right: 10px">주소</strong>  13817 경기도 과천시 상하벌로 110 국립과천과학관 <br>
								<strong style="margin-right: 10px">대표전화</strong>  02-3677-1500   <strong style="margin-right: 10px; margin-left: 10px;">Fax</strong>  02-3677-1328
							</div>
						</div>

						<div class="col s12 m6">
							<div class="row">
								<div class="col s4 m4">
									<div class="footer-link-images">
										<img src="<c:url value="/resources/img/main/footer-link-image1.png" />"
											alt="" class="visible-lg"/>
										<div class="footer-link-texts">
											<a href="http://www.sstm.org.cn/kjg_web/html/kjg_korea/portal/index/index.htm" target="_blank">
												<div class="sub-title">상해과학 기술관</div>
												<div class="sub-text">해외과학 기술관</div>
											</a>
										</div>
									</div>
								</div>
								<div class="col s4 m4">
									<div class="footer-link-images">
										<img src="<c:url value="/resources/img/main/footer-link-image2.png" />"
											alt="" class="visible-lg"/>
										<div class="footer-link-texts">
											<a href="http://60.247.10.155:8007/portal/findportal.do?portalid=orga2e3e19908bfa" target="_blank">
												<div class="sub-title">중국과학 기술관</div>
												<div class="sub-text">해외협력 과학관</div>
											</a>
										</div>
									</div>
								</div>
								<div class="col s4 m4">
									<div class="footer-link-images">
										<img src="<c:url value="/resources/img/main/footer-link-image3.png" />"
											alt="" class="visible-lg"/>
										<div class="footer-link-texts">
											<a href="http://www.miraikan.jst.go.jp/ko/" target="_blank">
												<div class="sub-title">일본과학 미래관</div>
												<div class="sub-text">해외협력 과학관</div>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('.dropdown-button').dropdown();
</script>