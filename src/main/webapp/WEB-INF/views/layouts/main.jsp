<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		
		<title>국립과천과학관</title>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">
			
		<!-- CSS -->
		<link href="<c:url value='/resources/css/ext/materialize.css'/> " rel="stylesheet"/>
		<link href="<c:url value='/resources/css/ext/materialize_override.css'/> " rel="stylesheet"/>
		<link href="<c:url value='/resources/css/ptech/ptech-calendar.css'/> " rel="stylesheet"/>
		<link href="<c:url value='/resources/css/application.css'/> " rel="stylesheet"/>
		<link href="<c:url value='/resources/css/main_c.css'/> " rel="stylesheet"/>
		<link href="<c:url value='/resources/css/main_a.css'/> " rel="stylesheet"/>
		
		<!-- SCRIPT -->
		<script src="<c:url value='/resources/js/ext/jquery-1.11.3.min.js'/> "></script> 
		<script src="<c:url value='/resources/js/ext/materialize.min.js'/> "></script>
		<script src="<c:url value='/resources/js/ext/lodash-3.10.1.min.js'/> "></script>
		<script src="<c:url value='/resources/js/ext/handlebars.3.0.3.js'/> "></script>
		<script src="<c:url value='/resources/js/ext/moment.min.js'/> "></script>
		<script src="<c:url value='/resources/js/common.js'/> "></script>
		<script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>
		<script src="<c:url value='/resources/js/ptech/ptech-calendar.js'/> "></script>
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="<c:url value='/resources/js/html5shiv.js'/>"></script>
			<script src="<c:url value='/resources/js/ext/respond.js'/>"></script>
		<![endif]-->
		
		<!--[if lt IE 9]>
		  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js" type="text/javascript"></script>
		<![endif]-->
		
		<!-- base URL for script -->
		<script type="text/javascript">
			var baseUrl = "<c:url value="/"/>";
		</script>
	</head>
	
	<body>
		<t:insertAttribute name="content"/>
		
		<script type="text/javascript">
		    $(function(){
		      $(".button-collapse").sideNav();
		      
		      $('.dropdown-button').dropdown({
		          inDuration: 300,
		          outDuration: 225,
		          constrain_width: false, // Does not change width of dropdown to that of the activator
		          gutter: 0, // Spacing from edge
		          belowOrigin: false // Displays dropdown below the button
		        }
		      );
		    });
		</script>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-66853433-1', 'auto');
		  ga('send', 'pageview');
		</script>
	</body>
</html>