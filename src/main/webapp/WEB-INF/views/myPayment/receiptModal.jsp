<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


	<!-- Modal -->
	<div class="modal fade" id="receipt-modal">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">예약증 출력</h4>
	      </div>
	      <div class="modal-body" id="receiptArea">
				<div class="row">
					<div class="col-md-12">
						<div class="reservation-receipe-wrapper" style="background-image:none;" id="reservation-receipt">
							
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer" id="receipt_btn">
				<button type="button" class="btn btn-primary" onclick="PaymentReceiptPrint();" >인쇄하기</button>
	        <button type="button" class="btn btn-teal reverse" data-dismiss="modal" onclick="paymentRefresh()">확인</button>
	      </div>
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
		function PaymentReceiptPrint(){
			if(confirm('예약증을 프린트 하시겠습니까?')){
				$.ajax({
					type : 'get',
					url  : '<c:url value="/payment/paymentPrint"/>?${_csrf.parameterName}=${_csrf.token}',
					data : $('form[name=frmPaymentReceiptPrint]').serialize(),
					dataType : 'json',
					success : function(data){
						if(data.result){
							PrintProc();
						}else{
							alert(data.Msg);
							return;
						}
					}
				}); 
			} 
		}
		
		function PrintProc(){
			$('#receipt_btn').hide();
			var AreaContents = document.getElementById('receipt-modal').innerHTML;
	        var strFeature;
	        strFeature = "width=900,height=750,toolbar=no,location=no,directories=no";
	        strFeature += ",status=no,menubar=no,scrollbars=yes,resizable=no";
	        var cssBody = '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/external.css"/> \' >';
			cssBody += '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/ptech.css"/> \' >';
	        cssBody += '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/application.css"/> \' >';
	        cssBody += '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/reservation.css"/> \' >';
	        cssBody += '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/user.css"/> \' >';
	       	
	        var meta = '<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">';
	        meta += '<meta charset="utf-8">';
	        meta += '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
	        meta += '<meta name="viewport" content="width=device-width, initial-scale=1.0">';
	        meta += '<meta name="author" content="">';
	        meta += '<meta name="description" content="">';
	        
	        objWin = window.open('', 'print', strFeature);
	        objWin.focus();
	        
	        
	        objWin.document.open();
	        objWin.document.write('<html>');
	        objWin.document.write(meta);
	        objWin.document.write('<head>');
	        objWin.document.write(cssBody);
	        objWin.document.write('</head><body class="modal-open" style="padding-right:17px">');
	        objWin.document.write('<div id="content-body"');
	        objWin.document.write(AreaContents);
	        objWin.document.write('</div>');
	        objWin.document.write('</body></html>');
	        objWin.document.close();
	    
	        var delay = setTimeout(function(){
	        	print(objWin);
	        },2000);
	        
	    }
		
		function print(Obj){
			$('#receipt_btn').show();
			console.log(Obj);
			Obj.print();
			Obj.close();
		}
		
		function paymentRefresh(){
			$('.close').trigger('click');
			Search();
		}
	</script>