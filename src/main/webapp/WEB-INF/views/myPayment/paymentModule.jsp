<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script type="text/javascript" src="${pay_targetUrl}"></script>

<script>
	//4. 결제하기 - 지정한 해시값을 생성 후
	function doPay(){
		var mallForm = document.mallForm;
		
		$.ajax({
			method: "GET",
			url	 : '<c:url value="/entrance/api/payment/hash" />?${_csrf.parameterName}=${_csrf.token}',
			data: {
				
				mbr_id		: mallForm.mbrId.value,			// 가맹점 ID
				salesPrice	: mallForm.salesPrice.value,	// 판매가격
				oid			: mallForm.oid.value			// 거래번호
			}
		}).done(function(data) {
			if (data.hasOwnProperty("error")) {
				alert("결제 초기화에 실패했습니다.\n나중에 다시 시도해주세요.");
			} else {
				mallForm.hashValue.value = data.hash;				// hashValue
				C2StdPay.pay();
			}
		}).fail(function(jqXHR, textStatus) {
			alert("결제 초기화에 실패했습니다.\n나중에 다시 시도해주세요.");
		});
		return;
	}
</script>
<div  id="payment-modal">
<iframe name="x_frame" id='x_frame' style="display:none" width="500" height="100"></iframe>
<form name="mallForm"  id="mallForm"  method="POST">
	<input type="hidden" name="version"			value="${pay_version}">			<!-- 버전 -->
	<input type="hidden" name="payKind"			value="1" />					<!-- 결제종류 (1:카드, 2:가상계좌, 3:계좌이체) -->
	<input type="hidden" name="mbrId"			value="${pay_mbrId}" />			<!-- 가맹점 아이디 -->
	<input type="hidden" name="mbrName"			value="${pay_mbrName}" />		<!-- 가맹점 이름 -->
	<input type="hidden" name="buyerName" 		value="${MEMBER_NAME}"/>		<!-- 구매자 이름 -->
	<input type="hidden" name="buyerMobile" 	value="${MEMBER_CEL}"/>		<!-- 구매자 휴대폰번호 -->
	<input type="hidden" name="buyerEmail" 		value="${MEMBER_EMAIL}"/>		<!-- 구매자 이메일번호 -->
	<input type="hidden" name="bizNo"			value="${pay_bizNo}">			<!-- 사업자번호 -->
	<input type="hidden" name="authType"		value="${pay_authType}">		<!-- 인증타입-->
	<input type="hidden" name="server"			value="${pay_server}" />		<!-- 0:테스트, 1:운영 -->
	<input type="hidden" name="returnType"		value="${pay_returnType}" />	<!-- 결제 구분 (payment:인증 & 승인, auth:인증) -->
	<c:if test="${SALE_TYPE eq '000005' }">
		<input type="hidden" name="callbackUrl" 	value="${fn:replace(c2_homepage_url,'https','http')}/resources/payment1.3/paymentCallBack.jsp"	/>
	</c:if>
	<c:if test="${SALE_TYPE != '000005' }">
		<input type="hidden" name="callbackUrl" 	value="${c2_homepage_url}/resources/payment1.3/paymentCallBack.jsp"	/>
	</c:if> 
	
	<input type="hidden" name="oid" />				<!-- 가맹점 주문번호 - 가맹점에서 관리하는 주문 PK(ApplyUniqueNbr 를 사용) -->
	<input type="hidden" name="productName" />		<!-- 상품명 -->
	<input type="hidden" name="salesPrice" />		<!-- 결제금액 -->
	<input type="hidden" name="productPrice" />		<!-- 상품가격 -->
	<input type="hidden" name="productCount" />		<!-- 상품수량 -->
	<input type="hidden" name="returnUrl" />		<!-- 리턴 url -->
	<input type="hidden" name="hashValue" />		<!-- 위변조방지서명값 제공함수 사용 -->
	
	<input type="hidden" name="openDate" />			<!-- 입장(오픈)일 (옵션-강좌/후원회원가입에는 사용하지 않음.)-->
	
	<!-- 공통 -->
	<input type="hidden" name="APPLY_UNIQUE_NBR" value="" />
	<input type="hidden" name="MEMBER_NO" value="${MEMBER_NO}" />
	<input type="hidden" name="SALE_TYPE" value="${SALETYPE}" />
	<input type="hidden" name="TOTAL_PRICE" value="" />
	<input type="hidden" name="APPLY_CNT" value="" />
	<input type="hidden" name="PAYMENT_AMT" value="" />
	<input type="hidden" name="COURSE_NAME" value="" />
	
</form>
</div>