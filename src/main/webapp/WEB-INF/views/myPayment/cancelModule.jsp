<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix='fn' uri='http://java.sun.com/jsp/jstl/functions'%>

<script type="text/javascript">
function doCancel()
{
	var url = '../payment/cancelDummy.jsp?${_csrf.parameterName}=${_csrf.token}';
	$('form[name=CancelForm]').attr('target', 'y_frame');
	$("form[name=CancelForm]").attr("action",url);
	$('form[name=CancelForm]').submit();
}
</script>
<div id="cancel-modal">
<iframe name="y_frame" id="y_frame" style="display:none" width="500" height="100"></iframe>
<form name="CancelForm" action="../payment/cancelDummy.jsp?${_csrf.parameterName}=${_csrf.token}"  method="POST">
	<input type="hidden" name="mbrId" value="551110" />		<!-- 가맹점 -->
	<input type="hidden" name="mbrName" value="CSQUARE" />	<!-- 가맹점 이름 -->
	<input type="hidden" name="cardTradeNo" value="" />		<!-- 카드승인번호 -->
	<input type="hidden" name="cardApprovDate" value="" />	<!-- 카드승인일 -->
	<input type="hidden" name="salesPrice" value="" />		<!-- 결제금액 -->
	<input type="hidden" name="payType" value="" />			<!-- 구분 CSQ : 비인증(KeyIn),ISP :ISP 결제,3D:안심 -->
</form>
</div>