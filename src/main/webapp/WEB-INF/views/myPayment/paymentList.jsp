<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix='fn' uri='http://java.sun.com/jsp/jstl/functions'%>
<style>
.my-reservation-tab .label{
	margin-bottom: 2px;
}
</style>
		<div class="container">
			<div class="row margin-top30">
				<div class="col-md-3">
					<c:import url="/WEB-INF/views/mypage/side.jsp">
						<c:param name="active" value="reservations"></c:param>
					</c:import>
				</div>
				
				<div class="col-md-9">
					<h4 class="doc-title teal">
						예약내역
					</h4>

					<div>
					  <!-- Nav tabs -->
					  <ul class="nav nav-tabs" role="tablist">
					    <li role="presentation" class="active">
					    	<a href="#programs" aria-controls="home" role="tab">프로그램 예약</a>
				    	</li>
					    <li role="presentation">
					    	<!-- <a href="my_page_movie.html">상영관 예약</a>  -->
					    	<a href="<%= request.getContextPath() %>/entrance/payment">천체투영관/스페이스월드 예약</a>
				    	</li>
					  </ul>

					  <!-- Tab panes -->
					  <div class="my-reservation-tab tab-content">
					    <div role="tabpanel" class="tab-pane active clearfix" id="programs">
					    	
					    <form name="frmSearch">
					    	<input type="hidden" name="pageNum" value="${pageNum }" />
					    	
					    	<div class="search-forms-in-mypage">
					    		<div class="row">
					    			<div class="col-sm-2 col-xs-6 col-sm-offset-3" style="margin-left:40%;">
					    				<select class="form-control" name="STATUS_TYPE">
					    					<option value="" ${StatusType eq ''?'selected=\'selected\'':''}>전체</option>
					    					<option value="RESER" ${StatusType eq 'RESER'?'selected=\'selected\'':''}>예약</option>
					    					<option value="CANCEL" ${StatusType eq 'CANCEL'?'selected=\'selected\'':''}>예약취소</option>
					    					<option value="PAY" ${StatusType eq 'PAY'?'selected=\'selected\'':''}>결제완료</option>
					    					<option value="REFUND" ${StatusType eq 'REFUND'?'selected=\'selected\'':''}>결제취소</option>
					    					<option value="RECEIPT" ${StatusType eq 'RECEIPT'?'selected=\'selected\'':''}>입금대기</option>
					    				</select>
					    			</div>
<!-- 					    			<div class="col-sm-2"> -->
<!-- 					    				<select class="form-control"> -->
<!-- 					    					<option>프로그램</option> -->
<!-- 					    				</select> -->
<!-- 					    			</div> -->
					    			<div class="col-sm-3">
					    				<input type="text" class="form-control" placeholder="검색어" name="SEARCH_NAME" value="${SearchName}" onKeyDown="javascript:if (event.keyCode == 13) Search();">
					    			</div>
					    			<div class="col-sm-2">
					    				<button type="button" class="btn btn-teal full-width"  onclick="Search()">
					    					검색
					    				</button>
					    			</div>
					    		</div>
					    	</div>
					    </form>

					    	<div class="table-responsive table-teal">
					    		<table class="table centered-table">
					    			<thead>
					    				<tr>
					    					<th>예약일</th>
					    					<th>프로그램</th>
					    					<th>인원</th>
					    					<th>상태</th>
					    					<th class="hidden-xs">출력</th>
					    				</tr>
					    			</thead>

					    			<tbody>
					    			
					    			<c:forEach items="${PaymentList}" var="PaymentList">
					    				<tr>
					    					<td>
					    						<fmt:parseDate value="${PaymentList.RESER_DATE }" var="RESER_DATE" pattern="yyyyMMdd"/>
												<fmt:formatDate value="${RESER_DATE }" var="ReserDate" pattern="yyyy.MM.dd"/>
												${ReserDate }
					    					</td>
					    					<td class="left" style="cursor:pointer;" onclick="javascript:location.href='<c:url value='/schedules/${PaymentList.COURSE_CD}'/>'">
					    						<h6 class="doc-title">
					    							${PaymentList.COURSE_NAME}
					    						</h6>
					    							<c:if test="${PaymentList.SCHEDULE_FLAG eq 'Y' }">
					    								<fmt:parseDate value="${PaymentList.COURSE_DATE }" var="COURSE_DATE" pattern="yyyyMMdd"/>
														<fmt:formatDate value="${COURSE_DATE }" var="CourseDate" pattern="yyyy.MM.dd"/>
														${CourseDate}(${PaymentList.COURSE_WEEK_STR})
					    							</c:if>
					    							
					    							<c:if test="${PaymentList.SCHEDULE_FLAG eq 'N' }">
					    								<fmt:parseDate value="${PaymentList.COURSE_START_DATE }" var="COURSE_START_DATE" pattern="yyyyMMdd"/>
														<fmt:formatDate value="${COURSE_START_DATE }" var="CourseStartDate" pattern="yyyy.MM.dd"/>
														<fmt:parseDate value="${PaymentList.COURSE_END_DATE }" var="COURSE_END_DATE" pattern="yyyyMMdd"/>
														<fmt:formatDate value="${COURSE_END_DATE }" var="CourseEndDate" pattern="yyyy.MM.dd"/>
														${CourseStartDate}~${CourseEndDate}(${PaymentList.COURSE_WEEK_STR})
					    							</c:if>
					    						<br>
					    						<fmt:parseDate value="${PaymentList.COURSE_START_TIME }" var="COURSE_START_TIME" pattern="HHmm"/>
												<fmt:formatDate value="${COURSE_START_TIME }" var="CourseStartTime" pattern="HH:mm"/>
												<fmt:parseDate value="${PaymentList.COURSE_END_TIME}" var="COURSE_END_TIME" pattern="HHmm"/>
												<fmt:formatDate value="${COURSE_END_TIME }" var="CourseEndTime" pattern="HH:mm"/>
					    							${PaymentList.PRICE_NAME} ${CourseStartTime} ~ ${CourseEndTime}
					    					</td>
					    					<td>
					    						<a href="javascript:void(0)" onclick="MemberInfo('${PaymentList.APPLY_UNIQUE_NBR}','${PaymentList.COURSE_CD}')">
					    							${PaymentList.APPLY_CNT } 명
					    						</a>
					    					</td>
					    					<td>				    						
					    						 <c:if test="${PaymentList.PAY_STATUS eq 'BILL' }">
						    						 <span class="label green-label">
						    						 	${PaymentList.APPLY_STATUS}
						    						 </span><br>
						    						 
						    						 <c:if test="${PaymentList.TOTAL_PRICE > 0}">
							    						 <a href="javascript:Receipe('${PaymentList.APPLY_UNIQUE_NBR }')">
								                      		영수증출력
								                      	 </a>
								                      </c:if>     
					    						 </c:if>
					    						 
					    						 <c:if test="${PaymentList.PAY_STATUS eq 'EXCEED' }">
					    						 	<span class="label danger-label">
					    						 		결제기한만료
					    						 	</span><br>
					    						 </c:if>
					    						 
					    						 <c:if test="${PaymentList.PAY_STATUS eq 'NONE' }">
					    						 		<c:if test="${PaymentList.TOTAL_PRICE == 0}">
					    						 			<span class="label warning-label">
					    						 				신청취소
					    						 			</span>
					    						 		</c:if>
					    						 		
					    						 		<c:if test="${PaymentList.TOTAL_PRICE > 0}">
					    						 			<c:choose>
					    						 				<c:when test="${PaymentList.CANCEL_FLAG eq 'Y' }">
					    						 					<fmt:parseDate value="${PaymentList.CANCEL_DATE }" var="CANCEL_DATE" pattern="yyyyMMdd"/>
			                      									<fmt:formatDate value="${CANCEL_DATE }" var="CancelDate" pattern="yyyy년 MM월 dd일"/>
			                      									<span class="label warning-label">
						    						 					${PaymentList.APPLY_STATUS}
						    						 				</span><br/> (${CancelDate})
					    						 				</c:when>
					    						 				
					    						 				<c:when test="${PaymentList.REFUND_FLAG eq 'Y' }">
					    						 					<fmt:parseDate value="${PaymentList.REFUND_DATE }" var="REFUND_DATE" pattern="yyyyMMdd"/>
			                      									<fmt:formatDate value="${REFUND_DATE }" var="RefundDate" pattern="yyyy년 MM월 dd일"/>
			                      									<span class="label warning-label">
						    						 					${PaymentList.APPLY_STATUS} 
						    						 				</span><br/>(${RefundDate})
					    						 				</c:when>
					    						 				
					    						 				<c:otherwise>
					    						 					<span class="label warning-label">
					    						 						${PaymentList.APPLY_STATUS}
					    						 					</span>
					    						 				</c:otherwise>
					    						 			</c:choose>
					    						 		</c:if>
					    						 	<br/>
					    						 	<c:if test="${PaymentList.REFUND_FLAG eq 'N' and PaymentList.CANCEL_FLAG  eq 'N' 
					    						 				and PaymentList.RECEIPT_FLAG  eq 'N' and PaymentList.RESER_FLAG  eq 'Y' 
					    						 				and PaymentList.PAY_FLAG  eq 'Y' and PaymentList.PAY_STATUS ne 'EXCEED'}">
					    						 		<a href="javascript:void(0)" onclick="VaInfo('${status.count}')">계좌정보</a>
					    						 		
					    						 		<!-- Modal -->
														<div class="modal fade" id="account-modal_${status.count}">
														  <div class="modal-dialog" role="document">
														    <div class="modal-content">
														      <div class="modal-header">
														        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
														        <h4 class="modal-title" id="myModalLabel">계좌정보</h4>
														      </div>
														      <div class="modal-body">
														        <table class="table centered-table lined-table">
														        	<tbody>
														        		<tr>
														        			<td>
															        			<fmt:parseDate value="${PaymentList.PAYMENT_LIMIT_DATE }" var="PAYMENT_LIMIT_DATE" pattern="yyyyMMddHHmm"/>
		                      													<fmt:formatDate value="${PAYMENT_LIMIT_DATE }" var="PaymentLimitDate" pattern="yyyy년 MM월 dd일 HH시 mm분"/>
														        				<strong>입금마감일</strong>
													        				</td>
														        			<td>${PaymentLimitDate }</td>
														        		</tr>
														        		<tr>
														        			<td>
														        				<strong>가상계좌정보</strong>
													        				</td>
														        			<td>${PaymentList.VA_BANK_NAME } (${PaymentList.VA_ACCOUNT_NO })</td>
														        		</tr>
														        		<tr>
														        			<td>
														        				<strong>예금주명</strong>
													        				</td>
														        			<td>씨스퀘어소프트</td>
														        		</tr>
														        		<tr>
														        			<td>
														        				<strong>결제금액</strong>
													        				</td>
													        				<fmt:formatNumber value="${PaymentList.TOTAL_PRICE}" var="TOTAL_PRICE"/>
														        			<td>${TOTAL_PRICE }원</td>
														        		</tr>
														        	</tbody>
														        </table>
														        <button type="button" class="btn btn-primary" onclick="VaModalClose()">닫기</button>
														      </div>
														    </div> 
														  </div>
														</div>
					    						 	</c:if>
					    						 </c:if>
					    						 
					    						 <c:if test="${PaymentList.PAY_STATUS eq 'PAY' }">
					    						 	<span class="label danger-label">
					    						 		${PaymentList.APPLY_STATUS }
					    						 	</span>
						    						 
						    						 <br />
 						    						 <a href="javascript:CourseApplyPay('${PaymentList.APPLY_UNIQUE_NBR}','${PaymentList.TOTAL_PRICE}','${PaymentList.APPLY_CNT}','${PaymentList.COURSE_NAME}','${PaymentList.MBR_ID}','${PaymentList.MBR_NAME}','${PaymentList.CLASS_CD}')"> 
 							    						 		결제하기  
 						    						 </a> 
						    						 <br />
					    						 		<fmt:parseDate value="${PaymentList.RESERVE_LIMIT_DATE }" var="RESERVE_LIMIT_DATE" pattern="yyyymmdd"/>
                     									<fmt:formatDate value="${RESERVE_LIMIT_DATE }" var="ReserveLimitDate" pattern="yyyy.mm.dd"/>
                     									예약결제일 (${ReserveLimitDate })
					    						 </c:if>
					    						 
					    						 <c:if test="${PaymentList.PAY_STATUS eq 'VABANK' }">
						    						 <span class="label danger-label">
							                     		<a href="#">${PaymentList.APPLY_STATUS }</a>
							                     		 
								                       <c:if test="${fn:length(PaymentList.PAYMENT_LIMIT_DATE) > 0 }">
											                 <fmt:parseDate value="${PaymentList.PAYMENT_LIMIT_DATE }" var="PAYMENT_LIMIT_DATE" pattern="yyyymmdd"/>
										                     <fmt:formatDate value="${PAYMENT_LIMIT_DATE }" var="PaymentLimitDate" pattern="yyyy.mm.dd"/>
								                       		<span class="book-board-12">${PaymentLimitDate } 마감</span>
								                       </c:if>
								                     </span>
						                     	</c:if>
						                     	
						                     	<c:if test="${PaymentList.REFUND_STATUS eq 'CANCEL' }">
						                     		<br />
							                      	<a href="javascript:void(0)" onclick="CourseApplyCancel('${PaymentList.APPLY_UNIQUE_NBR}');">
							                      		취소하기
							                      	</a>
							                     </c:if>
							                     
							                     <c:if test="${PaymentList.REFUND_STATUS eq 'REFUND' }">
							                     	<br />
							                     	<c:if test="${PaymentList.TOTAL_PRICE == 0}">
								                     	<a href="javascript:RefundPayZero('${PaymentList.APPLY_UNIQUE_NBR}')">
									                    		취소하기
									                    </a>
									                </c:if>
									                
									                <c:if test="${PaymentList.TOTAL_PRICE > 0}">
								                     	<a href="javascript:RefundPay('${PaymentList.APPLY_UNIQUE_NBR}')">
									                    		환불하기
									                    </a>
									                </c:if>
							                     </c:if>
					    					</td>
					    					<td class="hidden-xs">
												<c:choose>
		    						 				<c:when test="${(PaymentList.APPLY_STATUS eq '대기' ||
		    						 								PaymentList.APPLY_STATUS eq '예약' ||
		    						 								PaymentList.APPLY_STATUS eq '입금대기' ||
		    						 								PaymentList.APPLY_STATUS eq '결제완료' ||
		    						 								PaymentList.APPLY_STATUS eq '예약완료' ||
		    						 								PaymentList.APPLY_STATUS eq '신청완료') && 
		    						 								(PaymentList.PAY_STATUS != 'EXCEED') &&
		    						 								(PaymentList.PRINT_FLAG eq 'N')}">
		    						 					<a href="javascript:void(0)" class="btn btn-teal reverse receipe-btn" onclick="ReceiptPay('${PaymentList.APPLY_UNIQUE_NBR}');">
							    							<i class="fa fa-search-plus"></i> 예약증
							    						</a>
		    						 				</c:when>
		    						 				
		    						 				<c:when test="${PaymentList.PRINT_FLAG != 'N'}">
		    						 					<a href="javascript:void(0)" class="btn btn-teal receipe-btn" onclick="javascript:alert('이미 예약증을 인쇄했습니다.\n예약증은 1회만 인쇄가 가능합니다.');">
							    							<i class="fa fa-search-minus"></i> 출력됨
							    						</a>
		    						 				</c:when>
		    						 				
		    						 				<c:otherwise>
		    						 				</c:otherwise>
		    						 			</c:choose>
		    						 			<p/>
		    						 			<c:if test="${PaymentList.COMPLETE_FLAG eq 'Y'}">
							                     	<a target="_blank" href="#" class="btn btn-teal reverse receipe-btn" onclick="getComplete('${PaymentList.APPLY_UNIQUE_NBR}','${PaymentList.ACADEMY_CD}');">
						    							<i class="fa fa-search-minus"></i> 수료증
						    						</a>
								                </c:if>
					    					</td>
					    				</tr>
					    			</c:forEach>
					    			</tbody>
					    		</table>
					    		<div class="pagination-wrapper">
									${pg }
								</div>
					    	</div>
					    </div>
					  </div>
					</div>
				</div>
			</div>
		</div>
	
	
	
<form name="frmProgramList">
	<input type="hidden" name="SALE_TYPE" value="${SALE_TYPE }" />
	<input type="hidden" name="APPLY_UNIQUE_NBR" value="" />
</form>

<!-- 모달 페이지 -->
<c:import url="/WEB-INF/views/myPayment/refundModal.jsp"></c:import>
<c:import url="/WEB-INF/views/myPayment/receiptModal.jsp"></c:import>
<c:import url="/WEB-INF/views/myPayment/receipeModal.jsp"></c:import>
<c:import url="/WEB-INF/views/myPayment/paymentChoiceModal.jsp"></c:import>

<c:import url="/WEB-INF/views/myPayment/paymentModule.jsp"></c:import>

<c:import url="/WEB-INF/views/myPayment/memberInfo.jsp"></c:import>


	<div id="ajaxLoader">
		<img src="../img/ajax-loader.gif" alt="ajax-loader"/>
	</div>

<form name="frmPaymentInfo">
	<input type="hidden" name="APPLY_UNIQUE_NBR" value="" />
	<input type="hidden" name="MEMBER_NO" value="${MEMBER_NO }" />
	<input type="hidden" name="SALE_TYPE" value="${SALE_TYPE }" />
	<input type="hidden" name="TOTAL_PRICE" value="" />
	<input type="hidden" name="APPLY_CNT" value="" />
	<input type="hidden" name="PAYMENT_AMT" value="" />
	<input type="hidden" name="COURSE_NAME" value="" />
	<input type="hidden" name="CARD_APPROVE_DATE" value="" />
	<input type="hidden" name="CARD_APPROVE_NO" value="" />
	<input type="hidden" name="CARD_TRADE_NO" value="" />
	<input type="hidden" name="CARD_NAME" value="" />
	<input type="hidden" name="PAY_KIND" value="" /> 
	<input type="hidden" name="PAY_TYPE" value="" />
	<input type="hidden" name="VACCOUNT" value="" /> 
	<input type="hidden" name="VACCOUNT_BANK_NAME" value="" />
	
</form>

<form name="CancelForm">
	<input type="hidden" name="mbrId" value="551110" />		<!-- 가맹점 -->
	<input type="hidden" name="version"	value="1.2">		<!-- 버젼정보 -->
	<input type="hidden" name="mbrName" value="CSQUARE" />	<!-- 가맹점 이름 -->
	<input type="hidden" name="cardTradeNo" value="" />		<!-- 카드승인번호 -->
	<input type="hidden" name="cardApprovDate" value="" />	<!-- 카드승인일 -->
	<input type="hidden" name="salesPrice" value="" />		<!-- 결제금액 -->
	<input type="hidden" name="payType" value="" />			<!-- 구분 CSQ : 비인증(KeyIn),ISP :ISP 결제,3D:안심 -->
	<input type="hidden" name="buyerName" value="" />		<!-- 구매자 이름 -->
	<input type="hidden" name="buyerMobile" value="" />		<!-- 구매자 전화번호 -->
	<input type="hidden" name="productName" value="" />		<!-- 상품명 -->
</form>

<form name="CompleteForm">
	<input type="hidden" name="CompleteApplyUniqueNbr" id="CompleteApplyUniqueNbr" />
	<input type="hidden" name="CompleteAcademyCd" id="CompleteAcademyCd" />
</form>
<script type="text/javascript">

	// 입금정보
	function VaInfo(cnt){
		$('#account-modal_'+cnt).modal();
	}
	
	// 입금정보 닫기
	function VaModalClose(){
		$('.close').click();
	}

	//결제
	function PayPage(ApplyUniqueNbr, TotalPrice, ApplyCnt, CourseName, MemberName){
		
		$("#payment-choice").modal();
		
		var frm = document.mallForm;
		var frm2 = document.frmPaymentInfo;
		
		frm.APPLY_UNIQUE_NBR.value = ApplyUniqueNbr;
		frm.buyerName.value    = MemberName;
		frm.productName.value  = CourseName;
		frm.productPrice.value = TotalPrice;
		frm.productCount.value = ApplyCnt;
		frm.salesPrice.value   = TotalPrice;
		frm.TOTAL_PRICE.value = TotalPrice;
		frm.APPLY_CNT.value   = ApplyCnt;
		frm.PAYMENT_AMT.value = TotalPrice;
		frm.COURSE_NAME.value = CourseName;
		
		frm2.TOTAL_PRICE.value = TotalPrice;
		frm2.APPLY_CNT.value   = ApplyCnt;
		frm2.PAYMENT_AMT.value = TotalPrice;
		frm2.COURSE_NAME.value = CourseName;
		frm2.APPLY_UNIQUE_NBR.value = ApplyUniqueNbr;
		
	}
	
	// 결제 값
// 	function payResult(resultParam){
// 		var frm = document.frmPaymentInfo;
		
// 		frm.CARD_APPROVE_DATE.value = resultParam.cardApprovDate;
// 		frm.CARD_APPROVE_NO.value 	= resultParam.cardApprovNo;
// 		frm.CARD_NAME.value			= resultParam.cardName;
// 		frm.CARD_TRADE_NO.value		= resultParam.cardTradeNo;
// 		frm.PAY_KIND.value			= resultParam.payKind;
// 		frm.PAY_TYPE.value			= resultParam.payType;

// 		CourseApplyPay_payment(frm.APPLY_UNIQUE_NBR.value);		
// 	}
	
	// 가상계좌
// 	function VaResult(resultParam){
// 		var frm = document.frmPaymentInfo;
// 		/* 
// 			reusltParam
// 			cardApprovNo: 카드 승인번호
// 			cardTradeNo: 거래 번호
// 			cardName: 카드명
// 			cardApprovDate: 승인일
// 			cashReceipt: 현금영수증 번호	
// 			vAccountBankName: 가상계좌 은행명
// 			vAccount: 가상계좌 계좌명
// 			paykind : vAccount / card
// 		*/
// 		frm.CARD_TRADE_NO.value		 = resultParam.cardTradeNo;
// 		frm.VACCOUNT.value			 = resultParam.vAccount;
// 		frm.VACCOUNT_BANK_NAME.value = resultParam.vAccountBankName;
// 		frm.PAY_KIND.value			 = 'VAT';
// 		frm.PAY_TYPE.value			 = resultParam.payType;
		
// 		CourseApplyPay_Va(frm.APPLY_UNIQUE_NBR.value);
// 	}
	
	// VA
// 	function CourseApplyPay_Va(nbr){
		
// 		$.ajax({
// 			type : 'post',
// 			url  : '<c:url value="/schedules/CourseApplyPay" />?${_csrf.parameterName}=${_csrf.token}',
// 			data : $('form[name=frmPaymentInfo]').serialize(),
// 			dataType : 'json',
// 			success : function(data){
// 				if(data.result){
// 					alert('정상적으로 처리 되셨습니다.');
// 					Search();
// 				}
// 				else{
// 					alert(data.Msg);
// 					return;
// 				}
// 			}
// 		});
// 	}	
	
	//CARD
// 	function CourseApplyPay_payment(nbr){
		
// 		$.ajax({
// 			type : 'post',
// 			url  : '<c:url value="/schedules/CourseApplyPay" />?${_csrf.parameterName}=${_csrf.token}',
// 			data : $('form[name=frmPaymentInfo]').serialize(),
// 			dataType : 'json',
// 			success : function(data){
// 				if(data.result){
// 					alert('정상적으로 결제 되셨습니다.');
// 					Search();
// 				}
// 				else{
// 					alert(data.Msg);
// 					return;
// 				}
// 			}
// 		});
// 	}	

	// 수강인원 정보
	function MemberInfo(ApplyUniqueNbr,CourseCd){
		var params = 'APPLY_UNIQUE_NBR='+ApplyUniqueNbr+'&COURSE_CD='+CourseCd;
		$.ajax({
			type : 'get',
			url  : '<c:url value="/payment/MemberInfo" />?${_csrf.parameterName}=${_csrf.token}',
			data : params,
			dataType : 'json',
			success : function(data){
				StudentList(data.MemberInfo);
			}
		});
	}
	
	// 수강생 정보 리스트
	function StudentList(Obj){
		var html = '';
		
		for(var i=0; i < Obj.length;i++){
			var gender = (typeof Obj[i].GENDER_FLAG == 'undefined')?'':Obj[i].GENDER_FLAG;
			var birth  = (typeof Obj[i].BIRTH_DATE == 'undefined')?'':Obj[i].BIRTH_DATE;
			var tel	   = (typeof Obj[i].FAMILY_CEL == 'undefined')?'':Obj[i].FAMILY_CEL;
			var schoolName = (typeof Obj[i].SCHOOL_NAME == 'undefined')?'':Obj[i].SCHOOL_NAME;
					
			if(gender != ''){gender = (gender == 'M')?'남자':'여자';}
			if(birth.length == 8){birth = birth.substring(0,4)+'.'+birth.substring(4,6)+'.'+birth.substring(6);}
			if(tel.length == 11){tel = tel.substring(0,3)+'-'+tel.substring(3,7)+'-'+tel.substring(7);}
		
			html += '<tr>';
			html += '<td>'+Obj[i].NAME+'</td>';
			html += '<td>'+gender+'</td>';
			html += '<td>'+birth+'</td>';
			html += '<td>'+schoolName+'</td>';
			html += '<td>'+tel+'</td>';
			html += '<td>'+Obj[i].TARGET_NAME+'</td>';
			html += '</tr>';
		}
		
		$('#members-info').empty();
		$(html).appendTo('#members-info');
		
		$("#members-modal").modal();
	}
	
	// 영수증 정보
	function Receipe(ApplyUniqueNbr){
		$.ajax({
			type : 'get',
			url  : '<c:url value="/payment/Receipe" />?${_csrf.parameterName}=${_csrf.token}',
			data : 'APPLY_UNIQUE_NBR='+ApplyUniqueNbr,
			dataType : 'json',
			success : function(data){
				ReceipeModal(data.TranPaymentInfo[0]);
			}
		});
	}
	
	// 영수증 Modal
	function ReceipeModal(data){
		var html = '';
		var TranDate = data.RECEIPT_DATE;
		var TranTime = data.RECEIPT_TIME;
		var cardName = (typeof data.ISU_NAME == 'undefined')?'':data.ISU_NAME;
		var installNo = (typeof data.INSTALL_NO == 'undefined')?'':data.INSTALL_NO;
		var approveNo = (typeof data.APPROVE_NO == 'undefined')?'':data.APPROVE_NO;
		
		
		if(TranDate.length == 8){
			TranDate = TranDate.substring(0,4)+'/'+TranDate.substring(4,6)+'/'+TranDate.substring(6);
		}
		if(TranTime.length == 6){
			TranTime = TranTime.substring(0,2)+':'+TranTime.substring(2,4);
		}
		
		html += '<div>';
		html += '<img src=\'<c:url value="/resources/img/main_a/logo_kr.png" /> \' class="img-responsive" alt=""/ >';
		html += '</div>';
		html += '<table class="receipe-table table lined-table margin-bottom30">';
		html += '<tr>';
		html += '<th class="text-left">금액</th>';
		html += '<td class="text-right">'+Comma(data.PAYMENT_AMT)+'원</td>';
		html += '</tr>';
		html += '<tr>';
		if(data.PAYMENT_TYPE == '000002'){
			html += '<th class="text-left">부가세</th>';
			html += '<td class="text-right">'+Comma(data.VAT_AMT)+'원</td>';
		}else if(data.PAYMENT_TYPE == '000003'){
			html += '<th class="text-left">수수료</th>';
			html += '<td class="text-right">'+Comma(data.CMS_COMMISSION_AMT)+'원</td>';
		}
		html += '</tr>';
		html += '<tr>';
		html += '<th class="text-left">합계</th>';
		html += '<td class="text-right">'+Comma(Number(data.PAYMENT_AMT)+Number(data.VAT_AMT))+'원</td>';
		html += '</tr>';
		html += '</table>';
		html += '<div class="receipe-payment">';
		html += '<h4 class="teal">결제내역</h4>';
		html += '<div class="clearfix" style="padding: 10px; background-color: #f2f2f2; border: 1px solid #ddd;">';
		if(data.PAYMENT_TYPE == '000002'){
			if(data.REF_FLAG == 'Y'){
				html += '신용카드 <span class="right teal"><strong> -'+Comma(data.PAYMENT_AMT)+'원</strong></span>';
				html += '<div><span>'+cardName+' ****-****-****-****</span></div>';
			}else{
				html += '신용카드 <span class="right teal"><strong>'+Comma(Number(data.PAYMENT_AMT)+Number(data.VAT_AMT))+'원</strong></span>';
				html += '<div><span>'+cardName+' ****-****-****-****</span></div>';
			}
		}else if(data.PAYMENT_TYPE == '000003'){
			if(data.REF_FLAG == 'Y'){
				html += '가상계좌 <span class="right teal"><strong> - '+Comma(Number(data.PAYMENT_AMT)+Number(data.CMS_COMMISSION_AMT))+'원</strong></span>';
				html += '<div><span></span></div>';
			}else{
				html += '가상계좌 <span class="right teal"><strong> '+Comma(Number(data.PAYMENT_AMT)+Number(data.CMS_COMMISSION_AMT))+'원</strong></span>';
				html += '<div><span></span></div>';
			}
		}
		html += '</div>';
		html += '<table class="table lined-table margin-top30">';
		html += '<tr>';
		html += '<td width="70px">이름</td>';
		html += '<td>'+data.CUSTOMER_NAME+'</td>';
		html += '<td width="70px">일시</td>';
		html += '<td>'+TranDate+' '+TranTime+'</td>';
		html += '</tr>';
		if(data.PAYMENT_TYPE == '000002'){
			html += '<tr>';	
			html += '<td>할부</td>';
			if(installNo == '00'){
				html += '<td>일시불</td>';
			}else{
				// 현재 일시불뿐이 없다고 함
				//html += '<td>'+installNo+'개월</td>';
				html += '<td>일시불</td>';
			}
			html += '<td>승인번호</td>';
			html += '<td>'+approveNo+'</td>';
			html += '</tr>';
		}
		
		html += '</table>';
		html += '</div>';
		html += '<div class="center-infos">';
		html += '<div class="address margin-bottom30">';
		html += '13817 경기도 과천시 상하벌로 110 국립과천과학관<br>';
		html += '<strong style="margin-right: 10px">대표전화</strong> 02-3677-1500 <strong style="margin-right: 10px;margin-left: 30px">Fax</strong> 02-3677-1328';
		html += '</div>';
		html += '<h6>영수증은 타인에게 양도할 수 없습니다.</h6>';
		html += '<h6>문의사항은 고객센터를 이용해 주세요.</h6>';
		html += '</div>';
		
		$('#reservation-receipe').empty();
		$(html).appendTo('#reservation-receipe');
		
		$("#receipe-modal").modal();
		
	}
	
	//꼼마,천단위
	function Comma(n){
		var result = String(n);
		return result.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
	};
	
	// 숫자만
	function setNumber(TargetObj){
		if(TargetObj != null){
			TargetObj.value = TargetObj.value.replace(/[^0-9]/g, '');
		}
		return TargetObj;
	};
	
	// 환불 거래형태 구분
	var check = 0;
	function RefundPay(ApplyUniqueNbr){		
		if(check == 0) {
			check = 1;
			$.ajax({
				type : 'get',
				url  : '<c:url value="/payment/RefundPay" />?${_csrf.parameterName}=${_csrf.token}',
				data : 'APPLY_UNIQUE_NBR='+ApplyUniqueNbr,
				dataType : 'json',
				success : function(data){
					if(data.result){
						if(data.TranPaymentList[0].PAYMENT_TYPE == '000002'){
							if(confirm('환불신청 하시겠습니까?')){
								check = 2;
								CardRefundData(ApplyUniqueNbr);
							}
							else{
								return;
							}
						}else if(data.TranPaymentList[0].PAYMENT_TYPE == '000003'){
							RefundPage(data.CompleteInfo,data.BankList,data.TranPaymentList,data.ApplyUniqueNbr);
	 					}
					}
				}
			});
		} else if(check == 1) {
			alert("환불 중입니다.");
		}
	}
	
	// 0원 환불
	function RefundPayZero(ApplyUniqueNbr){
		var frm = document.frmProgramList;
		frm.APPLY_UNIQUE_NBR.value = ApplyUniqueNbr;
		
		if(confirm('신청을 취소하시겠습니까?')){
			$.ajax({
				type : 'get',
				url  : '<c:url value="/payment/CoureseApplyZeroCancel"/>?${_csrf.parameterName}=${_csrf.token}',
				data : $('form[name=frmProgramList]').serialize(),
				dataType : 'json',
				success : function(data){
					if(data.result){
						alert('신청취소 되었습니다.');
						Search();
					}else{
						alert(data.Msg);
						return;
					}
				}
			});
		}
	}
	
	// 환불 페이지
	function RefundPage(info,bankList,TranPaymentList,ApplyUniqueNbr){
		var frm = document.frmRefundVa;
		var infoHtml = '';
		var courseDate = '';
		var courseTime = '';
		var amt = TranPaymentList[0].PAYMENT_AMT;
		var disAmt = 500;
		var totAmt = Number(amt)-Number(disAmt);
	
		if(info.SCHEDULE_FLAG == 'Y'){
			courseDate = info.COURSE_DATE.substring(0,4)+'.'+info.COURSE_DATE.substring(4,6)+'.'+info.COURSE_DATE.substring(6);
			courseTime = info.COURSE_START_TIME.substring(0,2)+':'+info.COURSE_START_TIME.substring(2)+' ~ '+info.COURSE_END_TIME.substring(0,2)+':'+info.COURSE_END_TIME.substring(2);
		}else{
			courseDate = info.COURSE_START_DATE.substring(0,4)+'.'+info.COURSE_START_DATE.substring(4,6)+'.'+info.COURSE_START_DATE.substring(6)+' ~ '+info.COURSE_END_DATE.substring(0,4)+'.'+info.COURSE_END_DATE.substring(4,6)+'.'+info.COURSE_END_DATE.substring(6);
			courseTime = info.COURSE_START_TIME.substring(0,2)+':'+info.COURSE_START_TIME.substring(2)+' ~ '+info.COURSE_END_TIME.substring(0,2)+':'+info.COURSE_END_TIME.substring(2);
		}
		
		frm.REFUND_PAYMENT_AMT.value = totAmt;
		frm.APPLY_UNIQUE_NBR.value = ApplyUniqueNbr;
		frm.CMS_COMMISSION_AMT.value = disAmt;
		
		infoHtml += '<h5 class="doc-title">';
		infoHtml += info.COURSE_NAME+'<small> '+courseDate+' '+courseTime+'</small>';
		infoHtml += '</h5>';
		infoHtml += '<table class="table centered-table lined-table">';
		infoHtml += '<tbody>';
		infoHtml += 	'<tr>';
		infoHtml += 		'<td class="left">';
		infoHtml += 			'<strong>총 결제금액</strong><br>';
		infoHtml += 			'프로그램결제 금액 '+Comma(amt)+'원';
		infoHtml += 		'</td>';
		infoHtml += 		'<td class="right">'+Comma(amt)+'원</td>';
		infoHtml += 	'</tr>';
		infoHtml += 	'<tr>';
		infoHtml += 		'<td class="left">';
		infoHtml += 			'<strong>공제금액</strong><br>';
		infoHtml += 			'송금수수료 '+disAmt+'원';
		infoHtml += 		'</td>';
		infoHtml += 		'<td class="right">-'+disAmt+'원</td>';
		infoHtml += 	'</tr>';
		infoHtml += 	'<tr>';
		infoHtml += 		'<td class="left">';
		infoHtml += 			'<strong>환불예정금액</strong><br>';
		infoHtml += 			Comma(amt)+'원 -'+disAmt+' 원';
		infoHtml += 		'</td>';
		infoHtml += 		'<td class="right">'+Comma(totAmt)+'원</td>';
		infoHtml += 	'</tr>';
		infoHtml += '</tbody>';
		infoHtml += '</table>';
		
		infoHtml += '<div>';
		infoHtml += '<h6 class="doc-title">선택한 내역은 <span class="teal">가상계좌</span>(으)로 결제하였습니다. <small>공제금액 제외 후 환불됩니다.</small></h6>';
		infoHtml += '<h6 class="doc-title"><strong>결제정보입력</strong> <small>환불계좌 정보를 입력하세요.</small></h6>';
		infoHtml += '</div>';
		infoHtml += '<div class="form-group">';
		infoHtml += '<label>예금주명</label>';
		infoHtml += '<input type="text" class="form-control" name="REF_ACCOUNT_OWNER">';
		infoHtml += '</div>';
		infoHtml += '<div class="form-group">';
		infoHtml += '<label>생년월일</label>';
		infoHtml += '<input type="text" class="form-control" onblur="setNumber(this)" onkeyup="setNumber(this)" name="REF_ACCOUNT_REG_NO" maxlength="6">';
		infoHtml += '<p class="help-block">(예금주 법정 생년월일 6자리)</p>';
		infoHtml += '</div>';
		infoHtml += '<div class="form-group">';
		infoHtml += '<label>은행명</label>';
		infoHtml += '<select class="form-control" name="REF_BANK_CD">';
		infoHtml += '<option value="">은행선택</option>';
		for(var i = 0; i < bankList.length;i++ ){
			infoHtml += '<option value="'+bankList[i].COMMON_CD+'">'+bankList[i].COMMON_NAME+'</option>';
		}
		infoHtml += '</select>';
		infoHtml += '</div>';
		infoHtml += '<div class="form-group">';
		infoHtml += '<label>계좌번호</label>';
		infoHtml += '<input type="text" class="form-control" name="REF_ACCOUNT_NO" onblur="setNumber(this)" onkeyup="setNumber(this)" maxlength="20">';
		infoHtml += '<p class="help-block">(예금주 계좌번호)</p>';
		infoHtml += '</div>';
		infoHtml += '<div class="form-group">';
		infoHtml += '<label>전화번호</label>';
		infoHtml += '<input type="text" class="form-control">';
		infoHtml += '<p class="help-block">(현금 영수증 재 승인용)</p>';
		infoHtml += '</div>';
		infoHtml += '<ul>';
		infoHtml += '<li>예금주와 계좌번호가 동일하지 않을 경우 환불처리가 지연될 수 있습니다.</li>';
		infoHtml += '<li>가상계좌 예약은 취소시 송금수수료가 발생합니다.</li>';
		infoHtml += '</ul>';
		
		$('#refundPage').empty();
		$(infoHtml).appendTo('#refundPage');
		
		$("#refund-modal").modal();
	}
	
	//카드 환불 데이터
	function CardRefundData(ApplyUniqueNbr){
		$.ajax({
			type : 'get',
			url  : '<c:url value="/payment/CardRedfundData" />?${_csrf.parameterName}=${_csrf.token}',
			data : 'APPLY_UNIQUE_NBR='+ApplyUniqueNbr,
			dataType : 'json',
			success : function(data){
				CardRefundDataInsert(data.PaymentInfo, ApplyUniqueNbr);
			}
		});
	}
	
	// 카드 환불 창 띄우기
	function CardRefundDataInsert(Obj, ApplyUniqueNbr){
		var frm = document.CancelForm;
		console.log(Obj);
		frm.mbrId.value = Obj[0].MBR_NO;
		frm.cardTradeNo.value = Obj[0].C2S_UNIQUE_NBR;
		frm.cardApprovDate.value = Obj[0].APPROVE_DATE.substring(2);
		frm.salesPrice.value = Obj[0].PAY_SALE_AMT;
		frm.payType.value = Obj[0].PAY_SVC_TYPE;
		frm.buyerName.value = Obj[0].CUSTOMER_NAME;
		frm.buyerMobile.value = Obj[0].CUSTOMER_TEL;
		frm.productName.value = Obj[0].PRODUCT_NAME;
		ProcJsonp(ApplyUniqueNbr);
	}
	
	function ProcJsonp(ApplyUniqueNbr){
		var param = $('form[name=CancelForm]').serialize();
		$.ajax({
			type : 'get',
			url  : '<c:url value="/payment/CourseCardRefundJson" />?${_csrf.parameterName}=${_csrf.token}',
			data : param,
			dataType : 'json',
			success : function(data){
				if(data.result == '0000'){
					CourseApplyCardRefund(ApplyUniqueNbr);
				}
				else{
					alert(data.Msg);
					check = 0;
					return;
				}
			}
		});
	}
	
	//카드 환불
	function CourseApplyCardRefund(ApplyUniqueNbr){
		var frm = document.frmProgramList;
		frm.APPLY_UNIQUE_NBR.value = ApplyUniqueNbr;
		
		$.ajax({
			type : 'get',
			url  : '<c:url value="/payment/CourseApplyCardRefund" />?${_csrf.parameterName}=${_csrf.token}',
			data : $('form[name=frmProgramList]').serialize(),
			dataType : 'json',
			success : function(data){
				if(data.result){
					alert('환불 처리 되었습니다.');
					check = 0;
					Search();
				}else{
					alert(data.Msg);
					check = 0;
					return;
				}
			}
		});
	}

	// 예약증 페이지
	function ReceiptPay(ApplyUniqueNbr){
		var html ='';
		
		$.ajax({
			type : 'get',
			url  : '<c:url value="/payment/ReceiptPay" />?${_csrf.parameterName}=${_csrf.token}',
			data : 'APPLY_UNIQUE_NBR='+ApplyUniqueNbr,
			dataType : 'json',
			success : function(data){
				$("#receipt-modal").modal();
				
				var schedule = data.CompleteInfo.SCHEDULE_FLAG;
				var CourseDay ='';
				var CourseStartTime = data.CompleteInfo.COURSE_START_TIME.substring(0,2)+':'+data.CompleteInfo.COURSE_START_TIME.substring(2);
				var CourseEndTime = data.CompleteInfo.COURSE_END_TIME.substring(0,2)+':'+data.CompleteInfo.COURSE_END_TIME.substring(2);
				var ReserDate = data.CompleteInfo.RESER_DATE;
				var ReserTime = data.CompleteInfo.RESER_TIME;
				
				if(schedule == 'Y'){
					if(data.CompleteInfo.COURSE_DATE == null || data.CompleteInfo.COURSE_DATE == "" || !data.CompleteInfo.COURSE_DATE.length == 8){
						CourseDay = data.CompleteInfo.COURSE_DATE;
					}else{
						CourseDay = data.CompleteInfo.COURSE_DATE.substring(0,4)+'년 '+data.CompleteInfo.COURSE_DATE.substring(4,6)+'월 '+data.CompleteInfo.COURSE_DATE.substring(6)+'일';
					}
				}else{
					if(data.CompleteInfo.COURSE_START_DATE.length == 8){
						CourseDay = data.CompleteInfo.COURSE_START_DATE.substring(0,4)+'년 '+data.CompleteInfo.COURSE_START_DATE.substring(4,6)+'월 '+data.CompleteInfo.COURSE_START_DATE.substring(6)+'일'+
									' ~ '+data.CompleteInfo.COURSE_END_DATE.substring(0,4)+'년 '+data.CompleteInfo.COURSE_END_DATE.substring(4,6)+'월 '+data.CompleteInfo.COURSE_END_DATE.substring(6)+'일';
					}else{
						CourseDay = data.CompleteInfo.COURSE_START_DATE+'~'+data.CompleteInfo.COURSE_END_DATE;
					}
				}
				
				ReserDate = ReserDate.substring(0,4)+'년 '+ReserDate.substring(4,6)+'월 '+ReserDate.substring(6)+'일';
				ReserTime = ReserTime.substring(0,2)+':'+ReserDate.substring(2,4);
				
				html += '<form name="frmPaymentReceiptPrint">';
				html += '<input type="hidden" name="APPLY_UNIQUE_NBR" value="'+ApplyUniqueNbr+'" />';
				html += '</form>';
				html += '<div style="float:right; width="300px";>';
				html += '<img src=\'<c:url value="/resources/img/main_a/logo_kr.png" /> \' class="img-responsive" alt=""/ >';
				html += '</div>';
				html += '<div class="receipe-header">';
				html += '<h4 class="doc-title">예약증</h4>';
				html += '</div>';
				html += '<div class="receipe-body margin-bottom20">';
				html += '<h2 class="doc-title">'+data.CompleteInfo.COURSE_NAME+'</h2>';
				html += '<h5 class="doc-title">'+CourseDay+' '+data.CompleteInfo.COURSE_WEEK_STR +'</h5>';
				html += '<h5 class="doc-title">'+data.CompleteInfo.PRICE_NAME+' '+CourseStartTime+' ~ '+CourseEndTime;
				html += '</h5>';
				html += '<h5 class="doc-title">'+data.CompleteInfo.PLACE_INFO+'</h5>';
				html += '</div>';
				
				html += '<div class="receipe-info">';
				html += '<div class="table-responsive">';
				html += '<table class="table lined-table">';
				html += '<tr>';
				html += '<th>예약구분</th>';
				html += '<td>'+data.CompleteInfo.CLASS_NAME+'</td>';
				html += '<th>예약코드</th>';
				html += '<td>'+data.CompleteInfo.APPLY_UNIQUE_NBR+'</td>';
				html += '</tr>';
				html += '<tr>';
				
				html += '<th>예약일시</th>';
				html += '<td>'+ReserDate+' '+ReserTime+'</td>';
				html += '<th>예약상태</th>';
				html += '<td>'+data.CompleteInfo.APPLY_STATUS+'</td>';
				html += '</tr>';
				html += '<tr>';
				html += '<th>예약자명(수강생)</th>';
				html += '<td>'+data.CompleteInfo.STUDENT_NAME+'</br>('+data.CompleteInfo.STUDENT_NAME2+')</td>';
				html += '<th>연락처</th>';
				html += '<td>';
				html += (typeof data.CompleteInfo.MEMBER_CEL == 'undefined')?"":data.CompleteInfo.MEMBER_CEL;
				html += '</td>';
				html += '</tr>';
				html += '<tr>';
				html += '<th>관람인원</th>';
				html += '<td>'+data.CompleteInfo.APPLY_CNT+'명</td>';
				html += '<th>결제금액</th>';
				html += '<td>'+Comma(data.CompleteInfo.TOTAL_PRICE)+'원</td>';
				html += '</tr>';
				
				$('#reservation-receipt').empty();
				$(html).appendTo('#reservation-receipt');
			}
		});		
	}
	
	// 조회
	function Search(){
		var frm = document.frmSearch;
		frm.pageNum.value = "1";
		frm.action = '<c:url value="/payment/Search"/>?${_csrf.parameterName}=${_csrf.token}';
		frm.method = 'get';
		frm.target = '_self';
		frm.submit();
	}
	
	// 페이징
	function fn_page(num){
		var frm = document.frmSearch;
		frm.pageNum.value = num;
		frm.action = '<c:url value="/payment/Search"/>?${_csrf.parameterName}=${_csrf.token}';
		frm.method = 'get';
		frm.target = '_self';
		frm.submit();
	}
	
	// 예약 취소
	function CourseApplyCancel(ApplyUniqueNbr){
		var frm = document.frmProgramList;
		frm.APPLY_UNIQUE_NBR.value = ApplyUniqueNbr;
		
		if(confirm('예약을 취소하시겠습니까?')){
			$.ajax({
				type : 'get',
				url  : '<c:url value="/payment/CoureseApplyCancel"/>?${_csrf.parameterName}=${_csrf.token}',
				data : $('form[name=frmProgramList]').serialize(),
				dataType : 'json',
				success : function(data){
					if(data.result){
						alert('예약이 취소 되었습니다.');
						document.location.href = '<c:url value="/payment/"/>';
					}else{
						alert(data.Msg);
						return;
					}
				}
			});
		}
	}
	
	//강좌 수료증 발급
	function getComplete(ApplyUniqueNbr, AcademyCd){
		var frm = document.CompleteForm;
		$("#CompleteApplyUniqueNbr").val(ApplyUniqueNbr);
		$("#CompleteAcademyCd").val(AcademyCd);
		frm.action = '<c:url value="/payment/getCompleteLecture"/>';
		frm.method = 'get';
		frm.submit();
	}
	
	

	
	
	
	
	
	
	
	
	
	
	//2. 4단계 결제진행 마지막 단계 ([결제하기] 버튼이 있다면 여기서부터 시작)
	//ApplyUniqueNbr:예약한 고유의 값
	//TotalPrice:전체가격
	//ApplyCnt:수량
	//CourseName:상품명
	function CourseApplyPay( ApplyUniqueNbr, TotalPrice, ApplyCnt, CourseName, mbrId, mbrName, ClassCd){
		var mallForm = document.mallForm;
		
		mallForm.APPLY_UNIQUE_NBR.value = ApplyUniqueNbr;
		mallForm.TOTAL_PRICE.value 		= TotalPrice;
		mallForm.APPLY_CNT.value 		= ApplyCnt;
		mallForm.PAYMENT_AMT.value 		= TotalPrice;
		mallForm.COURSE_NAME.value 		= CourseName;
		
		mallForm.productName.value  = CourseName;
		mallForm.productPrice.value = TotalPrice;
		mallForm.productCount.value = ApplyCnt;
		mallForm.salesPrice.value   = TotalPrice;
		mallForm.oid.value   		= ApplyUniqueNbr;
		mallForm.mbrId.value   		= mbrId;
		mallForm.mbrName.value   	= mbrName;
		mallForm.returnUrl.value   	= "${fn:replace(c2_homepage_url,'https','http')}/payresponse/CourseApplyPay/";
		
		$("#payment-choice").modal();
		$("#button_payType2").hide();
		//if(ClassCd == "CL6003")	$("#button_payType2").show();
		//else					$("#button_payType2").hide();
	}


	//5. 리턴 URL에서 결과 저장

	//7. 콜백함수에서 완료페이지 호출
	function CompleteProc(oid){
		var frm = document.frmProgramList;
		frm.APPLY_UNIQUE_NBR.value = document.mallForm.APPLY_UNIQUE_NBR.value;
		frm.action = '<c:url value="/schedules/CompleteProc"/>?${_csrf.parameterName}=${_csrf.token}';
		frm.method = 'post';
		frm.submit();
	}
	
// 	var popUrl = "/sciencecenter/resources/payment1.3/paymentCallBack_test.jsp";	//팝업창에 출력될 페이지 URL
// 	var popOption = "width=370, height=360, resizable=no, scrollbars=no, status=no;";    //팝업창 옵션(optoin)
// 		window.open(popUrl,"",popOption);

	
</script>

	<script type="text/javascript">
		$('.dropdown-toggle').dropdown();
		$("#logout-btn").click(function(){
			$("#logout-form").submit();
		});
		
		$(".main-nav-item > a").click(function(e){
			e.preventDefault();
			
			$(this).find(".main-sub-nav").show();
		});
	</script>

	<script type="text/javascript">
		function ajaxLoading(){
			$("body").css("opacity", "0.5");
			$("#ajaxLoader").show();
		}

		function ajaxUnLoading(){
			$("body").css("opacity", "1");
			$("#ajaxLoader").hide();
		}
	</script>

	<script type="text/javascript">
		$(function(){
			$('body').scrollspy({ 
				target: '#scrollspy-nav',
				offset: 230
			});

			$("#scrollspy-nav.has-scroll a.item").click(function(e){
				e.preventDefault();

				var target = this.hash;
				var $target = $(target);

				$('html, body').stop().animate({
					'scrollTop': $target.offset().top - 150
				}, 500, 'swing', function () {
					window.location.hash = target;
				});
			});


			$('.dropdown-button').dropdown({
				inDuration: 300,
				outDuration: 225,
						    constrain_width: false, // Does not change width of dropdown to that of the activator
						    gutter: 0, // Spacing from edge
						    belowOrigin: false // Displays dropdown below the button
						  }
						  );
		});
	</script>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-66853433-1', 'auto');
		ga('send', 'pageview');
	</script>