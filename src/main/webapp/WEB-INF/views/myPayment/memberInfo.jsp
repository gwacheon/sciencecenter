<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix='fn' uri='http://java.sun.com/jsp/jstl/functions'%>

<div id="members-modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">예약자 목록</h4>
      </div>
      <div class="modal-body">
      	<div class="table-responsive">
	        <table class="table centered-table">
	        	<thead>
					<tr>
						<th class="center">이름</th>
						<th class="center">성별</th>
						<th class="center">생년월일</th>
						<th class="center">학교</th>
						<th class="center">연락처</th>
						<th class="center">대상</th>
					</tr>
				</thead>

				<tbody id="members-info">
				</tbody>
	        </table>
        </div>
      </div>

      <div class="modal-footer">
				<button type="button" class="btn btn-teal" data-dismiss="modal">확인</button>
			</div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->	