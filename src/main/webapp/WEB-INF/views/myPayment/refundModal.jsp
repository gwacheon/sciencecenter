<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

	<!-- Modal -->
	<div class="modal fade" id="refund-modal">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">환불하기</h4>
	      </div>
	      
	      <form name="frmRefundVa">
	      	<input type="hidden" name="REFUND_PAYMENT_TYPE" value="000003">
	      	<input type="hidden" name="REFUND_PAYMENT_AMT" value="" />
	      	<input type="hidden" name="CMS_COMMISSION_AMT" value="" />
	      	<input type="hidden" name="SALE_TYPE" value="${SALE_TYPE }" />
	      	<input type="hidden" name="APPLY_UNIQUE_NBR" value="" />
	      	
		      <div class="modal-body" id="refundPage">
		        
		      </div>
	      </form>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-teal reverse" data-dismiss="modal">취소</button>
	        <button type="button" class="btn btn-primary" onclick="RefundVa();">환불 신청하기</button>
	      </div>
	    </div>
	  </div>
	</div>

<script type="text/javascript">
	
	function RefundVa(){
		var frm = document.frmRefundVa;
		if(VaValidation()){
			$.ajax({
				type : 'get',
				url  : '<c:url value="/payment/CourseApplyRefundVa"/>?${_csrf.parameterName}=${_csrf.token}',
				data : $('form[name=frmRefundVa]').serialize(),
				dataType :'json',
				success : function(data){
					if(data.result){
						alert('정상적으로 처리 되었습니다.');
						Search();
					}
					else{
						alert(data.Msg);
						return;
					}
				}
			});
		}
	}
	
	function VaValidation(){
		var frm = document.frmRefundVa;
		
		if(frm.REF_ACCOUNT_OWNER.value == ''){
			alert('예금주명을 입력해 주세요');
			frm.REF_ACCOUNT_OWNER.focus();
			return;
		}else if(frm.REF_ACCOUNT_REG_NO.value == ''){
			alert('생년월일을 입력해 주세요');
			frm.REF_ACCOUNT_REG_NO.focus();
			return;
		}else if(frm.REF_BANK_CD.value ==''){
			alert('은행명을 선택해 주세요');
			frm.REF_BANK_CD.focus();
			return;
		}else if(frm.REF_ACCOUNT_NO.value ==''){
			alert('예금주 계좌번호를 입력해 주세요');
			frm.REF_ACCOUNT_NO.focus();
			return;
		}else{
			return true;
		}
	}
</script>