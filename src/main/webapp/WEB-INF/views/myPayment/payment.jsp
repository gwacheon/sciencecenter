<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix='fn' uri='http://java.sun.com/jsp/jstl/functions'%>

<div class="modal fade" id="payment-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">결제하기</h4>
	   </div>
	   <div class="modal-body process-wrapper clearfix">
	   	<div class="process-contents">
			<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<ul class="nav nav-tabs payment-tabs" role="tablist">
					    <li role="presentation" onclick="TypeChange('1')" class="active col-xs-6"><a href="#payment-card" aria-controls="home" role="tab" data-toggle="tab">카드결제</a></li>
					    <li role="presentation" onclick="TypeChange('2')" class="col-xs-6"><a href="#payment-account" aria-controls="home" role="tab" data-toggle="tab">가상계좌</a></li>
					  </ul>
					</div>
			</div>
				
			<div class="row">
				<div class="col-md-12">
					<div class="tab-content">
				    <div role="tabpanel" class="tab-pane active" id="payment-card">
				    
				    	<form name="frmCard" class="form-horizontal margin-top20 margin-bottom20">
					    	<input type="hidden" name="APPLY_UNIQUE_NBR" value="" />
							<input type="hidden" name="PAYMENT_TYPE" value="000002" />
							<input type="hidden" name="TOTAL_PRICE" value=""/>
							<input type="hidden" name="PAYMENT_AMT" value="" />
							<input type="hidden" name="CARD_CERT_GB" value="1" />
							 
				    		<div class="form-group">
							    <label  class="col-sm-2 control-label">
							    	카드구분
						    	</label>
							    <div class="col-sm-6">
							      <select class="form-control" name="CardType" onchange="CardTypeChange(this)">
							      	<option value="1">개인</option>
							      	<option value="2">사업자</option>
							      </select>
							    </div>
						    </div>
						    
							<div class="form-group" id="payBirth">
							  	<label  class="col-sm-2 control-label">생년월일</label>
							    <div class="col-xs-12 col-sm-8">
							    	<input type="text" class="form-control" name="CARD_SOCID" id="CARD_SOCID" maxlength="6" placeholder="(예:880101)" onblur="setNumber(this)" onkeyup="setNumber(this)"/>
							    </div>
							    
							    <div class="col-xs-12 col-sm-8" style="display:none">
							    	<input type="text" class="form-control" name="CARD_BUSINESS_ID" maxlength="16" id="CARD_BUSINESS_ID" placeholder="(예:1234567890)" onblur="setNumber(this)" onkeyup="setNumber(this)"/>
							    </div>
							</div>
							
							<div class="form-group" id="payCompany" style="display:none">
							  	<label  class="col-sm-2 control-label">사업자번호</label>
							    <div class="col-xs-12 col-sm-8" >
							    	<input type="text" class="form-control" name="CARD_BUSINESS_ID" maxlength="16" id="CARD_BUSINESS_ID" placeholder="(예:1234567890)" onblur="setNumber(this)" onkeyup="setNumber(this)"/>
							    </div>
							</div>
							
							<div class="form-group">
							    <label  class="col-sm-2 control-label">
							    	할부선택
						    	</label>
							    <div class="col-sm-6">
							      <select class="form-control" name="CARD_INSTALL_NO">
							      	<option value="00" selected="selected">일시불</option>
							        <c:forEach var="vo" begin="2" end="36">
							            <c:if test="${vo lt 10}">
							            	<option value="0${vo}">${vo}개월</option>
							            </c:if>
							          	<c:if test="${vo gt 9}">
							            	<option value="${vo}">${vo}개월</option>
							            </c:if>
							        </c:forEach>
							      </select>
							    </div>
							</div>
							
							<div class="form-group">
							    <label  class="col-sm-2 control-label">카드번호</label>
							    <div class="col-xs-3 col-sm-2">
							      <input type="text" class="form-control"
							      	maxlength="4" name="CARD_TRACK_DATA_1" onblur="setNumber(this)" onkeyup="setNumber(this)">
							    </div>
							    <div class="col-xs-3 col-sm-2">
							      <input type="text" class="form-control"
							      	maxlength="4" name="CARD_TRACK_DATA_2" onblur="setNumber(this)" onkeyup="setNumber(this)">
							    </div>
							    <div class="col-xs-3 col-sm-2">
							      <input type="password" class="form-control"
							      	maxlength="4" name="CARD_TRACK_DATA_3" onblur="setNumber(this)" onkeyup="setNumber(this)">
							    </div>
							    <div class="col-xs-3 col-sm-2">
							      <input type="text" class="form-control"
							      	maxlength="4" name="CARD_TRACK_DATA_4" onblur="setNumber(this)" onkeyup="setNumber(this)">
							    </div>
							</div>
							
							<div class="form-group">
							    <label  class="col-sm-2 control-label">
							    	유효기간
						    	</label>
							    <div class="col-xs-2 col-sm1">
							      <input type="text" class="form-control" id="" placeholder="YYYY"
							      	maxlength="2" name="CARD_EXPD_YY" onblur="setNumber(this)" onkeyup="setNumber(this)">
							    </div>
							    <div class="col-xs-2 col-sm1">
							      <input type="text" class="form-control" id="" placeholder="MM"
							      	maxlength="2" name="CARD_EXPD_MM" onblur="setNumber(this)" onkeyup="setNumber(this)">
							    </div>
							</div>
							
							<div class="form-group">
							    <label  class="col-sm-2 control-label">비밀번호</label>
							    <div class="col-sm-2">
							      <input type="password" class="form-control"
							      	maxlength="2" onblur="setNumber(this)" onkeyup="setNumber(this)" name="CARD_PASSWD">
							    </div>
							    <div class="col-sm-2">
							    	앞 두자리
							    </div>
							</div>
							  <div class="line gray margin-top20 margin-bottom20"></div>
						  </form>
				    </div>
				    
				    
				    <div role="tabpanel" class="tab-pane" id="payment-account">
				    
				    	<form name="frmVat" class="form-horizontal margin-top20 margin-bottom20">
				    		<input type="hidden" name="APPLY_UNIQUE_NBR" value="" />
							<input type="hidden" name="PAYMENT_TYPE" value="000003" />
							<input type="hidden" name="TOTAL_PRICE" value=""/>
							<input type="hidden" name="PAYMENT_AMT" value="" />
							
				    		<div class="form-group">
							    <label  class="col-sm-2 control-label">
							    	입금은행
						    	</label>
							    <div class="col-sm-6">
							      <select class="form-control" name="VA_BANK_CD" id="VA_BANK_CD" title="가상계좌은행">
							      	  <c:forEach var="vo" items="${VatList}">
							            <option value="<c:out value='${vo.commonCd}'/>"><c:out value='${vo.commonName}'/></option>
							          </c:forEach>
							      </select>
							    </div>
							  </div>
							  <div class="form-group">
							  	<label  class="col-sm-2 control-label">
							    	예금주
						    	</label>
						    	<div class="col-sm-10">
						    		(주)씨스퀘어 소프트
						    	</div>
							  </div>
							  <div class="form-group">
							    <label  class="col-sm-2 control-label">입금자명</label>
							    <div class="col-sm-6">
							      <input type="text" class="form-control">
							    </div>
							  </div>
							  <div class="form-group">
							    <label  class="col-sm-2 control-label">휴대폰번호</label>
							    <div class="col-xs-3">
							      <select class="form-control"  name="VA_MEMBER_TEL1">
							      	<option value="010">010</option>
							      	<option value="011">011</option>
							      	<option value="016">016</option>
							      	<option value="017">017</option>
							      </select>
							    </div>
							    <div class="col-xs-3">
							      <input type="text" name="VA_MEMBER_TEL2" class="form-control">
							    </div>
							    <div class="col-xs-4">
							      <input type="text" name="VA_MEMBER_TEL3" class="form-control">
							    </div>
							  </div>
							  
							  <div class="form-group">
							    <label  class="col-sm-2 control-label">현금영수증</label>
							    <div class="col-xs-3 col-sm-2">	
								  	<div class="radio radio-purple">
											<input name="receipe" class="receipe-type" type="radio" id="receipe-type1" value="P" onclick="CashReceipt(this)">
											<label for="receipe-type1">소득공제용</label>
										</div>
							  	</div>

							  	<div class="col-xs-3 col-sm-2">	
								  	<div class="radio radio-purple">
											<input name="receipe" class="receipe-type" type="radio" id="receipe-type2" value="G" onclick="CashReceipt(this)">
											<label for="receipe-type2">지출증빙용</label>
										</div>
							  	</div>

							  	<div class="col-xs-3 col-sm-2">	
								  	<div class="radio radio-purple">
											<input name="receipe" class="receipe-type" type="radio" id="receipe-type3" value="" onclick="CashReceipt(this)">
											<label for="receipe-type3">발급안함</label>
										</div>
							  	</div>
							  </div>

							  <div id="receipe-type-option-wrapper" class="form-group" style="display: none">
							  	<div class="col-sm-10 col-sm-offset-2">
							  		<div class="row">
							  			<div id="receipe-type-options">
							  				<div class="col-sm-9 col-xs-8">
							  					<input class="form-control" id="CASH_TRACK_DATA_KEYIN" onblur="setNumber(this)" onkeyup="setNumber(this)" name="CASH_TRACK_DATA_KEYIN" type="text" id="receipe-type-phone" placeholder="휴대폰번호(10자리 또는 11자리)">

							  				</div>
							  			</div>
							  		</div>
							  	</div>
							  </div>

							  <div class="line gray margin-top20 margin-bottom20"></div>
						  </form>
				    </div>
				  </div>

				  <div class="row">	
				  	<div class="col-xs-2">	
					  	<div class="checkbox checkbox-purple">
								<input name="payment-agreement" class="styled payment-agreement" type="checkbox" id="type1">
								<label for="type1">동의</label>
							</div>
				  	</div>

				  	<div class="col-xs-10">
				  		<div class="panel-group" id="accordion">
							  <div class="panel panel-default purple">
							    <div class="panel-heading" role="tab" id="headingOne">
							      <h4 class="panel-title">
							        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="collapsed">
							          전자금융거래 이용약관
							        </a>
							      </h4>
							    </div>
							    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
							      <div class="panel-body">전자금융거래 이용약관 내용</div>
							    </div>
							  </div>
					  	</div>
				  	</div>
				  </div>

				  <div class="row">	
				  	<div class="col-xs-2">	
					  	<div class="checkbox checkbox-purple">
								<input name="payment-agreement" class="styled payment-agreement" type="checkbox" id="type2">
								<label for="type2">동의</label>
							</div>
				  	</div>

				  	<div class="col-xs-10">
				  		<div class="panel-group" id="accordion0">
							  <div class="panel panel-default purple">
							    <div class="panel-heading" role="tab" id="headingOne">
							      <h4 class="panel-title">
							        <a role="button" data-toggle="collapse" data-parent="#accordion0" href="#accordion0-1" class="collapsed">
							          고유식별정보 수집 및 이용안내
							        </a>
							      </h4>
							    </div>
							    <div id="accordion0-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
							      <div class="panel-body">고유식별정보 수집 및 이용안내</div>
							    </div>
							  </div>
					  	</div>
				  	</div>
				  </div>

				  <div class="row">	
				  	<div class="col-xs-2">	
					  	<div class="checkbox checkbox-purple">
								<input name="payment-agreement" class="styled payment-agreement" type="checkbox" id="type3">
								<label for="type3">동의</label>
							</div>
				  	</div>

				  	<div class="col-xs-10">
				  		<div class="panel-group" id="accordion1">
							  <div class="panel panel-default purple">
							    <div class="panel-heading" role="tab" id="headingOne">
							      <h4 class="panel-title">
							        <a role="button" data-toggle="collapse" data-parent="#accordion1" href="#accordion1-1" class="collapsed">
							          개인정보 수집 및 이용안내
							        </a>
							      </h4>
							    </div>
							    <div id="accordion1-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
							      <div class="panel-body">개인정보 수집 및 이용안내</div>
							    </div>
							  </div>
					  	</div>
				  	</div>
				  </div>

				  <div class="row">	
				  	<div class="col-xs-2">
					  	<div class="checkbox checkbox-purple">
								<input name="payment-agreement" class="styled payment-agreement" type="checkbox" id="type4">
								<label for="type4">동의</label>
							</div>
				  	</div>

				  	<div class="col-xs-10">
				  		<div class="panel-group" id="accordion2">
							  <div class="panel panel-default purple">
							    <div class="panel-heading" role="tab" id="headingOne">
							      <h4 class="panel-title">
							        <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#accordion2-1" class="collapsed">
							          개인정보 제공 및 위탁안내
							        </a>
							      </h4>
							    </div>
							    <div id="accordion2-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
							      <div class="panel-body">개인정보 제공 및 위탁안내 이용안내</div>
							    </div>
							  </div>
					  	</div>
				  	</div>
				  </div>

				  <div class="row">	
				  	<div class="col-xs-12">
				  		<div class="checkbox checkbox-purple">
								<input name="payment-agreement" class="styled payment-agreement" type="checkbox" id="all-payment-agree" onclick="CheckAll()">
								<label for="all-payment-agree">전체 동의</label>
							</div>
				  	</div>
			  	</div>

			  	<div class="row margin-bottom30">	
			  		<div class="col-sm-12 center">
			  			<a id="payment-btn" href="javascript:void(0)" class="btn btn-purple" onclick="CheckPay()">
			  				결제하기
			  			</a>
			  		</div>
			  	</div>
				</div>
			</div>
			</div>
			</div>
		</div>
	</div>
</div>

<form name="frmPayment">
	<input type="hidden" name="PAYMENT_TYPE" id="PAYMENT_TYPE" value="000002" />
</form>
<script type="text/javascript">

	function CardTypeChange(type){
		var frm = document.frmCard;
		console.log(type.value);
		if(type.value == '1'){
			frm.CARD_CERT_GB.value = '1';
			$('#payBirth').attr('style','display:');
			$('#payCompany').attr('style','display:none');
		}else{
			frm.CARD_CERT_GB.value = '2';
			$('#payBirth').attr('style','display:none');
			$('#payCompany').attr('style','display:');
		}
	}
	
	function CashReceipt(Obj){
		var val = Obj.value;
		
		if(val == 'P'){
			$('#receipe-type-option-wrapper').show();
			$('#CASH_TRACK_DATA_KEYIN').attr('placeholder','휴대폰번호(10자리 또는 11자리)');
		}else if(val == 'G'){
			$('#receipe-type-option-wrapper').show();
			$('#CASH_TRACK_DATA_KEYIN').attr('placeholder','사업자등록번호 10자리');
		}else{
			$('#receipe-type-option-wrapper').hide();
		}
	}
	
	// 전체 동의
	function CheckAll(){
		var checkAll = document.getElementsByName("payment-agreement");
		var checkYn = document.getElementById('all-payment-agree').checked;
		
		if(checkYn == true){
			for(var i=0; i < checkAll.length;i++){
				checkAll[i].checked = true;
			}	
		}else{
			for(var i=0; i < checkAll.length;i++){
				checkAll[i].checked = false;
			}
		}		
	}

	//결제 진행
	function CheckPay(){
		var Type = document.getElementById('PAYMENT_TYPE').value;
		var PaymentAmt = document.frmCard.TOTAL_PRICE.value;
		var params = '';
	
		if(Type == '000002'){
			document.frmCard.PAYMENT_AMT.value = PaymentAmt;
			params = $('form[name=frmCard]').serialize();
		}else{
			document.frmVat.PAYMENT_AMT.value = PaymentAmt;
			params = $('form[name=frmVat]').serialize();
		}
				
 		if(Valid(Type)){
			$.ajax({
				type : 'get',
				url  : '<c:url value="/payment/CourseApplyPay" />',
				data : params,
				dataType : 'json',
				beforeSend : function(){
					$('#payment-btn').attr('onclick',"");
				}
				,complete : function(){
					$('#payment-btn').attr('onclick',"CheckPay()");
				},
				success : function(data){
					if(data.result){
						$('.close').trigger('click');
						Search();
					}else{
						alert('결제 중 오류가 발생했습니다.');
						return;
					}
				}
			});
 		}
	}
	
	function Valid(Type){
		var PaymentType = Type;
		var frm;
		var bl = true;
		
		if(PaymentType == '000002'){
			frm = document.frmCard;
			
			if(frm.CARD_INSTALL_NO.value == ''){
				alert('할부를 선택해 주세요')
				bl = false;
				return;
			}else if(frm.CARD_TRACK_DATA_1.value == '' || frm.CARD_TRACK_DATA_2.value == '' || frm.CARD_TRACK_DATA_3.value =='' || frm.CARD_TRACK_DATA_4.value == ''){
				alert('카드번호를 입력하셔야 합니다.');
				bl = false;
				return;
			}else if(frm.CARD_EXPD_YY.value == '' || frm.CARD_EXPD_MM.value == ''){
				alert('유효기간을 입력하셔야 합니다.');
				bl = false;
				return;
			}else if(frm.CARD_PASSWD.value == ''){
				alert('비밀번호를 입력해 주세요');
				return;
			}else if(document.getElementById('type1').checked == false){
				alert('전자금융거래 이용약관에 동의 하셔야 합니다.');
				bl = false;
				return
			}else if(document.getElementById('type2').checked == false){
				alert('고유식별정보 수집 및 이용안내에 동의 하셔야 합니다.');
				bl = false;
				return
			}else if(document.getElementById('type3').checked == false){
				alert('개인정보 수집 및 이용안내에 동의 하셔야 합니다.');
				bl = false;
				return
			}else if(document.getElementById('type4').checked == false){
				alert('개인정보 제공 및 위탁안내에 동의 하셔야 합니다.');
				bl = false;
				return
			}
			
		}else{
			frm = document.frmVat;
		}
		
		return bl;
	}

	// 카드 / 가상계좌
	function TypeChange(num){
		var frm = document.frmPayment;
		
		if(num == '1'){
			document.getElementById('PAYMENT_TYPE').value = '000002';	
		}else{
			document.getElementById('PAYMENT_TYPE').value = '000003';
		}
	}
	
	// 숫자만
	function setNumber(TargetObj){
		if(TargetObj != null){
			TargetObj.value = TargetObj.value.replace(/[^0-9]/g, '');
		}
		return TargetObj;
	};
		
</script>
