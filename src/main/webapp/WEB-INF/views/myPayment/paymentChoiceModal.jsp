<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
	<!-- Modal -->
	<div class="modal fade" id="payment-choice">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">결제방식을 선택해 주세요.</h4>
	      </div>
	      <div class="modal-footer">
	      	<span class="color-red" style="font-size: 12px;">신용카드 결제시, <a href="http://vp.co.kr" target="_blank">vp.co.kr</a> 접속하여 일반결제 ISP 설치 필요</span>
	        <button type="button" class="btn btn-primary" id="button_payType1" onclick="payType('1');">신용카드</button>
	        <button type="button" class="btn btn-primary" id="button_payType2" onclick="payType('2');">가상계좌</button>
	      </div>
	    </div>
	  </div>
	</div>

<script type="text/javascript">

	
	//결제 형태
	function payType(num){
		var mallForm = document.mallForm;
		mallForm.payKind.value = num;
		$("#payment-choice").modal('hide');
		
		doPay();
	}
	
</script>