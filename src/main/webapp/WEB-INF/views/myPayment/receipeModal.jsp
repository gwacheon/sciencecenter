<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix='fn' uri='http://java.sun.com/jsp/jstl/functions'%>

<div class="modal fade" id="receipe-modal">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">영수증 출력</h4>
	      </div>
	      <div class="modal-body" id="PrintArea">
					<div class="row">
						<div class="col-md-12">
							<div id="reservation-receipe">
								
							</div>
						</div>
					</div>
				</div>
			<div class="modal-footer" id="receipe_btn">
		        <button type="button" class="btn btn-teal reverse" data-dismiss="modal">닫기
		        </button>
		        <button type="button" class="btn btn-primary" onclick="ReceipePrint()">인쇄하기</button>
	      </div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function ReceipePrint(){
		$('#receipe_btn').hide();
		var AreaContents = document.getElementById('receipe-modal').innerHTML;
        var strFeature;
        strFeature = "width=850,height=800,toolbar=no,location=no,directories=no";
        strFeature += ",status=no,menubar=no,scrollbars=yes,resizable=no";
        var cssBody = '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/external.css"/> \'>';
		cssBody += '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/ptech.css"/> \'>';
        cssBody += '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/application.css"/> \'>';
        cssBody += '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/reservation.css"/> \'>';
        cssBody += '<link rel=\'stylesheet\' type=\'text/css\' href=\'<c:url value="/resources/css/user.css"/> \'>';
        

        objWin = window.open('', 'print', strFeature);
		
        objWin.document.open();
        objWin.document.write('<html><head>');
        objWin.document.write(cssBody);
        objWin.document.write('</head><body>');
        objWin.document.write(AreaContents);
        objWin.document.write('</body></html>');
        objWin.document.close();
        
        setTimeout(function(){
        	printReceipe(objWin);
        },2000);
	}
	
	function printReceipe(objWin){
		$('#receipe_btn').show();
		objWin.print();
        objWin.close();
	}
	
	
</script>