<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:forEach var="board" items="${boards }">
	<div class="row main-board-item">
		<a href="${param.url }/${board.id}">
	 		<div class="col-xs-12 col-lg-12">
	 			<span class="teal">&middot;</span> ${board.title }
	 		</div>
 		</a>
 	</div>
</c:forEach>
	
<div class="more">
	<c:choose>
		<c:when test="${not empty param.footUrl}">
			<a href="${param.footUrl }">
				더보기 + 
			</a>
		</c:when>
		<c:otherwise>
			<a href="${param.url }">
				더보기 + 
			</a>
		</c:otherwise>
	</c:choose>
</div>