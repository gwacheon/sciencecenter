<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<div id="topMemberFrame">
	<div class="container">
		<div class="frame-wrapper">
			<div id="member-frame-slide-window" class="ticker-wrap">
				<div id="member-frame-slide-wrapper" class="ticker">
					<div class="member-category gold">
						<div id="life-members" class="members ticker__item">
							<div id="life-member-frame" class="member-frame">
							</div>
						</div>
					</div>
					<div class="member-category">
						<div id="org-members" class="members ticker__item">
							<div id="org-member-frame" class="member-frame">
							</div>
						</div>
					</div>
				
					<div class="member-category">
						<div id="private-members" class="members ticker__item">
							<div id="private-member-frame" class="member-frame">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	Handlebars.registerHelper('ifSixth', function (index, options) {
	   if(index%6 == 5){
	      return options.fn(this);
	   } else {
	      return options.inverse(this);
	   }

	});
</script>


<!-- 후원회원 상단 프레임 -->
<script id="memberFrameTemplate" type="text/x-handlebars-template">
<div class="names" data-idx="">
{{#each members}}		
		<div class="name">
			{{{this}}}
		</div>
	{{#ifSixth @index}}
		</div>
		<div class="names">
	{{/ifSixth}}
{{/each}}
</div>
</script>

<script type="text/javascript">

	/* // 평생회원
	var lifeMembers = [
		"㈜케이티",
		"구글",
		"과천시청"
	]; */
	
	// 기관회원
	var orgMembers = [
		
		"진영자판기",
		"스포츠링",
		"과학기술인 협동조합",
		"(주)KHE",
		"SK에너지㈜",
		"연구소재중앙센터",
		"(사)과학기술포럼",
		"㈜엠피디에이",
		"서울디지털대학교",
		"유니온기업㈜",
		"㈜메타스페이스",
		"디씨씨케이터링",
		"㈜코오롱",
		"Konica Minolta",
		"SKY-SKAN",
		"㈜미디어스페이스",
		"동성제약㈜",
		"엘투산업",
		"한양원",
		"비알코리아㈜",
		"인력교육원",
		"한국과학기자협회",
		"㈜디지털크리처",
		"항공우주연구원",
		"마사회",
		"꿈꾸는과학",
		"나노소자특화팹센터",
		"일진제강㈜",
		"일진머티리얼즈㈜",
		"길의료재단",
		"한국과학기술연구원"
	];
	
	//개인회원
	var privateMembers = [
			"㈜ 티에스인베스트먼트",
			"(주)KHE",
			"강민구",
			"강민석",
			"강민제",
			"강은우",
			"강채훈",
			"고재형",
			"공복순",
			"구수정",
			"구영훈",
			"권경오",
			"권오갑",
			"권일찬",
			"권일헌",
			"권채순",
			"기혜림",
			"김가린",
			"김강연",
			"김광연",
			"김기문",
			"김대호",
			"김대희",
			"김도원",
			"김미란",
			"김미주",
			"김민규",
			"김민서",
			"김민성",
			"김민채",
			"김범진",
			"김병국",
			"김보림",
			"김부빈",
			"김상선",
			"김상직",
			"김선혜",
			"김선호",
			"김세용",
			"김세정",
			"김세준",
			"김순옥",
			"김시선",
			"김시원",
			"김영옥",
			"김영희",
			"김옥림",
			"김완숙",
			"김용휘",
			"김원기",
			"김의종",
			"김재영",
			"김정욱",
			"김정원",
			"김정일",
			"김종열",
			"김주일",
			"김주형",
			"김주호",
			"김준우",
			"김준하",
			"김중영",
			"김지상",
			"김지현",
			"김진규",
			"김창환",
			"김채아",
			"김충곤",
			"김태범",
			"김태형",
			"김태훈",
			"김하슬",
			"김하은",
			"김현서",
			"김현중",
			"김형봉",
			"김홍석",
			"나치수",
			"남기현",
			"남도우",
			"남예은",
			"남지선",
			"노재원",
			"노재익",
			"노한숙",
			"도준형",
			"류다현",
			"류상희",
			"류영웅",
			"문민재",
			"문송효",
			"문시우",
			"문찬우",
			"민정빈",
			"박단",
			"박미애",
			"박미희",
			"박민성",
			"박서준",
			"박성준",
			"박성진",
			"박세웅",
			"박세희",
			"박순희",
			"박영주",
			"박은미",
			"박재우",
			"박정화",
			"박주연",
			"박지환",
			"박진균",
			"박진서",
			"박진희",
			"박현진",
			"박혜현",
			"박희곤",
			"배부영",
			"배운지",
			"배윤경",
			"배재현",
			"백상종",
			"백승연",
			"백정현 ",
			"백한서",
			"백현숙",
			"백현우",
			"변경희",
			"서종효",
			"성혁",
			"손수일",
			"손아영",
			"손정현",
			"손주완",
			"송정호",
			"송준호",
			"송지호",
			"송현량",
			"신동혁",
			"신순호",
			"신용운",
			"신유미",
			"심원무",
			"안유리",
			"안유민",
			"안준성",
			"안지호",
			"안채영",
			"안형찬",
			"양광남",
			"양시원",
			"양해본",
			"양희",
			"염기수",
			"예지현",
			"오경주",
			"오연숙",
			"오영준",
			"오용환",
			"오재건",
			"옥상원",
			"우사임",
			"유만선",
			"유봉진",
			"유선영",
			"유승범",
			"유여진",
			"유재원",
			"유지혁",
			"유지흔",
			"유창영",
			"윤두영",
			"윤봉선",
			"윤선희",
			"윤승현",
			"윤영완",
			"윤용진",
			"윤용황",
			"윤진찬",
			"윤혜민",
			"이강국",
			"이건배",
			"이광은",
			"이근철",
			"이기선",
			"이미영",
			"이민규",
			"이병순",
			"이봉로",
			"이봉재",
			"이상덕",
			"이성숙",
			"이세용",
			"이수웅",
			"이순옥",
			"이승우",
			"이승준",
			"이승현",
			"이영일",
			"이영재",
			"이영주",
			"이원경",
			"이인일",
			"이임영",
			"이정민",
			"이정수",
			"이종옥",
			"이주원",
			"이준원",
			"이준호 ",
			"이지유",
			"이춘호",
			"이홍범",
			"임계현",
			"임승빈",
			"임지연",
			"임형준",
			"장서현",
			"장서현",
			"장재열",
			"전성윤",
			"전수현",
			"전현영",
			"정경욱",
			"정광훈",
			"정명기",
			"정민우",
			"정연란",
			"정원영",
			"정윤석",
			"정재한",
			"정지원 ",
			"조대흠",
			"조민성",
			"조성찬 ",
			"조영상",
			"조윤호",
			"조은진",
			"조정훈",
			"조춘익",
			"조태현",
			"조현석",
			"주예은",
			"지희정",
			"차신영",
			"최경숙",
			"최광민",
			"최락준",
			"최민수",
			"최성욱",
			"최소영",
			"최승혁",
			"최양희",
			"최어진",
			"최예담",
			"최예름",
			"최예지",
			"최원식",
			"최윤성",
			"최재희",
			"최정민",
			"최정원",
			"최창수",
			"피여경",
			"하서연",
			"한도욱",
			"한상국",
			"한성환",
			"한종우",
			"함창식",
			"허아숭",
			"허옥남",
			"허재현",
			"허종수",
			"현보아",
			"홍영재",
			"홍지서",
			"홍현선 ",
			"황규식 ",
			"황성하",
			"황지민",
			"황지훈"
	];
	// sorting
	//lifeMembers.sort();
	orgMembers.sort();
	privateMembers.sort();
	
	
	
	/* lifeMembers.unshift("<span class='member-category-title'>평생회원</span>");
	lifeMembers.push(" ");
	lifeMembers.push(" ");
	 */
	orgMembers.unshift("<span class='member-category-title'>기관회원</span>");
	orgMembers.unshift("㈜케이티");
	orgMembers.unshift("구글");
	orgMembers.unshift("과천시청");
	
	
	orgMembers.unshift("<span class='member-category-title'>평생회원</span>");
	privateMembers.unshift("<span class='member-category-title'>개인회원</span>");
	
	
	/* var lifeMembersSource = $("#memberFrameTemplate").html();
	var lifeMembersTemplate = Handlebars.compile(lifeMembersSource);
	var lifeMembersHtml = lifeMembersTemplate({members: lifeMembers});
	$("#life-member-frame").html(lifeMembersHtml); */
	
	var orgMembersSource = $("#memberFrameTemplate").html();
	var orgMembersTemplate = Handlebars.compile(orgMembersSource);
	var orgMembersHtml = orgMembersTemplate({members: orgMembers});
	$("#org-member-frame").html(orgMembersHtml);
	
	var privateMembersSource = $("#memberFrameTemplate").html();
	var privateMembersTemplate = Handlebars.compile(privateMembersSource);
	var privateMembersHtml = privateMembersTemplate({members: privateMembers});
	$("#private-member-frame").html(privateMembersHtml);

</script>

<script type="text/javascript">
	
	/* $( function() { 
		
		var windowSize = $('#member-frame-slide-window').width();
		var nameCount = $('.names .name').size();
		var wrapperWidth = 0;
		$('.members').each( function() {
			wrapperWidth = wrapperWidth + $(this).width();
		});
		
		$("#member-frame-slide-wrapper").width( wrapperWidth + "px" );
		
		$("#member-frame-slide-wrapper").animate({
	        "left": "-" + (wrapperWidth - windowSize) + "px"
	      },
	      {
			duration : 10000,
			comlete: function() {
				$("#member-frame-slide-wrapper").css('left', '');				
			}
	      }
	
	    );
	}); */
    
	
	//$("#member-frame-slide-wrapper").css('right', function(){ 
		//return $(this).offset().left; 
	//}).animate({"right":"0px"}, "slow");    ​
	
	// 평생회원 프레임 슬라이드
	/* var lifeMemberSlides = $('#life-members .names');
	var lifeMemberSlideCount = lifeMemberSlides.length;
	var lifeMemberIndex = 0;
	$("#life-members .names[data-idx=" + lifeMemberIndex + "]").addClass("active");
	window.setInterval( function() {
		$("#life-members .names[data-idx=" + lifeMemberIndex + "]").removeClass("active");
		if( lifeMemberIndex == (lifeMemberSlideCount - 1) ) {
			lifeMemberIndex = 0;
		}
		else {
			lifeMemberIndex = lifeMemberIndex + 1;
		}
		$("#life-members .names[data-idx=" + lifeMemberIndex + "]").addClass("active");
	}, 3000); */
	
	// 기관회원 프레임 슬라이드	
	/* var orgMemberSlides = $('#org-members .names');
	var orgMemberSlideCount = orgMemberSlides.length;
	var orgMemberIndex = 0;
	$("#org-members .names[data-idx=" + orgMemberIndex + "]").addClass("active");
	
	window.setInterval( function() {
		$("#org-members .names[data-idx=" + orgMemberIndex + "]").removeClass("active");
		if( orgMemberIndex == (orgMemberSlideCount - 1) ) {
			orgMemberIndex = 0;
		}
		else {
			orgMemberIndex = orgMemberIndex + 1;
		}
		$("#org-members .names[data-idx=" + orgMemberIndex + "]").addClass("active");
	}, 3000);
	
	// 일반회원 프레임 슬라이드
	var privateMemberSlides = $('#private-members .names');
	var privateMemberSlideCount = privateMemberSlides.length;
	var privateMemberIndex = 0;
	$("#private-members .names[data-idx=" + privateMemberIndex + "]").addClass("active"); */
	
	/* window.setInterval( function() {
		$("#private-members .names[data-idx=" + privateMemberIndex + "]").removeClass("active");
		if( privateMemberIndex == (privateMemberSlideCount - 1) ) {
			privateMemberIndex = 0;
		}
		else {
			privateMemberIndex = privateMemberIndex + 1;
		}
		$("#private-members .names[data-idx=" + privateMemberIndex + "]").addClass("active");
	}, 3000); */
	
</script>
