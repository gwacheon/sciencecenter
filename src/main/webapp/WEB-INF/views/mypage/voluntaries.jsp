<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container">
	<div class="row margin-top30">
		<div class="col-md-3">
			<c:import url="/WEB-INF/views/mypage/side.jsp">
				<c:param name="active" value="voluntaries"></c:param>
			</c:import>
		</div>
		
		<div class="col-md-9">
			<h4 class="doc-title teal">봉사활동 내역</h4>
			
			<table class="table">
				<thead>
					<tr>
						<th>프로그램명</th>
						<th class="center">장소</th>
						<th class="center">시간</th>
						<th class="center">상태</th>
						<th class="center"></th>
					</tr>
				</thead>
				
				<tbody>
					<c:forEach var="volunteer" items="${volunteers }">
						<tr>
							<td>${volunteer.voluntary.title }</td>
							<td class="center">${volunteer.voluntary.location }</td>
							<td class="center">
								<fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${volunteer.voluntaryTime.beginTime}" />
								<fmt:formatDate pattern=" ~ HH:mm" value="${volunteer.voluntaryTime.endTime}" />
							</td>
							
							<c:choose>
								<c:when test="${volunteer.status == 'accept' }">
									<td class="center">
										<span class="label success-label">
											신청완료
										</span>
									</td>
									<td class="center">
										<a href="#" class="btn">
											취소
										</a>
									</td>
								</c:when>
								
								<c:when test="${volunteer.status == 'pending' }">
									<td class="center">
										<span class="label warning-label">
											신청서 미작성
										</span>
									</td>
									<td class="center">
										<a href="#" class="btn">
											신청서 작성
										</a>
									</td>
								</c:when>
								
								<c:when test="${volunteer.status == 'completion' }">
									<td class="center">
										<span class="label warning-label">
											봉사활동 완료
										</span>
									</td>
									<td class="center">
										<a href="<c:url value="confirmationForm/${volunteer.id }"/>" target="_blank" class="btn">
											<span class="label">
												확인증 출력
											</span>
										</a>
									</td>
								</c:when>
							</c:choose>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			
			<div class="row">
				<div class="col m12 s12">
					<c:import url="/WEB-INF/views/layouts/pagination.jsp"></c:import>
				</div>
			</div>
		</div>
	</div>
