<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container">
	<div class="row margin-top30">
		<div class="col-md-3">
			<c:import url="/WEB-INF/views/mypage/side.jsp">
				<c:param name="active" value="reservations"></c:param>
			</c:import>
		</div>
		
		<div class="col-md-9">
			<h3 class="page-title teal">
				단체예약 내역
			</h3>
			
			<div class="row">
				<div class="col-md-12 center">
					<table class="table">
						<thead>
							<tr>
								<th class="center">예약번호</th>
								<th>프로그램명</th>
								<th>예약날짜</th>
								<th>인원</th>
								<th>상태</th>
							</tr>
						</thead>
						
						<tbody>
							<c:choose>
								<c:when test="${not empty orgApplicants }">
									<c:forEach var="applicant" items="${orgApplicants }">
										<tr>
											<td class="center">${applicant.id }</td>
											<td class="left">${applicant.program.title }</td>
											<td class="left">
												<fmt:formatDate pattern="yyyy/MM/dd HH:mm" value="${applicant.applicationTime }" />
											</td>
											<td class="left">
												${applicant.members } 
												(<spring:message code="orgProgram.type.${applicant.applicationType }" />)
											</td>
											<td class="left">
												<spring:message code="orgProgram.status.${applicant.applicationStatus }" />
											</td>
										</tr>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<tr>
										<td class="center" colspan="4">
											단체예약 정보가 없습니다.
										</td>
									</tr>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
					<c:import url="pagination.jsp"></c:import>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$(".nav.nav-tabs > li > a").click(function(e){
			var location = $(this).attr('href');
			var isInternal = location.indexOf("#") >= 0;
			
			if(!isInternal){
				window.location.href = location;
			}
		});
	});
</script>