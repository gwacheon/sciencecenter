<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="container">
	<div class="row margin-top30">
		<div class="col-md-3">
			<c:import url="/WEB-INF/views/mypage/side.jsp">
				<c:param name="active" value="reservations"></c:param>
			</c:import>
		</div>
		
		<div class="col-md-9">
			<h3 class="page-title teal">
				나의 예약내역
			</h3>
		</div>
	</div>
</div>