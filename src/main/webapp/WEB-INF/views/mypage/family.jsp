<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container">
	<div class="row margin-top30">
		<div class="col-md-3">
			<c:import url="/WEB-INF/views/mypage/side.jsp">
				<c:param name="active" value="privateInfo"></c:param>
			</c:import>
		</div>
		
		<div class="col-md-9">
			<h3 class="page-title teal">
				가족정보
			</h3>
			
			<div class="private-info-box">
				<div class="table-responsive table-teal">
					<table class="table">
						<thead>
							<tr>
								<th>이름</th>
								<th>회원번호</th>
								<th>구분</th>
								<th>관계</th>
								<th>비고</th>
							</tr>
						</thead>
						
						<tbody>
							<c:choose>
								<c:when test="${not empty memberWithFamily.families}">
									<c:forEach var="f" items="${memberWithFamily.families }">
										<tr>
											<td>${f.familyName }</td>
											<td>${f.familyNo }</td>
											<td>${f.membershipName }</td>
											<td>${f.relationTypeName }</td>
											<td>
												<div class="btn-group">
													<c:choose>
														<c:when test="${currentMemberNo == f.familyNo }">
															<a href="<c:url value='/mypage/privateInfo'/>" class="btn btn-teal reverse">
																수정
															</a>	
														</c:when>
														
														<c:otherwise>
															<a href="#" class="btn btn-teal reverse edit-family-btn"
																data-no="${f.familyNo }">
																수정
															</a>	
														</c:otherwise>
													</c:choose>
													
													<a href="#" class="btn btn-teal reverse delete-btn" data-id="${f.familyNo }">
														삭제
													</a>
												</div>
											</td>
										</tr>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<tr>
										<td class="center" colspan="5">등록 된 가족 정보가 없습니다.</td>
									</tr>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
					
					<div class="center">
						<a id="add-family-modal-btn" 
							href="#" class="btn btn-teal" data-toggle="modal">
							가족 추가
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="family-modal" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">가족정보</h4>
			</div>
			<div class="modal-body">
				<form id="family-form">
					<div class="row">
						<div class="col-md-8">
							<div class="form-group">
								<label for="familyName">가족이름</label> 
								<input type="text" class="form-control" id="familyName" name="familyName">
							</div>
						</div>
						
						<div class="col-md-4">
							<label for="성별">성별</label>
							<div class="radio">
								<input type="radio" name="genderFlag" id="gender-flag-male" value="M" checked>
								<label for="gender-flag-male">남성</label>
								
								<input type="radio" name="genderFlag" id="gender-flag-female" value="F">
								<label for="gender-flag-female">여성</label>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<label for="familyCel">가족 전화번호</label> 
						<input type="text" class="form-control" id="familyCel" name="familyCel">
					</div>
					<div class="form-group">
						<label for="familyEmail">가족 이메일</label> 
						<input type="email" class="form-control" id="familyEmail" name="familyEmail">
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label for="schoolName">학교명</label> 
								<input type="text" class="form-control" id="schoolName" name="schoolName">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="gradeName">학교 및 학년</label> 
								<input type="text" class="form-control" id="gradeName" name="gradeName">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="className">학교반</label> 
								<input type="text" class="form-control" id="className" name="className">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<label for="birthDate">가족생일</label>
							<div class="form-inline form-group">
								<input type="hidden" id="birth-date" name="birthDate">
								<select id="birth-year" class="form-control teal">
									<jsp:useBean id="now" class="java.util.Date" />
									<fmt:formatDate var="currentYear" value="${now}" pattern="yyyy" />
									<c:set var="defaultYear" value="${currentYear - 20 }"></c:set>
									<c:forEach var="y" begin="0" end="120" >
										<option value="${currentYear - y}" <c:if test="${y == 20 }">selected</c:if>>${currentYear -y }</option>
									</c:forEach>
								</select>
								
								<select id="birth-month" class="form-control teal">
									<c:forEach var="m" begin="1" end="12" >
										<option value="<fmt:formatNumber type="number" minIntegerDigits="2" value="${m}" />">
											<fmt:formatNumber type="number" minIntegerDigits="2" value="${m}" />
										</option>
									</c:forEach>
								</select>
								
								<select id="birth-day" class="form-control teal">
									<c:forEach var="m" begin="1" end="31" >
										<option value="<fmt:formatNumber type="number" minIntegerDigits="2" value="${m}" />">
											<fmt:formatNumber type="number" minIntegerDigits="2" value="${m}" />
										</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<label for="birthDate">양력/음력</label>
							<div class="radio">
								<input type="radio" name="lunarFlag" id="lunar-flag-false" value="false" checked>
								<label for="lunar-flag-false">양력</label>
								
								<input type="radio" name="lunarFlag" id="lunar-flag-true" value="true">
								<label for="lunar-flag-true">음력</label>
							</div>
						</div>
					</div>
					<input type="hidden" id="familyNo" name="familyNo">
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-teal reverse" data-dismiss="modal">취소</button>
				<button id="submit-family-btn" type="button" class="btn btn-teal">등록</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var families = {};
	<c:forEach var="f" items="${memberWithFamily.families }">
		families["${f.familyNo }"] = {
			familyNo: "${f.familyNo }",
			memberNo: "${f.memberNo }",
			familyName: "${f.familyName }",
			birthDate: "${f.birthDate }",
			familyCel: "${f.familyCel }",
			familyEmail: "${f.familyEmail }",
			schoolName: "${f.schoolName }",
			gradeName: "${f.gradeName }",
			className: "${f.className}",
			lunarFlag: "${f.lunarFlag }",
			genderFlag: "${f.genderFlag }"
		};		
	</c:forEach>
</script>

<script type="text/javascript">
	$(function(){
		$('.datepicker').datetimepicker({
			format: 'YYYY/MM/DD'
		}).on("dp.change", function (e) {
			console.log($(this).val());
        });
		
		$(".delete-btn").click(function(e){
			var familyNo = $(this).data('id');
			if (confirm("선택하신 가족 정보를 삭제하시겠습니까?")) {
				$.ajax({
					url: baseUrl + "mypage/deleteFamily",
					beforeSend: function(xhr) {
						ajaxLoading();
						xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));
					},
					contentType : 'application/json',
			        type: "POST",
			        data: JSON.stringify({"familyNo": familyNo}),
			        success: function(data){
			        	ajaxUnLoading();
			        	
			        	if (data.status == "complete"){
			        		location.reload();	
			        	}
			        },
			        error: function(err){
			        	ajaxUnLoading();
			        	console.log(err);
			        }
				})
			}
		});
		
		function setFamilyModalStatus(isNew) {
			if (isNew) {
				$("#submit-family-btn").text("등록");
				$("#submit-family-btn").removeClass("update-info");
				$("#submit-family-btn").addClass("new-info");
			} else {
				$("#submit-family-btn").text("수정");
				$("#submit-family-btn").removeClass("new-info");
				$("#submit-family-btn").addClass("update-info");
			}
		}
		
		$("#add-family-modal-btn").click(function(){
			setFamilyModalStatus(true);
			$("#family-modal").modal();
		});
		
		$(".edit-family-btn").click(function(e){
			var familyNo = $(this).data('no');
			var family = families[familyNo];
			_.each(family, function(v, k){
				$("#" + k).val(v);
			});
			
			$("input[name='genderFlag'][value='" + family.genderFlag + "']").prop("checked", true);
			$("input[name='lunarFlag'][value='" + family.lunarFlag + "']").prop("checked", true);
			$("#birth-year").val(family.birthDate.substr(0,4));
			$("#birth-month").val(family.birthDate.substr(4,2));
			$("#birth-day").val(family.birthDate.substr(6,2));
			
			setFamilyModalStatus(false);
			
			$("#family-modal").modal();
		});
		
		function getFamilyFormInfo(){
	
			var birthDate = $("#birth-year").val() + $("#birth-month").val() + $("#birth-day").val();
			var family = {
				familyNo: $("#familyNo").val(),
				familyName: $("#familyName").val(),
				familyCel: $("#familyCel").val(),
				familyEmail: $("#familyEmail").val(),
				schoolName: $("#schoolName").val(),
				gradeName: $("#gradeName").val(),
				className: $("#className").val(),
				birthDate: birthDate,
				lunarFlag: $("#lunar-flag-true").is(":checked"),
				genderFlag: $("input[name='genderFlag']:checked").val()
			};
			
			return family;
		}
		
		$(document).on("click", "#submit-family-btn.new-info", function(e){
			var family = getFamilyFormInfo();
			
			$.ajax({
				url: baseUrl + "mypage/addFamily",
				beforeSend: function(xhr) {
					ajaxLoading();
					xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));
				},
				contentType : 'application/json',
		        type: "POST",
		        data: JSON.stringify(family),
		        dataType: "JSON",
		        success: function(data){
		        	ajaxUnLoading();
		        	if (data.status == "complete"){
		        		location.reload();	
		        	}
		        },
		        error: function(err){
		        	ajaxUnLoading();
		        	console.log(err);
		        }
			});
		});
		
		$(document).on("click", "#submit-family-btn.update-info", function(e){
			var family = getFamilyFormInfo();
	
			$.ajax({
				url: baseUrl + "mypage/updateFamily",
				beforeSend: function(xhr) {
					ajaxLoading();
					xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));
				},
				contentType : 'application/json',
		        type: "POST",
		        data: JSON.stringify(family),
		        dataType: "JSON",
		        success: function(data){
		        	ajaxUnLoading();
		        	if (data.status == "complete"){
		        		location.reload();	
		        	}
		        },
		        error: function(err){
		        	ajaxUnLoading();
		        	console.log(err);
		        }
			});
		});
		
		$('#family-modal').on('hidden.bs.modal', function (e) {
			$("#family-form")[0].reset();
		});
	});
</script>