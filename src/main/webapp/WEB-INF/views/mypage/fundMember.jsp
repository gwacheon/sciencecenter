<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container">
	<div class="row margin-top30">
		<div class="col-md-3">
			<c:import url="/WEB-INF/views/mypage/side.jsp">
				<c:param name="active" value="privateInfo"></c:param>
			</c:import>
		</div>
		
		<div class="col-md-9">
			<h3 class="page-title teal">
				후원회원 신청
			</h3>
			
			<div class="private-info-box">
				<div class="alert alert-success">
					국립과천과학관 후원회 (사)과학사랑희망키움에서 후원회원을 모집합니다.<br>
					국립과천과학관 후원회는 2010년 10월 과학을 사랑하는 각계각층의 사람들이 모여 미래과학자들의 꿈과 희망을 실현 하고자 설립되었습니다.<br>
					특히, 경제적.지역적.문화적 소외계층 청소년들에게 과학을 접하고 즐길수 있도록 노력을 기울여 왔습니다.<br>
					그 결실로 2015년 구글(Google)의 에릭 슈미트 회장 방문과 86만 달러 후원으로 키즈메이커 스튜디오 및 구글 창조놀이터 건립이 진행되고 있습니다.<br>
					과학의 가치는 사회를 변화시키는 힘이 있습니다. 올해 4월부터, 청소년의 미래과학자 희망을 키워가기 위해 후원금을 모집합니다.<br>
					후원회원으로 가입하셔서 국립과천과학관에 사랑의 힘을 더해주시기를 부탁드립니다.<br>
				</div>
				
				<div class="center">
					<button type="button" id="request-fund-member-modal" class="btn btn-teal">
						후원회원 신청
					</button>
				</div>
				
				
				<div class="sub-section">
					<ul class="circle-list teal sub-section-title">
						<li>
							회원 종류 및 연회비
						</li>
					</ul>
					<div class="row">
						<div class="col-md-12">
							<div class="content table-responsive">
								<table class="table table-bordered centered-table">
									<thead>
										<tr>
											<th>회원종류</th>
											<th>회원비</th>
											<th>혜택적용 기간</th>
									</thead>
									<tbody>
										<tr>
											<td>
												개인회원 (월납)
											</td>
											<td>
												5,000원 / 월
											</td>
											<td>
												<ul class="dash-list no-margin text-left">
													<li>
														최초 3개월 납입시점부터 혜택적용(1, 2개월차에는 혜택없음)
													</li>
													<li>
														3개월 이상 기부금 미납 회원은 혜택이 제한될 수 있음.
													</li>
												</ul>
											</td>
										</tr>
										<tr>
											<td>
												개인회원 (연납)
											</td>
											<td>
												50,000원 / 연
											</td>
											<td>
												<ul class="dash-list no-margin text-left">
													<li>
														연 회원비 납입 후 바로혜택 적용
													</li>
												</ul>
											</td>
										</tr>
										<tr>
											<td>
												기관회원 (월납)
											</td>
											<td>
												10,000원 / 월
											</td>
											<td>
												<ul class="dash-list no-margin text-left">
													<li>
														최초 3개월 납입시점부터 혜택적용(1, 2개월차에는 혜택없음)
													</li>
													<li>
														3개월 이상 기부금 미납 회원은 혜택이 제한될 수 있음.
													</li>
												</ul>
											</td>
										</tr>
										<tr>
											<td>
												기관회원 (연납)
											</td>
											<td>
												100,000원 / 연
											</td>
											<td>
												<ul class="dash-list no-margin text-left">
													<li>
														연 회원비 납입 후 바로혜택 적용
													</li>
												</ul>
											</td>
										</tr>
										<tr>
											<td>
												Honor 평생 개인회원
											</td>
											<td>
												누적 1천만원 이상 (일괄납입 가능)
											</td>
											<td>
												<ul class="dash-list no-margin text-left">
													<li>
														평생 회원비 납입 후 바로혜택 적용
													</li>
												</ul>
											</td>
										</tr>
										<tr>
											<td>
												Honor 평생 기관회원
											</td>
											<td>
												누적 1억원 이상 (일괄납입 가능)
											</td>
											<td>
												<ul class="dash-list no-margin text-left">
													<li>
														평생 회원비 납입 후 바로혜택 적용
													</li>
												</ul>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						
						</div>
					</div>
				</div>
				
				<div class="sub-section">
					<ul class="circle-list teal sub-section-title">
						<li>
							후원 회원 혜택
						</li>
					</ul>
					<div class="content">
						<div class="row">
							<div class="col-md-12 content-title-wrapper">
								<ul class="arrow-list text-color text-bold teal">
									<li>
										개인회원 (월납 & 연납)	
										<ul>
											<li>
												상설전시관 관람료 무료
											</li>
											<li>
												도슨트 전문해설 서비스 무료  
											</li>
											<li>
												창의체험과학탐구 개인과정 교육비 20% 할인
											</li>
											<li>
												천체투영관 관람료 50%할인
											</li>
											<li>
												천체관측소 교육비 20% 할인
											</li>
											<li>
												과학교육 및 문화행사 참여 50% 우선배정
											</li>
											<li>
												자원봉사 활동 50% 우선배정
											</li>
											<li>
												체험전시물 예약 30% 우선배정
											</li>
											<li>
												과학문화 공연 및 특별기획전 관람료 할인(별도공지)
											</li>
											<li>
												과학관 자체 기획전시회 초대 등
											</li>												
										</ul>
									</li>
									<li>
										Honor 평생 개인회원
										<ul>
											<li>
												개인회원 예우 및 혜택 (상단 내용 확인)
											</li>
											<li>
												과학관 내 후원인 명판설치
											</li>
											<li>
												과학의 날 특별전 오픈행사 초대
											</li>
											<li>
												예약 방문 시 전시해설 서비스 제공
											</li>
										</ul>
									</li>
									<li>
										기업회원(월납&연납)
										<ul>
											<li>
												기업회원에 대한 예우 및 혜택은 협의 후 제공
											</li>
										</ul>
									</li>
									<li>
										Honor 평생 기업회원
										<ul>
											<li>
												평생회원 예우 및 혜택
											</li>
											<li>
												감사패 증정
											</li>
										</ul>
									</li>
								</ul>			
							</div>
						</div>
						<div class="color-red">
							*후원회원 혜택은 과학관 정책에 따라 변경 될 수 있습니다.
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="fund-member-modal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">후원회원 신청</h4>
			</div>
			<div class="modal-body">
				<form id="membership-form">
					<div class="form-group">
						<label for="membership-type">종류</label> 
						<select id="membership-type" name="membershipType" class="form-control">
							<option value="VB">개인회원 (월납)</option>
							<option value="VA">개인회원 (연납)</option>
							<option value="VC">기관회원 (월납)</option>
							<option value="VD">기관회원 (연납)</option>
							<option value="VE">Honor 평생 개인회원</option>
							<option value="VF">Honor 평생 기관회원</option>
						</select>
					</div>
					<div class="form-group">
						<label for="bank-cd">은행</label> 
						<input type="text" class="form-control" id="bank-cd" name="bankCd">
					</div>
					<div class="form-group">
						<label for="account-number">계좌번호</label> 
						<input type="text" class="form-control" id="account-number" name="accountNumber">
					</div>
					<div class="form-group">
						<label for="account-name">예금주명</label> 
						<input type="text" class="form-control" id="account-name" name="accountName">
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">취소</button>
				<button id="submit-fund-form" type="button" class="btn btn-primary">신청</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script type="text/javascript">
	$("#request-fund-member-modal").click(function(e){
		$('#fund-member-modal').modal();	
	});
	
	$("#submit-fund-form").click(function(e){
		var membership = {
			membershipType: $("#membership-type").val(),
			bankCd: $("#bank-cd").val(),
			accountNum: $("#account-number").val(),
			accountName: $("#account-name").val()
		};
		
		$.ajax({
			url: baseUrl + "mypage/privateInfo/fundMember",
			type : "POST",
			beforeSend: function(xhr) {
				ajaxLoading();
	            xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));
	        },
	        contentType : 'application/json',
	        data : JSON.stringify(membership),
	        success: function(data) {
	        	if (data.status == "complete") {
	        		alert('후원회원 신청이 정상적으로 이뤄졌습니다.');	
	        	}
	        	
	        	ajaxUnLoading();
	        },
	        error: function (xhr, ajaxOptions, thrownError) {
	        	ajaxUnLoading();
	        	alert('후원회원 신청이 이뤄지지 않았습니다. 문제가 지속될 경우 관리자에게 문의 바랍니다.');
	        	
				console.log(xhr.status);
				console.log(thrownError);
			}
		});
	});
</script>