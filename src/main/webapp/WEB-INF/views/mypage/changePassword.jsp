<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="container">
	<div class="row margin-top30">
		<div class="col-md-3">
			<c:import url="/WEB-INF/views/mypage/side.jsp">
				<c:param name="active" value="privateInfo"></c:param>
			</c:import>
		</div>
		
		<div class="col-md-9">
			<h3 class="page-title teal">
				비밀번호 변경
			</h3>
			
			<div class="private-info-box">
				<c:url value="/mypage/changePassword" var="action"></c:url>
				<form:form id="change-password-form"  class="" action="${action}" method="POST" commandName="member" modelAttribute="member">
					<form:input type="hidden" path="memberNo"/>
					<div class="row">
						<div class="col-md-offset-3 col-md-6">
							<div class="form-group">
								<label for="current-password">현재 비밀번호</label>
								<input type="password" id="current-password" class="form-control">
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-offset-3 col-md-6">
							<div class="form-group">
								<form:label path="password">변경 비밀번호</form:label>
								<form:password path="password" class="form-control"/>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-offset-3 col-md-6">
							<div class="form-group">
								<label for="password-confirm">비밀번호 확인</label>
								<input type="password" id="password-confirm" class="form-control">
							</div>
							
							<div class="help-block">
								* 영문소문자, 숫자, 특수문자(!,@,$,%,^,&,* 만 가능)를 모두 1번 이상씩 사용하여 조합(9~14자리 이내)
							</div>
						</div>
					</div>
					
					<div class="row center">
						<div class="col-md-offset-3 col-md-6">
							<button id="submit-btn" type="submit" class="btn btn-primary">
								비밀번호 변경
							</button>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var pwdExp = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{9,14}$/;
	
	$("#submit-btn").click(function(e){
		e.preventDefault();
		
		var err = false;
		var password = $("#password").val();
		var passwordConfirmation = $("#password-confirm").val();
		
		var passwordCheck = checkPassword(password, passwordConfirmation);
		
		if (passwordCheck["err"]) {
			alert(passwordCheck["errMsg"]);
		} else {
			$("#change-password-form")[0].submit();
		}
		
	});
	
	$(function(){
		var pageStatus = "${updateStatus}";
		if ( pageStatus != "" ){
			if ( pageStatus == "complete" ) {
				alert("정상적으로 변경 되었습니다.");	
			} else {
				alert("비밀번호 변경이 정상 반영 되지 않았습니다. 문제가 계속 될 시 고객센터로 문의 바랍니다.");
			}
		} 
	});
</script>