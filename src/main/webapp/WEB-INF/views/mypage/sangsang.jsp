<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container">
	<div class="row margin-top30">
		<div class="col-md-3">
			<c:import url="/WEB-INF/views/mypage/side.jsp">
				<c:param name="active" value="voluntaries"></c:param>
			</c:import>
		</div>
		
		<div class="col-md-9">
			<h4 class="doc-title teal">무한상상 예약 신청 내역</h4>
			
			<table class="table">
				<thead>
					<tr>
						<th>예약번호</th>
						<th class="center">예약</th>
						<th class="center">모델명</th>
						<th class="center">예약 날짜</th>
						<th class="center">상태</th>
					</tr>
				</thead>
				
				<tbody>
					<c:forEach var="reserve" items="${reserves }">
						<tr>
							<td>
								${ reserve.id }
							</td>
							<td>
								${ reserve.equipment.name }
							</td>
							<td>
								${ reserve.serial.serial }
							</td>
							<td>
								<fmt:formatDate pattern="YYYY-MM-dd ( hh:mm" value="${reserve.beginTime}" /> ~ <fmt:formatDate pattern="hh:mm" value="${reserve.endTime}" /> ) 
							</td>
							<td>
								<c:choose>
									<c:when test="${reserve.status eq 'accepts'}">
										<span class="label label-primary">승인완료</span>
									</c:when>
									<c:when test="${reserve.status eq 'cancel'}">
										<span class="label label-danger">반려</span>
									</c:when>
									<c:otherwise>
										<span class="label label-default">승인 대기중</span>
									</c:otherwise>
								</c:choose>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
