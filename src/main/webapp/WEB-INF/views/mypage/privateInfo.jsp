<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<link rel="stylesheet" href="<c:url value='/resources/css/ext/jquery.fileupload.css'/>">

<div class="container">
	<div class="row margin-top30">
		<div class="col-md-3">
			<c:import url="/WEB-INF/views/mypage/side.jsp">
				<c:param name="active" value="privateInfo"></c:param>
			</c:import>
		</div>
		
		<div class="col-md-9">
			<h3 class="page-title teal">
				개인정보 수정
			</h3>
			
			<div class="private-info-box">
				<c:if test="${not empty status and status == 'emailerror'}">
					<div class="alert alert-danger">
						회원정보 수정이 정상적으로 이뤄지지 않았습니다. 사용중인 이메일 입니다. 다시 확인하시고 시도해주시기 바랍니다.
					</div>
				</c:if>
			
				<c:if test="${not empty status and status != 'complete' and status != 'emailerror'}">
					<div class="alert alert-danger">
						회원정보 수정이 정상적으로 이뤄지지 않았습니다. 비빌번호를 다시 확인하시고 시도해주시기 바랍니다.
					</div>
				</c:if>
				
				<c:if test="${not empty status and status == 'complete' }">
					<div class="alert alert-success" role="alert">
						회원정보가 정상적으로 수정되었습니다.
					</div>
				</c:if>
				
				<c:url var="action" value="/mypage/${currentMemberNo }"></c:url>
				<form:form id="signup-form"  class=""
					action="${action }" method="PUT" commandName="member" modelAttribute="member">
					<form:hidden path="memberNo" value="${currentMemberNo }"/>
					<div class="row">
						<div class="col-md-6">
							<label for="fileupload">프로필사진</label>
							<div class="row">
								<div class="col-sm-5">
									<img id="profile-img" alt="프로필사진"
									class="img-responsive" 
									src="<c:url value='/resources/img/users/default-profile.png'/>">
								</div>
								
								<div class="col-sm-7">
									<div>
										<p style="margin-top: 0;">
											사진 선택을 클릭하여 프로필 사진을 설정하세요.
										</p>
										<span class="btn btn-teal fileinput-button"> 
											<i class="glyphicon glyphicon-plus"></i> <span>사진선택</span> <!-- The file input field used as target for the file upload widget -->
											<input id="fileupload" type="file" name="file" accept="image/*">
										</span>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<form:label path="memberName">이름</form:label>
								<form:input path="memberName" required="true" class="form-control"/>
							</div>
							
							<div class="form-group">
								<form:label path="passwordCheck">비밀번호</form:label>
								<form:password path="passwordCheck" required="true" class="form-control"/>
								<p class="help-block">
									* 개인정보 수정을 위해서는 비밀번호 입력이 필요합니다.
								</p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<form:label path="memberZipCode">우편번호</form:label>
								<div class="form-inline">
									<form:input path="memberZipCode" id="memberZipCode" type="text" class="form-control"/>
								</div>
							</div>
						</div>
						<div class="col-md-6">
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<form:label path="memberAddr1">주소</form:label>
								<form:input path="memberAddr1" required="true" class="form-control" readonly="true"/>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<form:label path="memberAddr2">상세주소</form:label>
								<form:input path="memberAddr2" required="true" class="form-control"/>
							</div>	
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<form:label path="memberCel">본인 휴대폰</form:label>
								<form:input path="memberCel" required="true" class="form-control phone-number-check" placeholder="본인 휴대폰(-없이 입력)"/>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<form:label path="memberEmail">본인 이메일</form:label>
								<form:input path="memberEmail" required="true" class="form-control" placeholder="본인 이메일"/>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<label>생년월일</label>
						</div>
						<div class="col-md-5">
							<div class="form-inline form-group">
								<input type="hidden" id="birth-date" name="birthDate"/>
								<form:hidden path="birthDay"/>
								<select id="birth-year" class="form-control teal">
									<option value="">선택 </option>
										<jsp:useBean id="now" class="java.util.Date" />
										<fmt:formatDate var="currentYear" value="${now}" pattern="yyyy" />
										<c:set var="defaultYear" value="${currentYear - 20 }"></c:set>
									<c:forEach var="y" begin="0" end="120">
										<option value="${currentYear - y}"
									<c:if test="${(currentYear-y) == fn:substring(member.birthDay, 0, 4) }">selected</c:if>>${currentYear -y }</option>
									</c:forEach>
								</select>
								<label>년</label>
								<select id="birth-month" class="form-control teal">
									<option value="">선택</option>
									<c:forEach var="i" begin="1" end="12">
										<option value="<fmt:formatNumber type="number" minIntegerDigits="2" value="${i}"/>"
										<c:if test="${i == fn:substring(member.birthDay, 4, 6) }">selected</c:if>>
										${i }
										</option>
									</c:forEach>
								</select>
								<label>월</label>
								<select id="birth-day" class="form-control teal">
									<option value="">선택</option>
									<c:forEach var="i" begin="1" end="31">
										<option value="<fmt:formatNumber type="number" minIntegerDigits="2" value="${i}"/>"
									<c:if test="${i == fn:substring(member.birthDay, 6, 8) }">selected</c:if>>
										${i }
										</option>
									</c:forEach>
								</select>
								<label>일</label>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<label>정보수신여부</label>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2">
							<div class="checkbox checkbox-teal">
								<input name="emailFlag" id="emailFlag" class="styled" type="checkbox" <c:if test="${member.emailFlag =='Y'}">checked</c:if>>
								<form:label path="emailFlag">이메일</form:label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="checkbox checkbox-teal">
								<input name="dmFlag" id="dmFlag" class="styled" type="checkbox" <c:if test="${member.dmFlag =='Y' }">checked</c:if>>
								<form:label path="dmFlag">뉴스레터</form:label>
							</div>
						</div>	
						<div class="col-md-2">
							<div class="checkbox checkbox-teal">
							  	<input name="smsFlag" id="smsFlag" class="styled" type="checkbox" <c:if test="${member.smsFlag =='Y' }">checked</c:if>>
								<form:label path="smsFlag">문자수신</form:label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">										
							<p class="help-block">
								* 국립과천과학관의 다양한 서비스를 받아보실 수 있습니다.<br>
								(교육예약 및 결제 확인, 이벤트, 과학정보 등을 받아 보실 수 있습니다.)
							</p>
						</div>
					</div>
					
					<div class="row margin-top30">
						<div class="col-md-12 center">
							<button id="update-btn" type="submit" class="btn btn-primary btn-wide">
								수정하기
							</button>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>

<script src="<c:url value="/resources/js/ext/jquery.fileupload/jquery.ui.widget.js"/>"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<c:url value="/resources/js/ext/jquery.fileupload/jquery.iframe-transport.js"/>"></script>
<!-- The basic File Upload plugin -->
<script src="<c:url value="/resources/js/ext/jquery.fileupload/jquery.fileupload.js"/>"></script>

<script>
/*jslint unparam: true */
/*global window, $ */
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
   	var url = baseUrl + "mypage/profileImage";
    
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        beforeSend: function(xhr) {
			ajaxLoading();
			xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));
		},
        done: function (e, data) {
        	ajaxUnLoading();
        	if(data.result != null && data.result.status == "complete") {
        		$("#profile-img").attr("src", baseUrl + baseFileUrl + data.result.fileUrl);
        	} else if(data.result != null && data.result.status == "error"){
        		alert(data.result.errMsg);
        	}
        },
        progressall: function (e, data) {
        	var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
</script>

<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script>
	var openDaumApi = false;
	$("#memberZipCode").focus(function(e){
		if (!openDaumApi){
			openDaumApi = true;
			
			new daum.Postcode({
		        oncomplete: function(data) {
		        	$("#memberZipCode").val(data.postcode);
		        	$("#memberAddr1").val(data.roadAddress).prop('readonly', true);
		        	$("#memberAddr2").focus();
		        	
		        	openDaumApi = false;
		        }
		    }).open();
		}	
	});
	
	$("#signup-form").submit(function(e){
		if ($("#passwordCheck").val() == "") {
			alert("회원정보 수정을 위해서는 비밀번호 입력이 필요합니다.");
			$("#passwordCheck").focus();
			e.preventDefault();
		} else {
			 var year = $("#birth-year").val();
			 var month = $("#birth-month").val();
			 var day = $("#birth-day").val();
			 
			var regNoDate = year.toString() + month.toString() + day;
			$("#birthDay").val(regNoDate)
		}
	});
</script>

<script type="text/javascript">
	
	
$(function(){
    $(".phone-number-check").on('keydown', function(e){
       // 숫자만 입력받기
        var trans_num = $(this).val().replace(/-/gi,'');
	var k = e.keyCode;
				
	if(trans_num.length >= 11 && ((k >= 48 && k <=126) || (k >= 12592 && k <= 12687 || k==32 || k==229 || (k>=45032 && k<=55203)) ))
	{
  	    e.preventDefault();
	}
    }).on('blur', function(){ // 포커스를 잃었을때 실행합니다.
        if($(this).val() == '') return;

        // 기존 번호에서 - 를 삭제합니다.
        var trans_num = $(this).val().replace(/-/gi,'');
      
        // 입력값이 있을때만 실행합니다.
        if(trans_num != null && trans_num != '')
        {
           // 총 핸드폰 자리수는 11글자이거나, 10자여야 합니다.
           if(trans_num.length==11 || trans_num.length==10) 
           {   
               // 유효성 체크
               var regExp_ctn = /^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})([0-9]{3,4})([0-9]{4})$/;
               if(regExp_ctn.test(trans_num))
               {
                   // 유효성 체크에 성공하면 하이픈을 넣고 값을 바꿔줍니다.
                   trans_num = trans_num.replace(/^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})-?([0-9]{3,4})-?([0-9]{4})$/, "$1-$2-$3");                  
                   $(this).val(trans_num);
               }
               else
               {
                   alert("유효하지 않은 전화번호 입니다.");
                   $(this).val("");
                   $(this).focus();
               }
           }
           else 
           {
               alert("유효하지 않은 전화번호 입니다.");
               $(this).val("");
               $(this).focus();
           }
      }
  });  
});
</script>