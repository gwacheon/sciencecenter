<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="container">
	<div class="row margin-top30">
		<div class="col-md-3">
			<c:import url="/WEB-INF/views/mypage/side.jsp">
				<c:param name="active" value="privateInfo"></c:param>
			</c:import>
		</div>
		
		<div class="col-md-9">
			<h3 class="page-title teal">
				연간회원 신청
			</h3>
			
			<div class="private-info-box">
				<div class="table-responsive table-teal">
					<table class="table">
						<thead>
							<tr>
								<th>이름</th>
								<th>회원번호</th>
								<th>구분</th>
								<th>관계</th>
								<th>선택</th>
							</tr>
						</thead>
						
						<tbody>
							<c:choose>
								<c:when test="${not empty memberWithFamily.families}">
									<c:forEach var="f" items="${memberWithFamily.families }">
										<tr>
											<td>${f.familyName}</td>
											<td>${f.familyNo}</td>
											<td>${f.membershipName}</td>
											<td>${f.relationTypeName}</td>
											<td>

												<input type="radio" name="radio_memberSelect" <c:if test="${f.membershipType == 'SV' || f.membershipType == 'SG'}">disabled</c:if>/>
												<input type="hidden" name="hidden_discount_flag" value="${f.discountFlag}" />
												<input type="hidden" name="hidden_membership_type" value="${f.membershipType}" />
											</td>
										</tr>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<tr>
										<td class="center" colspan="5">등록 된 가족 정보가 없습니다.</td>
									</tr>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
					<div class="center">
						<a id="add-family-modal-btn" class="btn btn-teal" data-toggle="modal">
							연간회원 신청
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>
<%-- <c:import url="/WEB-INF/views/myPayment/paymentChoiceModal.jsp"></c:import> --%>
<c:import url="/WEB-INF/views/myPayment/paymentModule.jsp"></c:import>

	
<script type="text/javascript">
	$(function(){
		
		//1. 연간회원 등록
		$("#add-family-modal-btn").click(function(e){
			
			var membershipPrice = "${membeship_price.get('MEMBERSHIP_PRICE')}";
			var totalAmt = membershipPrice;
			var family_no;
			var membership_type;
			
			if(!$('input:radio[name=radio_memberSelect]').is(':checked')) {
				alert("연간회원 신청자를 선택하세요.");
				return false;				
			}
			
			
			$('input:radio[name=radio_memberSelect]').each(function() {
	 			if($(this).is(':checked')){
	 				family_no = $(this).parents().prev().prev().prev().html();
	 				membership_type = $(this).next().next().val();
	 				if($(this).next().val() == "Y")	totalAmt = membershipPrice - (membershipPrice * 0.1);
	 			}
	 	   	});
			
			if(membership_type == 'FR' ) { // FR 무료회원
				var con_test = confirm("결제 후 7일 초과 시 환불되지 않으며, 결제 후 7일 이내라도 회원혜택을 받은 경우 환불되지 않습니다. \n\n 그래도 결제를 진행하시겠습니까?");
				if(con_test == true)
					setMallForm(totalAmt, family_no, membership_type);
				else if(con_test == false)
					return false;	
			} else if(membership_type == 'SV') { // SI 유료회원
				// 유료 회원에서 
			} else {
				alert( '연간 회원 -> 온라인 회원으로 전환만 할 수 있습니다.');
			} 
			
			//setMallForm(totalAmt, family_no, membership_type);
		});
		
		//2. 결제시 보내는 FORM 입력
		function setMallForm(TotalPrice, family_no, membership_type){
			
			var mallForm = document.mallForm;
			
			mallForm.APPLY_UNIQUE_NBR.value = "${tranUniqueNbr}";
			mallForm.TOTAL_PRICE.value 		= TotalPrice;
			mallForm.APPLY_CNT.value 		= 1;
			mallForm.PAYMENT_AMT.value 		= TotalPrice;
			mallForm.COURSE_NAME.value 		= "${pay_mbrName}";
			
			mallForm.productName.value  = "${pay_mbrName}";
			mallForm.productPrice.value = TotalPrice;
			mallForm.productCount.value = 1;
			mallForm.salesPrice.value   = TotalPrice;
			mallForm.oid.value   		= "${tranUniqueNbr}";
			mallForm.mbrId.value   		= "${pay_mbrId}";
			mallForm.mbrName.value   	= "${pay_mbrName}";
			mallForm.returnUrl.value   	= "${c2_homepage_url}/payresponse/setMembershipUpdate/?family_no=" + family_no+  "&member_no=${member.getMemberNo()}";	//리턴URL로 GUBUN:02-유료회워
			mallForm.payKind.value = "1";
			doPay();
		}
		
		//3. 결제가 진행된 후 setMembershipUpdate 에서 리턴값을 받는다. SERVER -> SERVER 로 공인 아이피에서 진행한 경우만 호출 됨.
		
	});
	
	//4. 결제가 끝나고 돌아가는 콜백함수(공용함수로 resource/payment1.3/paymentCallBack.jsp 에서 호출되는 함수)
	function CompleteProc(oid){
		//결제 및 연간회원 등록을 마치고 페이지를 다른곳으로 이동할 때 사용.
		window.location.reload(true);
	}
	
</script>