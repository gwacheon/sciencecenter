<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<link rel="stylesheet" href="<c:url value='/resources/css/ext/jquery.fileupload.css'/>">

<div class="container">
	<div class="row margin-top30">
		<div class="col-md-3">
			<c:import url="/WEB-INF/views/mypage/side.jsp">
				<c:param name="active" value="privateInfo"></c:param>
			</c:import>
		</div>
		
		<div class="col-md-9">
			<h3 class="page-title teal">
				회원탈퇴
			</h3>
			
			<div class="private-info-box">
				<c:url value="/mypage/dropout" var="action"></c:url>
				<form:form id="dropout-form"  class="" action="${action}" method="POST" commandName="member" modelAttribute="member">
					<form:input type="hidden" path="memberNo"/>
					<div class="row">
						<div class="col-md-offset-3 col-md-6">
							<div class="form-group">
								<label for="current-password">현재 비밀번호</label>
								<input name="password" type="password" id="password" class="form-control">
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-offset-3 col-md-6">
							<div class="form-group">
								<label for="dropReason">탈퇴사유</label>
								<textarea id="dropReason" name="dropReason" rows="5" class="form-control"></textarea>
							</div>
						</div>
					</div>
					
					<div class="row center">
						<div class="col-md-offset-3 col-md-6">
							<button id="submit-btn" type="submit" class="btn btn-teal">
								회원탈퇴
							</button>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>

${status }
<c:if test="${not empty status and status == 'complete' }">
	<script type="text/javascript">
		alert("그동안 과찬과학관 홈페이지를 이용해 주셔서 감사합니다. 보다 나은 서비스를 제공드리도록 노력하겠습니다.");
		location.href = baseUrl;
	</script>
</c:if>

<script type="text/javascript">
	
	$("#dropout-form").on('submit', function(e) {
		if ($("#password").val() == "") {
			e.preventDefault();
			$("#password").focus();
			alert("비밀번호는 필수 입력 항목 입니다.");
		}
	});
</script>