<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<ul class="side-menu-bar">
	<li>
		<a href="<c:url value='/mypage'/>" class="<c:if test="${param.active == 'mypage' }">active</c:if>">
			<spring:message code="mypage"/>
		</a>
	</li>
	<%-- 
	<li>
		<a href="<c:url value='/mypage/voluntaries'/>" class="<c:if test="${param.active == 'voluntaries' }">active</c:if>">
			<spring:message code="mypage.voluntaries"/>
		</a>
	</li>
	 --%>
	<li class="<c:if test="${param.active == 'reservations' }">active</c:if>">
		<a href="<c:url value='/payment'/>" class="<c:if test="${param.active == 'reservations' }">active</c:if>">
			예약내역
		</a>
		<%-- <ul class="nested-side-menu-bar">
			<li><a href="<c:url value='/mypage/reservations/orgApplicants'/>">단체예약</a></li>
		</ul> --%>
	</li>
	<li>
		<a href="<c:url value='/mypage/sangsang'/>" class="<c:if test="${param.active == 'sangsang' }">active</c:if>">
			무한상상 예약
		</a>
	</li>
	
	<li class="<c:if test="${param.active == 'privateInfo' }">active</c:if>">
		<a href="<c:url value='/mypage/privateInfo'/>" class="<c:if test="${param.active == 'privateInfo' }">active</c:if>">
			개인 정보
		</a>
		
		<ul class="nested-side-menu-bar">
			<li><a href="<c:url value='/mypage/privateInfo'/>">회원정보</a></li>
			<li><a href="<c:url value='/mypage/privateInfo/family'/>">가족정보</a></li>
			<li><a href="<c:url value='/mypage/updateMemberShip'/>">연간회원 신청</a></li>
			<%-- <li><a href="<c:url value='/mypage/privateInfo/fundMember'/>">후원회원 신청</a></li> --%>
			<li><a href="<c:url value='/mypage/changePassword'/>">비밀번호변경</a></li>
			<li><a href="<c:url value='/mypage/dropout'/>">회원탈퇴</a></li>
		</ul>
	</li>
</ul>
