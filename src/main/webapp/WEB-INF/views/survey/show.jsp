<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<div class="sub-content-nav scrollspy-orange">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>


<div class="sub-body">
	<div id="basic-info-spy" class="narrow-sub-top communication">
		<div class="sub-banner-tab-wrapper sub-banner-tab-orange">
			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="communication-body" class="container margin-bottom30">
		<div class="row">
			<div class="col-md-12">
				<h3 class="page-title orange">
					<div class="top-line"></div>
					 설문조사
				</h3>
			</div>
		</div>
		
		<table class="table survey-header-table margin-bottom30">
			<tbody>
				<tr>
					<th>제목</th>
					<td>${survey.title}</td>
				</tr>
				<tr>
					<th>기간</th>
					<td><fmt:formatDate value="${survey.beginTime}"
							pattern="yyyy-MM-dd" /> ~ <fmt:formatDate
							value="${survey.endTime}" pattern="yyyy-MM-dd" /></td>
				</tr>
			</tbody>
		</table>
		<div id="articles">
			<form id="reply-form">
				<c:forEach var="article" items="${survey.articles }">
					<div class="row">
						<div class="col-md-12">
							<c:choose>
								<c:when test="${article.qType == 'line' }">
									${article.question }<hr>
								</c:when>
								<c:otherwise>
									<div class="article-item">
										<div class="article-title">${article.question }</div>
										<c:choose>
											<c:when test="${article.qType == 'text' }">
												<div class="">
													<textarea rows="5" cols="" class="form-control reply-form"
														name="${article.id }" data-article-id="${article.id }"></textarea>
												</div>
											</c:when>
											<c:when test="${article.qType == 'select' }">
												<div class="row">
													<div class="col-md-12">
														<div class="radio radio-purple">
														<c:forEach var="selection" items="${article.selections }">
															<c:choose>
																<c:when test="${selection.directInput}">
																	<div class="row clearfix">
																		<div class="col-md-3">
																			<p>
																				<input name="${article.id }" type="radio"
																					id="selection-${selection.id }"
																					class="reply-form etc-radio"
																					data-article-id="${article.id }"
																					data-id="${selection.id }"
																					value="${selection.example }" /> <label
																					for="selection-${selection.id }">${selection.example }</label>
																			</p>
																		</div>
		
																		<div class="col-md-9">
																			<input type="text" class="etc-input-field"
																				data-id="${selection.id }"
																				data-article-id="${article.id }">
																		</div>
																	</div>
																</c:when>
																<c:otherwise>
																	<p
																		class="<c:if test="${article.renderType == 'horizontal' }">floated-selection</c:if>">
																		<input name="${article.id }" type="radio"
																			id="selection-${selection.id }" class="reply-form"
																			data-article-id="${article.id }"
																			value="${selection.example }" /> <label
																			for="selection-${selection.id }">${selection.example }</label>
																	</p>
																</c:otherwise>
															</c:choose>
														</c:forEach>
														</div>
													</div>
												</div>
											</c:when>
											<c:when test="${article.qType == 'multiple_select' }">
												<div class="row">
													<div class="col-md-12">
														<div class="checkbox checkbox-purple">
															<c:forEach var="selection" items="${article.selections }">
																<p
																	class="<c:if test="${article.renderType == 'horizontal' }">floated-selection</c:if>">
																	<input name="${article.id }" type="checkbox"
																		id="selection-${selection.id }" class="reply-form"
																		data-article-id="${article.id }"
																		value="${selection.example }" /> <label
																		for="selection-${selection.id }">${selection.example }</label>
																</p>
															</c:forEach>
														</div>
													</div>
												</div>
											</c:when>
										</c:choose>
									</div>	
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</c:forEach>
			</form>
		</div>

		<div class="row margin-bottom20">
			<div class="form-horizontal">
				<div class="col-sm-4 col-md-4">
					<label for="name-for-reply">성명</label> <input id="name-for-reply"
						type="text" class="validate form-control" name="name-for-reply">
	
				</div>
				<div class="col-sm-4 col-md-4">
					<label for="phone-for-reply">전화번호</label> <input
						id="phone-for-reply" type="text" class="validate form-control"
						name="phone-for-reply">
	
				</div>
				<div class="col-sm-4 col-md-4">
					<label for="email-for-reply">이메일</label> <input id="email-for-reply"
						type="text" class="validate form-control" name="email-for-reply">
				</div>
			</div>
		</div>

		<div class="row margin-bottom30">
			<div class="col-md-12">
				<button id="submit-survey-btn" type="button"
					class="right btn btn-primary">설문등록</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).on("click", ".etc-radio", function(e){
		var id = $(this).data("id");
		$(".etc-input-field[data-id='" + id + "']").focus();
		
	});
	
	$(document).on("blur", ".etc-input-field", function(e){
		var id = $(this).data("id");
		$(".etc-radio[data-id='" + id + "']").val($(this).val());
	});
	
	$(function() {
		var reply = {};
		var articleMap = {};
		
		<c:forEach var="article" items="${survey.articles }">
			<c:if test="${article.qType ne 'line'}">
				articleMap["${article.id}"] = [];
			</c:if>
		</c:forEach>
		
		$("#submit-survey-btn").click(function(e){
			e.preventDefault();
			$(this).prop("disabled",true);
			_.forEach($("#reply-form").serializeArray(), function(obj, idx){
				if(obj.value != ""){
					articleMap[obj.name].push(obj.value);
				}
			});
	
			var err = false;
			
			_.forEach(articleMap, function(val, key){
				if(val.length == 0){
					err = true;
				}
			});
			
			if(err){
				alert("모든 항목을 입력하셔야 설문에 참여가 됩니다.");
				$(this).prop("disabled",false);
			}
			
			/*
			if($("#name-for-reply").val() != ""){
				reply.name = $("#name-for-reply").val();
			}else{
				err = true;
			}
			
			if($("#phone-for-reply").val() != ""){
				reply.phone = $("#phone-for-reply").val();
			}else{
				err = true;
			}
			
			if($("#email-for-reply").val() != ""){
				reply.email = $("#email-for-reply").val();
			}else{
				err = true;
			}
			*/
			
			reply.name = $("#name-for-reply").val();
			reply.phone = $("#phone-for-reply").val();
			reply.email = $("#email-for-reply").val();
			
			if(!err){
				$.ajax({
					url: baseUrl + "surveys/${survey.id }/replies",
					contentType : "application/json; charset=utf-8",
					beforeSend: function(xhr) {
						ajaxLoading();
			            xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));
			        },
			        data: JSON.stringify({
			        	reply: reply,
			        	reply_details: articleMap
			        }),
			        type: "POST",
			        dataType: "JSON",
			        success: function(data){
			        	ajaxUnLoading();
			        	
			        	if(!data.error){
			        		alert("설문 조사가 정상적으로 등록 되었습니다.");
			        		reply = {};
				    		articleMap = {};
				    		history.back();
			        	}else{
			        		alert(data.error_msg);
			        	}
			        },
			        error: function(err){
			        	console.log(err);
			        }
				})
			}else{
				//alert("개인 정보를 입력해주셔야 설문에 참여가 됩니다.");
			}
			
			
		});
	});
</script>