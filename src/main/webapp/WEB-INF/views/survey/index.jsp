<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<div class="sub-content-nav scrollspy-orange">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>


<div class="sub-body">
	<div id="basic-info-spy" class="narrow-sub-top communication">
		<div class="sub-banner-tab-wrapper sub-banner-tab-orange">
			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="communication-body" class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="page-title orange">
					<div class="top-line"></div>
					 설문조사
				</h3>
			</div>			
		</div>
		<div class="row margin-bottom30">
			<div class="col-md-12 table-responsive">
				<table class="table table-orange centered-table margin-bottom30">
					<thead>
						<tr>
							<th width="30%">기간</th>
							<th>제목</th>
							<th>진행상황</th>
						</tr>
					</thead>
					
					<tbody>
						<c:forEach var="survey" items="${surveys }">
							<tr>
								<td>
									<fmt:formatDate value="${survey.beginTime}" pattern="yyyy-MM-dd"/>
									~
									<fmt:formatDate value="${survey.endTime}" pattern="yyyy-MM-dd"/>
								</td>
								<td class="non-active-title">
									<c:choose>
										<c:when test="${survey.availableType == 1 }">
											<a href="<c:url value='/surveys/${survey.id }'/>">
												${survey.title }
											</a>
										</c:when>
										<c:otherwise>
											${survey.title }
										</c:otherwise>
									</c:choose>
								</td>
								<td>
									<c:choose>
										<c:when test="${survey.availableType == 1 }">
											진행중
										</c:when>
										<c:when test="${survey.availableType == 2 }">
											예정
										</c:when>
										<c:otherwise>
											마감
										</c:otherwise>
									</c:choose>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		<div class="row table-orange">
			<div class="col-md-12">
				<c:import url="/WEB-INF/views/layouts/paginationModified.jsp" />
			</div>
		</div>
	</div>
</div>