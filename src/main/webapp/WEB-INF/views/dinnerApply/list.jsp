<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-green">
		<nav id="scrollspy-nav" class="navbar-default navbar-static has-scroll">
			<div class="container">
				<ul class="nav navbar-nav">
					<li>
						<a href="#basic-info-spy" class="item">
							석식신청(직원용) 
						</a>				
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<div class="bread-crumbs">
							석식신청(직원용)
						</div>
					</li>
				</ul>
			</div>
		</nav>
	</div>
	
	<div class="sub-body">
		<div class="container">
			<div id="basic-info-spy" class="section scrollspy">	
				<div class="row">
					<div class="col-xs-4 col-md-2">
						<h3 class="page-title green">
							<div class="top-line"></div>
							석식신청(직원용)
						</h3>
					</div>
				</div>
				<div class="row description">
					<div class="col-md-12">
						<p class="bg-primary bg-green" style="padding: 15px;">
							신청/취소 가능 시간 :
							<fmt:formatDate value="${dinnerApplyTime.applyBeginTime}" pattern="HH:mm"/> ~  
							<fmt:formatDate value="${dinnerApplyTime.applyEndTime}" pattern="HH:mm"/>
							
						</p>
					</div>
				</div>
				
				
				<div class="row">
					<div class="col-md-12">   
						<table class="table centered-table table-green">
							<thead>
								<tr>
									<th>신청일</th>
									<th>부서</th>
									<th>이름</th>
									<th>취소</th>
								</tr>
							</thead>
							<tbody>
								
								<c:choose>
									<c:when test="${empty dinnerApplies }">
										<tr>
											<td colspan="4" class="center-align">
												금일 신청 내역이 없습니다.
											</td>
										</tr>
									</c:when>
									<c:otherwise>  
										<c:forEach items="${dinnerApplies }" var="dinner">
											<tr>
												<td>
													<fmt:formatDate pattern="yyyy-MM-dd" value="${today}" />
												</td>   
												<td>  
													<c:out value="${dinner.department }" />
												</td>
												<td>
													<c:out value="${dinner.name }" />
												</td>
												<td>
													<c:url var="deleteUrl" value="/dinnerApply/${dinner.id }" />
													<form:form action="${deleteUrl }" method="delete" class="delete-form" data-id="${dinner.id }">
														<a data-id="${dinner.id}" data-department="${dinner.department }" 
															data-name="${dinner.name}" class="btn btn-danger btn-sm dinnerCancelBtn">
															취소
														</a>
													</form:form>
												</td>
											</tr>
										</c:forEach>
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 text-right">
						<a class="btn btn-primary btn-green" data-toggle="modal" data-target="#newDinnerModal">
							석식 신청
						</a>
					</div>
				</div>
				<div class="row table-green">
					<div class="col-md-12 text-center">
						<c:import url="/WEB-INF/views/layouts/admin/pagination.jsp"></c:import>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal Structure -->
<div id="newDinnerModal" class="modal fade">

	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">
					석식 신청
				</h4>
			</div>
			<div class="modal-body">
				<c:url value="/dinnerApply" var="createDinnerUrl" />
				<form:form id="dinnerApplyForm" name="dinnerApplyForm" 
					action="${createDinnerUrl}" method="POST" commandName="dinnerApply" 
					modelAttribute="dinnerApply" class="row">				
					<div class="col-md-12">
						<div class="form-group">
							<label for="applyDate">신청일</label>     
							<input id="applyDate" disabled type="text" value="<fmt:formatDate pattern='yyyy-MM-dd' value='${today}' />" class="form-control"/>
						</div>
		 			</div>
		 			<div class="col-md-12">
		 				<div class="form-group">
			  				<label for="department">부서</label>		 				
			 				<input id="department" type="text" name="department" class="validate form-control"/>
		 				</div>
		  			</div>   
		  			<div class="col-md-12">
		  				<div class="form-group">
			  				<label for="name">성명</label>
			  				<input id="name" type="text" name="name" class="validate form-control"/>
		  				</div>
		  			</div>
				</form:form>	
			</div>
			<div class="modal-footer">
				<button id="modalCloseBtn" data-dismiss="modal" class="btn btn-danger">
					취소
				</button>
				<a id="dinnerApplyBtn" class="modal-action btn btn-primary btn-green">
					신청        	
		      	</a>
			</div>
		</div>
	</div>
</div>




<script type="text/javascript">
	$( function() {
		
		applyDinner();
		cancelDinner();
	});
	
	function applyDinner() {
		$('#dinnerApplyBtn').on('click', function(e) {
			
			// get input values,
			var department = $('#department').val();
			var name = $('#name').val();
			
			var err = false;
			
			// 비어있는 필드가 없을 경우,
			if(isInputValid()) {
				$.ajax({
					url: baseUrl + "dinnerApply",
					contentType : "application/json; charset=utf-8",
					beforeSend: function(xhr) {
						ajaxLoading();
						xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));
					},
					data: JSON.stringify({
						dinnerApply: {
							'department': department,
							'name': name
						}
					}),
					type: "POST",
					dataType: "JSON",
					success: function(data) {
						ajaxUnLoading();
						// if the request fits all condition for applying Dinner,
						// there is no 'error' attribute from the response.
						if(!data.error) {
							alert("석식신청이 정상적으로 완료되었습니다.");
							$('#newDinnerModal').modal('hide')
							location.reload();
						}
						// if there is 'error' attribute,
						// show the error message which is set from the controller
						else {
							alert(data.error_msg);
						}						
					},
					error: function(err) {
						alert('ajax_error');
						
					}
				}) // ajax END...
			}
		});
		
	}
	
	function cancelDinner() {
		$('.dinnerCancelBtn').on('click', function(e) {
			e.preventDefault();
			var id = $(this).data('id');
			var department = $(this).data('department');
			var name = $(this).data('name');
			
			// current Time
			var currentTime = moment();
			
			// dinner Apply End Time
			var endHour = "<fmt:formatDate pattern='HH' value='${dinnerApplyTime.applyEndTime}' />";
			var endMin = "<fmt:formatDate pattern='mm' value='${dinnerApplyTime.applyEndTime}' />";
			var endSec = "<fmt:formatDate pattern='ss' value='${dinnerApplyTime.applyEndTime}' />";
			var endTime = moment();
			endTime.hour(endHour);
			endTime.minute(endMin);
			endTime.second(endSec);
			
			// 취소가능 시간이 지났는지 확인.
			var isAfter = currentTime.isAfter(endTime);
			
			// 취소 시간이 지났다면,
			if( isAfter ) {
				alert("신청시간내에만 취소가 가능합니다.");
			}
			else {
				if( confirm(department + ": " + name + " 의 석식 신청을 취소하시겠습니까?") ) {
					
					$(".delete-form[data-id='" + id + "']").submit();			
				}				
			}

		});
	}
	
	
	function isInputValid() {
		if( !$('#department').val() ) {
			alert("부서를 선택해주세요.");
			return false;
		}
		else if( !$('#name').val() ) {
			alert('성명을 기입해주세요.');
			return false;
		}
		else {
			return true;
		}
	}
	
	
</script>
