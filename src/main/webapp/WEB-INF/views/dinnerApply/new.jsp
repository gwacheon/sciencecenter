<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="sub-body">
	<div id="dinner-apply-form" class="container">
		<div class="row">
			<div class="col m6 offset-m3">
				<div class="row">
					<div class="col m4">
						<h4 class="section-title">석식 신청</h4>
					</div>
				</div>
				<c:url value="/dinnerApply" var="createDinnerUrl" />
				<form:form  name="dinnerApplyForm" action="${createDinnerUrl}" method="POST" commandName="dinnerApply" modelAttribute="dinnerApply">				
					<div class="input-field col s12">
						
						<input disabled type="text" value="<fmt:formatDate pattern='yyyy-MM-dd' value='${today}' />" />
	   					<label for="applyDate">신청일</label>     
	   				</div>
	   				<div class="input-field col s12">
	   					<input id="department" type="text" name="department" class="validate"/>
	   					<label for="department">부서</label>
	   				</div>   
	   				<div class="input-field col s12">
	   					<input id="name" type="text" name="name" class="validate"/>
	   					<label for="name">성명</label>
	   				</div>
	   				<div class="col s6">
	   					<div class="actions">
					      	<a href="<c:url value='/dinnerApply' />" class="btn waves-effect waves-light">
								목록으로 	
					      	</a>  	   					
	   					</div>
	   				</div>
	   				<div class="col s6 right-align">
	   					<div class="actions">
							<a id="dinnerApplyBtn" class="btn waves-effect waves-light">
								신청        	
					      	</a>
	   					</div>
	   				</div>
				</form:form>	
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$( function () {
		$('select').material_select();
		
		$('#dinnerApplyBtn').on('click', function() {
			// set start & end time
			var startMoment = moment(7, "HH");
			var endMoment = moment(15, "HH");
			// current time
			var currentMoment = moment();
			
			if( isInputValid() ) {
				// compare
				if( currentMoment > startMoment && currentMoment < endMoment ) {
					// submit the form
					document.dinnerApplyForm.submit();
				}
				else {
					alert("석식신청 가능 시간은 오전7시부터 오후3시까지만 가능합니다.");
				}				
			}
		});
		
		function isInputValid() {
			if( !$('#department').val() ) {
				alert("부서를 선택해주세요.");
				return false;
			}
			else if( !$('#name').val() ) {
				alert('성명을 기입해주세요.');
				return false;
			}
			else {
				return true;
			}
		}
		
	});
</script>
