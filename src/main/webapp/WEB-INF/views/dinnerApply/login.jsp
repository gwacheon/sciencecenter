<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div id="users-login-wrapper">
	<div id="login-banner">
		<h1 class="doc-title white center">국립과천과학관 석식신청 로그인</h1>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div id="login-form">
						<div class="row">
							<div class="col-md-12">
								<h3 class="doc-title teal center margin-bottom20">석식신청 로그인</h3>
							</div>
							<c:if test="${not empty error}">
								<div class="col-md-8 col-md-offset-2">
									<p class="bg-danger" style="padding: 15px;">
										<c:out value='${error }' />
									</p>
								</div>
							</c:if>
							<div class="col-md-8 col-md-offset-2">
								<c:url value="/dinnerApply/login" var="loginUrl" />
								<form:form id="dinner-apply-login-form" action="${loginUrl}" method="POST" commandName="dinnerApplyUser" modelAttribute="dinnerApplyUser">
									<div class="form-group">
									    <form:label path="userId">아이디</form:label>
				      					<form:input path="userId" autofocus="autofocus" class="form-control" data-name="아이디"/>
									</div>
									
									<div class="form-group">
									    <form:label path="password">비밀번호</form:label>
				      					<form:password path="password" class="form-control" data-name="비밀번호"/>
									</div>
									
									<div class="form-group center">
										<button id="login-btn" class="btn btn-primary wide-btn" type="submit" name="action">
								        	로그인
								      	</button>
									</div>
								</form:form>
							</div>
							
							<div class="col-md-12">
								<div class="line gray margin-bottom30"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$("form[id='dinner-apply-login-form']").submit( function(e) {
		/* var hasBlankInput = false;
		
		$("input.form-control").each( function() {
			if( $(this).val() == null || $(this).val() == "" ) {
				hasBlankInput = true;
				alert( $(this).data("name") + " 를 입력해주세요.");
				$(this).focus();
				return false;
			}
		});
		
		if(hasBlankInput) {
			return false;
		}
		else {
			return true;
		} */
		return true;
		
	});
</script>