<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="sub-content-nav scrollspy-orange">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>

<div id = "communication-wrapper" >
	
	<div id="introduction-body" class="sub-body">
		<div id="basic-info-spy" class="narrow-sub-top communication">
			<div class="sub-banner-tab-wrapper sub-banner-tab-orange">
				<div class="container">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div id = "board-spy" class="row">
				<div class="col-md-12">
					<h3 class="page-title orange">
						<div class="top-line"></div>
						 과학관 동영상
					</h3>
				</div>
			</div> 
			<div class="row">
				<div class="col-md-12">
					<div id="board-wrapper" class="table-orange">
						<c:url var="currentPath" value="/introduce/programIntro"/>
			 			
			 			<c:import url="/WEB-INF/views/boards/gallery.jsp">
			 				<c:param name="path" value="${currentPath }"/>
			 			</c:import>
		 			</div>
	 			</div>
			</div>
		</div> 
	</div>
</div> 