<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="sub-content-nav scrollspy-blue">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>

<div id="sub-cotent-wrapper">
	<div id="intro-body" class="sub-body">
		<div id="basic-info-spy" class="narrow-sub-top introduce">
			<div class="sub-banner-tab-wrapper sub-banner-tab-blue">
				<div class="container">
					<div class="row">
						
					</div>
				</div>
			</div>
		</div>
	
		<div id="history-spy" class="container scrollspy margin-bottom20">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title blue">
						<div class="top-line"></div>
						연혁
					</h3>
				</div>
			</div>
			<div id="history" class="row">
				<div class="col-md-12">
				</div>
			</div>
		</div>
		
		
		<div id="facility">
			<div class="container">
				<div id="facility-spy" class="row scrollspy">
					<div class="col-md-12">
						<h3 class="page-title blue">
							<div class="top-line"></div>
							시설개요
						</h3>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<div class="facility-row row">
							<div class="col-md-2">부지면적</div>
							<div class="col-md-10">243,970㎡</div>
						</div>
						<div class="facility-row row">
							<div class="col-md-2">건축연면적</div>
							<div class="col-md-10">52,487㎡</div>
						</div>
						<div class="facility-row row">
							<div class="col-md-2">층수</div>
							<div class="col-md-10">지하1층~지상3층</div>
						</div>
						<div class="facility-row row">
							<div class="col-md-2">최고높이</div>
							<div class="col-md-10">33.3m</div>
						</div>
						<div class="facility-row row">
							<div class="col-md-2">주요시설</div>
							<div class="col-md-10">
								주요시설옥내외 전시시설, 천체관, 천체관측소,<br>
								스페이스월드 생태체험학습장, 과학캠프장 등
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<button type="button" class="btn btn-teal" data-toggle="modal" data-target="#detail-map-modal">
  							과학관 상세 지도 보기
						</button>
					</div>
				</div>
			</div>
		</div>
		
		<%-- <div id="logos">
			<div class="container">
				<div id="logos-spy" class="row scrollspy">
					<div class="col-md-12">
						<h3 class="page-title blue">
							<div class="top-line"></div>
							국립과천과학관 로고
						</h3>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-4">
						<div class="logo-item">
							<h4 class="page-sub-title">01 심벌마크</h4>
							<div class="logo-img">
								<img alt="심벌마크" src="<c:url value='/img/intro/logos/logo01.png'/>"
									class="img-responsive">
								
								<div class="download-btns">
									<a href="<c:url value='/introduce/history/download/symbol_jpg'/>" class="half">
										<strong>Jpg</strong><br>이미지다운로드
									</a>
									<a href="<c:url value='/introduce/history/download/symbol_ai'/>" class="half">
										<strong>Ai</strong><br>원본 다운로드
									</a>
								</div>
							</div>
							<div class="logo-desc">
								우주를 유영하는 비행체 형상을 상징화 하여<br class="hidden-xs hidden-sm">
								미래세계로 향하는 첨단과학 이미지를 표현합니다.
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="logo-item">
							<h4 class="page-sub-title">02 레터마크</h4>
							<div class="logo-img">
								<img alt="레터마크" src="<c:url value='/img/intro/logos/logo02.png'/>"
									class="img-responsive">
								
								<div class="download-btns">
									<a href="<c:url value='/introduce/history/download/lettermark'/>" >
										<strong>Jpg</strong><br>이미지다운로드
									</a>
								</div>
							</div>
							<div class="logo-desc">
								Science + (audi)torium의 합성어로 다양한<br class="hidden-xs hidden-sm">
								과학문화 행사가 열리는 과학문화 체험공간인<br class="hidden-xs hidden-sm">
								국립과천과학관에서 과학에 대한 재미와 즐거움을<br class="visible-lg">
								마음껏 누리는 것을 표현합니다.
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="logo-item">
							<h4 class="page-sub-title">03 캐릭터</h4>
							<div class="logo-img">
								<img alt="캐릭터" src="<c:url value='/img/intro/logos/logo03.png'/>"
									class="img-responsive">
								
								<div class="download-btns">
									<a href="<c:url value='/introduce/history/download/character_jpg'/>" class="half">
										<strong>Jpg</strong><br>이미지다운로드
									</a>
									<a href="<c:url value='/introduce/history/download/character_ai'/>" class="half">
										Ai<br>원본 다운로드
									</a>
								</div>
							</div>
							<div class="logo-desc">
								물음표와 느낌표를 통해 과학에 대한 호기심을 가지고<br class="visible-lg">
								배우고 체험하면서 과학에 대한 깨달음와 즐거움을<br class="visible-lg">
								느끼는 것을 표현합니다.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> --%>
	</div>
</div>

<!-- 과학관 상세 지도 보기 Modal -->
<div class="modal fade" id="detail-map-modal" tabindex="-1">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">과학관 상세 지도</h4>
      </div>
      <div class="modal-body">
        <img src="<c:url value="/img/intro/detail-map.png"/>" alt="과학관 상세 지도" class="img-responsive"/>
      </div>
    </div>
  </div>
</div>

<script id="timeline-template" type="text/x-handlebars-template">
<ul class="timeline">
	{{#each timelineItems}}
		<li class="{{#if_even @index}}timeline-inverted{{/if_even}}">
			<div class="timeline-badge">
				&nbsp;
			</div>
			<div class="timeline-panel">
				<div class="timeline-heading">
					{{#each this}}
						<h4 class="timeline-title">
							<span class="year">{{year}}</span>
							<span class="month">{{month}}</span> 
							<span class="title">{{title}}</span>
						</h4>
					{{/each}}
				</div>
			</div>
		</li>
	{{/each}}
</ul>
</script>

<script type="text/javascript">
	Handlebars.registerHelper('if_even', function(conditional, options) {
	  if((conditional % 2) == 0) {
		  return options.inverse(this);
	  } else {
		  return options.fn(this);
	  }
	});

	var timelineItems = [
		[
			{year: 2016, month: "05", title: "책임운영기관 평가 최우수선정"}
		],
		[
			{year: 2015, month: "10", title: "제5대 조성찬 관장 취임"}
		],
		[
			{year: 2013, month: "10", title: "제4대 김선빈 관장 취임"}
		],
		[
		 	{year: 2011, month: "10", title: "제3대 최은철 관장 취임"},
		 	{month: "05", title: "책임운영기관 평가 최우수선정"}
		 ],
		[
		 	{year: 2009, month: "10", title: "제2대 이상희 관장 취임"},
		 	{month: "05", title: "과학관 관람객 백만명 돌파"}
		 ],
		 [
		 	{year: 2008, month: "11", title: "국립과천과학관 공식 개관"},
		 	{month: "10", title: "건축공사 준공식"},
		 	{month: "08", title: "제1대 장기열 관장 취임"}
		 ],
		 [
		 	{year: 2007, month: "09", title: "기관명칭 (국립과천과학관) 제정"}
		 ],
		 [
		 	{year: 2006, month: "12", title: "전시물 제작구매, 설치 계약 및 착수"}
		 ],
		 [
		 	{year: 2005, month: "06", title: "건축 기본설계 보완 및 실시설계 착수"}
		 ],
		 [
		 	{year: 2004, month: "12", title: "설계, 시공자 선정"}
		 ],
		 [
		 	{year: 2003, month: "12", title: "과학기술문화창달계획 반영(제14회 국가과학기술위원회)"}
		 ],
		 [
		 	{year: 2002, month: "02", title: "부지선정(과천시 과천동 서울대공원 인근 10만평)"}
		 ],
		 [
		 	{year: 2001, month: "04", title: "제34회 과학의 날 대통령 치사에서 국립과학관 건립 발표"}
		 ],
	];	
	
	var source   = $("#timeline-template").html();
	var template = Handlebars.compile(source);
	var html    = template({timelineItems: timelineItems});
	$("#history > div").append(html); 
</script>

<script type="text/javascript">
	$(document).ready(function() {
		
	});
</script>
