<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="sub-content-nav scrollspy-blue">
	<nav id="scrollspy-nav" class="navbar-default navbar-static has-scroll">
		<div class="container">
			<ul class="nav navbar-nav">
				<c:import url="/WEB-INF/views/intro/navbar.jsp"/>
				<li>
					<a href="#intro-comment-spy" class="item"> 인사말 </a>
				</li>
				<li>
					<a href="#intro-list-spy" class="item"> 기관장동정 </a>
				</li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li>
					<div class="bread-crumbs">
						과학관 소개 / 기관안내 / 관장소개
					</div>				
				</li>
			</ul>	
				
		</div>
	</nav>
</div>

<div id="intro-body" class="sub-body">
	<div id="intro-comment-spy">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title blue">
						<div class="top-line"></div>
						인사말
					</h3>
				</div>
			</div>
	
			<div id="chief-intro" class="row">
				<div class="col-md-3">
					<img alt="<spring:message code='intro.chief'/>"
						src="<c:url value='/img/intro/photo.png'/>"
						class="gray-circle-img responsive-img">
					<div class="chief-name">
						<spring:message code="intro.chief_html" />
					</div>
				</div>
	
				<div class="col-md-9 col-lg-9">
					<div class="chief-comment">
						<h4 class="page-sub-title">
							<spring:message code="intro.chief_message_title" />
						</h4>
	
						<p>
							<spring:message code="intro.chief_message_description" />
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="intro-list-spy">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title blue">
						<div class="top-line"></div>
						기관장 동정
					</h3>
				</div>
			</div>
			<div id="board-show" class="board-show-blue">
			
			</div>
		</div>
	</div>
</div>

<c:import url="/WEB-INF/views/boards/show.jsp"></c:import>

<script type="text/javascript">
var board = new Board(
        "#board-show",
        "<c:url value='/introduce/chief'/>",
        {
            id: <c:out value="${id}"/>,
        }
    );
board.renderBoardShow();
</script>