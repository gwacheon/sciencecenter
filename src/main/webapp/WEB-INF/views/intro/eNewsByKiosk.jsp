<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<style>
	#kiosk-title {
		background-color: #3d959e;
		color: white;
	}
	
	#kiosk-title h4.page-title {
		margin-top: 0;
		padding-top: 10px;
		padding-bottom: 10px;
		margin-bottom: 30px;
	}
	
	.pagination-wrapper {
		margin-top: 0;
	}
	
	.board-img {
		height: 134px;
		overflow: hidden;
	}
	
	.gallery-boards .board-gallery-item img {
		height: 134px;
	}
</style>

<div id="kiosk-title">
	<div class="">
		<h4 class="page-title text-center">과학관 소식지</h4>
	</div>
</div>

<div id="communication-library">
	<div id="support-body" class="">
		<div class="container">			
		 	<div class="row">
				<div class="col-md-12">
					<div id="board-wrapper" class="table-blue">
			 			<c:url var="currentPath" value="/introduce/eNews"/>
			 			
			 			<div class="row gallery-boards">
							<c:forEach var="board" items="${boards }">
								<div class="col-sm-6 col-md-4">
									<div class="board-gallery-item">
										<a target="_blank" href="${board.linkUrl }">
											<div class="board-img">
												<img src="<c:url value='${baseFileUrl}' />/${board.picture }" class="img-responsive" alt="${board.title }"/>
				                            </div>
				                            <div class="board-title">
				                            	${board.title }
				                            </div>
										</a>
									</div>
								</div>
							</c:forEach>
						</div>
						
						<div class="pagination-wrapper">
							<ul class="pagination">
								<c:if test="${page.beginPage > 1 }">
									<li>
										<a href="${currentPath }?page=1" data-page="1" class="page-link {{page.className}}">
											<i class="fa fa-chevron-left"></i>
										</a>
									</li>
									<li>
										<a href="${currentPath }?page=${page.beginPage - 1}" data-page="${page.beginPage - 1}" class="page-link"><i class="fa fa-ellipsis-h"></i></a>
									</li>
								</c:if>
								
								<c:forEach var="i" begin="${page.beginPage }" end="${page.endPage }" varStatus="loop">
									<c:choose>
										<c:when test="${i == page.currentPage }">
											<li class="active">
												<a href="#" class="page-link">${i }</a>
											</li>
										</c:when>
										<c:otherwise>
											<li>
												<a href="${currentPath }?page=${i }" class="page-link">${i }</a>
											</li>
										</c:otherwise>
									</c:choose>
								</c:forEach>
								
								<c:if test="${page.endPage < page.totalPages }">
									<li>
										<a href="${currentPath }?page=${page.endPage + 1}" data-page="${page.endPage + 1}" class="page-link"><i class="fa fa-ellipsis-h"></i></a>
									</li>
									<li>
										<a href="${currentPath }?page=${page.endPage }" data-page="${page.endPage }" class="page-link"><spring:message code="page.last" text=""/>
											<i class="fa fa-chevron-right"></i>
										</a>
									</li>
								</c:if>
							</ul>
						</div>
		 			</div>
				</div>
			</div>
		</div>
	</div>
</div>