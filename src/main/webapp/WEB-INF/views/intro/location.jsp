
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="sub-content-nav scrollspy-blue">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>

<div id="sub-content-wrapper">
	<div id="intro-body" class="sub-body">
		<div id="basic-info-spy" class="narrow-sub-top introduce">
			<div class="sub-banner-tab-wrapper sub-banner-tab-blue">
				<div class="container">
					<div class="row">
						
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div id="transportation-spy" class="row scrollspy">
				<div class="col-md-12">
					<h3 class="page-title blue">
						<div class="top-line"></div>
						교통안내
					</h3>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-8">
					<img alt="교통안내" src="<c:url value='/img/intro/location/location_map.png'/>"
						class="img-responsive">
				</div>
				<div class="margin-bottom20 visible-xs visible-sm"></div>
				<div class="col-md-4">
					<h4 class="page-sub-title">
						<i class="fa fa-map-marker" aria-hidden="true"></i> 주소
					</h4>
					
					<p class="location-info-desc">
						13817 경기도 과천시 상하벌로 110 국립과천과학관
					</p>
					
					<h4 class="page-sub-title">
						<i class="fa fa-phone" aria-hidden="true"></i> 전화번호
					</h4>
					
					<p class="location-info-desc">
						02-3677-1500
					</p>
					
					<h4 class="page-sub-title">
						<i class="fa fa-home" aria-hidden="true"></i> 홈페이지
					</h4>
					
					<p class="location-info-desc">
						http://www.sciencecenter.go.kr
					</p>
					
					<h4 class="page-sub-title">
						<i class="fa fa-clock-o" aria-hidden="true"></i> 이용시간
					</h4>
					
					<p class="location-info-desc">
						09:30 ~ 17:30 (화~금)<br>
						&middot; 월요일 휴관
					</p>
				</div>
				
				<div class="col-md-12">
					<div class="public-transportation">
						<h4 class="page-sub-title">
							<i class="fa fa-subway" aria-hidden="true"></i> 지하철 이용시
						</h4>
						
						<p class="location-info-desc">
							<strong>4호선 대공원역</strong> 6번 출구 바로 앞<br>
							<strong>4호선 경마공원역</strong> 5번 출구 (도보 약 10분 소요)
						</p>
						
						<h4 class="page-sub-title">
							<i class="fa fa-car" aria-hidden="true"></i> 자가용 이용시
						</h4>
						
						<div class="location-info-desc">
							<strong>01 과천/사당방면에서 오시는 길</strong>
							<div class="how-to-location">
								<span class="how-to-location-item">
									과천대로
								</span>
								<span class="how-to-location-right-arrow"> &gt; </span>
								<span class="how-to-location-item">
									대공원 방면으로 고가도로
								</span>
								<span class="how-to-location-right-arrow"> &gt; </span>
								<span class="how-to-location-item">
									대공원역 삼거리에서 좌회전
								</span>
								<span class="how-to-location-right-arrow"> &gt; </span>
								<span class="how-to-location-item">
									약 600m 진행 후 좌회전
								</span>
								<span class="how-to-location-right-arrow"> &gt; </span>
								<span class="how-to-location-item">
									주차장입구
								</span>
							</div>
							
							<strong>02 양재/선바위방면에서 오시는 길</strong>
							<div class="how-to-location">
								<span class="how-to-location-item">
									과천대로
								</span>
								<span class="how-to-location-right-arrow"> &gt; </span>
								<span class="how-to-location-item">
									경마공원역 방향으로 직진
								</span>
								<span class="how-to-location-right-arrow"> &gt; </span>
								<span class="how-to-location-item">
									과천과학관 출입구로 우회전
								</span>
								<span class="how-to-location-right-arrow"> &gt; </span>
								<span class="how-to-location-item">
									주차장입구
								</span>
							</div>
							
							<strong>03 수원/안양방면에서 오시는길</strong>
							<div class="how-to-location">
								<span class="how-to-location-item">
									과천대로
								</span>
								<span class="how-to-location-right-arrow"> &gt; </span>
								<span class="how-to-location-item">
									사당 방면으로 지하차도 옆길
								</span>
								<span class="how-to-location-right-arrow"> &gt; </span>
								<span class="how-to-location-item">
									대공원방면으로 우회전
								</span>
								<span class="how-to-location-right-arrow"> &gt; </span>
								<span class="how-to-location-item">
									주차장입구
								</span>
							</div>
							
							<strong>04 고속도로 이용시</strong>
							<div class="how-to-location">
								<div>
									영동, 경부 고속도로 이용 시
								</div>
								
								<div class="multiple-locations">
									<span class="how-to-location-item">
										양재IC
									</span>
									<span class="how-to-location-right-arrow"> &gt; </span>
									<span class="how-to-location-item">
										양재대로(과천방향)
									</span>
									<span class="how-to-location-right-arrow"> &gt; </span>
									<span class="how-to-location-item">
										선바위검문소에서 좌회전
									</span>
									<span class="how-to-location-right-arrow"> &gt; </span>
									<span class="how-to-location-item">
										경마공원
									</span>
									<span class="how-to-location-right-arrow"> &gt; </span>
									<span class="how-to-location-item">
										과천과학주차장
									</span>
								</div>
								
								<div>
									서해안고속도로 이용 시
								</div>
								<div class="multiple-locations">
									<span class="how-to-location-item">
										조남 JC
									</span>
									<span class="how-to-location-right-arrow"> &gt; </span>
									<span class="how-to-location-item">
										서울외곽순환도로(판교방향)
									</span>
									<span class="how-to-location-right-arrow"> &gt; </span>
									<span class="how-to-location-item">
										평촌IC지나서
									</span>
									<span class="how-to-location-right-arrow"> &gt; </span>
									<span class="how-to-location-item">
										학의분기점(과천방면)
									</span>
									<span class="how-to-location-right-arrow"> &gt; </span>
									<span class="how-to-location-item">
										과천터널 후 서울방향
									</span>
									<span class="how-to-location-right-arrow"> &gt; </span>
									<span class="how-to-location-item">
										오른쪽차선으로 진행
									</span>
									<span class="how-to-location-right-arrow"> &gt; </span>
									<span class="how-to-location-item">
										대공원방향으로 우회전
									</span>
									<span class="how-to-location-right-arrow"> &gt; </span>
									<span class="how-to-location-item">
										주차장입구
									</span>
								</div>	
							</div>
						</div>
						
						<div class="extra-info">
							* 주말 및 공휴일에는 과학관 주변이 매우 혼잡하오니 지하철 이용을 권장합니다.
						</div>
					</div>
				</div>
			</div>
			
			<div id="parking-spy" class="row scrollspy">
				<div class="col-md-12">
					<h3 class="page-title blue">
						<div class="top-line"></div>
						주차안내
					</h3>
				</div>
				
				<div class="col-md-12">
					<img alt="교통안내" src="<c:url value='/img/intro/location/parking-map.png'/>"
						class="img-responsive">
				</div>
				
				<div class="col-md-6 col-lg-3">
					<div class="parking-info-item">
						<div class="circle-item">
							<i class="fa fa-product-hunt" aria-hidden="true"></i>
						</div>
						
						국립과천과학관<br>
						주차장 수용가능 차량<br>
						총 1,112대
					</div>
				</div>
				
				<div class="col-md-6 col-lg-3">
					<div class="parking-info-item">
						<div class="circle-item">
							<i class="fa fa-car" aria-hidden="true"></i>
						</div>
						
						승용차 주차구역 총 1,016대<br>
						중앙주차장 : 412대<br>
						동주차장 : 86대<br>
						서주차장 : 518대
					</div>
				</div>
				
				<div class="col-md-6 col-lg-3">
					<div class="parking-info-item">
						<div class="circle-item">
							<i class="fa fa-bus" aria-hidden="true"></i>
						</div>
						
						대형 주차구역 총 40대<br>
						대형차량은<br>
						제 2출입구를<br>
						이용하시기 바랍니다.
					</div>
				</div>
				
				<div class="col-md-6 col-lg-3">
					<div class="parking-info-item">
						<div class="circle-item">
							<i class="fa fa-wheelchair" aria-hidden="true"></i>
						</div>
						
						장애인 주차구역 총 44대<br>
						중앙주차장 : 17대<br>
						동주차장 : 3대<br>
						서주차장 : 24대<br>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table centered-table parking-price-table">
							<thead>
								<tr>
									<th>구분</th>
									<th>요금(일일기준)</th>
									<th>비고</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>일반차량</td>
									<td>5,000원</td>
									<td rowspan="5" class="left bottom-teal-line">
										<div class="parking-infos">
											<span class="right-arrow">&gt;</span>이용시간
											
											<div class="parking-etc-info">
												입차 : 9:00 ~ 17:30<br>
												출차 : 9:00 ~ 20:00	
											</div>
										</div>
										
										<div class="parking-infos">
											<span class="right-arrow">&gt;</span>야간개방 시 주차장 운영시간 : 09:00 ~22:00
										</div>
										
										<div class="parking-infos">
											<span class="right-arrow">&gt;</span>15:00시 이후 입차시 50% 할인(전차종)
											<div class="parking-etc-info">
												단, 기존 할인율 적용차량 제외
											</div>
										</div>
										
										<div class="parking-infos">
											<span class="right-arrow">&gt;</span>신용카드, 교통카드 결제 가능
										</div>
										
									</td>
								</tr>
								<tr>
									<td>대형(25인승 이상)</td>
									<td>10,000원</td>
								</tr>
								<tr>
									<td>경차(1000cc 이하) 및<br>친환경차(하이브리드)</td>
									<td>정산금액의 50% 할인</td>
								</tr>
								<tr>
									<td>유료 과학교육 프로그램 수강생<br>(교육당일에 한함, 단체수업 제외)</td>
									<td>평일 무료, 주말 또는 공휴일 50% 할인</td>
								</tr>
								<tr>
									<td class="bottom-teal-line">장애인 차량</td>
									<td class="bottom-teal-line">주차요금 면제<br>장애인 복지카드 및 장애등록 차량으로 확인</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>