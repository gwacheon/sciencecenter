<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="sub-content-nav scrollspy-blue">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>

<div id="intro-body" class="sub-body">
	<div id="basic-info-spy" class="narrow-sub-top introduce">
		<div class="sub-banner-tab-wrapper sub-banner-tab-blue">
			<div class="container">
				<div class="row">
					
				</div>
			</div>
		</div>
	</div>
	<div id="intro-comment-spy">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title blue">
						<div class="top-line"></div>
						인사말
					</h3>
				</div>
			</div>
	
			<div id="chief-intro" class="row">
				<div class="col-md-3">
					<img alt="<spring:message code='intro.chief'/>"
						src="<c:url value='/img/intro/photo.png'/>"
						class="gray-circle-img responsive-img">
					<div class="chief-name">
						<spring:message code="intro.chief_html" />
					</div>
				</div>
	
				<div class="col-md-9 col-lg-9">
					<div class="chief-comment">
						<h4 class="page-sub-title">
							<spring:message code="intro.chief_message_title" />
						</h4>
	
						<p>
							<spring:message code="intro.chief_message_description" />
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>