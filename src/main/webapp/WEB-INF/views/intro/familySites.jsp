<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="sub-content-nav scrollspy-blue">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>

<div id="sub-content-wrapper">
	
	<div id="intro-body" class="sub-body">
		<div id="basic-info-spy" class="narrow-sub-top introduce">
			<div class="sub-banner-tab-wrapper sub-banner-tab-blue">
				<div class="container">
					<div class="row">
						
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div id="surround-spy" class="row scrollspy">
				<div class="col-md-12">
					<h3 class="page-title blue">
						<div class="top-line"></div>
						관련사이트
					</h3>
				</div>
			</div>
		</div>
		
		<div class="container family-site-links">
			<div class="row">
				<div class="col-sm-12">
					<h4 class="teal">
						국립과학관 / 박물관
					</h4>
				</div>
				
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.science.go.kr/">국립중앙과학관</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.rda.go.kr/aeh/user/main.do">농업과학관</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.forest.go.kr/kna/">산림박물관</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.ssm.go.kr/">국립서울과학관	</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.forest.go.kr/newkfsweb/kfs/idx/Index.do?mn=KFS_01">국립산림과학원</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://fsm.nfrda.re.kr/">수산과학관</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.sciencecenter.or.kr/">국립광주과학관</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.dnsm.or.kr/index.do">국립대구과학관</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.sciport.or.kr/front/index.do">국립부산과학관</a></div>
				
			</div>
			
			<div class="row">
				<div class="col-sm-12">
					<h4 class="teal">
						공립과학관 / 박물관
					</h4>
				</div>
				
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://sea.busan.go.kr/">부산해양자연사박물관</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.dise.go.kr/">대구광역시과학교육원</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.coal.go.kr/">문경석탄박물관</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.daegu.go.kr/Childhall/">대구어린이회관</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://gise.gen.go.kr/home/">광주교육과학연구원</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.des.re.kr/Good/">대전교육과학연구원</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://corp.ttdc.kr/muse/muse01.aspx">통영수산과학관</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="https://us.ulsanedu.kr/">울산광역시교육연구정보원</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.railroadmuseum.co.kr/xe/movie">철도박물관</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.1stcoal.go.kr/CmsHome/MainDefault.aspx">보령석탄박물관</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.gise.kr/">경기도과학교육원</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://star.metro.daejeon.kr/">대전시민천문대</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.keric.or.kr/">강원도교육연구원</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.cbesr.go.kr/main.php">충청북도교육과학연구원</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.jmfsm.or.kr/">전라남도 해양수산과학관</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://museum.jeju.go.kr/">제주도민속자연사박물관</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://jise.kr/index.jsp">전북과학교육원</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://astronomy.seogwipo.go.kr/">서귀포천문과학문화관</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.ssp.re.kr/main/main.jsp">서울과학전시관</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.jhstar.kr/index.do">장흥정남진천문과학관</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="https://www.bise.go.kr/main.asp">부산과학교육원</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.gsei.kr/">경북과학교육원</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.ckobs.kr/">양구국토정중앙천문대</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="https://real.childpia.kr/">부산어린이회관</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.gnse.kr/">경남과학교육원</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.ienet.re.kr/">인천교육과학연구원</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.cisec.or.kr/home/home.jsp">제주교육과학연구원</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.ntam.org/">별새꽃돌자연탐사과학관</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.sasm.or.kr/">신라역사과학관</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.lgsh.co.kr/">LG사이언스홀</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://hlsi.co.kr/main/main.php">한생연 4대 테마과학박물관</a></div>
			</div>
			
			<div class="row">
				<div class="col-sm-12">
					<h4 class="teal">
						미국과학관 / 박물관
					</h4>
				</div>
				
				<div class="col-sm-6 col-lg-4"><a href="http://www.nasm.si.edu/">미국 국립항공우주박물관 (National Air and Space Muse)</a></div>
				<div class="col-sm-6 col-lg-4"><a href="http://www.amnh.org/">미국 자연사박물관 (American Museum of Natural History)</a></div>
				<div class="col-sm-6 col-lg-4"><a href="http://www.exploratorium.edu/">익스플로래토리움 (Exploratorium)</a></div>
				<div class="col-sm-6 col-lg-4"><a href="http://www.msichicago.org/">시카고 과학산업박물관 (Museum of Sience and Industry)</a></div>
				<div class="col-sm-6 col-lg-4"><a href="http://www.mos.org/">보스톤 과학박물관 (Museum of Science)</a></div>
			</div>
			
			<div class="row">
				<div class="col-sm-12">
					<h4 class="teal">
						영국과학관 / 박물관
					</h4>
				</div>
				
				<div class="col-sm-6 col-lg-4"><a href="http://www.sciencemuseum.org.uk/">영국 국립과학박물관 (Science Museum)</a></div>
				<div class="col-sm-6 col-lg-4"><a href="http://www.nhm.ac.uk/">영국 런던 자연사박물관 (Natural History Museum)</a></div>
			</div>
			
			<div class="row">
				<div class="col-sm-12">
					<h4 class="teal">
						프랑스과학관 / 박물관
					</h4>
				</div>
				
				<div class="col-sm-6 col-lg-4"><a href="http://www.cite-sciences.fr/erreurs/la-page-nexiste-pas.html">프랑스 라빌레뜨 과학산업관 (Cite des Science et de L'Industrie)</a></div>
			</div>
			
			<div class="row">
				<div class="col-sm-12">
					<h4 class="teal">
						일본과학관 / 박물관
					</h4>
				</div>
				
				<div class="col-sm-6 col-lg-4"><a href="http://www.kahaku.go.jp/">일본 국립과학박물관 (National Musem of Nature and Science)</a></div>
				<div class="col-sm-6 col-lg-4"><a href="http://www.jsf.or.jp/">일본 과학기술관 (Science Museum)</a></div>
				<div class="col-sm-12"><a href="http://www.miraikan.jst.go.jp/">일본 과학미래관 (National Msuseum of Emerging Science and Innovation)</a></div>
			</div>
			
			<div class="row">
				<div class="col-sm-12">
					<h4 class="teal">
						독일과학관 / 박물관
					</h4>
				</div>
				
				<div class="col-sm-12"><a href="http://www.deutsches-museum.de/">독일 과학박물관 (Deutsches Museum)</a></div>
			</div>
			
			<div class="row">
				<div class="col-sm-12">
					<h4 class="teal">
						스페인과학관 / 박물관
					</h4>
				</div>
				
				<div class="col-sm-12"><a href="http://www.cac.es/">발렌시아 과학관 (Ciudad de las Artes y las Ciencias)</a></div>
			</div>
			
			<div class="row">
				<div class="col-sm-12">
					<h4 class="teal">
						중국과학관 / 박물관
					</h4>
				</div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.cstm.org.cn/">중국 과학기술관</a></div>
				<div class="col-sm-4 col-md-3 col-lg-2"><a href="http://www.sstm.org.cn/kjg_Web/html/defaultsite/portal/index/index.htm">중국 상하이과학기술관</a></div>
			</div>
		</div>
	</div>
</div>
