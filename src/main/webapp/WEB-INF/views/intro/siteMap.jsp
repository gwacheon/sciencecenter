<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="sub-content-nav scrollspy-blue">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>

<div id="sub-content-wrapper">
	
	<div id="intro-body" class="sub-body">
		<div id="basic-info-spy" class="narrow-sub-top introduce">
			<div class="sub-banner-tab-wrapper sub-banner-tab-blue">
				<div class="container">
					<div class="row">
						
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div id="surround-spy" class="row scrollspy">
				<div class="col-md-12">
					<h3 class="page-title blue">
						<div class="top-line"></div>
						관련사이트
					</h3>
				</div>
			</div>
		</div>
		
		<div id="sitemap" class="container">
			<c:forEach var="i" begin="1" end="8">
				<spring:message var="main_category" code="main.nav.catetory${i }" scope="application"/>
				<spring:message var="main_category_url" code="main.nav.catetory${i }.url" scope="application"/>
				<c:if test="${not empty main_category}">
					<div class="row">
						<div class="col-md-12">
							<div class="main-category">
								<h4 class="main-category-title">
									<c:out value="${main_category }"/>
								</h4>
								<div class="row">
									<c:forEach var="j" begin="1" end="12">
										<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
											<spring:message var="sub_category" code="main.nav.catetory${i}.sub${j}" scope="application" text='' />
											<spring:message var="sub_category_url" code="main.nav.catetory${i}.sub${j}.url" scope="application" text='' />
									 		<c:if test="${not empty sub_category}">
									 			<c:choose>
										 			<c:when test="${sub_category_url eq 'scheduleScript' }">
							 							<spring:message var="academy_cd" code="main.nav.catetory${i}.sub${j}.academy_cd" scope="application" text='' />
							 							<!-- 예약신청의 javascript를 통해 페이지를 들어가야할 경우 판단 -->
							 							<a href="javascript:void(0)" onclick="AcademyChoiceFromTopMenu('${academy_cd}')" class="sub-title">
									 						<c:out value="${sub_category }"/>
									 					</a>
							 						</c:when>
							 						<c:otherwise>
									 					<a href="<c:url value='${sub_category_url }' />" class="sub-title" data-sub-category-id="${j }">
									 						<c:out value="${sub_category }"/>
									 					</a>
							 						</c:otherwise>
						 						</c:choose>
						 						
						 						<div class="sub-item-wrapper">
							 						<c:choose>
														<c:when test="${ i == 4 && j == 2 }">
															<c:forEach items="${navMainEvents }" var="mainEvent">
																<c:choose>
										                        	<c:when test="${mainEvent.subType eq 'link' }">
										                                <a href="<c:url value='${mainEvent.linkUrl }' />" class="sub-item" target="_blank">
										                                    <c:out value ="${mainEvent.name }"/>
										                                </a>
										                        	</c:when>
										                        	<c:otherwise>
										                        		<a href="<c:url value="/mainEvents/${ mainEvent.id }" />" class="sub-item">
										                                    <c:out value ="${mainEvent.name }"/>
										                                </a>
										                        	</c:otherwise>
									                        	</c:choose>
															</c:forEach>
														</c:when>
														<c:otherwise>
										 					<c:forEach var="k" begin="1" end="9">
										 						<spring:message var="sub_item" code="main.nav.catetory${i}.sub${j}.sub${k}" scope="application" text='' />
																<spring:message var="sub_item_url" code="main.nav.catetory${i}.sub${j}.sub${k}.url" scope="application" text='' />
																<c:if test="${not empty sub_item}">
																	<div class="">
																		<a href="<c:url value="${sub_item_url }"/>" class="sub-item" >
																			<c:out value="${sub_item }"/>
											 							</a>	
																	</div>
																</c:if>
										 					</c:forEach>
										 				</c:otherwise>
													</c:choose>
												</div>
									 		</c:if>			
										</div>
									</c:forEach>
								</div>
								
							</div>
						</div>
					</div>
					
				</c:if>	
			</c:forEach>
		</div>
	</div>
</div>
