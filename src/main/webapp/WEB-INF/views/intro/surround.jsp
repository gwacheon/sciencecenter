<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="sub-content-nav scrollspy-blue">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>

<div id="sub-content-wrapper">
	<div id="intro-body" class="sub-body">
		<div id="basic-info-spy" class="narrow-sub-top introduce">
			<div class="sub-banner-tab-wrapper sub-banner-tab-blue">
				<div class="container">
					<div class="row">
						
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div id="surround-spy" class="row scrollspy">
				<div class="col-md-3">
					<h3 class="page-title teal">
	<div class="top-line"></div>
						주변정보
					</h3>
				</div>
			</div>
		</div>
		
		<div class="odd-surround surround-row">
			<div class="container">
				<div class="row">
					<div class="col-md-6 surround-img-wrapper">
						<div class="surround-img">
							<img alt="과천경마장" src="<c:url value='/img/intro/surround/surrounding-information-img01.png'/>"
								class="img-responsive">
						</div>
					</div>
					<div class="col-md-6 surround-text-wrapper">
						<div class="surround-title">
							과천경마장
						</div>
						
						<div class="surround-desc">
							KRA 한국마사회는,<br class="visible-lg"/>
							국가공익사업인 경마의 시행을 통하여 국민에게 건전한 여가와 레저공간을 제공하며,<br class="visible-lg"/>
							레저세,교육세 등으로 국가재정에 기여함은 물론 수익금의 사회 환원을 통하여 공익증진에<br class="visible-lg"/>
							기여하고 있다.<br class="visible-lg"/>
							경마는 1차산업에서 4차산업을 아우르는 복합산업으로 이들 산업을 움직이는 동력이다.<br class="visible-lg"/>
							현재 1000여개 농가에서 2만여두의 말을 사육하고 있는 농업계에서는 KRA 한국마사회의<br class="visible-lg"/>
							농축산지원에 의존하는 바가 매우 크다.
						</div>
						
						<a href="http://www.kra.co.kr/main.do" class="margin-bottom30 btn btn-primary" target="_blank">
							<i class="fa fa-home" aria-hidden="true"></i> 홈페이지
						</a>
					</div>
				</div>
			</div>
		</div>
		
		<div class="even-surround surround-row">
			<div class="container">
				<div class="row">
					<div class="col-md-6 surround-img-wrapper">
						<div class="surround-img">
		                    <img alt="서울랜드" src="<c:url value='/img/intro/surround/surrounding-information-img02.png'/>"
		                        class="img-responsive">
	                    </div>
	                </div>
					<div class="col-md-6 surround-text-wrapper">
						<div class="surround-title">
							서울랜드
						</div>
						
						<div class="surround-desc">
							1988년 5월, 서울 올림픽을 앞두고 문을 연 서울랜드는,<br class="visible-lg"/>
							국내 최초 테마파크로 세계의 광장, 모험의 나라, 환상의 나라 및 삼천리 동산 등<br class="visible-lg"/>
							각각 특색을 지니고 있는 다섯 개의 테마구역으로 이루어져 있습니다.<br class="visible-lg"/>
							매년 튤립, 벚꽃, 국화 등 화려한 꽃 축제와 함께 다양한 놀이시설 및 공연, 전시 이벤트 등이<br class="visible-lg"/>
							펼쳐지고, 특히 야간개장 시즌에는 불꽃놀이&레이져쇼로 시민들에게 즐거움을 전합니다.<br class="visible-lg"/>
							언제나 행복과 즐거움이 넘치는 서울랜드는 일상에서 지친 삶을 재충전할 수 있는 <br class="visible-lg"/>여러분들의 휴식 공간 입니다.
						</div>
						
						<a href="http://www.seoulland.co.kr/" class="margin-bottom30 btn btn-blue" target="_blank">
							<i class="fa fa-home" aria-hidden="true"></i> 홈페이지
						</a>
					</div>
				</div>
			</div>
		</div>
		
		<div class="odd-surround surround-row">
			<div class="container">
				<div class="row">
					<div class="col-md-6 surround-img-wrapper">
						<div class="surround-img">
							<img alt="국립현대미술관" src="<c:url value='/img/intro/surround/surrounding-information-img03.png'/>"
								class="img-responsive">
						</div>
					</div>
					
					<div class="col-md-6 surround-text-wrapper">
						<div class="surround-title">
							국립현대미술관
						</div>
						
						<div class="surround-desc">
							국립현대미술관은 한국 근·현대 미술과 해외의 우수한 미술작품을 소장, 전시하고 있는<br class="visible-lg"/>
							국내 유일의 국립미술관 이다.<br class="visible-lg"/>
							어린이미술관을 포함하여 총 9개의 전시실을 운영 하고 있으며 각 전시실에서는<br class="visible-lg"/>
							미술관의 소장 작품뿐 아니라 국내외 유명 작가들의 작품을 선보이는 기획 전시가 진행된다.<br class="visible-lg"/>
							방학 기간에는 어린이를 위한 특별전시가 진행된다.<br class="visible-lg"/>
							청계산을 배경으로 위치한 국립현대미술관 야외조각공원을 걷다보면 국내외 유명 작가들의 조각,<br class="visible-lg"/>
							설치작품을 만날 수 있다.
						</div>
						
						<a href="http://www.mmca.go.kr/" class="margin-bottom30 btn btn-primary" target="_blank">
							<i class="fa fa-home" aria-hidden="true"></i> 홈페이지
						</a>
					</div>
				</div>
			</div>
		</div>
		
		<div class="even-surround surround-row">
			<div class="container">
				<div class="row">
					<div class="col-md-6 surround-img-wrapper">
						<div class="surround-img">
		                    <img alt="서울대공원" src="<c:url value='/img/intro/surround/surrounding-information-img04.png'/>"
		                        class="img-responsive">
	                    </div>
	                </div>
					<div class="col-md-6 surround-text-wrapper">
						<div class="surround-title">
							서울대공원
						</div>
						
						<div class="surround-desc">
							서울대공원은 다양한 즐거움을 골라 맛보는 곳.<br class="visible-lg"/>
							아름다운 천혜의 대자연 속에 가족학습, 자연문화 오락공간 등 다양한 테마로 꾸며진 국제적인 명소로 각광을 받는 곳입니다.<br class="visible-lg"/>
							오늘날 세계 10대 동물원의 규모를 자랑하는 세계 속의 공원인 서울대공원 동물원엔 현재 세계적<br class="visible-lg"/>
							희귀종인 로랜드고릴라를 비롯해 세계 각국 동물들이 원산지와 생태·계통별로 나뉘어져<br class="visible-lg"/>
							자연생태에 가깝게 보호·관리되고 있습니다.<br class="visible-lg"/>
							특히 연중 사계절 야외에서 생활하는 동물들을 벗삼아 즐기는 체험학습의 명소뿐만 아니라<br class="visible-lg"/>
							국내 최대규모의 『장미원 축제』를 비롯해 『한여름밤의 별밤축제』, 『버스타고 즐기는 따뜻한<br class="visible-lg"/>
							동물원으로의 겨울여행』 등 화려한 축제가 즐거움을 더해주고 있습니다.<br class="visible-lg"/>
							자연과 인간이 함께 살아 숨쉬는 세계 속의 관광지인 서울대공원은 앞으로도 최고를 향해<br class="visible-lg"/>
							발돋움 할 것입니다.
	
						</div>
						
						<a href="http://grandpark.seoul.go.kr/main.do" class="margin-bottom30 btn btn-blue" target="_blank">
							<i class="fa fa-home" aria-hidden="true"></i> 홈페이지
						</a>
					</div>
				</div>
			</div>
		</div>
		
		<div class="odd-surround surround-row">
			<div class="container">
				<div class="row">
					<div class="col-md-6 surround-img-wrapper">
						<div class="surround-img">
							<img alt="한국 카메라 박물관" src="<c:url value='/img/intro/surround/surrounding-information-img05.png'/>"
								class="img-responsive">
						</div>
					</div>
					
					<div class="col-md-6 surround-text-wrapper"> 
						<div class="surround-title">
							한국 카메라 박물관
						</div>
						
						<div class="surround-desc">
							한국 카메라 박물관은 카메라와 관련된 자료를 전문적으로 전시하는 사립 박물관이다.<br class="visible-lg"/>
							2개의 기획전시관과 주전시관, 부전시관으로 이루어져 있으며 소장품은 총 1,500여 점에 달한다.<br class="visible-lg"/>
							그밖에 임대한 세계 희귀 카메라 150여 점, 액세서리·사진 및 유리건판 필름 150점,<br class="visible-lg"/>
							카메라 관련 희귀 서적 10종 등이 전시되어 있다.<br class="visible-lg"/>
							전시된 대부분의 카메라 내부를 볼 수 있도록 전시 공간을 배치한 것이 특징이며<br class="visible-lg"/>
							개인 카메라 박물관으로는 세계 5위 안에 들 정도로 박물관 규모가 크고, 전시 품목도 다양하다.
						</div>
						
						<a href="http://www.kcpm.or.kr/" class="margin-bottom30 btn btn-primary" target="_blank">
							<i class="fa fa-home" aria-hidden="true"></i> 홈페이지
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>