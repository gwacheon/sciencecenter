<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="sub-content-nav scrollspy-blue">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>

<div id="intro-body" class="sub-body">
	<div id="basic-info-spy" class="narrow-sub-top introduce">
		<div class="sub-banner-tab-wrapper sub-banner-tab-blue">
			<div class="container">
				<div class="row">
					
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row"> 
			<div class="col-md-12">
				<h3 class="page-title blue">
					<div class="top-line"></div>
					조직 및 업무
				</h3>
			</div>
		</div>
		<div id="organization-chart" class="row hidden-sm hidden-xs">
			<div class="col-md-12">
				<div id="first-node" class="organization-item" onclick="" data-org="org1">
					국립과천과학관장
				</div>
				<div id="first-node-vertical-line"></div>
				<div id="second-node-vertical-line"></div>
				<div id="node-line1" class="node-line"></div>
				<div id="node-line2" class="node-line"></div>
				<div id="node-line3" class="node-line"></div>
				<div id="node-line4" class="node-line"></div>
				<div id="node-line5" class="node-line"></div>
				<div id="node-line6" class="node-line"></div>
				<div id="node-line7" class="node-line"></div>
				
				<div id="second-node" class="organization-item" onclick="" data-org="org2">
					전시 <br>연구단장
				</div>
				
				<div class="organization-item third-node node1 left-side" onclick="" data-org="org3">
					경영<br>기획과
				</div>
				<div class="organization-item third-node node2 left-side" onclick="" data-org="org4">
					고객<br>창출과
				</div>
				<div class="organization-item third-node node3 left-side" onclick="" data-org="org5">
					운영<br>지원과
				</div>
				
				<div class="organization-item third-node node4" onclick="" data-org="org6">
					전시<br>기획연구과
				</div>
				<div class="organization-item third-node node5" onclick="" data-org="org7">
					전시<br> 운영총괄과
				</div>
				<div class="organization-item third-node node6" onclick="" data-org="org8">
					창조<br> 전시관리팀
				</div>
				<div class="organization-item third-node node7" onclick="" data-org="org9">
					전시<br> 기반조성과
				</div>
				<div class="organization-item third-node node8" onclick="" data-org="org10">
					과학<br> 탐구교육과
				</div>
				<div class="organization-item third-node node9" onclick="" data-org="org11">
					과학 <br>문화진흥과
				</div>
			</div>
		</div>
		
		<div id="organization-mobile-chart" class="row visible-sm visible-xs">
			<div class="col-sm-12">
				<div id="first-node" class="organization-item" onclick="" data-org="org1">
					국립과천과학관장
				</div>
				
				<div class="organization-item node1 left-side" onclick="" data-org="org3">
					기획 조정과
				</div>
				<div class="organization-item node2 left-side" onclick="" data-org="org4">
					고객 서비스과
				</div>
				<div class="organization-item node3 left-side" onclick="" data-org="org5">
					운영 지원과
				</div>
				
				<div id="second-node" class="organization-item" onclick="" data-org="org2">
					전시 연구단장
				</div>
				
				<div class="row">
					<div class="col-xs-6">
						<div class="organization-item third-node node4" onclick="" data-org="org6">
							첨단 기술 전시과
						</div>	
					</div>
					<div class="col-xs-6">
						<div class="organization-item third-node node5" onclick="" data-org="org7">
							기초 과학 전시과
						</div>
					</div>
					<div class="col-xs-6">
						<div class="organization-item third-node node6" onclick="" data-org="org8">
							천문 우주 전시팀
						</div>	
					</div>
					<div class="col-xs-6">
						<div class="organization-item third-node node7" onclick="" data-org="org9">
							자연 생명 전시과
						</div>
					</div>
					<div class="col-xs-6">
						<div class="organization-item third-node node8" onclick="" data-org="org10">
							기반 기획 전시과
						</div>
					</div>
					<div class="col-xs-6">
						<div class="organization-item third-node node9" onclick="" data-org="org11">
							과학 문화 전시과
						</div>	
					</div>
				</div>
			</div>
		</div>
		
		<div class="row margin-bottom20">
			<div id="organization-search-form" class="col-md-12">
				<div class="form-inline text-right">
					<div class="form-group">
						<input type="text"
							class="form-control" id="org-search-text" placeholder="검색어를 입력하세요">
						
					</div>
					<button type="button" id="org-search-btn" class="btn btn-teal">검색</button>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div id="organization-details" class="col-md-12">
				
			</div>
		</div>
	</div>
</div>

<script id="timeline-template" type="text/x-handlebars-template">
<table class="table org-info-table">
	<thead>
		<tr><th>{{name}}</th></tr>
	</thead>
	<tbody>
		<tr>
			<td>
				{{#each works}}
					{{this}}<br>
				{{/each}}
			</td>
		</tr>
	</tbody>
</table>

<table class="table org-member-table">
	<thead>
		<tr>
			<th class="center">성명</th>
			<th class="center">부서</th>
			<th class="center">직급</th>
			<th>업무내용</th>
			<th class="center">전화번호</th>
		</tr>
	</thead>
	<tbody>
		{{#each members}}
			<tr>
				<td class="center">{{name}}</td>
				<td class="center">{{dept}}</td>
				<td class="center">{{position}}</td>
				<td>{{{mission}}}</td>
				<td class="center">{{phone}}</td>
			</tr>
		{{/each}}
	</tbody>
</table>
</script>

<script id="search-template" type="text/x-handlebars-template">
<table class="table org-member-table">
	<thead>
		<tr>
			<th class="center">성명</th>
			<th class="center">부서</th>
			<th class="center">직급</th>
			<th>업무내용</th>
			<th class="center">전화번호</th>
		</tr>
	</thead>
	<tbody>
		{{#each members}}
			<tr>
				<td class="center">{{name}}</td>
				<td class="center">{{dept}}</td>
				<td class="center">{{position}}</td>
				<td>{{{mission}}}</td>
				<td class="center">{{phone}}</td>
			</tr>
		{{/each}}
	</tbody>
</table>
</script>

<script type="text/javascript">
	var organizations = {
			"org1": {
				name: "관장",
				works: ["국립과천과학관 업무 총괄"],
				members: [
					{name: "조성찬", dept: "관장", position: "관장", mission: "국립과천과학관 업무총괄", phone: "02-3677-1300"}
				]
			},
			"org2": {
				name: "전시연구단",
				works: ["전시연구단 업무 총괄"],
				members: [

				          {name:"김선호",dept:"전시연구단",position:"전시연구단장",mission:"전시연구단 업무 총괄",phone:"02-3677-1400"},{name:"권일찬",dept:"전시기획연구과",position:"과장",mission:"전시기획연구과 업무 총괄",phone:"02-3677-1380"},{name:"한성환",dept:"전시운영총괄과",position:"과장",mission:"전시운영총괄과 업무 총괄",phone:"02-3677-1370"},{name:"유창영",dept:"전시기반조성과",position:"과장",mission:"전시기반조성과 업무 총괄",phone:"02-3677-1401"},{name:"이인일",dept:"과학탐구교육과",position:"과장",mission:"과학탐구교육과 업무 총괄",phone:"02-3677-1520"},{name:"우사임",dept:"과학문화진흥과",position:"과장",mission:"과학문화진흥과 업무 총괄",phone:"02-3677-1420"},{name:"정광훈",dept:"전시기획연구과",position:"팀장",mission:"스마트과학관팀 업무 총괄<br>\n미래상상(SF)관 전시기획·설치(기업체험관)<br>\n자연사관 전시기획, 리모델링 및 공간디자인<br>\n중장기 전시개선사업계획 수립 정책연구 관리",phone:"02-3677-1388"},{name:"유만선",dept:"창조전시관리팀",position:"팀장",mission:"창조전시관리팀 업무 총괄",phone:"02-3677-1441"},{name:"박남식",dept:"창조전시관리팀",position:"연구관",mission:"무한상상실 종합 운영<br>\n메이커 영재 사관학교 교육프로그램 개발 및 운영",phone:"02-3677-1443"},{name:"김주일",dept:"전시기획연구과",position:"사무관",mission:"구글놀이터 전시기획·설치<br>\n전시시설문화 디자인 개선사업",phone:"02-3677-1381"},{name:"유봉진",dept:"전시기반조성과",position:"사무관",mission:"중장기 전시공간 재배치 계획 수립·시행<br>\n국가 위임사무(비영리법인 설립 인허가) 관리<br>\n과학기술 관련 단체 후원·지원<br>\n「세계전동차 역사 기획전 인·허가사항 및 기본계획(안)」 수립",phone:"02-3677-1411"},{name:"안효정",dept:"과학탐구교육과",position:"사무관",mission:"개인(주말) 학교 밖 수요 맞춤형 프로그램 개발·운영<br>\n자기주도 학습콘텐츠 개발",phone:"02-3677-1522"},{name:"구수정",dept:"과학탐구교육과",position:"연구관",mission:"중단기 과학교육 종합계획 수립·시행<br>\n대내외 교육 협력 및 지원 총괄<br>\n과학교육분야 연구 및 조사·분석<br>\n교사 및 학부모연수 프로그램 기획·운영",phone:"02-3677-1521"},{name:"이춘호",dept:"전시기반조성과",position:"연구사",mission:"다목적 컨벤션센터 기본개념 수립<br>\n상설 전시기반시설 및 편의시설 기획·설치·운영<br>\n「세계전동차 역사 기획전 전시기획 및 인테리어 계획(안)」 수립",phone:"02-3677-1412"},{name:"안인선",dept:"과학문화진흥과",position:"연구사",mission:"과학노벨상 관련 행사 기획·운영<br>\n과천과학탐구여행 기획·운영<br>\n특별전기획전 기획·운영",phone:"02-3677-1423"},{name:"이양복",dept:"전시기획연구과",position:"주무관",mission:"정보화기본계획 및 시행계획의 수립·운영<br>\n스마트 전시운영시스템 구축·운영<br>\n홈페이지 및 사이버전시관 구축<br>\n온라인 기능성 게임 프로그램 개발·운영<br>\n과학관 개인정보보호 총괄 및 교육에 관한 업무",phone:"02-3677-1390"},{name:"이진영",dept:"전시기획연구과",position:"주무관",mission:"행정정보화 및 행정전산시스템에 관한 업무<br>\n정보보안에 관한 업무<br>\n관내 네트워크 및 전산실 서버 구축·운영<br>\n홈페이지 및 사이버전시관 운영<br>\n홈페이지 분야 개인정보보호에 관한 업무",phone:"02-3677-1389"},{name:"김미란",dept:"과학문화진흥과",position:"주무관",mission:"골드버그대회 기획·운영<br>\n어울림홀 대관 활용계획 수립 및 운영<br>\n과학문화공연 기획·유치<br>\n통계로 보는 세상 특별전 기획·운영",phone:"02-3677-1428"},{name:"노한숙",dept:"과학문화진흥과",position:"주무관",mission:"과학문화사업 기본계획 수립<br>\n미래상상SF축제, 특별전 등 행사 및 기획전 총괄",phone:"02-3677-1421"},{name:"정상헌",dept:"과학문화진흥과",position:"주무관",mission:"수학문화축전 기획·운영<br>\n미래상상SF축제 운영 지원<br>\n과학문화부문 통계 관리 및 분석에 관한 사항",phone:"02-3677-1425"},{name:"최민수",dept:"전시기획연구과",position:"연구사",mission:"미래상상(SF)관 전시기획·설치<br>\n&nbsp;-휴먼과 에일리언, 미래 Job-world 테마관<br>\n첨단1관 기존 전시품 처리에 관한 사항",phone:"02-3677-1385"},{name:"최정원",dept:"전시기획연구과",position:"연구사",mission:"미래상상(SF)관 전시기획·설치<br>\n&nbsp;-전체 공정 및 사업관리, 개관행사\n&nbsp;-미래상상, 우주도시, 우주전쟁 테마관\n전시기획위원회 운영",phone:"02-3677-1383"},{name:"남경욱",dept:"전시운영총괄과",position:"연구사",mission:"전시관 종합운영계획 수립·시행<br>\n&nbsp;-신규전시관 운영계획 수립<br>\n전시운영위탁사업 총괄<br>\n&nbsp;-전시관·인력 운영 시스템 체계화<br>\n&nbsp;-운영요원 CS 내재화 및 역량강화 교육<br>\n&nbsp;-전시관·인력 운영 주기적 평가 및 환류체계구축<br>\n상설전시관 전시디자인 개선<br>\n&nbsp;-「자랑스러운 과학문화유산」전시 기획·설치",phone:"02-3677-1371"},{name:"정원영",dept:"전시운영총괄과",position:"연구사",mission:"전시해설프로그램 기획·운영<br>\n특별 해설프로그램 기획·운영<br>\n전시안내 콘텐츠 제작<br>\n&nbsp;-전시관 안내책자 기획·제작 <br>\n&nbsp;-Google 과천과학관 전시 콘텐츠 제작<br>\n전시품운영심의회 운영<br>\n대내외 협력",phone:"02-3677-1375"},{name:"김선혜",dept:"전시기반조성과",position:"연구사",mission:"조경 및 수목 관리 종합계획 수립·시행<br>\n옥외 전시기반시설 및 편의시설 기획·설치·운영<br>\n전시·과학기술자료실 운영 관리",phone:"02-3677-1414"},{name:"양회정",dept:"과학탐구교육과",position:"연구사",mission:"진로체험 등 자유학기제 프로그램 개발·운영<br>\n과학캠프 프로그램 기획·운영<br>\n캠프장 숙소 시설 운영관리",phone:"02-3677-1525"},{name:"배부영",dept:"과학문화진흥과",position:"연구사",mission:"자체 및 공동기획 특별전(교류전) 기본계획 수립<br>\n노벨상, 종이접기 특별전 등 행사 및 기획전 총괄<br>\n한불수교 130주년 특별전, 바이오아트 특별전 기획·운영",phone:"02-3677-1426"},{name:"손재덕",dept:"창조전시관리팀",position:"연구사",mission:"곤충생태관, 생태공원 운영 및 시설관리<br>\n곤충생태관 리모델링 기획<br>\n옥외전시장 전시품 운영 및 시설(도예체험 공방 포함) 관리",phone:"02-3677-1447"},{name:"신지혜",dept:"창조전시관리팀",position:"연구사",mission:"스페이스월드, 투영관 운영 및 시설관리<br>\n&nbsp;-극장형 운영을 위한 개선계획 수립·추진",phone:"02-3677-1445"},{name:"조재일",dept:"창조전시관리팀",position:"연구사",mission:"천체관측소 운영 및 시설관리<br>\n2017년도 야외전시장 야간운영 계획수립·기획<br>\n천문시설 연계 프로그램 기획·운영<br>\n&nbsp;-천문시설 국내 거점화 추진",phone:"02-3677-1448"},{name:"조춘익",dept:"창조전시관리팀",position:"연구사",mission:"무한상상 메이커랜드 행사 기획<br>\n풀뿌리메이커 정보공유 플랫폼 구축 및 운영<br>\n메이커 프리마켓 기획·운영(민간 서비스 연계 포함)",phone:"02-3677-1444"},{name:"정연란",dept:"전시기획연구과",position:"주무관",mission:"미래상상(SF)관 전시기획·설치<br>\n&nbsp;-한국의 SF관 테마관, 전시해설서 제작<br>\n과학전시교육 학부모자문단 구성·운영",phone:"02-3677-1386"},{name:"박순희",dept:"전시운영총괄과",position:"주무관",mission:"고객 민원 현장 대응 관리<br> \n중앙홀 안내데스크 운영 개선 및 관리<br>\n오늘의 전시, 행사 등 과학관 주요일정 모니터링 및 배포<br>\n전시관 운영 물품 관리·운영",phone:"02-3677-1373"},{name:"송현량",dept:"전시운영총괄과",position:"주무관",mission:"체험전시물 운영·관리<br>\n&nbsp;-체험전시물 온라인 예약 및 현장 발권시스템 운영·관리<br>\n&nbsp;-예약시스템 개선 방안 기획<br>\n&nbsp;-체험전시물 유료화 방안 기획<br>\n체험전시물 해설프로그램 개선<br>\n&nbsp;-신규 해설프로그램 개발·운영<br>\n미래상상관「이벤트홀」·「잡월드」운영 및 관리",phone:"02-3677-1374"},{name:"김주영",dept:"창조전시관리팀",position:"주무관",mission:"패밀리 창작놀이터 프로그램 기획 및 운영<br>\n학교 및 SNS기반 메이커 동아리 워크숍 개발 및 운영",phone:"02-3677-1449"},{name:"김영옥",dept:"전시기획연구과",position:"주무관",mission:"바이오아트 갤러리 전시기획·설치",phone:"02-3677-1382"},{name:"오연숙",dept:"전시운영총괄과",position:"주무관",mission:"전시관 과학체험교실 활용방안 마련<br>\n&nbsp;-기초과학관, 전통과학관, 자연사관 탐구교실 활용방안 마련<br>\n&nbsp;-탐구교실 활용 체험프로그램 개발·운영<br>\n「생명과학실험실」위탁사업 관리 및 향후 계획 수립<br>\n「수족관」위탁사업 운영·관리",phone:"02-3677-1372"},{name:"정명기",dept:"전시기반조성과",position:"주무관",mission:"전시품 보수요원 운영·관리<br>\n전시품 관리 및 수장고 운영<br>\n전시시설분야 안전관리",phone:"02-3677-1415"},{name:"김완숙",dept:"과학탐구교육과",position:"주무관",mission:"청춘과학대학 기획·운영<br>\n교육시설(강의·실험실, 캠프장)의 대관에 관한 사항",phone:"02-3677-1524"},{name:"윤덕호",dept:"과학문화진흥과",position:"주무관",mission:"과학문화광장 활용 사업 기획·운영<br>\n과학문화시설 대관, 개선 및 유지보수에 관한 사항<br>\n과학문화시설 운영 통계관리 및 분석에 관한 사항<br>\n연구개발성과전시, 우표전시 등 공동기획전 운영",phone:"02-3677-1429"},{name:"김광연",dept:"전시운영총괄과",position:"주무관",mission:"상설전시관 관람 환경 개선<br>\n&nbsp;-전시장 청결유지관리, 쾌적한 관람환경 조성<br>\n&nbsp;-전시관내 관람객 편의시설 확충 및 개선<br>\n전시관내 안전사고 방지<br>\n&nbsp;-상설 전시관 전시품 안전점검 및 사고 대응 조치<br>\n전시물 운영 업그레이드 및 개선, 보수, 유지 관리",phone:"02-3677-1376"},{name:"김세용",dept:"전시기반조성과",position:"주무관",mission:"전시품유지관리시스템 운영 및 유지관리<br>\n전시품 유지보수 위탁사업 발주·관리<br>\n전시품 운영현황 조사·안내 총괄<br>\n전시·연구자료 수증·수탁 및 보존관리",phone:"02-3677-1416"},{name:"이순옥",dept:"전시기반조성과",position:"주무관",mission:"자원봉사자 활용 및 운영계획 수립·시행<br>\n전시·학술·연구자료의 수증·수탁 관리 총괄<br>\n수증·수탁 및 수장물품 활용 효율화 계획 수립",phone:"02-3677-1413"},{name:"최주한",dept:"과학탐구교육과",position:"주무관",mission:"단체(주중) 창의적 체험활동 프로그램 기획·운영<br>\n과학관련 이벤트 및 교육행사 기획·운영<br>\n교육관 시설 및 물품 운영·관리\n과학교육 위탁사업 운영·관리",phone:"02-3677-1523"},{name:"정세용",dept:"창조전시관리팀",position:"주무관",mission:"자유학기제 연계 교육프로그램 개발 및 운영<br>\n무한상상 메이커랜드 행사(발명한마당 포함) 추진<br>\n메이커랜드 관련 공사 및 시설유지보수<br>\n&nbsp;-메이커 인큐베이터 시설 공사 및 메이커 프리마켓 설치<br>\n&nbsp;-무한상상실 시설·장비 유지보수 및 안전관리",phone:"02-3677-1450"},{name:"최유나",dept:"전시기획연구과",position:"연구사",mission:"미래상상(SF)관 전시기획·설치\n&nbsp;-전시관 공간디자인 및 전시연출\n자연사관 전시기획, 리모델링 및 공간디자인 지원",phone:"02-3677-1387"},{name:"나민환",dept:"과학문화진흥과",position:"연구사",mission:"V-Robot Live 특별전 기획·운영<br>\n도시농업특별전 기획·운영<br>\nSF축제, SF전시체험, 수학문화축전 전시 기획·운영<br>\n국내·외 과학문화행사 사례조사·분석",phone:"02-3677-1427"},{name:"이태우",dept:"과학문화진흥과",position:"연구사",mission:"해피사이언스 메이커랜드 기획·운영<br>\n미래상상SF축제 기획·운영<br>\n미래상상SF전시체험 기획·운영<br>\n국내·외 과학문화행사 협력사업 발굴 추진",phone:"02-3677-1422"},{name:"한가빈",dept:"과학문화진흥과",position:"연구사",mission:"전국청소년과학송경진대회 기획·운영<br>\n예술에 자연을 담다 특별전 기획·운영<br>\n종이접기 특별전 기획·운영<br>\n바이오아트 특별전 기획·운영 지원",phone:"02-3677-1424"},{name:"김영수",dept:"창조전시관리팀",position:"연구사",mission:"국제천체투영관 영화제 기획·추진<br>\n천문시설 연계 프로그램 기획·운영<br>\n&nbsp;-사이피아 돔 곤서트 기획·추진<br>\n스페이스월드 및 투영관 영상 전시사업<br>\n&nbsp;-스페이스월드 영상시스템 개선·영상물 제작<br>\n&nbsp;-천체투영관 돔 영상 구입",phone:"02-3677-1442"}
		        ]
			},
			"org3": {
				name: "경영기획과",
				works: [
					"1. 국립과천과학관 장기 발전방안의 기획ㆍ수립 및 운영에 관한 사항",
					"2. 주요 업무계획 및 시행계획의 수립ㆍ종합, 조정에 관한 사항",
					"3. 책임운영기관제도의 운영에 관한 사항",
					"4. 성과평가제도의 운영에 관한 사항",
					"5. 소요인력의 확보, 정원관리 등 조직 편제에 관한 사항",
					"6. 법령 및 내규의 제·개정에 관한 사항",
					"7. 공직기강, 부패방지 등 감사 및 사정에 관한 사항",
					"8. 국정감사 등 대국회 업무에 관한 사항",
					"9. 해외 과학관 및 국제 협력에 관한 사항",
					"10. 국립과천과학관 자문위원회 운영 등에 관한 사항",
					"11. 그 밖에 국립과천과학관 내 다른 과·팀의 주관에 속하지 아니하는 사항",
					"<예산후원팀>",
					"1. 중기재정계획의 수립에 관한 사항",
					"2. 신규 사업의 개발 기획 및 재원 확보에 관한 사항",
					"3. 사업별, 항목별 세입세출예산안 편성에 관한 사항",
					"4. 주요 사업(예산)의 진도파악과 그 결과 분석에 관한 사항",
					"5. (사)과학사랑희망키움 육성․관리에 관한 사항",
					"6. 국립과천과학관 후원회 활성화 및 운영지원 관한 사항",
					"<전시안전기획자문관>",
					"1. 전문가·이해관계자·고객에 관한 사항",
					"2. 전시 기획 및 안전 지원에 관한 사항"
				],
				members: [
				          {name:"나치수",dept:"경영기획과",position:"과장",mission:"경영기획과 업무 총괄",phone:"02-3677-1310"},{name:"임두원",dept:"경영기획과",position:"팀장",mission:"전시기획 및 안전 업무 등의 지원<br>\n관장 지시사항 검토<br>\n정책홍보기획 총괄",phone:"02-3677-1322"},{name:"황성하",dept:"경영기획과",position:"팀장",mission:"예산후원팀 업무 총괄",phone:"02-3677-1318"},{name:"심원무",dept:"경영기획과",position:"사무관",mission:"중장기 조직 및 정원 운영계획 수립·시행<br>\n사무분장 조정 및 규정 관리<br>\n성과관리기본계획 수립·시행<br>\n총액인건비제 세부시행계획수립·시행<br>\n부서평가 및 개인역량평가 등<br>\n개인별 성과상여금 지급계획 수립·시행<br>\n비정규직 직원 TO 검토 및 조정<br>\n일상 감사 및 사정업무 등 반부패 청렴 추진계획 수립·시행",phone:"02-3677-1311"},{name:"권채순",dept:"경영기획과",position:"연구관",mission:"장기 발전방안 기획·수립 및 운영<br>\n연도별 기관 사업계획 수립 및 과학관운영심의회 대응<br>\n책임운영기관 제도발전 및 운영<br>\n대외 기관 현황 및 현안(법인화 등) 대응<br>\n해외 유관기관과의 협력 및 자료 수집·분석",phone:"02-3677-1313"},{name:"김재영",dept:"경영기획과",position:"주무관",mission:"책임운영기관 종합평가 대응<br>\n통합성과지표 개발·운영<br>\n업무처리 절차 개선 발굴 및 관리<br>\n간부회의 운영",phone:"02-3677-1314"},{name:"김채아",dept:"경영기획과",position:"주무관",mission:"중기재정계획 수립 및 시행 업무<br>\n사업항목별 세입세출예산(안) 편성 업무<br>\n재정성과 계획 및 평가에 관한 사항<br>\n신규사업의 개발 및 조정·총괄 업무",phone:"02-3677-1320"},{name:"김태범",dept:"경영기획과",position:"주무관",mission:"부서장 직무성과계약제 운영<br>\n무료개방, 기관휴무 등 연간 기관운영 일정 조정<br>\n대내외 규정의 제정·개정 및 유지관리<br>\n일일 부서 주요업무관리",phone:"02-3677-1312"},{name:"정용준",dept:"경영기획과",position:"주무관",mission:"정부3.0과제 운영<br>\n국회(정기․임시회, 국정감사 등) 관련 업무<br>\n대통령․국무총리 및 장․차관 지시사항 관리<br>\n전시․연구개발용역심의회 운영<br>\n지적 재산권 관리",phone:"02-3677-1316"},{name:"전성윤",dept:"경영기획과",position:"연구사",mission:"예산집행심의위원회 운영<br>\n세출예산 이용·전용·이월에 관한 사항<br>\n세출예산의 집행상황의 점검 및 운영에 관한 사항 정리<br>\n과학관 후원회(사, 과학사랑희망키움) 지원<br>\n사이피아포럼 추진계획 수립 및 운영",phone:"02-3677-1321"}
				]
			},
			"org4": {
				name: "고객창출과",
				works: [
					"1. 장·단기 및 연도별 고객창출(마케팅) 기획 및 운영에 관한 사항",
					"2. 고객 대상층 및 규모별 차별화된 고객 확대유치 계획 수립․시행에 관한 사항",
					"3. 종합적인 고객만족도 제고 계획 수립 및 시행에 관한 사항",
					"4. 관람객의 통계 작성, 유지 및 분석에 관한 사항",
					"5. 단체 관람객 유치 방안에 관한 사항",
					"6. 고객 지원·안내 센터의 운영에 관한 사항", 
					"7. 회원 관리·운영에 관한 사항 ",
					"8. 관람객 안전 및 배상 등 고객 민원 처리에 관한 사항",
					"9. 전국 및 수도권의 중심과학관 기능 수행에 관한 사항",
					"10. 고객서비스헌장제도 운영에 관한 사항",
					"11. 그 밖에 국립과천과학관 내 다른 과·팀의 주관에 속하지 아니하는 고객지원관련 사항",
					
					"<홍보협력팀>",
					"1. 종합홍보계획의 수립 및 운영에 관한 사항",
					"2. 언론기관 등 유관기관과의 홍보시스템 구축에 관한 사항",
					"3. 개인·단체·기관 등 과의 협력사업 총괄에 관한 사항",
					"4. 국내 과학관의 동향 및 정책 자료의 수집에 관한 사항",
					"5. 홍보물의 제작 및 배포에 관한 사항",
					"6. 그 밖에 국립과천과학관 내 다른 과·팀의 주관에 속하지 아니하는 홍보 관련 사항"
				],
				members: [

				          
				          {name:"박혜현",dept:"고객창출과",position:"과장",mission:"고객창출과 업무 총괄",phone:"02-3677-1340"},{name:"홍현선",dept:"고객창출과",position:"팀장",mission:"홍보협력팀 업무 총괄",phone:"02-3677-1345"},{name:"연세용",dept:"고객창출과",position:"사무관",mission:"고객창출 강화 업무<br>\n전국·수도권 과학관 중심의 협력",phone:"02-3677-1341"},{name:"정지원",dept:"고객창출과",position:"주무관",mission:"종합홍보계획의 수립 및 운영에 관한 사항<br>\n언론기관 등 유관기관과의 홍보시스템 구축에 관한 사항<br>\n홍보물의 제작 및 배포에 관한 사항<br>\n옥외 광고 및 잡지매체 홍보 업무<br>\n과학관 대표 홍보 상품 기획·제작",phone:"02-3677-1346"},{name:"최대우",dept:"고객창출과",position:"주무관",mission:"고객만족도 향상에 관한 업무<br>\n고객지원·안내센터 운영에 관한 업무<br>\n관람 인원 등 과학관 일일 동향 파악<br>\n유료회원 등 과학관 회원제 운영관리<br>\n단체관람 사전답사 프로그램 운영",phone:"02-3677-1343"},{name:"고재형",dept:"고객창출과",position:"주무관",mission:"개인·단체·기관 등과의 협력 사업에 관한 사항<br>\n과학 관련 자료의 교환·양여 등 공·사립 과학관 업무지원 및 협력<br>\n국내 과학관의 동향 및 정책 자료의 수집에 관한 사항<br>\n관장 대외업무 지원에 관한 사항<br>\n외부 과학기술행사 후원에 관한 사항",phone:"02-3677-1347"},{name:"심인보",dept:"고객창출과",position:"주무관",mission:"기관 각종 통계 작성·관리 및 분석<br>\n전화안내센터 운영에 관한 업무<br>\n관람객 안전에 관한 업무<br>\n유관기관 연계 마케팅 및 대표상품 개발<br>\n전국 과학관 협력 업무",phone:"02-3677-1344"},{name:"이민지",dept:"고객창출과",position:"연구사",mission:"온라인(SNS) 홍보 업무<br>\ne-소식지 제작 및 배포, SNS 홍보이벤트<br> \n온라인 홍보콘텐츠 제작 및 배포 지원<br>\n온라인(SNS) 홍보기자단 운영관리<br>\n과학관 대표상품 디자인 기획제작 지원",phone:"02-3677-1348"}
				            
				      
				]
			},
			"org5": {
				name: "운영지원과",
				works: [
					"1. 보안 및 청사방호에 관한 사항",
					"2. 관인 및 관인대장의 관리에 관한 사항",
					"3. 공무원 등의 임용ㆍ교육훈련ㆍ연금 등에 관한 사항",
					"4. 공무원 등의 복무관리 및 징계에 관한 사항",
					"5. 문서의 분류ㆍ수발ㆍ심사ㆍ보존 등 문서관리에 관한 사항",
					"6. 물품의 구매ㆍ조달 및 관리 등에 관한 사항",
					"7. 자금의 운용ㆍ회계 및 결산 업무에 관한 사항",
					"8. 세입금의 결정, 징수 및 관리에 관한 사항",
					"9. 국유재산 및 지식재산권의 관리에 관한 사항",
					"10. 비상계획업무에 관한 사항",
					"11. 청사 등 시설물의 설치·유지·보수 및 관리·안전에 관한 사항",
					"12. 전력 및 냉난방 등 에너지관리에 관한 사항",
					"13. 그 밖에 국립과천과학관 내 다른 과·팀의 주관에 속하지 아니하는 시설관련 사항"
				],
				members: [

				          
				          {name:"백정현",dept:"운영지원과",position:"과장",mission:"운영지원과 업무 총괄",phone:"02-3677-1350"},{name:"박영주",dept:"운영지원과",position:"사무관",mission:"유관기관과 협력에 관한 사항<br>\n과학관의 발전적 조직문화 확산에 관한 사항",phone:"02-3677-1351"},{name:"김중영",dept:"운영지원과",position:"주무관",mission:"국유재산 관리 및 사용수익허가에 관한 사항<br>\n각종 세입 및 부가가치세에 관한 사항<br>\n일반지출(손익계정)에 관한 사항<br>\n편익시설(식당, 자판기 등)에 관한 사항<br>\n과학관 발전적 조직문화 확산의 실행에 관한 사항",phone:"02-3677-1357"},{name:"이재필",dept:"운영지원과",position:"주무관",mission:"보안 및 방호, 비상계획에 관한 사항<br>\n물품관리 및 민원·정보공개에 관한 사항<br>\n맞춤형복지, 관인관리 등 공통업무 관련 사항<br>\n기록물관리에 관한 사항",phone:"02-3677-1352"},{name:"김병곤",dept:"운영지원과",position:"주무관",mission:"공사,용역,물품 등 계약에 관한 사항<br>\n세입·세출 예산회계 결산 및 재무결산에 관한 사항<br>\n일반지출(자본계정)에 관한 사항<br>\n부서별 자금계획 수립·배정에 관한 사항",phone:"02-3677-1358"},{name:"박지호",dept:"운영지원과",position:"주무관",mission:"시설 환경개선에 관한 사항<br>\n시설비, 유지보수비, 위탁사업비 발주·관리에 관한 사항<br>\n재산종합 보험 및 재난대비에 관한 사항<br>\n에너지절약 추진 등에 관한 사항<br>\n미화, 경비 관련 보수·개선에 관한 사항",phone:"02-3677-1356"},{name:"유광복",dept:"운영지원과",position:"주무관",mission:"공무원 임용, 포상, 징계, 제증명 등 인사에 관한 사항<br>\n직원 복무관리, 고충처리, 교육훈련, 노동조합에 관한 사항",phone:"02-3677-1354"},{name:"김지현",dept:"운영지원과",position:"주무관",mission:"급여(직원, 기간제근로자)에 관한 사항 <br>\n관서운영경비(업무추진비, 여비 등)에 관한 사항 <br>\n일반지출(기본경비)에 관한 사항",phone:"02-3677-1353"},{name:"이건배",dept:"운영지원과",position:"주무관",mission:"관용차량 관리·운행 및 관장실 지원에 관한 사항<br>\n본부 문서수발, 공통업무 지원에 관한 사항",phone:"02-3677-1362"},{name:"인선민",dept:"운영지원과",position:"주무관",mission:"건축·기계·전기·통신·가스·승강기 시설유지보수에 관한 사항<br>\n시설 위탁사업 현장 관리에 관한 사항<br>\n소방·방재 및 시설물 법정검사에 관한 사항<br>\n미화 위탁사업 및 청소 현장 관리에 관한 사항",phone:"02-3677-1361"},{name:"한종수",dept:"운영지원과",position:"주무관",mission:"관용차랑 관리 및 운행에 관한 사항<br>\n주차·매표·경비 위탁사업 관리 및 유지보수에 관한 사항<br>\n당직근무 및 회의실에 관한 사항",phone:"02-3677-1363"}
				]
			},
			"org6": {
				name: "전시기획연구과",
				works: [
					"1. 중·단기 종합전시기본계획의 수립·시행에 관한 사항",
					"2. 중·단기기 전시품교체계획 수립·시행에 관한 사항",
					"3. 전시시설(관) 현대화(리모델링) 종합계획 수립·시행에 관한 사항",
					"4. 신규 전시품 기획·설계·제작·설치·시험운영에 관한 사항",
					"5. 신규 사업 기획 및 전시관 리모델링에 관한 사항",
					"6. 학술·연구 용역을 통한 국내·외 협력활동에 관한 사항", 
					"7. 전시기획위원회 운영에 관한 사항",
					"8. 지역과학관 전시품 제작 지원사업 기획·운영에 관한 사항",
					"9. 과학기술정책 및 국가연구개발사업의 연구개발성과 전시에 관한 사항",
					"10. 그 밖에 전시연구단내 다른 과에 속하지 아니하는 전시분야 사항",
					
					"<스마트과학관팀>",
					"1. 스마트 전시운영시스템 구축 및 운영 계획 수립․시행에 관한 사항",
					"2. 정보화기본계획 및 시행계획의 수립ㆍ운영에 관한 사항",
					"3. 행정정보화 및 행정전산시스템의 운영 관리ㆍ지원에 관한 사항",
					"4. 정보보호 및 정보보안에 관한 사항",
					"5. 홈페이지 운영에 관한 사항",
					"6. 사이버전시교육시스템 구축·운영에 관한 사항", 
					"7. 온라인 기능성 게임 프로그램 개발·운영 및 활용에 관한 사항"
				],
				members: [

				          
				          {name:"권일찬",dept:"전시기획연구과",position:"과장",mission:"전시기획연구과 업무 총괄",phone:"02-3677-1380"},{name:"정광훈",dept:"전시기획연구과",position:"팀장",mission:"스마트과학관팀 업무 총괄<br>\n미래상상(SF)관 전시기획·설치(기업체험관)<br>\n자연사관 전시기획, 리모델링 및 공간디자인<br>\n중장기 전시개선사업계획 수립 정책연구 관리",phone:"02-3677-1388"},{name:"김주일",dept:"전시기획연구과",position:"사무관",mission:"구글놀이터 전시기획·설치<br>\n전시시설문화 디자인 개선사업",phone:"02-3677-1381"},{name:"이양복",dept:"전시기획연구과",position:"주무관",mission:"정보화기본계획 및 시행계획의 수립·운영<br>\n스마트 전시운영시스템 구축·운영<br>\n홈페이지 및 사이버전시관 구축<br>\n온라인 기능성 게임 프로그램 개발·운영<br>\n과학관 개인정보보호 총괄 및 교육에 관한 업무",phone:"02-3677-1390"},{name:"이진영",dept:"전시기획연구과",position:"주무관",mission:"행정정보화 및 행정전산시스템에 관한 업무<br>\n정보보안에 관한 업무<br>\n관내 네트워크 및 전산실 서버 구축·운영<br>\n홈페이지 및 사이버전시관 운영<br>\n홈페이지 분야 개인정보보호에 관한 업무",phone:"02-3677-1389"},{name:"최민수",dept:"전시기획연구과",position:"연구사",mission:"미래상상(SF)관 전시기획·설치<br>\n&nbsp;-휴먼과 에일리언, 미래 Job-world 테마관<br>\n첨단1관 기존 전시품 처리에 관한 사항",phone:"02-3677-1385"},{name:"최정원",dept:"전시기획연구과",position:"연구사",mission:"미래상상(SF)관 전시기획·설치<br>\n&nbsp;-전체 공정 및 사업관리, 개관행사\n&nbsp;-미래상상, 우주도시, 우주전쟁 테마관\n전시기획위원회 운영",phone:"02-3677-1383"},{name:"정연란",dept:"전시기획연구과",position:"주무관",mission:"미래상상(SF)관 전시기획·설치<br>\n&nbsp;-한국의 SF관 테마관, 전시해설서 제작<br>\n과학전시교육 학부모자문단 구성·운영",phone:"02-3677-1386"},{name:"김영옥",dept:"전시기획연구과",position:"주무관",mission:"바이오아트 갤러리 전시기획·설치",phone:"02-3677-1382"},{name:"최유나",dept:"전시기획연구과",position:"연구사",mission:"미래상상(SF)관 전시기획·설치\n&nbsp;-전시관 공간디자인 및 전시연출\n자연사관 전시기획, 리모델링 및 공간디자인 지원",phone:"02-3677-1387"}
				            
				]
			},
			"org7": {
				name: "전시운영총괄과",
				works: [
					"1. 상설전시관(기초·전통·첨단·자연사·어린이탐구전시관, 노벨전시관, 명예의 전당) 등의 전시운영계획의 수립·시행에 관한 사항",
					"2. 전시품심의위원회 운영에 관한 사항",
					"3. 상설전시관 소규모 전시품 제작·설치·교체 및 유지관리 등에 관한 사항",
					"4. 상설전시관 연계 소규모 이벤트성 교육·문화프로그램 개발·운영에 관한 사항", 
					"5. 전시관 운영직원 운영 및 교육에 관한 사항",
					"6. 전시해설사 양성 및 운영에 관한 사항",
					"7. 그 밖에 전시연구단내 다른 과·팀에 속하지 아니하는 전시운영 관한 사항"
				],
				members: [
						{name:"한성환",dept:"전시운영총괄과",position:"과장",mission:"전시운영총괄과 업무 총괄",phone:"02-3677-1370"},{name:"남경욱",dept:"전시운영총괄과",position:"연구사",mission:"전시관 종합운영계획 수립·시행<br>\n&nbsp;-신규전시관 운영계획 수립<br>\n전시운영위탁사업 총괄<br>\n&nbsp;-전시관·인력 운영 시스템 체계화<br>\n&nbsp;-운영요원 CS 내재화 및 역량강화 교육<br>\n&nbsp;-전시관·인력 운영 주기적 평가 및 환류체계구축<br>\n상설전시관 전시디자인 개선<br>\n&nbsp;-「자랑스러운 과학문화유산」전시 기획·설치",phone:"02-3677-1371"},{name:"정원영",dept:"전시운영총괄과",position:"연구사",mission:"전시해설프로그램 기획·운영<br>\n특별 해설프로그램 기획·운영<br>\n전시안내 콘텐츠 제작<br>\n&nbsp;-전시관 안내책자 기획·제작 <br>\n&nbsp;-Google 과천과학관 전시 콘텐츠 제작<br>\n전시품운영심의회 운영<br>\n대내외 협력",phone:"02-3677-1375"},{name:"박순희",dept:"전시운영총괄과",position:"주무관",mission:"고객 민원 현장 대응 관리<br> \n중앙홀 안내데스크 운영 개선 및 관리<br>\n오늘의 전시, 행사 등 과학관 주요일정 모니터링 및 배포<br>\n전시관 운영 물품 관리·운영",phone:"02-3677-1373"},{name:"송현량",dept:"전시운영총괄과",position:"주무관",mission:"체험전시물 운영·관리<br>\n&nbsp;-체험전시물 온라인 예약 및 현장 발권시스템 운영·관리<br>\n&nbsp;-예약시스템 개선 방안 기획<br>\n&nbsp;-체험전시물 유료화 방안 기획<br>\n체험전시물 해설프로그램 개선<br>\n&nbsp;-신규 해설프로그램 개발·운영<br>\n미래상상관「이벤트홀」·「잡월드」운영 및 관리",phone:"02-3677-1374"},{name:"오연숙",dept:"전시운영총괄과",position:"주무관",mission:"전시관 과학체험교실 활용방안 마련<br>\n&nbsp;-기초과학관, 전통과학관, 자연사관 탐구교실 활용방안 마련<br>\n&nbsp;-탐구교실 활용 체험프로그램 개발·운영<br>\n「생명과학실험실」위탁사업 관리 및 향후 계획 수립<br>\n「수족관」위탁사업 운영·관리",phone:"02-3677-1372"},{name:"김광연",dept:"전시운영총괄과",position:"주무관",mission:"상설전시관 관람 환경 개선<br>\n&nbsp;-전시장 청결유지관리, 쾌적한 관람환경 조성<br>\n&nbsp;-전시관내 관람객 편의시설 확충 및 개선<br>\n전시관내 안전사고 방지<br>\n&nbsp;-상설 전시관 전시품 안전점검 및 사고 대응 조치<br>\n전시물 운영 업그레이드 및 개선, 보수, 유지 관리",phone:"02-3677-1376"}      
				]
			},
			"org8": {
				name: "창조전시관리팀",
				works: [
					"1. 옥외전시관(천체 투영관·관측소, 스페이스월드, 곤충관, 생태공원, 역사의 광장, 무한상상실, 지질·공룡동산·발사체 등) 전시운영계획의 수립 및 시행에 관한 사항",
					"2. 천문우주 및 지질생태 관련분야 소규모 전시품 제작·설치·교체 및 유지관리 등에 관한 사항",
					"3. 천문우주 관련분야 전시물 연계 교육·문화·체험프로그램 기획·운영에 관한 사항 ",
					"4. 지질 생태 등 옥외전시관(품) 연계 소규모 이벤트성 교육·문화·체험프로그램 기획·운영에 관한 사항", 
					"5. 천문우주 및 지질생태 관련분야 전시자료의 수집·보존·연구·관리 등에 관한 사항",
					"6. 천문우주 및 지질생태 관련분야 국내·외 학술 및 협력활동에 관한 사항",
					"7. 그 밖에 다른 과․팀에 속하지 아니하는 옥외 전시관(품) 관련 사항"
				],
				members: [
					{name:"유만선",dept:"창조전시관리팀",position:"팀장",mission:"창조전시관리팀 업무 총괄",phone:"02-3677-1441"},{name:"박남식",dept:"창조전시관리팀",position:"연구관",mission:"무한상상실 종합 운영<br>\n메이커 영재 사관학교 교육프로그램 개발 및 운영",phone:"02-3677-1443"},{name:"손재덕",dept:"창조전시관리팀",position:"연구사",mission:"곤충생태관, 생태공원 운영 및 시설관리<br>\n곤충생태관 리모델링 기획<br>\n옥외전시장 전시품 운영 및 시설(도예체험 공방 포함) 관리",phone:"02-3677-1447"},{name:"신지혜",dept:"창조전시관리팀",position:"연구사",mission:"스페이스월드, 투영관 운영 및 시설관리<br>\n&nbsp;-극장형 운영을 위한 개선계획 수립·추진",phone:"02-3677-1445"},{name:"조재일",dept:"창조전시관리팀",position:"연구사",mission:"천체관측소 운영 및 시설관리<br>\n2017년도 야외전시장 야간운영 계획수립·기획<br>\n천문시설 연계 프로그램 기획·운영<br>\n&nbsp;-천문시설 국내 거점화 추진",phone:"02-3677-1448"},{name:"조춘익",dept:"창조전시관리팀",position:"연구사",mission:"무한상상 메이커랜드 행사 기획<br>\n풀뿌리메이커 정보공유 플랫폼 구축 및 운영<br>\n메이커 프리마켓 기획·운영(민간 서비스 연계 포함)",phone:"02-3677-1444"},{name:"김주영",dept:"창조전시관리팀",position:"주무관",mission:"패밀리 창작놀이터 프로그램 기획 및 운영<br>\n학교 및 SNS기반 메이커 동아리 워크숍 개발 및 운영",phone:"02-3677-1449"},{name:"정세용",dept:"창조전시관리팀",position:"주무관",mission:"자유학기제 연계 교육프로그램 개발 및 운영<br>\n무한상상 메이커랜드 행사(발명한마당 포함) 추진<br>\n메이커랜드 관련 공사 및 시설유지보수<br>\n&nbsp;-메이커 인큐베이터 시설 공사 및 메이커 프리마켓 설치<br>\n&nbsp;-무한상상실 시설·장비 유지보수 및 안전관리",phone:"02-3677-1450"},{name:"김영수",dept:"창조전시관리팀",position:"연구사",mission:"국제천체투영관 영화제 기획·추진<br>\n천문시설 연계 프로그램 기획·운영<br>\n&nbsp;-사이피아 돔 곤서트 기획·추진<br>\n스페이스월드 및 투영관 영상 전시사업<br>\n&nbsp;-스페이스월드 영상시스템 개선·영상물 제작<br>\n&nbsp;-천체투영관 돔 영상 구입",phone:"02-3677-1442"}
				]
			},
			"org9": {
				name: "전시기반조성과",
				works: [			
					"1. 전시공간 중장기 배치계획 수립·시행에 관한 사항",
					"2. 상설 및 옥외 전시기반 시설 및 편의시설 기획·설치·운영에 관한 사항",
					"3. 전시품 유지보수에 관한 사항",
					"4. 전시품관리시스템 운영 개선에 관한 사항",
					"5. 과학기술 자료실 관리 운영 ",
					"6. 전시·학술·연구 자료의 구입·수집·보존 및 대여, 수증·수탁 등 관리에 관한 사항",
					"7. 과학문화단체(비영리법인 등) 관리 및 지원에 관한 사항 ",
					"8. 조경 및 수목 관리에 관한 사항",
					"9. 전시시설 안전에 관한 사항",
					"10. 전시관 내·외 전시디자인에 관한 사항",
					"11. 전시장 관람환경 개선에 관한 사항",
					"12. 자원봉사자 운영 및 지원에 관한 사항",
					"13. 전시관 운영요원의 교육 및 관리에 관한 사항",
					"14. 전시품 관리 및 수장고 운영에 관한사항"
				],
				members: [

				          
				          {
				            name : '유창영',
				            dept : '전시기반조성과',
				            position : '과장',
				            mission : '전시기반조성과 업무 총괄',
				            phone : '02-3677-1401'
				          },
				    
				          {
				            name : '유봉진',
				            dept : '전시기반조성과',
				            position : '사무관',
				            mission : '중장기 전시공간 재배치 계획 수립·시행<br>국가 위임사무(비영리법인 설립 인허가) 관리<br>과학기술 관련 단체 후원·지원<br>「세계전동차 역사 기획전 인·허가사항 및 기본계획(안)」 수립',
				            phone : '02-3677-1411'
				          },
				    
				          {
				            name : '이춘호',
				            dept : '전시기반조성과',
				            position : '연구사',
				            mission : '다목적 컨벤션센터 기본개념 수립<br>상설 전시기반시설 및 편의시설 기획·설치·운영<br>「세계전동차 역사 기획전 전시기획 및 인테리어 계획(안)」 수립',
				            phone : '02-3677-1412'
				          },
				    
				          {
				            name : '김선혜',
				            dept : '전시기반조성과',
				            position : '연구사',
				            mission : '조경 및 수목 관리 종합계획 수립·시행<br>옥외 전시기반시설 및 편의시설 기획·설치·운영<br>전시·과학기술자료실 운영 관리',
				            phone : '02-3677-1414'
				          },
				    
				          {
				            name : '정명기',
				            dept : '전시기반조성과',
				            position : '주무관',
				            mission : '전시품 보수요원 운영·관리<br>전시품 관리 및 수장고 운영<br>전시시설분야 안전관리',
				            phone : '02-3677-1415'
				          },
				    
				          {
				            name : '김세용',
				            dept : '전시기반조성과',
				            position : '주무관',
				            mission : '전시품유지관리시스템 운영 및 유지관리<br>전시품 유지보수 위탁사업 발주·관리<br>전시품 운영현황 조사·안내 총괄<br>전시·연구자료 수증·수탁 및 보존관리',
				            phone : '02-3677-1416'
				          },
				    
				          {
				            name : '이순옥',
				            dept : '전시기반조성과',
				            position : '주무관',
				            mission : '자원봉사자 활용 및 운영계획 수립·시행<br>전시·학술·연구자료의 수증·수탁 관리 총괄<br>수증·수탁 및 수장물품 활용 효율화 계획 수립',
				            phone : '02-3677-1413'
				          }
				            
				      
				]
			},
			"org10": {
				name: "과학탐구교육과",
				works: [			
					"1. 과학교육 종합계획 수립·시행에 관한 사항",
					"2. 학교 밖 과학탐구 프로그램 개발 및 운영에 관한 사항",
					"3. 과학교사 등 전문직 연수프로그램 개발 및 운영에 관한 사항",
					"4. 과학기술전시전문가 육성에 관한 사항",
					"5. 학부모 등 성인 대상 과학교육프로그램 기획·운영에 관한 사항",
					"6. 과학기술 체험·교육시설 운영관리 및 기자재 확보에 관한 사항",
					"7. 과학캠프프로그램 및 관련시설 관리운영에 관한 사항", 
					"8. 과학기술 자료실(학무모쉼터) 운영에 관한 사항",
					"9. 동아리 Lab 운영에 관한 사항 ",
					"10. 국·내외 과학교육기관의 조사 분석에 관한 사항",
					"11. 그 밖에 국립과천과학관 내 다른 과·팀에 속하지 아니하는 과학교육에 관한 사항"
				],
				members: [
					{name: '이인일',dept: '과학탐구교육과',position: '과장',mission: '과학탐구교육과 업무 총괄',phone: '02-3677-1520'},
					{name: '안효정',dept: '과학탐구교육과',position: '사무관',mission: '개인(주말) 학교 밖 수요 맞춤형 프로그램 개발·운영<br>자기주도 학습콘텐츠 개발',phone: '02-3677-1522'},
					{name: '구수정',dept: '과학탐구교육과',position: '연구관',mission: '중단기 과학교육 종합계획 수립·시행<br>대내외 교육 협력 및 지원 총괄<br>과학교육분야 연구 및 조사·분석<br>교사 및 학부모연수 프로그램 기획·운영',phone: '02-3677-1521'},
					{name: '양회정',dept: '과학탐구교육과',position: '연구사',mission: '진로체험 등 자유학기제 프로그램 개발·운영<br>과학캠프 프로그램 기획·운영<br>캠프장 숙소 시설 운영관리',phone: '02-3677-1525'},
					{name: '김완숙',dept: '과학탐구교육과',position: '주무관',mission: '청춘과학대학 기획·운영<br>교육시설(강의·실험실)의 대관에 관한 사항',phone: '02-3677-1524'},
					{name: '최주한',dept: '과학탐구교육과',position: '주무관',mission: '단체(주중) 창의적 체험활동 프로그램 기획·운영<br>과학관련 이벤트 및 교육행사 기획·운영<br>교육관 시설 및 물품 운영·관리과학교육 위탁사업 운영·관리',phone: '02-3677-1523'}
				]
			},
			"org11": {
				name: "과학문화진흥과",
				works: [			
					"1. 과학기술 문화분야(어울림홀, 상상홀, 창조홀, 야외문화공간 등) 시설 운영계획 수립·시행에 관한 사항",
					"2. SF페스티벌 등 과학문화행사 기획 및 운영에 관한 사항", 
					"3. 특별전 및 기획전시 기획·유치·운영에 관한 사항",
					"4. 과학문화 공연프로그램 기획·운영에 관한 사항",
					"5. 과학탐구 여행프로그램 기획·운영에 관한 사항",
					"6. 국내·외 과학행사의 섭외ㆍ유치·운영 및 지원에 관한 사항", 
					"7. 과학문화체험프로그램 기획 및 운영에 관한 사항",
					"8. 그 밖에 다른 과·팀에 속하지 아니하는 과학문화행사에 관한 사항"
				],
				members: [
					{name:"우사임",dept:"과학문화진흥과",position:"과장",mission:"과학문화진흥과 업무 총괄",phone:"02-3677-1420"},{name:"안인선",dept:"과학문화진흥과",position:"연구사",mission:"과학노벨상 관련 행사 기획·운영<br>\n과천과학탐구여행 기획·운영<br>\n특별전기획전 기획·운영",phone:"02-3677-1423"},{name:"김미란",dept:"과학문화진흥과",position:"주무관",mission:"골드버그대회 기획·운영<br>\n어울림홀 대관 활용계획 수립 및 운영<br>\n과학문화공연 기획·유치<br>\n통계로 보는 세상 특별전 기획·운영",phone:"02-3677-1428"},{name:"노한숙",dept:"과학문화진흥과",position:"주무관",mission:"과학문화사업 기본계획 수립<br>\n미래상상SF축제, 특별전 등 행사 및 기획전 총괄",phone:"02-3677-1421"},{name:"정상헌",dept:"과학문화진흥과",position:"주무관",mission:"수학문화축전 기획·운영<br>\n미래상상SF축제 운영 지원<br>\n과학문화부문 통계 관리 및 분석에 관한 사항",phone:"02-3677-1425"},{name:"배부영",dept:"과학문화진흥과",position:"연구사",mission:"자체 및 공동기획 특별전(교류전) 기본계획 수립<br>\n노벨상, 종이접기 특별전 등 행사 및 기획전 총괄<br>\n한불수교 130주년 특별전, 바이오아트 특별전 기획·운영",phone:"02-3677-1426"},{name:"윤덕호",dept:"과학문화진흥과",position:"주무관",mission:"과학문화광장 활용 사업 기획·운영<br>\n과학문화시설 대관, 개선 및 유지보수에 관한 사항<br>\n과학문화시설 운영 통계관리 및 분석에 관한 사항<br>\n연구개발성과전시, 우표전시 등 공동기획전 운영",phone:"02-3677-1429"},{name:"나민환",dept:"과학문화진흥과",position:"연구사",mission:"V-Robot Live 특별전 기획·운영<br>\n도시농업특별전 기획·운영<br>\nSF축제, SF전시체험, 수학문화축전 전시 기획·운영<br>\n국내·외 과학문화행사 사례조사·분석",phone:"02-3677-1427"},{name:"이태우",dept:"과학문화진흥과",position:"연구사",mission:"해피사이언스 메이커랜드 기획·운영<br>\n미래상상SF축제 기획·운영<br>\n미래상상SF전시체험 기획·운영<br>\n국내·외 과학문화행사 협력사업 발굴 추진",phone:"02-3677-1422"},{name:"한가빈",dept:"과학문화진흥과",position:"연구사",mission:"전국청소년과학송경진대회 기획·운영<br>\n예술에 자연을 담다 특별전 기획·운영<br>\n종이접기 특별전 기획·운영<br>\n바이오아트 특별전 기획·운영 지원",phone:"02-3677-1424"}
				]
			}


	}
</script>

<script type="text/javascript">
	var source   = $("#timeline-template").html();
	var template = Handlebars.compile(source);
	
	var searchSource = $("#search-template").html();
	var searchTemplate = Handlebars.compile(searchSource);

	$(document).on("click", ".organization-item", function(e){
		var orgName = $(this).data("org");
		var html = template(organizations[orgName]);
		$("#organization-details").html(html);
	});
	$('.organization-item:first').click();
	
	$("#org-search-btn").click(function(){
		var searchText = $("#org-search-text").val();
		if ( searchText == "" ) {
			alert("검색어를 입력해 주세요");
		} else {
			var results = [];
			var members = [];

			_.forEach(organizations, function(v, k){
				members = _.find(v.members, function(m){
					return (m.name.indexOf(searchText) >= 0 || m.dept.indexOf(searchText) >= 0);
				});
				
				if ( !_.isUndefined(members) ) {
					results = _.concat(results, members);	
				}
			});
			
			var searchHtml = searchTemplate({members: results});
			$("#organization-details").html(searchHtml);
		}
	});
	

	var members=[];

	var removeElements = function(text, selector) {
	    var wrapped = $("<div>" + text + "</div>");
	    wrapped.find(selector).remove();
	    return wrapped.html();
	}

	/* $('.dataTableBlue.mTm tbody tr').each(function(){
	    var item = {};

	    console.log("====================");
	    console.log($($(this).children("td")[0]).html());
	    var item = {
	      name : $($(this).children("td")[0]).html(),
	      dept : $($(this).children("td")[1]).html(),
	      position : $($(this).children("td")[2]).html(),
	      mission : $($(this).children("td")[3]).html(),
	      phone : $($(this).children("td")[4]).html()
	    };    

	    members.push(item);

	    console.log("====================")
	});

	JSON.stringify(members); */
</script>