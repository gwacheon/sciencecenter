<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="sub-content-nav scrollspy-blue">
	<c:import url="/WEB-INF/views/templates/detailedNavbar.jsp"/>
</div>


<div class="sub-body">
	<div id="intro-body">	
		<div id="basic-info-spy" class="narrow-sub-top introduce">
			<div class="sub-banner-tab-wrapper sub-banner-tab-blue">
				<div class="container">
					<div class="row">
						
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="section">
				<div class="row">
					<div class="col col-md-3">
						<h3 class="page-title blue">
							<div class="top-line"></div>     
							과학기술 자료실
						</h3>
					</div>
				</div>
				<div class="row margin-bottom20">
					<div class="col-md-12">
						국립과천과학관 과학기술자료실은 한국 과학기술의 역사를 볼 수 있는 과학기술자료를 수집/보존/연구하는 기관입니다.<br>과학기술자료실에는 전통시대와 근현대시대의 과학 기술 관련 문헌류, 사진, 영상류, 박물류 등의 자료를 열람할 수 있으며, 매년 우리 과학기술의 역사를 보여주는 특별전시와 상설전시를 개최하고 있습니다.
					</div>
				</div>
				<div class="sub-section margin-bottom30">  
					<div class="row">
						<div class="col-sm-6 col-md-6 margin-bottom20">
							<div>
								<img src="<c:url value='../img/intro/archieve_logo.png' />" class="img-responsive" />
							</div>
							<ul class="circle-list blue">
								<li>
									이용시간 및 휴관일 안내
									<ul>
										<li>
											위치 : 국립과천과학관 입구 왼편 '교육관' 3층
										</li>
										<li>
											자료열람시간 : 화~일 09:30 ~ 17:30
										</li>
										<li>
											휴관일 : 매주 월요일(단, 월요일이 공휴일인 경우 공휴일 다음 날), 설날 및 추석, 임시공휴일					
										</li>
										<li>
											홈페이지 : <a href="http://archives.scientorium.go.kr">http://archives.scientorium.go.kr</a>							
										</li>
									</ul>
								</li>
							</ul>
							<div class="content">
								
								<div class="content-title-wrapper">
									<ul class="arrow-list green content-title">
									</ul>
								</div>
								<div class="content-title-wrapper">
									<ul class="arrow-list green content-title">
									</ul>
								</div>
								<div class="content-title-wrapper">
									<ul class="arrow-list green content-title">
									</ul>
								</div>								
							</div>
						</div>					
						<div class="col-sm-6 col-md-6 margin-bottom30">  
							<img src="<c:url value='../img/intro/archive01.png' />" class="img-responsive" />
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6 col-md-6 margin-bottom30">  
							<img src="<c:url value='../img/intro/archive02.png' />" class="img-responsive" />
						</div>
						<div class="col-sm-6 col-md-6 margin-bottom30">  
							<img src="<c:url value='../img/intro/archive03.png' />" class="img-responsive" />
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>