<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="container">
	<div class="row margin-bottom30">
		<div class="col-sm-12">
			<h4 class="page-title">단체 프로그램 관리</h4>
		</div>
		
		<div class="col-sm-12 margin-bottom30">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>프로그램명</th>
						<th class="center">최대 수강인원</th>
						<th class="center">최소 예약가능일</th>
						<th class="center">상태</th>
						<th class="center">비고</th>
					</tr>
				</thead>
				
				<tbody>
					<c:forEach var="orgProgram" items="${orgPrograms }">
						<tr>
							<td>
								${orgProgram.title }
							</td>
							<td class="center">
								${orgProgram.maxMember }
							</td>
							<td class="center">
								${orgProgram.minDay }
							</td>
							<td class="center">
								<spring:message code="orgProgram.used.${orgProgram.used }" />
							</td>
							<td class="center">
								<a href="<c:url value='/admin/orgPrograms/${orgProgram.id }/edit'/>"
									class="btn-white btn btn-sm">
									수정
								</a>
								<a href="<c:url value='/admin/orgPrograms/${orgProgram.id }/applicants'/>"
									class="btn-white btn btn-sm">
									예약내역
								</a>
								<c:choose>
									<c:when test="${!orgProgram.used }">
										<a href="<c:url value='/admin/orgPrograms/${orgProgram.id }/active'/>" 
											class="edit-status-btn btn btn-white btn-sm"
											data-status="${orgProgram.status }">
											공개
										</a>
									</c:when>
									<c:otherwise>
										<a href="<c:url value='/admin/orgPrograms/${orgProgram.id }/inactive'/>"
											class="edit-status-btn btn btn-danger btn-sm"
											data-status="${orgProgram.status }">
											비공개
										</a>
									</c:otherwise>
								</c:choose>
								
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		
		<div class="col-sm-12">
			<a href="<c:url value="/admin/orgPrograms/new"/>" class="right btn-primary btn">
				프로그램 등록
			</a>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$(".edit-status-btn").click(function(e){
			if(!confirm("선택하신 프로프로그램의 상태를 변경하시겠습니까?")){
				e.preventDefault();
				return;
			} 
		});
	});
</script>