<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="container">
	<div class="row margin-bottom30">
		<div class="col-sm-12">
			<h4>${program.title } <small>예약내역</small></h4>
		</div>
		
		<c:url var="action" value='/admin/orgPrograms/${program.id }/applicants'/>
		<form:form id="orgApplicantSearchForm" action="${action }" class="col-sm-12"
			method="GET" commandName="searchApplicant" modelAttribute="searchApplicant">
			<div class="row">
				<div class="input-field col-sm-2">
					<div class="form-group">
						<form:label path="searchBeginDate">날짜검색(시작일)</form:label>
						<form:input path="searchBeginDate" class="datepicker form-control" placeholder="yyyy-mm-dd"/>
					</div>
				</div>
				<div class="input-field col-sm-2">
					<div class="form-group">
						<form:label path="searchEndDate">날짜검색(종료일)</form:label>
						<form:input path="searchEndDate" class="datepicker form-control" placeholder="yyyy-mm-dd"/>
					</div>
				</div>
				<div class="input-field col-sm-2">
					<div class="form-group">
						<label for="searchTitle">예약자명</label>
						<form:input path="searchTitle" class="form-control"/>
					</div>
				</div>
				<div class="input-field col-sm-2">
					<div class="form-group">
						<label for="searchStatus">상태</label>
						<c:set var="selectedStatus" value="${searchApplicant.searchStatus }"/>
						<form:select path="searchStatus" class="form-control">
							<option value="" <c:if test="${selectedStatus == '' }">selected</c:if>>전체</option>
							<option value="pending" <c:if test="${selectedStatus == 'pending' }">selected</c:if>>대기</option>
							<option value="reject" <c:if test="${selectedStatus == 'reject' }">selected</c:if>>거절</option>
							<option value="cancel" <c:if test="${selectedStatus == 'cancel' }">selected</c:if>>취소</option>
						</form:select>
					</div>
				</div>
				
				<div class="input-field col-sm-2">
					<div class="form-group margin-top30">
						<button type="submit" id="search-btn" class="btn btn-primary">
							검색
						</button>
					</div>
				</div>
			</div>
		</form:form>

		<div class="col-sm-12 margin-bottom30">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>예약번호</th>
						<th>예약자명 (Member No.)</th>
						<th>예약날짜</th>
						<th>인원</th>
						<th>상태</th>
						<th>비고</th>
					</tr>
				</thead>
				
				<tbody>
					<c:choose>
						<c:when test="${not empty applicants }">
							<c:forEach var="applicant" items="${applicants }">
								<tr>
									<td>${applicant.id }</td>
									<td>
										<a href="#" class="show-member-detail-btn"
											data-memberno="${applicant.memberNo }">
											${applicant.title } (${applicant.memberNo })
										</a>
									</td>
									<td>
										<strong>
											<fmt:formatDate pattern="yyyy/MM/dd HH:mm" value="${applicant.applicationTime }" />
										</strong>
									</td>
									<td>
										${applicant.members }
										(<spring:message code="orgProgram.type.${applicant.applicationType }" />)
									</td>
									<td>
										<spring:message code="orgProgram.status.${applicant.applicationStatus }" />
									</td>
									<td>
										<!--
										<c:choose>
											<c:when test="${applicant.applicationStatus == 'pending'}">
												<a href="#"
													class="btn btn-white">
													수락
												</a>
												<a href="#"
													class="btn btn-danger">
													반려
												</a>
											</c:when>
										</c:choose>
										-->
									</td>
								</tr>
							</c:forEach>
						</c:when>
						
						<c:otherwise>
							<tr>
								<td colspan="6" class="text-center">
									조회 된 예약 내역이 없습니다.
								</td>
							</tr>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
			<div>
				<c:import url="/WEB-INF/views/layouts/pagination.jsp"></c:import>
			</div>
		</div>
	</div>
</div>

<div id="request-applicant-modal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">단체 예약 신청자 조회</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered">
					<tr>
						<td>이름</td>
						<td id="request-applicant-name"></td>
					</tr>
					<tr>
						<td>이메일</td>
						<td id="request-applicant-email"></td>
					</tr>
					<tr>
						<td>연락처</td>
						<td id="request-applicant-phone"></td>
					</tr>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">확인</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script type="text/javascript">
$(document).ready(function() {
	$('.datepicker').pickadate({
		selectMonths : true, // Creates a dropdown to control month
		selectYears: 3, // Creates a dropdown of 15 years to control year
		format: 'yyyy-mm-dd'
	});
	
	$(".show-member-detail-btn").click(function(e){
		e.preventDefault();
		var memberNo = $(this).data('memberno');
		
		$.getJSON(baseUrl + "admin/orgPrograms/getMemberDetail/" + memberNo, function(data){
			console.log(data);
			$("#request-applicant-name").text(data.member.memberName);
			$("#request-applicant-email").text(data.member.memberEmail);
			$("#request-applicant-phone").text(data.member.memberCel);
			$("#request-applicant-modal").modal();
		});
		
	});
});
</script>