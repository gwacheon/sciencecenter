<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">${orgProgram.title } 수정</h4> 
		</div>
		
		<div class="col-sm-12">
			<c:url var="action" value="/admin/orgPrograms/${orgProgram.id }"></c:url>
			<c:import url="/WEB-INF/views/admin/orgPrograms/form.jsp">
				<c:param name="action" value="${action }"></c:param>
				<c:param name="method" value="PUT"></c:param>
			</c:import>
		</div>
	</div>
</div>