<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>	
	
<form:form id="org-program-form" action="${param.action }" method="${param.method }"
	modelAttribute="orgProgram" commandName="orgProgram">
	<div class="row margin-bottom30">
		<div class="input-field col-sm-6">
			<div class="form-group">
				<form:label path="title">프로그램 명</form:label>
				<form:input type='text' name='title' path='title' autofocus="autofocus" 
					class="form-control"/>
			</div>
		</div>
		
		<div class="input-field col-sm-2">
			<div class="form-group">
				<form:label path="minDay">최소 예약일</form:label>
				<form:input type='number' name='minDay' path='minDay' class="form-control"/>
			</div>
		</div>
		
		<div class="input-field col-sm-2">
			<div class="form-group">
				<form:label path="maxMember">최대 예약인원</form:label>
				<form:input type='number' name='maxMember' path='maxMember' class="form-control"/>
			</div>
		</div>
		<div class="col-sm-2">
			<div class="form-group margin-top30">
				<div class="checkbox checkbox-teal">
					<input type="checkbox" name="used" id="used" class="styled" <c:if test="${orgProgram.used }">checked</c:if>>
					<label for="used"> 공개 여부</label>
				</div>
			</div>
		</div>
	</div>
	<div class="row margin-bottom30">
		<div class="col-sm-12 input-field">
			<form:label path="description" style="display: none;">내용</form:label>
			<form:textarea cols="80" path="description" name="description" rows="10"></form:textarea>
		</div>
	</div>
	
	<div class="row margin-bottom30">
		<div class="col-sm-12">
			<div class="actions">
				<button class="btn btn-primary right" type="submit"
					name="action">저장</button>
			</div>
		</div>
	</div>
</form:form>

<script type="text/javascript">
<!--
	CKEDITOR.replace( 'description', {
	   filebrowserImageUploadUrl: baseUrl + 'attatchments' + '?${_csrf.parameterName}=${_csrf.token}'
} );
//-->
</script>