<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>

<h3>석식신청 관리</h3>
<ul class="list-group">
	<li class="list-group-item <c:if test="${currentSideMenu == 'applicationsToday'}">active</c:if>">
		<a href='<c:url value='/admin/dinnerApply' />'>
			금일 신청 내역
		</a>
	</li>
	<li class="list-group-item <c:if test="${currentSideMenu == 'applicationTime'}">active</c:if>">
		<a href='<c:url value='/admin/dinnerApply/applicationTime' />'>
			신청시간 조정
		</a>
	</li>
	<li class="list-group-item <c:if test="${currentSideMenu == 'users'}">active</c:if>">
		<a href='<c:url value='/admin/dinnerApply/users' />'>
			계정 관리
		</a>
	</li>
	<li class="list-group-item <c:if test="${currentSideMenu == 'allHistories'}">active</c:if>">
		<a href='<c:url value='/admin/dinnerApply/allHistories' />'>
			총 신청 내역
		</a>
	</li>
</ul>