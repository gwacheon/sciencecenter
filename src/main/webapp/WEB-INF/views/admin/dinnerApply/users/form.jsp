<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>

<form:form id="create-dinner-apply-user-form"
	action="${param.action}?${_csrf.parameterName}=${_csrf.token}"
	method="${param.method }" commandName="dinnerApplyUser" modelAttribute="dinnerApplyUser">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="form-group">
				<form:label path="userId">아이디</form:label>		
				<form:input type="text" path="userId" class="user-input form-control" data-name="아이디"/>
			</div>
		</div>
	</div>
	
	<c:choose>
		<c:when test="${ formType eq 'edit' }">
			<input type="hidden" id="id" name="id" value="${dinnerApplyUser.id }" />
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="form-group">
						<label for="currentPassword">현재 비밀번호</label>
						<input type="password" id="currentPassword" class="user-input form-control" data-name="현재 비밀번호"/>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="form-group">
						<form:label path="password">새 비밀번호</form:label>		
						<form:input type="password" path="password" class="user-input form-control" data-name="새 비밀번호"/>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="form-group">
						<label for="passwordConfirmation">새 비밀번호 확인</label>
						<input type="password" id="passwordConfirmation" class="user-input form-control" data-name="새 비밀번호 확인"/>
					</div>		
				</div>
			</div>
		</c:when>
		<c:otherwise>
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="form-group">
						<form:label path="password">비밀번호</form:label>		
						<form:input type="password" path="password" class="user-input form-control" data-name="비밀번호"/>
					</div>					
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="form-group">
						<label for="passwordConfirmation">비밀번호 확인</label>		
						<input type="password" id="passwordConfirmation" class="user-input form-control" data-name="비밀번호 확인"/>
					</div>
				</div>
			</div>
		</c:otherwise>
	</c:choose>

	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="actions text-right form-group">
				<button type="button" class="btn btn-danger btn-cancel">취소</button>
				<button id="create-user-btn" class="btn btn-primary" type="button"
					name="action">저장</button>
			</div>
		</div>
	</div>
</form:form>


<script type="text/javascript">
	//form cancel button event
	$('.btn-cancel').on('click', function() {
		if (confirm("취소하시겠습니까?")) {
			window.history.back();
		}
	});
	
	$("#create-user-btn").on("click", function(e) {
		if( isInputValid() ) {
			sendFormData();
		}
	});
	
	function isInputValid() {
		var noBlankInput = true;
		
		$(".user-input").each( function() {
			var inputValue = $(this).val();
			
			if( inputValue == null || inputValue == "") {
				alert( $(this).data("name") + " 을 입력해주세요.");
				$(this).focus();
				noBlankInput = false;
				return false;
			}

		});
		if( !noBlankInput ) {
			return false;
		}
		else {
			if( $("#password").val() != $("#passwordConfirmation").val() ) {
				alert( "비밀번호와 확인 값이 일치하지 않습니다." );
				return false;
			}			
		}		
		
		return true;
	}
</script>

<c:choose>
	<c:when test="${formType eq 'edit' }">
		<script type="text/javascript">
			function sendFormData() {
				//비어있는 필드가 없을 경우,
				if(isInputValid()) {
					
					$.ajax({
						url: baseUrl + "admin/dinnerApply/users/checkPassword",
						contentType : "application/json; charset=utf-8",
						beforeSend: function(xhr) {
							ajaxLoading();
							xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));
						},
						data: JSON.stringify({
							dinnerApplyUser: {
								'id': "<c:url value='${dinnerApplyUser.id}' />",
								'password': $("#currentPassword").val()
							}
						}),
						type: "POST",
						dataType: "JSON",
						success: function(data) {							
							if(!data.error) {
								$("form[id='create-dinner-apply-user-form']").submit();
							}
							else {
								ajaxUnLoading();
								alert(data.error_msg);
								$("#currentPassword").focus();
							}						
						},
						error: function(err) {
							alert(err);
							
						}
					}) // ajax END...
				}
			}
		</script>
	</c:when>
	<c:otherwise>
		<script type="text/javascript">
		function sendFormData() {
			$("form[id='create-dinner-apply-user-form']").submit();
		}
		</script>
	</c:otherwise>
</c:choose>




