<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt" %>

<div class="container">
	<div id="admin-board-index" class="margin-bottom30">
		<div class="row">		
			<div class="col-xs-12 col-sm-4 col-md-3">
				<c:import url="/WEB-INF/views/admin/dinnerApply/side.jsp" />
			</div>
			
			<div class="col-xs-12 col-sm-8 col-md-9">
				<h3>석식 신청 계정 목록</h3>
				<table class="table table-striped">
					<thead>
						<tr>
							<th width="10%">번호</th>
							<th width="20%">아이디</th>
							<th width="20%">생성일</th>
							<th width="20%">수정일</th>
							<th width="30%"></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${dinnerApplyUsers }" var="user">
							<tr>
								<td>
									<c:out value="${user.id }" />
								</td>
								<td>  
									<c:out value="${user.userId }" />
								</td>
								<td>
									<fmt:formatDate pattern="yyyy-MM-dd" value="${user.regDttm}" />
								</td>   
								<td>
									<fmt:formatDate pattern="yyyy-MM-dd" value="${user.updtDttm}" />
								</td>   
								<td>
									<c:url var="deleteUrl" value="/admin/dinnerApply/users/${user.id }" />
									<form:form action="${deleteUrl }" method="delete" class="delete-form" data-id="${user.id }">
										<a href="<c:url value='/admin/dinnerApply/users/${user.id}/edit' />" class="btn btn-warning btn-sm">수정</a>
											<button type="button" data-id="${user.id }" class="delete-btn btn btn-danger btn-sm">삭제</button>
									</form:form>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<div>
					<c:import url="/WEB-INF/views/layouts/admin/pagination.jsp"></c:import>
				</div>
				<div class="text-right">
					<a href="<c:url value='/admin/dinnerApply/users/new' />" class="btn btn-primary">
						계정 생성  
					</a>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(".delete-btn").click(function(e){
		var id = $(this).data("id");
		
		if(confirm("해당 계정을 정말 삭제 하시겠습니까?")){
			$("form[data-id='" + id + "']").submit();	
		}
	});
</script>
