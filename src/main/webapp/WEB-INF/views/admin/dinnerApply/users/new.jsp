<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
	
<div id="admin-board-index" class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-4 col-md-3">
				<c:import url="/WEB-INF/views/admin/dinnerApply/side.jsp" />
			</div>
		<div class="col-xs-12 col-sm-8 col-md-9">
			<h3>계정 생성</h3>
			<div class="row">
				<c:url var="action" value='/admin/dinnerApply/users' />
				<c:url var="method" value='POST' />
			</div>

			<jsp:include page="/WEB-INF/views/admin/dinnerApply/users/form.jsp">
				<jsp:param name="action" value="${action}" />
				<jsp:param name="method" value="${method}" />
			</jsp:include>
		</div>
	</div>
</div>


<script type="text/javascript">
<!--
	CKEDITOR.replace( 'content', {
	   filebrowserImageUploadUrl: baseUrl + 'attatchments' + '?${_csrf.parameterName}=${_csrf.token}'
} );
//-->
</script>
