<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt" %>

<div class="container">
	<div id="admin-board-index" class="margin-bottom30">
		<div class="row">		
			<div class="col-sm-12 col-md-3">
				<c:import url="/WEB-INF/views/admin/dinnerApply/side.jsp" />
			</div>
			
			<div class="col-sm-12 col-md-9">
				<h3>전체 신청 내역</h3>
				<table class="table table-striped">
					<thead>
						<tr>
							<th width="0%">번호</th>
							<th width="30%">신청일</th>
							<th width="30%">부서</th>
							<th width="30%">이름</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${empty dinnerApplies }">
								<tr>
									<td colspan="4" class="center-align">
										금일 신청 내역이 없습니다.
									</td>
								</tr>
							</c:when>
							<c:otherwise>  
								<c:forEach items="${dinnerApplies }" var="dinner">
									<tr>
										<td>
											<c:out value="${dinner.id }" />
										</td>
										<td>
											<fmt:formatDate pattern="YYYY-MM-dd HH:mm:ss" value="${dinner.regDttm}" />
										</td>   
										<td>  
											<c:out value="${dinner.department }" />
										</td>
										<td>
											<c:out value="${dinner.name }" />
										</td>
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
				<div>
					<c:import url="/WEB-INF/views/layouts/admin/pagination.jsp"></c:import>
				</div>
			</div>
		</div>
	</div>
</div>

