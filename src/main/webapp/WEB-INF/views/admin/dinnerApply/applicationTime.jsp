<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt" %>

<div class="container">
	<div id="admin-board-index" class="margin-bottom30">
		<div class="row">		
			<div class="col-xs-12 col-sm-4 col-md-3">
				<c:import url="/WEB-INF/views/admin/dinnerApply/side.jsp" />
			</div>
			
			<div class="col-xs-12 col-sm-8 col-md-9">
				<h3>신청시간 조정</h3>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<c:choose>
									<c:when test="${ empty dinnerApplyTime.applyBeginTime }">										
										현재 설정한 신청시간이 없습니다.										
									</c:when>
									<c:otherwise>
										현재 설정된 신청 시간 : <br>
										시작 : <fmt:formatDate value="${dinnerApplyTime.applyBeginTime}" pattern="HH:mm"/><br> 
										종료 : <fmt:formatDate value="${dinnerApplyTime.applyEndTime}" pattern="HH:mm"/>
									</c:otherwise>
								</c:choose>
							</div>
					  	</div>
					</div>
				</div>
				<div class="row">
					<c:url var="createApplicationTimeUrl" value="/admin/dinnerApply/applicationTime/create" />
					<c:url var="method" value="POST"/>
					<form id="createApplicationTimeForm"
						action="${createApplicationTimeUrl}?${_csrf.parameterName}=${_csrf.token}"
						method="${method }" class="form-horizontal">
						<div class="col-md-5">
							<div class="form-group">
								<label for="applyBeginTime" class="col-md-4 control-label">신청 시작 시간</label>		
								<div class="col-md-8">
									<input type="time" name="applyBeginTime" id="applyBeginTime" data-name="신청 시작 시간" class="form-control"/>
								</div>
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<label for="applyEndTime" class="col-md-4 control-label">신청 종료 시간</label>
								<div class="col-md-8">
									<input type="time" name="applyEndTime" id="applyEndTime" data-name="신청 종료 시간" class="form-control"/>
								</div>
							</div>		
						</div>
						<div class="col-md-2 text-right">
							<div class="actions">
								<button id="create-application-time-btn" class="btn btn-primary" type="button">
									변경
								</button>
							</div>
						</div>
					</form>
				</div>
			</div> 
		</div>
	</div>
</div>

<script type="text/javascript">
	$("#create-application-time-btn").on("click", function(e) {
		e.preventDefault();
		
		var isBlank = false;
		
		$("input[type='time']").each( function() {
			if( $(this).val() == null || $(this).val() == "") {
				alert( $(this).data("name") + "을 선택해야 합니다.");
				isBlank = true;
				$(this).focus();
				return false;
			}
		});
		
		
		if( isBlank == false ) {
			if( confirm("신청시간을 조정하시겠습니까?")) {
				$("form[id='createApplicationTimeForm']").submit();
			}			
		}
	});
</script>