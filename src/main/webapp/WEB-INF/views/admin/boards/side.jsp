<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>

<c:choose>
	<c:when test="${type.category eq 'mainEvent'}">
		<ul class="list-group margin-bottom30">
			<li class="list-group-item <c:if test="${type.name eq 'mainEventSeriesBasicInfo' }"> active</c:if>">
				<a href="<c:url value='/admin/mainEventSeries/${mainEventSeries.id }' />">
					기본정보 
				</a>	
			</li>
			
			<c:forEach var="visibleBoard" items="${mainEventSeries.visibleBoardNames }">
				<li class="list-group-item <c:if test="${ visibleBoard == type.name}">active</c:if>">
					<a href="<c:url value='/admin/mainEventSeries/${mainEventSeries.id }/${visibleBoard }' />">
						<spring:message code="board.${visibleBoard}" />
					</a>
				</li>
			</c:forEach>		
		</ul>			
		
		<a href="<c:url value='/admin/mainEvents' />" class="btn btn-primary">
			<i class="fa fa-list" aria-hidden="true"></i> 메인행사 목록
		</a>
	</c:when>
	<c:otherwise>
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">
			<c:forEach var="i" begin="1" end="8">
			<spring:message var="main_category" code="main.nav.catetory${i }" scope="application"/>
			<spring:message var="main_category_name" code="main.nav.catetory${i }.name" scope="application"/>
				<div class="panel panel-default">
	  				<div class="panel-heading" role="tab" data-name="${main_category_name }" id="collapseHeading${i }">
	    				<h4 class="panel-title">
	      					<a role="button" data-name="${main_category_name }" data-toggle="collapse" data-parent="#accordion" href="#collapseCategory${i }" aria-expanded="false" aria-controls="collapseCategory${i }">
	        					<c:out value="${main_category }" />
	      					</a>
	    				</h4>
	  				</div>
	  				<div id="collapseCategory${i }" class="panel-collapse collapse" 
	  					role="tabpanel" aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
	  					data-name="${main_category_name }" >
	  					<ul class="list-group side-group inner">
	  						<c:forEach items="${board_type_list}" var="item">
								<c:if test="${ main_category_name eq item.value.category && item.value.name ne 'faqTotal' }">
									<c:url value="/admin/boards?type=${item.key}" var="boardUrl" />
									<li class="list-group-item <c:if test="${item.key == type.name}">active</c:if>" data-name="${main_category_name }">
										<a href='${boardUrl}'>
										      <spring:message code="board.${item.value.name}" />  
										</a>
									</li>
								</c:if>
							</c:forEach>
	  					</ul>
	  				</div>
	  			</div>
	  		</c:forEach>
  		</div>
	</c:otherwise>
</c:choose>


<script type="text/javascript">
	$( function() {
		var currentActiveItem = $('.list-group-item.active');
		var categoryName = currentActiveItem.data('name');
		$(".panel-collapse[data-name='" + categoryName + "']").collapse();
		$(".panel-heading[data-name='" + categoryName + "']").addClass('active');
		
		$('.panel-collapse').on('show.bs.collapse', function () {
		  var categoryName = $(this).data('name');
		  $('.panel-heading').removeClass('active');
		  $(".panel-heading[data-name='" + categoryName + "']").addClass('active');
		});
	});
</script>