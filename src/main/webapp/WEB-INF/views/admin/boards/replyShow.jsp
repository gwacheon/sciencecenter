<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container">
	<div id="admin-board-index" class="margin-bottom30">
		<div class="row">		
			<div class="col-xs-12 col-sm-4 col-md-3">
				<c:choose>
					<c:when test="${type.category eq 'mainEvent'}">
						<h4>${mainEventSeries.name }</h4>
					</c:when>
					<c:otherwise>
						<h4>게시판 관리</h4>
					</c:otherwise>
				</c:choose>
				<c:import url="/WEB-INF/views/admin/boards/side.jsp" />
			</div>
			<div class="col-xs-12 col-sm-8 col-md-9">
				<h4><spring:message code="board.${type.name}" /> 답글</h4>
				
				<div class="row">
					<div class="col-md-12">
					<div class="margin-bottom30 admin-board-content">
						<h4>원문</h4>
						<div class="row">
							<div class="col-md-12">
								<div class="alert alert-info">
									<h4>${board.title }</h4>
									<p>
										${board.content }
									</p>
								</div>
							</div>
						</div>
						<h4>답글</h4>
						<table class="table table-bordered table-striped">  
							<tbody>
								<tr>
									<th>
										제목
									</th>
									<td colspan="3">
										RE: <c:out value="${reply.title }"/>
									</td>
								</tr>
								<tr>
									<th>
										작성자
									</th>
									<td>
										<c:out value="${reply.member.memberName }"/>
									</td>
									<th>
										조회수
									</th>
									<td>
										<c:out value="${reply.count }"/>
									</td>
								</tr>
								<tr>
									<th>
										등록일
									</th>
									<td>
										<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${board.regDttm}" />
									</td>
									<th>
										최근수정일
									</th>
									<td>
										<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${board.updtDttm}" />
									</td>
								</tr>
								<tr>
									<th>내용</th>
									<td colspan="3">
										<c:out value="${board.content }" escapeXml="false"/>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 text-right">
						<c:choose>
							<c:when test="${type.category eq 'mainEvent' }">
								<a href="<c:url value='/admin/mainEventSeries/${mainEventSeries.id }/${board.boardType }' />"
									class="btn btn-primary">
									목록
								</a>	
							</c:when>
							<c:otherwise>
								<a href="<c:url value='/admin/boards?type=${board.boardType }' />"
									class="btn btn-primary">
									게시판 목록
								</a>			
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
	  	</div>
  	</div>
</div>
<script type="text/javascript">
	$(".delete-btn").click(function(e){
		var id = $(this).data("id");
		
		if(confirm("해당 게시물을 정말 삭제 하시겠습니까?")){
			$("form[data-id='" + id + "']").submit();	
		}
	});
	
</script>