<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>

<c:url value="/admin/boards" var="createBoardUrl" />
<form:form
	action="${param.action}?${_csrf.parameterName}=${_csrf.token}"
	method="${param.method }" commandName="board" modelAttribute="board"
	enctype="multipart/form-data">
	
	<input type="hidden" name="boardType" value="${board.boardType}">
	<input type="hidden" name="eventSeriesId" value="${board.eventSeriesId }">
	<c:if test="${board_type.hasMainPicture == true }">
		<c:choose>
			<c:when test="${not empty board.picture }">
				<div id="current-main-image-file" class="col-sm-12 col-md-6">
					<div class="form-group">
						<form:input type="hidden" path="picture" name="picture"/>
						<p>
							현재 설정된 대표 이미지: 
							<button id="main-image-delete-btn" class="btn btn-danger btn-sm">삭제</button>
						</p>
						<img src="<c:url value='${baseFileUrl }' />${board.picture }" class="img-responsive" alt="${board.picture }"/>
					</div>
					
				</div>
				<div id="new-main-image-file">
				
				</div>
			</c:when>
			<c:otherwise>
				<div class="col-md-12">
					<div class="form-group">
						<label for="file">메인이미지</label>					
						<input id="file" type="file" name="file" class="form-control">
					</div>
					<form:input type="hidden" path="picture" name="picture"/>
				</div>
			</c:otherwise>
		</c:choose>
	</c:if>
	
	<div class="col-md-12">
		<div class="form-group">
			<form:label path="title">제목</form:label>
			<form:input type='text' name='title' path='title'
				autofocus="autofocus" required='true' class="form-control"/>
		</div>
	</div>
	<c:if test="${board_type.hasSubTitle == true }">
		<div class="col-md-12">
			<div class="form-group">
				<form:label path="subTitle">부제목</form:label>
				<form:input type='text' name='subTitle' path='subTitle' required='true' class="form-control"/>
			</div>
		</div>	
	</c:if>
	<div class="col-xs-12 col-sm-6 col-md-3">
		<div class="form-group">
			<form:label path="fixedOnTop">상단 고정</form:label>
			<form:select path="fixedOnTop" class="form-control teal">
				<option value="true" <c:if test="${board.fixedOnTop eq 'true' }">selected</c:if>>네</option>
				<option value="false" <c:if test="${board.fixedOnTop eq 'false' }">selected</c:if>>아니오</option>
			</form:select>
		</div>
	</div>
	<c:choose>
		<c:when test="${board_type.urlType == true }">
			<div class="col-md-12">
				<div class="form-group">				
				<form:label path="linkUrl">링크 주소</form:label>
				<form:input path="linkUrl" type='text' name='linkUrl' required='true' class="form-control"/>
				</div>
			</div>
		</c:when>
		<c:otherwise>
			<div class="col-md-12 margin-bottom20">
				<div class="form-group">
				<form:label path="content" class="active">내용</form:label>
				<form:textarea cols="80" path="content" name="content" rows="10" class="form-control"></form:textarea>
				</div>
			</div>
			<div class="col-md-12">
				<div id="video-wrapper">
					<div class="title">
						동영상
					</div>
					<c:if test="${not empty board.videoPath  }">
						<div class="current-video">
							현재 동영상 <buttom class="btn btn-danger video-delete-btn" data-id="${board.id }">삭제</buttom><br>
							<div class="col-md-3">
								<video autoplay="" loop="" controls="" style="width : 100%">
									<source src="<c:url value='${baseFileUrl}' />${board.videoPath }" type="video/mp4">
								</video>
							</div>
						</div>
					</c:if>
					
					<div>
						<div class="form-group">
							<input type="file" name="video" class="form-control">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div id="file-wrapper" class="margin-bottom20">
					<c:if test="${ not empty board.attatchments }">
						<div class="title">
							첨부파일 목록
						</div>
						<c:forEach var="attachment" items="${board.attatchments}" varStatus="status">
							<div id="file-${attachment.id }-item" class="file">
								${attachment.name } <i class="file-delete-btn fa fa-trash fa-lg" aria-hidden="true" data-id="${attachment.id }"></i>							
							</div>
						</c:forEach>
					</c:if>
				</div>
				<div id="board-files-wrapper">
					
				</div>
			</div>
			<c:if test="${ board_type.name ne 'opinions'}">
			<div class="col-md-12">
				<div class="title">
					첨부파일
				</div>
				<a id="add-file" class="btn btn-primary">새 첨부파일 추가</a>
			</div>
			</c:if>
		</c:otherwise>
	</c:choose>
	<c:if test="${board_type.name == 'scienceColumn' }">
		<div class="col-md-12">
			<div class="form-group">
				<form:label path="building">전시관</form:label>
				<form:input path="building" type='text' name='building' required='true' class="form-control"/>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<form:label path="object">전시물</form:label>
				<form:input path="object" type='text' name='object' required='true' class="form-control"/>
			</div>
		</div>
	</c:if>
	<div class="col-md-12">
		<div class="actions text-right margin-bottom20">
		<button type="button" class="btn btn-danger btn-cancel">취소</button>
			<button class="btn btn-primary" type="submit"
				name="action">저장</button>
		</div>
	</div>
</form:form>

<script type="text/javascript">
	
	var index = 0;
	
	$('#add-file').on('click',function(){
		var source = $("#file-template").html();
		var template = Handlebars.compile(source);	
		var data = {
				count :index
		};
		
		$('#board-files-wrapper').append(template(data));
		index++;
	});
	
	// form cancel button event
	$('.btn-cancel').on('click', function() {
		if (confirm("작성을 취소하시겠습니까?")) {
			window.history.back();
		}
	});
	
	
	$('.file-delete-btn').on("click", function(e) {
		e.preventDefault();
		if( confirm("해당 첨부파일이 영구 삭제됩니다.정말로 삭제하시겠습니까?")) {
			
			var fileId = $(this).data('id');
			
			$.ajax({
				type : "POST",
				beforeSend: function(xhr) {
		            xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));
		        },
				url : baseUrl + "admin/boards/" + fileId + "/deleteFile",
				data : JSON.stringify({
					fileId: fileId
				}),
				contentType : 'application/json',
				success : function(data) {
					if( data.success ) {
						alert("파일이 삭제되었습니다.");
						$("#file-" + fileId +"-item").remove();						
					}

				},
				error: function(err){
					alert(err);
				}
			});
		}
	});
	
	$('.video-delete-btn').on('click', function(){
		if( confirm("동영상이 삭제되면, 새로운 동영상을 업로드 해야 합니다. 정말로 삭제하시겠습니까?") ) {
			var boardId = $(this).data('id');
			$.ajax({
				type : "POST",
				beforeSend: function(xhr) {
		            xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));
		        },
				url : baseUrl + "admin/boards/" + boardId + "/deleteVideo",
				data : JSON.stringify({
					boardId: boardId
				}),
				contentType : 'application/json',
				success : function(data) {
					
					alert("동영상이 삭제되었습니다.");
					$(".current-video").remove();						
					

				},
				error: function(err){
					alert(err);
				}
			});
		}
	});
	$(document).on("click", "#main-image-delete-btn", function(e) {
		e.preventDefault();
		if( confirm("대표 이미지가 삭제되면, 새로운 이미지를 업로드 해야 합니다. 정말로 삭제하시겠습니까?") ) {
			$('#current-main-image-file').remove();
			var maineImageFilesource = $("#main-image-file-template").html();
			var mainImageFileTemplate = Handlebars.compile(maineImageFilesource);
			$("#new-main-image-file").append(mainImageFileTemplate);
		}
	});

</script>

<script id="file-template" type="text/x-handlebars-template">
	<div class="form-group">
		<input type="file" name="attatchments[{{count}}].file" class="form-control">
	</div>
</script>


<script id="main-image-file-template" type="text/x-handlebars-template">
<div class="col-md-12">
	<div class="form-group">
		<label for="file">대표이미지</label>
		<input id="file" type="file" name="file" class="form-control">
	</div>
</div>
</script>
