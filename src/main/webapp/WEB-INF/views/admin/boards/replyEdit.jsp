<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>

<div class="container">
	<div id="admin-board-index" class="margin-bottom30">
		<div class="row">		
			<div class="col-xs-12 col-sm-4 col-md-3">
				<c:choose>
					<c:when test="${type.category eq 'mainEvent'}">
						<h4>${mainEventSeries.name }</h4>
					</c:when>
					<c:otherwise>
						<h4>게시판 관리</h4>
					</c:otherwise>
				</c:choose>
				<c:import url="/WEB-INF/views/admin/boards/side.jsp" />
			</div>
			<div class="col-xs-12 col-sm-8 col-md-9">
				<h4><spring:message code="board.${type.name}" /> 답글</h4>
				<div class="row">
					<div class="col-md-12">
						<div class="alert alert-info">
							<h4>${board.title }</h4>
							<p>
								${board.content }
							</p>
						</div>
					</div>
				</div>
				
				<div id="auth-form">
					<div class="row">
						<div class="col-md-12">
						<c:url var="action" value='/admin/boards/reply/${ reply.id }/update' />
						<c:url var="method" value='POST' />
						<c:url value="/admin/boards?type=${board.boardType }" var="replyCancelUrl"/>
				
						<jsp:include page="/WEB-INF/views/admin/boards/replyForm.jsp">
							<jsp:param name="action" value="${action}" />
							<jsp:param name="method" value="${method}" />
							<jsp:param name="replyCancelUrl" value="${ replyCancelUrl}" />
						</jsp:include>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>