<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<div class="container">
	<div id="admin-board-index" class="margin-bottom30">
		<div class="row">		
			<div class="col-xs-12 col-sm-4 col-md-3">
				<c:choose>
					<c:when test="${type.category eq 'mainEvent'}">
						<h3>${mainEventSeries.name }</h3>
					</c:when>
					<c:otherwise>
						<h3>게시판 관리</h3>
					</c:otherwise>
				</c:choose>
				<c:import url="/WEB-INF/views/admin/boards/side.jsp" />
			</div>
			<div class="col-xs-12 col-sm-8 col-md-9">
				
				<h3><spring:message code="board.${type.name}" /></h3>
				
				<div class="table-responsive margin-bottom30">
					<table class="table table-striped">
						<thead>
							<tr>
								<td>
									id
								</td>
								<td>
									제목
								</td>
								<td class="center">
									상단고정
								</td>
								<td> 
									작성자
								</td>
								<td>
								</td>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${boards }" var="board">
								<tr>
									<td>
										<c:out value="${board.id}"></c:out>
									</td>
									<td style="max-width:0; width: 50%; overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">
										<a href="<c:url value='/admin/boards/${board.id }' />">											
											<c:out value="${board.title}"></c:out>
										</a>
									</td>
									<td class="center">
										<c:choose>
											<c:when test="${board.fixedOnTop eq true }">
												<i class="fa fa-circle-o" aria-hidden="true" style="color: green;"></i>
											</c:when>
											<c:otherwise>
												<i class="fa fa-times" aria-hidden="true" style="color: red;"></i>
											</c:otherwise>
										</c:choose>
										<%-- <c:out value="${board.fixedOnTop}" /> --%>
									</td>
									<td>
										<c:out value="${board.member.memberName}"></c:out>
									</td>
									<td>
										<c:choose>
											<c:when test="${type.category eq 'mainEvent'}">
												<c:url value="/admin/boards/${board.id}?type=${board.boardType }&mainEventSeriesId=${mainEventSeries.id }" var="deleteBoardUrl" />
											</c:when>
											<c:otherwise>
												<c:url value="/admin/boards/${board.id}?type=${board.boardType }" var="deleteBoardUrl" />
											</c:otherwise>										
										</c:choose>
										<form:form  action="${deleteBoardUrl}" method="DELETE" data-id="${board.id }">
											<a href="<c:url value='/admin/boards/${board.id}/edit' />" class="btn btn-warning btn-sm">수정</a>
											<button type="button" data-id="${board.id }" class="delete-btn btn btn-danger btn-sm">삭제</button>
										</form:form>
										 
									</td>
								</tr>
								<c:if test="${not empty board.replies }">
									<c:forEach var="reply" items="${board.replies }">
										<tr class="reply" style="background-color: #f2f2f2;">
											<td></td>
											<td class="left title">
												<a href="<c:url value="/admin/boards/reply/${reply.id }/show" />">Re: ${reply.title }</a>
											</td>
											<td class="center">
												-
											</td>
											<td>
												<c:if test="${reply.writtenByAdmin }">
													관리자
												</c:if>
											</td>
											<td>				
												<c:choose>
													<c:when test="${type.category eq 'mainEvent'}">
														<c:url value="/admin/boards/${reply.id}?type=${reply.boardType }&mainEventSeriesId=${mainEventSeries.id }" var="deleteBoardUrl" />
													</c:when>
													<c:otherwise>
														<c:url value="/admin/boards/${reply.id}?type=${reply.boardType }" var="deleteBoardUrl" />
													</c:otherwise>										
												</c:choose>									
												
												<form:form  action="${deleteBoardUrl}" method="DELETE" data-id="${reply.id }">
													<a href="<c:url value='/admin/boards/reply/${reply.id}/edit' />" class="btn btn-warning btn-sm">수정</a>
													<button type="button" data-id="${reply.id }" class="delete-btn btn btn-danger btn-sm">삭제</button>
												</form:form>
												 
											</td>
										</tr>
									</c:forEach>
								</c:if>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div class="row">
					<div class="col-md-12">
						<c:import url="/WEB-INF/views/layouts/adm/pagination.jsp" />
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 text-right"> 
						<c:choose>
							<c:when test="${type.category eq 'mainEvent'}">
								<c:url value="/admin/boards/${type.name}/new?mainEventSeriesId=${mainEventSeries.id }" var="createUrl" />
							</c:when>
							<c:otherwise>
								<c:url value="/admin/boards/${type.name}/new" var="createUrl" />
							</c:otherwise>
						</c:choose>
						
						<a href="${createUrl}" class="btn btn-primary">
							글쓰기 - <spring:message code="board.${type.name}" />  
							
						</a>
						<a href="#" class="btn btn-primary">
							엑셀 다운로드 - <spring:message code="board.${type.name}" />  
						</a>
					</div>
				</div>
			</div>
	  	</div>
  	</div>
</div>
<script type="text/javascript">
	$(".delete-btn").click(function(e){
		var id = $(this).data("id");
		
		if(confirm("해당 게시물을 정말 삭제 하시겠습니까?")){
			$("form[data-id='" + id + "']").submit();	
		}
	});
</script>