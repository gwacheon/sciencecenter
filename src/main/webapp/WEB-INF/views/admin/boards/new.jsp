<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
	
<div id="auth-form" class="container">
	<div class="row">
		<div class="col-md-12">
			<h4>게시글 생성 - <spring:message code="board.${board_type.name}" /></h4>
			<div class="row">
				<c:url var="action" value='/admin/boards' />
				<c:url var="method" value='POST' />

				<jsp:include page="/WEB-INF/views/admin/boards/form.jsp">
					<jsp:param name="action" value="${action}" />
					<jsp:param name="method" value="${method}" />
				</jsp:include>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
<!--
	CKEDITOR.replace( 'content', {
	   filebrowserImageUploadUrl: baseUrl + 'attatchments' + '?${_csrf.parameterName}=${_csrf.token}'
} );
//-->
</script>
