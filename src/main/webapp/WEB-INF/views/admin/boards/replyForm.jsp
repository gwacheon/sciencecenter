<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>

<form:form
	action="${param.action}?${_csrf.parameterName}=${_csrf.token}"
	method="${param.method }" commandName="reply" modelAttribute="reply"
	enctype="multipart/form-data">
	
	<input type="hidden" name="redirectUrl" value="${redirectUrl}">
	<input type="hidden" name="boardType" value="${board.boardType}">
	<form:hidden path="boardId" />
	
	<div class="form-group">
		<form:label path="title">제목</form:label>
		<form:input type='text' name='title' path='title'
			autofocus="autofocus" class="form-control"/>
	</div>
	<div class="form-group">
		<form:label path="content" class="active">내용</form:label>
		<form:textarea cols="80" path="content" name="content" rows="10" class="form-control ckeditor"></form:textarea>
	</div>
	
	<div class="form-group">
		<div class="text-right">
			<button type="button" class="btn btn-danger btn-cancel">취소</button>
			<button class="btn btn-primary" type="submit"
				name="action">저장</button>
		</div>
	</div>
</form:form>

<script type="text/javascript">
	//form cancel button event
	$('.btn-cancel').on('click', function() {
		if (confirm("작성을 취소하시겠습니까?")) {
			window.location = "<c:out value='${param.replyCancelUrl}' />";
		}
	});
</script>