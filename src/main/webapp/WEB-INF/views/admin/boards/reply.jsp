<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>

<div class="container">
	<div id="admin-board-index" class="margin-bottom30">
		<div class="row">		
			<div class="col-xs-12 col-sm-4 col-md-3">
				<c:choose>
					<c:when test="${type.category eq 'mainEvent'}">
						<h3>${mainEventSeries.name }</h3>
					</c:when>
					<c:otherwise>
						<h3>게시판 관리</h3>
					</c:otherwise>
				</c:choose>
				<c:import url="/WEB-INF/views/admin/boards/side.jsp" />
			</div>
			<div class="col-xs-12 col-sm-8 col-md-9">
				<h4>게시판 관리 - <spring:message code="board.${boardType.name}" />  답글 등록하기</h4>
				<div class="row">
					<div class="col-md-12">
						<div class="alert alert-info">
							<h4>${board.title }</h4>
						
							<p>
								${board.content }
							</p>
						</div>
					</div>
				</div>
				
				<div id="auth-form">
					<div class="row">
						<div class="col-md-12">
						<c:url var="action" value='/admin/boards/reply/${id }' />
						<c:url var="method" value='POST' />
						<c:url value="/admin/boards/${board.id }" var="replyCancelUrl"/>
						
						<jsp:include page="/WEB-INF/views/admin/boards/replyForm.jsp">
							<jsp:param name="action" value="${action}" />
							<jsp:param name="method" value="${method}" />
							<jsp:param name="replyCancelUrl" value="${ replyCancelUrl}" />
						</jsp:include>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>