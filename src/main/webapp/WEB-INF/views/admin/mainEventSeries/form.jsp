<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>

<form:form action="${param.action}" method="${param.method }"
	commandName="mainEventSeries" modelAttribute="mainEventSeries">
	<form:input type='hidden' name='mainEventId' path='mainEventId' />
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<form:label path="name" id="name">이름</form:label>
				<form:input type='text' name='name' path='name' class="form-control"/>
			</div>
		</div>
	</div>
	<div class="admin-board-content">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<form:label path="contents" class="active">내용</form:label>
					<form:textarea cols="80" path="contents" name="contents" rows="15" class="form-control"></form:textarea>
				</div>
			</div>
		</div>
	</div>
	<div class="row margin-bottom30">
		<div class="col-md-12">
			<div class="form-group">
				<c:forEach var="visibleItem" items="${mainEventSeries.boardMap }">
				<div class="checkbox checkbox-teal">
			    	<input id="${visibleItem.key }" name="${visibleItem.key }" class="styled" type="checkbox"
				    		<c:if test="${visibleItem.value == true }"> checked</c:if>>
				    <label for="${visibleItem.key }"><spring:message code="board.${visibleItem.key}" /></label>
				</div>
				</c:forEach>
			</div>
		</div>
	</div>
	<div class="row margin-bottom30">
		<div class="col-md-12">
			<div class="form-group">
				<div class="actions text-right">
					<button type="button" class="btn btn-danger btn-cancel">취소</button>
					<button class="btn btn-primary" type="submit"
						name="action">저장</button>
				</div>
			</div>
		</div>
	</div>
</form:form>

<script type="text/javascript">

	//form cancel button event
	$('.btn-cancel').on('click', function() {
		if (confirm("작성을 취소하시겠습니까?")) {
			window.history.back();
		}
	});
<!--
	CKEDITOR.replace( 'contents', {
	   filebrowserImageUploadUrl: baseUrl + 'attatchments' + '?${_csrf.parameterName}=${_csrf.token}'
} );
//-->
	
</script>
