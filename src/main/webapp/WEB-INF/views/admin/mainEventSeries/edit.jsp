<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>

<div id="admin-board-index" class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-4 col-md-3">
			<h4>${mainEventSeries.name }</h4>	
			<c:import url="/WEB-INF/views/admin/boards/side.jsp" />
		</div>
		<div class="col-xs-12 col-sm-8 col-md-9">
			<h3>기본정보 수정</h3>
			<c:url var="action" value='/admin/mainEventSeries/${mainEventSeries.id }/basicInfo/update' />
			<c:url var="method" value='PUT' />

			<jsp:include page="/WEB-INF/views/admin/mainEventSeries/form.jsp">
				<jsp:param name="action" value="${action}" />
				<jsp:param name="method" value="${method}" />
			</jsp:include>
		</div>
	</div>
</div>

