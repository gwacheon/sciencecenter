<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>

<div id="admin-board-index" class="container">
	<div class="row margin-bottom30">
		<div class="col-xs-12 col-sm-4 col-md-3">
			<h4>${mainEventSeries.name }</h4>
			<c:import url="/WEB-INF/views/admin/boards/side.jsp" />
		</div>
		<div class="col-xs-12 col-sm-8 col-md-9">
			<h4>기본정보</h4>
			<div class="admin-board-content margin-bottom20">
				<c:out value="${mainEventSeries.contents }" escapeXml="false"></c:out>
			</div>
			
			<div class="control margin-bottom20 text-right">
				<a href="<c:url value='/admin/mainEventSeries/${mainEventSeries.id }/basicInfo/edit' />" 
					class="btn btn-primary">
					수정
				</a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col s12">
			
		</div>
	</div>
</div>

