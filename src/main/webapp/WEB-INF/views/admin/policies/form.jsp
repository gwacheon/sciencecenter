<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>

<form:form
	action="${param.action}?${_csrf.parameterName}=${_csrf.token}"
	method="${param.method }" commandName="policy" modelAttribute="policy">

	<div class="col-md-4">
		<div class="form-group">
			<form:label path="type">종류</form:label>
			<c:choose>
				<c:when test="${ not empty policy.type}">
					<input type="text" value="<spring:message code="policy.${policy.type}" />" disabled class="form-control"/>
					<input type="hidden" name="type" value="${policy.type }" />
				</c:when>
				<c:otherwise>
					<form:select path="type" class="form-control">
						<c:forEach var="map" items="${policy.typeMap }">
							<option value="${map.key }">${map.value }</option>
						</c:forEach>
					</form:select>			
				</c:otherwise>
			</c:choose>
		</div>
	</div>
	<div class="col-md-12">
		<div class="form-group">
			<form:label path="content">내용</form:label>
			<form:textarea cols="80" path="content" name="content" rows="10" id="editor1" class="form-control"></form:textarea>
		</div>
	</div>
	<div class="col-md-12">
		<div class="form-group">
			<div class="text-right">
				<button type="button" class="btn btn-danger btn-cancel">
					취소
				</button>
				<button class="btn btn-primary" type="submit"
					name="action">저장</button>
			</div>
		</div>
	</div>
</form:form>

<script type="text/javascript">
$(document).ready(function() {
	CKEDITOR.replace("editor1",
 		{
 		     height: 550
 		}
 	);
});

//form cancel button event
$('.btn-cancel').on('click', function() {
	if (confirm("작성을 취소하시겠습니까?")) {
		window.history.back();
	}
});
</script>