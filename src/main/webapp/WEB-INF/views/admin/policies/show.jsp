<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container">
	<h3>이용약관 / 개인정보 취급방침 관리</h3>
	
	<div class="row margin-bottom30">
		<div class="col-md-12">
			<table class="table table-bordered">
				<tbody>
					<tr>
						<th>종류</th>
						<td><c:choose>
							<c:when test="${policy.type eq 'policy' }">
								이용약관
							</c:when>
							<c:otherwise>
								개인정보 취급방침
							</c:otherwise>
						</c:choose>
						</td>
					</tr>
					<tr>
						<th>작성일</th>
						<td><fmt:formatDate value="${policy.regDttm}" pattern="yyyy/MM/dd hh:mm:ss"/></td>
					</tr>
					<tr>
						<th>최종수정일</th>
						<td><fmt:formatDate value="${policy.updtDttm}" pattern="yyyy/MM/dd hh:mm:ss"/></td>
					</tr>
					<tr>
						<th>내용</th>
						<td class="inner-table-wrapper">
							<c:out value="${policy.content }" escapeXml="false" />
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="margin-bottom30">
		<a href="<c:url value="/admin/policies"/>" class="btn btn-primary">
			목록으로
		</a>
	</div>
</div>

<script type="text/javascript">
	$(function(e) {
		$('.inner-table-wrapper table').addClass("table").addClass("table-bordered")
			.addClass('centered-table');
	});
</script>