<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container">
	<h3>이용약관 / 개인정보 취급방침 관리</h3>
	<div class="panel panel-primary">
		<div class="panel-heading">
			가장 최근에 생성 또는 수정된 약관/방침이 실제 페이지에 보이게 됩니다.
		</div>
  	</div>
	<div class="row margin-bottom30">
		<div class="col-md-12">
			<table class="table table-striped margin-bottom30">
				<thead>
					<tr>
						<th>종류</th>
						<th>최초 작성일</th>
						<th>최근 수정일</th>
						<th>페이지 적용 유무</th>
						<th></th>
					</tr>
				</thead>
				
				<c:forEach var="policy" items="${policies }">
					<tr>
						<td>
							<c:choose>
								<c:when test="${policy.type eq 'policy' }">
									이용약관		
								</c:when>
								<c:when test="${policy.type eq 'media' }">
									영상정보처리기기 방침		
								</c:when>
								<c:otherwise>
									개인정보 취급방침
								</c:otherwise>
							</c:choose>
						</td>						
						<td>
							
							<fmt:formatDate value="${policy.regDttm}" pattern="yyyy/MM/dd hh:mm:ss"/>
						</td>
						<td>
							<fmt:formatDate value="${policy.updtDttm}" pattern="yyyy/MM/dd hh:mm:ss"/>
						</td>	
						<td>
							<c:choose>
								<c:when test="${policy.mostRecent eq true }">
									Y
								</c:when>
								<c:otherwise>
									N
								</c:otherwise>
							</c:choose>
						</td>
						<td>
							<c:url value="/admin/policies/${policy.id}/delete" var="deletePolicyUrl" />
							<form:form  action="${deletePolicyUrl}" method="DELETE" data-id="${policy.id }">
								<a href="<c:url value='/admin/policies/${policy.id}' />"
									class="btn btn-primary btn-sm">
									보기
								</a>
								<a href="<c:url value='/admin/policies/${policy.id}/edit' />" 
									class="btn btn-warning btn-sm">
									수정
								</a>
								<button type="submit" class="delete-btn btn btn-danger btn-sm" data-id="${policy.id }">삭제</button>
							</form:form>
						</td>
					</tr>
				</c:forEach>
			</table>
			
			<div class="text-right">
				<a href="<c:url value='/admin/policies/new'/>" class="btn btn-primary">
					등록하기
				</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(e) {
		
		$('.delete-btn').on('click', function(e) {
				
			e.preventDefault();	
			var id = $(this).data('id');
			console.log(id);
			if(confirm("해당 약관/방침을 정말 삭제 하시겠습니까?")) {
				$("form[data-id='" + id + "']").submit();
			}
		});
	});
</script>