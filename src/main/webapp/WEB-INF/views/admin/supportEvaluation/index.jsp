<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<div class="container">
	<h3>고객서비스 평가 관리</h3>
	
	<div class="row margin-bottom30">
		<div class="col-md-12">
			<table class="table table-striped margin-bottom30">
				<thead>
					<tr>
						<th>연도</th>
						<th>기간</th>
						<th>평가 점수</th>
						<th></th>
					</tr>
				</thead>
				
				<c:forEach var="evaluation" items="${supportEvaluations }">
					<tr>
						<td>
							<c:out value='${evaluation.year }' />
							<c:out value='${evaluation.seq }' /> 
						</td>							
					
						<td>${evaluation.duration }</td>
						<td>${evaluation.totalScore }</td>
						<td>
							<a href="<c:url value='/admin/supportEvaluation/${evaluation.id}' />"
								class="btn btn-primary btn-sm">
								보기
							</a>
							<a href="<c:url value='/admin/supportEvaluation/${evaluation.id}/edit' />" 
								class="btn btn-warning btn-sm">
								수정
							</a>
							<c:url value="/admin/supportEvaluation/${evaluation.id }" var="deleteEvalUrl"/>
							<form:form action="${deleteEvalUrl }" method="DELETE" data-id="${evaluation.id }" class="delete-form-in-table">
								<button type="submit" class="delete-btn btn btn-danger btn-sm">
									삭제
								</button>
							</form:form>
						</td>
					</tr>
				</c:forEach>
			</table>
			
			<div class="right">
				<a href="<c:url value='/admin/supportEvaluation/new'/>" class="btn btn-primary">
					신규 평가 등록하기
				</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(e) {
		
		$('.delete-btn').on('click', function(e) {
				
			var id = $(this).data('id');
			if(confirm("해당 평가를 정말 삭제 하시겠습니까?")) {
				$("form[data-id='" + id + "']").submit();
			}
			else {
				e.preventDefault();	
			}
		});
	});
</script>