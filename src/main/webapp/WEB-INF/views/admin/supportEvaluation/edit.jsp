<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="container">
	<h3>고객서비스 평가 수정</h3>
	<div class="row margin-bottom30">
		<c:set var="evaluation" value="${supportEvaluation }" />
		<c:url var="updateEvalUrl" value="/admin/supportEvaluation/${evaluation.id }"/>
		<form:form id="updateEvalForm" action="${updateEvalUrl }" method="PUT" 
			commandName="supportEvaluation" modelAttribute="supportEvaluation">
			<div class="col-md-4">
				<div class="form-group">
					<form:label path="year">연도</form:label>
					<form:input path="year" name="year" required="true" class="form-control"/>  
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<form:label path="seq">분기</form:label>
				<form:select path="seq" class="form-control">
					<option value="" <c:if test="${ empty seq }">selected</c:if>>전체</option>
					<option value="상반기" <c:if test="${supportEvaluation.seq eq '상반기' }">selected</c:if>>상반기</option>
					<option value="하반기" <c:if test="${supportEvaluation.seq eq '하반기' }">selected</c:if>>하반기</option>
				</form:select>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<form:label path="duration">평가대상기간</form:label>
					<form:input path="duration" name="duration" required="true" class="form-control"/>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<form:label path="evaluator">평가자</form:label>
					<form:input path="evaluator" name="evaluator" required="true" class="form-control"/>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<form:label path="totalScore">평가점수</form:label>
					<form:input path="totalScore" name="totalScore" required="true" class="form-control"/>
				</div>
			</div>
			<div class="col-md-12 margin-bottom20 text-area-lg">
				<div class="form-group">
					<form:label path="htmlData">평가테이블</form:label>
					<form:textarea path="htmlData" required="true" class="ckeditor" id="editor1" rows="10" cols="80"/>					
				</div>
			</div>    
			<div class="col-md-12">
				<div class="form-group text-right">
					<a href="<c:url value='/admin/supportEvaluation'/>" class="btn btn-danger">
						취소
					</a>  
					<button type="submit" class="btn btn-primary">수정</button>				
				</div>
			</div>
		</form:form>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		CKEDITOR.replace("editor1",
			{
			     height: 550
			}
		);
  	});
	
</script>