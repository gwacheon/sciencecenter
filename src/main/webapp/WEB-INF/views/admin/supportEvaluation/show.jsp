<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div id="support-body">
	<div class="container">
		<c:set var="evaluation" value="${supportEvaluation }" />
		<div class="section">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title orange">
						<div class="top-line"></div>      
						<c:out value="${evaluation.year }" />
						<c:out value="${evaluation.seq }" /> 
						평가 결과
					</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div>
						우리 과학관의 모든 임직원들은, 대한민국의 모든 국민이 즐겁게 과학문화 생활을 누릴 수 있도록
						최상의 서비스를 제공할 것을 약속드립니다.  
					</div>
				</div>
			</div>
			<div class="sub-section">
				<ul class="circle-list orange sub-section-title">
					<li>
						<c:out value="${evaluation.year }" />
						<c:out value="${evaluation.seq }" />
					 	국립과천과학과 고객서비스헌장의 서비스 실천 이행에 관한 평가결과를 알려드립니다.
					</li>
				</ul>
				<div class="content">
					<div class="row">
						<div class="col-md-12 content-title-wrapper">
							<ul class="arrow-list orange content-title">
								<li>
									평가대상기간
									<ul>
										<li>
											<c:out value="${evaluation.duration }" />
										
										</li>
									</ul>
								</li>
								<li>
									평가자
									<ul>
										<li>
											<c:out value="${evaluation.evaluator }" />
										</li>
									</ul>			
								</li>		
								<li>
									평가점수
									<ul>
										<li>
											<c:out value="${evaluation.totalScore }" />
										</li>
									</ul>			
								</li>						
							</ul>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 eval-table" >
							<c:out value="${evaluation.htmlData }" escapeXml="false" />
						</div>
						<div class="col-md-12">
							<div class="table-extra-info">
								점수환산기준 : 매우우수(100점), 우수(90점), 보통(80점), 미흡(70점), 불량(60점)
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row margin-bottom30">
			<div class="col-md-12">
				<a href="<c:url value="/admin/supportEvaluation" />" class="btn btn-primary">
					목록으로
				</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('table').addClass('table').addClass('table-bordered').addClass('centered-table');
</script>