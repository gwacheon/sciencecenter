<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="container">
	<h3>고객서비스 평가 등록</h3>
	
	<div class="row margin-bottom30">
		<c:url var="createEvalUrl" value="/admin/supportEvaluation/create"/>
		<form:form id="newEvalForm" action="${createEvalUrl }" method="POST" 
			commandName="supportEvaluation" modelAttribute="supportEvaluation">
			<div class="col-md-4">
				<div class="form-group">
					<form:label path="year" for="year">연도</form:label>				
					<form:input path="year" name="year" required="true" placeholder="2015년" class="form-control"/>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<form:label path="seq">분기</form:label>
					<form:select path="seq" name="seq" class="form-control">
						<option value="" selected>전체</option>
						<option value="상반기">상반기</option>
						<option value="하반기">하반기</option>
					</form:select>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<form:label path="duration">평가대상기간</form:label>
					<form:input path="duration" name="duration" required="true" placeholder="2015.1.1 ~ 2015.12.31" class="form-control"/>
				</div>
			</div>
			<div class="col-md-9">
				<div class="form-group">
					<form:label path="evaluator">평가자</form:label>
					<form:input path="evaluator" name="evaluator" required="true" placeholder="국립과천과학관 고객서비스향상자문위원" class="form-control"/>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<form:label path="totalScore">평가점수</form:label>
					<form:input path="totalScore" name="totalScore" required="true" placeholder="100점" class="form-control"/>
				</div>
			</div>  
			<div class="col-md-12">
				<div class="form-group">
					<form:label path="htmlData" class="active">평가테이블</form:label>
					<form:textarea path="htmlData" required="true" class="ckeditor" id="editor1" rows="10" cols="80"/>					
				</div>
			</div>    
			<div class="col-md-12">
				<div class="form-group text-right">
					<a href="<c:url value='/admin/supportEvaluation'/>" class="btn btn-danger">
						취소
					</a>  					
					<button type="submit" class="btn btn-primary">등록</button>				
				</div>
			</div>
		</form:form>
	</div>
</div>


<script id="eval-table-template" type="text/x-handlerbars-template">
		<table class="table table-bordered centered-table responsive-table">
			<thead>
				<tr>
					<th rowspan="2">구분</th>
					<th rowspan="2">세부평가항목</th>
					<th colspan="5">평가결과</th>
					<th rowspan="2">항목별 평균</th>
					<th rowspan="2" class="no-right-border">비고</th>
				</tr>
				<tr>
					<th>매우우수</th>
					<th>우수</th>
					<th>보통</th>
					<th>미흡</th>
					<th>불량</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td rowspan="9">접근성</td>
					<td style="text-align: left;">
						&#9312;
						과학관 진입 안내및 접근 편의성</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td style="text-align: left;">
						&#9313;
						사무실입구에 직원배치도 부착 관리 실태
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>	
				<tr>
					<td style="text-align: left;">
						&#9314;
						주요시설에 대한 운영 관리자 성명 부착여부
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>		
				<tr>
					<td style="text-align: left;">
						&#9315;
						과천과학관 홈페이지의 접속 편의성
						</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td style="text-align: left;">
						&#9316;
						전화문의시 담당직원(팀원)과의 연결 편의성
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td style="text-align: left;">
						&#9317;
						과천과학관 시설물 등의 청결도
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>		
				<tr>
					<td style="text-align: left;">
						&#9318;
						관람의 편의성
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>		
				<tr>
					<td style="text-align: left;">
						&#9319;
						직원의 신분증 패용여부
					</td>  
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>		
				<tr>
					<td>소계</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>			
				<tr>
					<td rowspan="4">친절성</td>
					<td style="text-align: left;">
						&#9312;
						전화문의시 담당직원의 통화태도 및 안내의 적극성
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td style="text-align: left;">
						&#9313;
						전시관 직원의 관람안내 태도 및 안내의 적극성
					</td>  
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td style="text-align: left;">
						&#9314;
						사무실 방문시 직원의 응대태도(신속성 등)
					</td>  
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>
						
						소계
					</td>  
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td rowspan="3">환류성</td>
					<td style="text-align: left;">
						&#9312;
						이행표준 평가결과에 대한 조치 실태
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td style="text-align: left;">
						&#9313;
						의견제출, 신고 등에 대한 후속조치 실태
					</td>  
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>		
						소계
					</td>  
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="2">총 계(평균)</td>
					<td colspan="7"></td>
				</tr>
			</tbody>
		</table> 
</script> 


<script type="text/javascript">
	$(document).ready(function() {
    	// set table template inside ckeditor
		var source = $("#eval-table-template").html();
    	$('#editor1').html(source);
    	
    	CKEDITOR.replace("editor1",
   			{
   			     height: 550
   			}
   		);
  	});

</script>