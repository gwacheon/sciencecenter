<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container">
	<h5>휴무일 관리</h5>
	
	<div class="row margin-bottom30">
		<c:url var = "action" value='/admin/holidays'/>
		<form:form id="newHolidayForm" action="${action }"
			class="col s12" 
			method="POST" 
			commandName="holiday" 
			modelAttribute="holiday">
			
			<div class="row">
				<div class="input-field col s4">
					<form:input type="date" class="datepicker" path="holiday" required="true" />
					<form:label path="holiday">날짜</form:label>
				</div>
				
				<div class="input-field col s4">
					<form:select path="timeType">
						<c:forEach var="type" items="${timeTypes}">
						   <option value="${type.value}">${type}</option>
						</c:forEach>
					</form:select>
					<form:label path="timeType">시간대</form:label>
				</div>
			
				<div class="input-field col s4">
					<form:input path="name" required="true" class="validate"/>
					<form:label path="name">휴무명</form:label>
				</div>
				
				<div class="col s12">
					<button type="submit" class="right btn btn-primary">추가</button>
				</div>
			</div>
		</form:form>
	</div>
	
	<h5>휴무일 리스트</h5>
	
	<div class="row">
		<c:forEach var="holiday" items="${holidays }">
			<div class="col m4 l3">
				<div class="card holiday-item">
					<c:set var="holydayTimeType" value="${holiday.timeType }"/>
					<div class="date"><fmt:formatDate pattern="yyyy-MM-dd" value="${holiday.holiday}" /></div>
					<div>
						<spring:message code="holyday.timetype.type${holydayTimeType }" />
					</div>
					<div class="name">${holiday.name }</div>
					<div>
						<c:url var="deleteUrl" value="/admin/holidays/${holiday.id }" />
						<form:form action="${deleteUrl }" method="delete" class="delete-form"  data-id="${holiday.id }">
					        <button type="button" data-id="${holiday.id }" class="delete-btn waves-effect waves-light btn red right">삭제</button>
						</form:form>
						
					</div>
				</div>
			</div>
		</c:forEach>
		<div class="col s12 center">
			<c:import url="pagination.jsp"></c:import>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$('select').material_select();
		$('.datepicker').pickadate({
			selectMonths : true, // Creates a dropdown to control month
			selectYears: 3, // Creates a dropdown of 15 years to control year
			format: 'yyyy-mm-dd'
		});
		
		$(".delete-btn").click(function(e){
			var id = $(this).data("id");
			
			if(confirm("선택하신 휴무일을 삭제하시겠습니까?")){
				$(".delete-form[data-id='" + id + "']").submit();	
			}
		});
	});
</script>