<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container">
	<div class="row">
		<div class="col s12">
			<h4>회원 관리</h4>
		</div>
	</div>
	<div class="row">
		<div class="col s12">
			<c:url value="/admin/users/excelDownload" var="userExcelUrl" />
			<a href="${userExcelUrl }" class="waves-effect waves-light btn">
				회원 리스트 엑셀 다운로드
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col s12">
			<table>
				<thead>
					<tr>
						<td>
							id
						</td>
						<td>
							이름
						</td>
						<td> 
							이메일
						</td>
						<td> 
							전화번호
						</td>
						<td>
							최종 로그인
						</td>
						<td>
							회원 구분
						</td>
						<td>
						</td>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${users }" var="user">
						<tr>
							<td>
								<c:out value="${user.id}"></c:out>
							</td>
							<td>
								<c:out value="${user.name}"></c:out>
							</td>
							<td>
								<c:out value="${user.email}"></c:out>
							</td>
							<td>
								<c:out value="${user.phone}"></c:out>
							</td>
							<td>
								<fmt:formatDate pattern="yyyy-MM-dd" value="${user.last_sign_in_at}" />
								<c:out value="${user.last_sign_in_at}"></c:out>
							</td>
							<td>
								<c:choose>
									<c:when test="${user.chargedMember == 0}">
										일반회원
									</c:when>
									<c:otherwise>
										연간회원
									</c:otherwise>
								</c:choose>
							</td>
							<td>
								<c:if test="${fn:length(user.userRoles) == 0}">
									<c:url value="/admin/users/${user.id}" var="deleteUserUrl" />
									<form:form  action="${deleteUserUrl}" method="DELETE" data-id="${user.id }">
										<button type="button" data-id="${user.id}" class="delete-btn waves-effect waves-light btn red small user-delete-btn">
											삭제
										</button>
									</form:form>
								</c:if> 
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
  	</div>
</div>

<script type="text/javascript">
	$(function(){
		$('.user-delete-btn').click(function(e){
			if(confirm('정말 해당 회원을 삭제 하시겠습니까 ? ')){
				$('form[data-id = '+this.data('id')+']').submit();	
			}
		})
	})
</script>