<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>

<h3>대표&amp;기타문화행사 관리</h3>
<ul class="list-group">
	<li class="list-group-item <c:if test="${mainEventType.type == 'major'}">active</c:if>">
		<a href='<c:url value='/admin/mainEvents' />'>
			대표문화행사
		</a>
	</li>
	<li class="list-group-item <c:if test="${mainEventType.type == 'minor'}">active</c:if>">
		<a href='<c:url value='/admin/mainEvents?type=minor' />' class="collection-item">
			기타문화행사
		</a>
	</li>
</ul>
