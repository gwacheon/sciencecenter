<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<style>
	.panel-heading .title {
		font-size: 19px;
	}
	.btn.btn-sm {
		padding: 5px 10px; 
	}
</style>

<div id="admin-board-index" class="container">
	<div class="row margin-bottom30">
		<%-- <div class="col-xs-12 col-sm-4 col-md-3">
			<c:import url="/WEB-INF/views/admin/mainEvents/side.jsp" />
		</div> --%>
		<div class="col-xs-12">
			<h3><c:out value='${mainEventType.name }' /></h3>
			<div class="text-right margin-bottom20">
				<button class="btn btn-primary" data-toggle="modal"
					data-target="#create-main-event-modal"> 
					<c:out value='${mainEventType.name }' /> 카테고리 추가 
				</button>
			</div> 
			<ul class="list-group">
				<c:forEach items="${mainEvents }" var="item">
					<div class="panel panel-default">						
						<div class="panel-heading">
							<!-- 대표문화행사 상위 카테고리 수정 Modal -->
							<div id="edit-main-event-modal-${item.id }" class="modal fade">
								<form id="main-event-form-${item.id }"
									action="<c:url value='/admin/mainEvents/update'/>"
									method="PUT">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" 
													aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<h4 class="modal-title">정기 행사 명칭 수정</h4>
											</div>
											<div class="modal-body">
													<!-- id 적용 -->											
													<input type="hidden" name="id" value="${item.id }">												
													<!-- value 적용 -->
													<div class="form-group">
														<label for="first_name">이름</label>
														<input name="name" type="text" value="${item.name}" class="form-control">
													</div>
													<c:if test="${item.subType eq 'link'}">
														<div class="form-group">
															<label for="linkUrl">링크주소</label>
															<input type="text" name="linkUrl" value="${item.linkUrl }" class="form-control"/>
														</div>
													</c:if>
													<input type="hidden" name="mainEventType"  value="${item.mainEventType}">
													<input type="hidden" name="subType"  value="${item.subType}">
													<input type="hidden" name="seq"  value="${item.seq}">
											</div>
											<div class="modal-footer">
												<button class="btn btn-warning" data-dismiss="modal">
													취소
												</button>
												<button class="btn btn-primary" type="submit" data-id="${item.id }">
													수정
												</button>
											</div>										
										</div>
									</div>
								</form>			
							</div>
							<div class="row">
								<div class="col-md-6">
									<span class="title"><c:out value="${item.name}"></c:out></span>
									<c:if test="${item.subType eq 'link' }">
										<span class="link"><a href="${item.linkUrl }" class="btn btn-primary btn-sm" target="_blank">링크 바로가기</a></span>
									</c:if>	
								</div>
								<div class="col-md-6 text-right">
									
									<c:if test="${item.subType eq 'series' }">
										<a href='<c:url value="/admin/mainEventSeries/new/${item.id }"></c:url>'
											class="btn btn-primary btn-sm"> 
											차수 추가
										</a>
									</c:if>
									<a href="#edit-main-event-modal-${item.id }"
										class="btn btn-warning btn-sm main-event-edit-btn" data-toggle="modal"
										data-target="#edit-main-event-modal-${item.id }" data-id="main-event-${item.id}">
										카테고리 수정
									</a>
									<a class="main-event-delete-btn btn btn-danger btn-sm" data-id="main-event-${item.id }"> 카테고리 삭제</a>
									<c:url value="/admin/mainEvents/${item.id}" var="deleteMainEventUrl" />
									<form:form  action="${deleteMainEventUrl}" method="DELETE" data-id="main-event-${item.id }">
									</form:form>
								</div>
							</div>
						</div>
						<ul class="list-group inner">
							<c:forEach items="${item.series }" var="series">	
								<li class="list-group-item inline">
									<a href='<c:url value="/admin/mainEventSeries/${series.id }"/>' 
										class="link">
										${series.name }
									</a> 
									<a data-id="series-${series.id }" href="#!" class="delete-event-series-btn secondary-content btn btn-danger btn-sm">
										삭제
									</a> 
									<c:url value="/admin/mainEventSeries/${series.id}" var="deleteEventSeriesUrl" />
									<form:form  action="${deleteEventSeriesUrl}" method="DELETE" data-id="series-${series.id }">
									</form:form>
								</li>
							</c:forEach>
						</ul>				
					</div>
				</c:forEach>
			</ul>
			<div class="text-right">
				<button class="btn btn-primary edit-main-event-order-btn">
					<c:out value='${mainEventType.name }' /> 순서 변경 
				</button>
			</div>
		</div>
	</div>
</div>


<div id="create-main-event-modal" class="modal fade">
	<form:form id="newEventForm" action="${param.action }"
		method="POST" commandName="mainEvent" modelAttribute="mainEvent">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" 
						aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><c:out value='${mainEventType.name }'/> 카테고리 추가</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<input type="hidden" name="seq" value="0">
						<c:choose>
							<c:when test="${mainEventType.type eq 'major'}">
								<input type="hidden" name="mainEventType" value="major" />
							</c:when>
							<c:otherwise>
								<input type="hidden" name="mainEventType" value="minor" />
							</c:otherwise>
						</c:choose>
						<form:label path="name">이름</form:label>
						<form:input type="text" path="name" class="form-control"/>
						<form:label path="subType">카테고리 유형</form:label>
						<form:select path="subType" name="subType" class="form-control" id="subTypeSelect">
							<option value="series">일반</option>
							<option value="link">링크</option>
						</form:select>
						<div id="linkUrlBox">
							<form:label path="linkUrl">링크주소</form:label>
							<form:input type="text" path="linkUrl" class="form-control"/>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button data-dismiss="modal"
						class="btn">닫기</button>
					<button type="submit" class="btn btn-primary">추가</button>
		
				</div>
			</div>
		</div>
	</form:form>
</div>


<div id="edit-main-event-order-modal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title"><c:out value='${mainEventType.name }'/> 순서 변경</h4>
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<table id="main-event-item-table" class="table table-striped">
								<thead>
									<tr>
										<th>제목</th>
									</tr>
								</thead>
								
								<tbody id="sortable-main-event-items">
								
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="close">취소</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script id="mainEventEditOrderTemplate" type="text/x-handlebars-template">
{{#each mainPageItems}}
<tr class="selector" data-id="{{id}}">
	<td>
		{{name}}
	</td>
</tr>
{{/each}}
</script>

<script type="text/javascript">
	var source   = $("#mainEventEditOrderTemplate").html();
	var template = Handlebars.compile(source);
	
	var type = "<c:out value='${mainEventType.type}'/>";
	$(document).on('click', '.edit-main-event-order-btn', function(e){
		if( type == null ) {
			type = major;
		}
		
		$.getJSON(baseUrl + "admin/mainEvents/editSequence/" + type, function(data){
			var items = [];
			
			if(data.mainEvents != null && data.mainEvents.length > 0) {
				_.each(data.mainEvents, function(d, i){
					items.push(d);
				});
				
				var html = template({mainPageItems: items});
				$("#main-event-item-table > tbody").html(html);
			}
			$("#edit-main-event-order-modal").modal();
		});
	});	
	
	$(function(){
		
		$("#sortable-main-event-items").sortable({
			stop: function(e,ui){
				var ids = [];

				$("#sortable-main-event-items tr").each(function(i){
					ids.push($(this).attr('data-id'));
				})
				$.ajax({
					url: baseUrl + "admin/mainEvents/sortable",
					contentType : "application/json; charset=utf-8",
					beforeSend: function(xhr) {
						ajaxLoading();
						xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));
					},
					data: JSON.stringify({
						data : ids
					}),
					type: "POST",
					dataType: "JSON",
					success: function(data) {
						ajaxUnLoading();
						alert("순서가 변경되었습니다.");
						//window.location = baseUrl + "admin/mainEvents?type=" + type;
					},
					error: function(err) {
						ajaxUnLoading();
						alert(err);
					}
				})
			}
		});
	});
	
	$('#edit-main-event-order-modal').on('hide.bs.modal', function(e) {
		location.reload();
	});
	
	

</script>

<script type="text/javascript">

	// main event delete process
	$('.main-event-delete-btn').click( function(e) {
		e.preventDefault();
		console.log("clicked");
		if( confirm("해당 카테고리를 삭제하면, 속하는 모든 차수 및 게시판이 삭제됩니다. 정말로 삭제하시겠습니까?")) {
			var id = $(this).data('id');
			console.log(id);
			$("form[data-id='" + id + "']").submit();	
		}
	});
	
	// main event series delete process
	$('.delete-event-series-btn').click( function(e) {
		e.preventDefault();
		
		if( confirm("해당 차수를 삭제하면, 포함된 모든 게시판이 삭제됩니다. 정말로 삭제하시겠습니까?")) {
			var id = $(this).data('id');
			console.log(id);
			$("form[data-id='" + id + "']")[0].submit();	
		}
	});
	
</script>

<script type="text/javascript">
	$( function() {
		$("#linkUrlBox").css("display", "none");	
	});
	
	$('#subTypeSelect').on("change", function() {
		var subType = $(this).val();
		if( subType == "series") {
			$("#linkUrlBox").css("display", "none");
		}
		else {
			$("#linkUrlBox").css("display", "block");
		}
	});
</script>
