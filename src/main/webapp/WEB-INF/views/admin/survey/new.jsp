<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container">
	<h4 class="page-title">설문조사 등록</h4>
	
	<div class="row margin-bottom30">
		<c:url var = "action" value='/admin/surveys'/>
		<c:url var = "method" value='POST'/>
		
		<jsp:include page="/WEB-INF/views/admin/survey/form.jsp">
			<jsp:param name="action" value="${action}"/>
			<jsp:param name="method" value="${method}"/>
		</jsp:include>
	</div>
</div>