<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container margin-bottom30">
	<h4 class="page-title">
		[${survey.title}] 설문항목 관리
		(<fmt:formatDate value="${survey.beginTime}" pattern="yyyy-MM-dd"/>
		~
		<fmt:formatDate value="${survey.endTime}" pattern="yyyy-MM-dd"/>)
	</h4>
	<div id="articles">
		<c:forEach var="article" items="${survey.articles }">
			<div class="row article-row" data-id="${article.id }">
				<div class="col-sm-12">
					<div class="article-item" data-id="${article.id }">
						<div class="article-title">
							${article.question }
							<a href="#" class="right delete-article-btn" data-id="${article.id }">
								삭제
							</a>
						</div>
						<c:choose>
							<c:when test="${article.qType == 'text' }">
								<div class="">
									<textarea rows="5" cols="" class="form-control reply-form"
										name="${article.id }"
										data-article-id="${article.id }"></textarea>
								</div>
							</c:when>
							<c:when test="${article.qType == 'select' }">
								<div class="row">
									<div class="col-sm-12">
										<c:forEach var="selection" items="${article.selections }">
											<c:choose>
												<c:when test="${selection.directInput}">
													<div class="row clearfix">
														<div class="col-sm-3 col-md-2">
															<p>
																<label for="selection-${selection.id }">
									      							<input name="${article.id }" type="radio" id="selection-${selection.id }"
									      								class="reply-form etc-radio" data-article-id="${article.id }"
									      								data-id="${selection.id }"
									      								value="${selection.example }"/> ${selection.example }
							      								</label>
								    						</p>
														</div>
														
														<div class="col-sm-9 col-md-10">
															<input type="text" class="etc-input-field form-control" data-id="${selection.id }" data-article-id="${article.id }">
														</div>
													</div>
												</c:when>
												<c:otherwise>
													<p class="<c:if test="${article.renderType == 'horizontal' }">floated-selection</c:if>">
														<label for="selection-${selection.id }">
							      							<input name="${article.id }" type="radio" id="selection-${selection.id }"
							      								class="reply-form" data-article-id="${article.id }"
							      								value="${selection.example }"/> ${selection.example }
					      								</label>
						    						</p>	
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</div>
								</div>
							</c:when>
							<c:when test="${article.qType == 'multiple_select' }">
								<div class="row">
									<div class="col-sm-12">
										<c:forEach var="selection" items="${article.selections }">
											<p class="<c:if test="${article.renderType == 'horizontal' }">floated-selection</c:if>">
												<label for="selection-${selection.id }">
					      							<input name="${article.id }" type="checkbox" id="selection-${selection.id }"
					      								class="reply-form" data-article-id="${article.id }" 
					      								value="${selection.example }"/> ${selection.example }
			      								</label>
				    						</p>
										</c:forEach>
									</div>
								</div>
							</c:when>
						</c:choose>
					</div>
				</div>
			</div>
		</c:forEach>
	</div>
	
	<div class="row margin-bottom30">
		<div class="col-sm-12">
			<button type="button" class="add-article-btn right btn btn-primary">설문추가</button>
		</div>
	</div>
</div>

<div id="question-modal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="question-detail-form">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	       			<h4 class="modal-title">설문 문항 등록</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="input-field col-sm-12">
							<div class="form-group">
								<label for="first_name">질문</label>
								<input type="text" name="question" id="article_question" class="form-control">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="input-field col-sm-12">
							<div class="form-group">
								<label for="disabled">질문타입</label>
								<select id="article_q_type" name="article[q_type]" class="form-control">
									<option value="text">서술형</option>
									<option value="select">선택형(단일선택)</option>
									<option value="multiple_select">선택형(중복가능)</option>
									<option value="line">절취선</option>
								</select> 
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<div>
									<label for="article_render_type_vertical">	
										<input name="article[render_type]" type="radio"
											id="article_render_type_vertical" 
											value="vertical" checked /> 세로형
									</label> 
								</div>
								
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<div>
									<label for="article_render_type_horizontal">	
										<input name="article[render_type]" type="radio"
											id="article_render_type_horizontal" value="horizontal" />  가로형
									</label> 
								</div>
							</div>
						</div>
					</div>

					<div id="question-selectors" class="row"></div>

					<div id="selector-add-btn-row" class="row margin-top20" style="display: none">
						<div class="col-sm-12">
							<a id="selector-add-btn" href="#!"
								class="btn btn-white"> 추가 </a>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">취소</button>
					<button id="submit-question-btn" type="submit" class="btn btn-primary">저장</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function() {
		$(".add-article-btn").click(function(e) {
			$("#question-modal").modal();
		});

		$("#article_q_type").on("change", function() {
			if ($(this).val() == "select" || $(this).val() == "multiple_select") {
				var question = $("#questionTemplate").html();
				var questionTemplate = Handlebars.compile(question);
				var questionHtml = questionTemplate({
					seq: $("#question-selectors .selection-input").length
				});
				$('#question-selectors').html(questionHtml);
				$('#selector-add-btn-row').show();
			} else {
				$('#selector-add-btn-row').hide();
				$('#question-selectors').html('');
			}
		});

		$("#selector-add-btn").on("click", function(e) {
			e.preventDefault();

			var question = $("#questionTemplate").html();
			var questionTemplate = Handlebars.compile(question);
			var questionHtml = questionTemplate({
				seq: $("#question-selectors .selection-input").length
			});
			$('#question-selectors').append(questionHtml);
		});
		
		$("#submit-question-btn").click(function(e){
			e.preventDefault();
			
			var obj = {};
			obj.article = {};
			obj.article.survey_id = "${survey.id}";
			obj.article.title = $("#article_question").val();
			obj.article.q_type = $("#article_q_type").val();
			obj.article.render_type = $("input[name='article[render_type]']:checked").val();
			
			if($("#article_q_type").val() != "text"){
				obj.selections = [];
				
				_.forEach($("#question-selectors .selection-row"), function(n, key){
					if($(n).find(".selection-input").val() != null && $(n).find(".selection-input").val() != ""){
						obj.selections.push({
								seq: key, 
								example: $(n).find(".selection-input").val(),
								direct_input: $(n).find(".direct-input-checkbox").is(":checked")
							}
						);						
					}
				});
			}
			
			$.ajax({
				type : "POST",
				beforeSend: function(xhr) {
		            xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));
		        },
				url : baseUrl + "admin/surveys/${survey.id}/addArticles",
				data : JSON.stringify(obj),
				contentType : 'application/json',
				success : function(data) {
					var article = $("#articleTemplate").html();
					var articleTemplate = Handlebars.compile(article);
					var articleHtml = articleTemplate(data);
					$('#articles').append(articleHtml);
					
					$('#selector-add-btn-row').hide();
					$('#question-selectors').html('');
					$("#question-modal").modal('hide');
					
					$("#articles").sortable({
						stop: function( event, ui ) {
							sortArticles();
						}	
					});
				},
				error: function(err){
					alert("문제가 발생하였습니다.");
				}
			});
		});
		
		$(document).on("click", ".delete-article-btn", function(e){
			e.preventDefault();
			
			var articleId = $(this).data("id");
			if(confirm("선택하신 항목을 삭제하시겠습니까?")){
				$.ajax({
					url: baseUrl + "admin/surveys/deleteArticle/" + articleId,
					beforeSend: function(xhr) {
			            xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));
			        },
			        type: "DELETE",
			        dataType: "JSON",
			        success: function(data){
			        	$(".article-row[data-id='" + articleId + "']").remove();
			        },
			        error: function(err){
			        	console.log(err);
			        }
				});
			}
		});
		
		$(document).on("click", ".etc-radio", function(e){
			var id = $(this).data("id");
			$(".etc-input-field[data-id='" + id + "']").focus();
			console.log(e);
		});
		
		$(document).on("blur", ".etc-input-field", function(e){
			var id = $(this).data("id");
			$(".etc-radio[data-id='" + id + "']").val($(this).val());
		});
		
		$("#articles").sortable({
			stop: function( event, ui ) {
				sortArticles();
			}	
		});
		
		function sortArticles(){
			var sortedIds = [];
			_.forEach($("#articles > .article-row"), function(n, key){
				sortedIds.push($(n).data("id"));
			});
			
			$.ajax({
				url: baseUrl + "admin/surveys/${survey.id}/articles/sort",
				beforeSend: function(xhr) {
		            xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));
		        },
		        data : JSON.stringify({ids: sortedIds}),
				contentType : 'application/json',
		        type: "POST",
		        dataType: "JSON",
		        success: function(data){
		        	alert("정상적으로 수정되었습니다.");
		        },
		        error: function(err){
		        	console.log(err);
		        }
			});
		}
	});	
</script>

<script id="questionTemplate" type="text/x-handlebars-template">
<div class="selection-row form-inline margin-bottom10">
	<div class="col-sm-9">
		<div class="form-group">
			<label for="">항목</label>
			<input name="selector[]" type="text" class="form-control selection-input" class="selectors">
		</div>

		<div class="form-group">
			<label for="direct_input_{{seq}}">
				<input type="checkbox" name="direct_input[]" id="direct_input_{{seq}}" 
					class="direct-input-checkbox"/> 직접입력
			</label>
		</div>
	</div>
</div>
</script>

<script id="articleTemplate" type="text/x-handlebars-template">
<div class="row article-row" data-id="{{article.id }}">
	<div class="col-sm-12">
		<div class="article-item" data-id="{{article.id }}">
			<div class="article-title">
				{{article.question }}
				<a href="#" class="right delete-article-btn" data-id="{{article.id }}">
					삭제
				</a>
			</div>

			{{#compare article.qType 'text' operator="=="}}
				<div class="">
					<textarea rows="5" cols="" class="form-control reply-form"
						name="{{article.id }}"
						data-article-id="{{article.id }}"></textarea>
				</div>
			{{/compare}}

			{{#compare article.qType 'select' operator="=="}}
				<div class="row">
					<div class="col-sm-12">
						{{#each article.selections}}
							{{#if this.directInput}}
								<div class="row clearfix">
									<div class="col-sm-3 col-md-2">
										<div class="form-group">
											<div class="radio">
												<label for="selection-{{this.id}}">
													<input name="{{../../article.id }}" type="radio" id="selection-{{this.id}}"
														class="reply-form etc-radio" data-article-id="{{../../article.id}}"
			    										data-id="{{this.id}}"
														value="{{this.example}}"/> {{this.example}}
												</label>
											</div>
										</div>
									</div>
									
									<div class="col-sm-9 col-md-10">
										<input type="text" class="etc-input-field form-control" data-id="{{this.id}}" data-article-id="{{../../article.id}}">
									</div>
								</div>
							{{else}}
								<p class="{{#compare ../../article.renderType 'horizontal' operator="=="}}floated-selection{{/compare}}">
	      							<label for="selection-{{this.id}}">
		      							<input name="{{../../article.id}}" type="radio" id="selection-{{this.id}}"
		      								class="reply-form" data-article-id="{{../../article.id}}"
	    	  								value="{{this.example}}"/> {{this.example}}
									</label>
	    						</p>	
							{{/if}}
						{{/each}}
					</div>
				</div>
			{{/compare}}

			{{#compare article.qType 'multiple_select' operator="=="}}
				<div class="row">
					<div class="col-sm-12">
						{{#each article.selections}}
							<p class="{{#compare ../../article.renderType 'horizontal' operator="=="}}floated-selection{{/compare}}">
								<label for="selection-{{this.id}}">
      								<input name="{{../../article.id}}" type="checkbox" id="selection-{{this.id}}"
	      								class="reply-form" data-article-id="{{../../article.id}}"
    	  								value="{{this.example}}"/> {{this.example}}
								</label>
    						</p>
						{{/each}}
					</div>
				</div>
			{{/compare}}
		</div>
	</div>
</div>
</script>
