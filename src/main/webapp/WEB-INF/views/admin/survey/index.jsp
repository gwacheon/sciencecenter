<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="container">
	<h4 class="page-title">설문조사 관리</h4>
	
	<div class="row margin-bottom30">
		<div class="col-sm-12">
			<table class="table margin-bottom30">
				<thead>
					<tr>
						<th>기간</th>
						<th>제목</th>
						<th>진행상황</th>
						<th>비고</th>
					</tr>
				</thead>
				
				<tbody>
					<c:forEach var="survey" items="${surveys }">
						<tr>
							<td>
								<fmt:formatDate value="${survey.beginTime}" pattern="yyyy-MM-dd"/>
								~
								<fmt:formatDate value="${survey.endTime}" pattern="yyyy-MM-dd"/>
							</td>
							<td>
								${survey.title }
							</td>
							<td>
								<c:choose>
									<c:when test="${survey.availableType == 1 }">
										진행중
									</c:when>
									<c:when test="${survey.availableType == 2 }">
										예정
									</c:when>
									<c:otherwise>
										마감
									</c:otherwise>
								</c:choose>
							</td>
							<td>
								<a href="<c:url value='/admin/surveys/${survey.id }/edit'/>"
									class="btn btn-white btn-sm">
									수정
								</a>
								
								<a href="<c:url value='/admin/surveys/${survey.id }/articles'/>"
									class="btn btn-white btn-sm">
									문항관리
								</a>
								
								<a href="<c:url value='/admin/surveys/${survey.id }/replies'/>"
									class="btn btn-white btn-sm">
									답변조회
								</a>
								
								<c:url var="deleteUrl" value="/admin/surveys/${survey.id }" />
								<form:form action="${deleteUrl }" method="delete" class="delete-form-in-table"  data-id="${survey.id }">
							        <button type="button" data-id="${survey.id }" 
							        	class="delete-btn btn btn-sm btn-danger">삭제</button>
								</form:form>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			
			<c:import url="/WEB-INF/views/layouts/adm/pagination.jsp" />
			
			<div class="right">
				<a href="<c:url value='/admin/surveys/new'/>" class="btn-primary btn">
					신규 설문조사 등록하기
				</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$(".delete-btn").click(function(e){
			e.preventDefault();
			var id = $(this).data('id');
			
			if(confirm("선택하신 설문 항목을 삭제하시겠습니까?")) {
				$(".delete-form-in-table[data-id='" + id +"']").submit();	
			}			
		});
	});
</script>