<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<form:form id="newHolidayForm" action="${param.action }"
	class="col-sm-12" 
	method="${param.method }" 
	commandName="survey" 
	modelAttribute="survey">
	
	<div class="row">
		<div class="input-field col-sm-6">
			<div class="form-group">
				<form:label path="title">설문명</form:label>
				<form:input type="text" path="title" required="true" class="form-control"/>			
			</div>
		</div>
		
		<div class="input-field col-sm-2">
			<div class="form-group">
				<form:label path="beginTime">프로그램 시작일</form:label>
				<form:input type="date" class="datepicker form-control" path="beginTime" />
			</div>
		</div>
		<div class="input-field col-sm-2">
			<div class="form-group">
				<form:label path="endTime">프로그램 종료일</form:label>
				<form:input type="date" class="datepicker form-control" path="endTime" />
			</div>
		</div>
		<div class="input-field col-sm-2">
			<div class="form-group">
				<div class="checkbox checkbox-teal margin-top30">
					<input type="checkbox" id="multiple" name="multiple" class="styled" <c:if test="${survey.multiple}">checked</c:if> /> 	
					<label for="multiple">동일 IP 허용</label>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-sm-12">
			<button type="submit" class="right btn btn-primary">저장</button>
		</div>
	</div>
</form:form>

<script type="text/javascript">
$('.datepicker').pickadate({
	selectMonths : true, // Creates a dropdown to control month
	selectYears: 3, // Creates a dropdown of 15 years to control year
	format: 'yyyy-mm-dd'
});
</script>
