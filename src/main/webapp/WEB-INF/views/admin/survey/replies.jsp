<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container">
	<h4 class="page-title">설문답변 관리</h4>
	
	<table class="table survey-header-table bordered margin-bottom30">
		<tbody>
			<tr>
				<th>제목</th>
				<td>${survey.title}</td>
			</tr>
			<tr>
				<th>기간</th>
				<td>
					<fmt:formatDate value="${survey.beginTime}" pattern="yyyy-MM-dd"/>
					~
					<fmt:formatDate value="${survey.endTime}" pattern="yyyy-MM-dd"/>
				</td>
			</tr>
		</tbody>
	</table>
	
	<h4 class="">총 설문자 수 : <c:out value="${replyCount }" /> 명</h4>
	
	<table class="table table-striped">
		<thead>
			<tr>
				<th>이름</th>
				<th>연락처</th>
				<th>Email</th>
				<th>작성일자</th>
			</tr>
		</thead>
		
		<tbody>
			<c:forEach var="reply" items="${replies }">
				<tr>
					<td>${reply.name }</td>
					<td>${reply.phone }</td>
					<td>${reply.email }</td>
					<td><fmt:formatDate value="${reply.regDttm}" pattern="yyyy-MM-dd"/></td>
				</tr>
			</c:forEach>
		</tbody>		
	</table>
	
	<div class="col-sm-12 center">
		<c:import url="pagination.jsp"></c:import>
	</div>
	
	<div class="col-sm-12 text-right">
		<a href="<c:url value='/admin/surveys/${survey.id }/replies/excel' />" class="btn btn-primary">
			설문답변 엑셀 다운로드
		</a>
	</div>
</div>