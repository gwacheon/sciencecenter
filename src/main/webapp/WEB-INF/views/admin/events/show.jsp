<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container">
	<div id="admin-board-index">
	    <div class="row margin-bottom30">
	    	<div class="col-xs-12 col-sm-4 col-md-3">
				<c:import url="/WEB-INF/views/admin/events/side.jsp" />
			</div>
			<div class="col-xs-12 col-sm-8 col-md-9">
				<h3>
					<spring:message code="event.${eventType.type}" /> - 상세보기
				</h3>
				<div class="admin-board-content margin-bottom20">
					<table class="table table-bordered table-striped">
						<tr>
							<th>
								제목
							</th>
							<td colspan="3">
								<c:out value="${event.title }" />
							</td>
						</tr>
						<tr>
							<th>
								시작일
							</th>
							<td>
								<fmt:formatDate pattern="yyyy-MM-dd" value="${event.startDate }" />
							</td>
							<th>
								종료일
							</th>
							<td>
								<fmt:formatDate pattern="yyyy-MM-dd" value="${event.endDate }" />
							</td>
						</tr>
						<tr>
							<th>
								진행상태
							</th>
							<td>
								<c:choose>
									<c:when test="${event.status== true }">
										진행 중
									</c:when>
									<c:otherwise>
										종료
									</c:otherwise>
								</c:choose>
							</td>
							<th>
								첨부파일
							</th>
							<td colspan="3">
								<c:forEach var="file" items="${event.attatchments }">
									<span style="margin-right: 5px;">
										<a href="<c:url value='${baseFileUrl }'/>${file.url}" >
											<c:out value="${file.name }" />
										</a>
									</span>
								</c:forEach>
							</td>
						</tr>
						<tr>
							<th>
								대표이미지
							</th>
							<td colspan="3">
								<img src="<c:url value='${baseFileUrl }'/>${event.pictureUrl }" class="responsive-img" alt="${event.title }"/>
							</td>
						</tr>
						<tr>
							<th>
								${eventType.link} 주소
							</th>
							<td colspan="3">
								<a target="_blank" href="http://${event.url }">${event.url }</a>
							</td>
						</tr>
						<tr>
							<th>
								내용
							</th>
							<td colspan="3">
								<c:out value="${event.content }" escapeXml="false"/>
							</td>
						</tr>
					</table>
				</div>
				<div class="right-align">
					<a href="<c:url value='/admin/events/list/${event.type }' />" class="waves-effect waves-light btn">
						목록으로
					</a>
				</div>
			</div>
		</div>
	</div>
</div>