<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<form:form id="newEventForm" action="${param.action }?${_csrf.parameterName}=${_csrf.token}"
	method="${param.method }" commandName="event" modelAttribute="event"
	enctype="multipart/form-data">
	
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<form:label path="type">분류</form:label>
				<form:select path="type" class="form-control">
					<option value="event" <c:if test="${event.type eq 'event' }">selected</c:if>>행사</option>
					<option value="play" <c:if test="${event.type eq 'play' }">selected</c:if>>과학문화공연</option>
					<option value="camp" <c:if test="${event.type eq 'camp' }">selected</c:if>>캠프</option>
					<option value="culture" <c:if test="${event.type eq 'culture' }">selected</c:if>>문화행사</option>
				</form:select>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<form:label path="title">제목</form:label>
				<form:input type="text" path="title" class="form-control"/>
			</div>
		</div>
	</div>
	<div class="row">
		<c:choose>
			<c:when test="${not empty event.pictureUrl }">
				<div id="current-main-image-file" class="col-sm-12 col-md-6">
					<div class="form-group">
						<form:input type="hidden" path="pictureUrl" name="pictureUrl"/>
						<p>
							현재 설정된 대표 이미지: 
							<button id="main-image-delete-btn" class="btn btn-danger btn-sm">삭제</button>
						</p>
						<img src="<c:url value='${baseFileUrl }'/>${event.pictureUrl }" class="img-responsive" alt="${event.pictureUrl }"/>
					</div>
				</div>
				<div id="new-main-image-file">
				
				</div>
			</c:when>
			<c:otherwise>
				<div class="col-md-12">
					<div class="form-group">
						<label for="file">대표이미지</label>
						<input id="file" type="file" name="file" class="form-control">
					</div>
					<form:input type="hidden" path="pictureUrl" name="pictureUrl" />
				</div>
			</c:otherwise>
		</c:choose>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<form:label path="startDate">시작일</form:label>
				<form:input type="date" class="datepicker" path="startDate"/>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<form:label path="endDate">종료일</form:label>
				<form:input type="date" class="datepicker" path="endDate"/>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<form:label path="place">장소</form:label>
				<form:input type="text" path="place" class="form-control"/>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<form:label path="pay">참가비 여부</form:label>
				<form:input type="text" path="pay" class="form-control"/>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<form:label path="url">예약페이지 상세보기 주소</form:label>
				<form:input type="text" path="url" class="form-control"/>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<form:label path="status">진행 상태</form:label>
				<form:select path="status" class="form-control">
					<option value="1">진행 중</option>
					<option value="0">종료</option>
				</form:select>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
	            <form:label path="content" class="active">내용</form:label>
				<form:textarea path="content" id="description" class="form-control"/>
	        </div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div id="file-wrapper" class="margin-bottom20">
				<c:if test="${ not empty event.attatchments }">
					<div class="title">
						첨부파일 목록
					</div>
					<c:forEach var="attachment" items="${event.attatchments}" varStatus="status">
						<div id="file-${attachment.id }-item" class="file">
							${attachment.name } <i class="fa fa-trash fa-lg file-delete-btn" aria-hidden="true" data-id="${attachment.id }"></i>							
						</div>
					</c:forEach>
				</c:if>
			</div>
			<div id="board-files-wrapper">
				
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<a id="add-file" class="btn btn-primary">새 첨부파일 추가</a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 margin-top20">
			<div class="text-right">
				<button type="button" class="btn btn-danger btn-cancel">취소</button>
				<button type="submit" class="btn btn-primary">저장</button>			
			</div>
		</div>
	</div>
</form:form>

<script type="text/javascript">
<!--
    CKEDITOR.replace( 'content', {
       filebrowserImageUploadUrl: baseUrl + 'attatchments' + '?${_csrf.parameterName}=${_csrf.token}'
} );
//-->
 
	$(document).ready(function() {
		$('select').material_select();

		$('.datepicker').pickadate({
			selectMonths : true, // Creates a dropdown to control month
			selectYears : 3, // Creates a dropdown of 15 years to control year
			format : 'yyyy-mm-dd'
		});
	});

	var index = 0;

	$('#add-file').on('click', function() {
		var source = $("#file-template").html();
		var template = Handlebars.compile(source);
		var data = {
			count : index
		};

		$('#board-files-wrapper').append(template(data));
		index++;
	});

	// form cancel button event
	$('.btn-cancel').on('click', function() {
		if (confirm("작성을 취소하시겠습니까?")) {
			window.history.back();
		}
	});
	
	$('.file-delete-btn').on("click", function(e) {
		e.preventDefault();
		if( confirm("해당 첨부파일이 영구 삭제됩니다.정말로 삭제하시겠습니까?")) {
			
			var fileId = $(this).data('id');
			
			$.ajax({
				type : "POST",
				beforeSend: function(xhr) {
		            xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));
		        },
				url : baseUrl + "admin/events/" + fileId + "/deleteFile",
				data : JSON.stringify({
					fileId: fileId
				}),
				contentType : 'application/json',
				success : function(data) {
					if( data.success ) {
						alert("파일이 삭제되었습니다.");
						$("#file-" + fileId +"-item").remove();						
					}

				},
				error: function(err){
					alert(err);
				}
			});
		}
	});

	$(document).on("click","#main-image-delete-btn", function(e) {
		e.preventDefault();
		if (confirm("대표 이미지가 삭제되면, 새로운 이미지를 업로드 해야 합니다. 정말로 삭제하시겠습니까?")) {
			$('#current-main-image-file').remove();
			var maineImageFilesource = $(
					"#main-image-file-template").html();
			var mainImageFileTemplate = Handlebars
					.compile(maineImageFilesource);
			$("#new-main-image-file").append(
					mainImageFileTemplate);
		}
	});
</script>

<script id="file-template" type="text/x-handlebars-template">
	<div class="form-group">
		<input type="file" name="attatchments[{{count}}].file" class="form-control">
	</div>
</script>

<script id="main-image-file-template" type="text/x-handlebars-template">
<div class="col-md-12">
	<div class="form-group">
		<label for="file" class="active">대표이미지</label>
		<input id="file" type="file" name="file" class="form-control">
	</div>
</div>
</script>
