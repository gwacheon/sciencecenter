<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="admin-board-index" class="container">
    <div class="row margin-bottom30">
    	<div class="col-xs-12 col-sm-4 col-md-3">
			<c:import url="/WEB-INF/views/admin/events/side.jsp" />
		</div>
		<div class="col-xs-12 col-sm-8 col-md-9">
			<h3><c:out value='${eventType.name }' /> 수정</h3>
	        <c:url var = "action" value='/admin/events/${event.id }'/>
	        <c:url var = "method" value='POST'/>
	        
	        <jsp:include page="/WEB-INF/views/admin/events/form.jsp">
	            <jsp:param name="action" value="${action}"/>
	            <jsp:param name="method" value="${method}"/>
	        </jsp:include>
		</div>
    </div>
</div>