<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>

<h3>문화행사 관리</h3>
<ul class="list-group">
	<c:forEach items="${eventTypes}" var="type">
		<c:url value="/admin/events/list/${type.key}" var="eventUrl" />
		<li class="list-group-item <c:if test="${eventType.type == type.key}">active</c:if>">
		<a href='${eventUrl}' data-name="${main_category_name }">
		      <spring:message code="event.${type.key}" />  
		</a>	
		</li>
		
	</c:forEach>
</ul>
