<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container">
	<div id="admin-board-index">
		<div class="row">
			<div class="col-xs-12 col-sm-4 col-md-3">
				<c:import url="/WEB-INF/views/admin/events/side.jsp" />
			</div>
			<div class="col-xs-12 col-sm-8 col-md-9">
				<h3><spring:message code="event.${eventType.type}" />  </h3>
	            <table class="table table-striped">
	                <thead>
	                    <tr>
	                        <th>
	                            제목
	                        </th>
	                        <th>
	                            시작일
	                        </th>
	                        <th>
	                            종료일
	                        </th>
	                        <th>
	                            진행상태
	                        </th>
	                        <th>
	                            
	                        </th>
	                    </tr>
	                </thead>
	                <tbody>
	                    <c:forEach var="event" items="${events }">
	                        <tr>
	                            <td>
	                            	<a href="<c:url value='/admin/events/${event.id }'/>" >
		                                <c:out value="${event.title }"></c:out>
	                            	</a>
	                                
	                            </td>
	                            <td>
	                                <fmt:formatDate pattern="yyyy-MM-dd" value="${event.startDate }" />
	                            </td>
	                            <td>
	                                <fmt:formatDate pattern="yyyy-MM-dd" value="${event.endDate }" />
	                            </td>
	                            <td>
	                                <c:choose>
	                                    <c:when test="${event.status== true }">
	                                        진행 중
	                                    </c:when>
	                                    <c:otherwise>
	                                        종료
	                                    </c:otherwise>
	                                </c:choose>
	                            </td>
	                            <td>
	                                <a href="<c:url value='/admin/events/edit/${event.id }'/>"
	                                    class="btn btn-warning btn-sm">
	                                    수정
	                                </a>
	                                
	                                <c:url var="deleteUrl" value="/admin/events/${event.id}/delete" />
	                                <form:form action="${deleteUrl }" method="DELETE" class="delete-form-in-table"  data-id="${event.id }">
	                                    <button type="button" data-id="${event.id }" 
	                                        class="delete-btn btn btn-danger btn-sm">삭제</button>
	                                </form:form>
	                            </td>
	                        </tr>
	                    </c:forEach>
	                </tbody>
	            </table>
	            <div class="text-right">
		            <a href="<c:url value="/admin/events/${eventType.type }/new"/>" class="btn btn-primary">
		            	<spring:message code="event.${eventType.type}" /> 생성
		            </a>
	            </div>
	            <c:import url="pagination.jsp"></c:import>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(function(){
    $(".delete-btn").click(function(e){
        var id = $(this).data("id");
        
        if(confirm("선택하신 프로그램을 삭제하시겠습니까?")){
            $("form[data-id='" + id + "']").submit();   
        }
    });
});
</script>
