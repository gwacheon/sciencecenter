<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container">
	<h5>프로그램 그룹 추가</h5>
	
	<div class="row margin-bottom30">
		<c:url var = "action" value='/admin/program_groups'/>
		<c:url var = "method" value='post'/>
		
		<jsp:include page="/WEB-INF/views/admin/program_groups/form.jsp">
			<jsp:param name="action" value="${action}"/>
			<jsp:param name="method" value="${method}"/>
		</jsp:include>
	</div>
</div>
