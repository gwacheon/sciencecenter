<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<form:form id="newHolidayForm" action="${param.action }"
	class="col s12" 
	method="${param.method }" 
	commandName="group" 
	modelAttribute="group">
	
	<div class="row">
		<div class="input-field col s4">
			<form:select path="category">
				<c:forEach var="category" items="${categories }">
				   <option value="${category}">${category }</option>
				</c:forEach>
			</form:select>
			<form:label path="category">분류</form:label>
		</div>
		
		<div class="input-field col s4">
			<form:input type="text" path="title" required="true" />
			<form:label path="title">프로그램그룹명</form:label>
		</div>
		
	
		<div class="input-field col s4">
			<form:select path="openYear">
				<c:forEach var="i" begin="2015" end="2020">
				   <option value="${i}">${i }</option>
				</c:forEach>
			</form:select>
			<form:label path="openYear">개강년도</form:label>
		</div>
	</div>			
	
	<div class="row">
		<div class="col s12">
			<button type="submit" class="right btn btn-primary">저장</button>
		</div>
	</div>
</form:form>


<script type="text/javascript">
	$(function(){
		$('select').material_select();
		
		$('.datepicker').pickadate({
			selectMonths : true, // Creates a dropdown to control month
			selectYears: 3, // Creates a dropdown of 15 years to control year
			format: 'yyyy-mm-dd'
		});
	});
</script>