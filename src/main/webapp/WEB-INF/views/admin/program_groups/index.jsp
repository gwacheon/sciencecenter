<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div class="container">
	<h5>프로그램 그룹 관리</h5>
	
	<div class="row margin-bottom30">
		<div class="col s12">
			<table class="striped">
				<thead>
					<tr>
						<th>No.</th>
						<th>카테고리</th>
						<th>그룸명</th>
						<th class="center">등록프로그램수</th>
						<th class="center">비고</th>
					</tr>
				</thead>
				
				<tbody>
					<c:forEach var="group" items="${groups }">
						<tr>
							<td>${group.id }</td>
							<td>${group.category }</td>
							<td>${group.title }</td>
							<td class="center">
								${fn:length(group.programs)}
							</td>
							<td class="center">
								<a href="<c:url value='/admin/program_groups/${group.id }/programs'/>" 
									class="waves-effect waves-light btn white small">
									프로그램
								</a>
								<a href="<c:url value='/admin/program_groups/${group.id }/edit'/>"
									class="waves-effect waves-light btn white small">
									수정
								</a>
								
								<c:url var="deleteUrl" value="/admin//program_groups/${group.id }" />
								<form:form action="${deleteUrl }" method="delete" class="delete-form-in-table"  data-id="${group.id }">
							        <button type="button" data-id="${group.id }" 
							        	class="delete-btn waves-effect waves-light btn red small">삭제</button>
								</form:form>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		
		<div class="col s12 center">
			<c:import url="pagination.jsp"></c:import>
		</div>
		
		<div class="col s12">
			<a href="<c:url value='/admin/program_groups/new'/>" class="right btn btn-primary">프로그램 그룹 등록</a>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$(".delete-btn").click(function(e){
			var id = $(this).data("id");
			
			if(confirm("선택하신 그룹을 삭제하시겠습니까? 그룹을 삭제하시면 해당 그룹에 포함된 프로그램 정보도 모두 함께 삭제됩니다.")){
				$("form[data-id='" + id + "']").submit();	
			}
		});
	});
</script>