<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="container">
	<div class="row margin-top30 margin-bottom30">
		<div class="">
			<div id="test1" class="col-sm-12">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>No</th>
							<th>
								<c:choose>
									<c:when test="${sangsangType eq 'building'}">
										건물명
									</c:when>
									<c:otherwise>
										장비명
									</c:otherwise>
								</c:choose>
							</th>
							<th>
								<c:choose>
									<c:when test="${sangsangType eq 'building'}">
										장소명
									</c:when>
									<c:otherwise>
										모델명
									</c:otherwise>
								</c:choose>
							</th>
							<th>수량</th>
							<th class="center">비고</th>
						</tr>
					</thead>
					
					<tbody>
						<c:forEach var="equipment" items="${equipments }">
							<tr class="equipment-row-${equipment.id }" >
								<td>
									${equipment.id}
								</td>
								<td>
									${equipment.name}
								</td>
								<td>
									${equipment.modelName}
								</td>
								<td>
									${fn:length(equipment.equipSerials)}
								</td>
								<td class="center">
									<a href="<c:url value='/admin/sangsang/show/${equipment.id }'/>"
										class="btn btn-white btn-sm">
										예약 내역 보기
									</a>
									<a href="<c:url value='/admin/sangsang/editEquipment/${equipment.id }'/>"
										class="btn btn-white btn-sm">
										수정
									</a>
									<a href="<c:url value='/admin/sangsang/unavailable/${equipment.id }'/>"
										class="btn btn-white btn-sm">
										 오프 날짜 선택
									</a>
									<button type="button" data-id="${equipment.id }" 
							        	class="delete-btn btn btn-danger btn-sm">삭제</button>
									
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				
				<div>
					<a href="<c:url value='/admin/sangsang/newEquipment/${type }'/>" class="right btn btn-primary">
						<c:choose>
							<c:when test="${sangsangType eq 'building'}">
								건물 추가
							</c:when>
							<c:otherwise>
								장비추가
							</c:otherwise>
						</c:choose>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
    	$('ul.tabs').tabs();
  	});
	
	$('.delete-btn').on('click',function(){
		var id = $(this).data('id');
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");
		
		if(confirm('정말 삭제하시겠습니까 ? ')){
			$.ajax({
				url : baseUrl + "admin/sangsang/delete",
				contentType : "application/json; charset=utf-8",
				dataType : "json",
				type : "POST",
				beforeSend: function(xhr){
					ajaxLoading();
					xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));			
				},
				data: JSON.stringify({id : id}),
				success : function(data){
					ajaxUnLoading();
					alert("저장되었습니다.");
					$('.equipment-row-'+id).remove();
				},
				error: function(err){
		    		console.log(err);
		    	}			
			});
		}
	});
	
</script>