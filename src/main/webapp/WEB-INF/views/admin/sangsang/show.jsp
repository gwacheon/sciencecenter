<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="container margin-bottom30">
	<div class="row">		
		<div class="col-xs-12 col-sm-4 col-md-3">
			<h3>${equipment.name }
			</h3>
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">
				<c:forEach var="serial" items="${equipment.equipSerials }">
				<div class="panel panel-default">
	  				<div class="panel-heading" role="tab">
	    				<h4 class="panel-title">
	      					<a href="<c:url value='/admin/sangsang/show/${equipment.id }'/>?serialId=${serial.id}" >
								${serial.serial}
							</a>
	    				</h4>
	  				</div>
  				</div>
  				</c:forEach>
			</div>
		</div>
		<div class="col-xs-12 col-sm-8 col-md-9">
			<div class="table-responsive margin-bottom30">
				<h3>예약 목록</h3>
				<table class="table table-teal">
					<thead>
						<tr>
							<th>예약번호</th>
							<th>예약날짜</th>
							<th>예약시간</th>
							<th>예약자</th>
							<th>연락처</th>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="reserve" items="${equipSerial.equipReserves }">
							<tr>
								<td>
									${reserve.id }
								</td>
								<td>
									<fmt:formatDate pattern="yyyy-MM-dd" value="${reserve.beginTime}" />
									 
								</td>
								<td>
									<fmt:formatDate pattern="HH:mm" value="${reserve.beginTime}" />
								</td>
								<td>
									${reserve.member.memberName }
								</td>
								<td>
									${reserve.member.memberCel }
								</td>
								<td>
									<button type="button" data-id="${reserve.id }" data-status="accepts"
										class="confirmation-btn btn btn-white btn-sm">
										 수락
								 	</button>
									<button type="button" data-id="${reserve.id }" data-status="cancel"
							        	class="delete-btn btn btn-danger btn-sm">반려</button>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>	
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(".confirmation-btn").on("click", function(){
		var id = $(this).data("id");
		var status = $(this).data("status");
		var header = $("meta[name='_csrf_header']").attr("content");
		var token = $("meta[name='_csrf']").attr("content");
		
		$.ajax({
			url: baseUrl + "admin/sangsang/confirmation",
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			type : "POST",
			beforeSend: function(xhr) {
	            xhr.setRequestHeader(header, token);
	            ajaxLoading();
	        },
			data: JSON.stringify({
				id: id,
				status: status
			}),
	    	success: function(data){
	    		ajaxUnLoading();
	    	}
		});
	});
</script>
