<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<style>
	table#sangsang-schedule-table > tbody > tr > td {
		padding-top: 10px;
		padding-bottom: 10px;
	}
</style>

<div class="container margin-top30 margin-bottom30">
	<div class="row">
		<input type="hidden" id="schedule-title-id" value="${sangSangSchedule.id }"/>
		<div class="input-field  col-sm-8">
			<div class="form-group">
				<label for="title">제목</label>
				<input placeholder="" id="title" type="text" class="form-control"
				value="${sangSangSchedule.title }">
			</div>
		</div>
		
		<div class="input-field  col-sm-4">
			<div class="form-group">
				<label for="month">달력</label>
				<input id="schedule-date" type="text" class="datepicker form-control" value="">
          	</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-sm-12">
			<table id="sangsang-schedule-table" class="lined-table centered-table">
				<thead>
					<tr>
						<th>일</th>
						<th>월</th>
						<th>화</th>
						<th>수</th>
						<th>목</th>
						<th>금</th>
						<th>토</th>
					</tr>
				</thead>
					
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row margin-top30">
		<div class="col-sm-12">
			<button id="save-scheudle-btn" type="button" class="btn btn-primary">
				저장
			</button>
		</div>
	</div>
</div>

<script id="sangsang-schedule-template" type="text/x-handlebars-template">
{{#each dates}}
	<tr>
		{{#each this}}
			<td class="date" data-date="{{formatedDate date "YYYY-MM-DD"}}">
				{{#if isInMonth}}
					<div class="day-no">
						{{formatedDate date "DD"}}
					</div>
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<div class="day-input">
								<input type="text" data-date="{{formatedDate date "YYYY-MM-DD"}}"
									class="schedule-name form-control" value="{{title}}"/>
							</div>
						</div>
					</div>
				{{/if}}
			</td>
		{{/each}}
	</tr>
{{/each}}
</script>

<script type="text/javascript">
	Handlebars.registerHelper('formatedDate', function(date, format) {
	  return moment(date).format(format);
	});
	
	var selectedDate = moment("<fmt:formatDate pattern="yyyy-MM-dd" value="${sangSangSchedule.beginMonth }" />", "YYYY-MM-DD").startOf('month');
	var dates = [];
	<c:forEach var="schedule" items="${sangSangSchedule.schedules }">
		dates.push({
			date: moment("<fmt:formatDate pattern="yyyy-MM-dd" value="${schedule.sDate }" />", "YYYY-MM-DD"),
			title: "${schedule.title }",
			isInMonth: true
		});
	</c:forEach>
	
	$("#schedule-date").val(selectedDate.format("YYYY-MM-DD"));
	
	$(function(){
		$('.datepicker').pickadate({
			format: 'yyyy-mm-dd'
		});
		
		$("#schedule-date").change(function(){
			var changedDate = moment($(this).val(), "YYYY-MM-DD").startOf('month');
			
			if(selectedDate.format("YYYY-MM-DD") != changedDate.format("YYYY-MM-DD")){
				selectedDate = changedDate;
				dates = [];
				renderCalendars();
			}
		});
		
		function renderCalendars(){
			var beginOfMonth = moment(selectedDate).startOf('month');
			var beginDate = moment(beginOfMonth).startOf('week');
			
			var endOfMonth = moment(selectedDate).endOf('month');
			var endDate = moment(endOfMonth).endOf('week');
			
			if(dates.length == 0){
				for(var temp = beginDate; temp.isBefore(endDate); temp.add(1, 'days')){
					var data = {};
					
					if(temp.isBefore(beginOfMonth) || temp.isAfter(endOfMonth)){
						data.isInMonth = false;
					}else{
						data.isInMonth = true;
					}
					
					data.date = temp.clone();
					
					dates.push(data);
				}
			}else{
				var beginTemp = moment(dates[0].date, "YYYY-MM-DD").startOf('week');
				var endTemp = moment(_.last(dates).date, "YYYY-MM-DD");
				
				for( ; beginTemp.isBefore(beginOfMonth); beginTemp.add(1, 'days')){
					dates.unshift({
						isInMonth: false,
						date: beginTemp.clone()
					});
				}
				
				for(endTemp.add(1, 'days') ; endTemp.isBefore(endDate); endTemp.add(1, 'days')){
					dates.push({
						isInMonth: false,
						date: endTemp.clone()
					});
				}
				
				console.log(dates);
			}
			
			resultedDates = _.chunk(dates, 7);
			
			var source = $("#sangsang-schedule-template").html();
			var template = Handlebars.compile(source);
			var html = template({dates: resultedDates});
			$("#sangsang-schedule-table > tbody").html(html);
		}
		
		renderCalendars();
	});
	
	$("#save-scheudle-btn").click(function(){
		if(confirm("입력하신 내용들을 저장하시겠습니까?")){
			var sendData = {
				id: $("#schedule-title-id").val(),
				title: $("#title").val(),
				beginMonth: $("#schedule-date").val(),
				schedules: []
			}; 
			
			_.forEach($(".schedule-name"), function(d, i){
				var obj = {
					date: $(d).data("date"),
					title: $(d).val()
				};
				
				sendData.schedules.push(obj);
			});
			
			$.ajax({
				url : baseUrl + "admin/sangsang/schedules/save",
				contentType : "application/json; charset=utf-8",
				dataType : "json",
				type : "POST",
				beforeSend: function(xhr){
					ajaxLoading();
					xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));			
				},
				data: JSON.stringify(sendData),
				success : function(data){
					ajaxUnLoading();
					alert("저장되었습니다.");
				},
				error: function(err){
		    		console.log(err);
		    	}			
			});
			
		}
	});
</script>