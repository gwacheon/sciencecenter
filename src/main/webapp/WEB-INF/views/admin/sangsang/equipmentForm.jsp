<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<form:form id="newEquipmentForm"
	action="${param.action }?${_csrf.parameterName}=${_csrf.token}"
	class="col-sm-12" method="POST" commandName="equipment"
	modelAttribute="equipment" enctype="multipart/form-data">

	<input type="hidden" name="type" value="${equipment.type }">
	<input type="hidden" name="id" value="${equipment.id }">
	<input type="hidden" name="picture" value="${equipment.picture }">
	<div class="row">
		<div class="col-sm-12 file-field input-field">
			<div class="form-group">
				<label for="file">대표이미지</label>
				<input id="file" type="file" name="file" accept="image/*" class="form-control">
			</div>
			<div class="file-path-wrapper">
				<input id="picture" class="file-path validate hidden" type="text"
					name='picture'>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="input-field col-sm-6">
			<div class="form-group">
				<form:label path="name">
					<c:choose>
						<c:when test="${sangsangType eq 'building'}">
							건물명
						</c:when>
						<c:otherwise>
							장비명
						</c:otherwise>
					</c:choose>
				</form:label>
				<form:input type="text" path="name" required="true" class="form-control"/>
			</div>
		</div>

		<div class="input-field col-sm-6">
			<div class="form-group">
				<form:label path="modelName">
					<c:choose>
						<c:when test="${sangsangType eq 'building'}">
							장소명
						</c:when>
						<c:otherwise>
							모델명
						</c:otherwise>
					</c:choose>
				</form:label>
				<form:input type="text" path="modelName" required="true" class="form-control"/>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="input-field col-sm-12">
			<div class="form-group">
				<form:label path="description"></form:label>
				<form:textarea path="description" id="description"
				class="form-control" />
			</div>
		</div>
	</div>

	<c:set var="serialIdx" value="0" />

	<div class="row">
		<div id="serial-wrapper">
			<div class="input-field col-sm-6">
				<div class="form-group">
					<form:label path="equipSerials[${serialIdx }].serial">
						<c:choose>
							<c:when test="${sangsangType eq 'building'}">
								건물 상세 장소
							</c:when>
							<c:otherwise>
								단말기 고유번호
							</c:otherwise>
						</c:choose>
					</form:label>
					<form:input type="text" path="equipSerials[${serialIdx }].serial"
					required="true" class="form-control"/>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="input-field col-sm-6">
			<a id="add-serial-btn" class="btn btn-white">
				<c:choose>
					<c:when test="${sangsangType eq 'building'}">
						건물 상세 추가 
					</c:when>
					<c:otherwise>
						단말기 추가
					</c:otherwise>
				</c:choose>
			</a>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<button type="submit" class="right btn btn-primary">저장</button>
		</div>
	</div>
</form:form>

<script type="text/javascript">
	var serialIdx = "${serialIdx }";
	
	$('#add-serial-btn').click(function(e){
		e.preventDefault();
		
		serialIdx++;
		
		var source   = $("#serial-form-template").html();
		var template = Handlebars.compile(source);
		var html = template({idx: serialIdx});
		
		$("#serial-wrapper").append(html);
	});
	<!--
    CKEDITOR.replace( 'description', {
       filebrowserImageUploadUrl: baseUrl + 'attatchments' + '?${_csrf.parameterName}=${_csrf.token}'
	} );
	//-->
</script>

<script id="serial-form-template" type="text/x-handlebars-template">
	<div class="input-field col-sm-6">
		<div class="form-group">
			<label for="equipSerials{{idx}}.serial" class="">단말기 고유번호</label>
			<input id="equipSerials{{idx}}.serial" name="equipSerials[{{idx}}].serial" 
				type="text" required="true" value="" class="form-control">
		</div>
	</div>
</script>