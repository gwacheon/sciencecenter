<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container">
	<h4 class="page-title">무한상상실 
		<c:choose>
			<c:when test="${sangsangType eq 'building'}">
				대관추가
			</c:when>
			<c:otherwise>
				장비추가
			</c:otherwise>
		</c:choose>
	</h4>
	
	<div class="row margin-bottom30">
	   <c:url value="/admin/sangsang/newEquipment" var="action" />
	   
		<jsp:include page="/WEB-INF/views/admin/sangsang/equipmentForm.jsp">
		  <jsp:param name="action" value="${action}"/>
		</jsp:include>
		
	</div>
</div>
