<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="container">
	<h4 class="page-title">
		무한상상실 장비 오프 날짜 관리
	</h4>
	
	<div class="row">
		<div class="input-field col-sm-12">
			<div class="form-group">
				<label>장비 선택</label>
				<select id="selected-equipment" class="form-control">
					<c:forEach items="${equipment.equipSerials }" var="serial">
						<option value="${serial.id }">${serial.serial }</option>
					</c:forEach>
				</select>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div id="equip-unavailable-calendars"></div>
		</div>
		<div class="col-sm-12">
			<button type="button" id="unavailable-confirm-btn"
				class="right btn btn-primary btn-flat">
				블럭 추가</button>
		</div>
	</div>
</div>
<!-- Modal Structure -->
<div id="unavailable-modal" class="modal">
	<div class="modal-content">
		<h4>예약 블럭 설정</h4>
		<p>선택하신 장비와 시간에 대한 예약 정지 하겠습니까 ?</p>

		<ul id="unavailable-infos">

		</ul>
		<div class="modal-footer">
			<a id="submit-unavailable-btn" href="#!"
				class="modal-action waves-effect waves-green btn-flat"> 추가하기 </a>
		</div>
	</div>
</div>


<script id="equipment-calendar-template"
	type="text/x-handlebars-template">

<div class="month-selector" style="text-align: center">
	<a id="prev-month" class="month-selector" href="#" data-date="{{prevMonth}}">
		&lt;
	</a>

	<div id="selected-month" style="display: inline-block">
		{{formatedDate selectedDate "YYYY.MM"}}
	</div>

	<a id="next-month" href="#" class="month-selector" data-date="{{nextMonth}}">
		&gt;
	</a>
</div>

<div id="equip-calendar" class="calendar-area">
	<table id="calendar-table" class="ui celled table unstackable">
		<thead>
			<tr>
				<th class="center">일</th>
				<th class="center">월</th>
				<th class="center">화</th>
				<th class="center">수</th>
				<th class="center">목</th>
				<th class="center">금</th>
				<th class="center">토</th>
			</tr>
		</thead>
		<tbody class="calendar-body">
			{{#each dates}}
				<tr>
					{{#each this}}
						<td class="center date" data-date="{{formatedDate this "YYYY-MM-DD"}}">
							{{formatedDate this "DD"}}
							<div class="circle"></div>
						</td>
					{{/each}}
				</tr>
			{{/each}}
		</tbody>
	</table>
</div>
</script>

<script type="text/javascript">
	var selectedDate = moment(new Date())._d;

	Handlebars.registerHelper('formatedDate', function(date, format) {
		return moment(date).format(format);
	});
	function prevMonth(date) {
		return moment(date).add(-1, 'Month')._d;
	}

	function nextMonth(date) {
		return moment(date).add(1, 'Month')._d;
	}
	function dates(date) {
		ret = [];

		if (!_.isDate(date)) {
			date = moment(new Date());
		}

		var beginOfMonth = moment(date).startOf('month').startOf('week');
		var endOfMonth = moment(date).endOf('month').endOf('week');

		for (var temp = beginOfMonth; temp.isBefore(endOfMonth); temp.add(1,
				'days')) {
			ret.push(temp.clone());
		}

		ret = _.chunk(ret, 7);

		return ret;
	}

	function renderUnavailable() {
		var data = {
			selectedDate : selectedDate,
			prevMonth : prevMonth(selectedDate),
			nextMonth : nextMonth(selectedDate),
			dates : dates(selectedDate)
		};
		var source = $("#equipment-calendar-template").html();
		var template = Handlebars.compile(source);
		var html = template(data);
		$("#equip-unavailable-calendars").html(html);
	}

	function markingUnavailable() {

		$.getJSON(baseUrl + "sangsang/getEquipUnavailable", {
			equipmentId : <c:out value='${equipment.id}'/>,
			equiSerialId : $('#selected-equipment').val()
		}, function(data) {
			_.forEach(data.unavailable, function(date) {
				var d = moment(date.beginTime);
				$("td.date[data-date='" + d.format("YYYY-MM-DD") + "']")
						.addClass('unavailable');
			});
		});
	}
	function unMarkingUnavailable() {
		$('td.date').removeClass('unavailable');
	}

	$(function() {
		renderUnavailable();
		unMarkingUnavailable();
		markingUnavailable();

		$('select').material_select();

		$("#selected-equipment").on("change", function() {
			unMarkingUnavailable();
			markingUnavailable();
		})

		$("td.date").on('click', function(e) {
			if ($(this).hasClass('unavailable')) {
				if (confirm($(this).data('date') + "를 활성화하시겠습니까 ?")) {
					console.log("delete");
					location.reload();
				}
			} else {
				$(this).addClass("active");
			}
		});

		$("#unavailable-confirm-btn").on('click', function() {
			var values = [];
			var source = "";
			_.forEach($('td.date.active'), function(n, key) {
				values.push($(n).data('date'));
				source += "<li>" + $(n).data('date') + "</li>";
			});

			$('#unavailable-infos').html(source);
			$('#unavailable-modal').openModal();
		});

		$('#submit-unavailable-btn').on('click', function() {

			var days = [];

			_.forEach($('#unavailable-infos li'), function(n, key) {
				days.push($(n).text());
			});
			var data = {
				equipmentId : <c:out value='${equipment.id}'/>,
				equiSerialId : $('#selected-equipment').val(),
				days : days
			};

			var token = $("meta[name='_csrf']").attr("content");
			var header = $("meta[name='_csrf_header']").attr("content");

			$.ajax({
				url : baseUrl + "sangsang/equipUnavailable",
				contentType : "application/json; charset=utf-8",
				dataType : "json",
				type : "POST",
				beforeSend : function(xhr) {
					xhr.setRequestHeader(header, token);
				},
				data : JSON.stringify(data),
				success : function(data) {
					alert("선택하신 날짜에 예약이 불가능하도록 설정되었습니다.");
					location.reload();
				},
				error : function(err) {
					console.log(err);
				}
			});
		});
	})
</script>