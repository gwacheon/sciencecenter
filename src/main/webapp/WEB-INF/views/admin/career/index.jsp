<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h4>진로탐구 관리</h4>
		</div>
	</div>
	
	<div class="row margin-bottom30">
		<div class="col-sm-12">
			<table class="table">
				<thead>
					<tr>
						<th>ID</th>
						<th>제목</th>
						<th>비고</th>
					</tr>
				</thead>
				
				<tbody>
					<c:forEach var="career" items="${careers }">
						<tr>
							<td>${career.id }</td>
							<td>${career.title }</td>
							<td>
								<a href="<c:url value='/admin/career/${career.id }/edit'/>" class="btn btn-white">
									수정
								</a>
								
								<c:url var="deleteUrl" value="/admin/career/${career.id}" />
								<form:form action="${deleteUrl }" method="delete" class="delete-form-in-table"  data-id="${career.id }">
							        <button type="button" data-id="${career.id }" 
							        	class="delete-btn btn btn-danger">삭제</button>
								</form:form>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	
	<div class="row margin-bottom30">
		<div class="col-sm-12">
			<a href="<c:url value="/admin/career/new"/>" class="right btn btn-primary">
				신규과정 등록
			</a>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$(".delete-btn").click(function(e){
			var id = $(this).data("id");
			
			if(confirm("선택하신 진로탐구 항목을 삭제하시겠습니까?")){
				$("form[data-id='" + id + "']").submit();	
			}
		});
	});
</script>