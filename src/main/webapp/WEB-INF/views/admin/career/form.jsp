<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<form:form id="careerForm" action="${param.action }" class="col s12"
	method="${param.method }" commandName="career" modelAttribute="career">
	
	<div class="row">
		<div class="col-sm-8">
			<div class="form-group">
				<form:label path="title">제목</form:label>
				<form:input type="text" path="title" class="form-control"/>
			</div>
		</div>
		
		<div class="col-sm-4">
			<div class="form-group">
				<form:label path="beginDate">시작일자</form:label>
				<form:input id="begin-date" type="text" path="beginDate" class="begin-date form-control" />
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-sm-12">
			<button type="submit" class="right btn btn-primary">저장</button>
		</div>
	</div>
</form:form>

<script type="text/javascript">
	$(function(){
		$('.begin-date').pickadate({
			selectMonths : true, // Creates a dropdown to control month
			selectYears : 3, // Creates a dropdown of 15 years to control year
			format : 'yyyy-mm-dd'
		});
	});
</script>