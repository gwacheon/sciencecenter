<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<style>
	.filled-in {
		margin-top: 20px !important;
	}
</style>

<div class="container">
    <h5>${career.title } 수정</h5>
    
    <div class="margin-bottom30">
        <c:url var = "action" value='/admin/career/${career.id }'/>
        <c:url var = "method" value='PUT'/>
        
        <jsp:include page="/WEB-INF/views/admin/career/form.jsp">
            <jsp:param name="action" value="${action}"/>
            <jsp:param name="method" value="${method}"/>
        </jsp:include>
    </div>
    
    <div class="row margin-bottom30">
    	<div class="col-sm-12">
    		<table class="career-schedule-form-table table table-bordered">
    			<thead id="day-header">
    				
    			</thead>
    			
    			<tbody id="career-schedules-body">
    				<c:forEach var="schedule" items="${career.schedules }" varStatus="loop">
    					<tr class="career-schedule-row" data-seq="${loop.index }">
    						<td>
								<div class="row">
									<div class="col-sm-10">
										<input type="text" class="schedule-title form-control" data-date="tue" 
											value="${schedule.tueWord }">
									</div>
									<div class="col-sm-2">
										<p>
					      					<input id="availalbe-${loop.index }-tue" type="checkbox" class="filled-in available"  
					      						data-date="tue" <c:if test="${schedule.tueAvailable }">checked</c:if> />
					      					<label for="availalbe-${loop.index }-tue"></label>
					    				</p>
									</div>
								</div>
							</td>
							<td>
								<div class="row">
									<div class="col-sm-10">
										<input type="text" class="schedule-title form-control" data-date="wed"
											value="${schedule.wedWord }">
									</div>
									<div class="col-sm-2">
										<p>
					      					<input id="availalbe-${loop.index }-wed" type="checkbox" class="filled-in available"  
					      						data-date="wed" <c:if test="${schedule.wedAvailable }">checked</c:if> />
					      					<label for="availalbe-${loop.index }-wed"></label>
					    				</p>
									</div>
								</div>
							</td>
							<td>
								<div class="row">
									<div class="col-sm-10">
										<input type="text" class="schedule-title form-control" data-date="thu"
											value="${schedule.thuWord }">
									</div>
									<div class="col-sm-2">
										<p>
					      					<input id="availalbe-${loop.index }-thu" type="checkbox" class="filled-in available"  
					      						data-date="thu" <c:if test="${schedule.thuAvailable }">checked</c:if> />
					      					<label for="availalbe-${loop.index }-thu"></label>
					    				</p>
									</div>
								</div>
							</td>
							<td>
								<div class="row">
									<div class="col-sm-10">
										<input type="text" class="schedule-title form-control" data-date="fri"
											value="${schedule.friWord }">
									</div>
									<div class="col-sm-2">
										<p>
					      					<input id="availalbe-${loop.index }-fri" type="checkbox" class="filled-in available" 
					      						data-date="fri" <c:if test="${schedule.friAvailable }">checked</c:if>/>
					      					<label for="availalbe-${loop.index }-fri"></label>
					    				</p>
									</div>
								</div>
							</td>
    					</tr>
    				</c:forEach>
    			</tbody>
    		</table>
    	</div>
    	
    	<div class="col-sm-12">
    		<a href="<c:url value='/admin/career'/>" class="btn btn-white">리스트</a>
    		<button type="button" class="save-btn right btn btn-primary">저장</button>
    		
    		<button type="button" class="right btn btn-white add-btn white" style="margin-right: 20px;">추가</button>
    	</div>
    </div>
</div>

<script type="text/javascript">
$(function(){
	var beginDate = moment("<fmt:formatDate pattern="yyyy-MM-dd" value="${career.beginDate }" />", "YYYY-MM-DD");
	var dates = [];
	var dayStrs = ["화", "수", "목", "금"];
	
	for(var i = 0; i < 4; i++){
		dates.push({
			dayStr: dayStrs[i],
			date: beginDate.clone().add(i, 'days').format("YYYY-MM-DD")
		});
	}
	
	var source   = $("#day-header-template").html();
	var template = Handlebars.compile(source);
	var data = {dates: dates};
	var html    = template(data);
	
	$("#day-header").html(html);
});



var scheduleSeq = "${fn:length(career.schedules)}" || 0;


$(".add-btn").click(function(e){
	var source   = $("#career-schedule-template").html();
	var template = Handlebars.compile(source);
	var data = {
		seq: scheduleSeq,
		dates: ['tue', 'wed', 'thu', 'fri']
	}
	var html    = template(data);
	$("#career-schedules-body").append(html);
	
	scheduleSeq++;
});

$(".save-btn").click(function(e){
	e.preventDefault();
	
	if(confirm("입력하신 내용들을 저장하시겠습니까?")){
		var careerSchedules = [];
		
		_.forEach($('.career-schedule-row'), function(d, i){
			careerSchedules.push({
				tueWord: $(d).find(".schedule-title[data-date='tue']").val(),
				tueAvailable: $(d).find(".available[data-date='tue']").is(":checked"),
				wedWord: $(d).find(".schedule-title[data-date='wed']").val(),
				wedAvailable: $(d).find(".available[data-date='wed']").is(":checked"),
				thuWord: $(d).find(".schedule-title[data-date='thu']").val(),
				thuAvailable: $(d).find(".available[data-date='thu']").is(":checked"),
				friWord: $(d).find(".schedule-title[data-date='fri']").val(),
				friAvailable: $(d).find(".available[data-date='fri']").is(":checked")
			});
		});	
		
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");
		
		$.ajax({
			url : baseUrl + "admin/career/${career.id}/addSchedules",
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			type : "POST",
			beforeSend : function(xhr) {
				ajaxLoading();
				xhr.setRequestHeader(header, token);
			},
			data : JSON.stringify(careerSchedules),
			success : function(data) {
				console.log(data);
				
				_.forEach(data.career.schedules, function(s, i){
					var scheduleRow = $(".career-schedule-row[data-seq='" + i + "']");
					console.log($(scheduleRow).find(".available[data-date='tue']"));
					$(scheduleRow).find(".available[data-date='tue']").prop('checked', s.tueAvailable);
					$(scheduleRow).find(".available[data-date='wed']").prop('checked', s.wedAvailable);
					$(scheduleRow).find(".available[data-date='thu']").prop('checked', s.thuAvailable);
					$(scheduleRow).find(".available[data-date='fri']").prop('checked', s.friAvailable);
				});
				
				ajaxUnLoading();
				alert("정상 저장되었습니다.");
			},
			error : function(err) {
				console.log(err);
			}
		});
	}
	
});

</script>

<script id="day-header-template" type="text/x-handlebars-template">
<tr>
	{{#each dates}}
		<th>{{dayStr}} ({{date}})</th>
	{{/each}}
</tr>
</script>

<script id="career-schedule-template" type="text/x-handlebars-template">
<tr class="career-schedule-row" data-seq="{{seq}}">
	{{#each dates}}
		<td>
			<div class="row">
				<div class="col-sm-10">
					<input type="text" class="schedule-title form-control" data-date="{{this}}">
				</div>
				<div class="col-sm-2">
					<p>
      					<input id="availalbe-{{../seq}}-{{this}}" type="checkbox" class="filled-in available" 
							data-date="{{this}}" checked/>
      					<label for="availalbe-{{../seq}}-{{this}}"></label>
    				</p>
				</div>
			</div>
		</td>
	{{/each}}
</tr>
</script>