<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div class="container">
	<div id="main-banner-row" class="row">
		<div class="col-sm-6 main-page-grid">
			<div class="height_5_6">
				<div class="main-page-grid-item">
					<c:set var="banners" value="${type1Banners}" scope="request" />
					<c:set var="bannerType" value="type1" scope="request"></c:set>
					<c:import url="/WEB-INF/views/admin/mainPage/banner_tmpl.jsp"/>
				</div>
			</div>
		</div>
		
		<div class="col-xs-6 col-sm-3 main-page-grid">
			<div class="height_5_4">
				<div class="main-page-grid-item">
					<c:set var="banners" value="${type2Banners}" scope="request" />
					<c:set var="bannerType" value="type2" scope="request"></c:set>
					<c:import url="/WEB-INF/views/admin/mainPage/banner_tmpl.jsp"/>
				</div>
			</div>
			
			<div class="height_5_4">
				<div class="main-page-grid-item">
					<c:set var="banners" value="${type3Banners}" scope="request" />
					<c:set var="bannerType" value="type3" scope="request"></c:set>
					<c:import url="/WEB-INF/views/admin/mainPage/banner_tmpl.jsp"/>
				</div>
			</div>
			
			<div class="height_5_4">
				<div class="main-page-grid-item">
					<c:set var="banners" value="${type4Banners}" scope="request" />
					<c:set var="bannerType" value="type4" scope="request"></c:set>
					<c:import url="/WEB-INF/views/admin/mainPage/banner_tmpl.jsp"/>
				</div>
			</div>
		</div>
		
		<div class="col-xs-6 col-sm-3 main-page-grid">
			<div class="height_5_8">
				<div class="main-page-grid-item">
					<c:set var="banners" value="${type5Banners}" scope="request" />
					<c:set var="bannerType" value="type5" scope="request"></c:set>
					<c:import url="/WEB-INF/views/admin/mainPage/banner_tmpl.jsp"/>
				</div>
			</div>
		
			<div class="height_5_2">
				<div class="main-page-grid-item">
					<c:set var="banners" value="${type6Banners}" scope="request" />
					<c:set var="bannerType" value="type6" scope="request"></c:set>
					<c:import url="/WEB-INF/views/admin/mainPage/banner_tmpl.jsp"/>
				</div>
			</div>
			
			<div class="height_5_2">
				<div class="main-page-grid-item">
					<c:set var="banners" value="${type7Banners}" scope="request" />
					<c:set var="bannerType" value="type7" scope="request"></c:set>
					<c:import url="/WEB-INF/views/admin/mainPage/banner_tmpl.jsp"/>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="main-icon-banners">
	<div class="container">
		<div class="row">
			<c:forEach var="iconBanner" items="${iconBanners }" varStatus="loop">
				<div class="col-xs-4 col-sm-3">
					<div class="iconBanner">
						<c:forEach var="banner" items="${iconBanner }">
							<img alt="${banner.title }" src="<c:url value="${baseFileUrl }${banner.bannerImageUrl }"/>"
								class="img-responsive"/>
						</c:forEach>
						<div class="banner-btns">
							<button type="button" class="btn btn-white new-banner-btn" data-type="iconBanner${loop.index }">
								등록
							</button>
							
							<button type="button" class="btn btn-teal edit-banner-btn" data-type="iconBanner${loop.index }">
								수정
							</button>
						</div>
					</div>
				</div>
			</c:forEach>
		</div>
	</div>
</div>

<div id="main-event-banners">
	<div class="container">
		<div class="row">
			<c:forEach var="eventBanner" items="${eventBanners }" varStatus="loop">
				<div class="col-xs-6 col-sm-4 main-page-grid">
					<div class="iconBanner">
						<c:forEach var="banner" items="${eventBanner }">
							<img alt="${banner.title }" src="<c:url value="${baseFileUrl }${banner.bannerImageUrl }"/>"
								class="img-responsive"/>
						</c:forEach>
						<div class="banner-btns">
							<button type="button" class="btn btn-white new-banner-btn" data-type="eventBanner${loop.index }">
								등록
							</button>
							
							<button type="button" class="btn btn-teal edit-banner-btn" data-type="eventBanner${loop.index }">
								수정
							</button>
						</div>
					</div>
				</div>
			</c:forEach>
		</div>
	</div>
</div>

<div id="main-footer-banners">
	<div class="container">
		<div class="row">
			<c:forEach var="footerBanner" items="${footerBanners }" varStatus="loop">
				<div class="col-xs-6 col-sm-4 main-page-grid">
					<div class="iconBanner">
						<c:forEach var="banner" items="${footerBanner }">
							<img alt="${banner.title }" src="<c:url value="${baseFileUrl }${banner.bannerImageUrl }"/>"
								class="img-responsive"/>
						</c:forEach>
						<div class="banner-btns">
							<button type="button" class="btn btn-white new-banner-btn" data-type="footerBanner${loop.index }">
								등록
							</button>
							
							<button type="button" class="btn btn-teal edit-banner-btn" data-type="footerBanner${loop.index }">
								수정
							</button>
						</div>
					</div>
				</div>
			</c:forEach>
		</div>
	</div>
</div>

<div id="new-main-banner-modal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">배너 등록</h4>
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="row">
						<c:url value="/admin/mainPage" var="mainPageUrl" />
						<form:form id="edit-main-banner-form" action="${mainPageUrl }?${_csrf.parameterName}=${_csrf.token}" class="col s12"
							method="POST" commandName="mainPageBanner" modelAttribute="mainPageBanner"
							enctype="multipart/form-data">
							<input type="hidden" id="new-banner-type" name="bannerType">
							<div class="col-xs-12">
								<div class="form-group">
									<label for="new-title">제목</label>
									<input id="new-title" name="title" 
										type="text" class="form-control">
								</div>
								
								<div class="form-group">
									<label for="new-banner-link-url">Link URL</label>
									<input id="new-banner-link-url" name="bannerLinkUrl"
										type="text" class="form-control">
								</div>
								
								<div class="form-group">
									<label for="new-banner-image">배너이미지</label>
									<input id="new-banner-image" name="bannerImageFile" type="file">
								</div>
							</div>
						</form:form>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">취소</button>
				<button id="submit-edit-main-banner" type="button" class="btn btn-primary">저장</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div id="edit-main-banner-modal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">배너 수정</h4>
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<table id="main-page-item-table" class="table table-striped">
								<thead>
									<tr>
										<th width="25%">이미지</th>
										<th>제목</th>
										<th width="20%"></th>
										<th width="20%"></th>
									</tr>
								</thead>
								
								<tbody id="sortable-banner-items">
								
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">취소</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div id="edit-main-banner-item-modal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">배너 수정</h4>
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="row">
						<c:url value="/admin/mainPage/banners/update" var="mainPageUrl" />
						<form:form id="new-main-banner-form" action="${mainPageUrl }?${_csrf.parameterName}=${_csrf.token}" class="col s12"
							method="POST" commandName="mainPageBanner" modelAttribute="mainPageBanner"
							enctype="multipart/form-data">
							<input type="hidden" id="edit-banner-id" name="id">
							<input type="hidden" id="edit-banner-type" name="bannerType">
							<input id="edit-img-url" name="bannerImageUrl" type="hidden">
							<div class="col-xs-12">
								<div class="form-group">
									<label for="edit-title">제목</label>
									<input id="edit-title" name="title" 
										type="text" class="form-control">
								</div>
								
								<div class="form-group">
									<label for="edit-banner-link-url">Link URL</label>
									<input id="edit-banner-link-url" name="bannerLinkUrl"
										type="text" class="form-control">
								</div>

								<div class="form-group">
									<label for="edit-banner-visible">보임</label>
									<input id ="edit-banner-visible" type="checkbox" name="visible">
								</div>
							</div>
						</form:form>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">취소</button>
				<button id="submit-new-main-banner" type="button" class="btn btn-primary">저장</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<script id="mainPageTemplate" type="text/x-handlebars-template">
{{#each mainPageItems}}
<tr class="selector" data-id="{{id}}">
	<td>
		<img src="{{imgUrl}}" alt="{{title}}" class="img-responsive"/>
	</td>
	<td>
		{{title}}
	</td>
	<td>
		<a href="#" class="btn btn-xs btn-white delete-banner-btn" data-id="{{id}}">
			삭제
		</a>
	</td>
	<td>
		<a href="#" class="btn btn-xs btn-teal edit-banner-item-btn" data-id="{{id}}">
			수정
		</a>
	</td>
</tr>
{{/each}}
</script>

<script type="text/javascript">
	var source   = $("#mainPageTemplate").html();
	var template = Handlebars.compile(source);
	
	$(document).on('click', '.new-banner-btn', function(e){
		var type = $(this).data('type');
		$("#new-banner-type").val(type);
		$("#new-main-banner-modal").modal();
	});
	
	$(document).on('click', '.edit-banner-btn', function(e){
		var type = $(this).data('type');
		
		$.getJSON(baseUrl + "admin/mainPage/getByTypes/" + type, function(data){
			var items = [];
			
			if(data.banners != null && data.banners.length > 0) {
				_.each(data.banners, function(d, i){
					d.imgUrl = baseUrl + baseFileUrl + d.bannerImageUrl;
					items.push(d);
				});
				
				var html    = template({mainPageItems: items});
				$("#main-page-item-table > tbody").html(html);
			}
			$("#edit-main-banner-modal").modal();
		});
	});	
	
	$(document).on('click', '.edit-banner-item-btn', function(e){
		var id = $(this).data('id');
		
		$.getJSON(baseUrl + "admin/mainPage/getById/" + id, function(data){
			
			$("#edit-title").val(data.banner.title);
			$("#edit-banner-link-url").val(data.banner.bannerLinkUrl);
			$("#edit-banner-id").val(data.banner.id);
			$("#edit-banner-type").val(data.banner.bannerType);
			$("#edit-img-url").val(data.banner.bannerImageUrl);
			
			if(data.banner.visible){
				$("#edit-banner-visible").prop('checked', true);
			}else{
				$("#edit-banner-visible").prop('checked', false);
			}
		});
		$("#edit-main-banner-item-modal").modal();
	});	
	
	$(document).on('click', '.delete-banner-btn', function(e){
		e.preventDefault();
		var id = $(this).data('id');
		
		if(confirm("선택하신 배너를 삭제하시겠습니까?")) {
			$.ajax({
				url: baseUrl + "admin/mainPage/" + id,
				contentType : "application/json; charset=utf-8",
				beforeSend: function(xhr) {
					ajaxLoading();
					xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));
				},
				data: JSON.stringify({
					dinnerApplyUser: {
						'id': "<c:url value='${dinnerApplyUser.id}' />",
						'password': $("#currentPassword").val()
					}
				}),
				type: "DELETE",
				dataType: "JSON",
				success: function(data) {
					ajaxUnLoading();
					alert("정상적으로 삭제되었습니다.");
					location.reload();
				},
				error: function(err) {
					ajaxUnLoading();
					alert(err);
				}
			}) // ajax END...
		}
	});
	
	$("#submit-new-main-banner").click(function(e){
		$("#new-main-banner-form")[0].submit();
	});
	$("#submit-edit-main-banner").click(function(e){
		$("#edit-main-banner-form")[0].submit();
	});
	
	$(function(){
		
		$("#sortable-banner-items").sortable({
			stop: function(e,ui){
				var ids = [];

				$("#sortable-banner-items tr").each(function(i){
					ids.push($(this).attr('data-id'));
				})
				$.ajax({
					url: baseUrl + "admin/mainPage/sortable",
					contentType : "application/json; charset=utf-8",
					beforeSend: function(xhr) {
						ajaxLoading();
						xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));
					},
					data: JSON.stringify({
						data : ids
					}),
					type: "POST",
					dataType: "JSON",
					success: function(data) {
						ajaxUnLoading();
						alert("순서가 변경되었습니다.");
					},
					error: function(err) {
						ajaxUnLoading();
						alert(err);
					}
				})
			}
		});
	})
</script>