<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!-- Carousel start-->
<c:if test="${not empty banners  }">
	<c:choose>
		<c:when test="${fn:length(banners) == 1 }">
			<c:forEach var="banner" items="${banners }">
				<img alt="${banner.title }" src="<c:url value="${baseFileUrl }${banner.bannerImageUrl }"/>"
					class="img-responsive"/>
			</c:forEach>
		</c:when>
		<c:otherwise>
			<div id="carousel-banner-type1" class="carousel-fade carousel slide">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<c:forEach var="banner" items="${banners }" varStatus="loop">
						<li data-target="#carousel-banner-type1" data-slide-to="${loop.index }"
							class="<c:if test="${loop.index == 0 }">active</c:if>"></li>
					</c:forEach>
				</ol>

				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
					<c:forEach var="banner" items="${banners }" varStatus="loop">
						<div class="item <c:if test="${loop.index == 0 }">active</c:if>">
							<img alt="${banner.title }" src="<c:url value="${baseFileUrl }${banner.bannerImageUrl }"/>"/>
						</div>
					</c:forEach>
				</div>
			</div>
		</c:otherwise>
	</c:choose>
</c:if>
<!-- Carousel end -->

<div class="banner-btns">
	<button type="button" class="btn btn-white new-banner-btn" data-type="${bannerType }">
		등록
	</button>
	
	<button type="button" class="btn btn-teal edit-banner-btn" data-type="${bannerType }">
		수정
	</button>
</div>