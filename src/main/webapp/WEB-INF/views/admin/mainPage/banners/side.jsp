<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>

<h4>메인페이지 관리</h4>

<div class="collection">
	<c:forEach items="${mainBannerTypes}" var="type">
		<c:url value="/admin/mainPage/banners?type=${type}" var="mainPageBannerUrl" />
		<a href='${mainPageBannerUrl}' data-name="${main_category_name }"
			class="collection-item
			      <c:if test="${currentType == type}">active</c:if> 
				">
		      <spring:message code="mainPageBanner.${type}" />  
		</a>	
		
	</c:forEach>
</div>
