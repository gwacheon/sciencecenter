<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>

<div id="auth-form" class="container">
	<div class="row">
		<div class="col s3">
			<c:import url="/WEB-INF/views/admin/mainPage/banners/side.jsp" />
		</div>
		<div class="col s9">
			<h4>배너 생성</h4>
			<div class="row">
				<c:url var="action" value='/admin/mainPage/banners' />
				<c:url var="method" value='POST' />

				<jsp:include page="/WEB-INF/views/admin/mainPage/banners/form.jsp">
					<jsp:param name="action" value="${action}" />
					<jsp:param name="method" value="${method}" />
				</jsp:include>
			</div>
		</div>
	</div>
</div>