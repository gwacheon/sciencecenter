<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>


<c:url value="/admin/boards" var="createBoardUrl" />
<form:form
	action="${param.action}?${_csrf.parameterName}=${_csrf.token}"
	method="${param.method }" commandName="mainPageBanner" modelAttribute="mainPageBanner"
	enctype="multipart/form-data" class="admin-form">
	
	<form:input type="hidden" path="id"/>
	<input type="hidden" name="bannerType" value="${mainPageBanner.bannerType}">
	
	<div class="input-field col s12">
		<form:input type='text' name='title' path='title'
			autofocus="autofocus" required="required" />
		<form:label path="title">배너 이름</form:label>
	</div>
	
	
	<c:choose>
		<c:when test="${not empty mainPageBanner.bannerImageUrl }">
			<div id="current-main-image-file" class="col s12 m12 l12">
				<form:input type="hidden" path="bannerImageUrl" name="bannerImageUrl"/>
				<p>
					현재 설정된 이미지: 
					<button id="main-image-delete-btn" class="btn red small">삭제</button>
				</p>
				<img src="${mainPageBanner.bannerImageUrl }" class="responsive-img" alt="${mainPageBanner.title }"/>
				
			</div>
			<div id="new-main-image-file">
			
			</div>
		</c:when>
		<c:otherwise>
			<div class="col s12 file-field input-field">
				<div class="btn">
					<span>배너 이미지</span> <input id="bannerImageFile" type="file" name="bannerImageFile">
				</div>
				<div class="file-path-wrapper">
					<input id="bannerImageUrl" class="file-path validate" type="text"
						name='bannerImageUrl'>
				</div>
			</div>
		</c:otherwise>
	</c:choose>

	<!--  상단좌측 슬라이드 배너는 전시물 배너로 추가 입력값이 들어간다. -->
	<c:choose>
		<c:when test="${mainPageBanner.bannerType eq 'mainLeftSlideBanner' }">
			<div class="col s12 m4 input-field">
				<form:select path="buildingName">
					<c:forEach var="i" begin="2" end="5">
						<c:forEach var="j" begin="1" end="9">
							<spring:message var="itemName" code="main.nav.catetory2.sub${i}.sub${j}" scope="application" text='' />
							<spring:message var="bannerLinkUrl" code="main.nav.catetory2.sub${i}.sub${j}.url" scope="application" text='' />
								<c:if test="${not empty itemName }">
									<option value="${itemName }"<c:if test="${i eq '2' && j eq '1' }">selected</c:if>><c:out value="${itemName }" /></option>
								</c:if>
								<c:if test="${not empty itemName }">
									
								</c:if>
						</c:forEach>
					</c:forEach>
				</form:select>
				<form:label path="visible">전시관</form:label>
			</div>
			
			<!-- buildingName에 따른 bannerLinkUrl 값 생성 -->
			<spring:message var="initialBannerLinkUrl" code="main.nav.catetory1.sub2.sub1.url" scope="application" text='' />
			<input type="hidden" id="bannerLinkUrl" name="bannerLinkUrl" value="${initialBannerLinkUrl }" />
			
			<div class="col s12 m4 input-field">
				<form:input type='text' name='exhibitionName' path='exhibitionName' required="required" />
				<form:label path="exhibitionName">전시물 이름</form:label>
			</div>
			
			<div class="col s12 input-field">		
				<form:textarea cols="80" path="description" name="description" rows="10" class="ckeditor"></form:textarea>
				<form:label path="description" class="active">배너 캡션 내용</form:label>
			</div>
		</c:when>
		<c:otherwise>
			<div class="col s12 m4 input-field">
				<form:input type="text" path="bannerLinkUrl"/>
				<form:label path="bannerLinkUrl">배너링크주소</form:label>
			</div>
		</c:otherwise>
	
	</c:choose>
	
	<div class="col s12 m4 input-field">		
		<form:select path="visible">
			<option value="true" <c:if test="${visible eq true }">selected</c:if>>보임</option>
			<option value="false" <c:if test="${visible eq false }">selected</c:if>>숨김</option>
		</form:select>
		<form:label path="visible">숨김여부</form:label>
	</div>
	
	<div class="col s12">
		<div class="actions right-align margin-bottom20">
			<button class="btn waves-effect waves-light" type="submit"
				name="action">저장</button>
		</div>
	</div>
</form:form>

<script type="text/javascript">	
	
	$( function() {
		$('select').material_select();
		
		CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
		
		// Enter Mode without enclosing P tag
		/* $("#description").ckeditor({
			enterMode : CKEDITOR.ENTER_BR,
			shiftEnterMode : CKEDITOR.ENTER_P
		}); */
	});
	
	$('select#buildingName').on('change', function() {
		var bannerLinkUrl = $(this).val();
		$("#bannerLinkUrl").val(bannerLinkUrl);
		
	});
	
	$(document).on("click", "#main-image-delete-btn", function(e) {
		e.preventDefault();
		if( confirm("대표 이미지가 삭제되면, 새로운 이미지를 업로드 해야 합니다. 정말로 삭제하시겠습니까?") ) {
			$('#current-main-image-file').remove();
			var maineImageFilesource = $("#main-image-file-template").html();
			var mainImageFileTemplate = Handlebars.compile(maineImageFilesource);
			$("#new-main-image-file").append(mainImageFileTemplate);
		}
	});
</script>

<script id="file-template" type="text/x-handlebars-template">
	<input type="file" name="attatchments[{{count}}].file">
</script>


<script id="main-image-file-template" type="text/x-handlebars-template">
<div class="col s12 file-field input-field">
	<div class="btn">
		<span>대표이미지</span> <input id="bannerImageFile" type="file" name="bannerImageFile">
	</div>
	<div class="file-path-wrapper">
		<input id="bannerImageUrl" class="file-path validate" type="text" name="bannerImageUrl">
	</div>	
</div>
</script>
