<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<div class="container">
	<div id="admin-board-index" class="margin-bottom30">
		<div class="row">		
			<div class="col s3">
				<c:import url="/WEB-INF/views/admin/mainPage/banners/side.jsp" />
			</div>
			<div class="col s9">
				<c:if test="${currentType eq 'newsLetterBanner' }">
					<blockquote>
				    	주의 : 뉴스레터 배너는 가장 최근에 '보임'으로 설정한 이미지가 화면에 적용됩니다. 
				    </blockquote>
				</c:if>
				<div class="table-responsive margin-bottom30">
					<table>
						<thead>
							<tr>
								<td>
									id
								</td>
								<td>
									제목
								</td>
								<td> 
									사용 유무
								</td>
								<td>
								</td>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${mainPageBanners }" var="banner">
								<tr>
									<td>
										<c:out value="${banner.typeSeq}"></c:out>
									</td>
									<td>
										<c:out value="${banner.title}"></c:out>
									</td>
									<td>
										<c:choose>
											<c:when test="${banner.visible eq true }">
												<i class="material-icons" style="color: green;">&#xE876;</i>
											</c:when>
											<c:otherwise>
												<i class="material-icons" style="color: red;">&#xE14C;</i>
											</c:otherwise>
										</c:choose>
									</td>
									<td>
										<c:url value="/admin/mainPage/banners/${banner.id}/delete" var="deleteBannerUrl" />
										<form:form  action="${deleteBannerUrl}" method="DELETE" data-id="${banner.id }">
											<a href="<c:url value='/admin/mainPage/banners/${banner.id}/edit' />" class="waves-effect waves-light btn white small">수정</a>
											<button type="submit" data-id="${banner.id }" class="delete-btn waves-effect waves-light btn red small">삭제</button>
										</form:form>
										 
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div class="row">
					<div class="col s12 right-align">
						<c:url value="/admin/mainPage/banners/new?type=${currentType }" var="createBannerUrl" /> 	
						<a href="${createBannerUrl}" class="waves-effect waves-light btn">
							배너 생성 - <spring:message code="mainPageBanner.${currentType }" />
						</a>
					</div>
				</div>
			</div>
	  	</div>
  	</div>
</div>
<script type="text/javascript">

	$(".delete-btn").click(function(e){
		e.preventDefault();
		var id = $(this).data("id");
		
		if(confirm("해당 게시물을 정말 삭제 하시겠습니까?")){
			$("form[data-id='" + id + "']").submit();	
		}
	});
	
</script>