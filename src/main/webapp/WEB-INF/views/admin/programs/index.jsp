<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:include page="/WEB-INF/views/admin/programs/group_info.jsp"/>

<div class="container">
	<div class="row margin-bottom30">
		<div class="col s12">
			<table class="striped">
				<thead>
					<th>No</th>
					<th>제목</th>
					<th>기간</th>
					<th>가격</th>
					<th class="center">비고</th>
				</thead>
				
				<tbody>
					<c:forEach var="program" items="${programs }">
						<tr>
							<td>${program.id }</td>
							<td>${program.title }</td>
							<td>
								<fmt:formatDate pattern="yyyy-MM-dd" value="${program.beginTime }" />
								 ~ 
								<fmt:formatDate pattern="yyyy-MM-dd" value="${program.endTime }" />
							</td>
							<td>
								<fmt:formatNumber value="${program.price}"/>
							</td>
							<td class="center">
								<a href="<c:url value='/admin/program_groups/${group.id }/programs/${program.id }'/>"
									class="waves-effect waves-light btn white small">
									상세보기
								</a>
								<a href="<c:url value='/admin/program_groups/${group.id }/programs/${program.id }/edit'/>"
									class="waves-effect waves-light btn white small">
									수정
								</a>
								
								<c:url var="deleteUrl" value="/admin/program_groups/${group.id }/programs/${program.id }" />
								<form:form action="${deleteUrl }" method="delete" class="delete-form-in-table"  data-id="${program.id }">
							        <button type="button" data-id="${program.id }" 
							        	class="delete-btn waves-effect waves-light btn red small">삭제</button>
								</form:form>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		
		<div class="col s12 center">
			<c:import url="pagination.jsp"></c:import>
		</div>
		
		<div class="col s12">
			<a href="<c:url value='/admin/program_groups/${group.id }/programs/new'/>" class="right btn btn-primary">프로그램 등록</a>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$(".delete-btn").click(function(e){
			var id = $(this).data("id");
			
			if(confirm("선택하신 프로그램을 삭제하시겠습니까?")){
				$("form[data-id='" + id + "']").submit();	
			}
		});
	});
</script>