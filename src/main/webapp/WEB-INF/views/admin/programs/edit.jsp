<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container">
	<h5>프로그램 수정</h5>
	
	<div class="row margin-bottom30">
		<c:url var = "action" value='/admin/program_groups/${groupId }/programs/${program.id }'/>
		
		<jsp:include page="/WEB-INF/views/admin/programs/form.jsp">
			<jsp:param name="action" value="${action}"/>
		</jsp:include>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		if("${program.ageType}" != ""){
			$("#${program.ageType}").attr('checked', true);
			$(".age-limit-text").attr('disabled', false);
		}
	});
</script>

<script type="text/javascript">
	
	$(function(){
		$("#locationCategory1").on("change", function(){
			renderSecondCategories();
		});
		
		renderSecondCategories("${program.locationCategory2 }");
	});
</script>