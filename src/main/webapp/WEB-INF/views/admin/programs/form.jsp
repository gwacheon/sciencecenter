<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<form:form id="newHolidayForm" action="${param.action }"
			class="col s12" 
			method="POST" 
			commandName="program" 
			modelAttribute="program">
			
	<form:hidden id="ageLimited" path="ageLimited"/>
	<form:hidden id="ageType" path="ageType"/>
	
	<div class="row">
		<div class="input-field col s8">
			<form:input type="text" path="title" required="true" />
			<form:label path="title">프로그램명</form:label>
		</div>
		
		<div class="input-field col s2">
			<form:input type="number" path="maxMember" />
			<form:label path="maxMember">정원</form:label>
		</div>
		<div class="input-field col s2">
			<form:input type="number" path="waitMember" />
			<form:label path="waitMember">대기인원</form:label>
		</div>
	</div>
	
	<div class="row">
		<div class="input-field col s4">
			<form:input type="number" path="price" />
			<form:label path="price">금액</form:label>
		</div>
	
		<div class="input-field col s4">
			<input class="with-gap age-limit" value="none" name="ageLimit" type="radio" id="noneAge" checked />
			<label class="has-radio" for="noneAge">나이제한 없음</label>
					
			<input class="with-gap age-limit" value="koAge" name="ageLimit" type="radio" id="koAge" />
			<label class="has-radio" for="koAge">한국나이</label>
					
			<input class="with-gap age-limit" value="enAge" name="ageLimit" type="radio" id="enAge" />
			<label class="has-radio" for="enAge">만나이</label>
		</div>
		
		<c:set var="disabled" value="true"/>
		
		<div class="input-field col s2">
			<form:input class="age-limit-text" type="number" path="ageBegin" disabled="${disabled}" />
			<form:label path="ageBegin">나이제한</form:label>
		</div>
		
		<div class="input-field col s2">
			<form:input class="age-limit-text" type="number" path="ageEnd" disabled="${disabled}" />
			<form:label path="ageEnd">나이제한</form:label>
		</div>
	</div>
	
	<div class="row">
		<div class="input-field col s4">
			<form:input type="date" class="datepicker" path="beginTime" />
			<form:label path="beginTime">프로그램 시작일</form:label>
		</div>
		<div class="input-field col s4">
			<form:input type="date" class="datepicker" path="endTime" />
			<form:label path="endTime">프로그램 종료일</form:label>
		</div>
		<div class="input-field col s4">
			<form:input type="number" path="requisitionDay" placeholder=""/>
			<form:label path="requisitionDay">사전접수</form:label>
		</div>
	</div>
	
	<div class="row">
		<div class="input-field col s4">
			<form:select path="locationCategory1">
				<c:forEach var="category" items="${categories }" varStatus="status">
					<c:choose>
						<c:when test="${not empty program.locationCategory1 and program.locationCategory1 == category.key}">
							<option value="${category.key}" selected>${category.key }</option>
						</c:when>
						<c:otherwise>
							<option value="${category.key}">${category.key }</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</form:select>
			<form:label path="locationCategory1">장소분류1</form:label>
		</div>
		
		<div class="input-field col s4">
			<form:select path="locationCategory2">
				<c:forEach var="categoryItem" items="${categories.get(0) }" varStatus="status">
				   <option value="${categoryItem}">${categoryItem }</option>
				</c:forEach>
			</form:select>
			<form:label path="locationCategory2">장소분류2</form:label>
		</div>
		
		<div class="input-field col s4">
			<form:input type="text" path="location" />
			<form:label path="location">장소</form:label>
		</div>
	</div>

	<div class="row">
		<div class="input-field col s12">
			<form:textarea path="content" id="content" class="ckeditor-box"/>
		</div>
	</div>
	
	<div class="row">
		<div class="col s12">부가입력 사항</div>
		<c:forEach var="addInfo" items="${additionalInfos }">
			<div class="col s2">
				<input type="checkbox" id="${addInfo }" data-value="${addInfo }" class="addInfo-checkbox" value="${addInfo }"/>
     			<label for="${addInfo }">${addInfo }</label>
     			<input type="hidden" name="addInfos" data-value="${addInfo }" class="hidden-info-value">
			</div>
		</c:forEach>
	</div>
	
	<div id="program-times">
		<div class="row">
			<div class="col s12">
				<h5>회차입력</h5>
			</div>
		</div>
		
		<c:set var="timeIdx" value="0"/>
		<c:forEach var="time" items="${program.times }" varStatus="loop">
			<div class="row time-row" data-seq="${timeIdx }">
				<div class="input-field col s4">
					<input id="times${timeIdx }.title" name="times[${timeIdx }].title" type="text" value="${time.title }">
					<label for="times${timeIdx }.title" class="">회차명</label>
				</div>
				<div class="input-field col s2">
					<input type="text" class="program-date" id="program-date${timeIdx }" 
						data-seq="${timeIdx }" name="times[${timeIdx }].dateStr" value="${time.dateStr }">
					<label for="program-date-${timeIdx }">일자</label>
				</div>
				<div class="input-field col s2">
					<input type="text" class="program-time begin-time" id="program-begin-time${timeIdx }" 
						data-seq="0" name="times[${timeIdx }].beginTimeStr" value="${time.beginTimeStr }">
					<label for="program-begin-time${timeIdx }">시작시간</label>
				</div>
				<div class="input-field col s2">
					<input type="text" class="program-time end-time" id="program-end-time${timeIdx }" 
						data-seq="${timeIdx }" name="times[${timeIdx }].endTimeStr" value="${time.endTimeStr }">
					<label for="program-end-time${timeIdx }">종료시간</label>
				</div>
				<div class="input-field col s2">
					<a class="waves-effect waves-light btn red delete-item-btn" data-seq="${timeIdx }">
						삭제
					</a>
				</div>
			</div>
			<c:set var="timeIdx" value="${timeIdx + 1 }"/>
		</c:forEach>
	</div>
	
	<div class="row">
		<div class="col s12">
			<a class="waves-effect waves-light btn add-item-btn">
				회차정보 추가
			</a>
		</div>
	</div>

	<div class="row">
		<div class="col s12">
			<button type="submit" class="right btn btn-primary">저장</button>
		</div>
	</div>
</form:form>

<script id="program-time-template" type="text/x-handlebars-template">
	<div class="row time-row" data-seq="{{seq}}">
		<div class="input-field col s4">
			<input id="times{{seq}}.title" name="times[{{seq}}].title" type="text" value="">
			<label for="times{{seq}}.title" class="">회차명</label>
		</div>
		<div class="input-field col s2">
			<input type="text" class="program-date" id="program-date{{seq}}" data-seq="{{seq}}" name="times[{{seq}}].dateStr">
			<label for="program-date{{seq}}">일자</label>
		</div>
		<div class="input-field col s2">
			<input type="text" class="program-time begin-time" id="program-begin-time{{seq}}" data-seq="{{seq}}" name="times[{{seq}}].beginTimeStr">
			<label for="program-begin-time{{seq}}">시작시간</label>
		</div>
		<div class="input-field col s2">
			<input type="text" class="program-time end-time" id="program-end-time{{seq}}" data-seq="{{seq}}" name="times[{{seq}}].endTimeStr">
			<label for="program-end-time{{seq}}">종료시간</label>
		</div>
		<div class="input-field col s2">
			<a class="waves-effect waves-light btn red delete-item-btn" data-seq="{{seq}}">
				삭제
			</a>
		</div>
	</div>
</script>

<script type="text/javascript">
	var currentSeq = "<c:out value='${timeIdx}'/>" || 1;
	
	$(document).on("click", ".add-item-btn", function(){
		var source   = $("#program-time-template").html();
		var context = {seq: currentSeq};
		var template = Handlebars.compile(source);
		var html = template(context);
		
		$("#program-times").append(html);
		
		$('.program-date').pickadate({
			selectMonths : true, // Creates a dropdown to control month
			selectYears: 3, // Creates a dropdown of 15 years to control year
			format: 'yyyy-mm-dd'
		});
		
		$('.program-time').pickatime();
		
		currentSeq++;
	});
	
	$(document).on("click", ".delete-item-btn", function(e){
		e.preventDefault();
		var deleteSeq = $(this).data("seq");
		$(".time-row[data-seq='" + deleteSeq + "']" ).remove();
	});

	$(function(){
		$(".age-limit").change(function(e){
			var value = $(this).val();
			if(value == "none"){
				$("#ageLimited").val("");
				$("#ageType").val("");
				$(".age-limit-text").attr('disabled', true);
			}else{
				$("#ageLimited").val("true");
				$("#ageType").val(value);
				$(".age-limit-text").attr('disabled', false);
			}
		});
		
		$('select').material_select();
		
		$('.datepicker').pickadate({
			selectMonths : true, // Creates a dropdown to control month
			selectYears: 3, // Creates a dropdown of 15 years to control year
			format: 'yyyy-mm-dd'
		});
		
		CKEDITOR.replace('content');
		
		$('.program-date').pickadate({
			selectMonths : true, // Creates a dropdown to control month
			selectYears: 3, // Creates a dropdown of 15 years to control year
			format: 'yyyy-mm-dd'
		});
		
		$('.program-time').pickatime({
			onClose: function() {
				console.log($(this));
				console.log(this);
				console.log('Closed now');
			}
		});
	});
</script>

<script type="text/javascript">
	var locationCategoryMap = {};
	<c:forEach var="category" items="${categories }">
		locationCategoryMap["${category.key}"] = [];
		
		<c:forEach var="categoryItem" items="${category.value}">
			locationCategoryMap["${category.key}"].push("${categoryItem }");
		</c:forEach>
	</c:forEach>
	
	<c:if test="${not empty program.infos}">
		<c:forEach var="addInfo" items="${program.infos }">
			$(".addInfo-checkbox[data-value='${addInfo.title}']").prop('checked', true);
			$(".hidden-info-value[data-value='${addInfo.title}']").val("${addInfo.title}");
		</c:forEach>
	</c:if>
	
	
	
	function renderSecondCategories(beforeValue){
		var locationCategory2Items = locationCategoryMap[$("#locationCategory1").val()];
		var optionsHtml = [];
		
		_.each(locationCategory2Items, function(value){
			if(beforeValue != null && beforeValue == value){
				optionsHtml.push("<option value='" + value + "' selected>" + value + "</option>");
			}else{
				optionsHtml.push("<option value='" + value + "'>" + value + "</option>");	
			}
		});
		
		$("#locationCategory2").html(optionsHtml.join(""));
		$('#locationCategory2').parent().find('.caret').remove();
		$("#locationCategory2").material_select();
	}
	
	
	$(function(){
		$("#locationCategory1").on("change", function(){
			renderSecondCategories(this);
		});
		
		renderSecondCategories();
		
		$(".addInfo-checkbox").on("change", function(){
			var value = $(this).val();
			
			if($(this).is(':checked')){
				$(".hidden-info-value[data-value='" + value + "']").val(value);
			}else{
				$(".hidden-info-value[data-value='" + value + "']").val('');
			}
		});
	});
</script>