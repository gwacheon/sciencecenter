<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container">
	<h5>프로그램 관리</h5>
	
	<div class="row margin-bottom30">
		<div class="col s12">
			<table class="bordered-line">
				<tbody>
					<tr>
						<td class="column-header">카테고리</td>
						<td>${group.category }</td>
						<td class="column-header">프로그램그룹명</td>
						<td>${group.title }</td>
						<td class="column-header">개강년도</td>
						<td>${group.openYear }</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>