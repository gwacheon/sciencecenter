<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<div id="auth-form" class="container">
	<div class="row">
		<div class="card col s12">
			<h4>정보 수정</h4> 
			
			<c:url value="/admin/section/${section.id }" var="updateSectionUrl" />
			<form:form  action="${updateSectionUrl}" method="PUT" commandName="section" modelAttribute="section">
				<div class="input-field col s12">
   					<form:input type='text' path="name" autofocus="autofocus"/>
   					<label for="title">이름</label>
   				</div>
   				<div class="col s12">
   					<form:input type='text' path="phone" />
   					<label for="title">전화번호</label>
   				</div>
   				<div class="col s12">
   					<div class="actions">
						<button class="btn waves-effect waves-light" type="submit" name="action">
				        	정보 수정	
				      	</button>
				    </div>
   				</div>
			</form:form>	
		</div>
	</div>
</div>
