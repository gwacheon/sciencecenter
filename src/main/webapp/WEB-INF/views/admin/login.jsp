<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div id="users-login-wrapper">
	<div id="login-banner">
		<h1 class="doc-title white center">국립과천과학관 관리자 페이지</h1>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div id="login-form">
						<div class="row">
							<div class="col-md-12">
								<h3 class="doc-title teal center margin-bottom20">관리자 로그인</h3>
							</div>
							
							<div class="col-md-8 col-md-offset-2">
								<c:url value="/admin/login" var="loginUrl" />
								<form:form action="${loginUrl}" method="POST" commandName="admin" modelAttribute="admin">
									<div class="form-group">
									    <form:label path="adminId">관리자 아이디</form:label>
				      					<form:input path="adminId" autofocus="autofocus" class="form-control"/>
									</div>
									
									<div class="form-group">
									    <form:label path="password">비밀번호</form:label>
				      					<form:password path="password" class="form-control"/>
									</div>
									
									<div class="form-group center">
										<button class="btn btn-primary wide-btn" type="submit" name="action">
								        	로그인
								      	</button>
									</div>
								</form:form>
							</div>
							
							<div class="col-md-12">
								<div class="line gray margin-bottom30"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>