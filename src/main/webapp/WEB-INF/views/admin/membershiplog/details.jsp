<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h4>
				연간회원 방문 상세 내역
			</h4>
		</div>
		
		<div class="col-sm-12">
			<table class="table">
				<thead>
					<tr>
						<th>MemberNo</th>
						<th>이름</th>
						<th>방문일자</th>
					</tr>
				</thead>
				
				<tbody>
					<c:forEach var="log" items="${logs }">
						<tr>
							<td>${log.memberNo }</td>
							<td>${log.memberName }</td>
							<td>
								<fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${log.regDttm }" />
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			
			<div class="col-sm-12 center">
				<c:import url="pagination.jsp"></c:import>
			</div>
		</div>
	</div>
</div>