<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h4>연간회원 방문 관리</h4>
		</div>
		
		<div class="col-sm-12">
			<div class="form-group">
				<label for="search-name">회원 이름</label>
				<input type="text" name="name" id="search-name" class="form-control">
			</div>
			
			<div class="form-group">
				<button type="button" id="search-btn" class="btn btn-primary">
					검색
				</button>
			</div>
		</div>
		
		<div class="col-sm-12">
			<table id="membership-list-table" class="table">
				<thead>
					<tr>
						<th>이름</th>
						<th>ID</th>
						<th>생년월일</th>
						<th></th>
					</tr>
				</thead>
				
				<tbody>
				
				</tbody>
			</table>
		</div>
	</div>
</div>

<script id="membership-list-template" type="text/x-handlebars-template">
{{#each members}}
<tr>
	<td>{{memberName}}</td>
	<td>{{memberId}}</td>
	<td>{{birthDate}}</td>
	<td>
		<button type="button" class="btn btn-primary reverse btn-sm entrance-btn" 
			data-memberno="{{memberNo}}"
			data-name="{{memberName}}">
			입장처리
		</button>
	</td>
</tr>
{{/each}}
</script>

<script type="text/javascript">
	$("#search-btn").click(function(){
		var name = $("#search-name").val();
		
		if(name != ""){
			$.ajax({
				type : "POST",
				beforeSend: function(xhr) {
		            xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));
		        },
				url : baseUrl + "admin/membershipLog/search",
				data : JSON.stringify({name: name}),
				contentType : 'application/json',
				success : function(data) {
					console.log(data);
					var source   = $("#membership-list-template").html();
					var template = Handlebars.compile(source);
					var html = template({members: data.members});
					
					$("#membership-list-table > tbody").html(html);
				},
				error: function(err){
					alert("문제가 발생하였습니다.");
				}
			});	
		} else {
			alert("이름을 입력해 주세요");
		}
	});
	
	$(document).on("click", ".entrance-btn", function(){
		var memberNo = $(this).data('memberno');
		var name = $(this).data('name');
		
		$.ajax({
			type : "POST",
			beforeSend: function(xhr) {
	            xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));
	        },
			url : baseUrl + "admin/membershipLog/create",
			data : JSON.stringify({name: name, memberNo: memberNo}),
			contentType : 'application/json',
			success : function(data) {
				alert("정상 처리 되었습니다.");
			},
			error: function(err){
				alert("문제가 발생하였습니다.");
			}
		});
	});
</script>