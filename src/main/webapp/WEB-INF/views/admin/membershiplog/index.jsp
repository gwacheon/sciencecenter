<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h4>
				연간회원 방문 통계 페이지
				<a href="<c:url value='/admin/membershipLog/manager'/>" class="btn btn-white">
					관리 페이지로 이동
				</a>
			</h4>
		</div>
		
		<div class="col-sm-12">
			<table class="table">
				<thead>
					<tr>
						<th>MemberNo</th>
						<th>이름</th>
						<th class="text-center">방문횟수</th>
						<th class="text-center">
							비고
						</th>
					</tr>
				</thead>
				
				<tbody>
					<c:forEach var="member" items="${members }">
						<tr>
							<td>${member.memberNo }</td>
							<td>${member.memberName }</td>
							<td class="text-center">${member.cnt }</td>
							<td class="text-center">
								<a href="<c:url value='/admin/membershipLog/${member.memberNo }'/>"
									class="btn btn-sm btn-white">
									상세 내역 조회
								</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			
			<div class="col-sm-12 center">
				<c:import url="pagination.jsp"></c:import>
			</div>
		</div>
	</div>
</div>