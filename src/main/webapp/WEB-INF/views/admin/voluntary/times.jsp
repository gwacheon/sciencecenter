<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container margin-bottom30">
	<h5>봉사활동 상세 일정 관리</h5>
	
	<div class="row margin-bottom30">
		<div class="col-sm-12">
			<table class="table lined-table">
				<tbody>
					<tr>
						<th>제목</th>
						<td>${voluntary.title }</td>
						<th>인원(접수인원)</th>
						<td>
							${voluntary.maxMember }(${voluntary.waitMember })
						</td>
					</tr>
					
					<tr>
						<th>기간</th>
						<td>
							<fmt:formatDate value="${voluntary.beginDate}" pattern="yyyy-MM-dd"/>
							~
							<fmt:formatDate value="${voluntary.endDate}" pattern="yyyy-MM-dd"/>
						</td>
						<th>예약가능일</th>
						<td>
							${voluntary.reserveAvailableDay }일 전부터 예약 가능
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	
	<div class="row margin-bottom30">
		<div class="col-sm-12">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th class="center">회차명</th>
						<th class="center">내용</th>
						<th class="center">시간</th>
						<th class="center">접수인원</th>
						<th class="center">비고</th>
					</tr>
				</thead>
				
				<tbody id="times-wrapper">
					<c:forEach var="time" items="${voluntary.voluntaryTimes }" varStatus="loop">
						<tr class="time-row" data-id="${time.id }">
							<td class="center">${time.title }</td>
							<td class="center">${time.content }</td>
							<td class="center">
								<fmt:formatDate value="${time.beginTime}" pattern="yyyy-MM-dd "/>
								(
								<fmt:formatDate value="${time.beginTime}" pattern="hh:mm a"/>
								~
								<fmt:formatDate value="${time.endTime}" pattern="hh:mm a"/>
								)
							</td>
							<td class="center">
								${time.applicationMembers }
							</td>
							<td class="center">
								<a href="<c:url value='/admin/voluntaries/${voluntary.id }/times/${time.id }'/>" 
									class="btn-white btn volunteers-btn" data-id="${time.id }">
									접수현황
								</a>
								<a href="#" class="btn btn-danger delete-time-btn" data-id="${time.id }">
									삭제
								</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		
		<div>
			<form id="voluntary-time-form">
				<div class="input-field col-sm-3">
					<div class="form-group">
						<label for="voluntary-title" class="">회차명</label>
						<input id="voluntary-title" name="title" type="text" class="form-control">
					</div>
				</div>
				<div class="input-field col-sm-3">
					<div class="form-group">
						<label for="voluntary-content" class="">내용</label>
						<input id="voluntary-content" name="title" type="text" class="form-control">
					</div>
				</div>
				<div class="input-field col-sm-2">
					<div class="form-group">
						<label for="voluntary-date">일자</label>
						<input type="text" class="voluntary-date form-control" id="voluntary-date" name="voluntary-date">
					</div>
				</div>
				<div class="input-field col-sm-2">
					<div class="form-group">
						<label for="voluntary-begin-time">시작시간</label>
						<input type="text" class="voluntary-time begin-time form-control" 
							id="voluntary-begin-time" name="voluntary-begin-time-str">
					</div>
				</div>
				<div class="input-field col-sm-2">
					<div class="form-group">
						<label for="voluntary-end-time">종료시간</label>
						<input type="text" class="voluntary-time end-time form-control" id="voluntary-end-time" 
							name="voluntary-end-time-str">
					</div>					
				</div>
			</form>
		</div>
		
		<div class="col-sm-12">
			<a id="submit-time-btn" class="btn-primary btn right">
				저장
			</a>
		</div>
	</div>
</div>

<script id="voluntary-time-template" type="text/x-handlebars-template">
	<tr class="time-row" data-id="{{id}}">
		<td class="center">{{title}}</td>
		<td class="center">{{content}}</td>
		<td class="center">{{beginDate}} ( {{beginTime}} ~ {{endTime}} )</td>
		<td class="center">0</td>
		<td class="center">
			<a href="#" class="waves-effect waves-light btn red delete-time-btn" data-id="{{id}}">
				삭제
			</a>
		</td>
	</tr>
</script>

<script type="text/javascript">
	$(function(){
		$('.voluntary-date').pickadate({
			selectMonths : true, // Creates a dropdown to control month
			selectYears: 3, // Creates a dropdown of 15 years to control year
			format: 'yyyy-mm-dd',
			min: new Date("<fmt:formatDate value="${voluntary.beginDate}" pattern="yyyy-MM-dd"/>"),
			max: new Date("<fmt:formatDate value="${voluntary.endDate}" pattern="yyyy-MM-dd"/>")
		});
		
		$('.voluntary-time').pickatime();
	});
	
	$("#submit-time-btn").click(function(e){
		e.preventDefault();
		
		var voluntaryTime = {
			voluntaryId: "${voluntary.id}",
			title: $("#voluntary-title").val(),
			content: $("#voluntary-content").val(),
			beginTime: $("#voluntary-date").val() + " " + $("#voluntary-begin-time").val(),
			endTime: $("#voluntary-date").val() + " " + $("#voluntary-end-time").val()
		}
		
		if(confirm("입력하신 회차정보를 등록하시겠습니까?")){
			$.ajax({
				url: baseUrl + "admin/voluntaries/${voluntary.id}/times",
				beforeSend: function(xhr) {
					ajaxLoading();
		            xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));
		        },
		        data : JSON.stringify(voluntaryTime),
		        contentType : 'application/json',
		        type: "POST",
		        dataType: "JSON",
		        success: function(data){
		        	ajaxUnLoading();
		        	if(data.voluntaryTime != null){
		        		var beginMoment = moment(data.voluntaryTime.beginTime);
			        	var endMoment = moment(data.voluntaryTime.endTime);
			        	
			        	data.voluntaryTime.beginDate = beginMoment.format("YYYY-MM-DD");
			        	data.voluntaryTime.beginTime = beginMoment.format("hh:mm A");
			        	data.voluntaryTime.endTime = endMoment.format("hh:mm A");
			        	
			        	var source   = $("#voluntary-time-template").html();
			    		var template = Handlebars.compile(source);
			    		var html = template(data.voluntaryTime);
			    		
			    		$("#times-wrapper").append(html);
		        	}
		        },
		        error: function(err){
		        	ajaxUnLoading();
		        	console.log(err);
		        }
			});
		}
	});
	
	$(document).on("click", ".delete-time-btn", function(e){
		e.preventDefault();
		var id = $(this).data('id');
		
		if(confirm("선택하신 회차를 삭제하시겠습니까? 삭제를 수행하실 경우 해당 회차와 관련된 모든 정보가 삭제됩니다.")){
			$.ajax({
				url: baseUrl + "admin/voluntaries/${voluntary.id }/times/" + id,
				beforeSend: function(xhr) {
					ajaxLoading();
		            xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));
		        },
		        type: "DELETE",
		        dataType: "JSON",
		        success: function(data){
		        	ajaxUnLoading();
		        	if(!data.error){
		        		$(".time-row[data-id='" + id + "']").remove();
		        		alert("정상 삭제 되었습니다.");
		        	}else{
		        		alert("문제가 발생하였습니다. 동일 증상에 계속 발생하면 관리자에게 문의하시기 바랍니다.");
		        	}
		        },
		        error: function(err){
		        	ajaxUnLoading();
		        	console.log(err);
		        }
			});		
		}
	});
	
	
</script>