<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div class="container margin-bottom30">
	<h5>봉사활동 상세관리</h5>
	
	<div class="row margin-bottom30">
		<div class="col-sm-12 margin-bottom30">
			<table class="table lined-table">
				<tbody>
					<tr>
						<th>제목</th>
						<td>${voluntary.title }</td>
						<th>접수인원</th>
						<td>
							${fn:length(voluntaryTime.volunteers)} / ${voluntary.waitMember }
						</td>
					</tr>
					
					<tr>
						<th>기간</th>
						<td>
							<fmt:formatDate value="${voluntary.beginDate}" pattern="yyyy-MM-dd"/>
							~
							<fmt:formatDate value="${voluntary.endDate}" pattern="yyyy-MM-dd"/>
						</td>
						<th>예약가능일</th>
						<td>
							${voluntary.reserveAvailableDay }일 전부터 예약 가능
						</td>
					</tr>
					<tr>
						<th>상세일정</th>
						<td>
							<fmt:formatDate value="${voluntaryTime.beginTime}" pattern="yyyy-MM-dd HH:mm"/>
							~
							<fmt:formatDate value="${voluntaryTime.endTime}" pattern="HH:mm"/>
							
							<c:choose>
								<c:when test="${timeStatus == 'regist' }">
									(접수 진행중)
								</c:when>
								
								<c:when test="${timeStatus == 'complete' }">
									(봉사활동 마감)
								</c:when>
							</c:choose>
						</td>
						<th>EXCEL</th>
						<td>
							<a href="<c:url value='/admin/voluntaries/${voluntary.id}/times/${voluntaryTime.id }/excel'/>"
								class="btn btn-primary download-excel-btn" data-id="${voluntaryTime.id }">
								다운로드
							</a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		
		<div class="col-sm-12">
			<table class="table striped centered">
				<thead>
					<tr>
						<th>이름</th>
						<th>연락처</th>
						<th>이메일</th>
						<th>상태</th>
						<th>비고</th>
					</tr>
				</thead>
				
				<tbody>
					<c:forEach var="volunteer" items="${voluntaryTime.volunteers }">
						<tr class="volunteer-row" data-id="${volunteer.id }">
							<td>${volunteer.name }</td>
							<td>${volunteer.phone }</td>
							<td>${volunteer.email }</td>
							<td class="status">
								<c:choose>
									<c:when test="${volunteer.status == 'pending' }">
										대기
									</c:when>
									<c:when test="${volunteer.status == 'accept' }">
										수락
									</c:when>
									<c:when test="${volunteer.status == 'reject' }">
										거절
									</c:when>
									<c:when test="${volunteer.status == 'completion' }">
										수료
									</c:when>
									<c:when test="${volunteer.status == 'return' }">
										반려
									</c:when>
								</c:choose>
							</td>
							<td>
								<button type="button" class="btn btn-white introduction-btn" data-id="${volunteer.id }">
									지원서
								</button>
								
								<c:choose>
									<c:when test="${timeStatus == 'regist' }">
										<c:choose>
											<c:when test="${volunteer.status == 'pending' }">
												<button type="button" class="btn status-btn accept-btn" data-id="${volunteer.id }" data-status='accept'>
													수락
												</button>
												<button type="button" class="btn status-btn btn-danger reject-btn" data-id="${volunteer.id }" data-status='reject'>
													거절
												</button>
											</c:when>
											<c:when test="${volunteer.status == 'accept' }">
												<button type="button" class="btn status-btn accept-btn" data-id="${volunteer.id }" data-status='accept' style="display: none">
													수락
												</button>
												<button type="button" class="btn status-btn btn-danger reject-btn" data-id="${volunteer.id }" data-status='reject'>
													거절
												</button>
											</c:when>
											<c:when test="${volunteer.status == 'reject' }">
												<button type="button" class="btn status-btn accept-btn" data-id="${volunteer.id }" data-status='accept'>
													수락
												</button>
												<button type="button" class="btn btn-danger status-btn reject-btn" data-id="${volunteer.id }" data-status='reject' style="display: none">
													거절
												</button>
											</c:when>
										</c:choose>
									</c:when>
									<c:when test="${timeStatus == 'complete' }">
										<c:choose>
											<c:when test="${volunteer.status == 'accept' }">
												<button type="button" class="btn status-btn completion-btn" data-id="${volunteer.id }" data-status='completion'>
													수료
												</button>
												<button type="button" class="btn status-btn red return-btn" data-id="${volunteer.id }" data-status='return'>
													반려
												</button>
											</c:when>
											
											<c:when test="${volunteer.status == 'completion' }">
												<a href="<c:url value='/voluntary/${volunteer.id }/certificate'/>" 
													class="btn show-certificate" data-id="${volunteer.id }"
													target="_blank">
													수료증 조회
												</a>
											</c:when>
										</c:choose>
									</c:when>
								</c:choose>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>

<!-- Modal Structure -->
<div id="introduction-modal" class="modal modal-fixed-footer">
	
</div>

<script id="introduction-template" type="text/x-handlebars-template">
	<div class="modal-content">
		<h4>봉사활동 지원서</h4>
		<table class="table lined-table margin-bottom30">
			<tbody>
				<tr>
					<th>이름</th>
					<td>{{name}}</td>

					<th>이메일</th>
					<td>{{email}}</td>
				</tr>
				<tr>
					<th>연락처</th>
					<td>{{phone}}</td>
				
					<th>학교</th>
					<td>{{school}}</td>
				</tr>
				<tr>
					<th>학년 / 반 / 번호</th>
					<td>{{grade}}학년 {{groupNo}}반 {{myNo}}번</td>
				
					<th>1365 ID</th>
					<td>{{otherId}}</td>
				</tr>
			</tbody>
		</table>
		
		<div class="col-sm-12" style="white-space: pre">{{introduction}}</div>
	</div>

	

	<div class="modal-footer">
		{{#compare status 'pending' operator="=="}}
			<button type="button" class="btn accept-btn status-btn"  data-id="{{id}}" style="margin-left: 10px;">
				수락
			</button>
			<button type="button" class="btn btn-danger accept-btn status-btn" data-id="{{id}}" style="margin-left: 10px;">
				거절
			</button>
		{{/compare}}
		<a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat " style="margin-left: 10px;">
			닫기
		</a>		
	</div>
</script>

<script type="text/javascript" src="<c:url value='/js/volunteer.js'/>"></script>
<script type="text/javascript">
	$(document).on('click', '.introduction-btn', function(e){
		e.preventDefault();
		
		var volunteerId = $(this).data('id');
		var volunteer = new Volunteer({ id: volunteerId });
		
		$.getJSON(baseUrl + "admin/voluntaries/volunteers/" + volunteerId, function(data){
			volunteer.setVolunteer(data.volunteer);
			$("#introduction-modal").html(volunteer.render());
			
			$("#introduction-modal").openModal();
		});
	});
	
	$(document).on('click', '.status-btn', function(e){
		e.preventDefault();
		
		var volunteer = new Volunteer({ id: $(this).data('id') });
		volunteer.saveStatus($(this).data('status'));
	});
	
	$(".random-cast-btn").click(function(e){
		if(confirm("선정되지 않은 인원중 랜덤으로 선정을 하시겠습니까?")){
			$.ajax({
				url: baseUrl + "admin/voluntaries/${voluntary.id}/times/${voluntaryTime.id}/randomCast",
				type: "GET",
				beforeSend: function(){
					ajaxLoading();
				},
				success: function(data){
					ajaxUnLoading();
					console.log(data);
					if(data.castedVolunteers == null || data.castedVolunteers.length == 0){
						alert("반영 된 사항 없음(현재 정원 등록 완료된 상태입니다.)");
					}else{
						alert(data.castedVolunteers.length + "명의 인원이 추첨되었습니다.");
						location.reload();
					}
				},
				error: function(err){
					ajaxUnLoading();
					console.log(err);
				}
			});
		}
	});
</script>