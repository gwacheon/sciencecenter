<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="container">
	<h4 class="page-title">${voluntary.title } 수정</h4>
	
	<div class="row margin-bottom30">
		<c:url var = "action" value='/admin/voluntaries/${voluntary.id }'/>
		<c:url var = "method" value='PUT'/>
		
		<jsp:include page="/WEB-INF/views/admin/voluntary/form.jsp">
			<jsp:param name="action" value="${action}"/>
			<jsp:param name="method" value="${method}"/>
		</jsp:include>
	</div>
</div>