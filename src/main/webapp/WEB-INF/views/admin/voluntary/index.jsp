<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container">
	<h5>봉사활동 관리</h5>
	
	<div class="row margin-bottom30">
		<div class="col-sm-12">
			<table class="table lined-table margin-bottom30">
				<thead>
					<tr>
						<th class="center">제목</th>
						<th class="center">기간</th>
						<th class="center">비고</th>
					</tr>
				</thead>
				
				<c:forEach var="voluntary" items="${voluntaries }">
					<tr>
						<td class="center">${voluntary.title }</td>
						<td class="center">
							<fmt:formatDate value="${voluntary.beginDate}" pattern="yyyy-MM-dd"/>
							~
							<fmt:formatDate value="${voluntary.endDate}" pattern="yyyy-MM-dd"/>
						</td>
						<td class="center">
							<a href="<c:url value='/admin/voluntaries/${voluntary.id }/times'/>" class="btn btn-white">
								상세보기
							</a>
							<a href="<c:url value='/admin/voluntaries/${voluntary.id }/edit'/>" class="btn btn-white">
								수정
							</a>
							<a href="#" class="btn btn-danger delete-btn"
								data-id="${voluntary.id }">
								삭제
							</a>
						</td>
					</tr>
				</c:forEach>
			</table>
			
			<div class="right">
				<a href="<c:url value='/admin/voluntaries/new'/>" class="btn btn-primary">
					신규 봉사활동 등록하기
				</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$(".delete-btn").click(function(e){
			var id = $(this).data('id');
			
			if(confirm("선택하신 봉사활동을 삭제하시겠습니까?")) {
				$.ajax({
					url: baseUrl + "admin/voluntaries/" + id,
					type: "DELETE",
					beforeSend: function(xhr) {
			            xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));
			            ajaxLoading();
			        },
					success: function(data){
						if(data.isDeleted) {
							location.reload();
						}
						
						ajaxUnLoading();
					},
					error: function(err) {
						ajaxUnLoading();
						console.log(err);
					}
				});
			}
		});
	});
</script>