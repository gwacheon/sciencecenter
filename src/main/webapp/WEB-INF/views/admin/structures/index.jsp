<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="container">
	<h5>대관시설물 관리</h5>
	
	<div class="row margin-bottom30">
		<div class="col-sm-12 table-responsive">
			<table class="table centered-table lined-table margin-bottom30 structure-price-table">
				<thead>
					<tr>
					    <th width="15%" rowspan="2" class="center"></th>
					    <th rowspan="2" class="center">구분</th>
						<th rowspan="2" class="center">시설명</th>
						<th rowspan="2" class="center">면적(m<sup>2</sup>)</th>
						<th class="center" colspan="3">대관료 최소금액</th>
						<th rowspan="2" class="center">담당자</th>
						<th rowspan="2" class="center">비고</th>
						<th rowspan="2" class="center"></th>
					</tr>
					
					<tr>
						<th>봄ㆍ가을철</th>
						<th>여름철</th>
						<th>겨울철</th>
					</tr>
				</thead>
				
				<c:forEach var="structure" items="${structures }">
					<tr>
					   <td class="center" width="15%">
					   		<c:url var="pictureUrl" value="${baseFileUrl }${structure.picture }"/>
                            <img alt="${structure.title }" src="${pictureUrl }"
                            	class="img-responsive materialboxed">
                            ${structure.description }
                        </td>
                        <td class="center">
                        	<spring:message code="structure.category.${structure.category }" text="" />
						</td>
						<td class="center">
							${structure.title }
						</td>
						<td class="center">
							${structure.area }
						</td>
						<td class="center">
							<fmt:formatNumber value="${structure.priceSpring}" />
						</td>
						<td class="center">
							<fmt:formatNumber value="${structure.priceSummer}" />
						</td>
						<td class="center">
							<fmt:formatNumber value="${structure.priceWinter}" />
						</td>
						<td class="center">
							${structure.person }
						</td>
						<td class="center">
							<span style="white-space:pre"><c:out value="${structure.etc }" escapeXml="false" /></span>
						</td>
						<td class="center">
							<a href="<c:url value='/admin/structures/${structure.id }/edit'/>"
								class="btn btn-white">
								수정
							</a>
							
							<a href="<c:url value='/admin/structures/${structure.id }/calendars'/>"
								class="btn btn-white btn-sm">
								일정관리
							</a>
							
							<c:url var="deleteUrl" value="/admin/structures/${structure.id}" />
							<form:form id="delete-structure-form-${structure.id }" action="${deleteUrl }" method="delete" class="delete-form-in-table"  data-id="${program.id }">
						        <button type="button" data-id="${structure.id }" 
						        	class="delete-btn btn btn-sm btn-danger">삭제</button>
							</form:form>
						</td>
					</tr>
				</c:forEach>
			</table>
			
			<div class="right">
				<a href="<c:url value='/admin/structures/new'/>" class="btn btn-primary">
					신규 대관시설 등록하기
				</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$(".delete-btn").click(function(e){
			var id = $(this).data("id");
			
			if(confirm("선택하신 대관시설을 삭제하시겠습니까?")){
				$("#delete-structure-form-" + id).submit();	
			}
		});
	});
</script>