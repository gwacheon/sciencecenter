<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container">
	<h5>대관 일정 관리</h5>
	<div class="row margin-bottom30">
		<div class="col-sm-12">
			<table class="table lined-table">
				<tr>
					<th class="center">구분</th>
					<td>${structure.title }</td>
					<th class="center">면적</th>
					<td>${structure.area } 면적(m<sup>2</sup>)</td>
					<th class="center">담당자</th>
					<td>${structure.person }</td>
				</tr>
			</table>
		</div>
	</div>
	
	<div class="row margin-bottom30">
		<div class="col-sm-12">
			<div id="structure-unavailable-calendars">
			
			</div>
		</div>
	</div>
	
	<div class="row">
		<form class="col-sm-12">
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<label for="unavailable-date">날짜</label>
						<input id="unavailable-date" placeholder="YYYY-MM-DD" type="text" 
							class="unavailable-date form-control">
					</div>
				</div>
				
				<div class="col-sm-2">
					<div class="form-group">
						<label for="begin-time">시작시간</label>
						<input id="begin-time" type="text" placeholder="HH:mm" 
							class="unavailable-time form-control" name="beginTime">
					</div>
				</div>
				
				<div class="col-sm-2">
					<div class="form-group">
						<label for="end-time">종료시간</label>
						<input id="end-time" type="text" placeholder="HH:mm" 
							class="unavailable-time form-control" name="endTime">
					</div>
				</div>
				
				<div class="col-sm-12">
					<div class="form-group">
						<button type="button" id="unavailable-confirm-btn"
							class="btn btn-primary">
							신청 불가 날짜 추가
						</button>
					</div>
				</div>
			</div>
		</form>
		
		<div class="col-sm-12">
			
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		var structureCalendar = new StructureCalendar({
			date: moment(new Date()),
			container: "#structure-unavailable-calendars",
			template: "#structure-calendar-template",
			url: "<c:url value='/admin/structures/${structure.id}/calendars/dates'/>",
			hasAdminRole: true
		});
		
		$('.unavailable-date').pickadate({
			selectMonths : true, // Creates a dropdown to control month
			selectYears: 3, // Creates a dropdown of 15 years to control year
			format: 'yyyy-mm-dd'
		});
		
		$('.unavailable-time').pickatime({
			format: "HH:i",
			min: [8,0],
		  	max: [20,0]
		});
	});	
</script>

<script id="structure-calendar-template"
	type="text/x-handlebars-template">

<div class="month-selector" style="text-align: center">
	<a id="prev-month" class="month-selector" href="#" data-date="{{prevMonth}}">
		&lt;
	</a>

	<div id="selected-month" style="display: inline-block">
		{{formatedDate selectedDate "YYYY.MM"}}
	</div>

	<a id="next-month" href="#" class="month-selector" data-date="{{nextMonth}}">
		&gt;
	</a>
</div>

<div id="structure-calendar" class="calendar-area">
	<table id="calendar-table" class="ui celled table unstackable">
		<thead>
			<tr>
				<th class="center">일</th>
				<th class="center">월</th>
				<th class="center">화</th>
				<th class="center">수</th>
				<th class="center">목</th>
				<th class="center">금</th>
				<th class="center">토</th>
			</tr>
		</thead>
		<tbody class="calendar-body">
			{{#each dates}}
				<tr>
					{{#each this}}
						<td class="center date {{#if this.disable}}disabled{{else}}available{{/if}}" 
							data-date="{{formatedDate this.date "YYYY-MM-DD"}}">
							<div class="circle admin">
								{{formatedDate this.date "DD"}}
							</div>
							<div class="unavailable-times">
								
							</div>
						</td>
					{{/each}}
				</tr>
			{{/each}}
		</tbody>
	</table>
</div>
</script>