<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="container">
	<h5>대관시설 등록</h5>
	
	<div class="row margin-bottom30">
		<c:url var = "action" value='/admin/structures/${structure.id }'/>
		<c:url var = "method" value='POST'/>
		
		<jsp:include page="/WEB-INF/views/admin/structures/form.jsp">
			<jsp:param name="action" value="${action}"/>
			<jsp:param name="method" value="${method}"/>
		</jsp:include>
	</div>
</div>