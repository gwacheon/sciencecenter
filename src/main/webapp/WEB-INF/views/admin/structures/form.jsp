<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<form:form id="newStructureForm"
	action="${param.action }?${_csrf.parameterName}=${_csrf.token}"
	class="col-sm-12" method="${param.method }" commandName="structure"
	modelAttribute="structure" enctype="multipart/form-data">
	<div class="row">
		<div class="col-sm-3">
			<div class="form-group">
				<form:label path="category">구분</form:label>
				<select name="category" id="category" class="form-control">
					<c:forEach var="category" items="${categories }">
						<option value="${category }" <c:if test="${category == structure.category }">selected</c:if>><spring:message code="structure.category.${category }"/></option>
					</c:forEach>
				</select>
			</div>
		</div>
		
		<div class="col-sm-3">
			<div class="form-group">
				<form:label path="title">시설명</form:label>
				<form:input type="text" path="title" required="true" class="form-control"/>
			</div>
		</div>
		<div class="col-sm-2">
			<div class="form-group">
				<form:label path="person">담당자</form:label>
				<form:input type="text" path="person" required="true" class="form-control"/>
			</div>
		</div>
		
		<div class="col-sm-2">
			<form:label path="area">면적(m<sup>2</sup>)</form:label>
			<form:input type="number" path="area" step="any" class="form-control"/>
		</div>
		
		<div class="col-sm-2">
			<form:label path="area">면적(m<sup>2</sup>)</form:label>
			<form:input type="number" path="area" step="any" class="form-control"/>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label for="file">대표이미지</label>
				<input id="file" type="file" name="file" accept="image/*">
				
				<div class="file-path-wrapper">
					<input id="picture" class="file-path hidden" type="text"
						name='picture'>
				</div>
				<c:if test="${not empty structure.picture and structure.picture != '' }">
					<div class="row">
						<div class="col-sm-4">
							<c:url var="pictureUrl" value="${baseFileUrl }${structure.picture }"/>
							<img alt="${structure.title }" src="${pictureUrl }" class="img-responsive">
						</div>
					</div>
				</c:if>
			</div>
		</div>
		
		<div class="col-sm-6">
			<div class="form-group">
				<form:label path="description">상세설명</form:label>
				<form:input type="text" path="description" class="form-control"/>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-4">
			<div class="form-group">
				<form:label path="priceSpring">봄/가을철</form:label>
				<form:input type="number" path="priceSpring" required="true" class="form-control" />
			</div>
		</div>
		<div class="col-sm-4">
			<div class="form-group">
				<form:label path="priceSummer">여름철</form:label>
				<form:input type="number" path="priceSummer" required="true" class="form-control" />
			</div>
		</div>
		<div class="col-sm-4">
			<div class="form-group">
				<form:label path="priceWinter">겨울철</form:label>
				<form:input type="number" path="priceWinter" required="true" class="form-control" />
			</div>
		</div>
	</div>

	<div class="row">
		<div class="input-field col-sm-12">
			<div class="form-group">
				<form:label path="etc">비고</form:label>
				<form:textarea path="etc" class="form-control" />
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<button id="submit-btn" type="submit" class="right btn btn-primary">저장</button>
		</div>
	</div>
</form:form>

<script type="text/javascript">
	$(document).ready(function() {
		$('select').material_select();
		
		$("#newStructureForm").submit(function(){
			$("#submit-btn").prop("disabled", true);
		});
	});
</script>
