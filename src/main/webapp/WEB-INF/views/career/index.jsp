<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="sub-content-wrapper">
	<div class="sub-content-nav scrollspy-teal">
		<div class="container">
			<div class="table-of-contents">
				<a href="#basic-info-spy" class="item">
					과정안내 
				</a>
			
				<a href="#basic-map-spy" class="item">
					교육비
				</a>
				<a href="#major-exhibitions-spy" class="item">
					프로그램 일정
				</a>
				<div class="bread-crumbs">
					과학교육 / 진로탐구
				</div>
			</div>
		</div>
	</div>
</div>

<div class="sub-body">
	<div class="container">
		<div class="row margin-bottom30">
			<div id="careers-wrapper" class="col s12">
			
			</div>
		</div>
	</div>
</div>

<script id="career-template" type="text/x-handlebars-template">
<table class="career-table">
	<thead>
		<tr>
			<th colspan="4">{{title}}</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			{{#each dates}}
				<td>{{dateStr}} ({{date}})</td>
			{{/each}}
		</tr>

		{{#each schedules}}
			<tr>
				<td class="{{#if tueAvailable}}available{{else}}unavailable{{/if}}">
					{{tueWord}}
				</td>
				<td class="{{#if wedAvailable}}available{{else}}unavailable{{/if}}">
					{{wedWord}}
				</td>
				<td class="{{#if thuAvailable}}available{{else}}unavailable{{/if}}">
					{{thuWord}}
				</td>
				<td class="{{#if friAvailable}}available{{else}}unavailable{{/if}}">
					{{friWord}}
				</td> 
			</tr>
		{{/each}}
	</tbody>
</table>
</script>
<script type="text/javascript" src="<c:url value='/js/career.js'/>"></script>
<script type="text/javascript">
var careers = [];
<c:forEach var="career" items="${careers }">
	var career = new Career("${career.id }", "${career.title }", "<fmt:formatDate pattern="yyyy-MM-dd" value="${career.beginDate }" />");
	
	<c:forEach var="s" items="${career.schedules }">
		var schedule = new Schedule(
			"${s.id }",
			"${s.tueWord }", "${s.tueAvailable }",
			"${s.wedWord }", "${s.wedAvailable }",
			"${s.thuWord }", "${s.thuAvailable }",
			"${s.friWord }", "${s.friAvailable }"
		);
		
		career.addSchedule(schedule);
	</c:forEach>
	
	careers.push(career);
</c:forEach>	

$(function(){
	_.forEach(careers, function(career, i){
		career.render();
	});
});
</script>