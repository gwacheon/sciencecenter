<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!-- container 아래 출력할 것. -->
<script src="<c:url value='/resources/js/board.js'/> "></script>

<script type="text/javascript">
Handlebars.registerHelper('encode', function(url) {
  return encodeURI(url);
});
</script>

<script id="board-show-template" type="text/x-handlebars-template">
    <div class="board-show-wrapper row">
        <div class="col-md-12">
            <div class="board-body">
					<div class="header-item-wrapper">
		                <div class="row">
                        	<div class="col-md-9">
								<div class="header-item title">
									<div class="row">
										<div class="col-xs-3 col-sm-2 col-md-2">
											<div class="header">
												제목										
											</div>
										</div>
										<div class="col-xs-9 col-sm-10 col-md-10">
											<div class="value">
												{{board.title}}
												{{#if board.subTitle }}
													<span class="sub-value">(부제 : {{board.subTitle}})</span>
												{{/if }}									
											</div>
										</div>
									</div>
								</div>   	
							</div>
							<div class="col-md-3">
								<div class="header-item">
									<div class="row">
										<div class="col-xs-3 col-sm-2 col-md-4">
											<div class="header">
												등록일
											</div>
										</div>
										<div class="col-xs-9 col-sm-10 col-md-8">
											<div class="value">
												{{formatedDate board.regDttm "YYYY-MM-DD"}}									
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
                    </div>
                
					<div class="header-item-wrapper">
                    	<div class="row">
                        	<div class="col-sm-6 col-md-9">
								<div class="header-item">
									<div class="row">
										<div class="col-xs-3 col-sm-2 col-md-2">
											<div class="header">
												작성자
											</div>
										</div>
										<div class="col-xs-9 col-sm-10 col-md-10">
											<div class="value">
												{{board.member.memberName}}
											</div>                            		
										</div>
									</div>
								</div>
                        	</div>
                        	<div class="col-sm-6 col-md-3">
								<div class="header-item">
									<div class="row">
										<div class="col-xs-3 col-sm-2 col-md-4">
											<div class="header">
												조회수
											</div>
										</div>
										<div class="col-xs-9 col-sm-10 col-md-8">
											<div class="value">
												{{board.count}}
											</div>								
										</div>
									</div>
								</div>
                        	</div>
                    	</div>
					</div>

					<div class="header-item-wrapper">
                    	<div class="row">
                        	<div class="col-md-9">
								<div class="header-item">
									<div class="row">
										<div class="col-xs-3 col-sm-2 col-md-2">
											<div class="header">
												첨부파일
											</div>
										</div>
										<div class="col-xs-9 col-sm-10 col-md-10">
											<div class="value">		
												{{#if board.attatchments}}
													{{#each board.attatchments}}												
															<i class="material-icons">&#xE873;</i>
															<a href="{{../baseUrl}}/downloadRequest?filePath={{{encode url}}}" class="file-link">{{name}}</a>
															<br>
													{{/each}}
												{{/if}}
											</div>                            		
										</div>
									</div>
								</div>
                        	</div>
                    	</div>
					</div>
					{{#if board.videoPath}}
					<div class="header-item-wrapper">
                    	<div class="row">
                        	<div class="col-md-9">
								<div class="header-item">
									<div class="row">
										<div class="col-xs-3 col-sm-2 col-md-2">
											<div class="header">
												동영상
											</div>
										</div>
										<div class="col-xs-9 col-sm-10 col-md-10">
											<div class="value">		
												<video autoplay="" loop="" controls="" style="width : 100%">
													<source src="{{../baseUrl}}{{../baseFileUrl}}{{board.videoPath}}" type="video/mp4">
												</video>		
											</div>                            		
										</div>
									</div>
								</div>
                        	</div>
                    	</div>
					</div>
					{{/if}}
                
                <div class="row">
                    <div class="col-md-12">
						<div class="contents">
                        	{{{board.content}}}
						</div>
                    </div>
                </div>
             </div>
        </div>
        <div class="board-list-btn-wrapper col-md-12">
            {{#if isMyBoard}}
				<div class="edit-btns">
				<a href="{{url}}/private/{{board.id}}/edit" class="btn btn-primary btn-reverse">
                	수정
           		</a>
				<a href="#" class="btn btn-primary btn-reverse board-delete-btn" data-id="{{board.id}}">
                	삭제
           		</a>
				</div>
			{{/if}}
			<a href="{{url}}" class="btn btn-primary">
                글 목록
            </a>
        </div>
        <div class="margin-bottom20">
                <div class="col-md-12">
                    <div class="next-prev-board-wrapper">
                        <div class="board">
                            <div class="header">
                                <i class="fa fa-angle-up"></i> 다음글
                            </div>
                            <div class="title">
                                    {{#if nextBoard}}
                                    <a href= "{{url}}/{{nextBoard.id}}">
                                        {{nextBoard.title}}
                                    </a>
                                    {{else}}
                                        다음 글이 존재하지 않습니다.
                                    {{/if}}
                            </div>
                        </div>
                        <div class="board">
                            <div class="header">
                                <i class="fa fa-angle-down"></i> 이전글
                            </div>
                            <div class="title">
                                {{#if prevBoard}}
                                <a href= "{{url}}/{{prevBoard.id}}">
                                    {{prevBoard.title}}
                                </a>
                                {{else}}
                                    이전 글이 존재하지 않습니다.
                                {{/if}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</script>