<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="row gallery-boards">
	<c:forEach var="board" items="${boards }">
		<div class="col-sm-6 col-md-4">
			<div class="board-gallery-item">
				<c:choose>
					<c:when test="${boardType.urlType }">
						<a target="_blank" href="${board.linkUrl }">
							<div class="board-img">
								<img src="<c:url value='${baseFileUrl}' />${board.picture }" class="img-responsive" alt="${board.title }"/>
                            </div>
                            <div class="board-title">
                            	${board.title }
                            	<div class="gallery-board-date">
                            		<fmt:formatDate pattern="yyyy-MM-dd" value="${board.regDttm }" />
                            	</div>
                            </div>
						</a>
					</c:when>
					<c:otherwise>
						<a href="${param.path }/${board.id }">
							<div class="board-img">
								<img src="<c:url value='${baseFileUrl}' />${board.picture }" class="img-responsive" alt="${board.title }"/>
                            </div>
                            <div class="board-title">
                            	${board.title }
                            	<c:if test="${currentCategory eq 'main.nav.category6' }">
	                            	<div class="gallery-board-date">
	                            		<fmt:formatDate pattern="yyyy-MM-dd" value="${board.regDttm }" />
	                            	</div>
                            	</c:if>
                            	<c:if test="${boardType.hasSubTitle }">
                            		<div class="gallary-sub-title">
                            			${board.subTitle }
                            		</div>
                            	</c:if>
                            </div>
						</a>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</c:forEach>
</div>

<div class="pagination-wrapper">
	<ul class="pagination">
		<c:if test="${page.beginPage > 1 }">
			<li>
				<a href="${param.path }?page=1" data-page="1" class="page-link {{page.className}}">
					<i class="fa fa-chevron-left"></i>
				</a>
			</li>
			<li>
				<a href="${param.path }?page=${page.beginPage - 1}" data-page="${page.beginPage - 1}" class="page-link"><i class="fa fa-ellipsis-h"></i></a>
			</li>
		</c:if>
		
		<c:forEach var="i" begin="${page.beginPage }" end="${page.endPage }" varStatus="loop">
			<c:choose>
				<c:when test="${i == page.currentPage }">
					<li class="active">
						<a href="#" class="page-link">${i }</a>
					</li>
				</c:when>
				<c:otherwise>
					<li>
						<a href="${param.path }?page=${i }" class="page-link">${i }</a>
					</li>
				</c:otherwise>
			</c:choose>
		</c:forEach>
		
		<c:if test="${page.endPage < page.totalPages }">
			<li>
				<a href="${param.path }?page=${page.endPage + 1}" data-page="${page.endPage + 1}" class="page-link"><i class="fa fa-ellipsis-h"></i></a>
			</li>
			<li>
				<a href="${param.path }?page=${page.endPage }" data-page="${page.endPage }" class="page-link"><spring:message code="page.last" text=""/>
					<i class="fa fa-chevron-right"></i>
				</a>
			</li>
		</c:if>
	</ul>
</div>