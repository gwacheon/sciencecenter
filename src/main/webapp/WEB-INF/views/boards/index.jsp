<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<script src="<c:url value='/resources/js/board.js'/> "></script>
<script id="board-list-template" type="text/x-handlebars-template">
	<div class="list-board-wrapper table-responsive">
			<table class="table">
				{{#compare boardType.name "scienceColumn" operator="=="}}
					<thead>
						<tr>
							<th>
								번호
							</th>
							<th>
								제목
							</th>
							<th>
								전시관
							</th>
							<th>
								전시물
							</th>
							<th>
								발행일
							</th>
							<th>
								작성자
							</th>
						</tr>
					</thead>
				{{else}}
					<thead>
						<tr>
							<th>
								번호
							</th>
							<th style="width:50%">
								제목
							</th>
							<th>
								작성자
							</th>
							<th>
								등록일
							</th>
							<th>
								조회수
							</th>
							<th>
								첨부파일
							</th>
						</tr>
					</thead>
				{{/compare}}
				{{#compare boardType.name "scienceColumn" operator="=="}}
					<tbody>
						{{#each boards}}
							<tr>
								<td>
									{{id}}
								</td>
								<td>
                                    <a href="{{link_url}}" target="_blank">{{title}}</a>
								</td>
								<td>
									{{building}}
								</td>
								<td>
									{{object}}
								</td>
								<td>
									
								</td>
								<td>
									
								</td>
							</tr>
						{{/each}}
					</tbody>
				{{else}}
					<tbody>
						{{#each boards}}
							<tr>
								<td>
									{{id}}
								</td>
								<td class="title">
									{{#if ../boardType.urlType }}
                                       <a target="_blank" href="{{linkUrl}}">{{title}}</a>
                                    {{else if ../../isFaqTotal}}
											<a href="{{../../../url}}/{{../../boardType}}/{{id}}">{{title}}</a>
									{{else}}
										<a href="{{../../../url}}/{{id}}">{{title}}</a>
										
                                    {{/if}}
								</td>
								<td>
									{{user.name}}
								</td>
								<td>
									{{formatedDate regDttm "YYYY-MM-DD"}}
								</td>
								<td>
									{{count}}
								</td>
								<td>
									{{#if attatchments}}
										<i class="material-icons file-icon">&#xE873;</i>
									{{else}}
										-
									{{/if}}

								</td>
							</tr>
						{{/each}}
					</tbody>
				{{/compare}}
			</table>			
		
	</div>
	<div class="pagination-wrapper">
		<ul class="pagination">
			{{#compare page.beginPage 1 operator=">"}}
				<li>
					<a href="{{url}}?page=" data-page="1" class="page-link {{page.className}}">&laquo; <spring:message code="page.first"/></a>
				</li>
				<li>
					<a href="{{url}}?page={{beforePage page.beginPage}}" data-page={{beforePage page.beginPage}} class="page-link {{className}}">...</a>
				</li>	
			{{/compare}}
		
			{{#for page.beginPage page.endPage 1}}
				{{#compare this ../page.currentPage operator="=="}}
					<li class="active">
						<a href="{{../../url}}?page={{this}}" data-page="{{this}}" class="page-link {{../../page.className}}">{{this}}</a>
					</li>
				{{else}}
					<li>
						<a href="{{../../url}}?page={{this}}" class="page-link {{../../page.className}}">{{this}}</a>
					</li>
				{{/compare}}
			{{/for}}
		
			{{#compare page.endPage page.totalPages operator="<"}}
				<li>
					<a href="{{url}}?page={{nextPage page.endPage}}" data-page="{{nextPage page.endPage}}" class="page-link {{page.className}}">...</a>
				</li>
				<li>
					<a href="{{url}}?page={{nextPage page.totalPages}}" data-page={{page.totalPages}} class="page-link {{page.className}}"><spring:message code="page.last"/> &raquo;</a>
				</li>
			{{/compare}}
		</ul>
	</div>
</script>

<script id="board-gallary-template" type="text/x-handlebars-template">
	<div class="list-board-wrapper">
		<div class="row gallery-boards">
			{{#each boards}}
				<div class="col-sm-6 col-md-4">
					<div class="board-gallery-item">
                        {{#if ../boardType.urlType }}
                            <a target="_blank" href="{{linkUrl}}">
                                <div class="board-img">
                                    <img src="{{picture}}"
                                        class="img-responsive"/>
                                </div>
                                <div class="board-title">
                                    {{title}}
                                    {{#compare ../../boardType.name "digitalEducation" operator="=="}}
                                        <br>
                                        {{formatedDate ../regDttm "YYYY.MM.DD"}}
                                    {{/compare}}
                                    {{#compare ../../boardType.name "sciencePlay" operator="=="}}
                                        <br>
                                        {{formatedDate ../regDttm "YYYY.MM.DD"}}
                                    {{/compare}}
                                </div>
                            </a>
                        {{else}}
                            <a href="{{../../url}}/{{id}}" >
                                <div class="board-img">
                                    <img src="{{picture}}"
                                        class="img-responsive"/>
                                </div>
                                <div class="board-title">
                                    {{title}}
                                    {{#compare ../../boardType.name "digitalEducation" operator="=="}}
                                        <br>
                                        {{formatedDate ../regDttm "YYYY.MM.DD"}}
                                    {{/compare}}
                                    {{#compare ../../boardType.name "sciencePlay" operator="=="}}
                                        <br>
                                        {{formatedDate ../regDttm "YYYY.MM.DD"}}
                                    {{/compare}}
                                </div>
                            </a>
                        {{/if}}
						
					</div>
				</div>
			{{/each}}
		</div>
	</div>
	<div class="pagination-wrapper">
		<ul class="pagination">
			{{#compare page.beginPage 1 operator=">"}}
				<li>
					<a href="{{url}}?page=" data-page="1" class="page-link {{page.className}}">&laquo; <spring:message code="page.first"/></a>
				</li>
				<li>
					<a href="{{url}}?page={{beforePage page.beginPage}}" data-page={{beforePage page.beginPage}} class="page-link {{className}}">...</a>
				</li>	
			{{/compare}}
		
			{{#for page.beginPage page.endPage 1}}
				{{#compare this ../page.currentPage operator="=="}}
					<li class="active">
						<a href="{{../../url}}?page={{this}}" data-page="{{this}}" class="page-link {{../../page.className}}">{{this}}</a>
					</li>
				{{else}}
					<li>
						<a href="{{../../url}}?page={{this}}" class="page-link {{../../page.className}}">{{this}}</a>
					</li>
				{{/compare}}
			{{/for}}
		
			{{#compare page.endPage page.totalPages operator="<"}}
				<li>
					<a href="{{url}}?page={{nextPage page.endPage}}" data-page="{{nextPage page.endPage}}" class="page-link {{page.className}}">...</a>
				</li>
				<li>
					<a href="{{url}}?page={{nextPage page.totalPages}}" data-page={{page.totalPages}} class="page-link {{page.className}}"><spring:message code="page.last"/> &raquo;</a>
				</li>
			{{/compare}}
		</ul>
	</div>
</script>

