<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="list-board-wrapper table-responsive">
	<div id="board-search-condition" class="search-condition-wrapper">
		<form class="search-condition-box form-inline" action="${param.path }">
			<select id="board-search-type" class="form-control teal" name="searchType">
				<option value="title" <c:if test="${empty searchType or searchType == 'title' }">selected</c:if>>
					제목
				</option>
				<option value="content" <c:if test="${searchType == 'content' }">selected</c:if>>
					내용
				</option>
			</select>
			<div class="search-text">
				<input type="text" id="board-search-text"  name="searchKey" class="form-control" placeholder="검색어를 입력하세요" 
					value="${searchKey }">
				<i id="board-search-icon" class="fa fa-search"></i>
			</div>
		</form>
	</div>
	
	<table class="table centered-table">
		<thead>
			<tr>
				<th></th>
				<th>번호</th>
				<th style="width: 50%">제목</th>
				<th>작성자</th>
				<c:if test="${param.isFaq ne 'faq' }">
					<th>등록일</th>
				</c:if>
				<th>조회수</th>
				<th>첨부파일</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="board" items="${boards }">
				<tr <c:if test="${board.fixedOnTop eq true }">class="fixed-on-top"</c:if>>
					<td><c:if test="${board.fixedOnTop eq true }"><i class="fa fa-flag" aria-hidden="true"></i> </c:if></td>
					<td>${board.typeSeq }</td>
					<td class="left title">
						<c:choose>
							<c:when test="${boardType.urlType }">
								<a target="_blank" href="${board.linkUrl }">
									${board.title }
								</a>
							</c:when>
							<c:otherwise>
								<a href="${param.path }/${board.id }">${board.title }</a></td>	
							</c:otherwise>
						</c:choose>
						
					<td>
						${board.member.memberName }	
					</td>
					<c:if test="${param.isFaq ne 'faq' }">
						<td>
							<fmt:formatDate pattern="yyyy-MM-dd" value="${board.regDttm }" />
						</td>
					</c:if>
					<td>${board.count }</td>
					<td>
						<c:choose>
							<c:when test="${fn:length(board.attatchments) > 0}">
								<i class="fa fa-file-text"></i>
							</c:when>
							<c:otherwise>
								- 
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
				
				<c:if test="${not empty board.replies }">
					<c:forEach var="reply" items="${board.replies }">
						<tr class="reply" style="background-color: #f2f2f2;">
							<td></td>
							<td></td>
							<td class="left title">
								<a href="${param.path }/${reply.id }">Re: ${reply.title }</a>
							</td>
							<td>
								<c:if test="${reply.writtenByAdmin }">
									관리자
								</c:if>
							</td>
							<td>
								<fmt:formatDate pattern="yyyy-MM-dd" value="${reply.regDttm }" />
							</td>
							<td>
								${reply.count }
							</td>
							<td>
							- 
							</td>
						</tr>
					</c:forEach>
				</c:if>
			</c:forEach>
		</tbody>
	</table>
</div>

<c:if test="${boardType.name eq 'opinions'}">
	<div class="text-right">
		<a href="<c:url value='/communication/opinions/private/new' />" class="btn btn-orange">
			글쓰기
		</a>
	</div>
</c:if>

<div class="pagination-wrapper">
	<ul class="pagination">
		<c:if test="${page.beginPage > 1 }">
			<li>
				<a href="${page.path }page=1" data-page="1" class="page-link {{page.className}}">
					<i class="fa fa-chevron-left"></i>
				</a>
			</li>
			<li>
				<a href="${page.path }page=${page.beginPage - 1}" data-page="${page.beginPage - 1}" class="page-link"><i class="fa fa-ellipsis-h"></i></a>
			</li>
		</c:if>
		
		<c:forEach var="i" begin="${page.beginPage }" end="${page.endPage }" varStatus="loop">
			<c:choose>
				<c:when test="${i == page.currentPage }">
					<li class="active">
						<a href="#" class="page-link">${i }</a>
					</li>
				</c:when>
				<c:otherwise>
					<li>
						<a href="${page.path }page=${i }" class="page-link">${i }</a>
					</li>
				</c:otherwise>
			</c:choose>
		</c:forEach>
		
		<c:if test="${page.endPage < page.totalPages }">
			<li>
				<a href="${page.path }page=${page.endPage + 1}" data-page="${page.endPage + 1}" class="page-link"><i class="fa fa-ellipsis-h"></i></a>
			</li>
			<li>
				<a href="${page.path }page=${page.totalPages }" data-page="${page.totalPages }" class="page-link"><spring:message code="page.last" text=""/>
					<i class="fa fa-chevron-right"></i>
				</a>
			</li>
		</c:if>
	</ul>
</div>

<script type="text/javascript">
	$(function() {
		$("#board-search-icon").on("click", function(e) {
			search($(this), e);
		});
		/* $("#board-search-text").on("keyup", function(e) {
			e.preventDefault();
			if( e.keyCode == 13) {
				//search($(this), e);
			}
		}); */
		
		function search(selector, e) {
			var searchText = selector.val();
			
			if ( searchText == "" ) {
				alert('검색어를 입력하세요.');
				return;
			} else {
				$("#board-search-form").submit();
			}
		}
	});

</script>