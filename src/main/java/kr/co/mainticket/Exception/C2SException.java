package kr.co.mainticket.Exception;

/**
 * User Define Exception
 * @Author E.S. Shin
 * @Date 2008. 12. 08.
 */

public class C2SException extends Exception{
	private String ExceptionCode;
	
	public C2SException(String ExceptCode, String ExceptMsg){
		super(ExceptMsg);
		this.ExceptionCode = ExceptCode;
	}
	
	public String getExceptCode(){
		return this.ExceptionCode;
	}
}