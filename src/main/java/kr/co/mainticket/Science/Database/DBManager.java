package kr.co.mainticket.Science.Database;

import java.sql.Connection;

import kr.co.mainticket.Science.Config.Environment;
import kr.co.mainticket.Database.DBConnectionManager;


/**
 * Set the System Environment
 */
public class DBManager{
	private static String DatabaseName;
	
	/**
	 * Initialize Environments
	 */
	public static Connection getConn() throws Exception{
		if(DatabaseName == null){
			DatabaseName = Environment.getDbName();
		}
		
		if(!DBConnectionManager.getInstance().ExistPool(DatabaseName)){
			DBConnectionManager.getInstance(DatabaseName, Environment.getDbPoolName());
		}
		
		return DBConnectionManager.getInstance().getConnection(DatabaseName);
	}
	
	public static Connection getConn(String JupiterDatabaseName) throws Exception{
		if(!DBConnectionManager.getInstance().ExistPool(JupiterDatabaseName)){
			DBConnectionManager.getInstance(JupiterDatabaseName, "ScienceJupiterPool");
		}
		
		return DBConnectionManager.getInstance().getConnection(JupiterDatabaseName);
	}
	
	public static Connection getConn(String DBType, String ServerName, int Port, String Sid, String UserId, String UserPw) throws Exception{
		return DBConnectionManager.getInstance().getConnection(DBType, ServerName, Port, Sid, UserId, UserPw);
	}
}
