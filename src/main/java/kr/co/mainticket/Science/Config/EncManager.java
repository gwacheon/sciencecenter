package kr.co.mainticket.Science.Config;

import kr.co.mainticket.Utility.Utility;

import com.Ineb.Dguard.DguardManager;

/**
 * Set the System Environment
 */
public class EncManager{
	private static DguardManager dgmgr;
	
	
	/**
	 * Initialize Environments
	 */
	public static void Initialize(){
		//String path = "/app/ineb/etc/WasAgent2.txt";
		String path = "D:/Project/ScienceCenterHome/ScienceCenterHome/WebContent/DGuard/WasAgent2.txt";
		
		try{
			dgmgr = DguardManager.Init("gnsmdb_real", "agent!1234", "agent!1234", path.trim());
		}catch(Exception e){
			e.printStackTrace();
			dgmgr = null;
		}
			
	}
	
	public static String getEncData(String TableName, String ColumnName, String TargetData){
		String EncData = "";
		
		if(dgmgr == null){
			//Initialize();
		}
		
		String DB_SID = "GNSMDB";
		String DB_USER = "SCIENCECENTER";
		
		if(TargetData.length() > 0){
			//EncData = dgmgr.Encrypt(DB_SID, DB_USER, TableName, ColumnName, Utility.CheckNull(TargetData));
			EncData = TargetData;
		}
		else{
			EncData = TargetData;
		}
		
		return EncData;
	}
	
	public static String getDecData(String TableName, String ColumnName, String TargetData){
		String DecData = "";
		
		if(dgmgr == null){
			Initialize();
		}
		
		String DB_SID = "GNSMDB";
		String DB_USER = "SCIENCECENTER";
		
		DecData = dgmgr.Decrypt(DB_SID, DB_USER, TableName, ColumnName, Utility.CheckNull(TargetData));
		
		return DecData;
	}
}
