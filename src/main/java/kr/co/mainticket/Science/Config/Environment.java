package kr.co.mainticket.Science.Config;

import java.io.File;
import java.util.Hashtable;
import java.util.ResourceBundle;

import org.w3c.dom.NodeList;

import kr.co.mainticket.Database.DBConnectionManager;
import kr.co.mainticket.Log.C2SLog;
import kr.co.mainticket.Payment.Card.Card;
import kr.co.mainticket.Payment.Cash.Cash;
import kr.co.mainticket.Payment.VA.VA;
import kr.co.mainticket.Payment.RA.RA;
import kr.co.mainticket.Point.C2SPointEnv;
import kr.co.mainticket.Utility.HttpUtility;
import kr.co.mainticket.Utility.Utility;


/**
 * Set the System Environment
 */
public class Environment{
	
	/**
	 * System Environment Variable
	 */
	private static String XML_PATH;
	private static int LOG_LEVEL;
	private static String LOG_SYSTEM_FLAG;
	private static String LOG_PATH;
	private static String FILE_PATH;
	private static String FILE_URL;
	private static String POST_ENC_FLAG;
	private static String GET_ENC_FLAG;
	private static String ENCODING_SET;
	private static String DB_NAME;
	private static String DB_POOL_NAME;
	
	
	/**
	 * Query Layout Set
	 */	
	private static Hashtable QueryLayout;
	
	
	/**
	 * Initialize Environments
	 */
	public static void Initialize(){
		C2SLog.printConsole("#################### SCIENCE ENVIRONMENT SETTING START ####################");
		
		try{
			ResourceBundle bundle = ResourceBundle.getBundle("ScienceEnvironment");
			
			try{
				XML_PATH		= bundle.getString("XML_PATH");
				C2SLog.printConsole("[XML_PATH]        : " + XML_PATH);
				
				LOG_LEVEL		= Utility.StringToInt(bundle.getString("LOG_LEVEL"));
				C2SLog.printConsole("[LOG_LEVEL]       : " + LOG_LEVEL);
				
				LOG_SYSTEM_FLAG	= bundle.getString("LOG_SYSTEM_FLAG");
				C2SLog.printConsole("[LOG_SYSTEM_FLAG] : " + LOG_SYSTEM_FLAG);
				
				LOG_PATH		= bundle.getString("LOG_PATH");
				C2SLog.printConsole("[LOG_PATH]        : " + LOG_PATH);
				
				FILE_PATH		= bundle.getString("FILE_PATH");
				C2SLog.printConsole("[FILE_PATH]       : " + FILE_PATH);
				
				FILE_URL		= bundle.getString("FILE_URL");
				C2SLog.printConsole("[FILE_URL]        : " + FILE_URL);
				
				POST_ENC_FLAG	= bundle.getString("POST_ENC_FLAG");
				C2SLog.printConsole("[POST_ENC_FLAG]   : " + POST_ENC_FLAG);
				
				GET_ENC_FLAG	= bundle.getString("GET_ENC_FLAG");
				C2SLog.printConsole("[GET_ENC_FLAG]    : " + GET_ENC_FLAG);
				
				ENCODING_SET	= bundle.getString("ENCODING_SET");
				C2SLog.printConsole("[ENCODING_SET]    : " + ENCODING_SET);
				
				DB_NAME			= bundle.getString("DB_NAME");
				C2SLog.printConsole("[DB_NAME]         : " + DB_NAME);
				
				DB_POOL_NAME	= bundle.getString("DB_POOL_NAME");
				C2SLog.printConsole("[DB_POOL_NAME]    : " + DB_POOL_NAME);
				
				Card.SetServerInfo(bundle.getString("CARD_IP"), Utility.StringToInt(bundle.getString("CARD_PORT")), Utility.StringToInt(bundle.getString("CARD_TIME_OUT")));
				Card.SetLogInfo(true, bundle.getString("CARD_LOG_PATH"));
				C2SLog.printConsole("[CARD CONFIG]     : " + bundle.getString("CARD_IP") + ":" + bundle.getString("CARD_PORT") + ":" + bundle.getString("CARD_TIME_OUT") + ":" + bundle.getString("CARD_LOG_PATH"));
				
				Cash.SetServerInfo(bundle.getString("CASH_IP"), Utility.StringToInt(bundle.getString("CASH_PORT")), Utility.StringToInt(bundle.getString("CASH_TIME_OUT")));
				Cash.SetLogInfo(true, bundle.getString("CASH_LOG_PATH"));
				C2SLog.printConsole("[CASH CONFIG]     : " + bundle.getString("CASH_IP") + ":" + bundle.getString("CASH_PORT") + ":" + bundle.getString("CASH_TIME_OUT") + ":" + bundle.getString("CASH_LOG_PATH"));
				
				VA.SetServerInfo(bundle.getString("VA_IP"), Utility.StringToInt(bundle.getString("VA_PORT")), Utility.StringToInt(bundle.getString("VA_TIME_OUT")));
				VA.SetLogInfo(true, bundle.getString("VA_LOG_PATH"));
				C2SLog.printConsole("[VA CONFIG]       : " + bundle.getString("VA_IP") + ":" + bundle.getString("VA_PORT") + ":" + bundle.getString("VA_TIME_OUT") + ":" + bundle.getString("VA_LOG_PATH"));
				
				RA.SetServerInfo(bundle.getString("RA_IP"), Utility.StringToInt(bundle.getString("RA_PORT")), Utility.StringToInt(bundle.getString("RA_TIME_OUT")));
				RA.SetLogInfo(true, bundle.getString("RA_LOG_PATH"));
				C2SLog.printConsole("[RA CONFIG]       : " + bundle.getString("RA_IP") + ":" + bundle.getString("RA_PORT") + ":" + bundle.getString("RA_TIME_OUT") + ":" + bundle.getString("RA_LOG_PATH"));
				
				C2SPointEnv.setServerInfo(bundle.getString("POINT_IP"), Utility.StringToInt(bundle.getString("POINT_PORT")), Utility.StringToInt(bundle.getString("POINT_TIME_OUT")));
				C2SPointEnv.setLogInfo(true, bundle.getString("POINT_LOG_PATH"));
				C2SLog.printConsole("[POINT CONFIG]    : " + bundle.getString("POINT_IP") + ":" + bundle.getString("POINT_PORT") + ":" + bundle.getString("POINT_TIME_OUT") + ":" + bundle.getString("POINT_LOG_PATH"));
				
				if(POST_ENC_FLAG.equals("N")){
					HttpUtility.setPostEncFlag(false);
				}
				
				if(GET_ENC_FLAG.equals("N")){
					HttpUtility.setGetEncFlag(false);
				}
				
				if(ENCODING_SET.length() > 0){
					HttpUtility.setEncodingSet(ENCODING_SET);
				}
			}catch(Exception de){
				C2SLog.printConsole("#################### SCIENCE ENVIRONMENT SETTING EXCEPTION ####################");
				de.printStackTrace();
			}
			
			C2SLog.printConsole("#################### SCIENCE ENVIRONMENT SETTING END ####################");
		}catch(Exception e){
			C2SLog.printConsole("#################### SCIENCE ENVIRONMENT SETTING EXCEPTION ####################");
			e.printStackTrace();
		}
		
		setLogPool();
		
		setDBPool();
		
		setQueryLayout();
	}
	
	
	/**
	 * Set the Log Pool
	 */
	private static void setLogPool(){
		C2SLog.printConsole("#################### SCIENCE LOG POOL START ####################");
		
		try{
			C2SLog.Initialize("SCIENCE_LOG", LOG_LEVEL, LOG_PATH, LOG_SYSTEM_FLAG.equals("Y")?true:false);
			
			C2SLog.printConsole("#################### SCIENCE LOG POOL END ####################");
		}catch(Exception e){
			C2SLog.printConsole("#################### SCIENCE LOG POOL EXCEPTION ####################");
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Set the DB Pool
	 */
	private static void setDBPool(){
		C2SLog.printLogger("SCIENCE_LOG", "SCIENCE_INFO", "#################### SCIENCE DB POOL START ####################", null, 0);
		
		try{
			DBConnectionManager.getInstance(DB_NAME, DB_POOL_NAME);
			DBConnectionManager.getInstance("SCIENCE_JUPITER", "ScienceJupiterPool");
			
			C2SLog.printLogger("SCIENCE_LOG", "SCIENCE_INFO", "#################### SCIENCE DB POOL END ####################", null, 0);
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "SCIENCE_INFO", "#################### SCIENCE DB POOL EXCEPTION ####################", e, 0);
		}
	}
	
	
	/**
	 * Set the Query Layout
	 */
	private static void setQueryLayout(){
		C2SLog.printLogger("SCIENCE_LOG", "SCIENCE_INFO", "#################### SCIENCE XML LAYOUT START ####################", null, 0);
		
		try{
			File[] XmlList = (new File(XML_PATH)).listFiles();
			
			if(XmlList == null){
				C2SLog.printLogger("SCIENCE_LOG", "SCIENCE_INFO", "Xml Path not exist! " + XML_PATH, null, 0);
			}
			
			int QueryCnt = 0;
			
			String FileName = "";
			int DotPoint = 0;
			String Extension = "";
			NodeList XmlNodeList = null;
			
			for(int idx = 0; idx < XmlList.length; idx++){
				if(XmlList[idx].isFile()){
					FileName = XmlList[idx].getName();
					DotPoint = FileName.lastIndexOf(".");
					
					if(DotPoint > 0){
						Extension = FileName.substring(DotPoint + 1).toUpperCase();
						
						if(Extension.equals("XML")){
							XmlNodeList = Utility.getElementList(Utility.loadXml(XML_PATH + FileName), "SQL");
							
							QueryCnt += XmlNodeList.getLength();
						}
					}
				}
			}
			
			QueryLayout = new Hashtable(QueryCnt);
			
			for(int idx = 0; idx < XmlList.length; idx++){
				if(XmlList[idx].isFile()){
					FileName = XmlList[idx].getName();
					DotPoint = FileName.lastIndexOf(".");
					
					if(DotPoint > 0){
						Extension = FileName.substring(DotPoint + 1).toUpperCase();
						if(Extension.equals("XML")){
							C2SLog.printLogger("SCIENCE_LOG", "SCIENCE_INFO", "Xml File Name : " + FileName, null, 0);
							
							XmlNodeList = Utility.getElementList(Utility.loadXml(XML_PATH + FileName), "SQL");
							for(int Xidx = 0; Xidx < XmlNodeList.getLength(); Xidx++){
								if(Utility.getNodeValue(XmlNodeList.item(Xidx).getAttributes().getNamedItem("ID")).length() > 0){
									QueryLayout.put(FileName.substring(0, DotPoint) + "_" + Utility.getNodeValue(XmlNodeList.item(Xidx).getAttributes().getNamedItem("ID")), Utility.getNodeValue(XmlNodeList.item(Xidx)));
								}
								
							}
						}
					}
				}
			}
			
			C2SLog.printLogger("SCIENCE_LOG", "SCIENCE_INFO", "#################### SCIENCE XML LAYOUT END ####################", null, 0);
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "SCIENCE_INFO", "#################### SCIENCE XML LAYOUT EXCEPTION ####################", e, 0);
		}
	}
	
	
	/**
	 * Get the Query
	 * @param FileName
	 * @param QueryID
	 * @return
	 */
	public static String getQuery(String FileName, String QueryID){
		String Query = "";
		
		try{
			Query = (String)QueryLayout.get(FileName + "_" + QueryID);
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "SCIENCE_INFO", "Exception occured on getQuery! " + e.getMessage(), null, 1);
			Query = "";
		}
		
		return Query;
	}
	
	
	/**
	 * Destroy Environments
	 */
	public static void Destroy(){
		C2SLog.printConsole("#################### SCIENCE ENVIRONMENT DESTROY START ####################");
		
		XML_PATH = null;
		LOG_LEVEL = 0;
		LOG_SYSTEM_FLAG = null;
		LOG_PATH = null;
		FILE_PATH = null;
		FILE_URL = null;
		POST_ENC_FLAG = null;
		GET_ENC_FLAG = null;
		DB_NAME = null;
		DB_POOL_NAME = null;
		
		QueryLayout = null;
		
		// Call Garbage Collector
		System.gc();
		
		C2SLog.printConsole("#################### SCIENCE ENVIRONMENT DESTROY END ####################");
	}
	
	
	/**
	 * Reload Environments
	 */
	public static void Reload(){
		Destroy();
		Initialize();	
	}
	
	
	public static String getXmlPath(){
		return XML_PATH;
	}
	
	public static int getLogLevel(){
		return LOG_LEVEL;
	}

	public static String getLogPath(){
		return LOG_PATH;
	}
	
	public static String getLogSystemFlag(){
		return LOG_SYSTEM_FLAG;
	}
	
	public static String getFilePath(){
		return FILE_PATH;
	}
	
	public static String getFileUrl(){
		return FILE_URL;
	}
	
	public static String getPostEncFlag(){
		return POST_ENC_FLAG;
	}
	
	public static String getGetEncFlag(){
		return GET_ENC_FLAG;
	}
	
	public static String getDbName(){
		return DB_NAME;
	}
	
	public static String getDbPoolName(){
		return DB_POOL_NAME;
	}
}
