package kr.co.mainticket.Science.Interlock.Member;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.etri.saml2.assertion.Statement;

import kr.co.mainticket.Encrypt.DigestCoder;
import kr.co.mainticket.Entity.*;
import kr.co.mainticket.Exception.C2SException;
import kr.co.mainticket.Log.*;
import kr.co.mainticket.Science.Config.Environment;
import kr.co.mainticket.Science.Database.DBManager;
import kr.co.mainticket.Science.Interlock.Common;
import kr.co.mainticket.Science.Interlock.Point.PointInfo;
import kr.co.mainticket.Utility.DateTime;
import kr.co.mainticket.Utility.Utility;
import kr.go.sciencecenter.controller.UserController;
import kr.go.sciencecenter.service.UserService;

public class Check{
	private static final Logger logger = LoggerFactory.getLogger(Check.class);
	private EntityList MemberInfo;
	private EntityList FamilyList;
	private EntityList MembershipTypeList;
	private EntityList RegionList;
	private EntityList JobList;
	private EntityList MotiveList;
	private EntityList OptionList;
	private EntityList RelationList;
	private Entity MembershipInfo;
	private EntityList PriceList;
	private EntityList PaymentTypeList;
	
	private String ErrCode;
	private String ErrMessage;
	
	@Autowired
	UserService userService;
	
	public EntityList getMemberInfo(){
		return this.MemberInfo;
	}
	
	public EntityList getFamilyList(){
		return this.FamilyList;
	}
	
	public EntityList getMembershipTypeList(){
		return this.MembershipTypeList;
	}
	
	public EntityList getRegionList(){
		return this.RegionList;
	}
	
	public EntityList getJobList(){
		return this.JobList;
	}
	
	public EntityList getMotiveList(){
		return this.MotiveList;
	}
	
	public EntityList getOptionList(){
		return this.OptionList;
	}
	
	public EntityList getRelationList(){
		return this.RelationList;
	}
	
	public Entity getMembershipInfo(){
		return this.MembershipInfo;
	}
	
	public EntityList getPriceList(){
		return this.PriceList;
	}
	
	public EntityList getPaymentTypeList(){
		return this.PaymentTypeList;
	}
	
	
	public String getErrCode(){
		return Utility.CheckNull(this.ErrCode);
	}
	
	public String getErrMessage(){
		return Utility.CheckNull(this.ErrMessage).replaceAll("\n", "\\\\n").replaceAll("\"", "'");
	}
	
	
	
	/**
	 * Check Duplicate
	 * @param CompanyCd
	 * @param DataEntity
	 * @return
	 * @throws Exception
	 */
	private static final String DB_DRIVER = "oracle.jdbc.driver.OracleDriver";
	private static final String DB_CONNECTION = "jdbc:oracle:thin:@192.168.100.4:1521:GNSMDB";
	private static final String DB_USER = "SCIENCECENTER";
	private static final String DB_PASSWORD = "cmsgnsm";
	
	public boolean CheckDuplicate(String CompanyCd, Entity DataEntity) throws Exception{
		boolean result = false;
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		ResultSet rs = null;
		
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try{
			logger.info("===1");
			Class.forName(DB_DRIVER);
			conn = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
			logger.info("===2");
			String CertifyType = DataEntity.getFieldNoCheck("CERTIFY_TYPE");
			String DuplicateKey = DataEntity.getFieldNoCheck("DUPLICATE_KEY");
			logger.info("===3");
			if(DuplicateKey.length() == 0){
				throw new C2SException("CD01", "NOT_EXIST_DUPLICATE_KEY");
			}
			logger.info("===4");
			if(CertifyType.equals("A00001")){
				DuplicateKey = DigestCoder.DigestEncryptString("SHA-256", DuplicateKey);
			}
			logger.info("===5");
			Entity CheckCon = new Entity(2);
			logger.info("CompanyCd : "+CompanyCd);
			logger.info("DuplicateKey : "+DuplicateKey);
			CheckCon.setField("1", CompanyCd);
			CheckCon.setField("2", DuplicateKey);
			
			
			//int isDuplicate = userService.findDuplicateCode(CompanyCd, DuplicateKey);
			int isDuplicate = 0;
			preparedStatement = conn.prepareStatement("SELECT COUNT(*) cnt FROM TM_MEMBER MB WHERE "
					+ "MB.COMPANY_CD = ? AND "
					+ "MB.DUPLICATE_KEY = ? AND "
					+ "MB.USE_FLAG = ? ");
			preparedStatement.setString(1, CompanyCd);
			preparedStatement.setString(2, DuplicateKey);
			preparedStatement.setString(3, "Y");

			// execute select SQL stetement
			rs = preparedStatement.executeQuery();
			while (rs.next()) {
				logger.info(" rs : "+rs.getInt("cnt"));
				isDuplicate = rs.getInt("cnt");
			}
			
			logger.info("result : "+isDuplicate);
			if(isDuplicate > 0){
				logger.info("throw Exception ");
				throw new C2SException("CD02", "EXIST_DUPLICATE_KEY");
			}
			//EntityList CheckList = EntityUtility.SelectQueryParagraphList(conn, Environment.getQuery("MemberCheck", "SELECT_MEMBER_DUPLICATE_KEY"), CheckCon);
//			if(CheckList.size() > 0){
//				Entity CheckItem = CheckList.get(0);
//				
//				if(Utility.StringToInt(CheckItem.getField("CHECK_CNT")) > 0){
//					throw new C2SException("CD02", "EXIST_DUPLICATE_KEY");
//				}
//			}
//			else{
//				throw new C2SException("CD03", "CHECK_ERROR");
//			}
			
			result = true;
		}catch(C2SException ce){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".CheckDuplicate()! " + ce.getMessage(), ce, 1);
			this.ErrCode = ce.getExceptCode();
			this.ErrMessage = ce.getMessage();
			result = false;
		}catch(Exception e){
			//C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".CheckDuplicate()! " + e.getMessage(), e, 1);
			logger.info("error "+ e.getMessage());
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			result = false;
		}finally{
			if(conn != null) conn.close();
			if(preparedStatement != null) preparedStatement.close();
			if(rs != null) rs.close();
		}

		return result;
	}
	
	
	/**
	 * Check Offline Member
	 * @param CompanyCd
	 * @param DataEntity
	 * @return
	 * @throws Exception
	 */
	public boolean CheckOffline(String CompanyCd, Entity DataEntity) throws Exception{
		boolean result = false;
		Connection conn = null;
		
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try{
			conn = DBManager.getConn();
			
			String MemberName = DataEntity.getFieldNoCheck("MEMBER_NAME");
			String MemberCel = DataEntity.getFieldNoCheck("MEMBER_CEL");
			String MemberEmail = DataEntity.getFieldNoCheck("MEMBER_EMAIL");
			String CertifyType = DataEntity.getFieldNoCheck("CERTIFY_TYPE");
			String DuplicateKey = DataEntity.getFieldNoCheck("DUPLICATE_KEY");
			
			if(MemberName.length() == 0){
				throw new C2SException("CO01", "NOT_EXIST_MEMBER_NAME");
			}
			else if(MemberCel.length() == 0){
				throw new C2SException("CO02", "NOT_EXIST_MEMBER_CEL");
			}
			
			if(CertifyType.equals("A00001") && DuplicateKey.length() > 0){
				DuplicateKey = DigestCoder.DigestEncryptString("SHA-256", DuplicateKey);
			}
			
			Entity CheckCon = null;
			
			if(DuplicateKey.length() > 0){
				CheckCon = new Entity(2);
				
				CheckCon.setField("1", CompanyCd);
				CheckCon.setField("2", DuplicateKey);
				
				EntityList CheckList = EntityUtility.SelectQueryParagraphList(conn, Environment.getQuery("MemberCheck", "SELECT_MEMBER_DUPLICATE_KEY"), CheckCon);
				
				if(CheckList.size() > 0){
					Entity CheckItem = CheckList.get(0);
					
					if(Utility.StringToInt(CheckItem.getField("CHECK_CNT")) > 0){
						throw new C2SException("CO03", "EXIST_DUPLICATE_KEY");
					}
				}
				else{
					throw new C2SException("CO04", "CHECK_ERROR");
				}
			}
			
			String OfflineDuplicateKey = MemberName + MemberCel;
			
			CheckCon = new Entity(2);
			
			CheckCon.setField("1", CompanyCd);
			CheckCon.setField("2", OfflineDuplicateKey);
			
			this.MemberInfo = EntityUtility.SelectQueryParagraphList(conn, Environment.getQuery("MemberCheck", "SELECT_MEMBER_OFFLINE"), CheckCon);
			
			if(this.MemberInfo.size() == 0){
				throw new C2SException("CO05", "NOT_EXIST_MEMBER");
			}
			else if(this.MemberInfo.size() > 1){
				throw new C2SException("CO06", "EXIST_MANY_MEMBER");
			}
			
			Entity MemberEntity = this.MemberInfo.get(0);
			
			if(!MemberName.equals(MemberEntity.getField("MEMBER_NAME"))){
				throw new C2SException("CO07", "NOT_EXIST_MEMBER");
			}
			else if(!MemberCel.equals(MemberEntity.getField("MEMBER_CEL"))){
				throw new C2SException("CO08", "NOT_EXIST_MEMBER");
			}
			
			if(MemberEmail.length() > 0){
				if(!MemberEmail.equals(MemberEntity.getField("MEMBER_EMAIL"))){
					throw new C2SException("CO09", "NOT_EXIST_MEMBER");
				}
			}
			
			String MemberNo = MemberEntity.getField("MEMBER_NO");
			
			Common cm = new Common();
			
			this.RegionList = cm.CommonList(conn, CompanyCd, "CM01", false);
			this.JobList = cm.CommonList(conn, CompanyCd, "CM02", false);
			this.MotiveList = cm.CommonList(conn, CompanyCd, "CM03", false);
			this.RelationList = cm.CommonList(conn, "MASTER", "M003", false);
			
			Entity TargetCon = new Entity(3);
			
			TargetCon.setField("1", CompanyCd);
			TargetCon.setField("2", CompanyCd);
			TargetCon.setField("3", MemberNo);
			
			this.OptionList = EntityUtility.SelectQueryParagraphList(conn, Environment.getQuery("MemberInformation", "SELECT_MEMBER_OPTION"), TargetCon);
			
			TargetCon = new Entity(2);
			
			TargetCon.setField("1", CompanyCd);
			TargetCon.setField("2", MemberNo);
			
			this.FamilyList = EntityUtility.SelectQueryParagraphList(conn, Environment.getQuery("MemberInformation", "SELECT_MEMBER_FAMILY_LIST"), TargetCon);
			
			result = true;
		}catch(C2SException ce){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".CheckOffline()! " + ce.getMessage(), ce, 1);
			this.ErrCode = ce.getExceptCode();
			this.ErrMessage = ce.getMessage();
			result = false;
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".CheckOffline()! " + e.getMessage(), e, 1);
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			result = false;
		}finally{
			if(conn != null) conn.close();
		}

		return result;
	}
	
	
	/**
	 * Check Name
	 * @param CompanyCd
	 * @param DataEntity
	 * @return
	 * @throws Exception
	 */
	public boolean CheckName(String CompanyCd, Entity DataEntity) throws Exception{
		boolean result = false;
		Connection conn = null;
		
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try{
			conn = DBManager.getConn();
			
			String MemberName = DataEntity.getFieldNoCheck("MEMBER_NAME");
			String MemberCel = DataEntity.getFieldNoCheck("MEMBER_CEL");
			String MemberEmail = DataEntity.getFieldNoCheck("MEMBER_EMAIL");
			
			Entity CheckCon = new Entity(4);
			
			CheckCon.setField("1", CompanyCd);
			CheckCon.setField("2", MemberName);
			CheckCon.setField("3", MemberCel);
			CheckCon.setField("4", MemberEmail);
			
			EntityList CheckList = EntityUtility.SelectQueryParagraphList(conn, Environment.getQuery("MemberCheck", "SELECT_MEMBER_NAME"), CheckCon);
			
			if(CheckList.size() > 0){
				Entity CheckItem = CheckList.get(0);
				
				if(Utility.StringToInt(CheckItem.getField("CHECK_CNT")) > 0){
					throw new C2SException("CN01", "EXIST_NAME");
				}
			}
			else{
				throw new C2SException("CN02", "CHECK_ERROR");
			}
			
			result = true;
		}catch(C2SException ce){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".CheckName()! " + ce.getMessage(), ce, 1);
			this.ErrCode = ce.getExceptCode();
			this.ErrMessage = ce.getMessage();
			result = false;
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".CheckName()! " + e.getMessage(), e, 1);
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			result = false;
		}finally{
			if(conn != null) conn.close();
		}

		return result;
	}
	
	
	/**
	 * Check Identity
	 * @param CompanyCd
	 * @param DataEntity
	 * @return
	 * @throws Exception
	 */
	public boolean CheckIdentity(String CompanyCd, Entity DataEntity) throws Exception{
		boolean result = false;
		Connection conn = null;
		
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try{
			conn = DBManager.getConn();
			
			String MemberId = DataEntity.getFieldNoCheck("MEMBER_ID");
			
			if(MemberId.length() == 0){
				throw new C2SException("CI01", "NOT_EXIST_MEMBER_ID");
			}
			
			Entity CheckCon = new Entity(2);
			
			CheckCon.setField("1", CompanyCd);
			CheckCon.setField("2", MemberId);
			
			EntityList CheckList = EntityUtility.SelectQueryParagraphList(conn, Environment.getQuery("MemberCheck", "SELECT_MEMBER_MEMBER_ID"), CheckCon);
			
			if(CheckList.size() > 0){
				Entity CheckItem = CheckList.get(0);
				
				if(Utility.StringToInt(CheckItem.getField("CHECK_CNT")) != 0){
					throw new C2SException("CI02", "EXIST_MEMBER_ID");
				}
			}
			else{
				throw new C2SException("CI03", "CHECK_ERROR");
			}
			
			String InterlockList = DataEntity.getFieldNoCheck("INTERLOCK_LIST");
			
			if(InterlockList.length() > 0){
				String[] TargetList = Utility.Split(InterlockList, "^");
				
				String InterlockCd = "";
				Entity InterlockEntity = null;
				
				for(int idx = 0; idx < TargetList.length; idx++){
					if(TargetList[idx].length() > 0){
						InterlockCd = TargetList[idx];
						
						InterlockEntity = new Entity(5);
						
						InterlockEntity.setField("INTERLOCK_CD", InterlockCd);
						InterlockEntity.setField("INTERLOCK_MEMBER_ID", MemberId);

						MemberInterlock mgr = new MemberInterlock();

						boolean InterlockResult = mgr.CheckId(InterlockEntity);
						
						if(InterlockResult){
							Entity InterlockInfo = mgr.getInterlockInfo();
							
							if(InterlockInfo.getFieldNoCheck("RSP_CD").equals("0000")){
								if(!InterlockInfo.getFieldNoCheck("USABLE_FLAG").equals("Y")){
									throw new C2SException("CI04", "EXIST_MEMBER_ID");
								}
							}
						}
					}
				}
			}
			
			result = true;
		}catch(C2SException ce){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".CheckIdentity()! " + ce.getMessage(), ce, 1);
			this.ErrCode = ce.getExceptCode();
			this.ErrMessage = ce.getMessage();
			result = false;
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".CheckIdentity()! " + e.getMessage(), e, 1);
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			result = false;
		}finally{
			if(conn != null) conn.close();
		}

		return result;
	}
	
	
	/**
	 * Membership Type List
	 * @param CompanyCd
	 * @param DataEntity
	 * @return
	 * @throws Exception
	 */
	public boolean MembershipTypeList(String CompanyCd, Entity DataEntity) throws Exception{
		boolean result = false;
		Connection conn = null;
		
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try{
			conn = DBManager.getConn();
			
			String SaleType = DataEntity.getFieldNoCheck("SALE_TYPE");
			String MemberNo = DataEntity.getFieldNoCheck("MEMBER_NO");
			String BirthDate = DataEntity.getFieldNoCheck("BIRTH_DATE");
			String RegNo1 = DataEntity.getFieldNoCheck("REG_NO1");
			
			if(SaleType.length() == 0){
				SaleType = "000002";
			}
			
			/*
			 * Check the Member REG_NO1
			 */
			String CurrDate = DateTime.getFormatDateString("yyyyMMdd", 0);
			
			if(MemberNo.length() > 0){
				Entity CheckCon = new Entity(2);
				
				CheckCon.setField("1", CompanyCd);
				CheckCon.setField("2", MemberNo);
				
				EntityList CheckList = EntityUtility.SelectQueryParagraphList(conn, Environment.getQuery("MemberCheck", "SELECT_MEMBER_REG_NO"), CheckCon);
				
				if(CheckList.size() > 0){
					RegNo1 = CheckList.get(0).getField("REG_NO1");
					
					if(RegNo1.length() > 0){
						if(Utility.StringToInt(RegNo1.substring(0, 2)) > Utility.StringToInt(CurrDate.substring(2, 4))){
							BirthDate = "19" + RegNo1;
						}
						else{
							BirthDate = "20" + RegNo1;
						}
					}
				}
			}
			else if(BirthDate.length() == 0 && RegNo1.length() > 0){
				if(Utility.StringToInt(RegNo1.substring(0, 2)) > Utility.StringToInt(CurrDate.substring(2, 4))){
					BirthDate = "19" + RegNo1;
				}
				else{
					BirthDate = "20" + RegNo1;
				}
			}
			
			int ConIdx = 1;
			Entity TargetCon = new Entity(4);
			StringBuffer Where = new StringBuffer();
			
			TargetCon.setField(Integer.toString(ConIdx++), CompanyCd);
			TargetCon.setField(Integer.toString(ConIdx++), SaleType);
			
			Where.append("AND (MT.AGE_FLAG = 'N' ");
			
			/*
			 * Check the Age
			 */
			if(BirthDate.length() > 0){
				try{
					int CurrYear = Utility.StringToInt(CurrDate.substring(0, 4));
					int BirthYear = Utility.StringToInt(BirthDate.substring(0, 4));
					
					int Years = CurrYear - BirthYear + 1;
					int FullYears = CurrYear - BirthYear;
					
					if(Utility.StringToInt(BirthDate.substring(4, 8)) > Utility.StringToInt(CurrDate.substring(4, 8))){
						FullYears = FullYears - 1;
					}
					
					Where.append("OR (MT.AGE_FLAG = 'Y' AND MT.AGE_TYPE = 'N' AND ? BETWEEN MT.AGE_LIMIT_MIN AND MT.AGE_LIMIT_MAX) ");
					Where.append("OR (MT.AGE_FLAG = 'Y' AND MT.AGE_TYPE = 'F' AND ? BETWEEN MT.AGE_LIMIT_MIN AND MT.AGE_LIMIT_MAX) ");
					
					TargetCon.setField(Integer.toString(ConIdx++), Integer.toString(Years));
					TargetCon.setField(Integer.toString(ConIdx++), Integer.toString(FullYears));
				}catch(Exception e){
				}
			}
			
			Where.append(")");
			
			String Paragraph = Environment.getQuery("MemberCheck", "SELECT_MEMBERSHIP_TYPE");
			Paragraph = Utility.ReplaceAll(Paragraph, "/*WHERE*/", Where.toString());
			
			this.MembershipTypeList = EntityUtility.SelectQueryParagraphList(conn, Paragraph, TargetCon);
			
			/*
			 * Include the Common List
			 */
			if(DataEntity.getFieldNoCheck("COMMON_FLAG").equals("Y")){
				Common cm = new Common();
				
				this.RegionList = cm.CommonList(conn, CompanyCd, "CM01", false);
				this.JobList = cm.CommonList(conn, CompanyCd, "CM02", false);
				this.MotiveList = cm.CommonList(conn, CompanyCd, "CM03", false);
				this.RelationList = cm.CommonList(conn, "MASTER", "M003", false);
				
				TargetCon = new Entity(3);
				
				TargetCon.setField("1", CompanyCd);
				TargetCon.setField("2", CompanyCd);
				TargetCon.setField("3", MemberNo);
				
				this.OptionList = EntityUtility.SelectQueryParagraphList(conn, Environment.getQuery("MemberInformation", "SELECT_MEMBER_OPTION"), TargetCon);
			}
			else{
				this.RegionList = new EntityList();
				this.JobList = new EntityList();
				this.MotiveList = new EntityList();
				this.OptionList = new EntityList();
				this.RelationList = new EntityList();
			}
			
			result = true;
		}catch(C2SException ce){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".MembershipTypeList()! " + ce.getMessage(), ce, 1);
			this.ErrCode = ce.getExceptCode();
			this.ErrMessage = ce.getMessage();
			result = false;
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".MembershipTypeList()! " + e.getMessage(), e, 1);
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			result = false;
		}finally{
			if(conn != null) conn.close();
		}

		return result;
	}
	
	
	/**
	 * Membership Price List
	 * @param CompanyCd
	 * @param DataEntity
	 * @return
	 * @throws Exception
	 */
	public boolean PriceList(String CompanyCd, Entity DataEntity) throws Exception{
		boolean result = false;
		Connection conn = null;
		
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try{
			conn = DBManager.getConn();
			
			String SaleType = DataEntity.getFieldNoCheck("SALE_TYPE");
			String MemberNo = DataEntity.getFieldNoCheck("MEMBER_NO");
			String FamilyNo = DataEntity.getFieldNoCheck("FAMILY_NO");
			String MembershipType = DataEntity.getFieldNoCheck("MEMBERSHIP_TYPE");
			
			if(MembershipType.length() == 0){
				throw new C2SException("PL01", "NOT_EXIST_MEMBERSHIP_TYPE");
			}
			
			if(SaleType.length() == 0){
				SaleType = "000002";
			}
			
			String CurrDate = DateTime.getFormatDateString("yyyyMMdd", 0);
			String MembershipStartDate = CurrDate;
			String LastDate = "";	// 2014.03.01
			
			/*
			 * Set the Membership Start Date
			 */
			if(MemberNo.length() > 0){
				Entity CheckCon = new Entity(7);
				
				CheckCon.setField("1", CurrDate);
				CheckCon.setField("2", CurrDate);
				CheckCon.setField("3", CurrDate);
				CheckCon.setField("4", CompanyCd);
				CheckCon.setField("5", MemberNo);
				CheckCon.setField("6", FamilyNo);
				CheckCon.setField("7", MembershipType);
				
				EntityList CheckList = EntityUtility.SelectQueryParagraphList(conn, Environment.getQuery("MemberCheck", "SELECT_MEMBERSHIP_START_DATE"), CheckCon);
				
				if(CheckList.size() > 0){
					MembershipStartDate = CheckList.get(0).getField("MEMBERSHIP_START_DATE");
					LastDate = CheckList.get(0).getField("LAST_DATE");	// 2014.03.01
				}
				else{
					MembershipStartDate = CurrDate;
				}
			}
			
			this.MembershipInfo = new Entity(5);
			
			this.MembershipInfo.setField("MEMBERSHIP_START_DATE", MembershipStartDate);
			this.MembershipInfo.setField("LAST_DATE", LastDate);	// 2014.03.01
			
			Entity TargetCon = new Entity(3);
			
			TargetCon.setField("1", CompanyCd);
			TargetCon.setField("2", MembershipType);
			TargetCon.setField("3", SaleType);
			
			this.PriceList = EntityUtility.SelectQueryParagraphList(conn, Environment.getQuery("MemberCheck", "SELECT_MEMBERSHIP_PRICE"), TargetCon);
			
			result = true;
		}catch(C2SException ce){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".PriceList()! " + ce.getMessage(), ce, 1);
			this.ErrCode = ce.getExceptCode();
			this.ErrMessage = ce.getMessage();
			result = false;
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".PriceList()! " + e.getMessage(), e, 1);
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			result = false;
		}finally{
			if(conn != null) conn.close();
		}

		return result;
	}
	
	
	/**
	 * Membership Payment Type List
	 * @param CompanyCd
	 * @param DataEntity
	 * @return
	 * @throws Exception
	 */
	public boolean PaymentTypeList(String CompanyCd, Entity DataEntity) throws Exception{
		boolean result = false;
		Connection conn = null;
		
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try{
			conn = DBManager.getConn();
			
			String SaleType = DataEntity.getFieldNoCheck("SALE_TYPE");
			String MemberNo = DataEntity.getFieldNoCheck("MEMBER_NO");
			String FamilyNo = DataEntity.getFieldNoCheck("FAMILY_NO");
			String MembershipUniqueNbr = DataEntity.getFieldNoCheck("MEMBERSHIP_UNIQUE_NBR");
			String MembershipType = DataEntity.getFieldNoCheck("MEMBERSHIP_TYPE");
			String PriceNbr = DataEntity.getFieldNoCheck("PRICE_NBR");
			String MembershipStartDate = DataEntity.getFieldNoCheck("MEMBERSHIP_START_DATE");
			
			if(MembershipType.length() == 0){
				throw new C2SException("PT01", "NOT_EXIST_MEMBERSHIP_TYPE");
			}
			else if(PriceNbr.length() == 0){
				throw new C2SException("PT02", "NOT_EXIST_PRICE_NBR");
			}
			
			if(SaleType.length() == 0){
				SaleType = "000002";
			}
			
			Entity TargetCon = new Entity(4);
			
			TargetCon.setField("1", CompanyCd);
			TargetCon.setField("2", MembershipType);
			TargetCon.setField("3", PriceNbr);
			TargetCon.setField("4", SaleType);
			
			EntityList CheckList = EntityUtility.SelectQueryParagraphList(conn, Environment.getQuery("MemberCheck", "SELECT_MEMBERSHIP_SALE_TYPE"), TargetCon);
			
			if(CheckList.size() > 0){
				this.MembershipInfo = CheckList.get(0);
			}
			else{
				throw new C2SException("PT03", "NOT_EXIST_PRICE");
			}
			
			this.PaymentTypeList = EntityUtility.SelectQueryParagraphList(conn, Environment.getQuery("MemberCheck", "SELECT_COMPANY_PAYMENT"), TargetCon);
			
			if(MemberNo.length() > 0){
				/*
				 * Check the Multi Membership and Duplicate
				 */
				
				if(MembershipStartDate.length() > 0){
					Entity CheckCon = new Entity(6);
					
					CheckCon.setField("1", CompanyCd);				// COMPANY_CD
					CheckCon.setField("2", MemberNo);				// MEMBER_NO
					CheckCon.setField("3", FamilyNo);				// FAMILY_NO
					CheckCon.setField("4", MembershipUniqueNbr);	// MEMBERSHIP_UNIQUE_NBR
					CheckCon.setField("5", MembershipType);			// MEMBERSHIP_TYPE
					CheckCon.setField("6", PriceNbr);				// PRICE_NBR
					CheckCon.setField("7", MembershipStartDate);	// MEMBERSHIP_START_DATE
					
					CheckList = EntityUtility.SelectQueryParagraphList(conn, Environment.getQuery("MemberCheck", "SELECT_MEMBERSHIP_CHECK"), CheckCon);
					
					if(CheckList.size() > 0){
						Entity CheckEntity = CheckList.get(0);
						
						if(Utility.StringToInt(CheckEntity.getField("DEFAULT_CNT")) > 0){
							throw new C2SException("PT04", "MULTI_DEFAULT_MEMBERSHIP");
						}
						else if(Utility.StringToInt(CheckEntity.getField("ADDITION_CNT")) > 0){
							throw new C2SException("PT05", "MULTI_ADDITION_MEMBERSHIP");
						}
						else if(Utility.StringToInt(CheckEntity.getField("DUPLICATE_CNT")) > 0){
							throw new C2SException("PT06", "EXIST_MEMBERSHIP_TYPE");
						}
					}
				}
			}
			
			/*
			 * Check the Point
			 */
			Entity PaymentEntity = null;
			
			for(int idx = 0; idx < this.PaymentTypeList.size(); idx++){
				PaymentEntity = this.PaymentTypeList.get(idx);
				
				if(PaymentEntity.getField("PAYMENT_TYPE").equals("000005")){
					String AccumulatePoint = "0";
					long UsablePoint = 0;
					
					if(MemberNo.length() > 0){
						Entity PointEntity = new Entity(4);
						
						PointEntity.setField("MEMBER_NO", MemberNo);
						PointEntity.setField("SYSTEM_TYPE", "MEM");
						PointEntity.setField("SALE_TYPE", SaleType);
						PointEntity.setField("PAYMENT_TYPE", "000005");
						
						PointInfo pi = new PointInfo();
						boolean PayResult = pi.MemberPoint(CompanyCd, PointEntity);
						
						if(PayResult){
							Entity PointInfo = pi.getPointInfo();
							
							if(PointInfo.getField("RSP_CODE").equals("0000")){
								AccumulatePoint = PointInfo.getField("ACCUMULATE_POINT");
								UsablePoint = Utility.StringToLong(PointInfo.getField("USABLE_POINT"));
							}
						}
					}
					
					if(UsablePoint > 0){
						PaymentEntity.setField("ACCUMULATE_POINT", AccumulatePoint);
						PaymentEntity.setField("USABLE_POINT", Long.toString(UsablePoint));
					}
					else{
						this.PaymentTypeList.remove(idx);
					}
					
					break;
				}
			}
			
			result = true;
		}catch(C2SException ce){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".PaymentTypeList()! " + ce.getMessage(), ce, 1);
			this.ErrCode = ce.getExceptCode();
			this.ErrMessage = ce.getMessage();
			result = false;
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".PaymentTypeList()! " + e.getMessage(), e, 1);
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			result = false;
		}finally{
			if(conn != null) conn.close();
		}

		return result;
	}
	
	
	/**
	 * �̸��� �ߺ�üũ
	 * @param emailEntity
	 * @throws C2SException 
	 * @throws SQLException 
	 */
	public boolean checkDuplicateEmail(Entity emailEntity) throws C2SException, SQLException {
		Connection conn = null;
		boolean result = true;
		try{
			conn = DBManager.getConn();
			conn.setAutoCommit(true);
			EntityList entityList = EntityUtility.SelectQueryParagraphList(conn, Environment.getQuery("MemberCheck", "CHECK_DUPLICATE_EMAIL"), emailEntity);
			C2SLog.printConsole(entityList.size() + "--");
			C2SLog.printConsole(entityList.get(0).getField("CNT") + "--");
			if(entityList.size() > 0 && Integer.valueOf(entityList.get(0).getField("CNT").toString()) > 0) result = false;
			
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".AddMembershipSpon()! " + e.getMessage(), e, 1);
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			throw new C2SException(this.ErrCode, this.ErrMessage);
		}finally{
			if(conn != null) conn.close();
		}
		return result;
	}
}