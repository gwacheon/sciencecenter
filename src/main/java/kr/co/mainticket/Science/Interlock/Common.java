package kr.co.mainticket.Science.Interlock;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;

import kr.co.mainticket.Entity.*;
import kr.co.mainticket.Log.*;
import kr.co.mainticket.Science.Config.*;
import kr.co.mainticket.Science.Database.*;
import kr.co.mainticket.Utility.Utility;

public class Common{
	private Entity CompanyInfo;
	private EntityList SaleTypeList;
	private EntityList CommonList;
	private EntityList ZipList;
	private EntityList FileList;
	
	private int CompanyListCount;
	
	private String ErrMessage;
	
	
	public Entity getCompanyInfo(){
		return this.CompanyInfo;
	}
	
	public EntityList getSaleTypeList(){
		return this.SaleTypeList;
	}
	
	public EntityList getCommonList(){
		return this.CommonList;
	}
	
	public EntityList getZipList(){
		return this.ZipList;
	}
	
	public EntityList getFileList(){
		return this.FileList;
	}
	
	public int getCompanyListCount(){
		return this.CompanyListCount;
	}
	
	public String getErrMessage(){
		return Utility.CheckNull(this.ErrMessage).replaceAll("\n", "\\\\n").replaceAll("\"", "'");
	}
	
	
	/**
	 * Get the Index value
	 * @param CompanyCd
	 * @param IndexType
	 * @param SeedDate
	 * @param Interval
	 * @return
	 * @throws Exception
	 */
	public long getIndex(String CompanyCd, String IndexType, String SeedDate, int Interval) throws Exception {
		long IndexValue = 0;
		Connection conn = null;
		CallableStatement cs = null;
		
		try{
			conn = DBManager.getConn();
			
			cs = conn.prepareCall("BEGIN GET_INDEX(?, ?, ?, ?, ?); END;");
			cs.setString(1, CompanyCd);
			cs.setString(2, IndexType);
			cs.setString(3, SeedDate);
			cs.setInt(4, Interval); 
			cs.registerOutParameter(5, Types.INTEGER);			
			cs.execute();
			
			IndexValue = cs.getLong(5);
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "SCIENCE_INFO", "Exception occured on " + this.getClass().getName() + ".getIndex()! " + e.getMessage(), e, 1);
			throw e;
		}finally{
			if(cs != null) cs.close();
			if(conn != null) conn.close();
		}
		
		return IndexValue;
	}
	
	
	/**
	 * Get the Index value
	 * @param CompanyCd
	 * @param IndexType
	 * @param SeedDate
	 * @param Interval
	 * @return
	 * @throws Exception
	 */
	public String getIndexStr(String CompanyCd, String IndexType, String SeedDate, int Interval) throws Exception {
		return Long.toString(getIndex(CompanyCd, IndexType, SeedDate, Interval));
	}
	
	
	/**
	 * Company Information
	 * @param conn
	 * @param CompanyCd
	 * @return
	 * @throws Exception
	 */
	public Entity CompanyInfo(Connection conn, String CompanyCd) throws Exception{
		Entity CompanyEntity = new Entity(1);
		
		CompanyEntity.setField("1", CompanyCd);
		
		EntityList CheckList = EntityUtility.SelectQueryParagraphList(conn, Environment.getQuery("Common", "SELECT_COMPANY_INFO"), CompanyEntity);
		
		if(CheckList.size() != 1){
			throw new Exception("NOT_EXIST_COMPANY_INFO");
		}
		
		return CheckList.get(0);
	}
	
	
	/**
	 * Company Information
	 * @param conn
	 * @param CompanyCd
	 * @return
	 * @throws Exception
	 */
	public Entity CompanyInfoDefault(Connection conn, String CompanyCd) throws Exception{
		Entity CompanyEntity = new Entity(1);
		
		CompanyEntity.setField("1", CompanyCd);
		
		EntityList CheckList = EntityUtility.SelectQueryParagraphList(conn, Environment.getQuery("Common", "SELECT_COMPANY_INFO"), CompanyEntity);
		
		Entity CompanyInfo = null;
		
		if(CheckList.size() == 1){
			CompanyInfo = CheckList.get(0);
		}
		else if(CheckList.size() > 1){
			throw new Exception("EXIST_MANY_COMPANY_INFO");
		}
		else{
			CompanyInfo = new Entity(1);
		}
		
		return CompanyInfo;
	}
	
	
	/**
	 * Company Information
	 * @param CompanyCd
	 * @return
	 * @throws Exception
	 */
	public boolean CompanyInfo(String CompanyCd) throws Exception{
		boolean result = false;
		Connection conn = null;
		
		this.ErrMessage = "";
		
		try{
			conn = DBManager.getConn();
			
			this.CompanyInfo = CompanyInfo(conn, CompanyCd);
			
			result = true;
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "SCIENCE_INFO", "Exception occured on " + this.getClass().getName() + ".CompanyInfo()! " + e.getMessage(), e, 1);
			this.ErrMessage = e.getMessage();
			result = false;
		}finally{
			if(conn != null) conn.close();
		}

		return result;
	}
	
	
	/**
	 * Company Sale Type List
	 * @param conn
	 * @param CompanyCd
	 * @param SystemType
	 * @return
	 * @throws Exception
	 */
	public EntityList CompanySaleTypeList(Connection conn, String CompanyCd, String SystemType) throws Exception{
		Entity CompanyEntity = new Entity(2);
		
		CompanyEntity.setField("1", CompanyCd);
		CompanyEntity.setField("2", SystemType);
		
		return EntityUtility.SelectQueryParagraphList(conn, Environment.getQuery("Common", "SELECT_COMPANY_SALE_TYPE"), CompanyEntity);
	}
	
	
	/**
	 * Company Sale Type List
	 * @param CompanyCd
	 * @param SystemType
	 * @return
	 * @throws Exception
	 */
	public boolean CompanySaleTypeList(String CompanyCd, String SystemType) throws Exception{
		boolean result = false;
		Connection conn = null;
		
		this.ErrMessage = "";
		
		try{
			conn = DBManager.getConn();
			
			this.SaleTypeList = CompanySaleTypeList(conn, CompanyCd, SystemType);
			
			result = true;
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "SCIENCE_INFO", "Exception occured on " + this.getClass().getName() + ".CompanySaleTypeList()! " + e.getMessage(), e, 1);
			this.ErrMessage = e.getMessage();
			result = false;
		}finally{
			if(conn != null) conn.close();
		}

		return result;
	}
	
	
	/**
	 * Common List
	 * @param conn
	 * @param CompanyCd
	 * @param GroupCd
	 * @param AllFlag
	 * @return
	 * @throws Exception
	 */
	public EntityList CommonList(Connection conn, String CompanyCd, String GroupCd, boolean AllFlag) throws Exception{
		Entity GroupEntity = new Entity(2);
		
		GroupEntity.setField("1", CompanyCd);
		GroupEntity.setField("2", GroupCd);
		
		String Paragraph = Environment.getQuery("Common", "SELECT_COMMON");
		
		if(!AllFlag){
			Paragraph = Utility.ReplaceAll(Paragraph, "/*WHERE*/", "AND CM.USE_FLAG = 'Y'");
		}
		
		return EntityUtility.SelectQueryParagraphList(conn, Paragraph, GroupEntity);
	}
	
	
	/**
	 * Common List
	 * @param CompanyCd
	 * @param GroupCd
	 * @param AllFlag
	 * @return
	 * @throws Exception
	 */
	public boolean CommonList(String CompanyCd, String GroupCd, boolean AllFlag) throws Exception{
		boolean result = false;
		Connection conn = null;
		
		this.ErrMessage = "";
		
		try{
			conn = DBManager.getConn();
			
			this.CommonList = CommonList(conn, CompanyCd, GroupCd, AllFlag);
			
			result = true;
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "SCIENCE_INFO", "Exception occured on " + this.getClass().getName() + ".CommonList()! " + e.getMessage(), e, 1);
			this.ErrMessage = e.getMessage();
			result = false;
		}finally{
			if(conn != null) conn.close();
		}

		return result;
	}
	
	
	/**
	 * Zip Code Information
	 * @param conn
	 * @param Keyword
	 * @return
	 * @throws Exception
	 */
	public EntityList ZipInfo(Connection conn, String Keyword) throws Exception{
		Entity ZipEntity = new Entity(2);
		
		ZipEntity.setField("1", "%" + Keyword + "%");
		
		return EntityUtility.SelectQueryParagraphList(conn, Environment.getQuery("Common", "SELECT_ZIP_CODE"), ZipEntity);
	}
	
	
	/**
	 * Zip Code Information
	 * @param Keyword
	 * @return
	 * @throws Exception
	 */
	public boolean ZipInfo(String Keyword) throws Exception{
		boolean result = false;
		Connection conn = null;
		
		this.ErrMessage = "";
		
		try{
			conn = DBManager.getConn();
			
			this.ZipList = ZipInfo(conn, Keyword);
			
			result = true;
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "SCIENCE_INFO", "Exception occured on " + this.getClass().getName() + ".ZipInfo()! " + e.getMessage(), e, 1);
			this.ErrMessage = e.getMessage();
			result = false;
		}finally{
			if(conn != null) conn.close();
		}

		return result;
	}
	
	
	/**
	 * File List
	 * @param conn
	 * @param CompanyCd
	 * @param FileType
	 * @param FileIdx
	 * @param FileNbr
	 * @return
	 * @throws Exception
	 */
	public EntityList FileList(Connection conn, String CompanyCd, String FileType, String FileIdx, String FileNbr) throws Exception{
		Entity FileEntity = new Entity(4);
		
		FileEntity.setField("1", CompanyCd);
		FileEntity.setField("2", FileType);
		FileEntity.setField("3", FileIdx);
		FileEntity.setField("4", FileNbr);
		
		return EntityUtility.SelectQueryParagraphList(conn, Environment.getQuery("Common", "SELECT_FILE_NBR"), FileEntity);
	}
	
	
	/**
	 * File List
	 * @param CompanyCd
	 * @param FileType
	 * @param FileIdx
	 * @param FileNbr
	 * @return
	 * @throws Exception
	 */
	public boolean FileList(String CompanyCd, String FileType, String FileIdx, String FileNbr) throws Exception{
		boolean result = false;
		Connection conn = null;
		
		this.ErrMessage = "";
		
		try{
			conn = DBManager.getConn();
			
			this.FileList = FileList(conn, CompanyCd, FileType, FileIdx, FileNbr);
			
			result = true;
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "SCIENCE_INFO", "Exception occured on " + this.getClass().getName() + ".FileList()! " + e.getMessage(), e, 1);
			this.ErrMessage = e.getMessage();
			result = false;
		}finally{
			if(conn != null) conn.close();
		}

		return result;
	}
	
	
	/**
	 * File List
	 * @param conn
	 * @param CompanyCd
	 * @param FileType
	 * @param FileIdx
	 * @return
	 * @throws Exception
	 */
	public EntityList FileList(Connection conn, String CompanyCd, String FileType, String FileIdx) throws Exception{
		Entity FileEntity = new Entity(3);
		
		FileEntity.setField("1", CompanyCd);
		FileEntity.setField("2", FileType);
		FileEntity.setField("3", FileIdx);
		
		return EntityUtility.SelectQueryParagraphList(conn, Environment.getQuery("Common", "SELECT_FILE"), FileEntity);
	}
	
	
	/**
	 * File List
	 * @param CompanyCd
	 * @param FileType
	 * @param FileIdx
	 * @return
	 * @throws Exception
	 */
	public boolean FileList(String CompanyCd, String FileType, String FileIdx) throws Exception{
		boolean result = false;
		Connection conn = null;
		
		this.ErrMessage = "";
		
		try{
			conn = DBManager.getConn();
			
			this.FileList = FileList(conn, CompanyCd, FileType, FileIdx);
			
			result = true;
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "SCIENCE_INFO", "Exception occured on " + this.getClass().getName() + ".FileList()! " + e.getMessage(), e, 1);
			this.ErrMessage = e.getMessage();
			result = false;
		}finally{
			if(conn != null) conn.close();
		}

		return result;
	}
	
	
	/**
	 * Insert File
	 * @param conn
	 * @param FileEntity
	 * @throws Exception
	 */
	public void InsertFile(Connection conn, Entity FileEntity) throws Exception{
		Entity TranEntity = new Entity(13);
		
		TranEntity.setField("1", FileEntity.getField("COMPANY_CD"));
		TranEntity.setField("2", FileEntity.getField("FILE_TYPE"));
		TranEntity.setField("3", FileEntity.getField("FILE_IDX"));
		TranEntity.setField("4", FileEntity.getField("COMPANY_CD"));
		TranEntity.setField("5", FileEntity.getField("FILE_TYPE"));
		TranEntity.setField("6", FileEntity.getField("FILE_IDX"));
		TranEntity.setField("7", FileEntity.getFieldNoCheck("FILE_PATH"));
		TranEntity.setField("8", FileEntity.getFieldNoCheck("FILE_NAME"));
		TranEntity.setField("9", FileEntity.getFieldNoCheck("FILE_URL"));
		TranEntity.setField("10", "Y");
		TranEntity.setField("11", FileEntity.getFieldNoCheck("INSERT_USER"));
		TranEntity.setField("12", FileEntity.getFieldNoCheck("INSERT_DATE"));
		TranEntity.setField("13", FileEntity.getFieldNoCheck("INSERT_TIME"));
		
		EntityUtility.UpdateQueryParagraph(conn, Environment.getQuery("Common", "INSERT_FILE"), TranEntity);
	}
	
	
	/**
	 * Delete File
	 * @param conn
	 * @param FileEntity
	 * @throws Exception
	 */
	public void DeleteFile(Connection conn, Entity FileEntity) throws Exception{
		Entity TranEntity = new Entity(8);
		
		TranEntity.setField("1", "N");
		TranEntity.setField("2", FileEntity.getFieldNoCheck("INSERT_USER"));
		TranEntity.setField("3", FileEntity.getFieldNoCheck("INSERT_DATE"));
		TranEntity.setField("4", FileEntity.getFieldNoCheck("INSERT_TIME"));
		TranEntity.setField("5", FileEntity.getField("COMPANY_CD"));
		TranEntity.setField("6", FileEntity.getField("FILE_TYPE"));
		TranEntity.setField("7", FileEntity.getField("FILE_IDX"));
		TranEntity.setField("8", FileEntity.getField("FILE_NBR"));
		
		EntityUtility.UpdateQueryParagraph(conn, Environment.getQuery("Common", "UPDATE_FILE"), TranEntity);
	}
	
	
	/**
	 * Delete All Files
	 * @param conn
	 * @param FileEntity
	 * @throws Exception
	 */
	public void DeleteAllFile(Connection conn, Entity FileEntity) throws Exception{
		Entity TranEntity = new Entity(7);
		
		TranEntity.setField("1", "N");
		TranEntity.setField("2", FileEntity.getFieldNoCheck("INSERT_USER"));
		TranEntity.setField("3", FileEntity.getFieldNoCheck("INSERT_DATE"));
		TranEntity.setField("4", FileEntity.getFieldNoCheck("INSERT_TIME"));
		TranEntity.setField("5", FileEntity.getField("COMPANY_CD"));
		TranEntity.setField("6", FileEntity.getField("FILE_TYPE"));
		TranEntity.setField("7", FileEntity.getField("FILE_IDX"));
		
		EntityUtility.UpdateQueryParagraph(conn, Environment.getQuery("Common", "UPDATE_FILE_ALL"), TranEntity);
	}
}