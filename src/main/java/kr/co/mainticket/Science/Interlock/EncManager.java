package kr.co.mainticket.Science.Interlock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kr.co.mainticket.Utility.Utility;

import com.Ineb.Dguard.DguardManager;

/**
 * Set the System Environment
 */
public class EncManager{
	private static String enc = kr.go.sciencecenter.util.Utility.getConfigProperty("legacy.enc");
	private static DguardManager dgmgr;
	
	private static final Logger logger = LoggerFactory.getLogger(EncManager.class);
	
	
	/**
	 * Initialize Environments
	 */
	public static void Initialize(){
		
		logger.info("EncManager.Initialize -- !" + enc);
		
		String path = "";
		if(enc.equals("real")) path = "/app/ineb/etc/WasAgent2.txt";
		
		try{
			dgmgr = DguardManager.Init("gnsmdb_real", "agent!1234", "agent!1234", path.trim());
			logger.info("dgmgr == " + dgmgr.toString());
		}catch(Exception e){
			e.printStackTrace();
			dgmgr = null;
		}
			
	}
	
	public static String getEncData(String TableName, String ColumnName, String TargetData){
		
		logger.info("EncManager.getEncData -- " + TableName + " " + ColumnName + " " + TargetData);
		
		if(dgmgr == null)	if(enc.equals("real")) Initialize();
		
		String DB_SID = "GNSMDB";
		String DB_USER = "SCIENCECENTER";
		
		String EncData = TargetData;
		if(enc.equals("real")) EncData = dgmgr.Encrypt(DB_SID, DB_USER, TableName, ColumnName, Utility.CheckNull(TargetData));
			
		logger.info("EncData == " + EncData);
		
		return EncData;
	}
	
	public static String getDecData(String TableName, String ColumnName, String TargetData){
		
		
		if(dgmgr == null)	if(enc.equals("real")) Initialize();
		
		String DB_SID = "GNSMDB";
		String DB_USER = "SCIENCECENTER";
		
		String DecData = TargetData;
		if(enc.equals("real")) DecData = dgmgr.Decrypt(DB_SID, DB_USER, TableName, ColumnName, Utility.CheckNull(TargetData));

		logger.info("DecData == " + DecData);
		
		return DecData;
	}
}
