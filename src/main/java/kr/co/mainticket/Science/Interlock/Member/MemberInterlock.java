package kr.co.mainticket.Science.Interlock.Member;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.util.Random;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import kr.co.mainticket.Encrypt.AESCoder;
import kr.co.mainticket.Entity.*;
import kr.co.mainticket.Exception.C2SException;
import kr.co.mainticket.Log.*;
import kr.co.mainticket.Science.Config.Environment;
import kr.co.mainticket.Science.Database.DBManager;
import kr.co.mainticket.Science.Interlock.Common;
import kr.co.mainticket.Science.Interlock.XmlParser;
import kr.co.mainticket.Utility.DateTime;
import kr.co.mainticket.Utility.Utility;

public class MemberInterlock{
	private Entity InterlockInfo;
	private String ErrCode;
	private String ErrMessage;
	
	
	public Entity getInterlockInfo(){
		return InterlockInfo;
	}
	
	public String getErrCode(){
		return Utility.CheckNull(this.ErrCode);
	}
	
	public String getErrMessage(){
		return Utility.CheckNull(this.ErrMessage).replaceAll("\n", "\\\\n").replaceAll("\"", "'");
	}
	
	
	
	/**
	 * Interlock Data
	 * @param InterlockUrl
	 * @param InterlockType
	 * @param XmlData
	 * @return
	 */
	public Entity getInterlockData(String InterlockUrl, String InterlockType, StringBuffer XmlData){
		Entity InterlockData = null;
		
		try{
			int[] IdxArray = {8, 11, 19, 27};
			
			Random RandomKeygen = new Random();
			int RandomIdx = RandomKeygen.nextInt(4);
			
			int KeyIdx = IdxArray[RandomIdx];
			
			HttpParams hp = new BasicHttpParams();
			hp.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
			
			HttpClient InterlockClient = new DefaultHttpClient(hp);
			HttpPost post = new HttpPost(InterlockUrl);

			post.setHeader("Content-Type", "text/xml");
			post.setHeader("INTERLOCK_CD", "GNSM");
			post.setHeader("INTERLOCK_TYPE", InterlockType);
			post.setHeader("KEY_IDX", Integer.toString(KeyIdx));
			post.setHeader("ENCODING", "UTF-8");
			
			String ReqData = AESCoder.AESEncryptString(XmlData.toString(), KeyIdx);
			
			StringEntity body = new StringEntity(ReqData, "UTF-8");
			post.setEntity(body);
			
			HttpResponse HResponse = InterlockClient.execute(post);
			
			StringBuffer RspData = new StringBuffer();
			
			int StatusCode = HResponse.getStatusLine().getStatusCode();
			
			if(StatusCode == 200){
				HttpEntity entity = HResponse.getEntity();
				
				BufferedReader br = null;
				String BuffLine = null;
				
				try{
				
					br = new BufferedReader(new InputStreamReader(entity.getContent(), "UTF-8"));
					
					while((BuffLine = br.readLine()) != null){
						RspData.append(BuffLine);
					}
				}catch(Exception e){
					e.printStackTrace();
					RspData = new StringBuffer();
				}finally{
					if(br != null){br.close();}
				}
			}
			else{
				InterlockData = new Entity(2);
				
				InterlockData.setField("RSP_CD", "ERROR");
				InterlockData.setField("RSP_MSG", "WRONG_STATUS_CODE[" + StatusCode + "]");
			}
			
			InterlockClient.getConnectionManager().shutdown();
			
			if(StatusCode == 200 && RspData.length() > 0){
				Header RspHeader = HResponse.getFirstHeader("KEY_IDX");
				
				if(RspHeader != null){
					int RspKeyIdx = Utility.StringToInt(RspHeader.getValue());
					
					String RspXmlData = "";
					
					if(RspKeyIdx == -1){
						RspXmlData = RspData.toString();
					}
					else if(RspKeyIdx == 8 || RspKeyIdx == 11 || RspKeyIdx == 19 || RspKeyIdx == 27){
						RspXmlData = AESCoder.AESDecryptString(RspData.toString(), RspKeyIdx);
					}
					else{
						InterlockData = new Entity(2);
						
						InterlockData.setField("RSP_CD", "ERROR");
						InterlockData.setField("RSP_MSG", "WORONG_KEY_IDX");
					}
					
					if(RspXmlData.length() > 0){
						Document XmlDoc = Utility.loadXmlString(RspXmlData);
						
						NodeList XmlNodeList = XmlDoc.getFirstChild().getChildNodes();
						
						InterlockData = XmlParser.ParseXmlToEntity(XmlNodeList);
						
						if(!InterlockData.getFieldExist("RSP_CD")){
							InterlockData.setField("RSP_CD", "0000");
							InterlockData.setField("RSP_MSG", "");
						}
						
						StringBuffer LogData = new StringBuffer();
						LogData.append("\n##########################");
						LogData.append("\nINTERLOCK_URL : " + InterlockUrl);
						LogData.append("\nREQUEST_DATA : " + XmlData);
						LogData.append("\nRESPONSE DATA : " + RspXmlData);
						LogData.append("\n##########################");
						
						C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_SITE", LogData.toString(), null, 2);
					}
				}
				else{
					InterlockData = new Entity(2);
					
					InterlockData.setField("RSP_CD", "ERROR");
					InterlockData.setField("RSP_MSG", "NOT_EXIST_KEY_IDX");
				}
			}
			else{
				InterlockData = new Entity(2);
				
				InterlockData.setField("RSP_CD", "ERROR");
				InterlockData.setField("RSP_MSG", "NOT_EXIST_RESPONSE_DATA");
			}
		}catch(Exception e){
			e.printStackTrace();
			
			try{
				InterlockData = new Entity(2);
			
				InterlockData.setField("RSP_CD", "ERROR");
				InterlockData.setField("RSP_MSG", e.getMessage());
			}catch(Exception ec){
				InterlockData = new Entity();
			}
		}
		
		return InterlockData;
	}
	
	
	/**
	 * Interlock Url
	 * @param InterlockCd
	 * @return
	 * @throws Exception
	 */
	public String getInterlockUrl(String InterlockCd) throws Exception{
		String InterlockUrl = "";
		Connection conn = null;
		
		if(InterlockCd.length() == 0){
			throw new C2SException("GI01", "NOT_EXIST_INTERLOCK_CD");
		}
		
		try{
			conn = DBManager.getConn();
			
			Entity CheckCon = new Entity(5);
			
			CheckCon.setField("1", InterlockCd);
			
			EntityList CheckList = EntityUtility.SelectQueryParagraphList(conn, Environment.getQuery("MemberInterlock", "SELECT_INTERLOCK"), CheckCon);
			Entity CheckEntity = null;
			
			if(CheckList.size() > 0){
				CheckEntity = CheckList.get(0);
				
				InterlockUrl = CheckEntity.getField("INTERLOCK_URL");
			}
			else{
				throw new C2SException("GI02", "NOT_EXIST_INTERLOCK");
			}
		}finally{
			if(conn != null) conn.close();
		}

		return InterlockUrl;
	}
	
	
	/**
	 * Check Interlock
	 * @param CompanyCd
	 * @param MemberNo
	 * @param InterlockCd
	 * @param InterlockMemberNo
	 * @param InterlockMemberId
	 * @return
	 * @throws Exception
	 */
	public boolean CheckInterlock(String CompanyCd, String MemberNo, String InterlockCd, String InterlockMemberNo, String InterlockMemberId) throws Exception{
		boolean CheckFlag = false;
		Connection conn = null;
		
		try{
			conn = DBManager.getConn();
			
			EntityList CheckList = null;
			Entity CheckCon = null;
			
			if(MemberNo.length() > 0){
				CheckCon = new Entity(5);
				
				CheckCon.setField("1", CompanyCd);
				CheckCon.setField("2", MemberNo);
				CheckCon.setField("3", InterlockCd);
				
				CheckList = EntityUtility.SelectQueryParagraphList(conn, Environment.getQuery("MemberInterlock", "SELECT_INTERLOCK_MAP_MEMBER"), CheckCon);
				
				if(CheckList.size() > 0){
					throw new Exception("EXIST_INTERLOCK_MAP");
				}
			}
			
			if(InterlockMemberId.length() > 0){
				CheckCon = new Entity(5);
				
				CheckCon.setField("1", InterlockCd);
				CheckCon.setField("2", InterlockMemberId);
				
				CheckList = EntityUtility.SelectQueryParagraphList(conn, Environment.getQuery("MemberInterlock", "SELECT_INTERLOCK_MAP_MEMBER_ID"), CheckCon);
				
				if(CheckList.size() > 0){
					throw new Exception("EXIST_INTERLOCK_MAP");
				}
			}
			
			CheckFlag = true;
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".CheckInterlock()! " + e.getMessage(), e, 1);
			CheckFlag = false;
		}finally{
			if(conn != null) conn.close();
		}

		return CheckFlag;
	}
	
	
	/**
	 * Insert Interlock Log
	 * @param conn
	 * @param CompanyCd
	 * @param MemberNo
	 * @param InterlockCd
	 * @param InterlockType
	 * @param InterlockData
	 * @param ProcessFlag
	 * @param ProcessMsg
	 * @return
	 * @throws Exception
	 */
	public boolean InsertInterlockLog(Connection conn, String CompanyCd, String MemberNo, String InterlockCd, String InterlockType, String InterlockData, String ProcessFlag, String ProcessMsg) throws Exception{
		boolean CheckFlag = false;
		
		try{
			String CurrDate = DateTime.getFormatDateString("yyyyMMddHHmmss", 0);
			
			Common cm = new Common();
			String InterlockUniqueNbr = "IL" +  CurrDate.substring(2, 8) + CompanyCd + Utility.LPAD(cm.getIndex(CompanyCd, "INTERLOCK_UNIQUE_NBR", CurrDate.substring(0, 8), 1), 6, '0');
			
			Entity InterlockEntity = new Entity(10);
			
			InterlockEntity.setField("1", CompanyCd);					// COMPANY_CD
			InterlockEntity.setField("2", InterlockUniqueNbr);
			InterlockEntity.setField("3", MemberNo);
			InterlockEntity.setField("4", InterlockCd);
			InterlockEntity.setField("5", InterlockType);
			InterlockEntity.setField("6", InterlockData);
			InterlockEntity.setField("7", CurrDate.substring(0, 8));
			InterlockEntity.setField("8", CurrDate.substring(8));
			InterlockEntity.setField("9", ProcessFlag);
			InterlockEntity.setField("10", ProcessMsg);
			
			int UpdateCount = EntityUtility.UpdateQueryParagraph(conn, Environment.getQuery("MemberInterlock", "INSERT_INTERLOCK_LOG"), InterlockEntity);
			
			if(UpdateCount != 1){
				throw new Exception("NOT_INSERT_INTERLOCK_LOG");
			}
			
			CheckFlag = true;
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".InsertInterlockLog()! " + e.getMessage(), e, 1);
			CheckFlag = false;
		}

		return CheckFlag;
	}
	
	
	/**
	 * Insert Interlock Log
	 * @param CompanyCd
	 * @param MemberNo
	 * @param InterlockCd
	 * @param InterlockType
	 * @param InterlockData
	 * @param ProcessFlag
	 * @param ProcessMsg
	 * @return
	 * @throws Exception
	 */
	public boolean InsertInterlockLog(String CompanyCd, String MemberNo, String InterlockCd, String InterlockType, String InterlockData, String ProcessFlag, String ProcessMsg) throws Exception{
		boolean CheckFlag = false;
		Connection conn = null;
		
		try{
			conn = DBManager.getConn();
			conn.setAutoCommit(false);
			
			boolean UpdateResult = InsertInterlockLog(conn, CompanyCd, MemberNo, InterlockCd, InterlockType, InterlockData, ProcessFlag, ProcessMsg);
			
			conn.commit();
			CheckFlag = true;
		}catch(Exception e){
			if(conn != null) conn.rollback();
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".InsertInterlockLog()! " + e.getMessage(), e, 1);
			CheckFlag = false;
		}finally{
			if(conn != null) conn.close();
		}

		return CheckFlag;
	}
	
	
	/**
	 * Insert Interlock
	 * @param CompanyCd
	 * @param MemberNo
	 * @param InterlockCd
	 * @param InterlockMemberNo
	 * @param InterlockMemberId
	 * @return
	 * @throws Exception
	 */
	public boolean InsertInterlock(String CompanyCd, String MemberNo, String InterlockCd, String InterlockMemberNo, String InterlockMemberId) throws Exception{
		boolean CheckFlag = false;
		Connection conn = null;
		
		try{
			conn = DBManager.getConn();
			conn.setAutoCommit(false);
			
			String CurrDate = DateTime.getFormatDateString("yyyyMMddHHmmss", 0);
			
			Entity InterlockEntity = new Entity(10);
			
			InterlockEntity.setField("1", CompanyCd);					// COMPANY_CD
			InterlockEntity.setField("2", MemberNo);
			InterlockEntity.setField("3", InterlockCd);
			InterlockEntity.setField("4", InterlockMemberNo);
			InterlockEntity.setField("5", InterlockMemberId);
			InterlockEntity.setField("6", "Y");
			InterlockEntity.setField("7", CurrDate.substring(0, 8));
			InterlockEntity.setField("8", CurrDate.substring(8));
			
			int UpdateCount = EntityUtility.UpdateQueryParagraph(conn, Environment.getQuery("MemberInterlock", "INSERT_INTERLOCK_MAP"), InterlockEntity);
			
			if(UpdateCount != 1){
				throw new Exception("NOT_INSERT_INTERLOCK_MAP");
			}
			
			conn.commit();
			CheckFlag = true;
		}catch(Exception e){
			if(conn != null) conn.rollback();
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".InsertInterlock()! " + e.getMessage(), e, 1);
			CheckFlag = false;
		}finally{
			if(conn != null) conn.close();
		}

		return CheckFlag;
	}
	
	
	/**
	 * Check Member Id
	 * @param DataEntity
	 * @return
	 * @throws Exception
	 */
	public boolean CheckId(Entity DataEntity) throws Exception{
		boolean result = false;
		
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try{
			String InterlockCd = DataEntity.getFieldNoCheck("INTERLOCK_CD");
			String InterlockMemberId = DataEntity.getFieldNoCheck("INTERLOCK_MEMBER_ID");
			
			if(InterlockCd.length() == 0){
				throw new C2SException("CI01", "NOT_EXIST_INTERLOCK_CD");
			}
			else if(InterlockMemberId.length() == 0){
				throw new C2SException("CI02", "NOT_EXIST_INTERLOCK_MEMBER_ID");
			}
			
			String InterlockUrl = getInterlockUrl(InterlockCd);
			
			String InterlockType = "CHECK_ID";
			StringBuffer XmlData = new StringBuffer();
			
			XmlData.append("<MEMBER>");
			XmlData.append("<MEMBER_ID>" + InterlockMemberId + "</MEMBER_ID>");
			XmlData.append("</MEMBER>");
			
			this.InterlockInfo = getInterlockData(InterlockUrl, InterlockType, XmlData);
			
			if(!this.InterlockInfo.getFieldNoCheck("RSP_CD").equals("0000")){
				throw new C2SException("CI03", this.InterlockInfo.getFieldNoCheck("RSP_MSG"));
			}
			
			result = true;
		}catch(C2SException ce){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".CheckId()! " + ce.getMessage(), ce, 1);
			this.ErrCode = ce.getExceptCode();
			this.ErrMessage = ce.getMessage();
			result = false;
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".CheckId()! " + e.getMessage(), e, 1);
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			result = false;
		}

		return result;
	}
	
	
	/**
	 * Member Information
	 * @param DataEntity
	 * @return
	 * @throws Exception
	 */
	public boolean MemberInfo(Entity DataEntity) throws Exception{
		boolean result = false;
		
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try{
			String InterlockCd = DataEntity.getFieldNoCheck("INTERLOCK_CD");
			String InterlockMemberId = DataEntity.getFieldNoCheck("INTERLOCK_MEMBER_ID");
			String Password = DataEntity.getFieldNoCheck("PASSWORD");
			
			if(InterlockCd.length() == 0){
				throw new C2SException("MI01", "NOT_EXIST_INTERLOCK_CD");
			}
			else if(InterlockMemberId.length() == 0){
				throw new C2SException("MI02", "NOT_EXIST_INTERLOCK_MEMBER_ID");
			}
			
			String InterlockUrl = getInterlockUrl(InterlockCd);
			
			String InterlockType = "";
			StringBuffer XmlData = new StringBuffer();
			
			if(Password.length() > 0){
				InterlockType = "INFO_PW";
				
				XmlData.append("<MEMBER>");
				XmlData.append("<MEMBER_ID>" + InterlockMemberId + "</MEMBER_ID>");
				XmlData.append("<PASSWORD>" + Password + "</PASSWORD>");
				XmlData.append("</MEMBER>");
			}
			else{
				InterlockType = "INFO_ID";
				
				XmlData.append("<MEMBER>");
				XmlData.append("<MEMBER_ID>" + InterlockMemberId + "</MEMBER_ID>");
				XmlData.append("</MEMBER>");
			}
			
			this.InterlockInfo = getInterlockData(InterlockUrl, InterlockType, XmlData);
			
			if(this.InterlockInfo.getFieldNoCheck("RSP_CD").equals("0000")){
				boolean CheckFlag = CheckInterlock("", "", InterlockCd, "", InterlockMemberId);
				
				if(!CheckFlag){
					throw new C2SException("MI03", "error");
				}
			}
			else{
				throw new C2SException("MI04", this.InterlockInfo.getFieldNoCheck("RSP_MSG"));
			}
			
			result = true;
		}catch(C2SException ce){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".MemberInfo()! " + ce.getMessage(), ce, 1);
			this.ErrCode = ce.getExceptCode();
			this.ErrMessage = ce.getMessage();
			result = false;
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".MemberInfo()! " + e.getMessage(), e, 1);
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			result = false;
		}

		return result;
	}
	
	
	/**
	 * Member Regist
	 * @param DataEntity
	 * @return
	 * @throws Exception
	 */
	public boolean MemberRegist(String CompanyCd, Entity DataEntity) throws Exception{
		boolean result = false;
		
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try{
			String InterlockCd = DataEntity.getFieldNoCheck("INTERLOCK_CD");
			String MemberNo = DataEntity.getFieldNoCheck("MEMBER_NO");
			String InterlockMemberId = DataEntity.getFieldNoCheck("INTERLOCK_MEMBER_ID");
			String Password = DataEntity.getFieldNoCheck("PASSWORD");
			String MemberName = DataEntity.getFieldNoCheck("MEMBER_NAME");
			
			if(InterlockCd.length() == 0){
				throw new C2SException("MR01", "NOT_EXIST_INTERLOCK_CD");
			}
			else if(MemberNo.length() == 0){
				throw new C2SException("MR02", "NOT_EXIST_MEMBER_NO");
			}
			else if(InterlockMemberId.length() == 0){
				throw new C2SException("MR03", "NOT_EXIST_INTERLOCK_MEMBER_ID");
			}
			else if(MemberName.length() == 0){
				throw new C2SException("MR04", "NOT_EXIST_MEMBER_NAME");
			}
			
			boolean CheckFlag = CheckInterlock(CompanyCd, MemberNo, InterlockCd, "", "");
			
			if(!CheckFlag){
				throw new C2SException("MR04", "EXIST_INTERLOCK_MAP");
			}
			
			String InterlockUrl = getInterlockUrl(InterlockCd);
			
			String InterlockType = "REGIST";
			StringBuffer XmlData = new StringBuffer();
			
			XmlData.append("<MEMBER>");
			XmlData.append("<MEMBER_ID>" + InterlockMemberId + "</MEMBER_ID>");
			XmlData.append("<PASSWORD>" + Password + "</PASSWORD>");
			XmlData.append("<MEMBER_NAME><![CDATA[" + MemberName + "]]></MEMBER_NAME>");
			XmlData.append("<BIRTH_DATE>" + DataEntity.getFieldNoCheck("BIRTH_DATE") + "</BIRTH_DATE>");
			XmlData.append("<MEMBER_CEL>" + DataEntity.getFieldNoCheck("MEMBER_CEL") + "</MEMBER_CEL>");
			XmlData.append("<MEMBER_EMAIL>" + DataEntity.getFieldNoCheck("MEMBER_EMAIL") + "</MEMBER_EMAIL>");
			XmlData.append("<MEMBER_ZIP_CD>" + DataEntity.getFieldNoCheck("MEMBER_ZIP_CD") + "</MEMBER_ZIP_CD>");
			XmlData.append("<MEMBER_ADDR1><![CDATA[" + DataEntity.getFieldNoCheck("MEMBER_ADDR1") + "]]></MEMBER_ADDR1>");
			XmlData.append("<MEMBER_ADDR2><![CDATA[" + DataEntity.getFieldNoCheck("MEMBER_ADDR2") + "]]></MEMBER_ADDR2>");
			XmlData.append("</MEMBER>");
			
			this.InterlockInfo = getInterlockData(InterlockUrl, InterlockType, XmlData);
			
			InsertInterlockLog(CompanyCd, MemberNo, InterlockCd, InterlockType, "", (this.InterlockInfo.getFieldNoCheck("RSP_CD").equals("0000") || this.InterlockInfo.getFieldNoCheck("RSP_CD").equals("0")?"Y":"F"), this.InterlockInfo.getFieldNoCheck("RSP_MSG"));
			
			if(this.InterlockInfo.getFieldNoCheck("RSP_CD").equals("0000") || this.InterlockInfo.getFieldNoCheck("RSP_CD").equals("0")){
				boolean RegistFlag = InsertInterlock(CompanyCd, MemberNo, InterlockCd, this.InterlockInfo.getFieldNoCheck("MEMBER_NO"), this.InterlockInfo.getFieldNoCheck("MEMBER_ID"));
			}
			else{
				throw new C2SException("MR05", this.InterlockInfo.getFieldNoCheck("RSP_MSG"));
			}
			
			result = true;
		}catch(C2SException ce){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".MemberRegist()! " + ce.getMessage(), ce, 1);
			this.ErrCode = ce.getExceptCode();
			this.ErrMessage = ce.getMessage();
			result = false;
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".MemberRegist()! " + e.getMessage(), e, 1);
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			result = false;
		}

		return result;
	}
	
	
	/**
	 * Member Mapping
	 * @param DataEntity
	 * @return
	 * @throws Exception
	 */
	public boolean MemberMapping(String CompanyCd, Entity DataEntity) throws Exception{
		boolean result = false;
		
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try{
			String InterlockCd = DataEntity.getFieldNoCheck("INTERLOCK_CD");
			String MemberNo = DataEntity.getFieldNoCheck("MEMBER_NO");
			String InterlockMemberNo = DataEntity.getFieldNoCheck("INTERLOCK_MEMBER_NO");
			String InterlockMemberId = DataEntity.getFieldNoCheck("INTERLOCK_MEMBER_ID");
			
			if(InterlockCd.length() == 0){
				throw new C2SException("MP01", "NOT_EXIST_INTERLOCK_CD");
			}
			else if(InterlockMemberId.length() == 0){
				throw new C2SException("MP02", "NOT_EXIST_INTERLOCK_MEMBER_ID");
			}
			
			if(!InterlockCd.equals("21CYSC")){
				boolean CheckFlag = CheckInterlock(CompanyCd, MemberNo, InterlockCd, InterlockMemberNo, InterlockMemberId);
				
				if(!CheckFlag){
					throw new C2SException("MP03", "EXIST_INTERLOCK_MAP");
				}
				
				String InterlockUrl = getInterlockUrl(InterlockCd);
				
				String InterlockType = "MAPPING";
				StringBuffer XmlData = new StringBuffer();
				
				XmlData.append("<MEMBER>");
				XmlData.append("<MEMBER_NO>" + InterlockMemberNo + "</MEMBER_NO>");
				XmlData.append("<MEMBER_ID>" + InterlockMemberId + "</MEMBER_ID>");
				XmlData.append("</MEMBER>");
				
				this.InterlockInfo = getInterlockData(InterlockUrl, InterlockType, XmlData);
				
				InsertInterlockLog(CompanyCd, MemberNo, InterlockCd, InterlockType, "", (this.InterlockInfo.getFieldNoCheck("RSP_CD").equals("0000") || this.InterlockInfo.getFieldNoCheck("RSP_CD").equals("0")?"Y":"F"), this.InterlockInfo.getFieldNoCheck("RSP_MSG"));
				
				if(this.InterlockInfo.getFieldNoCheck("RSP_CD").equals("0000") || this.InterlockInfo.getFieldNoCheck("RSP_CD").equals("0")){
					boolean RegistFlag = InsertInterlock(CompanyCd, MemberNo, InterlockCd, this.InterlockInfo.getFieldNoCheck("MEMBER_NO"), this.InterlockInfo.getFieldNoCheck("MEMBER_ID"));
				}
				else{
					throw new C2SException("MP03", this.InterlockInfo.getFieldNoCheck("RSP_MSG"));
				}
			}
			else{
				boolean RegistFlag = InsertInterlock(CompanyCd, MemberNo, InterlockCd, InterlockMemberNo, InterlockMemberId);
			}
			
			result = true;
		}catch(C2SException ce){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".MemberMapping()! " + ce.getMessage(), ce, 1);
			this.ErrCode = ce.getExceptCode();
			this.ErrMessage = ce.getMessage();
			result = false;
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".MemberMapping()! " + e.getMessage(), e, 1);
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			result = false;
		}

		return result;
	}
	
	
	/**
	 * Member Login
	 * @param DataEntity
	 * @return
	 * @throws Exception
	 */
	public boolean MemberLogin(Entity DataEntity) throws Exception{
		boolean result = false;
		
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try{
			String InterlockCd = DataEntity.getFieldNoCheck("INTERLOCK_CD");
			String InterlockMemberNo = DataEntity.getFieldNoCheck("INTERLOCK_MEMBER_NO");
			String InterlockMemberId = DataEntity.getFieldNoCheck("INTERLOCK_MEMBER_ID");
			
			if(InterlockCd.length() == 0){
				throw new C2SException("ML01", "NOT_EXIST_INTERLOCK_CD");
			}
			else if(InterlockMemberId.length() == 0){
				throw new C2SException("ML02", "NOT_EXIST_INTERLOCK_MEMBER_ID");
			}
			
			String InterlockUrl = getInterlockUrl(InterlockCd);
			
			String InterlockType = "LOGIN";
			StringBuffer XmlData = new StringBuffer();
			
			XmlData.append("<MEMBER>");
			XmlData.append("<MEMBER_NO>" + InterlockMemberNo + "</MEMBER_NO>");
			XmlData.append("<MEMBER_ID>" + InterlockMemberId + "</MEMBER_ID>");
			XmlData.append("</MEMBER>");
			
			this.InterlockInfo = getInterlockData(InterlockUrl, InterlockType, XmlData);
			
			result = true;
		}catch(C2SException ce){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".MemberLogin()! " + ce.getMessage(), ce, 1);
			this.ErrCode = ce.getExceptCode();
			this.ErrMessage = ce.getMessage();
			result = false;
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".MemberLogin()! " + e.getMessage(), e, 1);
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			result = false;
		}

		return result;
	}
	
	
	/**
	 * Member Modify
	 * @param DataEntity
	 * @return
	 * @throws Exception
	 */
	public boolean MemberModify(Connection conn, String CompanyCd, Entity DataEntity) throws Exception{
		boolean result = false;
		
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try{
			String MemberNo = DataEntity.getFieldNoCheck("MEMBER_NO");
			
			if(MemberNo.length() == 0){
				throw new C2SException("MM01", "NOT_EXIST_MEMBER_NO");
			}
			
			Entity CheckCon = new Entity(2);
			
			CheckCon.setField("1", CompanyCd);
			CheckCon.setField("2", MemberNo);
			
			EntityList CheckList = EntityUtility.SelectQueryParagraphList(conn, Environment.getQuery("MemberInterlock", "SELECT_INTERLOCK_MAP_MEMBER_ALL"), CheckCon);
			
			if(CheckList.size() > 0){
				StringBuffer XmlData = new StringBuffer();
				
				if(DataEntity.getFieldNoCheck("PASSWORD").length() > 0){
					XmlData.append("<PASSWORD>" + DataEntity.getFieldNoCheck("PASSWORD") + "</PASSWORD>");
				}
				
				if(DataEntity.getFieldNoCheck("BIRTH_DATE").length() > 0){
					XmlData.append("<BIRTH_DATE>" + DataEntity.getFieldNoCheck("BIRTH_DATE") + "</BIRTH_DATE>");
				}
				
				if(DataEntity.getFieldNoCheck("MEMBER_CEL").length() > 0){
					XmlData.append("<MEMBER_CEL>" + DataEntity.getFieldNoCheck("MEMBER_CEL") + "</MEMBER_CEL>");
				}
				
				if(DataEntity.getFieldNoCheck("MEMBER_EMAIL").length() > 0){
					//XmlData.append("<MEMBER_EMAIL>" + DataEntity.getFieldNoCheck("MEMBER_EMAIL") + "</MEMBER_EMAIL>");
				}
				
				if(DataEntity.getFieldNoCheck("MEMBER_ZIP_CD").length() > 0){
					XmlData.append("<MEMBER_ZIP_CD>" + DataEntity.getFieldNoCheck("MEMBER_ZIP_CD") + "</MEMBER_ZIP_CD>");
					XmlData.append("<MEMBER_ADDR1><![CDATA[" + DataEntity.getFieldNoCheck("MEMBER_ADDR1") + "]]></MEMBER_ADDR1>");
					XmlData.append("<MEMBER_ADDR2><![CDATA[" + DataEntity.getFieldNoCheck("MEMBER_ADDR2") + "]]></MEMBER_ADDR2>");
				}
				
				StringBuffer InterlockData = new StringBuffer();
				Entity CheckEntity = null;
				
				for(int idx = 0; idx < CheckList.size(); idx++){
					CheckEntity = CheckList.get(idx);
					
					InterlockData.delete(0, InterlockData.length());
					
					InterlockData.append("<MEMBER><MEMBER_NO>" + CheckEntity.getField("INTERLOCK_MEMBER_NO") + "</MEMBER_NO><MEMBER_ID>" + CheckEntity.getField("INTERLOCK_MEMBER_ID") + "</MEMBER_ID>" + XmlData.toString() + "</MEMBER>");
					
					this.InterlockInfo = getInterlockData(CheckEntity.getField("INTERLOCK_URL"), "MODIFY", InterlockData);
					
					InsertInterlockLog(conn, CompanyCd, MemberNo, CheckEntity.getField("INTERLOCK_CD"), "MODIFY", "", (this.InterlockInfo.getFieldNoCheck("RSP_CD").equals("0000") || this.InterlockInfo.getFieldNoCheck("RSP_CD").equals("0")?"Y":"F"), this.InterlockInfo.getFieldNoCheck("RSP_MSG"));
				}
			}
			
			result = true;
		}catch(C2SException ce){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".MemberRegist()! " + ce.getMessage(), ce, 1);
			this.ErrCode = ce.getExceptCode();
			this.ErrMessage = ce.getMessage();
			result = false;
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".MemberRegist()! " + e.getMessage(), e, 1);
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			result = false;
		}

		return result;
	}
	
	
	/**
	 * Member Withdraw
	 * @param DataEntity
	 * @return
	 * @throws Exception
	 */
	public boolean MemberWithdraw(Connection conn, String CompanyCd, String MemberNo) throws Exception{
		boolean result = false;
		
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try{
			if(MemberNo.length() == 0){
				throw new C2SException("MW01", "NOT_EXIST_MEMBER_NO");
			}
			
			Entity CheckCon = new Entity(2);
			
			CheckCon.setField("1", CompanyCd);
			CheckCon.setField("2", MemberNo);
			
			EntityList CheckList = EntityUtility.SelectQueryParagraphList(conn, Environment.getQuery("MemberInterlock", "SELECT_INTERLOCK_MAP_MEMBER_ALL"), CheckCon);
			
			if(CheckList.size() > 0){
				StringBuffer InterlockData = new StringBuffer();
				Entity CheckEntity = null;
				
				for(int idx = 0; idx < CheckList.size(); idx++){
					CheckEntity = CheckList.get(idx);
					
					InterlockData.delete(0, InterlockData.length());
					
					InterlockData.append("<MEMBER><MEMBER_NO>" + CheckEntity.getField("INTERLOCK_MEMBER_NO") + "</MEMBER_NO><MEMBER_ID>" + CheckEntity.getField("INTERLOCK_MEMBER_ID") + "</MEMBER_ID></MEMBER>");
					
					this.InterlockInfo = getInterlockData(CheckEntity.getField("INTERLOCK_URL"), "WITHDRAW", InterlockData);
					
					InsertInterlockLog(conn, CompanyCd, MemberNo, CheckEntity.getField("INTERLOCK_CD"), "WITHDRAW", "", (this.InterlockInfo.getFieldNoCheck("RSP_CD").equals("0000") || this.InterlockInfo.getFieldNoCheck("RSP_CD").equals("0")?"Y":"F"), this.InterlockInfo.getFieldNoCheck("RSP_MSG"));
				}
			}
			
			result = true;
		}catch(C2SException ce){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".MemberRegist()! " + ce.getMessage(), ce, 1);
			this.ErrCode = ce.getExceptCode();
			this.ErrMessage = ce.getMessage();
			result = false;
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_MEMBER", "Exception occured on " + this.getClass().getName() + ".MemberRegist()! " + e.getMessage(), e, 1);
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			result = false;
		}

		return result;
	}
}