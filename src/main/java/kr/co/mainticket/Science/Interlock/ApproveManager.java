package kr.co.mainticket.Science.Interlock;

import java.net.URLDecoder;
import java.sql.Connection;

import kr.co.mainticket.Entity.*;
import kr.co.mainticket.Exception.C2SException;
import kr.co.mainticket.Log.*;
import kr.co.mainticket.Utility.*;
import kr.co.mainticket.Science.Config.EncManager;
import kr.co.mainticket.Science.Config.Environment;
import kr.co.mainticket.Science.Database.*;
import kr.co.mainticket.Payment.Card.CardProcess;
import kr.co.mainticket.Payment.Cash.CashProcess;
import kr.co.mainticket.Payment.RA.RAProcess;
import kr.co.mainticket.Payment.VA.VAProcess;
import kr.co.mainticket.Point.C2SPoint;

public class ApproveManager{
	private Entity ApproveInfo;
	
	private String ErrCode;
	private String ErrMessage;
	
	
	public Entity getApproveInfo(){
		return this.ApproveInfo;
	}
	
	
	public String getErrCode(){
		return Utility.CheckNull(this.ErrCode);
	}
	
	public String getErrMessage(){
		return Utility.CheckNull(this.ErrMessage).replaceAll("\n", "\\\\n").replaceAll("\"", "'");
	}
	
	
	
	/**
	 * Create InfUniqueNbr
	 */
	public String InfUniqueNbr(String CompanyCd, String SystemType){
		String CurrDate = DateTime.getFormatDateString("yyyyMMddHHmmss", 0);
		
		Common cm = new Common();
		
		String InfUniqueNbr = "";
		
		try{
			InfUniqueNbr = CurrDate.substring(2, 8) + CompanyCd + SystemType + Utility.LPAD(cm.getIndex(CompanyCd, "INF_UNIQUE_NBR", CurrDate.substring(0, 8), 1), 5, '0');
		}catch(Exception e){
			InfUniqueNbr = "";
		}
		
		return InfUniqueNbr;
	}
	
	/**
	 * Select TA_COMPANY_PAYMENT
	 * @param ApproveEntity
	 * @return
	 * @throws Exception
	 */
	public EntityList SelectPayment(String CompanyCd, String SystemType, String ServiceType, String SaleType, String PaymentType) throws Exception{
		EntityList PaymentInfo = null;
		Connection conn = null;
		
		try{
			conn = DBManager.getConn();
			
			Entity ConPay = new Entity(5);
			
			ConPay.setField("1", CompanyCd);
			ConPay.setField("2", SystemType);
			ConPay.setField("3", ServiceType);
			ConPay.setField("4", SaleType);
			ConPay.setField("5", PaymentType);
			
			PaymentInfo = EntityUtility.SelectQueryParagraphList(conn, Environment.getQuery("Approve", "SELECT_COMPANY_PAYMENT"), ConPay);
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "APPROVE_INFO", "Exception occured on " + this.getClass().getName() + ".SelectPayment()! " + e.getMessage(), e, 1);
			PaymentInfo = new EntityList();
		}finally{
			if(conn != null) conn.close();
		}

		return PaymentInfo;
	}
	
	/**
	 * Insert TA_APPROVE
	 * @param ApproveEntity
	 * @return
	 * @throws Exception
	 */
	public int InsertApprove(Entity ApproveEntity) throws Exception{
		int UpdateCount = 0;
		Connection conn = null;
		
		try{
			conn = DBManager.getConn();
			
			UpdateCount = EntityUtility.UpdateQueryParagraph(conn, Environment.getQuery("Approve", "INSERT_APPROVE"), ApproveEntity);
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "APPROVE_INFO", "Exception occured on " + this.getClass().getName() + ".InsertApprove()! " + e.getMessage(), e, 1);
			UpdateCount = 0;
		}finally{
			if(conn != null) conn.close();
		}

		return UpdateCount;
	}
	
	
	/**
	 * Insert TA_VIRTUAL_ACCOUNT
	 * @param ApproveEntity
	 * @return
	 * @throws Exception
	 */
	public int InsertVirtualAccount(Entity ApproveEntity) throws Exception{
		int UpdateCount = 0;
		Connection conn = null;
		
		try{
			conn = DBManager.getConn();
			
			UpdateCount = EntityUtility.UpdateQueryParagraph(conn, Environment.getQuery("Approve", "INSERT_VIRTUAL_ACCOUNT"), ApproveEntity);
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "APPROVE_INFO", "Exception occured on " + this.getClass().getName() + ".InsertVirtualAccount()! " + e.getMessage(), e, 1);
			UpdateCount = 0;
		}finally{
			if(conn != null) conn.close();
		}

		return UpdateCount;
	}
	
	
	/**
	 * Update TA_APPROVE
	 * @param ApproveEntity
	 * @return
	 * @throws Exception
	 */
	public int UpdateApprove(Entity ApproveEntity) throws Exception{
		int UpdateCount = 0;
		Connection conn = null;
		
		try{
			conn = DBManager.getConn();
			
			UpdateCount = EntityUtility.UpdateQueryParagraph(conn, Environment.getQuery("Approve", "UPDATE_APPROVE"), ApproveEntity);
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "APPROVE_INFO", "Exception occured on " + this.getClass().getName() + ".UpdateApprove()! " + e.getMessage(), e, 1);
			UpdateCount = 0;
		}finally{
			if(conn != null) conn.close();
		}

		return UpdateCount;
	}
	
	
	/**
	 * Approve
	 * @param CompanyCd
	 * @param SaleType
	 * @param PaymentType
	 * @param InfUniqueNbr
	 * @param DataEntity
	 * @return
	 * @throws Exception
	 */
	public boolean Approve(String CompanyCd, String SystemType, String ServiceType, String SaleType, String WinType, String InfUniqueNbr, Entity DataEntity) throws Exception{
		boolean ApproveResult = false;
		
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try{
			String PaySvcType = DataEntity.getFieldNoCheck("PAY_SVC_TYPE");
			String PaymentType = "";
			
			if(PaySvcType.equals("001")){
				SaleType = "000001";
				PaymentType = "000001";
			}
			else if(PaySvcType.equals("002") || PaySvcType.equals("ISP") || PaySvcType.equals("3D")){
				PaymentType = "000002";
			}
			else if(PaySvcType.equals("VAT")){
				PaymentType = "000003";
			}
			else if(PaySvcType.equals("ATF")){
				PaymentType = "000004";
			}
			else if(PaySvcType.equals("PIT")){
				PaymentType = "000005";
			}
			else{
				throw new C2SException("AM01", "WRONG_PAY_SVC_TYPE");
			}
			
			if(ServiceType.length() == 0){
				ServiceType = "XXXXXX";
			}
			
			String MbrNo = "";
			int ReceiptPeriod = 0;
			
			EntityList CheckList = SelectPayment(CompanyCd, SystemType, ServiceType, SaleType, PaymentType);
			Entity CheckItem = null; 
			
			if(CheckList.size() > 0){
				CheckItem = CheckList.get(0);
				
				MbrNo = CheckItem.getField("MBR_NO");
				ReceiptPeriod = Utility.StringToInt(CheckItem.getField("RECEIPT_PERIOD"));
				
				if(MbrNo.length() == 0){
					throw new C2SException("AM02", "NOT_EXIST_MBR_NO");
				}
			}
			else{
				throw new C2SException("AM03", "NOT_EXIST_COMPANY_PAYMENT");
			}
			
			/**********************************************************************************************/
			
			String CurrDate = DateTime.getFormatDateString("yyyyMMddHHmmss", 0);
			
			String PayType = "A";
			String PayTranType = "010";
			String TrackData = "";
			
			String PayApplyDate = CurrDate.substring(0, 8);
			String PaySaleDate = CurrDate.substring(0, 8);
			long PayUnitAmt = Utility.StringToLong(DataEntity.getFieldNoCheck("PAYMENT_AMT"));
			String PayRegNo = DataEntity.getFieldNoCheck("REG_NO");
			
			// ����Ʈ�ý��ۿ�
			String OrgInfUniqueNbr = "";
			String OrgApproveDate = "";
			
			if(PaySvcType.equals("001")){		// ���ݿ�����
				PayTranType = "900";
				TrackData = "37" + DataEntity.getFieldNoCheck("TRACK_DATA");
				
				if(DataEntity.getFieldNoCheck("KEYIN_TYPE").equals("K")){
					TrackData += "=0000";
				}
			}
			else if(PaySvcType.equals("002")){	// �ſ�ī��(�Ϲ�)
				TrackData = "37" + DataEntity.getFieldNoCheck("TRACK_DATA");
				
				if(DataEntity.getFieldNoCheck("KEYIN_TYPE").equals("K")){
					TrackData += "=" + DataEntity.getFieldNoCheck("VALID_MONTH");
				}
			}
			else if(PaySvcType.equals("ISP")){	// �ſ�ī��(ISP)
			}
			else if(PaySvcType.equals("3D")){	// �ſ�ī��(3D)
				TrackData = "37" + DataEntity.getFieldNoCheck("TRACK_DATA");
				
				if(DataEntity.getFieldNoCheck("KEYIN_TYPE").equals("K")){
					TrackData += "=" + DataEntity.getFieldNoCheck("VALID_MONTH");
				}
			}
			else if(PaySvcType.equals("VAT")){	// �������
				PayTranType = "810";
				
				// �Աݸ�����
				if(DataEntity.getFieldNoCheck("RECEIPT_LIMIT_DATE").length() > 0){
					PaySaleDate = DataEntity.getFieldNoCheck("RECEIPT_LIMIT_DATE");
				}
				else{
					PaySaleDate = DateTime.addDays(CurrDate.substring(0, 8), ReceiptPeriod);
				}
				
				if(PayRegNo.length() == 0){
					PayRegNo = Utility.LPAD("", 13, '0');
				}
			}
			else if(PaySvcType.equals("ATF")){	// ������ü
				if(DataEntity.getFieldNoCheck("FIRM_TYPE").equals("K")){		// ����������
					PayTranType = "801";
					TrackData = "37" + DataEntity.getFieldNoCheck("MTID");
				}
				else if(DataEntity.getFieldNoCheck("FIRM_TYPE").equals("F")){	// POPBANKING
					PayTranType = "800";
					TrackData = "37" + DataEntity.getFieldNoCheck("RA_INF_UNIQUE_NBR");
				}
			}
			else if(PaySvcType.equals("PIT")){	// ����Ʈ
				PayTranType = DataEntity.getFieldNoCheck("TRAN_TYPE");
				
				// ����������
				if(DataEntity.getFieldNoCheck("PAY_APPLY_DATE").length() > 0){
					PayApplyDate = DataEntity.getFieldNoCheck("PAY_APPLY_DATE");
				}
				
				// �ŷ��ݾ�
				if(Utility.StringToLong(DataEntity.getFieldNoCheck("UNIT_AMT")) > 0){
					PayUnitAmt = Utility.StringToLong(DataEntity.getFieldNoCheck("UNIT_AMT"));
				}
				
				if(PayTranType.equals("02")){			// �������
					PayType = "C";
					OrgInfUniqueNbr = DataEntity.getFieldNoCheck("ORG_INF_UNIQUE_NBR");
					OrgApproveDate = DataEntity.getFieldNoCheck("ORG_APPROVE_DATE");
				}
				else if(PayTranType.equals("04")){		// ������
					PayType = "C";
					OrgInfUniqueNbr = DataEntity.getFieldNoCheck("ORG_INF_UNIQUE_NBR");
					OrgApproveDate = DataEntity.getFieldNoCheck("ORG_APPROVE_DATE");
				}
			}
			
			long PaySaleAmt = Utility.StringToLong(DataEntity.getFieldNoCheck("PAYMENT_AMT"));;
			long PaySaleSvcAmt = Utility.StringToLong(DataEntity.getFieldNoCheck("SVC_AMT"));;
			long PaySaleVatAmt = Utility.StringToLong(DataEntity.getFieldNoCheck("VAT_AMT"));;
			
			PaySaleSvcAmt = 0;
			PaySaleVatAmt = 0;
			
			String CustomerName = Utility.RPAD(DataEntity.getFieldNoCheck("CUSTOMER_NAME"), 30, ' ').trim();
			String CustomerTel = Utility.RPAD(DataEntity.getFieldNoCheck("CUSTOMER_TEL"), 15, ' ').trim();
			String CustomerEmail = Utility.RPAD(DataEntity.getFieldNoCheck("CUSTOMER_EMAIL"), 50, ' ').trim();
			String ProduceName = Utility.RPAD(DataEntity.getFieldNoCheck("PRODUCT_NAME"), 30, ' ').trim();
			
			Entity ApproveEntity = new Entity(43);
			
			ApproveEntity.setField("1", InfUniqueNbr);										// INF_UNIQUE_NBR
			ApproveEntity.setField("2", CurrDate.substring(0, 8));							// TRAN_DATE
			ApproveEntity.setField("3", CurrDate.substring(8));								// TRAN_TIME
			ApproveEntity.setField("4", CompanyCd);											// TRAN_COMPANY_CD
			ApproveEntity.setField("5", SystemType);										// SYSTEM_TYPE
			ApproveEntity.setField("6", SaleType);											// SALE_TYPE
			ApproveEntity.setField("7", WinType);											// WIN_TYPE
			ApproveEntity.setField("8", PayType);											// PAY_TYPE
			ApproveEntity.setField("9", PaySvcType);										// PAY_SVC_TYPE
			ApproveEntity.setField("10", OrgInfUniqueNbr);									// ORG_INF_UNIQUE_NBR
			ApproveEntity.setField("11", MbrNo);											// MBR_NO
			ApproveEntity.setField("12", "0200");											// PAY_MSG_TYPE
			ApproveEntity.setField("13", "");												// PAY_WIN_CD
			ApproveEntity.setField("14", "");												// PAY_SHOP_CD
			ApproveEntity.setField("15", "");												// PAY_HALL_CD
			ApproveEntity.setField("16", PayTranType);										// PAY_TRAN_TYPE
			ApproveEntity.setField("17", "1");												// PAY_SALE_CNT
			ApproveEntity.setField("18", PayApplyDate);										// PAY_APPLY_DATE
			ApproveEntity.setField("19", PaySaleDate);										// PAY_SALE_DATE
			ApproveEntity.setField("20", Long.toString(PayUnitAmt));						// PAY_UNIT_AMT
			ApproveEntity.setField("21", "");												// CANCEL_KEY_TYPE
			ApproveEntity.setField("22", DataEntity.getFieldNoCheck("KEYIN_TYPE"));			// KEYIN_TYPE
			//(�������� ����)ApproveEntity.setField("23", TrackData);										// TRACK_DATA
			ApproveEntity.setField("23", "");												// TRACK_DATA
			ApproveEntity.setField("24", DataEntity.getFieldNoCheck("INSTALL_NO"));			// INSTALL_NO
			ApproveEntity.setField("25", "410");											// PAY_CURRENCY
			ApproveEntity.setField("26", Long.toString(PaySaleAmt));						// PAY_SALE_AMT
			ApproveEntity.setField("27", Long.toString(PaySaleSvcAmt));						// PAY_SALE_SVC_AMT
			ApproveEntity.setField("28", Long.toString(PaySaleVatAmt));						// PAY_SALE_VAT_AMT
			ApproveEntity.setField("29", DataEntity.getFieldNoCheck("CERTIFY_TYPE"));		// PAY_CERTIFY_TYPE
			ApproveEntity.setField("30", PayRegNo);											// PAY_REG_NO
			ApproveEntity.setField("31", DataEntity.getFieldNoCheck("CARD_PASSWORD"));		// CARD_PASSWORD
			ApproveEntity.setField("32", DataEntity.getFieldNoCheck("BANK_CD"));			// PAY_BANK_CD
			ApproveEntity.setField("33", DataEntity.getFieldNoCheck("WITHDRAW_NAME"));		// PAY_WITHDRAW_NAME
			ApproveEntity.setField("34", DataEntity.getFieldNoCheck("FIRM_APPROVE_NO"));	// FIRM_APPROVE_NO
			ApproveEntity.setField("35", OrgApproveDate);									// ORG_APPROVE_DATE
			ApproveEntity.setField("36", "");												// SVC_CD1
			ApproveEntity.setField("37", "");												// SVC_CD2
			ApproveEntity.setField("38", "");												// SVC_CD3
			ApproveEntity.setField("39", CustomerName);										// CUSTOMER_NAME
			ApproveEntity.setField("40", EncManager.getEncData("TA_APPROVE", "CUSTOMER_TEL", CustomerTel));										// CUSTOMER_TEL
			ApproveEntity.setField("41", EncManager.getEncData("TA_APPROVE", "CUSTOMER_EMAIL", CustomerEmail));									// CUSTOMER_EMAIL
			ApproveEntity.setField("42", ProduceName);										// PRODUCT_NAME
			ApproveEntity.setField("43", "R");												// STATUS_TYPE
			
			int UpdateCount = InsertApprove(ApproveEntity);
			
			if(UpdateCount <= 0){
				throw new C2SException("AM04", "FAILED_INSERT_APPROVE");
			}
			
			/**********************************************************************************************/
			
			Entity ReqEntity = null;
			Entity RspEntity = null;
			
			boolean PayResult = false;
			String PayErrCd = "";
			String PayErrMsg = "";
			
			String CardMbrNo = "";
			String ApproveNo = "";
			String ApproveDate = "";
			String PayRspCd = "";
			String PayRspMsg = "";
			String C2sUniqueNbr = "";
			String PayAddMsg = "";
			String PayAccountNo = "";
			String IsuCd = "";
			String IsuName = "";
			String IsuCardName = "";
			String AcqCd = "";
			String AcqName = "";
			String ExtendApproveNo = "";
			String CancelType = "";
			String StatusType = "";
			
			String VaBankCd = "";
			String VaAccountNo = "";
			String VaLimitReceiptDate = "";
			String InstallNo = "";
			
			if(!PaySvcType.equals("PIT")){
				ReqEntity = new Entity(21);
				RspEntity = new Entity(52);
				
				ReqEntity.setField("MBR_NO", MbrNo);											
				ReqEntity.setField("TRAN_DATE", CurrDate.substring(2));							
				ReqEntity.setField("UNIQUE_NBR", InfUniqueNbr);									
				ReqEntity.setField("SALE_CNT", "1");											
				ReqEntity.setField("APPLY_DATE", CurrDate.substring(2, 8));						
				ReqEntity.setField("SALE_DATE", PaySaleDate.substring(2, 8));					
				ReqEntity.setField("UNIT_AMT", Long.toString(PayUnitAmt));							
				ReqEntity.setField("KEYIN_TYPE", DataEntity.getFieldNoCheck("KEYIN_TYPE"));			
				ReqEntity.setField("TRACK_DATA", TrackData);										
				ReqEntity.setField("INSTALL", DataEntity.getFieldNoCheck("INSTALL_NO"));			
				ReqEntity.setField("SALE_AMT", Long.toString(PaySaleAmt));							
				ReqEntity.setField("SALE_SVC_AMT", Long.toString(PaySaleSvcAmt));					
				ReqEntity.setField("SALE_VAT_AMT", Long.toString(PaySaleVatAmt));					
				ReqEntity.setField("REG_NO", PayRegNo);												
				ReqEntity.setField("CUSTOMER_NAME", CustomerName);									
				ReqEntity.setField("CUSTOMER_TEL", CustomerTel);									
				ReqEntity.setField("CUSTOMER_EMAIL", CustomerEmail);								
				ReqEntity.setField("PRODUCT_NAME", ProduceName);									
				
				if(PaySvcType.equals("001")){		
					ReqEntity.setType("CASH");
					RspEntity.setType("CASH");
					
					ReqEntity.setField("INSTALL", DataEntity.getFieldNoCheck("CERTIFY_TYPE"));			
					
					CashProcess cp = new CashProcess();
					PayResult = cp.SendCash(ReqEntity, RspEntity, true);
					
					if(!PayResult){
						PayErrCd = cp.getErrorCode();
						PayErrMsg = cp.getErrorMessage();
					}
				}
				else if(PaySvcType.equals("002")){
					ReqEntity.setType("CARD");
					RspEntity.setType("CARD");
					
					InstallNo = DataEntity.getFieldNoCheck("INSTALL_NO");
					
					CardProcess cp = new CardProcess();
					PayResult = cp.SendCard(ReqEntity, RspEntity, true);
					
					if(!PayResult){
						PayErrCd = cp.getErrorCode();
						PayErrMsg = cp.getErrorMessage();
					}
				}
				else if(PaySvcType.equals("ISP")){
					ReqEntity.setType("ISP_REQ");
					RspEntity.setType("ISP_RSP");
					
					String KvpCardCode = DataEntity.getFieldNoCheck("ISP_CARD_CODE");
					String KvpSessionKey = DataEntity.getFieldNoCheck("ISP_SESSION_KEY");
					String KvpEncData = DataEntity.getFieldNoCheck("ISP_ENCRYPT_DATA");
					
					KvpCardCode = Utility.LPAD(KvpCardCode.getBytes().length, 2, '0') + KvpCardCode;
					KvpSessionKey = Utility.LPAD(URLDecoder.decode(KvpSessionKey).getBytes().length, 4, '0') + URLDecoder.decode(KvpSessionKey);
					KvpEncData = Utility.LPAD(URLDecoder.decode(KvpEncData).getBytes().length, 4, '0') + URLDecoder.decode(KvpEncData);
					 
					ReqEntity.setField("KVP_CARDCODE", KvpCardCode);									// KVP 
					ReqEntity.setField("KVP_SESSIONKEY", KvpSessionKey);								// Session Key
					ReqEntity.setField("KVP_ENCDATA", KvpEncData);										// Encrypted Data
					ReqEntity.setField("CUSTOMER_IP", DataEntity.getFieldNoCheck("CLIENT_IP"));			// Client IP Address
					
					InstallNo = DataEntity.getFieldNoCheck("INSTALL_NO");
					
					CardProcess cp = new CardProcess();
					PayResult = cp.SendCard(ReqEntity, RspEntity, true);
					
					if(!PayResult){
						PayErrCd = cp.getErrorCode();
						PayErrMsg = cp.getErrorMessage();
					}
				}
				else if(PaySvcType.equals("3D")){
					ReqEntity.setType("3D_REQ");
					RspEntity.setType("3D_RSP");
					
					ReqEntity.setField("CAVV", "080" + DataEntity.getFieldNoCheck("CAVV"));				// CAVV
					ReqEntity.setField("XID", DataEntity.getFieldNoCheck("X_ID"));						// X-ID
					
					InstallNo = DataEntity.getFieldNoCheck("INSTALL_NO");
					
					CardProcess cp = new CardProcess();
					PayResult = cp.SendCard(ReqEntity, RspEntity, true);
					
					if(!PayResult){
						PayErrCd = cp.getErrorCode();
						PayErrMsg = cp.getErrorMessage();
					}
				}
				else if(PaySvcType.equals("VAT")){
					ReqEntity.setType("ISSUE");
					RspEntity.setType("ISSUE");
					
					if(DataEntity.getFieldNoCheck("FIRM_TYPE").equals("D")){
						ReqEntity.setField("MSG_TYPE", "0201");
					}
					
					ReqEntity.setField("SALE_DATE", PaySaleDate.substring(2, 8));						
					ReqEntity.setField("ISU_CD", DataEntity.getFieldNoCheck("BANK_CD"));				
					
					VaBankCd = DataEntity.getFieldNoCheck("BANK_CD");
					VaLimitReceiptDate = PaySaleDate;
					
					VAProcess vp = new VAProcess();
					PayResult = vp.SendVA(ReqEntity, RspEntity, true);
					
					if(!PayResult){
						PayErrCd = vp.getErrorCode();
						PayErrMsg = vp.getErrorMessage();
					}
				}
				else if(PaySvcType.equals("ATF")){
					ReqEntity.setType("TRANSFER");
					RspEntity.setType("TRANSFER");
					
					ReqEntity.setField("TRAN_TYPE", PayTranType);
					ReqEntity.setField("CARD_MBR_NO", DataEntity.getFieldNoCheck("FIRM_APPROVE_NO"));	
					ReqEntity.setField("ADD_MSG", DataEntity.getFieldNoCheck("WITHDRAW_NAME"));			
					ReqEntity.setField("ISU_CD", DataEntity.getFieldNoCheck("BANK_CD"));				
					
					RAProcess rp = new RAProcess();
					PayResult = rp.SendRA(ReqEntity, RspEntity, true);
					
					if(!PayResult){
						PayErrCd = rp.getErrorCode();
						PayErrMsg = rp.getErrorMessage();
					}
				}
				
				if(PayResult){
					CardMbrNo = RspEntity.getFieldNoCheck("CARD_MBR_NO");
					ApproveNo = RspEntity.getFieldNoCheck("APPROVE_NO");
					
					if(RspEntity.getFieldNoCheck("APPROVE_DATE").length() > 0){
						ApproveDate = CurrDate.substring(0, 2) + RspEntity.getFieldNoCheck("APPROVE_DATE");
					}
					
					PayRspCd = RspEntity.getFieldNoCheck("RSP_CD");
					PayRspMsg = RspEntity.getFieldNoCheck("RSP_MSG");
					C2sUniqueNbr = RspEntity.getFieldNoCheck("C2S_UNIQUE_NBR");
					PayAddMsg = RspEntity.getFieldNoCheck("ADD_MSG");
					PayAccountNo = (PaySvcType.equals("VAT")?RspEntity.getFieldNoCheck("ADD_MSG"):"");
					IsuCd = RspEntity.getFieldNoCheck("ISU_CD");
					IsuName = RspEntity.getFieldNoCheck("ISU_NAME");
					IsuCardName = RspEntity.getFieldNoCheck("ISU_CARD_NAME");
					AcqCd = RspEntity.getFieldNoCheck("ACQ_CD");
					AcqName = RspEntity.getFieldNoCheck("ACQ_NAME");
					ExtendApproveNo = RspEntity.getFieldNoCheck("EXTEND_APPROVE_NO");
					StatusType = "C";
					
					if(RspEntity.getFieldNoCheck("RSP_CD").substring(0, 2).equals("00")){
						ApproveResult = true;
						
						VaAccountNo = (PaySvcType.equals("VAT")?RspEntity.getFieldNoCheck("ADD_MSG"):"");
					}
					else{
						PayErrCd = RspEntity.getField("RSP_CD");
						PayErrMsg = RspEntity.getFieldNoCheck("RSP_MSG");
					}
				}
				else{
					PayAddMsg = PayErrCd;
					
					if(PayErrCd.equals("ER01")){			// REVERSE SUCCESS
						CancelType = "R";
						StatusType = "C";
					}
					else if(PayErrCd.equals("ER51")){		// REVERSE FAIL
						CancelType = "R";
						StatusType = "E";
					}
					else{
						StatusType = "E";
					}
				}
			}
			else{
				ReqEntity = new Entity(30);
				RspEntity = null;
				
				ReqEntity.setField("COMPANY_CD", CompanyCd);
				ReqEntity.setField("TRAN_UNIQUE_NBR", InfUniqueNbr);
				ReqEntity.setField("MBR_NO", MbrNo);
				ReqEntity.setField("MEMBER_NO", DataEntity.getFieldNoCheck("MEMBER_NO"));
				ReqEntity.setField("MEMBER_NAME", DataEntity.getFieldNoCheck("CUSTOMER_NAME"));
				ReqEntity.setField("MEMBER_GRADE", " ");
				ReqEntity.setField("RSV_NBR", " ");
				ReqEntity.setField("MEMBER_REG_NO", " ");
				ReqEntity.setField("REG_COMPANY_CD", CompanyCd);
				ReqEntity.setField("PROGRAM_CD", " ");
				ReqEntity.setField("PROGRAM_NAME", DataEntity.getFieldNoCheck("PRODUCT_NAME"));
				ReqEntity.setField("PLAY_DATE", PayApplyDate);
				ReqEntity.setField("SALE_DATE", PaySaleDate);
				ReqEntity.setField("TKT_CNT", "1");
				ReqEntity.setField("TOT_AMT", Long.toString(PayUnitAmt));
				ReqEntity.setField("SVC_AMT", Long.toString(PaySaleSvcAmt));
				ReqEntity.setField("TAX_AMT", Long.toString(PaySaleVatAmt));
				ReqEntity.setField("ISS_POINT", Long.toString(PaySaleAmt));
				ReqEntity.setField("ORG_TRAN_UNIQUE_NBR", DataEntity.getFieldNoCheck("ORG_INF_UNIQUE_NBR"));
				ReqEntity.setField("ORG_TRAN_DATE", DataEntity.getFieldNoCheck("ORG_APPROVE_DATE"));
				
				C2SPoint cp = new C2SPoint();
				
				if(PayTranType.equals("99")){
					PayResult = cp.PointEnquiry(ReqEntity);
				}
				else if(PayTranType.equals("01")){
					PayResult = cp.PointSave(ReqEntity);
				}
				else if(PayTranType.equals("02")){
					PayResult = cp.PointSaveCancel(ReqEntity);
				}
				else if(PayTranType.equals("03")){
					PayResult = cp.PointUse(ReqEntity);
				}
				else if(PayTranType.equals("04")){
					PayResult = cp.PointUseCancel(ReqEntity);
				}
				
				if(PayResult){
					RspEntity = cp.getRspEntity();
					
					PayRspCd = RspEntity.getField("RSP_CD");
					PayRspMsg = RspEntity.getFieldNoCheck("RSP_MSG");
					StatusType = "C";
					
					if(PayRspCd.equals("0000")){
						ApproveNo = RspEntity.getFieldNoCheck("AUTH_NO");
						ApproveDate = RspEntity.getFieldNoCheck("AUTH_DATE");
						PayAddMsg = RspEntity.getFieldNoCheck("ADD_MSG");
						
						ApproveResult = true;
					}
					else{
						PayErrCd = RspEntity.getField("RSP_CD");
						PayErrMsg = RspEntity.getFieldNoCheck("RSP_MSG");
					}
				}
				else{
					PayErrCd = "E999";
					PayErrMsg = cp.getErrorMessage();
					
					PayAddMsg = "E999";
					StatusType = "E";
				}
			}
			
			Entity PayRspEntity = new Entity(17);
			
			PayRspEntity.setField("1", CardMbrNo);
			PayRspEntity.setField("2", ApproveNo);
			PayRspEntity.setField("3", ApproveDate);
			PayRspEntity.setField("4", PayRspCd);
			PayRspEntity.setField("5", PayRspMsg);
			PayRspEntity.setField("6", C2sUniqueNbr);
			PayRspEntity.setField("7", PayAddMsg);
			PayRspEntity.setField("8", PayAccountNo);
			PayRspEntity.setField("9", IsuCd);
			PayRspEntity.setField("10", IsuName);
			PayRspEntity.setField("11", IsuCardName);
			PayRspEntity.setField("12", AcqCd);
			PayRspEntity.setField("13", AcqName);
			PayRspEntity.setField("14", ExtendApproveNo);
			PayRspEntity.setField("15", CancelType);
			PayRspEntity.setField("16", StatusType);
			PayRspEntity.setField("17", InfUniqueNbr);
			
			UpdateCount = UpdateApprove(PayRspEntity);
			
			if(UpdateCount <= 0){
				throw new C2SException("AM05", "FAILED_UPDATE_APPROVE");
			}
			
			if(ApproveResult){
				if(PaySvcType.equals("VAT")){
					Entity VAEntity = new Entity(3);
					
					VAEntity.setField("1", InfUniqueNbr);				// INF_UNIQUE_NBR
					VAEntity.setField("2", "N");						// PROCESS_FLAG
					VAEntity.setField("3", "N");						// TAXSAVE_FLAG
					
					UpdateCount = InsertVirtualAccount(VAEntity);
					
					if(UpdateCount <= 0){
						throw new C2SException("AM06", "FAILED_INSERT_VIRTUAL_ACCOUNT");
					}
				}
				
				this.ApproveInfo = new Entity(10);
				
				this.ApproveInfo.setField("BANK_CD", VaBankCd);
				this.ApproveInfo.setField("ACCOUNT_NO", VaAccountNo);
				this.ApproveInfo.setField("LIMIT_RECEIPT_DATE", VaLimitReceiptDate);
				this.ApproveInfo.setField("INSTALL_NO", InstallNo);
				this.ApproveInfo.setField("APPROVE_NO", ApproveNo);
				this.ApproveInfo.setField("ISU_CD", IsuCd);
				this.ApproveInfo.setField("ISU_NAME", IsuName);
				this.ApproveInfo.setField("ISU_CARD_NAME", IsuCardName);
				this.ApproveInfo.setField("ACQ_CD", AcqCd);
				this.ApproveInfo.setField("ACQ_NAME", AcqName);
			}
			else{
				throw new C2SException("AM07", PayErrMsg + "[" + PayErrCd + "]");
			}
		}catch(C2SException ce){
			C2SLog.printLogger("SCIENCE_LOG", "APPROVE_INFO", "Exception occured on " + this.getClass().getName() + ".Approve()! " + ce.getMessage(), ce, 1);
			this.ErrCode = ce.getExceptCode();
			this.ErrMessage = ce.getMessage();
			ApproveResult = false;
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "APPROVE_INFO", "Exception occured on " + this.getClass().getName() + ".Approve()! " + e.getMessage(), e, 1);
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			ApproveResult = false;
		}

		return ApproveResult;
	}
	
	
	/**
	 * Check the Approve Data
	 * @param DataEntity
	 * @return
	 * @throws Exception
	 */
	public boolean CheckData(Entity DataEntity) throws Exception{
		boolean result = false;
		
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try{
			String PaySvcType = DataEntity.getFieldNoCheck("PAY_SVC_TYPE");
			
			if(PaySvcType.length() == 0){
				throw new C2SException("CD01", "NOT_EXIST_PAYMENT_TYPE");
			}
			
			// ���ݿ�����
			if(PaySvcType.equals("001")){
				if(DataEntity.getFieldNoCheck("KEYIN_TYPE").length() == 0){
					throw new C2SException("CD02", "NOT_EXIST_KEYIN_TYPE");
				}
				else if(DataEntity.getFieldNoCheck("TRACK_DATA").length() == 0){
					throw new C2SException("CD03", "NOT_EXIST_TRACK_DATA");
				}
				else if(DataEntity.getFieldNoCheck("CERTIFY_TYPE").length() == 0){
					throw new C2SException("CD04", "NOT_EXIST_CERTIFY_TYPE");
				}
			}
			else if(PaySvcType.equals("002") || PaySvcType.equals("ISP") || PaySvcType.equals("3D")){
				if(DataEntity.getFieldNoCheck("INSTALL_NO").length() == 0){
					throw new C2SException("CD05", "NOT_EXIST_INSTALL_NO");
				}
				
				if(PaySvcType.equals("002")){
					if(DataEntity.getFieldNoCheck("KEYIN_TYPE").length() == 0){
						throw new C2SException("CD06", "NOT_EXIST_KEYIN_TYPE");
					}
					else if(DataEntity.getFieldNoCheck("TRACK_DATA").length() == 0){
						throw new C2SException("CD07", "NOT_EXIST_TRACK_DATA");
					}
					else if(DataEntity.getFieldNoCheck("TRACK_DATA").equals("K") && DataEntity.getFieldNoCheck("VALID_MONTH").length() == 0){
						throw new C2SException("CD08", "NOT_EXIST_VALID_MONTH");
					}
				}
				else if(PaySvcType.equals("ISP")){
					if(DataEntity.getFieldNoCheck("ISP_CARD_CODE").length() == 0){
						throw new C2SException("CD09", "NOT_EXIST_ISP_CARD_CODE");
					}
					else if(DataEntity.getFieldNoCheck("ISP_SESSION_KEY").length() == 0){
						throw new C2SException("CD10", "NOT_EXIST_ISP_SESSION_KEY");
					}
					else if(DataEntity.getFieldNoCheck("ISP_ENCRYPT_DATA").length() == 0){
						throw new C2SException("CD11", "NOT_EXIST_ISP_ENCRYPT_DATA");
					}
					else if(DataEntity.getFieldNoCheck("CLIENT_IP").length() == 0){
						throw new C2SException("CD12", "NOT_EXIST_CLIENT_IP");
					}
				}
				else if(PaySvcType.equals("3D")){
					if(DataEntity.getFieldNoCheck("CAVV").length() == 0){
						throw new C2SException("CD13", "NOT_EXIST_3D_CAVV");
					}
					else if(DataEntity.getFieldNoCheck("X_ID").length() == 0){
						throw new C2SException("CD14", "NOT_EXIST_3D_X_ID");
					}
				}
			}
			else if(PaySvcType.equals("VAT")){	
				if(DataEntity.getFieldNoCheck("BANK_CD").length() == 0){
					throw new C2SException("CD15", "NOT_EXIST_BANK_CD");
				}
			}
			else if(PaySvcType.equals("ATF")){	
				if(DataEntity.getFieldNoCheck("FIRM_TYPE").length() == 0){
					throw new C2SException("CD16", "NOT_EXIST_FIRM_TYPE");
				}
				else if(DataEntity.getFieldNoCheck("BANK_CD").length() == 0){
					throw new C2SException("CD17", "NOT_EXIST_BANK_CD");
				}
				else if(DataEntity.getFieldNoCheck("FIRM_TYPE").equals("K")){
					if(DataEntity.getFieldNoCheck("FIRM_APPROVE_NO").length() == 0){
						throw new C2SException("CD18", "NOT_EXIST_FIRM_APPROVE_NO");
					}
					else if(DataEntity.getFieldNoCheck("WITHDRAW_NAME").length() == 0){
						throw new C2SException("CD19", "NOT_EXIST_WITHDRAW_NAME");
					}
					else if(DataEntity.getFieldNoCheck("MTID").length() == 0){
						throw new C2SException("CD20", "NOT_EXIST_MTID");
					}
				}
				else if(DataEntity.getFieldNoCheck("FIRM_TYPE").equals("F")){
					if(DataEntity.getFieldNoCheck("RA_INF_UNIQUE_NBR").length() == 0){
						throw new C2SException("CD21", "NOT_EXIST_RA_INF_UNIQUE_NBR");
					}
				}
			}
			else if(PaySvcType.equals("PIT")){	
				
			}
			
			result = true;
		}catch(C2SException ce){
			C2SLog.printLogger("SCIENCE_LOG", "APPROVE_INFO", "Exception occured on " + this.getClass().getName() + ".CheckData()! " + ce.getMessage(), ce, 1);
			this.ErrCode = ce.getExceptCode();
			this.ErrMessage = ce.getMessage();
			result = false;
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "APPROVE_INFO", "Exception occured on " + this.getClass().getName() + ".CheckData()! " + e.getMessage(), e, 1);
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			result = false;
		}

		return result;
	}
	
	
	/**
	 * Check the Approve Data
	 * @param DataList
	 * @return
	 * @throws Exception
	 */
	public boolean CheckData(EntityList DataList) throws Exception{
		boolean result = true;
		
		for(int idx = 0; idx < DataList.size(); idx++){
			result = CheckData(DataList.get(idx));
			
			if(!result){
				break;
			}
		}

		return result;
	}
	
	
	/**
	 * Reverse Approve
	 * @param conn
	 * @param ReverseEntity
	 * @return
	 * @throws Exception
	 */
	public int ReverseApprove(Connection conn, Entity ReverseEntity) throws Exception{
		return EntityUtility.UpdateQueryParagraph(conn, Environment.getQuery("Approve", "INSERT_APPROVE_CANCEL"), ReverseEntity);
	}
	
	
	/**
	 * Reverse Approve
	 * @param CompanyCd
	 * @param SystemType
	 * @param InfUniqueNbrList
	 * @return
	 * @throws Exception
	 */
	public int ReverseApprove(String CompanyCd, String SystemType, EntityList InfUniqueNbrList) throws Exception{
		int UpdateCount = 0;
		Connection conn = null;
		
		try{
			conn = DBManager.getConn();
			
			Entity entity = null;
			Entity UpdateEntity = new Entity(5);
			String InfUniqueNbr = "";
			
			for(int idx = 0; idx < InfUniqueNbrList.size(); idx++){
				entity = InfUniqueNbrList.get(idx);
				
				InfUniqueNbr = InfUniqueNbr(CompanyCd, SystemType);
				
				UpdateEntity.setField("1", InfUniqueNbr);								// INF_UNIQUE_NBR
				UpdateEntity.setField("2", "");											// TRAN_DATE
				UpdateEntity.setField("3", "");											// TRAN_TIME
				UpdateEntity.setField("4", entity.getFieldNoCheck("CANCEL_TYPE"));		// CANCEL_TYPE
				UpdateEntity.setField("5", entity.getFieldNoCheck("INF_UNIQUE_NBR"));	// ORG_INF_UNIQUE_NBR
				
				try{
					UpdateCount = ReverseApprove(conn, UpdateEntity);
				}catch(Exception e){
				}
			}
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "APPROVE_INFO", "Exception occured on " + this.getClass().getName() + ".ReverseApprove()! " + e.getMessage(), e, 1);
			UpdateCount = 0;
		}finally{
			if(conn != null) conn.close();
		}

		return UpdateCount;
	}
	
	
	
	/**
	 * Approve Information
	 * @param CompanyCd
	 * @param InfUniqueNbr
	 * @return
	 * @throws Exception
	 */
	public EntityList ApproveInfo(String CompanyCd, String InfUniqueNbr) throws Exception{
		EntityList InfoList = null;
		Connection conn = null;
		
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try{
			conn = DBManager.getConn();
			
			Entity ApproveCon = new Entity(1);
			
			ApproveCon.setField("1", InfUniqueNbr);
			
			InfoList = EntityUtility.SelectQueryParagraphList(conn, Environment.getQuery("Approve", "SELECT_APPROVE"), ApproveCon);
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "APPROVE_INFO", "Exception occured on " + this.getClass().getName() + ".ApproveInfo()! " + e.getMessage(), e, 1);
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			InfoList = new EntityList();
		}finally{
			if(conn != null) conn.close();
		}

		return InfoList;
	}
}