package kr.co.mainticket.Science.Interlock.Point;

import java.sql.Connection;

import kr.co.mainticket.Entity.*;
import kr.co.mainticket.Exception.C2SException;
import kr.co.mainticket.Log.*;
import kr.co.mainticket.Point.C2SPoint;
import kr.co.mainticket.Science.Database.DBManager;
import kr.co.mainticket.Science.Interlock.ApproveManager;
import kr.co.mainticket.Science.Interlock.Common;
import kr.co.mainticket.Utility.Utility;

public class PointInfo{
	private Entity PointInfo;
	
	private String ErrCode;
	private String ErrMessage;
	
	
	public Entity getPointInfo(){
		return this.PointInfo;
	}
	
	public String getErrCode(){
		return Utility.CheckNull(this.ErrCode);
	}
	
	public String getErrMessage(){
		return Utility.CheckNull(this.ErrMessage).replaceAll("\n", "\\\\n").replaceAll("\"", "'");
	}
	
	
	
	/**
	 * Member Point Information
	 * @param CompanyCd
	 * @param DataEntity
	 * @return
	 * @throws Exception
	 */
	public boolean MemberPoint(String CompanyCd, Entity DataEntity) throws Exception{
		boolean result = false;
		Connection conn = null;
		
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try{
			String MemberNo = DataEntity.getFieldNoCheck("MEMBER_NO");
			String SystemType = DataEntity.getFieldNoCheck("SYSTEM_TYPE");
			String SaleType = DataEntity.getFieldNoCheck("SALE_TYPE");
			String PaymentType = DataEntity.getFieldNoCheck("PAYMENT_TYPE");
			
			if(MemberNo.length() == 0){
				throw new C2SException("PI01", "NOT_EXIST_MEMBER_NO");
			}
			else if(SystemType.length() == 0){
				throw new C2SException("PI02", "NOT_EXIST_SYSTEM_TYPE");
			}
			else if(SaleType.length() == 0){
				throw new C2SException("PI03", "NOT_EXIST_SALE_TYPE");
			}
			else if(PaymentType.length() == 0){
				throw new C2SException("PI04", "NOT_EXIST_PAYMENT_TYPE");
			}
			
			conn = DBManager.getConn();
			
			Common cm = new Common();
			
			String MbrNo = "";
			
			ApproveManager mgr = new ApproveManager();
			
			EntityList CheckList = mgr.SelectPayment(CompanyCd, SystemType, "XXXXXX", SaleType, PaymentType);
			Entity CheckItem = null; 
			
			if(CheckList.size() > 0){
				CheckItem = CheckList.get(0);
				
				MbrNo = CheckItem.getField("MBR_NO");
				
				if(MbrNo.length() == 0){
					throw new C2SException("PI07", "NOT_EXIST_MBR_NO");
				}
			}
			else{
				throw new C2SException("PI08", "NOT_EXIST_COMPANY_PAYMENT");
			}
			
			String InfUniqueNbr = mgr.InfUniqueNbr(CompanyCd, SystemType);
			
			Entity PointEntity = new Entity(6);
			
			PointEntity.setField("COMPANY_CD", CompanyCd);
			PointEntity.setField("MBR_NO", MbrNo);
			PointEntity.setField("MEMBER_NAME", " ");
			PointEntity.setField("MEMBER_REG_NO", " ");
			PointEntity.setField("REG_COMPANY_CD", " ");
			PointEntity.setField("TRAN_UNIQUE_NBR", InfUniqueNbr);
			PointEntity.setField("MEMBER_NO", MemberNo);
			
			C2SPoint cp = new C2SPoint();
			
			boolean PayResult = cp.PointEnquiry(PointEntity);
			String RspCode = "";
			String RspMsg = "";
			long AccumulatePoint = 0;
			long UsablePoint = 0;
			
			if(PayResult){
				Entity RspEntity = cp.getRspEntity();
				
				RspCode = RspEntity.getField("RSP_CD");
				
				if(RspCode.equals("0000")){
					AccumulatePoint = Utility.StringToLong(RspEntity.getField("ACUM_POINT"));
					UsablePoint = Utility.StringToLong(RspEntity.getField("AVAL_POINT"));
				}
				else{
					RspMsg = RspEntity.getField("RSP_MSG");
				}
			}
			else{
				RspCode = "E999";
				RspMsg = cp.getErrorMessage();
			}
			
			this.PointInfo = new Entity(6);
			
			this.PointInfo.setField("COMPANY_CD", CompanyCd);
			this.PointInfo.setField("MEMBER_NO", MemberNo);
			this.PointInfo.setField("RSP_CODE", RspCode);
			this.PointInfo.setField("RSP_MSG", RspMsg);
			this.PointInfo.setField("ACCUMULATE_POINT", Long.toString(AccumulatePoint));
			this.PointInfo.setField("USABLE_POINT", Long.toString(UsablePoint));
			
			result = true;
		}catch(C2SException ce){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_POINT", "Exception occured on " + this.getClass().getName() + ".MemberPoint()! " + ce.getMessage(), ce, 1);
			this.ErrCode = ce.getExceptCode();
			this.ErrMessage = ce.getMessage();
			result = false;
		}catch(Exception e){
			C2SLog.printLogger("SCIENCE_LOG", "INTERLOCK_POINT", "Exception occured on " + this.getClass().getName() + ".MemberPoint()! " + e.getMessage(), e, 1);
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			result = false;
		}finally{
			if(conn != null) conn.close();
		}

		return result;
	}
}