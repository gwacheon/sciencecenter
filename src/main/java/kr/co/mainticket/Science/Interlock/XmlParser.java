package kr.co.mainticket.Science.Interlock;


import kr.co.mainticket.Entity.Entity;
import kr.co.mainticket.Entity.EntityList;
import kr.co.mainticket.Utility.Utility;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmlParser{

	private XmlParser(){
	}
	
	
	/**
	 * @param XmlNodeList
	 * @return
	 * @throws Exception
	 */
	public static Entity ParseXmlToEntity(NodeList XmlNodeList) throws Exception{
		Entity DataEntity = new Entity(XmlNodeList.getLength());
		
		Node XmlNode = null;
		NodeList XmlChildList = null;
		boolean ChkNode = false;
		String Title = "";
		String Value = "";
		
		for(int idx = 0; idx < XmlNodeList.getLength(); idx++){
			XmlNode = XmlNodeList.item(idx);
			
			Title = XmlNode.getNodeName();
			
			XmlChildList = XmlNode.getChildNodes();
			
			if(XmlChildList.getLength() > 0){
				ChkNode = false;
				
				for(int Nidx = 0; Nidx <XmlChildList.getLength(); Nidx++){
					if(XmlChildList.item(Nidx).getNodeType() == Node.ELEMENT_NODE){
						ChkNode = true;
						break;
					}
				}
				
				if(ChkNode){
					DataEntity.addField(Title, ParseXmlToEntity(XmlChildList));
				}
				else{
					Value = Utility.getNodeValue(XmlNode);
					DataEntity.setField(Title, Value);
				}
			}
			else{
				Value = Utility.getNodeValue(XmlNode);
				DataEntity.setField(Title, Value);
			}
		}
		
		return DataEntity;
	}
}