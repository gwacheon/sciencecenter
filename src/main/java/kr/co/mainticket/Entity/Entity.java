package kr.co.mainticket.Entity;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.io.Serializable;

import kr.co.mainticket.Utility.*;


/**
 * Body Entity
 * @Author E.S. Shin
 * @Date 2009. 03. 05.
 */

public class Entity implements Serializable{
	private String EntityType;
	private Hashtable EntityData;		// Entity Data
	
	public Entity(){
		this.EntityData = new Hashtable(10);
	}
	
	
	/**
	 * Entity Type
	 * @param Type
	 * @param FieldCount
	 * @throws Exception
	 */
	public Entity(String Type) throws Exception{
		try{
			if(Type == null){
				throw new Exception("Entity Type is NULL!");
			}
			else{
				this.EntityType = Type;
				this.EntityData = new Hashtable(10);
			}
		}catch(Exception e){
			throw e;
		}
	}
	
	
	/**
	 * Entity Size
	 * @param FieldCount
	 * @throws Exception
	 */
	public Entity(int FieldCount) throws Exception{
		try{
			if(FieldCount <= 0){
				throw new Exception("Field Count has to more than 0!");
			}
			else{
				this.EntityData = new Hashtable(FieldCount);
			}
		}catch(Exception e){
			throw e;
		}
	}
	
	
	/**
	 * Entity Type, Size
	 * @param Type
	 * @param FieldCount
	 * @throws Exception
	 */
	public Entity(String Type, int FieldCount) throws Exception{
		try{
			if(Type == null){
				throw new Exception("Entity Type is NULL!");
			}
			else{
				if(FieldCount <= 0){
					throw new Exception("Field Count has to more than 0!");
				}
				else{
					this.EntityData = new Hashtable(FieldCount);
				}
				
				this.EntityType = Type;
			}
		}catch(Exception e){
			throw e;
		}
	}
	
	
	/**
	 * Entity Type 
	 * @param Type
	 */
	public void setType(String Type){
		this.EntityType = Type;
	}
	
	
	/**
	 * Entity Field 
	 * @param EntityTitle
	 * @param EntityValue
	 */
	public void setField(String EntityTitle, String EntityValue) throws Exception{
		try{
			if(EntityTitle == null){
				throw new Exception("Entity Title is NULL!");
			}
			else if(EntityTitle.trim().length() == 0){
				throw new Exception("Length of Entity Title is zero!");
			}
			else if(EntityValue == null){
				throw new Exception("Entity Value is NULL! Entity Title : " + EntityTitle);
			}
			else{
				this.EntityData.put(EntityTitle, EntityValue);
			}
		}catch(Exception e){
			throw e;
		}
	}
	
	
	/**
	 * Entity Field Value Return
	 * @param EntityTitle
	 * @return
	 */
	public String getField(String EntityTitle) throws Exception{
		String FieldValue = "";
		
		try{
			if(EntityTitle == null){
				throw new Exception("Entity Title is NULL!");
			}
			else if(EntityTitle.trim().length() == 0){
				throw new Exception("Length of Entity Title is zero!");
			}
			else{
				if(this.EntityData.get(EntityTitle) == null){
					throw new Exception("Not Exist " + EntityTitle + " in Entity!");
				}
				else if(Utility.getClassName(this.EntityData.get(EntityTitle)).equals("STRING")){
					FieldValue = (String)this.EntityData.get(EntityTitle);
				}
				else{
					throw new Exception(EntityTitle + " is not String Type!");
				}
			}
		}catch(Exception e){
			throw e;
		}
		
		return FieldValue;
	}
	
	
	/**
	 * Body Field Value Return with No Check Data Null
	 * @param EntityTitle
	 * @return
	 * @throws Exception
	 */
	public String getFieldNoCheck(String EntityTitle) throws Exception{
		String FieldValue = "";
		
		try{
//			if(EntityTitle == null){
//				throw new Exception("Entity Title is NULL!");
//			}
//			else if(EntityTitle.trim().length() == 0){
//				throw new Exception("Length of Body Entity Title is zero!");
//			}
//			else{
				if(this.EntityData.get(EntityTitle) == null){
					FieldValue = "";
				}
				else if(Utility.getClassName(this.EntityData.get(EntityTitle)).equals("STRING")){
					FieldValue = (String)this.EntityData.get(EntityTitle);
				}
				else{
					throw new Exception(EntityTitle + " is not String Type!");
				}
//			}
		}catch(Exception e){
			throw e;
		}
		
		return FieldValue;
	}
	
	
	/**
	 * Entity Field 
	 * @param EntityTitle
	 * @param EntityValue
	 */
	public void setField(String EntityTitle, int EntityValue) throws Exception{
		try{
			if(EntityTitle == null){
				throw new Exception("Entity Title is NULL!");
			}
			else if(EntityTitle.trim().length() == 0){
				throw new Exception("Length of Entity Title is zero!");
			}
			else{
				this.EntityData.put(EntityTitle, EntityValue);
			}
		}catch(Exception e){
			throw e;
		}
	}
	
	
	/**
	 * Entity Field Value Return
	 * @param EntityTitle
	 * @return
	 */
	public int getFieldInt(String EntityTitle) throws Exception{
		int FieldValue = 0;
		
		try{
			if(EntityTitle == null){
				throw new Exception("Entity Title is NULL!");
			}
			else if(EntityTitle.trim().length() == 0){
				throw new Exception("Length of Entity Title is zero!");
			}
			else{
				if(this.EntityData.get(EntityTitle) == null){
					throw new Exception("Not Exist " + EntityTitle + " in Entity!");
				}
				else if(Utility.getClassName(this.EntityData.get(EntityTitle)).indexOf("INT") >= 0){
					FieldValue = (Integer)this.EntityData.get(EntityTitle);
				}
				else{
					throw new Exception(EntityTitle + " is not Integer Type!");
				}
			}
		}catch(Exception e){
			throw e;
		}
		
		return FieldValue;
	}
	
	
	/**
	 * Body Field Value Return with No Check Data Null
	 * @param EntityTitle
	 * @return
	 * @throws Exception
	 */
	public int getFieldIntNoCheck(String EntityTitle) throws Exception{
		int FieldValue = 0;
		
		try{
			if(EntityTitle == null){
				throw new Exception("Entity Title is NULL!");
			}
			else if(EntityTitle.trim().length() == 0){
				throw new Exception("Length of Body Entity Title is zero!");
			}
			else{
				if(this.EntityData.get(EntityTitle) == null){
					FieldValue = 0;
				}
				else if(Utility.getClassName(this.EntityData.get(EntityTitle)).indexOf("INT") >= 0){
					FieldValue = (Integer)this.EntityData.get(EntityTitle);
				}
				else{
					throw new Exception(EntityTitle + " is not Integer Type!");
				}
			}
		}catch(Exception e){
			throw e;
		}
		
		return FieldValue;
	}
	
	
	/**
	 * Entity Field 
	 * @param EntityTitle
	 * @param EntityValue
	 */
	public void setField(String EntityTitle, Entity EntityValue) throws Exception{
		try{
			if(EntityTitle == null){
				throw new Exception("Entity Title is NULL!");
			}
			else if(EntityTitle.trim().length() == 0){
				throw new Exception("Length of Entity Title is zero!");
			}
			else{
				ArrayList EntityArray = new ArrayList();
				
				EntityArray.add(EntityValue);
				
				this.EntityData.put(EntityTitle, EntityArray);
			}
		}catch(Exception e){
			throw e;
		}
	}
	
	
	/**
	 * Entity Field 
	 * @param EntityTitle
	 * @param EntityValue
	 */
	public void addField(String EntityTitle, Entity EntityValue) throws Exception{
		try{
			if(EntityTitle == null){
				throw new Exception("Entity Title is NULL!");
			}
			else if(EntityTitle.trim().length() == 0){
				throw new Exception("Length of Entity Title is zero!");
			}
			else{
				ArrayList EntityArray = (ArrayList)this.EntityData.get(EntityTitle);
				
				if(EntityArray == null){
					EntityArray = new ArrayList();
				}
				
				EntityArray.add(EntityValue);
				
				this.EntityData.put(EntityTitle, EntityArray);
			}
		}catch(Exception e){
			throw e;
		}
	}
	
	
	/**
	 * Entity Count Return
	 * @param EntityTitle
	 * @return
	 */
	public int getFieldEntitySize(String EntityTitle) throws Exception{
		int EntitySize = 0;
		
		try{
			if(EntityTitle == null){
				throw new Exception("Entity Title is NULL!");
			}
			else if(EntityTitle.trim().length() == 0){
				throw new Exception("Length of Entity Title is zero!");
			}
			else{
				ArrayList EntityArray = (ArrayList)this.EntityData.get(EntityTitle);
				
				if(EntityArray != null){
					EntitySize = EntityArray.size();
				}
			}
		}catch(Exception e){
			throw e;
		}
		
		return EntitySize;
	}
	
	
	/**
	 * Entity Field Value Return
	 * @param EntityTitle
	 * @return
	 */
	public Entity getFieldEntity(String EntityTitle) throws Exception{
		Entity FieldValue = null;
		
		try{
			if(EntityTitle == null){
				throw new Exception("Entity Title is NULL!");
			}
			else if(EntityTitle.trim().length() == 0){
				throw new Exception("Length of Entity Title is zero!");
			}
			else{
				if(this.EntityData.get(EntityTitle) == null){
					throw new Exception("Not Exist " + EntityTitle + " in Entity!");
				}
				/*
				else if(Utility.getClassName(this.EntityData.get(EntityTitle)).equals("ENTITY")){
					FieldValue = (Entity)this.EntityData.get(EntityTitle);
				}
				*/
				else if(Utility.getClassName(this.EntityData.get(EntityTitle)).equals("ARRAYLIST")){
					ArrayList EntityArray = (ArrayList)this.EntityData.get(EntityTitle);
					FieldValue = (Entity)EntityArray.get(0);
				}
				else{
					throw new Exception(EntityTitle + " is not Entity Type!");
				}
			}
		}catch(Exception e){
			throw e;
		}
		
		return FieldValue;
	}
	
	
	/**
	 * Entity Field Value Return
	 * @param EntityTitle
	 * @return
	 */
	public Entity getFieldEntity(String EntityTitle, int EntityIdx) throws Exception{
		Entity FieldValue = null;
		
		try{
			if(EntityTitle == null){
				throw new Exception("Entity Title is NULL!");
			}
			else if(EntityTitle.trim().length() == 0){
				throw new Exception("Length of Entity Title is zero!");
			}
			else{
				if(this.EntityData.get(EntityTitle) == null){
					throw new Exception("Not Exist " + EntityTitle + " in Entity!");
				}
				/*
				else if(Utility.getClassName(this.EntityData.get(EntityTitle)).equals("ENTITY")){
					FieldValue = (Entity)this.EntityData.get(EntityTitle);
				}
				*/
				else if(Utility.getClassName(this.EntityData.get(EntityTitle)).equals("ARRAYLIST")){
					ArrayList EntityArray = (ArrayList)this.EntityData.get(EntityTitle);
					FieldValue = (Entity)EntityArray.get(EntityIdx);
				}
				else{
					throw new Exception(EntityTitle + " is not Entity Type!");
				}
			}
		}catch(Exception e){
			throw e;
		}
		
		return FieldValue;
	}
	
	
	/**
	 * Body Field Value Return with No Check Data Null
	 * @param EntityTitle
	 * @return
	 * @throws Exception
	 */
	public Entity getFieldEntityNoCheck(String EntityTitle) throws Exception{
		Entity FieldValue = null;
		
		try{
			if(EntityTitle == null){
				throw new Exception("Entity Title is NULL!");
			}
			else if(EntityTitle.trim().length() == 0){
				throw new Exception("Length of Body Entity Title is zero!");
			}
			else{
				if(this.EntityData.get(EntityTitle) == null){
					FieldValue = new Entity(1);
				}
				/*
				else if(Utility.getClassName(this.EntityData.get(EntityTitle)).equals("ENTITY")){
					FieldValue = (Entity)this.EntityData.get(EntityTitle);
				}
				*/
				else if(Utility.getClassName(this.EntityData.get(EntityTitle)).equals("ARRAYLIST")){
					ArrayList EntityArray = (ArrayList)this.EntityData.get(EntityTitle);
					
					if(EntityArray.size() > 0){
						FieldValue = (Entity)EntityArray.get(0);
					}
					else{
						FieldValue = new Entity(1);
					}
				}
				else{
					throw new Exception(EntityTitle + " is not Entity Type!");
				}
			}
		}catch(Exception e){
			throw e;
		}
		
		return FieldValue;
	}
	
	
	/**
	 * Body Field Value Return with No Check Data Null
	 * @param EntityTitle
	 * @return
	 * @throws Exception
	 */
	public Entity getFieldEntityNoCheck(String EntityTitle, int EntityIdx) throws Exception{
		Entity FieldValue = null;
		
		try{
			if(EntityTitle == null){
				throw new Exception("Entity Title is NULL!");
			}
			else if(EntityTitle.trim().length() == 0){
				throw new Exception("Length of Body Entity Title is zero!");
			}
			else{
				if(this.EntityData.get(EntityTitle) == null){
					FieldValue = new Entity(1);
				}
				/*
				else if(Utility.getClassName(this.EntityData.get(EntityTitle)).equals("ENTITY")){
					FieldValue = (Entity)this.EntityData.get(EntityTitle);
				}
				*/
				else if(Utility.getClassName(this.EntityData.get(EntityTitle)).equals("ARRAYLIST")){
					ArrayList EntityArray = (ArrayList)this.EntityData.get(EntityTitle);
					
					if(EntityIdx < EntityArray.size()){
						FieldValue = (Entity)EntityArray.get(EntityIdx);
					}
					else{
						FieldValue = new Entity(1);
					}
				}
				else{
					throw new Exception(EntityTitle + " is not Entity Type!");
				}
			}
		}catch(Exception e){
			throw e;
		}
		
		return FieldValue;
	}
	
	
	/**
	 * Entity Field 
	 * @param EntityTitle
	 * @param EntityValue
	 */
	public void setField(String EntityTitle, ArrayList EntityValue) throws Exception{
		try{
			if(EntityTitle == null){
				throw new Exception("Entity Title is NULL!");
			}
			else if(EntityTitle.trim().length() == 0){
				throw new Exception("Length of Entity Title is zero!");
			}
			else{
				this.EntityData.put(EntityTitle, EntityValue);
			}
		}catch(Exception e){
			throw e;
		}
	}
	
	
	/**
	 * Entity Field Value Return
	 * @param EntityTitle
	 * @return
	 */
	public ArrayList getFieldArray(String EntityTitle) throws Exception{
		ArrayList FieldValue = null;
		
		try{
			if(EntityTitle == null){
				throw new Exception("Entity Title is NULL!");
			}
			else if(EntityTitle.trim().length() == 0){
				throw new Exception("Length of Entity Title is zero!");
			}
			else{
				if(this.EntityData.get(EntityTitle) == null){
					throw new Exception("Not Exist " + EntityTitle + " in Entity!");
				}
				else if(Utility.getClassName(this.EntityData.get(EntityTitle)).equals("ARRAYLIST")){
					FieldValue = (ArrayList)this.EntityData.get(EntityTitle);
				}
				else{
					throw new Exception(EntityTitle + " is not Array Type!");
				}
			}
		}catch(Exception e){
			throw e;
		}
		
		return FieldValue;
	}
	
	
	/**
	 * Body Field Value Return with No Check Data Null
	 * @param EntityTitle
	 * @return
	 * @throws Exception
	 */
	public ArrayList getFieldArrayNoCheck(String EntityTitle) throws Exception{
		ArrayList FieldValue = null;
		
		try{
			if(EntityTitle == null){
				throw new Exception("Entity Title is NULL!");
			}
			else if(EntityTitle.trim().length() == 0){
				throw new Exception("Length of Body Entity Title is zero!");
			}
			else{
				if(this.EntityData.get(EntityTitle) == null){
					FieldValue = new ArrayList();
				}
				else if(Utility.getClassName(this.EntityData.get(EntityTitle)).equals("ARRAYLIST")){
					FieldValue = (ArrayList)this.EntityData.get(EntityTitle);
				}
				else{
					throw new Exception(EntityTitle + " is not Array Type!");
				}
			}
		}catch(Exception e){
			throw e;
		}
		
		return FieldValue;
	}
	
	
	/**
	 * Entity Field 
	 * @param EntityTitle
	 * @param EntityValue
	 */
	public void setField(String EntityTitle, EntityList EntityValue) throws Exception{
		try{
			if(EntityTitle == null){
				throw new Exception("Entity Title is NULL!");
			}
			else if(EntityTitle.trim().length() == 0){
				throw new Exception("Length of Entity Title is zero!");
			}
			else{
				this.EntityData.put(EntityTitle, EntityValue);
			}
		}catch(Exception e){
			throw e;
		}
	}
	
	
	/**
	 * Entity Field Value Return
	 * @param EntityTitle
	 * @return
	 */
	public EntityList getFieldList(String EntityTitle) throws Exception{
		EntityList FieldValue = null;
		
		try{
			if(EntityTitle == null){
				throw new Exception("Entity Title is NULL!");
			}
			else if(EntityTitle.trim().length() == 0){
				throw new Exception("Length of Entity Title is zero!");
			}
			else{
				if(this.EntityData.get(EntityTitle) == null){
					throw new Exception("Not Exist " + EntityTitle + " in Entity!");
				}
				else if(Utility.getClassName(this.EntityData.get(EntityTitle)).equals("ENTITYLIST")){
					FieldValue = (EntityList)this.EntityData.get(EntityTitle);
				}
				else{
					throw new Exception(EntityTitle + " is not List Type!");
				}
			}
		}catch(Exception e){
			throw e;
		}
		
		return FieldValue;
	}
	
	
	/**
	 * Body Field Value Return with No Check Data Null
	 * @param EntityTitle
	 * @return
	 * @throws Exception
	 */
	public EntityList getFieldListNoCheck(String EntityTitle) throws Exception{
		EntityList FieldValue = null;
		
		try{
			if(EntityTitle == null){
				throw new Exception("Entity Title is NULL!");
			}
			else if(EntityTitle.trim().length() == 0){
				throw new Exception("Length of Body Entity Title is zero!");
			}
			else{
				if(this.EntityData.get(EntityTitle) == null){
					FieldValue = new EntityList();
				}
				else if(Utility.getClassName(this.EntityData.get(EntityTitle)).equals("ENTITYLIST")){
					FieldValue = (EntityList)this.EntityData.get(EntityTitle);
				}
				else{
					throw new Exception(EntityTitle + " is not List Type!(" + Utility.getClassName(this.EntityData.get(EntityTitle)) + ")");
				}
			}
		}catch(Exception e){
			throw e;
		}
		
		return FieldValue;
	}
	
	
	
	public boolean getFieldExist(String EntityTitle) throws Exception{
		return this.EntityData.containsKey(EntityTitle);
	}
	
	
	/**
	 * Body Type Return
	 * @return
	 * @throws Exception
	 */
	public String getEntityType() throws Exception{
		try{
			if(this.EntityType == null){
				throw new Exception("Body Type is NULL!");
			}
			else if(this.EntityType.length() == 0){
				throw new Exception("Length of Body Type is zero!");
			}
		
			return this.EntityType;
		}catch(Exception e){
			throw e;
		}
	}
	
	
	/**
	 * Entity Size Return
	 * @return
	 */
	public int getEntitySize(){
		int EntitySize = 0;
		
		try{
			EntitySize = this.EntityData.size();
		}catch(Exception e){
			EntitySize = 0;
		}
		
		return EntitySize;
	}
	
	
	/**
	 * Entity Keys Return
	 * @return
	 */
	public Enumeration getKeys(){
		return this.EntityData.keys();
	}
}