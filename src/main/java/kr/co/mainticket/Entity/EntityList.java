package kr.co.mainticket.Entity;

import java.util.ArrayList;
import java.io.Serializable;


/**
 * Body Entity
 * @Author E.S. Shin
 * @Date 2009. 03. 05.
 */

public class EntityList implements Serializable{
	private ArrayList ListData = null;
	
	public EntityList(){
		ListData = new ArrayList();
	}
	
	
	/**
	 * Add the Entity into the ListData
	 * @param entity
	 * @throws Exception
	 */
	public void add(Entity entity) throws Exception{
		if(ListData == null){
			throw new Exception("EntityList is not Constructed!");
		}
		
		ListData.add(entity);
	}
	
	
	/**
	 * Add the EntityList into the ListData
	 * @param entity
	 * @throws Exception
	 */
	public void add(EntityList ListEntity) throws Exception{
		if(ListData == null){
			throw new Exception("EntityList is not Constructed!");
		}
		
		for(int idx = 0; idx < ListEntity.size(); idx++){
			ListData.add(ListEntity.get(idx));
		}
	}
	
	
	/**
	 * Return the Entity
	 * @param idx
	 * @return
	 * @throws Exception
	 */
	public Entity get(int idx) throws Exception{
		if(ListData == null){
			throw new Exception("EntityList is not Constructed!");
		}
		else if(idx < 0 || idx >= ListData.size()){
			throw new Exception("Index is overed range!");
		}
		
		return (Entity)ListData.get(idx);
	}
	
	/**
	 * Remove the Entity
	 * @param idx
	 * @return
	 * @throws Exception
	 */
	public Entity remove(int idx) throws Exception{
		if(ListData == null){
			throw new Exception("EntityList is not Constructed!");
		}
		else if(idx < 0 || idx >= ListData.size()){
			throw new Exception("Index is overed range!");
		}
		
		return (Entity)ListData.remove(idx);
	}
	
	
	/**
	 * Size of Entity List
	 * @return
	 */
	public int size(){
		int ListSize = 0;
		
		if(ListData != null){
			ListSize = ListData.size();
		}
		
		return ListSize;
	}
	
	/**
	 * clear the Entity List
	 * @return
	 */
	public void clear(){
		if(ListData != null){
			ListData.clear();
		}
	}
}