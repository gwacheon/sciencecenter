package kr.co.mainticket.Entity;

import java.sql.*;
import java.util.*;

import kr.co.mainticket.Log.*;


public class EntityUtility{

	private EntityUtility(){
	}
	
	
	/**
	 * Select Query
	 * 
	 * @param rs
	 * @return
	 * @throws Exception
	 */
	public static ArrayList SelectQuery(ResultSet rs) throws Exception{
		ArrayList list = new ArrayList();
		
		try{
			
			Entity entity = null;
			ResultSetMetaData rsmd = rs.getMetaData();
			
			String ColumnName = "";
			int ColumnCount = rsmd.getColumnCount();
			
			while(rs.next()){
				entity = new Entity(ColumnCount);
				
				for(int idx = 0; idx < ColumnCount; idx++){
					ColumnName = rsmd.getColumnName(idx + 1);
					
					if(rs.getString(ColumnName) != null){
						entity.setField(ColumnName, rs.getString(ColumnName));
					}
					else{
						entity.setField(ColumnName, "");
					}
				}
				
				list.add(entity);
			}
		}catch(Exception e){
			list = new ArrayList();
			C2SLog.printConsole("Exception occured on EntityUtility.SelectQuery()! " + e.getMessage());
		}
		
		return list;
	}
	
	
	/**
	 * Select Query
	 * 
	 * @param rs
	 * @return
	 * @throws Exception
	 */
	public static EntityList SelectQueryList(ResultSet rs) throws Exception{
		EntityList list = new EntityList();
		
		try{
			
			Entity entity = null;
			ResultSetMetaData rsmd = rs.getMetaData();
			
			String ColumnName = "";
			int ColumnCount = rsmd.getColumnCount();
			
			while(rs.next()){
				entity = new Entity(ColumnCount);
				
				for(int idx = 0; idx < ColumnCount; idx++){
					ColumnName = rsmd.getColumnName(idx + 1);
					
					if(rs.getString(ColumnName) != null){
						entity.setField(ColumnName, rs.getString(ColumnName));
					}
					else{
						entity.setField(ColumnName, "");
					}
				}
				
				list.add(entity);
			}
		}catch(Exception e){
			list = new EntityList();
			C2SLog.printConsole("Exception occured on EntityUtility.SelectQueryList()! " + e.getMessage());
		}
		
		return list;
	}
	
	
	/**
	 * Select Query
	 * @param conn
	 * @param Paragraph
	 * @param condition
	 * @return
	 * @throws Exception
	 */
	public static ArrayList SelectQueryParagraph(Connection conn, String Paragraph, Entity condition) throws Exception{
		ArrayList list = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try{
			pstmt = conn.prepareStatement(Paragraph);
			
			if(condition != null){
				for(int idx = 0; idx < condition.getEntitySize(); idx++){
					pstmt.setString(idx + 1, condition.getField(Integer.toString(idx + 1)));
				}
			}
			
			rs = pstmt.executeQuery();
			
			list = EntityUtility.SelectQuery(rs);
		}catch(Exception e){
			C2SLog.printConsole("Exception occured on EntityUtility.SelectQueryParagraph()! " + e.getMessage());
			C2SLog.printConsole("Exception Query : " + Paragraph);
			throw e;
		}finally{
			if(rs != null) rs.close();
			if(pstmt != null) pstmt.close();
		}
		
		return list;
	}
	
	
	/**
	 * Select Query
	 * @param conn
	 * @param Paragraph
	 * @param condition
	 * @return
	 * @throws Exception
	 */
	public static EntityList SelectQueryParagraphList(Connection conn, String Paragraph, Entity condition) throws Exception{
		EntityList list = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try{
			C2SLog.printLogger("SCIENCE_LOG", "SCIENCE_INFO", Paragraph, null, 1);//쿼리로그.
			
			pstmt = conn.prepareStatement(Paragraph);
		
			if(condition != null){
				for(int idx = 0; idx < condition.getEntitySize(); idx++){
					pstmt.setString(idx + 1, condition.getField(Integer.toString(idx + 1)));
					C2SLog.printLogger("SCIENCE_LOG", "SCIENCE_INFO", "PARAM:["+(idx + 1) + ","+condition.getField(Integer.toString(idx + 1))+"]", null, 1);//파람로그.
				}
			}
			
			rs = pstmt.executeQuery();
			
			list = EntityUtility.SelectQueryList(rs);
		}catch(Exception e){
			C2SLog.printConsole("Exception occured on EntityUtility.SelectQueryParagraphList()! " + e.getMessage());
			C2SLog.printConsole("Exception Query : " + Paragraph);
			throw e;
		}finally{
			if(rs != null) rs.close();
			if(pstmt != null) pstmt.close();
		}
		
		return list;
	}
	
	
	/**
	 * Insert, Update Query
	 * @param conn
	 * @param QueryParagraph
	 * @param condition
	 * @return
	 * @throws Exception
	 */
	public static int UpdateQueryParagraph(Connection conn, String QueryParagraph, Entity condition) throws Exception{
		int UpdateCount = 0;
		PreparedStatement pstmt = null;

		try{
			pstmt = conn.prepareStatement(QueryParagraph);
			
			if(condition != null){
				for(int idx = 0; idx < condition.getEntitySize(); idx++){
					pstmt.setString(idx + 1, condition.getField(Integer.toString(idx + 1)));
				}
			}
			
			UpdateCount = pstmt.executeUpdate();
			
			pstmt.close();
		}catch(Exception e){
			C2SLog.printConsole("Exception occured on EntityUtility.UpdateQuery()! " + e.getMessage());
			C2SLog.printConsole("Exception Query : " + QueryParagraph);
			throw e;
		}finally{
			if(pstmt != null){
				try{
					pstmt.close();
				}catch(Exception pe){
				}
			}
		}
		
		return UpdateCount;
	}
	
	
	/**
	 * Insert, Update Looping Query
	 * @param conn
	 * @param Paragraph
	 * @param ConditionList
	 * @return
	 * @throws Exception
	 */
	public static int UpdateQueryParagraph(Connection conn, String Paragraph, ArrayList ConditionList) throws Exception{
		int UpdateCount = 0;
		PreparedStatement pstmt = null;

		try{
			pstmt = conn.prepareStatement(Paragraph);
			
			Entity condition = null;
			for(int idxL = 0; idxL < ConditionList.size(); idxL++){
				pstmt.clearParameters();
				condition = (Entity)ConditionList.get(idxL);
				
				for(int idx = 0; idx < condition.getEntitySize(); idx++){
					pstmt.setString(idx + 1, condition.getField(Integer.toString(idx + 1)));
				}
				
				UpdateCount += pstmt.executeUpdate();
			}
		}catch(Exception e){
			C2SLog.printConsole("Exception occured on EntityUtility.UpdateQuery()! " + e.getMessage());
			C2SLog.printConsole("Exception Query : " + Paragraph);
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
		}
		
		return UpdateCount;
	}
	
	
	/**
	 * Insert, Update Looping Query
	 * @param conn
	 * @param Paragraph
	 * @param ConditionList
	 * @return
	 * @throws Exception
	 */
	public static int UpdateQueryParagraphList(Connection conn, String Paragraph, EntityList ConditionList) throws Exception{
		int UpdateCount = 0;
		PreparedStatement pstmt = null;

		try{
			pstmt = conn.prepareStatement(Paragraph);
			
			Entity condition = null;
			for(int idxL = 0; idxL < ConditionList.size(); idxL++){
				pstmt.clearParameters();
				condition = ConditionList.get(idxL);
				
				for(int idx = 0; idx < condition.getEntitySize(); idx++){
					pstmt.setString(idx + 1, condition.getField(Integer.toString(idx + 1)));
				}
				
				UpdateCount += pstmt.executeUpdate();
			}
		}catch(Exception e){
			C2SLog.printConsole("Exception occured on EntityUtility.UpdateQuery()! " + e.getMessage());
			C2SLog.printConsole("Exception Query : " + Paragraph);
			throw e;
		}finally{
			if(pstmt != null) pstmt.close();
		}
		
		return UpdateCount;
	}
	
	
	/**
	 * Insert, Update Query Set
	 * ArrayList({Query Paragraph(String), Mapping Data(Entity)}
	 * @param conn
	 * @param UpdateList
	 * @return
	 * @throws Exception
	 */
	public static int UpdateQueryParagraph(Connection conn, ArrayList UpdateList) throws Exception{
		int UpdateCount = 0;
		String Paragraph = "";
		PreparedStatement pstmt = null;

		try{
			ArrayList ConditionList = null;
			Entity condition = null;
			
			for(int idxL = 0; idxL < UpdateList.size(); idxL++){
				ConditionList = (ArrayList)UpdateList.get(idxL);
				
				Paragraph = (String)ConditionList.get(0);
				condition = (Entity)ConditionList.get(1);
				
				pstmt = conn.prepareStatement(Paragraph);
			
				for(int idx = 0; idx < condition.getEntitySize(); idx++){
					pstmt.setString(idx + 1, condition.getField(Integer.toString(idx + 1)));
				}
				
				UpdateCount += pstmt.executeUpdate();
				
				pstmt.close();
			}
		}catch(Exception e){
			C2SLog.printConsole("Exception occured on EntityUtility.UpdateQuery()! " + e.getMessage());
			C2SLog.printConsole("Exception Query : " + Paragraph);
			throw e;
		}finally{
			if(pstmt != null){
				try{
					pstmt.close();
				}catch(Exception pe){
				}
			}
		}
		
		return UpdateCount;
	}
}