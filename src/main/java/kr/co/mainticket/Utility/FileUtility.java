package kr.co.mainticket.Utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class FileUtility {
	
	private FileUtility(){
	}
	
	/**
	 * @author SESB
	 * @description If the direcoty is not exist, make the directory.
	 * @param DirectoryPath
	 * @return
	 */
	public static boolean CheckDirectory(String DirectoryPath){
		boolean result = false;
		
		try{
			File Directory = new File(DirectoryPath);
			
			if(Directory.isDirectory() == false){
				if(Directory.mkdirs() == false){ 
					throw new SecurityException(); 
				}
			}
			
			result = true;
		}catch(Exception e){
			result = false;
		}
		
		return result;
	}
	
	
	/**
	 * @author SESB
	 * @param SourceFilePath
	 * @param SourceFileName
	 * @param TargetFilePath
	 * @param TargetFileName
	 * @param IndexFlag
	 * @param SourceDeleteFlag
	 * @return
	 */
	public static String ArrangeFile(String SourceFilePath, String SourceFileName, String TargetFilePath, String TargetFileName, boolean IndexFlag, boolean SourceDeleteFlag){
		String ArrangeFileName = "";
		
		if(SourceFileName.length() > 0 && TargetFilePath.length() > 0){
			try{
				CheckDirectory(TargetFilePath);
				
				if(TargetFileName.length() > 0){
					ArrangeFileName = TargetFileName;
				}
				else{
					ArrangeFileName = SourceFileName;
				}
				
				String Prefix = ArrangeFileName.substring(0, ArrangeFileName.lastIndexOf('.'));
				String Suffix = ArrangeFileName.substring(ArrangeFileName.lastIndexOf('.'));
				
				File SourceFile = new File(SourceFilePath + SourceFileName);
				File TargetFile = new File(TargetFilePath + ArrangeFileName);
				
				if(IndexFlag){
					int ExistIdx = 1;
					while(!TargetFile.createNewFile()){
						ArrangeFileName = Prefix + "[" + ExistIdx + "]" + Suffix;
						TargetFile = new File(TargetFilePath + ArrangeFileName);
						ExistIdx++;
					}
				}
				else{
					if(TargetFile.exists()){
						TargetFile.renameTo(new File(TargetFilePath + Prefix + "_BAK" + DateTime.getFormatDateString("yyyyMMddHHmmss", 0) + Suffix));
					}
				}
				
				
				InputStream is = null;
				OutputStream os = null;
				byte[] buff = new byte[1024];
				int read = 0;
				
				try{
					is = new FileInputStream(SourceFile);
					os = new FileOutputStream(TargetFile);
					
					while((read = is.read(buff)) != -1){
						os.write(buff, 0 , read);
						os.flush();
					}
				}finally{
					if(os != null) os.close();
					if(is != null) is.close();
				}
				
				if(SourceDeleteFlag){
					SourceFile.delete();
				}
			}catch(Exception e){
				ArrangeFileName = "";
			}
		}
		
		return ArrangeFileName;
	}
}