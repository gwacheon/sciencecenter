package kr.co.mainticket.Utility;


public class DateTime {

	/**
	 * Don't let anyone instantiate this class
	 */
	private DateTime() {}
	
	
	/**
	 * @return formatted string representation of Julian Day
	 */
	public static int getJulianDay() throws java.text.ParseException{
		String CurrDate = getShortDateString();
		
		return daysBetween(CurrDate.substring(0, 4) + "0101", CurrDate) + 1;
	}
	
	/**
	 * @return formatted string representation of Julian Day String
	 */
	public static String getJulianDayString() throws java.text.ParseException{
		return Integer.toString(getJulianDay());
	}
	
	
	/**
	 * @return formatted string representation of current day with "yyyyMMdd".
	 */
	public static String getShortDateString(){
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyyMMdd", java.util.Locale.KOREA);
		return formatter.format(new java.util.Date());
	}
	
	
	/**
	 * @return formatted string representation of current time with "yyyy-MM-dd HH:mm:ss".
	 */
	public static String getTimeStampString() {
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS", java.util.Locale.KOREA);
		return formatter.format(new java.util.Date());
	}
	
	
	/**
	 * @param format
	 * @param days
	 * @return format string representation of the date format. For example, "yyyy-MM-dd".
	 */
	public static String getFormatDateString(String format, int days){
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(format, java.util.Locale.KOREA);
		java.util.Date date = new java.util.Date();
		date.setTime(date.getTime() + ((long)days * 1000 * 60 * 60 * 24));
		
		return formatter.format(date);
	}
	
	
	/**
	 * @param format
	 * @param days
	 * @return format string representation of the date format. For example, "yyyy-MM-dd".
	 */
	public static java.util.Date getFormatDate(String format, String DateStr){
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(format, java.util.Locale.KOREA);
		java.util.Date date = null;
		try{
			date = formatter.parse(DateStr);
		}catch(Exception e){
			date = new java.util.Date();
		}
		
		return date;
	}
	

	public static int whichDay(String DateStr) throws java.text.ParseException{
		return whichDay("yyyyMMdd", DateStr);
	}
	
	public static int whichDay(String format, String DateStr) throws java.text.ParseException{
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(format, java.util.Locale.KOREA);
		java.util.Date date = getFormatDate(format, DateStr);

		java.util.Calendar calendar = formatter.getCalendar();
		calendar.setTime(date);
	
		return calendar.get(java.util.Calendar.DAY_OF_WEEK);
	}
	
	public static String whichDayString(String DateStr){
		String DayString = "";
		
		try{
			int DayInt = whichDay("yyyyMMdd", DateStr);
			
			if(DayInt == 1){
				DayString = "일";
			}
			else if(DayInt == 2){
				DayString = "월";
			}
			else if(DayInt == 3){
				DayString = "화";
			}
			else if(DayInt == 4){
				DayString = "수";
			}
			else if(DayInt == 5){
				DayString = "목";
			}
			else if(DayInt == 6){
				DayString = "금";
			}
			else if(DayInt == 7){
				DayString = "토";
			}
		}catch(Exception e){
			DayString = "";
		}
		return DayString;
	}
	
	public static int timesBetween(String from, String to) throws java.text.ParseException{
		return timesBetween("yyyyMMddHHmm", from, to);
	}
	
	public static int timesBetween(String format, String from, String to) throws java.text.ParseException {
		java.util.Date d1 = getFormatDate(format, from);
		java.util.Date d2 = getFormatDate(format, to);

		long duration = d2.getTime() - d1.getTime();

		return (int)(duration/(1000 * 60));
    }
	
	public static boolean checkTimesBetween(String date, String from, String to) throws java.text.ParseException{
		if(date.length() == 0 || from.length() == 0 || to.length() == 0){
			return false;
		}
		
		if(timesBetween("yyyyMMddHHmmss", from, date) >= 0 && timesBetween("yyyyMMddHHmmss", date, to) >= 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	
	public static int daysBetween(String from, String to) throws java.text.ParseException{
		return daysBetween("yyyyMMdd", from, to);
	}
	
	public static int daysBetween(String format, String from, String to) throws java.text.ParseException {
		java.util.Date d1 = getFormatDate(format, from);
		java.util.Date d2 = getFormatDate(format, to);

		long duration = d2.getTime() - d1.getTime();

		return (int)(duration/(1000 * 60 * 60 * 24));
    }
	
	public static boolean checkBetween(String date, String from, String to) throws java.text.ParseException{
		if(date.length() == 0 || from.length() == 0 || to.length() == 0){
			return false;
		}
		
		if(daysBetween("yyyyMMdd", from, date) >= 0 && daysBetween("yyyyMMdd", date, to) >= 0){
			return true;
		}
		else{
			return false;
		}
	}
	

	public static int yearsBetween(String from, String to) throws java.text.ParseException{
		return yearsBetween("yyyyMMdd", from, to);
	}
	
	public static int yearsBetween(String format, String from, String to) throws java.text.ParseException {
		java.util.Date fromDate = getFormatDate(format, from);
		java.util.Date toDate = getFormatDate(format, to);
		
		java.text.SimpleDateFormat DaysFormat = new java.text.SimpleDateFormat("yyyyMMdd", java.util.Locale.KOREA);
		
		int result = (Integer.parseInt(DaysFormat.format(toDate)) - Integer.parseInt(DaysFormat.format(fromDate))) / 10000;
		
		return result;
	}
	
	
	public static int monthsBetween(String from, String to) throws java.text.ParseException{
		return monthsBetween("yyyyMMdd", from, to);
	}

	public static int monthsBetween(String format, String from, String to) throws java.text.ParseException {
		java.util.Date fromDate = getFormatDate(format, from);
		java.util.Date toDate = getFormatDate(format, to);

		// if two date are same, return 0.
		if(fromDate.compareTo(toDate) == 0) return 0;

		java.text.SimpleDateFormat yearFormat = new java.text.SimpleDateFormat("yyyy", java.util.Locale.KOREA);
		java.text.SimpleDateFormat monthFormat = new java.text.SimpleDateFormat("MM", java.util.Locale.KOREA);
		java.text.SimpleDateFormat dayFormat = new java.text.SimpleDateFormat("dd", java.util.Locale.KOREA);

		int fromYear = Integer.parseInt(yearFormat.format(fromDate));
		int toYear = Integer.parseInt(yearFormat.format(toDate));
		int fromMonth = Integer.parseInt(monthFormat.format(fromDate));
		int toMonth = Integer.parseInt(monthFormat.format(toDate));
		int fromDay = Integer.parseInt(dayFormat.format(fromDate));
		int toDay = Integer.parseInt(dayFormat.format(toDate));

		int result = 0;
		result += ((toYear - fromYear) * 12);
		result += (toMonth - fromMonth);

		if((toDay - fromDay) > 0){
			result += toDate.compareTo(fromDate);
		}

		return result;
	}
	
	public static String addMinutes(String DateStr, int minutes) throws java.text.ParseException{
		return addMinutes("yyyyMMddHHmmss", DateStr, minutes);
	}

	public static String addMinutes(String format, String DateStr, int minutes) throws java.text.ParseException{
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(format, java.util.Locale.KOREA);
		java.util.Date date = getFormatDate(format, DateStr);

		date.setTime(date.getTime() + ((long)minutes * 1000 * 60));
		return formatter.format(date);
	}
	
	public static String addHours(String DateStr, int hour) throws java.text.ParseException{
		return addHours("yyyyMMddHHmmss", DateStr, hour);
	}

	public static String addHours(String format, String DateStr, int hour) throws java.text.ParseException{
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(format, java.util.Locale.KOREA);
		java.util.Date date = getFormatDate(format, DateStr);

		date.setTime(date.getTime() + ((long)hour * 1000 * 60 * 60));
		return formatter.format(date);
	}


	public static String addDays(String DateStr, int day) throws java.text.ParseException{
		return addDays("yyyyMMdd", DateStr, day);
	}

	public static String addDays(String format, String DateStr, int day) throws java.text.ParseException{
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(format, java.util.Locale.KOREA);
		java.util.Date date = getFormatDate(format, DateStr);

		date.setTime(date.getTime() + ((long)day * 1000 * 60 * 60 * 24));
		return formatter.format(date);
	}

	public static String addMonths(String DateStr, int month) throws Exception{
		return addMonths("yyyyMMdd", DateStr, month);
	}
	
	public static String addMonths(String format, String DateStr, int addMonth) throws Exception {
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(format, java.util.Locale.KOREA);
		java.util.Date date = getFormatDate(format, DateStr);

		java.text.SimpleDateFormat yearFormat = new java.text.SimpleDateFormat("yyyy", java.util.Locale.KOREA);
		java.text.SimpleDateFormat monthFormat = new java.text.SimpleDateFormat("MM", java.util.Locale.KOREA);
		java.text.SimpleDateFormat dayFormat = new java.text.SimpleDateFormat("dd", java.util.Locale.KOREA);
		
		int year = Integer.parseInt(yearFormat.format(date));
		int month = Integer.parseInt(monthFormat.format(date));
		int day = Integer.parseInt(dayFormat.format(date));

		month += addMonth;
		if(addMonth > 0){
			while(month > 12){
				month -= 12;
				year += 1;
			}
		}
		else{
			while(month <= 0){
				month += 12;
				year -= 1;
			}
		}

		java.text.DecimalFormat fourDf = new java.text.DecimalFormat("0000");
		java.text.DecimalFormat twoDf = new java.text.DecimalFormat("00");
		String tempDate = String.valueOf(fourDf.format(year)) + String.valueOf(twoDf.format(month)) + String.valueOf(twoDf.format(day));
		java.util.Date targetDate = null;

		try{
			targetDate = getFormatDate("yyyyMMdd", tempDate);
		}catch(Exception pe){
			day = lastDay(year, month);
			tempDate = String.valueOf(fourDf.format(year)) + String.valueOf(twoDf.format(month)) + String.valueOf(twoDf.format(day));
			targetDate = getFormatDate("yyyyMMdd", tempDate);
		}

		return formatter.format(targetDate);
	}

	
	public static String addYears(String DateStr, int year) throws java.text.ParseException{
		return addYears("yyyyMMdd", DateStr, year);
	}

	public static String addYears(String format, String DateStr, int year) throws java.text.ParseException {
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(format, java.util.Locale.KOREA);
		java.util.Date date = getFormatDate(format, DateStr);
		date.setTime(date.getTime() + ((long)year * 1000 * 60 * 60 * 24 * (365 + 1)));
		return formatter.format(date);
	}
	
	
	public static String lastDayOfMonth(String DateStr) throws java.text.ParseException{
        return lastDayOfMonth("yyyyMMdd", DateStr);
    }

	public static String lastDayOfMonth(String format, String DateStr) throws java.text.ParseException {
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(format, java.util.Locale.KOREA);
		java.util.Date date = getFormatDate(format, DateStr);

		java.text.SimpleDateFormat yearFormat = new java.text.SimpleDateFormat("yyyy", java.util.Locale.KOREA);
		java.text.SimpleDateFormat monthFormat = new java.text.SimpleDateFormat("MM", java.util.Locale.KOREA);

		int year = Integer.parseInt(yearFormat.format(date));
		int month = Integer.parseInt(monthFormat.format(date));
		int day = lastDay(year, month);

		java.text.DecimalFormat fourDf = new java.text.DecimalFormat("0000");
		java.text.DecimalFormat twoDf = new java.text.DecimalFormat("00");
		String tempDate = String.valueOf(fourDf.format(year)) + String.valueOf(twoDf.format(month)) + String.valueOf(twoDf.format(day));
		date = getFormatDate(format, tempDate);

		return formatter.format(date);
	}

	
	public static int lastDay(int year, int month) throws java.text.ParseException{
		int day = 0;
		switch(month){
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12: day = 31; break;
			case 2: if((year % 4) == 0){
						if((year % 100) == 0 && (year % 400) != 0){
							day = 28;
						}
						else{
							day = 29;
						}
					}
					else{
						day = 28;
					}
					break;
			default: day = 30;
		}

		return day;
	}
}
