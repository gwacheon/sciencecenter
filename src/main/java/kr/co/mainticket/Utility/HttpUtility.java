package kr.co.mainticket.Utility;

import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;


public class HttpUtility{
	private static boolean PostEncFlag = true;
	private static boolean GetEncFlag = true;
	private static String EncodingSet = "";
	
	/**
	 * You can't call the constructor.
	 */
	private HttpUtility(){}

	
	/**
	 * Decode a string from <code>x-www-form-urlencoded</code> format.
	 *
	 * @param   s   an encoded <code>String</code> to be translated.
	 * @return  the original <code>String</code>.
	 * @see		java.net.URLEncoder#encode(java.lang.String)
	 */
	public static String decode(String s, String enc) throws UnsupportedEncodingException{
		return java.net.URLDecoder.decode(s, enc); //since jdk 1.4
	}
	
	
	/**
	 * Translates a string into <code>x-www-form-urlencoded</code> format.
	 *
	 * @param   s   <code>String</code> to be translated.
	 * @return  the translated <code>String</code>.
	 * @see		java.net.URLEncoder#encode(java.lang.String)
	 */
	public static String encode(String s, String enc) throws UnsupportedEncodingException{
		return java.net.URLEncoder.encode(s, enc); //since jdk 1.4
	}
	

	/**
	 * @param req javax.servlet.http.HttpServletRequest
	 * @param name box name for this SessionBox
	 */
	public static Box getBox(HttpServletRequest req){
		Box box = new Box("requestbox");
		
		if(req.getMethod().equals("POST") && PostEncFlag){
			box.setChkKor(true);
		}
		// Local Tomcat Server
		else if(req.getMethod().equals("GET") && GetEncFlag){
			box.setChkKor(true);
		}
		
		if(EncodingSet.length() > 0){
			box.setEncodingSet(EncodingSet);
		}

		Enumeration e = req.getParameterNames();
		while(e.hasMoreElements()){
			String key = (String)e.nextElement();
			box.put(key, req.getParameterValues(key));
		}
		return box;
	}
	
	
	/**
	 * @param req javax.servlet.http.HttpServletRequest
	 * @param name box name for this SessionBox
	 */
	public static Box getBoxFromCookie(HttpServletRequest req){
		Box cookiebox = new Box("cookiebox");
		javax.servlet.http.Cookie[] cookies = req.getCookies();
		
		if(cookies == null) return cookiebox;

		for(int i = 0; cookies != null && i < cookies.length; i++){
			String key = cookies[i].getName();
			String value = cookies[i].getValue();
			
			if(value == null){
				value = "";
			}
			
			String[] values = new String[1];
			values[0] = value;
			cookiebox.put(key,values);
		}
		return cookiebox;
	}	
	

	/**
	 * @param req javax.servlet.http.HttpServletRequest
	 * @param name box name for this SessionBox
	 */
	public static SessionBox getNewSessionBox(HttpServletRequest req){
		return getNewSessionBox(req, "shared session box");
	}
	
	
	
	

	/**
	 * @param req javax.servlet.http.HttpServletRequest
	 * @param name box name for this SessionBox
	 */
	public static SessionBox getNewSessionBox(HttpServletRequest req, String name){
		javax.servlet.http.HttpSession  session = req.getSession(true);
		
		SessionBox sessionbox = null;
		
		if(!session.isNew()){
			Object o = session.getAttribute(name);
			if(o != null){
				session.removeAttribute(name);
			}
		}

		if(sessionbox == null){
			sessionbox = new SessionBox(session, name);
			session.setAttribute(name, sessionbox);
			session.setMaxInactiveInterval(60 * 60 * 24);
		}
		
		Enumeration e = req.getParameterNames();
		
		while(e.hasMoreElements()){
			String key = (String)e.nextElement();
			sessionbox.put(key, req.getParameterValues(key));
		}
		return sessionbox;
	}
	
	
	/**
	 * @param req javax.servlet.http.HttpServletRequest
	 * @param name box name for this SessionBox
	 */
	public static SessionBox getSessionBox(HttpServletRequest req){
		return getSessionBox(req, "shared session box");
	}
	

	/**
	 * @param req javax.servlet.http.HttpServletRequest
	 * @param name box name for this SessionBox
	 */
	public static SessionBox getSessionBox(HttpServletRequest req, String name){
		javax.servlet.http.HttpSession  session = req.getSession(true);
		
		SessionBox sessionbox = null;
		
		Object o = session.getAttribute(name);
		if(o != null){
			if(o instanceof SessionBox){
				sessionbox = (SessionBox)o;
			}
			else{
				session.removeAttribute(name);
			}
		}

		if(sessionbox == null){
			sessionbox = new SessionBox(session, name);
			session.setAttribute(name, sessionbox);
			session.setMaxInactiveInterval(60 * 60 * 24);
		}
		
		Enumeration e = req.getParameterNames();
		while(e.hasMoreElements()){
			String key = (String)e.nextElement();
			sessionbox.put(key, req.getParameterValues(key));
		}
		return sessionbox;
	}	
	
	
	/**
	 * 
	 * @return boolean
	 * @param req HttpServletRequest
	 */
	public static boolean isOverIE50(HttpServletRequest req){
		String user_agent = (String) req.getHeader("user-agent");

		if(user_agent == null){
			return false;
		}

		int index = user_agent.indexOf("MSIE");
		
		if(index == -1){
			return false;
		}

		int version = 0;
		try{
			version = Integer.parseInt(user_agent.substring(index + 5, index + 5 + 1));
		}catch(Exception e){}
		
		if(version < 5){
			return false;
		}
		
		return true;
	}
	
	
	public static void setPostEncFlag(boolean Flag){
		PostEncFlag = Flag;
	}
	
	public static void setGetEncFlag(boolean Flag){
		GetEncFlag = Flag;
	}
	
	public static void setEncodingSet(String EncSet){
		EncodingSet = EncSet;
	}
	
	@SuppressWarnings("rawtypes")
	public static HashMap<String, String> handleRequest (HttpServletRequest request) throws Exception, IllegalStateException {
		
    	HashMap<String, String> params = new HashMap<String, String>();
		String value = "";
		Enumeration enu = request.getParameterNames();
		while(enu.hasMoreElements()){
			String param =(String)enu.nextElement();
			value = request.getParameter(param);
			if(value == null) value = "";
			params.put(param, value);
		}
		return params;
	}
}