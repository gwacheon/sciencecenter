package kr.co.mainticket.Encrypt;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class AESCoder{
	
	public static byte[] AESEncrypt(byte[] SourceData, String AesKey){
		byte[] AESData = null;
		
		try{
			if(SourceData != null && SourceData.length > 0){
				SecretKeySpec KeySpec = new SecretKeySpec(AesKey.getBytes(), "AES");
				Cipher cipher = Cipher.getInstance("AES");
				
				cipher.init(Cipher.ENCRYPT_MODE, KeySpec);
				
				AESData = cipher.doFinal(SourceData);
			}
			else{
				AESData = new byte[0];
			}
		}catch(Exception e){
			AESData = null;
		}
		
		return AESData;
	}
	
	public static byte[] AESEncrypt(String SourceData, int KeyIndex){
		String AesKey = C2SAESKey.getDecryptKey(KeyIndex);
		
		return AESEncrypt(SourceData.getBytes(), AesKey);
	}
	
	public static String AESEncryptString(byte[] SourceData, int KeyIndex){
		String AesKey = C2SAESKey.getDecryptKey(KeyIndex);
		
		return Base64Coder.BaseEncodeString(AESEncrypt(SourceData, AesKey), "2");
	}
	
	public static String AESEncryptString(String SourceData, int KeyIndex){
		String AesKey = C2SAESKey.getDecryptKey(KeyIndex);
		
		return Base64Coder.BaseEncodeString(AESEncrypt(SourceData.getBytes(), AesKey), "2");
	}
	
	
	
	public static byte[] AESDecrypt(byte[] SourceData, String AesKey){
		byte[] AESData = null;
		
		try{
			if(SourceData != null && SourceData.length > 0){
				SecretKeySpec KeySpec = new SecretKeySpec(AesKey.getBytes(), "AES");
				Cipher cipher = Cipher.getInstance("AES");
				
				cipher.init(Cipher.DECRYPT_MODE, KeySpec);
				
				AESData = cipher.doFinal(SourceData);
			}
			else{
				AESData = new byte[0];
			}
		}catch(Exception e){
			AESData = null;
			e.printStackTrace();
		}
		
		return AESData;
	}
	
	public static byte[] AESDecrypt(String SourceData, int KeyIndex){
		String AesKey = C2SAESKey.getDecryptKey(KeyIndex);
		
		return AESDecrypt(Base64Coder.BaseDecode(SourceData, "2"), AesKey);
	}
	
	public static String AESDecryptString(byte[] SourceData, int KeyIndex){
		String AesKey = C2SAESKey.getDecryptKey(KeyIndex);
		
		return new String(AESDecrypt(SourceData, AesKey));
	}
	
	public static String AESDecryptString(String SourceData, int KeyIndex){
		String AesKey = C2SAESKey.getDecryptKey(KeyIndex);
		
		return new String(AESDecrypt(Base64Coder.BaseDecode(SourceData, "2"), AesKey));
	}
}