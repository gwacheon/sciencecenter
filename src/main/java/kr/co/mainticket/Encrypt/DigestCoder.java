package kr.co.mainticket.Encrypt;

import java.security.MessageDigest;

/**
 * MD5 Encoder/Decoder.
 * @author Administrator
 */

public class DigestCoder{

	private static byte[] DigestEncrypt(String DigestType, byte[] SourceData) throws Exception{
		/*
		 * DigestType
		 * MD2, MD5, SHA-1, SHA-256, SHA-384, SHA-512
		 */
		
		byte[] EncryptData = null;
		
		if(SourceData != null && SourceData.length > 0){
			MessageDigest md = MessageDigest.getInstance(DigestType);
			
			md.update(SourceData);
			
			EncryptData = md.digest();
		}
		else{
			EncryptData = new byte[0];
		}
		
		
		return EncryptData;
	}
	
	public static byte[] DigestEncrypt(String DigestType, String SourceData) throws Exception{
		return DigestEncrypt(DigestType, SourceData.getBytes());
	}
	
	public static String DigestEncryptString(String DigestType, byte[] SourceData) throws Exception{
		byte[] EncryptData = DigestEncrypt(DigestType, SourceData);
		StringBuffer EncryptString = new StringBuffer();
		
		for(int idx = 0; idx < EncryptData.length; idx++){
			EncryptString.append(Integer.toString((EncryptData[idx] & 0xff) + 0x100, 16).substring(1));
		}
		
		return EncryptString.toString();
	}
	
	public static String DigestEncryptString(String DigestType, String SourceData) throws Exception{
		return DigestEncryptString(DigestType, SourceData.getBytes());
	}
	
	public static String DigestEncryptBase64(String DigestType, byte[] SourceData) throws Exception{
		return Base64Coder.BaseEncodeString(DigestEncrypt(DigestType, SourceData));
	}
	
	public static String DigestEncryptBase64(String DigestType, String SourceData) throws Exception{
		return new String(DigestEncryptBase64(DigestType, SourceData.getBytes()));
	}
}
