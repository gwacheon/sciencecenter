package kr.co.mainticket.Encrypt;

import java.security.PrivateKey;

public class C2Decrypt {
	private String MESSAGE;
	
	public C2Decrypt(String message){
		this.MESSAGE = message;
	}
	
	public String getBaseDecrypt() throws Exception{
		if(this.MESSAGE == null || this.MESSAGE.length() == 0){
			throw new Exception("Must call Constructor!!");
		}
		
		try{
			String DecryptData = Base64Coder.BaseDecodeString(this.MESSAGE);
			
			return DecryptData;
		}
		catch(Exception e){
			e.printStackTrace();
			throw new Exception("C2Decrypt.getBaseDecrypt : " + e.getMessage());
		}
	}
	
	public String getSeedDecrypt() throws Exception{
		if(this.MESSAGE == null || this.MESSAGE.length() == 0){
			throw new Exception("Must call Constructor!!");
		}
		
		try{
			int KeyIndex = Integer.parseInt(this.MESSAGE.substring(0, 2));
			
			String DecryptData = SeedCoder.SeedDecryptString(this.MESSAGE, KeyIndex, C2SSeedKey.getDecryptKey(KeyIndex));
			
			return DecryptData;
		}
		catch(Exception e){
			e.printStackTrace();
			throw new Exception("C2Decrypt.getSeedDecrypt : " + e.getMessage());
		}
	}
	
	public String getSeedBaseDecrypt() throws Exception{
		if(this.MESSAGE == null || this.MESSAGE.length() == 0){
			throw new Exception("Must call Constructor!!");
		}
		
		try{
			byte[] BaseData = Base64Coder.BaseDecode(this.MESSAGE);
			
			byte KeyIndexByte[] = new byte[2];
			for(int idx = 0; idx < KeyIndexByte.length; idx++){
				KeyIndexByte[idx] = BaseData[idx];
			}
			
			int KeyIndex = Integer.parseInt(new String(KeyIndexByte));
			
			byte source[] = new byte[BaseData.length - KeyIndexByte.length];
			for(int idx = 0; idx < source.length; idx++){
				source[idx] = BaseData[idx + KeyIndexByte.length];
			}
			
			String DecryptData = SeedCoder.SeedDecryptString(source, KeyIndex, C2SSeedKey.getDecryptKey(KeyIndex));
			
			return DecryptData;
		}
		catch(Exception e){
			e.printStackTrace();
			throw new Exception("C2Decrypt.getSeedBaseDecrypt : " + e.getMessage());
		}
	}
	
	public String getRSADecrypt(PrivateKey PriKey) throws Exception{
		if(this.MESSAGE == null){
			throw new Exception("Must call Constructor!!");
		}
		else if(PriKey == null){
			throw new Exception("Private Key is NULL!");
		}
		
		try{
			String DecryptData = RSACoder.RSADecryptString(this.MESSAGE, PriKey);
			
			return DecryptData;
		}
		catch(Exception e){
			e.printStackTrace();
			throw new Exception("C2Decrypt.getRSADecrypt : " + e.getMessage());
		}
	}
}