package kr.co.mainticket.Encrypt;

import java.security.PublicKey;

public class C2Encrypt {
	private String MESSAGE;
	
	public C2Encrypt(String message){
		this.MESSAGE = message;
	}
	
	public String getBaseEncrypt() throws Exception{
		if(this.MESSAGE == null || this.MESSAGE.length() == 0){
			throw new Exception("Must call Constructor!!");
		}
		
		try{
			String EncryptData = Base64Coder.BaseEncodeString(this.MESSAGE);
			
			return EncryptData;
		}
		catch(Exception e){
			e.printStackTrace();
			throw new Exception("C2Encrypt.getBaseEncrypt : " + e.getMessage());
		}
	}
	
	public String getSeedEncrypt() throws Exception{
		if(this.MESSAGE == null || this.MESSAGE.length() == 0){
			throw new Exception("Must call Constructor!!");
		}
		
		try{
			C2SSeedKey SeedKey = new C2SSeedKey();
			boolean check = SeedKey.RandomKey();
			
			String EncryptData = "";
			
			if(check){
				EncryptData = SeedCoder.SeedEncryptString(this.MESSAGE, SeedKey.getKeyIndex(), SeedKey.getKey());
			}
			
			return EncryptData;
		}
		catch(Exception e){
			e.printStackTrace();
			throw new Exception("C2Encrypt.getSeedEncrypt : " + e.getMessage());
		}
	}
	
	public String getSeedBaseEncrypt() throws Exception{
		if(this.MESSAGE == null || this.MESSAGE.length() == 0){
			throw new Exception("Must call Constructor!!");
		}
		
		try{
			C2SSeedKey SeedKey = new C2SSeedKey();
			boolean check = SeedKey.RandomKey();
			
			String EncryptData = "";
			
			if(check){
				byte SeedData[] = SeedCoder.SeedEncrypt(this.MESSAGE, SeedKey.getKeyIndex(), SeedKey.getKey());
				
				EncryptData = Base64Coder.BaseEncodeString(SeedData);
			}
			
			return EncryptData;
		}
		catch(Exception e){
			e.printStackTrace();
			throw new Exception("C2Encrypt.getSeedBaseEncrypt : " + e.getMessage());
		}
	}
	
	public String getDigestEncrypt(String DigestType) throws Exception{
		if(this.MESSAGE == null || this.MESSAGE.length() == 0){
			throw new Exception("Must call Constructor!!");
		}
		
		try{
			String EncryptData = DigestCoder.DigestEncryptString(DigestType, this.MESSAGE);
			
			return EncryptData;
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("C2Encrypt.getDigestEncrypt : " + e.getMessage());
		}
	}
	
	public String getRSAEncrypt(PublicKey PubKey) throws Exception{
		if(this.MESSAGE == null){
			throw new Exception("Must call Constructor!!");
		}
		else if(PubKey == null){
			throw new Exception("Public Key is NULL!");
		}
		
		try{
			String EncryptData = RSACoder.RSAEncryptString(this.MESSAGE, PubKey);
			
			return EncryptData;
		}
		catch(Exception e){
			e.printStackTrace();
			throw new Exception("C2EncryptgetRSAEncrypt : " + e.getMessage());
		}
	}
}