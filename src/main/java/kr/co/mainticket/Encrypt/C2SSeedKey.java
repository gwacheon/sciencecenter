package kr.co.mainticket.Encrypt;

import java.util.Date;

public class C2SSeedKey {
	private int KeyIndex = -1;
	
	private static String C2SKEY[] = {
			"8FA3AC2F4KBKB020",
			"C2C2CBH245414405",
			"082028284E424TVE",
			"C8D0D8QM0B23282B",
			"42424BH2C4D0D414",
			"C7F3F4378DA1AC2D",
			"C2D2D0HW0B03080B",
			"475354178EA2AC2E",
			"CFF3FC3F4D717C3D",
			"C1D1D0SEBH202020",
			"NE0304NECBD3D81B",
			"CYW1DC1D415150SE",
			"8QMQMBH10F030TVF",
			"869294164B737GTB",
			"C8TVC8RU8E929C1E",
			"8F939C1F4A52581A",
			"05SE1415CBF3FGTB",
			"Y46KB42Y4D616C2D",
			"4672743609SEQM19",
			"CAF2FGTAZNZNFGH1",
			"858QM405041ZN414",
			"87939417CCF0FC3C",
			"47737437Y4505414",
			"CDE1EC2D485058QM",
			"BH3XWXWRU5919415",
			"092HW82980A0AVR0",
			"0C1ZNC1C8CB0BC3C",
			"NE131417C4TVC404",
			"85A1A425C9F1FGT9",
			"43737XW347636427",
			"4D515C1D84909414",
			"C5F1F4358A82880A",
			"41616VRQMEB2BC3E",
			"C3E3EVR3CCE0EC2C",
			"C3F3FXW349414809",
			"C5D1D41584B0B434",
			"4E525C1E89A1A829",
			"ZN2HWVRQMC8RUTVC",
			"4E727C3ECAD2D81A",
			"4541Y405C1E1EVRC",
			"0BGG282B4662648D",
			"8DA1AC2D8RURUA7F",
			"8EA2AC2ERUFGH875",
			"BH2VRVR0C7D3D41D",
			"415150SE809090YW",
			"4B737GTB4C505C69",
			"4A52581AC2F2F0F9",
			"4D616C2DC6C2CY46",
			"09SEQM19CEF2FC67",
			"ZNZNFGH1TVF0F0AF",
			"041ZN414898QM858",
			"CCF0FC3TVEHW1C79",
			"Y450541482B2BNE7",
			"485058QM425250DE",
			"85919415456164H3",
			"80A0AVR0C2E2E092",
			"8CB0BC3CKB3GG4C1",
			"C4TVC404KBHW1471",
			"0D313C3D8E828CF3",
			"C9F1FGT90DZN0C5A",
			"0FGG2C2FC1F1FNE2",
			"476364278CA0AC37",
			"0C2VRC2C8AA2A8AC",
			"84909414RU10QMD5",
			"CDC1CTVD86828431",
			"8A82880A4A62685F",
			"04FGH40Y486KB822",
			"8EB2BC3EC6E2E416",
			"GTA3AVRGTBA3A8A9",
			"CCE0EC2C8D8QMC3E",
			"43636VR3XWGG2ZNA",
			"49414809487NEGTF",
			"05313435BH1ZN0F7",
			"84B0B434CAE2E85D",
			"TVE0EVRRUDB1BC21",
			"89A1A829465254E5",
			"C5E1E4254840480B",
			"8C8RUTVTVB13QMHW",
			"4F434TVFAZAZAZ52",
			"CAD2D81AC9C1C8E7",
			"0FMQ3C3F0D313C3D",
			"NEGG24270FGG2C2F",
			"CAC2C80A0C2VRC2C",
			"XW131ZN3CDC1CTVD",
			"VR222VR204FGH404",
			"8A92981AGTA3AVR3",
			"81A1AVR143636VR3",
			"4F737C3F05313435",
			"VRHW1ZN2TVE0EVR0",
			"80B0BXW0C5E1E425",
			"052HW4254F434TVF",
			"4KBKBVR0455154FA",
			"4E424TVEC6F2F482",
			"C4D0D41Y41414B24",
			"0BXWRU0BCEE2EC2D",
			"4D717C3DC1C1CBFF",
			"CBD3D81B8D919C70",
			"0FXW0TVF4743Y4QM",
			"8E929C1E8C909C8C",
			"CBF3FGTB4NENE051"
	};
	
	public C2SSeedKey(){
		this.KeyIndex = -1;
	}
	
	public boolean RandomKey(){
		boolean result = false;
		
		try{
			Date date = new Date();
			long time = date.getTime();
			int index = (int)(time % C2SKEY.length);
			this.KeyIndex = index;
			result = true;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return result;
	}
	
	public String getKey() throws Exception{
		if(this.KeyIndex == -1){
			throw new Exception("Must execute RandomKey();");
		}
		else if(this.KeyIndex < -1 || this.KeyIndex >= C2SKEY.length){
			throw new Exception("Warning! Invalid Key Index.");
		}
		
		return C2SKEY[this.KeyIndex];
	}
	
	public int getKeyIndex() throws Exception{
		if(this.KeyIndex == -1){
			throw new Exception("Must execute RandomKey();");
		}
		else if(this.KeyIndex < -1 || this.KeyIndex >= C2SKEY.length){
			throw new Exception("Warning! Invalid Key Index.");
		}
		
		return this.KeyIndex;
	}
	
	public static String getDecryptKey(int Key){
		return C2SKEY[Key];
	}
}