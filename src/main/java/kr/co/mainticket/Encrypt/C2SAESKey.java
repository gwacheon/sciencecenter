package kr.co.mainticket.Encrypt;

import java.util.Date;

public class C2SAESKey {
	private int KeyIndex = -1;
	
	private static String C2SKEY[] = {
			"D0D8A6HEDF84SES1",
			"8FA3AA2F4KBKB020",
			"C2C2CBH2E5414405",
			"082028B84E424TVE",
			"C8D0D8QMWB23282B",
			"42424BH2C4D5D414",
			"C7F3F437ODA1AC2D",
			"C2D2D0HW6B03080B",
			"47535417SEA2AC2E",
			"CFF3FC3F7D717C3D",
			"C1D1D0SEXH202020",
			"NE0304NE8BD3D81B",
			"CYW1DC1DM15150SE",
			"8QMQMBH19F030TVF",
			"86929416NB737GTB",
			"C8TVC8RU7E929C1E",
			"8F939C1FQA52581A",
			"05SE1415BBF3FGTB",
			"Y46KB42YAD616C2D",
			"4672743619SEQM19",
			"CAF2FGTAUNZNFGH1",
			"858QM405241ZN414",
			"87939417OCF0FC3C",
			"47737437K4505414",
			"CDE1EC2DO85058QM",
			"BH3XWXWRZ5919415",
			"092HW829O0A0AVR0",
			"0C1ZNC1CKCB0BC3C",
			"NE131417O4TVC404",
			"85A1A425W9F1FGT9",
			"43737XW327636427"
	};
	
	public C2SAESKey(){
		this.KeyIndex = -1;
	}
	
	public boolean RandomKey(){
		boolean result = false;
		
		try{
			Date date = new Date();
			long time = date.getTime();
			int index = (int)(time % C2SKEY.length);
			this.KeyIndex = index;
			result = true;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return result;
	}
	
	public String getKey() throws Exception{
		if(this.KeyIndex == -1){
			throw new Exception("Must execute RandomKey();");
		}
		else if(this.KeyIndex < -1 || this.KeyIndex >= C2SKEY.length){
			throw new Exception("Warning! Invalid Key Index.");
		}
		
		return C2SKEY[this.KeyIndex];
	}
	
	public int getKeyIndex() throws Exception{
		if(this.KeyIndex == -1){
			throw new Exception("Must execute RandomKey();");
		}
		else if(this.KeyIndex < -1 || this.KeyIndex >= C2SKEY.length){
			throw new Exception("Warning! Invalid Key Index.");
		}
		
		return this.KeyIndex;
	}
	
	public static String getDecryptKey(int Key){
		return C2SKEY[Key];
	}
}