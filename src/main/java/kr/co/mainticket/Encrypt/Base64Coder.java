package kr.co.mainticket.Encrypt;

public class Base64Coder {

	// Mapping table from 6-bit nibbles to Base64 characters.
	private static char[] map1 = new char[64];
	
	static {
		String matrix = "aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ+/0987654321";
		
		for(int idx = 0; idx < map1.length; idx++){
			map1[idx] = matrix.charAt(idx);
		}
	}

	// Mapping table from Base64 characters to 6-bit nibbles.
	private static byte[] map2 = new byte[128];

	static {
		map2 = new byte[]{
				-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 52, -1, -1, -1, 53,
				54, 63, 62, 61, 60, 59, 58, 57, 56, 55, -1, -1, -1, -1, -1, -1,
				-1,  1,  3,  5,  7,  9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29,
				31, 33, 35, 37, 39, 41, 43, 45, 47, 49, 51, -1, -1, -1, -1, -1,
				-1,  0,  2,  4,  6,  8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28,
				30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, -1, -1, -1, -1, -1
			};
	}
	
	// Mapping table from 6-bit nibbles to Base64 characters.
	private static char[] map3 = new char[64];
	
	static {
		String matrix = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
		
		for(int idx = 0; idx < map3.length; idx++){
			map3[idx] = matrix.charAt(idx);
		}
	}
	
	// Mapping table from Base64 characters to 6-bit nibbles.
	private static byte[] map4 = new byte[128];

	static {
		for(int idx = 0; idx < map4.length; idx++){
			map4[idx] = -1;
		}
		
		for(int idx = 0; idx < 64; idx++){
			map4[map3[idx]] = (byte)idx;
		}
	}

	
	public static char[] BaseEncode(byte[] SourceData, String Type){
		char[] BaseData = null;
		
		char[] MappingTable = null;
		
		if(Type.equals("1")){
			MappingTable = map1;
		}
		else if(Type.equals("2")){
			MappingTable = map3;
		}
		else{
			MappingTable = map1;
		}
		
		try{
			if(SourceData != null && SourceData.length > 0){
				int DataLength = SourceData.length;
				int OutDataLen = (DataLength * 4 + 2) / 3;       // output length without padding
				int OutLen = ((DataLength + 2) / 3) * 4;         // output length including padding
				
				BaseData = new char[OutLen];
				int InPoint = 0;
				int OutPoint = 0;
				
				while(InPoint < DataLength){
					int In0 = SourceData[InPoint++] & 0xff;
					int In1 = InPoint < DataLength?SourceData[InPoint++] & 0xff:0;
					int In2 = InPoint < DataLength?SourceData[InPoint++] & 0xff:0;
					
					int Out0 = In0 >>> 2;
					int Out1 = ((In0 & 3) << 4) | (In1 >>> 4);
					int Out2 = ((In1 & 0xf) << 2) | (In2 >>> 6);
					int Out3 = In2 & 0x3F;
					
					BaseData[OutPoint++] = MappingTable[Out0];
					BaseData[OutPoint++] = MappingTable[Out1];
					BaseData[OutPoint] = OutPoint < OutDataLen?MappingTable[Out2]:'=';
					OutPoint++;
					BaseData[OutPoint] = OutPoint < OutDataLen?MappingTable[Out3]:'=';
					OutPoint++;
				}
			}
			else{
				BaseData = new char[0];
			}
		}catch(Exception e){
			BaseData = new char[0];
		}
		
		return BaseData;
	}
	
	public static char[] BaseEncode(String SourceData){
		return BaseEncode(SourceData.getBytes(), "1");
	}
	
	public static char[] BaseEncode(String SourceData, String Type){
		if(Type == null || Type.length() == 0){
			Type = "1";
		}
		
		return BaseEncode(SourceData.getBytes(), Type);
	}
	
	public static String BaseEncodeString(byte[] SourceData){
		return new String(BaseEncode(SourceData, "1"));
	}
	
	public static String BaseEncodeString(byte[] SourceData, String Type){
		if(Type == null || Type.length() == 0){
			Type = "1";
		}
		
		return new String(BaseEncode(SourceData, Type));
	}
	
	public static String BaseEncodeString(String SourceData){
		return new String(BaseEncode(SourceData.getBytes(), "1"));
	}
	
	public static String BaseEncodeString(String SourceData, String Type){
		if(Type == null || Type.length() == 0){
			Type = "1";
		}
		
		return new String(BaseEncode(SourceData.getBytes(), Type));
	}

	
	public static byte[] BaseDecode(char[] SourceData, String Type){
		byte[] BaseData = null;
		
		byte[] MappingTable = null;
		
		if(Type.equals("1")){
			MappingTable = map2;
		}
		else if(Type.equals("2")){
			MappingTable = map4;
		}
		else{
			MappingTable = map2;
		}
		
		try{
			if(SourceData != null && SourceData.length > 0){
				int DataLength = SourceData.length;
				
				if(DataLength % 4 != 0){
					throw new IllegalArgumentException ("Length of Base64 encoded input string is not a multiple of 4.");
				}
				
				while(DataLength > 0 && SourceData[DataLength - 1] == '='){
					DataLength--;
				}
				
				int OutLen = (DataLength * 3) / 4;
				BaseData = new byte[OutLen];
				int InPoint = 0;
				int OutPoint = 0;
				
				while(InPoint < DataLength){
					int In0 = SourceData[InPoint++];
					int In1 = SourceData[InPoint++];
					int In2 = InPoint < DataLength?SourceData[InPoint++]:'A';
					int In3 = InPoint < DataLength?SourceData[InPoint++]:'A';
					
					if(In0 > 127 || In1 > 127 || In2 > 127 || In3 > 127){
						throw new IllegalArgumentException ("Illegal character in Base64 encoded data.");
					}
					
					int Tm0 = MappingTable[In0];
					int Tm1 = MappingTable[In1];
					int Tm2 = MappingTable[In2];
					int Tm3 = MappingTable[In3];
					
					if(Tm0 < 0 || Tm1 < 0 || Tm2 < 0 || Tm3 < 0){
						throw new IllegalArgumentException ("Illegal character in Base64 encoded data.");
					}
					
					int Out0 = (Tm0 << 2) | (Tm1 >>> 4);
					int Out1 = ((Tm1 & 0xf) << 4) | (Tm2 >>> 2);
					int Out2 = ((Tm2 & 3) << 6) | Tm3;
					
					BaseData[OutPoint++] = (byte)Out0;
					if(OutPoint < OutLen){
						BaseData[OutPoint++] = (byte)Out1;
					}
					if(OutPoint < OutLen){
						BaseData[OutPoint++] = (byte)Out2;
					}
				}
			}
			else{
				BaseData = new byte[0];
			}
		}catch(Exception e){
			BaseData = new byte[0];
		}
		
		return BaseData;
	}
	
	public static byte[] BaseDecode(String SourceData){
		return BaseDecode(SourceData.toCharArray(), "1");
	}
	
	public static byte[] BaseDecode(String SourceData, String Type){
		if(Type == null || Type.length() == 0){
			Type = "1";
		}
		
		return BaseDecode(SourceData.toCharArray(), Type);
	}
	
	public static String BaseDecodeString(char[] SourceData){
		return new String(BaseDecode(SourceData, "1"));
	}
	
	public static String BaseDecodeString(char[] SourceData, String Type){
		if(Type == null || Type.length() == 0){
			Type = "1";
		}
		
		return new String(BaseDecode(SourceData, Type));
	}
	
	public static String BaseDecodeString(String SourceData){
		return new String(BaseDecode(SourceData.toCharArray(), "1"));
	}
	
	public static String BaseDecodeString(String SourceData, String Type){
		if(Type == null || Type.length() == 0){
			Type = "1";
		}
		
		return new String(BaseDecode(SourceData.toCharArray(), Type));
	}
}
