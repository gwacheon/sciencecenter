package kr.co.mainticket.Encrypt;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;

/*******************************************************************************
* Made   : 2006. 7.18.
* Update : 2006.11.24 
* FILE   : seedx.java
*
* DESCRIPTION: Core routines for the enhanced SEED
* 
*******************************************************************************/


public class RSAKey{
	private PublicKey PubKey;
	private PrivateKey PriKey;
	
	public boolean GenerateKey(int KeySize){
		boolean result = false;
		
		try{
			KeyPairGenerator KeyGen = KeyPairGenerator.getInstance("RSA");
			
			KeyGen.initialize(KeySize);
			KeyPair kp = KeyGen.generateKeyPair();
			
			this.PubKey = kp.getPublic();
			this.PriKey = kp.getPrivate();
			
			result = true;
		}catch(Exception e){
			this.PubKey = null;
			this.PriKey = null;
			
			result = false;
		}
		
		return result;
	}
	
	public PublicKey getPublicKey(){
		return this.PubKey;
	}
	
	public PrivateKey getPrivateKey(){
		return this.PriKey;
	}
	
	public boolean SavePublicKey(String FilePath){
		boolean result = false;
		
		ObjectOutputStream out = null;
		
		try{
			out = new ObjectOutputStream(new FileOutputStream(FilePath));
			out.writeObject(this.PubKey);
		}catch(Exception e){
			result = false;
		}finally{
			if(out != null){
				try{
					out.close();
				}catch(Exception oe){
				}
			}
		}
		
		return result;
	}
	
	public boolean SavePrivateKey(String FilePath){
		boolean result = false;
		
		ObjectOutputStream out = null;
		
		try{
			out = new ObjectOutputStream(new FileOutputStream(FilePath));
			out.writeObject(this.PriKey);
		}catch(Exception e){
			result = false;
		}finally{
			if(out != null){
				try{
					out.close();
				}catch(Exception oe){
				}
			}
		}
		
		return result;
	}
	
	public PublicKey LoadPublicKey(String FilePath){
		PublicKey PubKey = null;
		ObjectInputStream in = null;
		
		try{
			in = new ObjectInputStream(new FileInputStream(FilePath));
			PubKey = (PublicKey)in.readObject();
		}catch(Exception e){
			PubKey = null;
		}finally{
			if(in != null){
				try{
					in.close();
				}catch(Exception oe){
				}
			}
		}
		
		return PubKey;
	}
	
	public PrivateKey LoadPrivateKey(String FilePath){
		PrivateKey PriKey = null;
		ObjectInputStream in = null;
		
		try{
			in = new ObjectInputStream(new FileInputStream(FilePath));
			PriKey = (PrivateKey)in.readObject();
		}catch(Exception e){
			PriKey = null;
		}finally{
			if(in != null){
				try{
					in.close();
				}catch(Exception oe){
				}
			}
		}
		
		return PriKey;
	}
}