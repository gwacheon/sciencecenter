package kr.co.mainticket.Encrypt;

import java.security.PrivateKey;
import java.security.PublicKey;

import javax.crypto.Cipher;

public class RSACoder{
	
	public static byte[] RSAEncrypt(byte[] SourceData, PublicKey PubKey){
		byte[] RSAData = null;
		
		try{
			if(SourceData != null && SourceData.length > 0){
				Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
				
				cipher.init(Cipher.ENCRYPT_MODE, PubKey);
				
				RSAData = cipher.doFinal(SourceData);
			}
			else{
				RSAData = new byte[0];
			}
		}catch(Exception e){
			RSAData = null;
		}
		
		return RSAData;
	}
	
	public static byte[] RSAEncrypt(String SourceData, PublicKey PubKey){
		return RSAEncrypt(SourceData.getBytes(), PubKey);
	}
	
	public static String RSAEncryptString(byte[] SourceData, PublicKey PubKey){
		//return new String(RSAEncrypt(SourceData, PubKey));
		return Base64Coder.BaseEncodeString(RSAEncrypt(SourceData, PubKey));
	}
	
	public static String RSAEncryptString(String SourceData, PublicKey PubKey){
		//return new String(RSAEncrypt(SourceData.getBytes(), PubKey));
		return Base64Coder.BaseEncodeString(RSAEncrypt(SourceData.getBytes(), PubKey));
	}
	
	
	
	public static byte[] RSADecrypt(byte[] SourceData, PrivateKey PriKey){
		byte[] RSAData = null;
		
		try{
			if(SourceData != null && SourceData.length > 0){
				Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
				
				cipher.init(Cipher.DECRYPT_MODE, PriKey);
				
				RSAData = cipher.doFinal(SourceData);
			}
			else{
				RSAData = new byte[0];
			}
		}catch(Exception e){
			RSAData = null;
			e.printStackTrace();
		}
		
		return RSAData;
	}
	
	public static byte[] RSADecrypt(String SourceData, PrivateKey PriKey){
		//return RSADecrypt(SourceData.getBytes(), PriKey);
		return RSADecrypt(Base64Coder.BaseDecode(SourceData), PriKey);
	}
	
	public static String RSADecryptString(byte[] SourceData, PrivateKey PriKey){
		return new String(RSADecrypt(SourceData, PriKey));
	}
	
	public static String RSADecryptString(String SourceData, PrivateKey PriKey){
		//return new String(RSADecrypt(SourceData.getBytes(), PriKey));
		return new String(RSADecrypt(Base64Coder.BaseDecode(SourceData), PriKey));
	}
}