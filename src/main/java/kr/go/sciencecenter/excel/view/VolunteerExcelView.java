package kr.go.sciencecenter.excel.view;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import kr.go.sciencecenter.model.Volunteer;

public class VolunteerExcelView extends AbstractExcelView{

	@Override
	protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook, 
			HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		String userAgent = request.getHeader("User-Agent");
		
		Date now = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmm");
		
		String fileName = "봉사활동대장리스트_" + format.format(now) + ".xls";
		fileName = URLEncoder.encode(fileName, "utf-8");
		
		response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\";");
		response.setHeader("Content-Transfer-Encoding", "binary");
		
		HSSFSheet excelSheet = workbook.createSheet("봉사활동대장리스트");
		setExcelHeader(excelSheet);
		setExcelRows(excelSheet, (List<Volunteer>) model.get("volunteers"));
	}

	private void setExcelHeader(HSSFSheet excelSheet) {
		HSSFRow excelHeader = excelSheet.createRow(0);
		int cellIndex = 0;
		excelHeader.createCell(cellIndex++).setCellValue("ID");
		excelHeader.createCell(cellIndex++).setCellValue("상태");
		excelHeader.createCell(cellIndex++).setCellValue("이름");
		excelHeader.createCell(cellIndex++).setCellValue("이메일");
		excelHeader.createCell(cellIndex++).setCellValue("학교");
		excelHeader.createCell(cellIndex++).setCellValue("반");
		excelHeader.createCell(cellIndex++).setCellValue("번호");
		excelHeader.createCell(cellIndex++).setCellValue("1365 ID");
		excelHeader.createCell(cellIndex++).setCellValue("지원서");
	}

	private void setExcelRows(HSSFSheet excelSheet, List<Volunteer> volunteers) {
		int index = 1;
		for (Volunteer volunteer : volunteers) {
			int cellIndex = 0;
			HSSFRow excelRow = excelSheet.createRow(index++);
			excelRow.createCell(cellIndex++).setCellValue(volunteer.getId());
			
			if(volunteer.getStatus().equals("pending")){
				excelRow.createCell(cellIndex++).setCellValue("접수대기");
			}else if(volunteer.getStatus().equals("accept")){
				excelRow.createCell(cellIndex++).setCellValue("접수완료");
			}else if(volunteer.getStatus().equals("reject")){
				excelRow.createCell(cellIndex++).setCellValue("접수거절");
			}else if(volunteer.getStatus().equals("failed")){
				excelRow.createCell(cellIndex++).setCellValue("불참");
			}else if(volunteer.getStatus().equals("completion")){
				excelRow.createCell(cellIndex++).setCellValue("봉사완료");
			}
			
			excelRow.createCell(cellIndex++).setCellValue(volunteer.getName());
			excelRow.createCell(cellIndex++).setCellValue(volunteer.getEmail());
			excelRow.createCell(cellIndex++).setCellValue(volunteer.getSchool());
			excelRow.createCell(cellIndex++).setCellValue(volunteer.getGroupNo());
			excelRow.createCell(cellIndex++).setCellValue(volunteer.getMemberNo());
			excelRow.createCell(cellIndex++).setCellValue(volunteer.getOtherId());
			excelRow.createCell(cellIndex++).setCellValue(volunteer.getIntroduction());
		}
	}
	
	

}
