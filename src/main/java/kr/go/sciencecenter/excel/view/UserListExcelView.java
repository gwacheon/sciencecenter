package kr.go.sciencecenter.excel.view;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import kr.go.sciencecenter.model.User;

public class UserListExcelView extends AbstractExcelView {

	@Override
	protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HSSFSheet excelSheet = workbook.createSheet("회원 리스트");
		setExcelHeader(excelSheet);
		setExcelRows(excelSheet, (List<User>) model.get("userList"));

	}

	private void setExcelHeader(HSSFSheet excelSheet) {
		HSSFRow excelHeader = excelSheet.createRow(0);
		int cellIndex = 0;
		excelHeader.createCell(cellIndex++).setCellValue("ID");
		excelHeader.createCell(cellIndex++).setCellValue("이름");
		excelHeader.createCell(cellIndex++).setCellValue("이메일");
		excelHeader.createCell(cellIndex++).setCellValue("전화번호");
		excelHeader.createCell(cellIndex++).setCellValue("주소");
		excelHeader.createCell(cellIndex++).setCellValue("회원구분");
	}

	private void setExcelRows(HSSFSheet excelSheet, List<User> userList) {
		int index = 1;
		for (User user : userList) {
			int cellIndex = 0;
			HSSFRow excelRow = excelSheet.createRow(index++);
			excelRow.createCell(cellIndex++).setCellValue(user.getId());
			excelRow.createCell(cellIndex++).setCellValue(user.getName());
			excelRow.createCell(cellIndex++).setCellValue(user.getEmail());
			excelRow.createCell(cellIndex++).setCellValue(user.getPhone());
			excelRow.createCell(cellIndex++).setCellValue(user.getAddress1() + " " + user.getAddress2());
			excelRow.createCell(cellIndex++).setCellValue(user.getChargedMember());
		}

	}
}
