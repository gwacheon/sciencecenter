package kr.go.sciencecenter.excel.view;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import kr.go.sciencecenter.model.Article;
import kr.go.sciencecenter.model.Reply;
import kr.go.sciencecenter.model.ReplyDetail;
import kr.go.sciencecenter.model.Survey;

public class SurveyRepliesExcelView extends AbstractExcelView {

	@Override
	protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		String userAgent = request.getHeader("User-Agent");
		
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		
		Survey survey = (Survey) model.get("survey");
		Map<Integer, Article> articlesMap = new HashMap<Integer, Article>();
		for( Article a : survey.getArticles()) {
			articlesMap.put(a.getId(), a);
		}
		
		String fileName = survey.getTitle() + "답변리스트_" + format.format(survey.getBeginTime()) +"_" + format.format(survey.getEndTime()) + ".xls";
		fileName = URLEncoder.encode(fileName, "utf-8");
		
		response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\";");
		response.setHeader("Content-Transfer-Encoding", "binary");
		
		HSSFSheet excelSheet = workbook.createSheet("봉사활동대장리스트");
		HSSFCellStyle cellStyle = workbook.createCellStyle();
		setExcelHeader(workbook, excelSheet, cellStyle);
		setExcelRows(workbook, excelSheet, articlesMap, (List<Reply>) model.get("replies"));
		SetHeaderStyle(workbook, excelSheet.getRow(0));
	}
	
	private static void SetHeaderStyle(HSSFWorkbook workbook, HSSFRow row){
	    CellStyle style = workbook.createCellStyle();//Create style
	    HSSFFont font = workbook.createFont();//Create font
	    font.setFontName("Calibri");
	    font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);//Make font bold
	    font.setFontHeightInPoints((short)14);
	    style.setAlignment(CellStyle.ALIGN_CENTER);
	    style.setFont(font);//set it to bold
	    

	    for(int i = 0; i < row.getLastCellNum(); i++){//For each cell in the row 
	        row.getCell(i).setCellStyle(style);//Set the sty;e
	    }
	}
	
	
	private void setExcelHeader(HSSFWorkbook workbook, HSSFSheet excelSheet, HSSFCellStyle cellStyle) {
		HSSFRow excelHeader = excelSheet.createRow(0);
		int cellIndex = 0;
				
		excelHeader.createCell(cellIndex++).setCellValue("이름");
		excelHeader.createCell(cellIndex++).setCellValue("연락처");
		excelHeader.createCell(cellIndex++).setCellValue("이메일");
		excelHeader.createCell(cellIndex++).setCellValue("질문");
		excelHeader.createCell(cellIndex++).setCellValue("질문유형");
		excelHeader.createCell(cellIndex++).setCellValue("답변");

	}

	private void setExcelRows(HSSFWorkbook workbook, HSSFSheet excelSheet, Map<Integer,Article> articlesMap, List<Reply> replies) {
		int index = 1;
		for (Reply reply : replies) {
			for( ReplyDetail detail : reply.getDetails() ) {
			
				int cellIndex = 0;
				HSSFRow excelRow = excelSheet.createRow(index++);
				
				// 답변자 정보
				excelRow.createCell(cellIndex++).setCellValue(reply.getName());
				excelSheet.autoSizeColumn(cellIndex, false);
				excelRow.createCell(cellIndex++).setCellValue(reply.getPhone());
				excelSheet.autoSizeColumn(cellIndex, false);
				excelRow.createCell(cellIndex++).setCellValue(reply.getEmail());
				excelSheet.autoSizeColumn(cellIndex, false);
				// 항목 질문 정보
				Article article = articlesMap.get(detail.getArticleId());
				excelRow.createCell(cellIndex++).setCellValue(article.getQuestion());
				excelSheet.autoSizeColumn(cellIndex, false);
				if( article.getqType().equals("text")) {
					excelRow.createCell(cellIndex++).setCellValue("서술형");
					excelSheet.autoSizeColumn(cellIndex, false);
				}
				else if( article.getqType().equals("select")) {
					excelRow.createCell(cellIndex++).setCellValue("단일선택");
					excelSheet.autoSizeColumn(cellIndex, false);
				}
				else if( article.getqType().equals("multiple_select")) {
					excelRow.createCell(cellIndex++).setCellValue("중복선택");
					excelSheet.autoSizeColumn(cellIndex, false);
				}
				// 답변
				excelRow.createCell(cellIndex++).setCellValue(detail.getReply());
				excelSheet.autoSizeColumn(cellIndex, false);
				
				// set font style
				CellStyle style = workbook.createCellStyle();
			    HSSFFont font = workbook.createFont();
			    font.setFontName("Calibri");
			    font.setFontHeightInPoints((short)12);
			    style.setFont(font);
			    for(int i = 0; i < excelRow.getLastCellNum(); i++){ 
			    	excelRow.getCell(i).setCellStyle(style);
			    }
			}
			
			
		}
	}
}
