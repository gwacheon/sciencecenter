package kr.go.sciencecenter.mailer;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.mail.internet.MimeMessage;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;
import org.springframework.ui.velocity.VelocityEngineUtils;

import kr.go.sciencecenter.model.ContactUsEmail;
import kr.go.sciencecenter.model.User;

//@Component("emailSender")
public class UserMailer {
//	@Autowired
//	private JavaMailSender mailSender;
//
//	@Autowired
//	private VelocityEngine velocityEngine;
//	
//	@Autowired
//	private Environment env;
//
//	public void sendConfirmationEmail(final User user, final String serverUrl){
//		MimeMessagePreparator preparator = new MimeMessagePreparator() {
//			@Override
//			public void prepare(MimeMessage mimeMessage) throws Exception {
//				String encodingOptions = "text/html; charset=UTF-8";
//				mimeMessage.setHeader("Content-Type", encodingOptions);
//
//				mimeMessage.setSubject("[국립과천과학관] 회원가입 인증 메일 입니다.", "UTF-8");
//				MimeMessageHelper message = new MimeMessageHelper(mimeMessage, "UTF-8");
//				message.setTo(user.getEmail());
//				message.setFrom("jhlim8182@gmail.com");
//
//
//				Map<String, Object> model = new HashMap<String, Object>();
//				model.put("serverUrl", serverUrl);
//				model.put("user", user);
//
//				String text = VelocityEngineUtils.mergeTemplateIntoString(
//						velocityEngine,
//						"/kr/go/sciencecenter/mailer/template/confirmationMail.vm", 
//						"UTF-8", model);
//				message.setText(text,true);
//
//			}
//		};
//
//		this.mailSender.send(preparator);
//	}
//
//	public void sendEmailToAdmin(final ContactUsEmail contactUsEmail) {
//		MimeMessagePreparator preparator = new MimeMessagePreparator() {
//			@Override
//			public void prepare(MimeMessage mimeMessage) throws Exception {
//				String encodingOptions = "text/html; charset=UTF-8";
//				mimeMessage.setHeader("Content-Type", encodingOptions);
//				mimeMessage.setSubject("[국립과천과학관] 사용자로부터 운영자에게 온 문의 메일입니다.", "UTF-8");
//				MimeMessageHelper message = new MimeMessageHelper(mimeMessage, "UTF-8");
//				
//				message.setTo("laylow861@gmail.com");
//				message.setFrom(env.getProperty("email.username"));
//
//				// set date
//				Date todayDate = new Date();
//				message.setSentDate(todayDate);
//				
//				Map<String, Object> model = new HashMap<String, Object>();
//				model.put("mail", contactUsEmail);
//
//				String text = VelocityEngineUtils.mergeTemplateIntoString(
//						velocityEngine,
//						"/kr/go/sciencecenter/mailer/template/contactUsEmail.vm", 
//						"UTF-8", model);
//				message.setText(text,true);
//
//			}
//		};
//
//		this.mailSender.send(preparator);
//
//	}
}
