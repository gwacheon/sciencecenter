package kr.go.sciencecenter.api;

import kr.go.sciencecenter.model.api.MemberShip;

public class MembershipApi extends ApiHelper{
	private final static String SPONSOR = "MEMBERSHIP_SPONSOR";
	
	public MembershipApi() {
		
	}
	
	public MemberShip requestSponsor(MemberShip membership) {
		super.setXStream(MemberShip.class);
		super.setResXStream(MemberShip.class);
		
		return (MemberShip) this.callApi(SPONSOR, MemberShip.class, MemberShip.class, membership);
	}

}
