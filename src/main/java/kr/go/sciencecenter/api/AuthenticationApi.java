package kr.go.sciencecenter.api;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;

import kr.go.sciencecenter.model.api.Family;
import kr.go.sciencecenter.model.api.Member;
import kr.go.sciencecenter.util.PropertyHelper;
import kr.go.sciencecenter.util.Utility;

public class AuthenticationApi {
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public String C2_HOMEPAGE_URL = Utility.getConfigProperty("c2_homepage_url");
	public String C2_API_URL = Utility.getConfigProperty("c2_admin_url");
	public final static String C2_USER_SUFFIX = "/Interlock";
	
	public final static String API_TYPE_PICTURE = "PICTURE";
	public final static String API_TYPE_UPDATE = "UPDATE";
	public final static String API_TYPE_WITHDRAW = "DELETE_ID";
	public final static String API_TYPE_MEMBER_INFO = "MEMBER_INFO";
	public final static String API_TYPE_CHANGE_PW = "CHANGE_PW";
	public final static String API_TYPE_FIND_PW = "FIND_PW";
	public final static String API_TYPE_FIND_ID = "FIND_ID";
	public final static String API_TYPE_LOGIN = "LOGIN";
	public final static String API_TYPE_REGIST = "REGIST";
	public final static String API_TYPE_CHECK_ID = "CHECK_ID";
	public final static String API_TYPE_DELETE_ID = "DELETE_ID";
	public final static String API_TYPE_FAMILY = "FAMILY_LIST";
	public final static String API_TYPE_FAMILY_REGIST = "FAMILY_REGIST";
	public final static String API_TYPE_FAMILY_UPDATE = "FAMILY_UPDATE";
	public final static String API_TYPE_FAMILY_DELETE = "FAMILY_DELETE";
	
	public static AuthenticationApi apiInstance;
	
	public final static Map<String, String> ERR_CODE_MAP;
	static {
		ERR_CODE_MAP = new HashMap<String, String>();
		ERR_CODE_MAP.put("0000", "SUCCESS");
		ERR_CODE_MAP.put("LN03", "NOT_EXIST_MEMBER");
		ERR_CODE_MAP.put("LN05", "NOT_MATCH_PASSWORD");
		ERR_CODE_MAP.put("SI05", "EXIST_MANY_MEMBER");
		ERR_CODE_MAP.put("SI04", "NOT_EXIST_MEMBER");
		ERR_CODE_MAP.put("SP06", "NOT_EXIST_MEMBER");
		ERR_CODE_MAP.put("9999", "Server Error");
	}
	
	public static AuthenticationApi getInstance () {
		if(apiInstance == null) {
			apiInstance = new AuthenticationApi();
		}
		
		return apiInstance;
	}
	
	public void deleteFamily(Family family) {
		callFamilyApi(API_TYPE_FAMILY_DELETE, family);
	}
	
	public Family updateFamily(Family family) {
		return callFamilyApi(API_TYPE_FAMILY_UPDATE, family);
	}
	
	public Family registFamily(Family family) {
		return callFamilyApi(API_TYPE_FAMILY_REGIST, family);
	}
	
	public Member getFamilyList(Member member) {
		return callApi(API_TYPE_FAMILY, member);
	}
	
	public Member update(Member member) {
		return callApi(API_TYPE_UPDATE, member);
	}
	
	public Member withdraw(Member member) {
		return callApi(API_TYPE_WITHDRAW, member);
	}
	
	public Member getMemberInfo(Member member) {
		return callApi(API_TYPE_MEMBER_INFO, member);
	}
	
	public Member changePassword(Member member) {
		return callApi(API_TYPE_CHANGE_PW, member);
	}
	
	public Member findPassword(Member member) {
		return callApi(API_TYPE_FIND_PW, member);
	}
	
	public Member updatePicture(Member member) {
		return callApi(API_TYPE_PICTURE, member);
	}
	
	public Member findId(Member member) {
		return callApi(API_TYPE_FIND_ID, member);
	}
	
	public Member login(Member member) {
		return callApi(API_TYPE_LOGIN, member);
	}
	
	public Member delete(Member member){
		return callApi(API_TYPE_DELETE_ID, member);
	}
	
	public Member regist(Member member) {
		member.setRegNoDate("11111111");
		return callApi(API_TYPE_REGIST, member);
	}
	
	public Member checkId(String memberId) {
		Member member = new Member();
		member.setMemberId(memberId);
		return callApi(API_TYPE_CHECK_ID, member);
	}
	
	private Family callFamilyApi(String apiType, Family family) {
		String url = C2_API_URL + C2_USER_SUFFIX;
		XStream xStream = getXStream("family");
		String requestBody = xStream.toXML(family);
		
		try {
			HttpResponse<String> response = Unirest.post(url)
					.headers(getHeader(apiType))
					.body(requestBody)
					.asString();
		
			XStream xs = getXStream("member");
			Family resFamily = (Family) xs.fromXML(response.getBody());
			family.mergeMember(resFamily);
		} catch (UnirestException e) {
			e.printStackTrace();
			family.setRspCd("9999");
			family.setRspMsg("REST API Call Exception");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			family.setRspCd("9999");
			family.setRspMsg("서버 응답실패");
		}
		
		return family;
	}
	
	private Member callApi(String apiType, Member member) {
		String url = C2_API_URL + C2_USER_SUFFIX;
		XStream xStream = getXStream("member");
		String requestBody = xStream.toXML(member);
		
		try {
			HttpResponse<String> response = Unirest.post(url)
					.headers(getHeader(apiType))
					.body(requestBody)
					.asString();
		
			logger.info("Request Body -----------------------------------");
			logger.info(requestBody);
			logger.info("Response -----------------------------------");
			logger.info(response.getBody());
			
			XStream xs = getXStream("member");
			Member resMember = (Member) xs.fromXML(response.getBody());
			member.mergeMember(resMember);
		} catch (UnirestException e) {
			e.printStackTrace();
			member.setRspCd("9999");
			member.setRspMsg("REST API Call Exception");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			member.setRspCd("9999");
			member.setRspMsg("서버 응답실패");
		}
		
		return member;
	}
	
	public XStream getXStream(String type) {
		XmlFriendlyNameCoder replacer = new XmlFriendlyNameCoder("ddd", "_");
		XStream xStream = new XStream(new DomDriver("UTF-8", replacer));
		if(type.equals("member")){
			xStream.processAnnotations(Member.class);
		}else {
			xStream.processAnnotations(Family.class);
		}
		
		xStream.ignoreUnknownElements();
		
		return xStream;
	}	
	
	public Map<String, String> getHeader(String type){
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("INTERLOCK_TYPE", type);
		headerMap.put("INTERLOCK_CD", "GNSM");
		headerMap.put("KEY_IDX", "-1");
		headerMap.put("ENCODING", "EUC-KR");
		headerMap.put("Content-type", "text/xml");
		
		return headerMap;
	}
	
	public Member getInfo(String memberNo) {
		Member member = new Member(memberNo);
		return AuthenticationApi.getInstance().getMemberInfo(member);
	}
}
