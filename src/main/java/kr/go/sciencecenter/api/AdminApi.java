package kr.go.sciencecenter.api;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;

import kr.go.sciencecenter.model.api.Admin;
import kr.go.sciencecenter.util.PropertyHelper;
import kr.go.sciencecenter.util.Utility;

public class AdminApi {
	
	public String C2_API_URL = Utility.getConfigProperty("c2_admin_url");
	public final static String C2_USER_SUFFIX = "/Interlock";
	
	public final static String API_TYPE_LOGIN = "ADMIN_LOGIN";
	public final static String API_TYPE_LOGOUT = "ADMIN_LOGOUT";
	public final static String API_TYPE_ACCESS = "ADMIN_ACCESS";
	
	public static AdminApi adminApi;
	public static AdminApi getInstance () {
		if(adminApi == null) {
			adminApi = new AdminApi();
		}
		
		return adminApi;
	}
	
	public void access(String taManagerNo) {
		Admin admin = new Admin();
		admin.setTaManagerNo(taManagerNo);
		callApi(API_TYPE_ACCESS, admin);
	}
	
	public void logout(Admin admin) {
		callApi(API_TYPE_LOGOUT, admin);
	}
	
	public void login(Admin admin) {
		callApi(API_TYPE_LOGIN, admin);
	}
	
	private void callApi(String apiType, Admin admin) {
		String url = C2_API_URL + C2_USER_SUFFIX;
		XStream xStream = getXStream();
		String requestBody = xStream.toXML(admin);
		
		try {
			HttpResponse<String> response = Unirest.post(url)
					.headers(getHeader(apiType))
					.body(requestBody)
					.asString();
			
			XStream xs = getXStream();
			Admin resAdmin = (Admin) xs.fromXML(response.getBody());
			admin.mergeMember(resAdmin);
			
		} catch (UnirestException e) {
			e.printStackTrace();
			admin.setRspCd("9999");
			admin.setRspMsg("REST API Call Exception");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			admin.setRspCd("9999");
			admin.setRspMsg("서버 응답실패");
		}
	}
	
	private XStream getXStream() {
		XmlFriendlyNameCoder replacer = new XmlFriendlyNameCoder("ddd", "_");
		XStream xStream = new XStream(new DomDriver("UTF-8", replacer));
		xStream.processAnnotations(Admin.class);
		xStream.ignoreUnknownElements();
		
		return xStream;
	}	
	
	private Map<String, String> getHeader(String type){
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("INTERLOCK_TYPE", type);
		headerMap.put("INTERLOCK_CD", "GNSM");
		headerMap.put("KEY_IDX", "-1");
		headerMap.put("ENCODING", "EUC-KR");
		headerMap.put("Content-type", "text/xml");
		
		return headerMap;
	}
}
