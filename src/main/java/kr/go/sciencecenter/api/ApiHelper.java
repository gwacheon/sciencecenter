package kr.go.sciencecenter.api;

import java.util.HashMap;
import java.util.Map;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;

import kr.go.sciencecenter.util.Utility;

public class ApiHelper {
	
	public String C2_API_URL = Utility.getConfigProperty("c2_admin_url");
	public final static String C2_USER_SUFFIX = "/Interlock";
	
	private XStream xStream;
	private XmlFriendlyNameCoder replacer;
	
	private XStream resXStream;
	
	public ApiHelper() {
		
	}
			
	protected String getApiUrl() {
		return C2_API_URL + C2_USER_SUFFIX;
	}
	
	protected void setXStream(final Class<?> type) {
		this.replacer = new XmlFriendlyNameCoder("ddd", "_");
		this.xStream = new XStream(new DomDriver("UTF-8", replacer));
		this.xStream.processAnnotations(type);
		this.xStream.ignoreUnknownElements();
	}
	
	protected void setResXStream(final Class<?> type) {
		if (this.replacer == null) {
			this.replacer = new XmlFriendlyNameCoder("ddd", "_");
		}
		
		this.resXStream = new XStream(new DomDriver("UTF-8", replacer));
		this.resXStream.registerConverter((Converter) new IntConverter());
		this.resXStream.processAnnotations(type);
		this.resXStream.ignoreUnknownElements();
	}
	
	protected Object callApi(String apiType, Class<?> type, Class<?> resType, Object obj) {
		Object retObj = null;
		
		if (this.xStream == null) {
			this.setXStream(type);
		}
		
		if (this.resXStream == null) {
			this.setResXStream(resType);
		}
		
		String requestBody = this.xStream.toXML(obj);
		System.out.println("Lecture Request : " + requestBody);
		try {
			HttpResponse<String> response = Unirest.post(getApiUrl())
					.headers(getHeader(apiType))
					.body(requestBody)
					.asString();
			
			System.out.println("INFO : " + response.getBody());
			retObj = resXStream.fromXML(response.getBody());
		}catch (UnirestException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return retObj;
	}
	
	protected Map<String, String> getHeader(String type){
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("INTERLOCK_TYPE", type);
		headerMap.put("INTERLOCK_CD", "GNSM");
		headerMap.put("KEY_IDX", "-1");
		headerMap.put("ENCODING", "EUC-KR");
		headerMap.put("Content-type", "text/xml");
		
		return headerMap;
	}
}
