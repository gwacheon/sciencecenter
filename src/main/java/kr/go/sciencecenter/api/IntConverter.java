package kr.go.sciencecenter.api;

import org.apache.commons.lang.StringUtils;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class IntConverter implements Converter {
	@Override
	public boolean canConvert(Class clazz) {
		return clazz.equals(Integer.class);
	}

	@Override
	public void marshal(Object Obj, HierarchicalStreamWriter arg1, MarshallingContext arg2) {

	}

	@Override
	public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
		String value = reader.getValue();
		if (StringUtils.isEmpty(value)) {
			return 0;
		}
		return Integer.valueOf(value);
	}
}