package kr.go.sciencecenter.api;

import kr.go.sciencecenter.model.api.Lecture;
import kr.go.sciencecenter.model.api.LectureDetail;
import kr.go.sciencecenter.model.api.ReqLecture;

public class LectureApi extends ApiHelper{
	private final static String API_TYPE_LECTURE_LIST = "LECTURE_LIST";
	private final static String API_TYPE_LECTURE_DETAIL = "LECTURE_DETAIL";
	
	public LectureApi() {
		
	}
	
	public Lecture getList(ReqLecture reqLecture) {
		super.setXStream(ReqLecture.class);
		super.setResXStream(Lecture.class);
		return (Lecture) this.callApi(API_TYPE_LECTURE_LIST, ReqLecture.class, Lecture.class, reqLecture);
	}
	
	public LectureDetail getDetail(ReqLecture reqLecture) {
		super.setXStream(ReqLecture.class);
		super.setResXStream(LectureDetail.class);
		return (LectureDetail) this.callApi(API_TYPE_LECTURE_DETAIL, ReqLecture.class, LectureDetail.class, reqLecture);
	}
}
