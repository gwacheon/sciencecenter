package kr.go.sciencecenter.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;


public class Utility{

	private Utility(){
	}
	
	
	/**
	 * String check NULL
	 * @param str
	 * @return
	 */
	public static String CheckNull(String str){
		if(str != null){
			return str;
		}
		else{
			return "";
		}
	}
	
	
	/**
	 * 占쏙옙占썰리 占쏙옙占쏙옙 체크
	 * @param path
	 * @return
	 */
	public static boolean CheckDirectory(String path){
		boolean result = false;
		
		try{
			// 占쏙옙占썰리占쏙옙 占쏙옙占쏙옙占쏙옙 占쏙옙占쏙옙占�
			File DPath = new File(path);
			
			if(DPath.isDirectory() == false){
				if(DPath.mkdirs() == false){ 
  					throw new SecurityException(); 
  				}
			}
			
			result = true;
		}catch(Exception e){
		}
		
		return result;
	}
	
	
	/**
	 * 占쏙옙占쏙옙 占쏙옙占쏙옙 占쏙옙占쏙옙 체크
	 * @param FilePath
	 * @return
	 */
	public static boolean CheckFile(String FilePath){
		boolean result = false;
		
		try{
			File file = new File(FilePath);
			
			result = file.exists();
		}catch(Exception e){
			result = false;
		}
		
		return result;
	}


	/**
	 * 8859_1占쏙옙 KSC5601占쏙옙 占쏙옙환
	 * @param OrgStr
	 * @return
	 */
	public static String toKorean(String OrgStr){
		String ConvStr = "";
		try{
			if(OrgStr == null){
				return "";
			}

			// 占쏙옙占썹문占쌘울옙占쏙옙 8859_1占쏙옙占쏙옙占쏙옙占�占싻어내占쏙옙 KSC5601占쏙옙占쏙옙占쏙옙占�占쏙옙환
			ConvStr = new String(OrgStr.getBytes("8859_1"), "KSC5601");
		}catch(UnsupportedEncodingException e){
		}
		
		return ConvStr;
	}
	
	/**
	 * EncSet占쏙옙 KSC5601占쏙옙 占쏙옙환
	 * @param OrgStr
	 * @param EncSet
	 * @return
	 */
	public static String toKorean(String OrgStr, String EncSet){
		String ConvStr = "";
		try{
			if(OrgStr == null){
				return "";
			}

			// 占쏙옙占썹문占쌘울옙占쏙옙 8859_1占쏙옙占쏙옙占쏙옙占�占싻어내占쏙옙 EncSet占쏙옙占쏙옙占쏙옙占�占쏙옙환
			ConvStr = new String(OrgStr.getBytes(EncSet), "KSC5601");
		}catch(UnsupportedEncodingException e){
		}
		
		return ConvStr;
	}

	
	/**
	 * KSC5601占쏙옙 8859_1占쏙옙 占쏙옙환
	 * @param OrgStr
	 * @return
	 */
	public static String fromKorean(String OrgStr){
		String ConvStr = "";
		try{
			if(OrgStr == null){
				return "";
			}

			// 占쏙옙占썹문占쌘울옙占쏙옙 KSC5601占쏙옙占쏙옙占쏙옙占�占싻어내占쏙옙 8859_1占쏙옙占쏙옙占쏙옙占�占쏙옙환
			ConvStr = new String(OrgStr.getBytes("KSC5601"), "8859_1");
		}catch(UnsupportedEncodingException e){
		}
		
		return ConvStr;
	}
	
	/**
	 * KSC5601占쏙옙 EncSet占쏙옙 占쏙옙환
	 * @param OrgStr
	 * @return
	 */
	public static String fromKorean(String OrgStr, String EncSet){
		String ConvStr = "";
		try{
			if(OrgStr == null){
				return "";
			}

			// 占쏙옙占썹문占쌘울옙占쏙옙 KSC5601占쏙옙占쏙옙占쏙옙占�占싻어내占쏙옙 EncSet占쏙옙占쏙옙占쏙옙占�占쏙옙환
			ConvStr = new String(OrgStr.getBytes("KSC5601"), EncSet);
		}catch(UnsupportedEncodingException e){
		}
		
		return ConvStr;
	}

	
	/**
	 * @return formatted string representation of current day with "yyyyMMdd".
	 */
	public static String getShortDateString(){
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyyMMdd", java.util.Locale.KOREA);
		return formatter.format(new java.util.Date());
	}
	
	
	/**
	 * @return formatted string representation of current time with "yyyy-MM-dd-HH:mm:ss".
	 */
	public static String getTimeStampString() {
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS", java.util.Locale.KOREA);
		return formatter.format(new java.util.Date());
	}
	
	
	/**
	 * @param format
	 * @param days
	 * @return format string representation of the date format. For example, "yyyy-MM-dd".
	 */
	public static String getFormatDateString(String format, int days){
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(format, java.util.Locale.KOREA);
		java.util.Date date = new java.util.Date();
		date.setTime(date.getTime() + ((long)days * 1000 * 60 * 60 * 24));
		
		return formatter.format(date);
	}
	
	
	/**
	 * @param format
	 * @param days
	 * @return format string representation of the date format. For example, "yyyy-MM-dd".
	 */
	public static java.util.Date getFormatDate(String format, String DateStr){
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(format, java.util.Locale.KOREA);
		java.util.Date date = null;
		try{
			date = formatter.parse(DateStr);
		}catch(Exception e){
			date = new java.util.Date();
		}
		
		return date;
	}
	
	
	/**
	 * return times between two date strings with default defined format.
	 * @param format
	 * @param from
	 * @param to
	 * @return
	 */
	public static long timesBetween(String format, String from, String to){
		java.util.Date d1 = getFormatDate(format, from);
		java.util.Date d2 = getFormatDate(format, to);

		long duration = d2.getTime() - d1.getTime();

		return duration;
	}

	
	/**
	 * 占쌉력뱄옙占쏙옙 String占쏙옙 占쏙옙占싹댐옙 占쏙옙占싱몌옙큼 占쏙옙占싹댐옙 占쏙옙占쌘뤄옙 占쏙옙占쏙옙占쏙옙占쏙옙 채占쏙옙占쌍댐옙 占쌉쇽옙
	 * @param SourceStr
	 * @param StrLen
	 * @param PadChar
	 * @return
	 */
	public static String RPAD(String SourceStr, int StrLen, char PadChar){
		String PaddingStr = SourceStr;
		int PadCnt = 0;
		
		if(SourceStr == null || SourceStr.length() == 0){
			PaddingStr = "";
			PadCnt = StrLen;
		}
		else{
			char[] TempStr = SourceStr.toCharArray();
			
			int StrByteSize = 0;
			int CharSize = 0;
			
			for(int idx = 0; idx < TempStr.length; idx++){
				if(TempStr[idx] < 256){
					CharSize = 1;
				}
				else{
					CharSize = 2;
				}
				
				if(StrByteSize + CharSize > StrLen){
					PaddingStr = SourceStr.substring(0, idx);
					break;
				}
				else{
					StrByteSize += CharSize;
				}
			}
			
			PadCnt = StrLen - StrByteSize;
		}
		
		for(int idx = 0; idx < PadCnt; idx++){
			PaddingStr = PaddingStr + PadChar;
		}

		return PaddingStr;
	}
	
	public static String RPAD(int SourceStr, int StrLen, char PadChar){
		String result = RPAD(Integer.toString(SourceStr), StrLen, PadChar);
		
		return result;
	}
	
	public static String RPAD(long SourceStr, int StrLen, char PadChar){
		String result = RPAD(Long.toString(SourceStr), StrLen, PadChar);
		
		return result;
	}

	
	/**
	 * 占쌉력뱄옙占쏙옙 String占쏙옙 占쏙옙占싹댐옙 占쏙옙占싱몌옙큼 占쏙옙占싹댐옙 占쏙옙占쌘뤄옙 占쏙옙占쏙옙占쏙옙 채占쏙옙占쌍댐옙 占쌉쇽옙
	 * @param SourceStr
	 * @param StrLen
	 * @param PadChar
	 * @return
	 */
	public static String LPAD(String SourceStr, int StrLen, char PadChar){
		String PaddingStr = SourceStr;
		int PadCnt = 0;
		
		if(SourceStr == null || SourceStr.length() == 0){
			PaddingStr = "";
			PadCnt = StrLen;
		}
		else{
			char[] TempStr = SourceStr.toCharArray();
			
			int StrByteSize = 0;
			int CharSize = 0;
			
			for(int idx = 0; idx < TempStr.length; idx++){
				if(TempStr[idx] < 256){
					CharSize = 1;
				}
				else{
					CharSize = 2;
				}
				
				if(StrByteSize + CharSize > StrLen){
					PaddingStr = SourceStr.substring(0, idx);
					break;
				}
				else{
					StrByteSize += CharSize;
				}
			}
			
			PadCnt = StrLen - StrByteSize;
		}
		
		for(int idx = 0; idx < PadCnt; idx++){
			PaddingStr = PadChar + PaddingStr;
		}

		return PaddingStr;
	}
	
	public static String LPAD(int SourceStr, int StrLen, char PadChar){
		String result = LPAD(Integer.toString(SourceStr), StrLen, PadChar);
		
		return result;
	}
	
	public static String LPAD(long SourceStr, int StrLen, char PadChar){
		String result = LPAD(Long.toString(SourceStr), StrLen, PadChar);
		
		return result;
	}
	
	
	/**
	 * byte 占썼열占쏙옙占쏙옙 특占쏙옙 String占쏙옙 占싸듸옙占쏙옙 Return
	 * @param bytes
	 * @param Char
	 * @return
	 * @throws Exception
	 */
	public static int IndexOf(byte[] bytes, String Char) throws Exception{
		int idx = -1;
		int size = Char.getBytes().length;
		
		String TempStr = "";
		for(int i = 0; i < bytes.length; i++){
			TempStr = new String(bytes, i, size);
			if(TempStr.equals(Char)){
				idx = i;
				break;
			}
		}
		
		return idx;
	}
	
	
	/**
	 * byte 占썼열占쏙옙占쏙옙 Point占쏙옙占쏙옙 占쏙옙占쏙옙占싹댐옙 특占쏙옙 String占쏙옙 占싸듸옙占쏙옙 Return
	 * @param bytes
	 * @param Point
	 * @param Char
	 * @return
	 * @throws Exception
	 */
	public static int IndexOf(byte[] bytes, int Point, String Char) throws Exception{
		int idx = -1;
		int size = Char.getBytes().length;
		
		String TempStr = "";
		for(int i = Point; i < bytes.length; i++){
			TempStr = new String(bytes, i, size);
			if(TempStr.equals(Char)){
				idx = i;
				break;
			}
		}
		
		return idx;
	}

	
	/**
	 * byte占썼열占쏙옙占쏙옙 占쏙옙占쏙옙占싸븝옙 Return
	 * @param bytes
	 * @param sPoint
	 * @param length
	 * @return
	 * @throws Exception
	 */
	public static byte[] getBytes(byte[] bytes, int sPoint, int length) throws Exception{
		byte[] value = new byte[length];
		
		if(bytes.length < sPoint + length){
			throw new Exception("Length of bytes is less. length : " + bytes.length + " sPoint : " + sPoint + " length : " + length);
		}
		
		for(int i = 0; i < length; i++){
			value[i] = bytes[sPoint + i];
		}

		return value;
	}

	
	/**
	 * byte占썼열占쏙옙占쏙옙 占쏙옙占쏙옙占싸븝옙占쏙옙 String占쏙옙占쏙옙 Return
	 * @param bytes
	 * @param sPoint
	 * @param length
	 * @return
	 * @throws Exception
	 */
	public static String getString(byte[] bytes, int sPoint, int length) throws Exception{
		byte[] value = new byte[length];
		
		if(bytes.length < sPoint + length){
			throw new Exception("Length of bytes is less. length : " + bytes.length + " sPoint : " + sPoint + " length : " + length);
		}
		
		for(int i = 0; i < length; i++){
			value[i] = bytes[sPoint + i];
		}

		return new String(value).trim();
	}
	
	
	/**
	 * byte占썼열占쏙옙占쏙옙 占쏙옙占쏙옙占싸븝옙占쏙옙 String占쏙옙占쏙옙 Return
	 * @param bytes
	 * @param sPoint
	 * @param length
	 * @return
	 * @throws Exception
	 */
	public static String getString(byte[] bytes, int sPoint, int length, String EncodingLang) throws Exception{
		byte[] value = new byte[length];
		
		if(bytes.length < sPoint + length){
			throw new Exception("Length of bytes is less. length : " + bytes.length + " sPoint : " + sPoint + " length : " + length);
		}
		
		for(int i = 0; i < length; i++){
			value[i] = bytes[sPoint + i];
		}

		return new String(value, EncodingLang).trim();
	}

	
	/**
	 * byte占썼열占쏙옙 int占쏙옙 占쏙옙환
	 * @param bytes
	 * @return
	 * @throws Exception
	 */
	public static int byteToInt(byte[] bytes) throws Exception{
		int result;

		try{
			result = Integer.parseInt(new String(bytes).trim());
		}catch(Exception e){
			throw new Exception("Can not convert to int. byte : " + new String(bytes));
		}

		return result;
	}
	
	
	/**
	 * 占쏙옙체占쏙옙 Class Type占쏙옙 占쎈문占쌘뤄옙 Return 
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	public static String getClassName(Object obj) throws Exception{
		String Type = obj.getClass().getName();
		
		Type = Type.substring(Type.lastIndexOf(".") + 1).toUpperCase();
		
		return Type;
	}
	
	
	/**
	 * @param Str
	 * @return
	 * @throws Exception
	 */
	public static int StringToInt(String Str) throws Exception{
		int result;
		
		try{
			result = Integer.parseInt(Str);
		}catch(Exception e){
			//throw new Exception("Can not convert to Int. String : " + Str);
			result = 0;
		}
		
		return result;
	}
	
	
	/**
	 * @param Str
	 * @return
	 * @throws Exception
	 */
	public static long StringToLong(String Str) throws Exception{
		long result;
		
		try{
			result = Long.parseLong(Str);
		}catch(Exception e){
			//throw new Exception("Can not convert to Int. String : " + Str);
			result = 0;
		}
		
		return result;
	}
	
	
	/**
	 * @param Str
	 * @param TrueStr
	 * @return
	 * @throws Exception
	 */
	public static boolean StringToBoolean(String Str, String TrueStr) throws Exception{
		boolean result = false;
		
		try{
			if(Str.equals(TrueStr)){
				result = true;
			}
		}catch(Exception e){
			throw new Exception("Can not convert to Boolean. String : " + Str + ", True String : " + TrueStr);
		}
		
		return result;
	}
	
	
	/**
	 * @param srcStr
	 * @param regex
	 * @return
	 */
	public static String[] Split(String srcStr, String regex){
		if(srcStr == null){
			return null;
		}
		
		if(regex == null || regex.length() == 0){
			String[] returnStr = new String[1];
			returnStr[0] = srcStr;
			return returnStr;
		}
		
		int SplitSize = 0;
		String TempStr = srcStr;
		String Temp = "";
		int point = 0;
		
		while(TempStr.length() > 0){
			point = TempStr.indexOf(regex);
			if(point >= 0){
				Temp = TempStr.substring(0, point);
				TempStr = TempStr.substring(point + regex.length());
			}
			else{
				Temp = TempStr;
				TempStr = "";
			}
			
			if(Temp.length() > 0){
				SplitSize++;
			}
		}
		
		String[] returnStr = new String[SplitSize];
		SplitSize = 0;
		
		TempStr = srcStr;
		while(TempStr.length() > 0){
			point = TempStr.indexOf(regex);
			if(point >= 0){
				Temp = TempStr.substring(0, point);
				TempStr = TempStr.substring(point + regex.length());
			}
			else{
				Temp = TempStr;
				TempStr = "";
			}
			
			if(Temp.length() > 0){
				returnStr[SplitSize] = Temp;
				SplitSize++;
			}
		}
		
		return returnStr;
	}
	
	
	/**
	 * @param srcStr
	 * @param regex
	 * @return
	 */
	public static int[] SplitInt(String srcStr, String regex){
		if(srcStr == null){
			return null;
		}
		
		if(regex == null || regex.length() == 0){
			int[] returnStr = new int[1];
			try{
				returnStr[0] = Utility.StringToInt(srcStr);
			}catch(Exception e){
				returnStr[0] = 0;
			}
			return returnStr;
		}
		
		int SplitSize = 0;
		String TempStr = srcStr;
		String Temp = "";
		int point = 0;
		
		while(TempStr.length() > 0){
			point = TempStr.indexOf(regex);
			if(point >= 0){
				Temp = TempStr.substring(0, point);
				TempStr = TempStr.substring(point + regex.length());
			}
			else{
				Temp = TempStr;
				TempStr = "";
			}
			
			if(Temp.length() > 0){
				SplitSize++;
			}
		}
		
		int[] returnStr = new int[SplitSize];
		SplitSize = 0;
		
		TempStr = srcStr;
		while(TempStr.length() > 0){
			point = TempStr.indexOf(regex);
			if(point >= 0){
				Temp = TempStr.substring(0, point);
				TempStr = TempStr.substring(point + regex.length());
			}
			else{
				Temp = TempStr;
				TempStr = "";
			}
			
			if(Temp.length() > 0){
				try{
					returnStr[SplitSize] = Utility.StringToInt(Temp);
				}catch(Exception e){
					returnStr[SplitSize] = 0;
				}
				SplitSize++;
			}
		}
		
		return returnStr;
	}
	
	
	/**
	 * @param srcStr
	 * @param regex
	 * @return
	 */
	public static boolean[] SplitBoolean(String srcStr, String regex){
		if(srcStr == null){
			return null;
		}
		
		if(regex == null || regex.length() == 0){
			boolean[] returnStr = new boolean[1];
			try{
				returnStr[0] = Utility.StringToBoolean(srcStr.toUpperCase(), "TRUE");
			}catch(Exception e){
				returnStr[0] = false;
			}
			return returnStr;
		}
		
		int SplitSize = 0;
		String TempStr = srcStr;
		String Temp = "";
		int point = 0;
		
		while(TempStr.length() > 0){
			point = TempStr.indexOf(regex);
			if(point >= 0){
				Temp = TempStr.substring(0, point);
				TempStr = TempStr.substring(point + regex.length());
			}
			else{
				Temp = TempStr;
				TempStr = "";
			}
			
			if(Temp.length() > 0){
				SplitSize++;
			}
		}
		
		boolean[] returnStr = new boolean[SplitSize];
		SplitSize = 0;
		
		TempStr = srcStr;
		while(TempStr.length() > 0){
			point = TempStr.indexOf(regex);
			if(point >= 0){
				Temp = TempStr.substring(0, point);
				TempStr = TempStr.substring(point + regex.length());
			}
			else{
				Temp = TempStr;
				TempStr = "";
			}
			
			if(Temp.length() > 0){
				try{
					returnStr[SplitSize] = Utility.StringToBoolean(Temp.toUpperCase(), "TRUE");
				}catch(Exception e){
					returnStr[SplitSize] = false;
				}
				SplitSize++;
			}
		}
		
		return returnStr;
	}
	
	
	/**
	 * @param srcStr
	 * @param regex
	 * @return
	 */
	public static String[] SplitAll(String srcStr, String regex){
		if(srcStr == null){
			return null;
		}
		
		if(regex == null || regex.length() == 0){
			String[] returnStr = new String[1];
			returnStr[0] = srcStr;
			return returnStr;
		}
		
		int SplitSize = 0;
		String TempStr = srcStr;
		String Temp = "";
		int point = 0;
		
		while(TempStr.length() > 0){
			point = TempStr.indexOf(regex);
			if(point >= 0){
				Temp = TempStr.substring(0, point);
				TempStr = TempStr.substring(point + regex.length());
			}
			else{
				Temp = TempStr;
				TempStr = "";
			}
			
			SplitSize++;
		}
		
		String[] returnStr = new String[SplitSize];
		SplitSize = 0;
		
		TempStr = srcStr;
		while(TempStr.length() > 0){
			point = TempStr.indexOf(regex);
			if(point >= 0){
				Temp = TempStr.substring(0, point);
				TempStr = TempStr.substring(point + regex.length());
			}
			else{
				Temp = TempStr;
				TempStr = "";
			}
			
			returnStr[SplitSize] = Temp;
			SplitSize++;
		}
		
		return returnStr;
	}
	
	
	/**
	 * @param srcStr
	 * @param regex
	 * @return
	 */
	public static int[] SplitIntAll(String srcStr, String regex){
		if(srcStr == null){
			return null;
		}
		
		if(regex == null || regex.length() == 0){
			int[] returnStr = new int[1];
			try{
				returnStr[0] = Utility.StringToInt(srcStr);
			}catch(Exception e){
				returnStr[0] = 0;
			}
			return returnStr;
		}
		
		int SplitSize = 0;
		String TempStr = srcStr;
		String Temp = "";
		int point = 0;
		
		while(TempStr.length() > 0){
			point = TempStr.indexOf(regex);
			if(point >= 0){
				Temp = TempStr.substring(0, point);
				TempStr = TempStr.substring(point + regex.length());
			}
			else{
				Temp = TempStr;
				TempStr = "";
			}
			
			SplitSize++;
		}
		
		int[] returnStr = new int[SplitSize];
		SplitSize = 0;
		
		TempStr = srcStr;
		while(TempStr.length() > 0){
			point = TempStr.indexOf(regex);
			if(point >= 0){
				Temp = TempStr.substring(0, point);
				TempStr = TempStr.substring(point + regex.length());
			}
			else{
				Temp = TempStr;
				TempStr = "";
			}
			
			try{
				returnStr[SplitSize] = Utility.StringToInt(Temp);
			}catch(Exception e){
				returnStr[SplitSize] = 0;
			}
			SplitSize++;
		}
		
		return returnStr;
	}
	
	
	/**
	 * @param srcStr
	 * @param regex
	 * @return
	 */
	public static boolean[] SplitBooleanAll(String srcStr, String regex){
		if(srcStr == null){
			return null;
		}
		
		if(regex == null || regex.length() == 0){
			boolean[] returnStr = new boolean[1];
			try{
				returnStr[0] = Utility.StringToBoolean(srcStr.toUpperCase(), "TRUE");
			}catch(Exception e){
				returnStr[0] = false;
			}
			return returnStr;
		}
		
		int SplitSize = 0;
		String TempStr = srcStr;
		String Temp = "";
		int point = 0;
		
		while(TempStr.length() > 0){
			point = TempStr.indexOf(regex);
			if(point >= 0){
				Temp = TempStr.substring(0, point);
				TempStr = TempStr.substring(point + regex.length());
			}
			else{
				Temp = TempStr;
				TempStr = "";
			}
			
			SplitSize++;
		}
		
		boolean[] returnStr = new boolean[SplitSize];
		SplitSize = 0;
		
		TempStr = srcStr;
		while(TempStr.length() > 0){
			point = TempStr.indexOf(regex);
			if(point >= 0){
				Temp = TempStr.substring(0, point);
				TempStr = TempStr.substring(point + regex.length());
			}
			else{
				Temp = TempStr;
				TempStr = "";
			}
			
			try{
				returnStr[SplitSize] = Utility.StringToBoolean(Temp.toUpperCase(), "TRUE");
			}catch(Exception e){
				returnStr[SplitSize] = false;
			}
			SplitSize++;
		}
		
		return returnStr;
	}
	
	
	/**
	 * StringBuffer replaceAll
	 * @param SrcBuff
	 * @param RegexStr
	 * @param ReplaceStr
	 * @return
	 */
	public static StringBuffer StringBufferReplaceAll(StringBuffer SrcBuff, String RegexStr, String ReplaceStr){
		int idx = -1;
		
		while((idx = SrcBuff.indexOf(RegexStr)) >= 0){
			SrcBuff = SrcBuff.replace(idx, idx + RegexStr.length(), ReplaceStr);
		}
		
		return SrcBuff;
	}
	
	
	public static String MarkDate(String DateStr, String Mark){
		String Result = "";
		
		if(DateStr.length() == 6){
			Result = DateStr.substring(0, 2) + Mark + DateStr.substring(2, 4) + Mark + DateStr.substring(4);
		}
		else if(DateStr.length() == 8){
			Result = DateStr.substring(0, 4) + Mark + DateStr.substring(4, 6) + Mark + DateStr.substring(6);
		}
		else{
			Result = DateStr;
		}
		
		return Result;
	}
	
	public static String MarkDateWithYoil(String DateStr, String Mark){
		String Result = "";
		String yoil = "";
		if(DateStr.length() == 6){
			Result = DateStr.substring(0, 2) + Mark + DateStr.substring(2, 4) + Mark + DateStr.substring(4);
			return Result;
		}
		else if(DateStr.length() == 8){
			Result = DateStr.substring(0, 4) + Mark + DateStr.substring(4, 6) + Mark + DateStr.substring(6);
		}
		else{
			return DateStr;
		}

		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyyMMdd", java.util.Locale.KOREA);
		java.util.Date date = DateTime.getFormatDate("yyyyMMdd", DateStr);

		java.util.Calendar calendar = formatter.getCalendar();
		calendar.setTime(date);		
   		int dayOfWeek = calendar.get(java.util.Calendar.DAY_OF_WEEK);
		if (dayOfWeek == java.util.Calendar.MONDAY)
			yoil = "(占쏙옙)";
		if (dayOfWeek == java.util.Calendar.TUESDAY)
			yoil = "(화)";	 	
		if (dayOfWeek == java.util.Calendar.WEDNESDAY)
			yoil = "(占쏙옙)";		 
		if (dayOfWeek == java.util.Calendar.THURSDAY)
			yoil = "(占쏙옙)";	 
		if (dayOfWeek == java.util.Calendar.FRIDAY)
			yoil = "(占쏙옙)";	 		
		if (dayOfWeek == java.util.Calendar.SATURDAY)
			yoil = "(占쏙옙)";		  
		if (dayOfWeek == java.util.Calendar.SUNDAY)
			yoil = "(占쏙옙)";	
		
		
		return Result + yoil;
	}
	

	public static String MarkTime(String TimeStr, String Mark){
		String Result = "";
		
		if(TimeStr.length() == 4){
			Result = TimeStr.substring(0, 2) + Mark + TimeStr.substring(2);
		}
		else if(TimeStr.length() == 6){
			Result = TimeStr.substring(0, 2) + Mark + TimeStr.substring(2, 4) + Mark + TimeStr.substring(4);
		}
		else{
			Result = TimeStr;
		}
		
		return Result;
	}
	
	
	public static String setComma(String str){
		if(str == null || str == "") return "";
		
		str = str.trim();
		String BuffStr = str;
		
		String ReturnStr = "";
		
		try{
			String Minus = "";
			if(BuffStr.substring(0, 1).equals("-")){
				Minus = "-";
				BuffStr = BuffStr.substring(1);
			}
	
			int DotPoint = BuffStr.indexOf(".");
			
			String Prefix = "";
			String Suffix = "";
			
			if(DotPoint > 0){
				Prefix = BuffStr.substring(0, DotPoint);
				Suffix = BuffStr.substring(DotPoint + 1);
			}
			else{
				Prefix = BuffStr;
			}
			
			for(int idx = Prefix.length(); idx > 0; idx = idx - 3){
				if(idx - 3 > 0){
					ReturnStr = "," + BuffStr.substring(idx - 3, idx) + ReturnStr;
				}
				else{
					ReturnStr = BuffStr.substring(0, idx) + ReturnStr;
				}
			}
			
			if(DotPoint > 0){
				ReturnStr += "." + Suffix;
			}
			
			ReturnStr = Minus + ReturnStr;
		}catch(Exception e){
			ReturnStr = str;
		}
		
		return  ReturnStr;
	}
	
	public static String setComma(int str){
		return  setComma(Integer.toString(str));
	}
	
	public static String setComma(long str){
		return  setComma(Long.toString(str));
	}
	
	
	/**
	 * Xml 占쏙옙占쏙옙 占싻는댐옙.
	 * Document占쏙옙 占쏙옙환占싼댐옙.
	 * 
	 * @param is
	 * @return
	 * @throws Exception
	 */
	public static Document loadXml(InputStream is) throws Exception{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setIgnoringElementContentWhitespace(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		
		Document doc = builder.parse(is);
		
		return doc;
	}
	
	
	/**
	 * Xml 占쏙옙占쏙옙 占싻는댐옙.
	 * Document占쏙옙 占쏙옙환占싼댐옙.
	 * 
	 * @param rd
	 * @return
	 * @throws Exception
	 */
	public static Document loadXml(Reader rd) throws Exception{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setIgnoringElementContentWhitespace(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		
		InputSource iu = new InputSource(new BufferedReader(rd));
		
		Document doc = builder.parse(iu);
		
		return doc;
	}
	
	
	/**
	 * Xml 占쏙옙占쏙옙 占싻는댐옙.
	 * Document占쏙옙 占쏙옙환占싼댐옙.
	 * 
	 * @param FileName
	 * @return
	 * @throws Exception
	 */
	public static Document loadXml(String FileName) throws Exception{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setIgnoringElementContentWhitespace(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.parse(FileName);
		
		return doc;
	}
	
	
	/**
	 * xml 占쏙옙占쏙옙 占싻는댐옙.
	 * Document占쏙옙 占쏙옙환占싼댐옙.
	 * 
	 * @param fileName
	 * @return
	 * @throws Exception
	 */
	public static Document loadXmlFile(String FileName) throws Exception{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setIgnoringElementContentWhitespace(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.parse(Utility.class.getResourceAsStream(FileName));
		
		return doc;
	}
	
	
	/**
	 * Xml String 占싻는댐옙.
	 * Document占쏙옙 占쏙옙환占싼댐옙.
	 * 
	 * @param XmlData
	 * @return
	 * @throws Exception
	 */
	public static Document loadXmlString(String XmlData) throws Exception{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setIgnoringElementContentWhitespace(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		
		InputSource iu = new InputSource(new StringReader(XmlData));
		
		Document doc = builder.parse(iu);
		
		return doc;
	}
	
	
	/**
	 * Document占쏙옙占쏙옙 TagName占쏙옙 Element List 占쏙옙占쏙옙
	 * 
	 * @param Doc
	 * @param TagName
	 * @return
	 * @throws Exception
	 */
	public static NodeList getElementList(Document Doc, String TagName) throws Exception{
		NodeList XmlNodeList = Doc.getElementsByTagName(TagName);
		
		if(XmlNodeList.getLength() == 0){
			throw new Exception("Not Exist " + TagName + " Elements!");
		}
		
		return XmlNodeList;
	}
	
	
	/**
	 * Node占쏙옙占쏙옙 TagName占쏙옙 Element List 占쏙옙占쏙옙
	 * 
	 * @param XmlNode
	 * @param TagName
	 * @return
	 * @throws Exception
	 */
	public static NodeList getElementList(Node XmlNode, String TagName) throws Exception{
		NodeList XmlNodeList = ((Element)XmlNode).getElementsByTagName(TagName);
		
		if(XmlNodeList.getLength() == 0){
			throw new Exception("Not Exist " + TagName + " Elements!");
		}
		
		return XmlNodeList;
	}
	
	
	/**
	 * Node value 占쏙옙占쏙옙
	 * 
	 * @param XmlNode
	 * @return
	 * @throws Exception
	 */
	public static String getNodeValue(Node XmlNode) throws Exception{
		String NodeValue = XmlNode.getNodeValue();
		
		if(NodeValue == null){
			try{
				NodeList XmlNodeList = ((Element)XmlNode).getChildNodes();
				
				for(int idx = 0; idx < XmlNodeList.getLength(); idx++){
					if(XmlNodeList.item(idx).getNodeType() == Node.TEXT_NODE || XmlNodeList.item(idx).getNodeType() == Node.CDATA_SECTION_NODE){
						NodeValue = XmlNodeList.item(idx).getNodeValue();
					}
				}
			}catch(Exception e){
				NodeValue = "";
			}
		}
		
		if(NodeValue == null){
			//throw new Exception("Not Exist Node Value!");
			NodeValue = "";
		}
		
		return NodeValue.trim();
	}
	
	
	/**
	 * Node占쏙옙占쏙옙 TagName占쏙옙 value 占쏙옙占쏙옙
	 * 
	 * @param XmlNode
	 * @param TagName
	 * @return
	 * @throws Exception
	 */
	public static String getNodeValue(Node XmlNode, String TagName) throws Exception{
		NodeList XmlNodeList = getElementList(XmlNode, TagName);
		
		String NodeValue = "";
		if(XmlNodeList.getLength() > 0){
			NodeValue = getNodeValue(XmlNodeList.item(0));
		}
		
		return NodeValue;
	}
	
	
	/**
	 * Node占쏙옙占쏙옙 TagName占쏙옙 value 占쏙옙占쏙옙
	 * 
	 * @param XmlNode
	 * @param TagName
	 * @return
	 */
	public static String getNodeValueNoCheck(Node XmlNode, String TagName){
		String NodeValue = "";
		
		try{
			NodeList XmlNodeList = getElementList(XmlNode, TagName);
			
			if(XmlNodeList.getLength() > 0){
				NodeValue = getNodeValue(XmlNodeList.item(0));
			}
		}catch(Exception e){
			NodeValue = "";
		}
		
		return NodeValue;
	}
	
	
	/**
	 * Paging Script
	 * @param TotalPage
	 * @param CurrPage
	 * @param FirstImgUrl
	 * @param PrevImgUrl
	 * @param NextImgUrl
	 * @param LastImgUrl
	 * @return
	 */
	public static String PagePrintScript(int TotalPage, int CurrPage, String FirstImgUrl, String PrevImgUrl, String NextImgUrl, String LastImgUrl){
		int page_per_block = 10;
		int member_page = 0;
		int total_block = 1;
		
		if(TotalPage % page_per_block == 0){
			total_block = TotalPage / page_per_block;
		}
		else{
			total_block = TotalPage / page_per_block + 1;
		}
		
		int block = 1;
		if(CurrPage % page_per_block == 0){
			block = CurrPage / page_per_block;
		}
		else{
			block = CurrPage / page_per_block + 1;
		}
		
		int first_page = (block - 1) * page_per_block;
		int last_page = block * page_per_block;
		if(total_block <= block){
			last_page = TotalPage;
		}
		
		StringBuffer result = new StringBuffer();
		if(block > 1){
			member_page = 1;
			
			if(FirstImgUrl.length() > 0){
				result.append("<a href='JavaScript:ListPage(" + member_page + ")'><img src=\"" + FirstImgUrl + "\" alt=\"\uCC98\uC74C\" /></a>\n");
			}
			else{
				result.append("<a href='JavaScript:ListPage(" + member_page + ")' style=\"text-decoration:none\">[\uCC98\uC74C]</a>\n");
			}
		}
		if(block > 1){
			member_page = first_page;
			
			if(PrevImgUrl.length() > 0){
				result.append("<a href='JavaScript:ListPage(" + member_page + ")'><img src=\"" + PrevImgUrl + "\" alt=\"\uC774\uC804 " + page_per_block + "\uAC1C\" /></a>\n");
			}
			else{
				result.append("<a href='JavaScript:ListPage(" + member_page + ")' style=\"text-decoration:none\">[\uC774\uC804 " + page_per_block + "\uAC1C]</a>\n");
			}
		}
		
		result.append("<ul>");
		
		for(int direct_page = first_page + 1; direct_page <= last_page; direct_page++){
			if(CurrPage == direct_page){
				result.append("<li class=\"CurrPage\">" + direct_page + "</li>");
			}
			else{
				result.append("<li><a href='JavaScript:ListPage(" + direct_page + ")'>" + direct_page + "</a></li>");
			}
		}
		
		result.append("</ul>");
		
		if(block < total_block){
			member_page = last_page + 1;
			
			if(NextImgUrl.length() > 0){
				result.append("\n<a href='JavaScript:ListPage(" + member_page + ")'><img src=\"" + NextImgUrl + "\" alt=\"\uB2E4\uC74C " + page_per_block + "\uAC1C\" /></a>");
			}
			else{
				result.append("\n<a href='JavaScript:ListPage(" + member_page + ")' style=\"text-decoration:none\">[\uB2E4\uC74C " + page_per_block + "\uAC1C]</a>");
			}
		}
		if(block < total_block){
			member_page = TotalPage;
			
			if(LastImgUrl.length() > 0){
				result.append("\n<a href='JavaScript:ListPage(" + member_page + ")'><img src=\"" + LastImgUrl + "\" alt=\"\uB9C8\uC9C0\uB9C9\" /></a>");
			}
			else{
				result.append("\n<a href='JavaScript:ListPage(" + member_page + ")' style=\"text-decoration:none\">[\uB9C8\uC9C0\uB9C9]</a>");
			}
		}
		return result.toString();
	}
	
	
	/**
	 * Paging Script
	 * @param TotalPage
	 * @param CurrPage
	 * @return
	 */
	public static String PagePrintScript(int TotalPage, int CurrPage){
		int page_per_block = 10;
		int member_page = 0;
		int total_block = 1;
		
		if(TotalPage % page_per_block == 0){
			total_block = TotalPage / page_per_block;
		}
		else{
			total_block = TotalPage / page_per_block + 1;
		}
		
		int block = 1;
		if(CurrPage % page_per_block == 0){
			block = CurrPage / page_per_block;
		}
		else{
			block = CurrPage / page_per_block + 1;
		}
		
		int first_page = (block - 1) * page_per_block;
		int last_page = block * page_per_block;
		if(total_block <= block){
			last_page = TotalPage;
		}
		
		StringBuffer result = new StringBuffer();
		if(block > 1){
			member_page = 1;
			
			result.append("<span class='firstPage'><a href='#' onclick='ListPage(" + member_page + ")'>&lt;&lt;</a></span>\n");
		}
		else{
			result.append("<span class='firstPage'>&lt;&lt;</span>\n");
		}
		
		if(block > 1){
			member_page = first_page;
			
			result.append("<span class='prevPage'><a href='#' onclick='ListPage(" + member_page + ")'>&lt;</a></span>\n");
		}
		else{
			result.append("<span class='prevPage'>&lt;</span>\n");
		}
		
		for(int direct_page = first_page + 1; direct_page <= last_page; direct_page++){
			if(CurrPage == direct_page){
				result.append("<span class='now' title='������吏�>" + direct_page + "</span>");
			}
			else{
				result.append("<a href='#' onclick='ListPage(" + direct_page + ")' title='" + direct_page + "���吏��'>" + direct_page + "</a>\n");
			}
		}
		
		if(block < total_block){
			member_page = last_page + 1;
			
			result.append("<span class='nextPage'><a href='#' onclick='ListPage(" + member_page + ")' title='�ㅼ� ���吏�>&gt;</a></span>\n");
		}
		else{
			result.append("<span class='nextPage'>&gt;</span>\n");
		}
		
		if(block < total_block){
			member_page = TotalPage;
			
			result.append("<span class='lastPage'><a href='#' onclick='ListPage(" + member_page + ")' title='留��留����吏�>&gt;&gt;</a></span>\n");
		}
		else{
			result.append("<span class='lastPage'>&gt;&gt;</span>\n");
		}
		return result.toString();
	}
	
	
	public static String ReplaceAll(String str, String src, String tgt){
		StringBuffer buf = new StringBuffer();
		String ch  = null;

		if(str == null || str.length() == 0){
			return "";
		}

		int idx = 0;
		int len = src.length();
		while(idx < str.length() - len + 1){
			ch = str.substring(idx, idx + len);
			if(ch.equals(src)){
				buf.append(tgt);
				idx = idx + len;
			}
			else{
				buf.append(str.substring(idx, idx + 1));
				idx++;
			}
		}

		if(idx < str.length()){
			buf.append(str.substring(idx, str.length()));
		}

		return buf.toString();
	}
	
	
	public static String ReplaceAll(String str, String src, int tgt){
		StringBuffer buf = new StringBuffer();
		String ch  = null;

		if(str == null || str.length() == 0){
			return "";
		}

		int idx = 0;
		int len = src.length();
		while(idx < str.length() - len + 1){
			ch = str.substring(idx, idx + len);
			if(ch.equals(src)){
				buf.append(tgt);
				idx = idx + len;
			}
			else{
				buf.append(str.substring(idx, idx + 1));
				idx++;
			}
		}

		if(idx < str.length()){
			buf.append(str.substring(idx, str.length()));
		}

		return buf.toString();
	}
	
	
	public static String ReplaceAll(String str, String src, long tgt){
		StringBuffer buf = new StringBuffer();
		String ch  = null;

		if(str == null || str.length() == 0){
			return "";
		}

		int idx = 0;
		int len = src.length();
		while(idx < str.length() - len + 1){
			ch = str.substring(idx, idx + len);
			if(ch.equals(src)){
				buf.append(tgt);
				idx = idx + len;
			}
			else{
				buf.append(str.substring(idx, idx + 1));
				idx++;
			}
		}

		if(idx < str.length()){
			buf.append(str.substring(idx, str.length()));
		}

		return buf.toString();
	}
	
	public static String getConfigProperty(String propertyName) {
		ClassLoader cl;
        cl = (Thread.currentThread()).getContextClassLoader();
        if( cl == null ) {
            cl = ClassLoader.getSystemClassLoader(); 
        }
		String sFilePath = cl.getResource("/").getPath().replace("%20", " ") + "/application.properties"; 
		Properties p = new Properties();
		FileInputStream in = null;
		try {
			in = new FileInputStream("/" + sFilePath);
			p.load(in);
			in.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return p.getProperty(propertyName);
	}
}
