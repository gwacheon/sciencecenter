package kr.go.sciencecenter.util;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.multipart.MultipartFile;

public class FileUploadHelper {
	@Value("${webroot.path}") private String fileRoot;
	@Value("${legacy.companycd}") private String companycd;
	
	public static final String FILE_PATH = "File";
	/**
     * 첨부파일을 서버에 저장한다.
     * 
     * @param file
     * @param subPath
     * @param fileName
     * @throws Exception
     */
	public boolean upload(MultipartFile file, String subPath, String fileName) {
		boolean isUpload = false;
		
		String directory = fileRoot + companycd + "/" + subPath;
		File dir = new File(directory);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		
		if (fileName == null) {
			fileName = file.getOriginalFilename();
		} 
		
		File filePath = new File(directory + "/" + fileName);
		
		try {
			file.transferTo(filePath);
			isUpload = true;
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return isUpload;
	}
	
	public boolean multiUpload(List<MultipartFile> files, String subPath) {
		for(MultipartFile f : files) {
			this.upload(f, subPath, f.getOriginalFilename());
		}
		return false;
	}
	
	public boolean delete(String subPath, String fileName) {
		boolean isDeleted = false;
		
		String directory = fileRoot + companycd + "/" + subPath;
		File dir = new File(directory + "/" + fileName);
		if( dir.exists() && dir.delete()) {
			isDeleted = true;
		}
		
		return isDeleted;
		
	}
	
	public boolean multiDelete(String subPath) {
		boolean isDeleted = false;
		
		String directory = fileRoot + companycd + "/" + subPath;
		File dir = new File(directory);
		if( dir.exists() ) {
			try {
				FileUtils.deleteDirectory(dir);
				isDeleted = true;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return isDeleted;
	}
}
