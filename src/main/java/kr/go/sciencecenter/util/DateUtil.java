package kr.go.sciencecenter.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class DateUtil {
	public List<Date> getDateRange(Integer year, Integer month) {
		List<Date> dates = new ArrayList<Date>();

	    {
	        Calendar calendar = getCalendarForNow(year, month);
	        calendar.set(Calendar.DAY_OF_MONTH,
	                calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
	        setTimeToBeginningOfDay(calendar);
	        dates.add(calendar.getTime());
	    }

	    {
	        Calendar calendar = getCalendarForNow(year, month);
	        calendar.set(Calendar.DAY_OF_MONTH,
	                calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
	        setTimeToEndofDay(calendar);
	        dates.add(calendar.getTime());
	    }

	    return dates;
	}
	
	public static Calendar getCalendarForNow(Integer year, Integer month) {
	    Calendar calendar = GregorianCalendar.getInstance();
	    if(year == null || month == null){
	    	calendar.setTime(new Date());
	    }else{
	    	calendar.set(Calendar.YEAR, year);
	    	calendar.set(Calendar.MONTH, (month-1));
	    }
	    
	    return calendar;
	}

	public static void setTimeToBeginningOfDay(Calendar calendar) {
	    calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);
	}

	public static void setTimeToEndofDay(Calendar calendar) {
	    calendar.set(Calendar.HOUR_OF_DAY, 23);
	    calendar.set(Calendar.MINUTE, 59);
	    calendar.set(Calendar.SECOND, 59);
	    calendar.set(Calendar.MILLISECOND, 999);
	}
	
	
	
	public static String getWeekName(String date) {
		return getWeekName(date, "yyyyMMdd");

	}
	
	public static String getWeekName(String date, String dateType) {
		String weekName = "";

		SimpleDateFormat dateFormat = new SimpleDateFormat(dateType);
		Date nDate = null;
		try {
			nDate = dateFormat.parse(date);
			Calendar cal = Calendar.getInstance();
			cal.setTime(nDate);

			int dayNum = cal.get(Calendar.DAY_OF_WEEK);

			switch (dayNum) {
			case 1:
				weekName = "일";
				break;
			case 2:
				weekName = "월";
				break;
			case 3:
				weekName = "화";
				break;
			case 4:
				weekName = "수";
				break;
			case 5:
				weekName = "목";
				break;
			case 6:
				weekName = "금";
				break;
			case 7:
				weekName = "토";
				break;

			}
		} catch (ParseException e) {}

		return weekName;
	}
}
