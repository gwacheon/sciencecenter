package kr.go.sciencecenter.util;

public class Paging { 
	
	public int start_row; // 현재 페이지에 첫번째 글번호
	public int end_row; // 현재 페이지에 마지막 글번호
	public int block_cnt;  // 한 페이지에 보여줄 글 목록 갯수
	public int block_page; // 한 페이지에 보여줄 페이지 블럭 갯수 [1] [2] [3] [4] [5] 갯수
	private int start_page; //시작 페이지
	private int end_page; // 마지막 페이지 
	private int page_count;  //전체 페이지의 수
	
	private int total_cnt; // 전체 글 갯수
	private int currentPage;
	
	StringBuffer sb = new StringBuffer();
	
	public Paging(int cntPage, int record_cnt,int block_cnt,int block_page){  
		//currentPage는 현재 페이지,record_cnd는 총 카운트,block_cnt는 한면에 보여줄 게시물수
		this.block_page = block_page;
		this.currentPage = cntPage;
		this.total_cnt = record_cnt;
		this.block_cnt = block_cnt;
		
		page_count = (int)Math.ceil((double)total_cnt/block_cnt);
		
		// 전체 페이지가 없을때 페이지가 1 
		if(page_count ==0){
			page_count=1;
		}
		// 현재 페이지가 전체 페이지보다 크면 현재 페이지가 전체 페이지
		if(currentPage > page_count){
			currentPage = page_count;
		}
		
		start_row = (currentPage - 1) * block_cnt+1;
		
		if(page_count == currentPage) end_row = total_cnt; 
		else end_row = currentPage * block_cnt;
		
		if((currentPage%block_page) == 0){
			start_page = ((int)(currentPage/block_page)-1)*block_page+1; // 만약 현재 페이지가 5일때 0으로 나누어 떨어지므로 자기 자신을 제외하고 연산해주면 됨.
		}else{
			start_page = (int)(currentPage/block_page) * block_page + 1; 
		}
		
		
	    end_page = start_page +block_page -1;
	    if(end_page>page_count) end_page = page_count; //마지막 페이지가 총 페이지 수 보다 큰경우는 마지막 페이지가 곧 페이지의 총 갯수와 같다. 
	    
	}
	
	public String showBlock_page(){
		sb.append("<ul class='pagination'>");
	   if(start_page>block_page){
		   sb.append("<li><a href='javascript:void(0)' onclick=\'fn_page(" + 1 + ")\'>");
		   sb.append("<i class=\'fa fa-chevron-left\'></i>");
		   sb.append("</a></li>");
		   
		   sb.append("<li><a href='javascript:void(0)' onclick=\'fn_page(" + (currentPage - 1) + ")\' >");
		   sb.append("<i class=\'fa fa-ellipsis-h\'></i>");
		   sb.append("</a></li>");
	   }
	
	   for(int i = start_page; i<=end_page; i++){
		   if(currentPage == i){
			   sb.append("<li class='active'><a class='page-link' href=\'javascript:void(0)\'>"  + i + "</a></li>");
		   }else{
			   sb.append("<li><a href=\'javascript:void(0)\' class='page-link' onclick=\'fn_page(" + i + ")\'>" + i + "</a></li>");
		   }
			   
	   }
	   
	   if(end_page < page_count){
		   sb.append("<li><a href=\'javascript:void(0)\' onclick=\'fn_page(" + (currentPage + 1) + ")\' class=\'btn\'>");
		   sb.append("<i class=\'fa fa-ellipsis-h\'></i></a>");
		   sb.append("</a></li>");
		   
		   sb.append("<li><a href=\'javascript:void(0)\' onclick=\'fn_page(" + page_count + ")\' class=\'btn\'>");
		   sb.append("<i class=\'fa fa-chevron-right\'></i>");
		   sb.append("</a></li>");
	   }
	   sb.append("</ul>");
	   
	   return sb.toString();
	}
}