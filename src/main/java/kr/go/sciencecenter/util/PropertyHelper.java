package kr.go.sciencecenter.util;

import org.springframework.beans.factory.annotation.Value;

public class PropertyHelper {
	@Value("${server_url}") 			private String server_url;
	@Value("${webroot.path}") 			private String fileRoot;
	@Value("${webroot.relativePath} ") 	private String fileRelativeRoot;
	@Value("${legacy.companycd}") 		private String companycd;
	
	@Value("${c2_admin_url}") 			private String c2_admin_url;
	
	@Value("${pay.bizNo}") 				private String pay_bizNo;		//사업자번호
	@Value("${pay.returnType}") 		private String pay_returnType;	//결제타입 고정 값 - payment
	@Value("${pay.version}") 			private String pay_version;		//1.3
	@Value("${pay.targetUrl}") 			private String pay_targetUrl;
	@Value("${pay.authType}") 			private String pay_authType;
	@Value("${pay.server}") 			private String pay_server;
	@Value("${c2_homepage_url}") 		private String c2_homepage_url;	//홈페이지 url
	@Value("${pay.cancel}") 			private String pay_cancel;		//승인취소
	
	@Value("${pay.mbrId}") 				private String pay_mbrId;		//연간회원 결제시 사용
	@Value("${pay.mbrName}") 			private String pay_mbrName;		//연간회원 결제시 사용
	
	@Value("${releaseHoldSeatTime}")    private String releaseHoldSeatTime;	 // 스케줄러에서 예약 후 미결제 보류 좌석 조회 기준 시간 
	@Value("${scheduleOn}")    			private String scheduleOn;			 // 스케줄러 사용여부
	
	public String getServer_url() {
		return server_url;
	}
	public String getFileRoot() {
		return fileRoot;
	}
	public String getFileRelativeRoot() {
		return fileRelativeRoot;
	}
	public String getCompanycd() {
		return companycd;
	}
	public String getFileRootUrl() {
		return this.fileRelativeRoot + this.companycd;
	}
	
	public String getC2_admin_url() {
		return c2_admin_url;
	}
	
	public String getPay_bizNo() {
		return pay_bizNo;
	}
	public String getPay_returnType() {
		return pay_returnType;
	}
	public String getPay_version() {
		return pay_version;
	}
	public String getPay_targetUrl() {
		return pay_targetUrl;
	}
	public String getPay_authType() {
		return pay_authType;
	}
	public String getPay_server() {
		return pay_server;
	}
	public String getC2_homepage_url() {
		return c2_homepage_url;
	}
	public String getPay_cancel() {
		return pay_cancel;
	}
	public String getPay_mbrId() {
		return pay_mbrId;
	}
	public String getPay_mbrName() {
		return pay_mbrName;
	}
	public String getReleaseHoldSeatTime() {
		return releaseHoldSeatTime;
	}
	public void setReleaseHoldSeatTime(String releaseHoldSeatTime) {
		this.releaseHoldSeatTime = releaseHoldSeatTime;
	}
	public String getScheduleOn() {
		return scheduleOn;
	}
	
}
