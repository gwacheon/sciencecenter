package kr.go.sciencecenter.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DownloadHelper {
	public static void downloadFile(HttpServletRequest request, HttpServletResponse response, String path, String fileName, String fileStr) {
		FileInputStream inputStream;
		OutputStream outStream;
		
		try {
			String mimeType = "application/octet-stream";
			String webappRootPath = request.getSession().getServletContext().getRealPath("/");
	    	String filePath = webappRootPath + "/resources/downloads/" + path + "/" + fileStr;
			
	    	fileName = URLEncoder.encode(fileName, "utf-8");
	    	
	    	File downloadFile = new File(filePath);
	        
			inputStream = new FileInputStream(downloadFile);
	    	
	    	response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\";");
			response.setHeader("Content-Transfer-Encoding", "binary");
	    	response.setContentType(mimeType);
	        response.setContentLength((int) downloadFile.length());
	    	
	        outStream = response.getOutputStream();
	        
	        byte[] buffer = new byte[4096];
	        int bytesRead = -1;
	 
	        // write bytes read from the input stream into the output stream
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }
	    	
			inputStream.close();
	        outStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
		} 
	}
}
