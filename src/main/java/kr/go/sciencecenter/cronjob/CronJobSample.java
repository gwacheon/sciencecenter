package kr.go.sciencecenter.cronjob;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@EnableScheduling
public class CronJobSample {
  
  @Scheduled(fixedDelay = 5000)
  public void fixedDelayTask(){
    //System.out.println("cron");
  }
}
