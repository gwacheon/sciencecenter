package kr.go.sciencecenter.cronjob;

import java.util.HashMap;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import kr.co.mainticket.Science.Interlock.EncManager;
import kr.go.sciencecenter.service.EntranceService;
import kr.go.sciencecenter.util.PropertyHelper;

/**
 * 좌석 예약 후 결제 진행이 되지 않는 보류 좌석에 대해서 QuartzJob Scheduler 으로 보류 해제 진행
 * 1. 좌석 HOLD 이후 결제 진행이 없는 예매번호 리스트 조회 (application.properties - releaseHoldSeatTime : 10분 셋팅 )
 * 2. HOLD된 좌석의 예매번호 기준으로 예약정보(T_RESERVE), 거래정보(T_TRAN) 삭제 및 스케쥴별좌석정보(T_SCHEDULE_SEAT_SUM) 변경한다.
 * @author Raphael
 * 2016.07.06
 */
public class CronJobReleaseHoldSeat extends QuartzJobBean{
	private EntranceService entranceService;
	private PropertyHelper propertyHelper;
	
	private static final Logger logger = LoggerFactory.getLogger(CronJobReleaseHoldSeat.class);
	
	public void setEntranceService(EntranceService entranceService) {
		this.entranceService = entranceService;
	}
	
	public void setPropertyHelper(PropertyHelper propertyHelper) {
		this.propertyHelper = propertyHelper;
	}
	
	
	@Override
	protected void executeInternal(JobExecutionContext ctx) throws JobExecutionException {
		try {
			
			logger.info("ScheduleOn : {} ", propertyHelper.getScheduleOn());
			if(propertyHelper.getScheduleOn().equals("Y")) {
				System.out.println("############[Release Hold Seat: Batch Start]##########");
				
				//1. 좌석 HOLD 이후 결제 진행이 없는 예매번호 리스트 조회 (application.properties - releaseHoldSeatTime :  10분 셋팅 )
				List<HashMap<String,String>> holdSeatList  = entranceService.getHoldSeatList();
				
				System.out.println("#####[holdSeatList : " + holdSeatList.size());
				for(int i=0; i < holdSeatList.size() ; i++){
					
					HashMap<String, Object> params = new HashMap<String,Object>();
					
					params.put("UNIQUE_NBR",		holdSeatList.get(i).get("UNIQUE_NBR"));
					
					System.out.println("Delete UNIQUE_NBR ==> " + params.get("UNIQUE_NBR"));
					
					
					//2. HOLD된 좌석의 예매번호 기준으로 예약정보(T_RESERVE), 거래정보(T_TRAN) 삭제 및 스케쥴별좌석정보(T_SCHEDULE_SEAT_SUM) 변경한다.
					entranceService.ReleaseHoldSeat(params);
				}
			} 
			
			//System.out.println("############[Release Hold Seat: Batch Success End]##########");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("############[Release Hold Seat: Batch Fail End]##########");
		}
	}
}
