package kr.go.sciencecenter.persistenceEnt;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface EntranceMapper {
	public List<HashMap<String,String>> getProgramList(HashMap<String, Object> param);
	
	// 월별 스케쥴 리스트
	public List<HashMap<String,String>> getScheduleList(HashMap<String, Object> param);
	
	// 일별 회차 리스트
	public List<HashMap<String,String>> getSeqList(HashMap<String, Object> param);
	
	// 회차별 권종 리스트
	public List<HashMap<String,String>> getPriceTypeList(HashMap<String, Object> param);

	
	int getPaymentListCount(Map<String,Object> params)throws Exception;
	
	public List<HashMap<String,String>> getPaymentList(Map<String, Object> param);
	
	
	public HashMap<String,Object> getReserveCard(Map<String, Object> param);
	
	//예약취소
	public void delReserve(Map<String, Object> param);
	
	//예약 취소/결제 취소시 좌석수 계산 
	public void minusSeatsum(Map<String, Object> param);

	//결제 완료 시 좌석수 계산
	public void plusSeatsum(Map<String, Object> param);
	
	
	/**
	 * 거래 일련 번호 채번
	 * @param param
	 * @return
	 */
	public HashMap<String,String> getMaxTranNbr(HashMap<String, Object> param);
	
	/**
	 * 예매번호 채번
	 * @param param
	 * @return
	 */
	public HashMap<String,String> getRsvNbr(HashMap<String, Object> param);
	
	
	public HashMap<String,String> getInfUniqueNbr(HashMap<String, Object> param);

	// 예약  
	int regTran(HashMap<String, Object> param);
	int regReserve(HashMap<String, Object> param);
	int regPaymentTicket(HashMap<String, Object> param);
	int regCardApprove(HashMap<String, Object> param);

	// 취소 
	void regTran_cancel(HashMap<String, Object> param);
	void regReserve_cancel(HashMap<String, Object> param);
	void regPaymentTicket_cancel(HashMap<String, Object> param);
	void regPaymentTicket_cancel_update(HashMap<String, Object> param);
	void updatePaymentResult_cancel(HashMap<String, Object> param);
	void regCardApprove_cancel(HashMap<String, Object> param);

	void regCMS(HashMap<String, Object> param);
	
	public HashMap<String,String> getTran(HashMap<String, Object> param);
	public HashMap<String, Object> getReserve(HashMap<String, Object> param);

	
	//  결제 완료 콜백 후 처리 
	public int postPayProc(Map<String, Object> param);
	
	void print(HashMap<String, Object> param);
	
	public HashMap<String, Object> getLastPaymentResult(HashMap<String, Object> param);


	public int regPaymentResult(HashMap<String, Object> param);
	public int updatePaymentResult(HashMap<String, Object> param);

	public HashMap<String,String> getPaymentTicketCount(HashMap<String, Object> param);
	
	public Map<String,Object> getSmsData(String oid);

	//예약 취소 시 거래 고유번호 기준으로 좌석 수 계산하여 계산 : 2016.07.06
	public void minusSeatsum2(Map<String, Object> param);
	
	//좌석 HOLD 시 DB상의 현재 좌석 후 조회 후 체크 : 2016.07.06
	public HashMap<String,String> getScheduleSeatCnt(HashMap<String, Object> param);
	
	//좌석 HOLD 이후 10분동안 결제 진행이 없는 예매번호 리스트 : 2016.07.06
	public List<HashMap<String,String>> getHoldSeatList(HashMap<String, Object> param);

	//결제 진행 전 진행하는 예매번호가 존재하는지 체크 : 2016.07.06
	public HashMap<String,String> getUniqueNbrExists(HashMap<String, Object> param);
	
	//결제 완료 후 거래정보 및 예매정보에 입금 구분 변경( 0 -> 1) : 2016.07.06
	public void updateReserveTran(Map<String, Object> param);
}
