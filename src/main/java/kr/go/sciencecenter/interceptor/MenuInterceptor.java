package kr.go.sciencecenter.interceptor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

import kr.go.sciencecenter.model.BreadCrumb;
import kr.go.sciencecenter.model.MainEvent;
import kr.go.sciencecenter.model.MainEventSeries;
import kr.go.sciencecenter.service.MainEventSeriesService;
import kr.go.sciencecenter.service.MainEventService;

@Configuration
@PropertySource("classpath:kr/go/sciencecenter/i18n/message_kr.properties")
public class MenuInterceptor  implements HandlerInterceptor{
	private static final Logger logger = LoggerFactory.getLogger(MenuInterceptor.class);

	@Autowired
	private Environment env;
	
	@Autowired
	private MainEventService mainEventService;
	
	@Autowired
	MainEventSeriesService mainEventSeriesService;
	
	private boolean isResourceHandler(Object handler){
        return handler instanceof ResourceHttpRequestHandler;
    }
	
	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		if (!isResourceHandler(handler)){
			
			// get the current request URI
			String requestURI = request.getRequestURI();
			// get the current path
			String currentPath = requestURI.substring(request.getContextPath().length());
			
			logger.error("============current path : " + currentPath);
			BreadCrumb crumb = new BreadCrumb();
			
			String subCategoryUrl;
			String subChildUrl;
			String hiddenChild;
			String hiddenChildUrl;
			boolean menuFound = false;
			
			// searching depth-1 menu
			for( int i = 1; i <= 8; i++ ) {
				crumb.setMainCategoryNumber(i); // set main category (depth 1)
				// searching depth-2 menu
				for( int j = 1; j <= 8; j++ ) {
					crumb.setSubCategoryNumber(j);	// set sub category (depth 2)
					
					subCategoryUrl = env.getProperty(crumb.getSubCategoryUrl());
					if(  subCategoryUrl!= null && !subCategoryUrl.equals("#") ) {
						// find a match from sub category menus
						// ONLY IF there is no sub child menu.
						subChildUrl = env.getProperty(crumb.getSubCategory()+".sub1");
						if( subChildUrl == null && 
							(	subCategoryUrl.startsWith(currentPath) || currentPath.startsWith(subCategoryUrl))
							) {
							logger.error("subCategoryUrl : " + subCategoryUrl);
							menuFound = true;
							request.setAttribute("crumb", crumb);
							break;								
						}
					}
					// searching depth-3 menu
					for( int k = 1; k <= 9; k++ ) {
						logger.error("searching sub child");
						crumb.setSubChildNumber(k);
						subChildUrl = env.getProperty(crumb.getSubChildUrl());
						if( subChildUrl != null && !subChildUrl.equals("#") ) {
							
							
							// 현재 request 들어온 URL 과 매치되는 메뉴 URL 을 찾으면,
							if(subChildUrl.startsWith(currentPath) || currentPath.startsWith(subChildUrl)) {
								logger.error("subChildUrl : " + subChildUrl);
								menuFound = true;
								request.setAttribute("crumb", crumb);
								break;
							}
							
							// searching extra hidden url.
							// GNB에는 나타나지 않지만, 특정 페이지 내의 tab 으로 갈라지는 url.
							// 이 때에는, GNB 에 노출되는 가장 첫번째 Tab에 해당하는 메뉴로 BreadCrumb init.
							// 예) 메이커랜드 내의 메이커프리마켓, 패밀리창작놀이터, 무한상상실 등
//							hiddenChild = crumb.getSubChild() + ".sub1";
//							if( env.getProperty(hiddenChild) != null) {
//								for( int l = 1; l <= 5; l++ ) {
//									hiddenChild = crumb.getSubChild() + ".sub" + l;
//									hiddenChildUrl = env.getProperty(hiddenChild + ".url");
//									if( hiddenChildUrl != null 
//											&& !hiddenChildUrl.equals("#") 
//											&& currentPath.startsWith(hiddenChildUrl) ) {
//										logger.error("hidden ChildUrl : " + hiddenChildUrl);
//										logger.error("setting subCHild : " + crumb.getSubChild());
//										menuFound = true;
//										request.setAttribute("crumb", crumb);
//										break;
//									}
//								} // for loop depth 4 END ...
//							}
						}
						if( menuFound) {
							break;
						}

					}// for loop depth 3 END ...
					if( menuFound ) {
						break;
					} 
				} // for loop depth 2 END...
				if( menuFound ) {
					break;
				} 
			} // for loop depth 1 END...
		}
		
		return true;
	}

}
