package kr.go.sciencecenter.interceptor;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.go.sciencecenter.controller.admin.AdminController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

import kr.go.sciencecenter.api.AdminApi;
import kr.go.sciencecenter.model.api.Admin;

public class AdminInterceptor implements HandlerInterceptor {
	private static final Logger logger = LoggerFactory.getLogger(AdminInterceptor.class);

	@Autowired
	private ServletContext servletContext;
	
	private boolean isResourceHandler(Object handler){
        return handler instanceof ResourceHttpRequestHandler;
    }
	
	
	@Override
	public boolean preHandle(HttpServletRequest request, 
			HttpServletResponse response, 
			Object handler)throws Exception {
		if (!isResourceHandler(handler)) {
			String currentUserNo = (String)request.getSession().getAttribute(Admin.KEY_FIELD);
			logger.error("CurrentUserNo: " + currentUserNo);
			if( currentUserNo != null) {
				AdminApi.getInstance().access(currentUserNo);
				
				return true;
			}
			// if there is no user session,
			else {
				String contextPath = servletContext.getContextPath();
				response.sendRedirect(contextPath + "/admin/login");
				return false;
			}
		}
		
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		
		
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		
	}
	

	
}
