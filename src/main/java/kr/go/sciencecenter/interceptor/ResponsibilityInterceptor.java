package kr.go.sciencecenter.interceptor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

import kr.go.sciencecenter.model.Responsibility;
import kr.go.sciencecenter.service.ResponsibilityService;

@PropertySource("classpath:/application.properties")
public class ResponsibilityInterceptor  implements HandlerInterceptor{
	private static final Logger logger = LoggerFactory.getLogger(ResponsibilityInterceptor.class);
	
	@Autowired
	private Environment env;
	
	@Autowired
	private ResponsibilityService responsibilityService;
	
	private boolean isResourceHandler(Object handler){
        return handler instanceof ResourceHttpRequestHandler;
    }
	
	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		if (!isResourceHandler(handler)){
			String requestURI = request.getRequestURI();
			
			String environmentMode = env.getProperty("mode");
			
			if(request.getSession().getAttribute("environmentMode") == null){
				request.getSession().setAttribute("environmentMode", environmentMode);
			}
			
			List<Responsibility> responsibilities = responsibilityService.list(requestURI);
			
			request.getSession().setAttribute("responsibilities", responsibilities);
			request.getSession().setAttribute("responsibilityRequestURI", requestURI);
		}
		
		return true;
	}

}
