package kr.go.sciencecenter.interceptor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.go.sciencecenter.model.MainEvent;
import kr.go.sciencecenter.service.MainEventService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

public class MainEventInterceptor implements HandlerInterceptor {
	
	@Autowired
	private Environment env;
	
	@Autowired
	private MainEventService mainEventService;
	
	private boolean isResourceHandler(Object handler){
        return handler instanceof ResourceHttpRequestHandler;
    }
	
	@Override
	public void afterCompletion(HttpServletRequest arg0,
			HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1,
			Object arg2, ModelAndView arg3) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean preHandle(HttpServletRequest request, 
			HttpServletResponse response,
			Object handler) throws Exception {
		if (!isResourceHandler(handler) ) {
			
			request.setAttribute("navMainEvents", mainEventService.listOnlyHasSeries());
		}
		return true;
	}

}
