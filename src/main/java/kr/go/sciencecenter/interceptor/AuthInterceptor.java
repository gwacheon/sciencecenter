package kr.go.sciencecenter.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

import kr.go.sciencecenter.model.api.Member;

public class AuthInterceptor implements HandlerInterceptor{
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private boolean isResourceHandler(Object handler){
        return handler instanceof ResourceHttpRequestHandler;
    }

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		if (!isResourceHandler(handler) ) {
			String memberNo = (String)request.getSession().getAttribute(Member.CURRENT_MEMBER_NO);
			
			if(memberNo != null && !memberNo.equals("")){
				return true;
			}else{
				StringBuffer requestURL = request.getRequestURL();
			    String queryString = request.getQueryString();
			    String lastPath = "/";
			    
			    if(queryString == null){
			    	lastPath = requestURL.toString();
			    } else {
			    	lastPath = requestURL.append('?').append(queryString).toString();
			    }
			    
			    request.getSession().setAttribute("lastPath", lastPath);
			    
				response.sendRedirect(request.getContextPath() + "/users/login");
				return false;
			}
		}
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

}
