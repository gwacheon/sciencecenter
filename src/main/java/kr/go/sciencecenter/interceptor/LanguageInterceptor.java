package kr.go.sciencecenter.interceptor;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

public class LanguageInterceptor implements HandlerInterceptor{

	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		String lang = request.getParameter("lang");
		
		if(lang != null && !lang.isEmpty()){
			Locale locale = new Locale(lang);
			RequestContextUtils.getLocaleResolver(request).setLocale(request, response, locale);
		}else{
			if(request.getSession().getAttribute("lang") != null){
				lang = (String) request.getSession().getAttribute("lang");
			}else{
				lang = "kr";
			}
			
			Locale locale = new Locale(lang);
			RequestContextUtils.getLocaleResolver(request).setLocale(request, response, locale);
		}
		
		request.getSession().setAttribute("lang", lang);
		return true;
	}
	

}
