package kr.go.sciencecenter.interceptor;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

public class DinnerApplyInterceptor implements HandlerInterceptor {

	@Autowired
	private ServletContext servletContext;
	
	private boolean isResourceHandler(Object handler){
        return handler instanceof ResourceHttpRequestHandler;
    }
	
	
	@Override
	public boolean preHandle(HttpServletRequest request, 
			HttpServletResponse response, 
			Object handler)throws Exception {
		if (!isResourceHandler(handler)) {
			String dinnerApplySession = (String)request.getSession().getAttribute("dinnerApplySession");
			if( dinnerApplySession == null || !dinnerApplySession.equals("loggedIn") ) {
				String contextPath = servletContext.getContextPath();
				response.sendRedirect(contextPath + "/dinnerApply/login");
				return false;
			}
			
		}
		
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		
		
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		
	}
	

	
}
