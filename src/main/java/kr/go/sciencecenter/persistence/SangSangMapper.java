package kr.go.sciencecenter.persistence;

import kr.go.sciencecenter.model.SangSangSchedule;
import kr.go.sciencecenter.model.SangSangScheduleTitle;

public interface SangSangMapper {

	SangSangScheduleTitle findWithSchedules();

	void update(SangSangScheduleTitle sangSangScheduleTitle);

	void createSchedule(SangSangSchedule schedule);

	void removeSchedules();

}
