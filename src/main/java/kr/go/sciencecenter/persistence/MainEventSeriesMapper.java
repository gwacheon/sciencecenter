package kr.go.sciencecenter.persistence;

import kr.go.sciencecenter.model.MainEventSeries;

public interface MainEventSeriesMapper {

	public void create(MainEventSeries mainEventSeries);

	public MainEventSeries find(int id);

	public MainEventSeries findRecentByMainEventId(int mainEventId);

	public void update(MainEventSeries mainEventSeries);

	public void delete(int mainEventSeriesId);

	public void deleteByMainEventId(int mainEventId);
}
