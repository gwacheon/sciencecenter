package kr.go.sciencecenter.persistence;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import kr.go.sciencecenter.model.Event;
import kr.go.sciencecenter.model.Page;

public interface EventsMapper {

    void create(Event event);

    List<Event> list(@Param("type") String type, @Param("status") Object status, @Param("page") Page page);
    
    List<Event> listByMultipleTypes(@Param("typeList")List<String> typeList, @Param("status")boolean status, @Param("page")Page page);
    
    Event prevEvent(@Param("id")int id,@Param("type") String type);
    Event prevEventByMultipleTypes(@Param("id")int id,@Param("typeList") List<String> typeList);

    Event find(int id);
    
    Event nextEvent(@Param("id")int id,@Param("type") String type);
    Event nextEventByMultipleTypes(@Param("id")int id,@Param("typeList") List<String> typeList);
    
    void update(Event event);
    
    int count(@Param("type") String type, @Param("status") Object status);
    
    int countByMultipleTypes(@Param("typeList")List<String> typeList, @Param("status") boolean status);

    public void delete(@Param("id")int id);

	

}