package kr.go.sciencecenter.persistence;

import java.util.List;
import java.util.Map;

public interface FamilyModifyMapper {

	/**
	 * 가족 업데이트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	int UPDATE_MEMBER_FAMILY(Map<String,Object> params)throws Exception;
	
	/**
	 * 가족 등록
	 * @param params
	 * @return
	 * @throws Exception
	 */
	int INSERT_MEMBER_FAMILY(Map<String,Object> params)throws Exception;
	
	/**
	 * 가족 확인
	 * @param params
	 * @return
	 * @throws Exception
	 */
	List SELECT_MEMBER_FAMILY_CHECK(Map<String,Object> params)throws Exception;
	
	/**
	 * 가족외 회원 업데이트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	int UPDATE_MEMBER_FRIEND(Map<String,Object> params)throws Exception;
	
	/**
	 * 가족외 회원 등록
	 * @param params
	 * @return
	 * @throws Exception
	 */
	int INSERT_MEMBER_FRIEND(Map<String,Object> params)throws Exception;
	
	/**
	 * 가족외 회원 체크
	 * @param params
	 * @return
	 * @throws Exception
	 */
	List SELECT_MEMBER_FRIEND_CHECK(Map<String,Object> params)throws Exception;
	
	/**
	 * 로그 등록
	 * @param params
	 * @return
	 * @throws Exception
	 */
	int INSERT_MEMBER_FAMILY_LOG(Map<String,Object> params)throws Exception;
	
	/**
	 * 멤버 등급 등록
	 * @param params
	 * @return
	 * @throws Exception
	 */
	int INSERT_MEMBERSHIP_FAMILY(Map<String,Object> params)throws Exception;
}
