package kr.go.sciencecenter.persistence;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.model.api.Member;

public interface MembershipLogMapper {

	List<Member> searchMembership(String name);

	void createLog(@Param("memberNo") String memberNo, @Param("name") String name);
	
	List<Member> statistics(@Param("page") Page page);

	List<Member> logByMemberNo(@Param("memberNo") String memberNo
			, @Param("page") Page page);

	int staticCount();

	int countByMember(String memberNo);
}
