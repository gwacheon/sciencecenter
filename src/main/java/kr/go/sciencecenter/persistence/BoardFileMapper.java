package kr.go.sciencecenter.persistence;

import java.util.List;

import kr.go.sciencecenter.model.BoardFile;

import org.apache.ibatis.annotations.Param;

public interface BoardFileMapper {
    void createAll(List<BoardFile> files);
    void create(BoardFile file);
    void update(BoardFile f);
	void deleteFile(@Param("fileId")int id);
	BoardFile find(@Param("fileId") int id);
	void deleteFiles(@Param("boardId")int id);
}
