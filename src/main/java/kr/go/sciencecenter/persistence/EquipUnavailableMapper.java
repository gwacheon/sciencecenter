package kr.go.sciencecenter.persistence;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import kr.go.sciencecenter.model.EquipUnavailable;

public interface EquipUnavailableMapper {
    void bulkInsert(List<EquipUnavailable> EquipUnavailable);

    List<EquipUnavailable> findByIdAndSerial(
	    @Param("equipmentId")int equipmentId, 
	    @Param("equiSerialId") int equiSerialId);

}
