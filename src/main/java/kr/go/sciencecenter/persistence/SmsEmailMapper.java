package kr.go.sciencecenter.persistence;

import java.util.Map;

public interface SmsEmailMapper {
	@SuppressWarnings("rawtypes")
	int insertSmsHistory(Map map);

	@SuppressWarnings("rawtypes")
	int insertEmailHistory(Map map);
	
	@SuppressWarnings("rawtypes")
	Map selectContentsLayout(Map map);
}
