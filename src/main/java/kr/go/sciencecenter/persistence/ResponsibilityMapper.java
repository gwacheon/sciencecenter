package kr.go.sciencecenter.persistence;

import java.util.List;

import kr.go.sciencecenter.model.Responsibility;

public interface ResponsibilityMapper {

	public Responsibility find(String requestURI);

	public void create(Responsibility responsibility);

	public void update(Responsibility responsibility);

	public List<Responsibility> list(String requestURI);

	public Responsibility findByResponsibility(Responsibility responsibility);

}
