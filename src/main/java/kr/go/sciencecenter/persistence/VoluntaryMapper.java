package kr.go.sciencecenter.persistence;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.model.User;
import kr.go.sciencecenter.model.Voluntary;
import kr.go.sciencecenter.model.VoluntaryTime;
import kr.go.sciencecenter.model.Volunteer;

public interface VoluntaryMapper {

	List<Voluntary> list();

	void create(Voluntary voluntary);

	Voluntary find(int id);
	Voluntary findWithTimes(int id);

	void createTime(VoluntaryTime voluntaryTime);

	int deleteTime(int id);

	void createVolunteer(Volunteer volunteer);

	Voluntary findTimesWithVolunteerCount(@Param("id") int timeId);
	
	List<VoluntaryTime> findTimesAndVolunteerCount(@Param("id") int timeId, 
			@Param("showBefore") boolean showBefore,
			@Param("order") String order);

	VoluntaryTime findTime(int timeId);

	Volunteer findAvailVolunteer(int timeId);
	
	Volunteer findVolunteerWithTimeAndMember(@Param("timeId") int timeId, @Param("memberNo") String memberNo);

	int pendingVolunteer(Volunteer volunteer);

	Volunteer findVolunteer(int volunteerId);

	void updateVolunteerInfo(Volunteer volunteer);

	VoluntaryTime findTimeWithVolunteers(int id);

	void updateVolunteerStatus(Volunteer volunteer);
	
	List<Volunteer> volunteersByStatus(@Param("timeId") int timeId, @Param("status")  String status);

	int volunteersCount(int timeId);

	List<Volunteer> findVolunteers(int timeId);

	void acceptVolunteer(Volunteer volunteer);

	List<Volunteer> myVolunteers(@Param("currentMemberNo") String currentMemberNo, @Param("page") Page page);

	int myVolunteersCount(String currentMemberNo);

	Voluntary findByTimeId(int timeId);

	void update(Voluntary voluntary);

	void delete(int id);
}
