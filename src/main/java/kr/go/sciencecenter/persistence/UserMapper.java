package kr.go.sciencecenter.persistence;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import kr.go.sciencecenter.model.User;

public interface UserMapper {
	
	public void create(User user);
	public User findByEmail(String email);
	public User findByEmailWithRoles(String email);
	public User findByIdAndAuthenticationToken(@Param("id") int id,@Param("authenticationToken")  String authenticationToken);
	public void auth(User user);
	public List<User> list();
	public void delete(@Param("id") int id);
	public void updateLastLoginDateById(@Param("id") int id);
	public String findDuplicateKey(@Param("id") String id,
			@Param("email") String email,
			@Param("name") String name);
	
	/**
	 * 사용자 정보 MEMBER_NO를 이용해서 회원정보를 가져옴. API에 정의되어 있으나 여기서도 정의.(20160502 일 기준으로 회원정보 API와 같은 값을 가져옴)
	 * @param map
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Map getLegacyUser(Map map);
	@SuppressWarnings("rawtypes")
	public Map getMembershipPrice(Map map);
	
	/**
	 * 모든 회원등급을 리스트함(후원회원 포함)
	 * @param map
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public List getMembershipPriceList();
	
	/**
	 * MEMBER_NO 를 통해 사용자가 현재 등록되어 있는 사용자등급을 리스트함.
	 * @param map
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public List getMembershipList(Map map);
	
	
	public int findDuplicateCode(@Param("companyCd")String companyCd, @Param("duplicateKey")String duplicateKey);
}
