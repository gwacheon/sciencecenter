package kr.go.sciencecenter.persistence;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import kr.go.sciencecenter.model.DinnerApply;
import kr.go.sciencecenter.model.Page;

public interface DinnerApplyMapper {
	
	public int create(DinnerApply dinnerApply);
	List<DinnerApply> list(@Param("page") Page page);
	public int count(@Param("todayDate") Date todayDate);
	public void delete(int id);
	public DinnerApply findDuplicates(DinnerApply dinnerApply);
	public List<DinnerApply> listAllHistories(@Param("page") Page page);
	
}
