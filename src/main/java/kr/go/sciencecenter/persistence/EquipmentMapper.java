package kr.go.sciencecenter.persistence;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import kr.go.sciencecenter.model.EquipSerial;
import kr.go.sciencecenter.model.Equipment;

public interface EquipmentMapper {

    List<Equipment> list();

    void create(Equipment equipment);

    void createSerials(Equipment equipment);

    List<EquipSerial> serials(int equipmentId);

    EquipSerial findSerial(int id);

    List<Equipment> listByType(@Param("type")String type);
    
    Equipment find(int id);

    void update(Equipment equipment);
}
