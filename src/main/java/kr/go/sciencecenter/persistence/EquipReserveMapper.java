package kr.go.sciencecenter.persistence;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import kr.go.sciencecenter.model.EquipReserve;

public interface EquipReserveMapper {

	void create(EquipReserve equipReserve);

	List<EquipReserve> list(@Param("equiSerialId") int equiSerialId, @Param("dateStr") String dateStr);

	void bulkInsert(@Param("reserves") List<EquipReserve> equipReserves);

	List<EquipReserve> getReservationsInMonth(@Param("equipmentId") int equipmentId, 
			@Param("beginDate") String beginDate, @Param("endDate") String endDate);

	List<EquipReserve> getReservationsPerMonth(
		@Param("equiSerialId") int equiSerialId,
		@Param("beginDate") String beginDate, 
		@Param("endDate") String endDate);

	EquipReserve list(int serialId);

	List<EquipReserve> getReserves(@Param("memberNo")String memberNo);

	void changeStatus(
			@Param("id") int id,
			@Param("status") String status);

}
