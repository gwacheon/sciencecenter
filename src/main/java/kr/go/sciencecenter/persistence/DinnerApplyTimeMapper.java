package kr.go.sciencecenter.persistence;

import kr.go.sciencecenter.model.DinnerApplyTime;

public interface DinnerApplyTimeMapper {
	
	public int create(DinnerApplyTime dinnerApplyTime);
	public DinnerApplyTime findApplicationTimeMostRecent();
	
}
