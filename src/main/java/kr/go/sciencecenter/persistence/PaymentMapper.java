package kr.go.sciencecenter.persistence;

import java.util.List;
import java.util.Map;

public interface PaymentMapper {
	
	/**
	 * 프로그램 리스트
	 * @param params
	 * @throws Exception
	 */
	List getPaymentList(Map<String,Object> params)throws Exception;
	
	/**
	 * 프로그램 리스트 카운트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	int getPaymentListCount(Map<String,Object> params)throws Exception;
	
	/**
	 * 예약증 업데이트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	int getPrintFlagUpdate(Map<String,Object> params)throws Exception;
	
	/**
	 * 예약증 프린트 확인 유무
	 * @param params
	 * @return
	 * @throws Exception
	 */
	List getPrintFlag(Map<String,Object> params)throws Exception;
	
	/**
	 * 0원 환불
	 * @param params
	 * @return
	 * @throws Exception
	 */
	int getUpdateCourseZero(Map<String,Object> params)throws Exception;
	
	/**
	 * 가상계좌 은행 리스트
	 * @return
	 * @throws Exception
	 */
	List getVatList()throws Exception;
	
	/**
	 * 거래정보
	 * @param param
	 * @return
	 * @throws Exception
	 */
	List getTranPaymentList(Map<String,Object> param)throws Exception;
	
	/**
	 * 영수증
	 * @param param
	 * @return
	 * @throws Exception
	 */
	List getReceiptInfo(Map<String,Object> param)throws Exception;
	
	/**
	 * 은행 리스트
	 * @return
	 * @throws Exception
	 */
	List getBankList()throws Exception;
	
	/**
	 * 결제 데이터
	 * @param params
	 * @return
	 * @throws Exception
	 */
	List PAYMENT_DATA(Map<String,Object> params)throws Exception;
	
	/**
	 * 수강생 인원 정보
	 * @param params
	 * @return
	 * @throws Exception
	 */
	List MemberInfoList(Map<String,Object> params)throws Exception;

	/**
	 * 파라미터 내용 테이블 저장 로그용 (VACALLBACK)
	 * @param params
	 * @return
	 */
	int insertVaCallBack(Map<String, Object> params);

	/**
	 * STOREID, ORDNO, ACCOUNTNO, RCPTNAME, AMT 을 비교하여 강좌내용에 있는지를 확인해서 UPDATE 처리
	 * @param params
	 * @return
	 */
	int updateLectureVaCallBack(Map<String, Object> params);
	
	/**
	 * 가상계좌 입금
	 * @param params
	 * @return
	 */
	int updateVAPament(Map<String, Object> params);
	
	/**
	 * 가상계좌 입금
	 * @param params
	 * @return
	 */
	int updateVAPament2(Map<String, Object> params);

	/**
	 * 강좌 결제내역이 있는지 확인
	 * @param map
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	int countTA_TRAN_PAYMENT(Map map);

	int insertTM_MEMBERSHIP(Map map);

	/**
	 * 강좌 수료증 발급
	 * @param map
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	Map getCompleteLecture(Map map);

}
