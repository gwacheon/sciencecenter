package kr.go.sciencecenter.persistence;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import kr.go.sciencecenter.model.MainEvent;

public interface MainEventMapper {

    void create(MainEvent mainEvent);

    List<MainEvent> listWithSeries(@Param("type")String type);
    
    List<MainEvent> list();
    List<MainEvent> listOnlyHasSeries();
    List<MainEvent> listByType(@Param("type")String type);
    
    void update(MainEvent mainEvent);

	MainEvent find(int id);

	MainEvent findWithRecentSeries(int mainEventId);

	void delete(int id);

}
