package kr.go.sciencecenter.persistence;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import kr.go.sciencecenter.model.EventFile;

public interface EventFileMapper {
    void createAll(List<EventFile> files);
    void create(EventFile file);
    void update(EventFile f);
	void deleteFile(@Param("fileId")int id);
	EventFile find(@Param("fileId") int id);
	void deleteFiles(@Param("eventId")int id);
}
