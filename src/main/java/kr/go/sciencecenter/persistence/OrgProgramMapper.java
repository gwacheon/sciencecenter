package kr.go.sciencecenter.persistence;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import kr.go.sciencecenter.model.OrgApplicant;
import kr.go.sciencecenter.model.OrgProgram;
import kr.go.sciencecenter.model.Page;

public interface OrgProgramMapper {

	List<OrgProgram> list();

	void create(OrgProgram orgProgram);

	OrgProgram find(int id);

	List<OrgApplicant> applicants(@Param("id") int id
			, @Param("searchApplicant") OrgApplicant searchOrgApplicant
			, @Param("page") Page page
			, @Param("dateStr") String dateStr);
	
	int applicantsCount(@Param("id") int orgProgramId
			, @Param("searchApplicant") OrgApplicant searchOrgApplicant
			, @Param("dateStr") String dateStr);

	void createApplicant(OrgApplicant orgApplicant);

	OrgApplicant findByApplicant(OrgApplicant orgApplicant);

	List<OrgApplicant> myApplicants(@Param("memberNo") String memberNo
			,@Param("page") Page page);

	int myApplicantsCount(String memberNo);
	
	void update(OrgProgram orgProgram);

	List<OrgProgram> adminList();
}
