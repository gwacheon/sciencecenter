package kr.go.sciencecenter.persistence;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import kr.go.sciencecenter.model.Holiday;
import kr.go.sciencecenter.model.Page;

public interface HolidayMapper {
	public List<Holiday> list(@Param("page") Page page);
	public int create(Holiday holyday);
	public void search(int year, int month);
	public int count();
	public void delete(int id);
}
