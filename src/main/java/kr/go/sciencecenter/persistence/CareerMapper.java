package kr.go.sciencecenter.persistence;

import java.util.List;

import kr.go.sciencecenter.model.Career;
import kr.go.sciencecenter.model.CareerSchedule;

public interface CareerMapper {

	List<Career> list();

	int maxSeq();

	void create(Career career);

	Career find(int id);

	void createSchedule(CareerSchedule schedule);

	void removeSchedules(int id);

	void delete(int id);

	void update(Career career);

}
