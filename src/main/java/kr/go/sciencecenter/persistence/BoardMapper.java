package kr.go.sciencecenter.persistence;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import kr.go.sciencecenter.model.Board;
import kr.go.sciencecenter.model.BoardType;
import kr.go.sciencecenter.model.Page;

public interface BoardMapper {

	public int create(Board board);  
	public int count(
			@Param("boardType")String boardType, 
			@Param("searchType")String searchType, 
			@Param("searchKey")String searchKey);
	public int countFaqTotal(
			@Param("faqTypes")List<String> faqTypes, 
			@Param("searchType")String searchType, 
			@Param("searchKey")String searchKey);
	public int countEventSeriesBoards(
			@Param("boardType")String boardType, 
			@Param("eventSeriesId")Integer eventSeriesId,
			@Param("searchType")String searchType, 
			@Param("searchKey")String searchKey);
	List<Board> list(@Param("boardType") String boardType,
			@Param("page") Page page, 
			@Param("searchType") String searchType,
			@Param("searchKey")  String searchKey,
			@Param("lang")  String lang);
	List<Board> listFaqTotal(@Param("faqTypes")List<String> faqTypes,
			@Param("page")Page page, 
			@Param("searchType")String searchType, 
			@Param("searchKey")String searchKey,
			@Param("lang")String lang);
	public List<Board> listEventSeriesBoards(
			@Param("boardType")String boardType,
			@Param("eventSeriesId")Integer eventSeriesId, 
			@Param("page")Page page, 
			@Param("searchType")String searchType,
			@Param("searchKey")String searchKey, 
			@Param("lang")String lang);
	public Board find(int id);
	public void update(Board board);
	public void delete(int id);
	public void deleteReplies(int boardId);
	public Board getPrevBoard(@Param("id")int id,@Param("type") String boardType);  
	public Board getNextBoard(@Param("id")int id,@Param("type") String boardType);
	public void addCount(Board board);
	public void deleteByMainEventSeriesId(int eventSeriesId);
	public void deleteByMainEventId(int mainEventId);
	public int countByTotalSearch(@Param("searchKey")String searchKey);
	public List<Board> listByTotalSearch(
			@Param("page")Page page, 
			@Param("searchKey")String searchKey);
	
	public List<Board> mainList(@Param("boardType") String boardType, 
			@Param("page") Page page, 
			@Param("searchType") String searchType, 
			@Param("lang") String lang);
	
	public int countFixedOnTop(@Param("boardType")String type);
	public List<Board> listFixedOnTop(@Param("boardType")String type);
	


}
