package kr.go.sciencecenter.persistence;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import kr.go.sciencecenter.model.Article;
import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.model.Reply;
import kr.go.sciencecenter.model.ReplyDetail;
import kr.go.sciencecenter.model.Selection;
import kr.go.sciencecenter.model.Survey;

public interface SurveyMapper {

	List<Survey> list(@Param("page")Page page);
	
	void create(Survey survey);

	Survey find(int id);

	void createArticle(Article article);

	void createSelection(Selection selection);

	Survey findWithArticles(int id);

	void update(Survey survey);

	void deleteArticle(int articleId);

	void createReply(Reply reply);

	void createReplyDetail(ReplyDetail replyDetail);

	List<Reply> replies(@Param("surveyId") int surveyId, @Param("page") Page page);
	
	List<Reply> repliesWithDetails(@Param("surveyId") int surveyId, @Param("page") Page page);

	void updateArticleSeq(@Param("surveyId") int surveyId, @Param("articleId") int articleId, @Param("seq") int seq);

	Reply findReplyWithIpAddress(@Param("surveyId") int id, @Param("remoteIpAddress")  String remoteIpAddress);

	int count(int surveyId);

	void delete(int id);

	int countAll();

}
