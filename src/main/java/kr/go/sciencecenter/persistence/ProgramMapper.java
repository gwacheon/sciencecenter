package kr.go.sciencecenter.persistence;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.model.Program;
import kr.go.sciencecenter.model.ProgramGroup;

public interface ProgramMapper {

	int count(ProgramGroup group);

	List<Program> list(@Param("group") ProgramGroup group, @Param("page") Page page);

	int create(Program program);

	List<Program> listByRange(Date date);

	void delete(int id);

	Program find(int id);

	void update(Program program);

	void createWday(@Param("program") Program program, @Param("wday") int wday);

	void deleteWdays(int programId);

	void addInfo(@Param("program") Program program, @Param("info") String info);

	void deleteAddInfos(int id);
}
