package kr.go.sciencecenter.persistence;

import java.util.List;

import kr.go.sciencecenter.model.SupportEvaluation;

public interface SupportEvaluationMapper {

	public List<SupportEvaluation> list();
	public int create(SupportEvaluation supportEvaluation);
	public SupportEvaluation find(int id);
	public void update(SupportEvaluation supportEvaluation);
	public void delete(int id);

}
