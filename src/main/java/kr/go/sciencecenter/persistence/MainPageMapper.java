package kr.go.sciencecenter.persistence;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import kr.go.sciencecenter.model.MainPageBanner;
import kr.go.sciencecenter.model.Page;

public interface MainPageMapper {

	List<MainPageBanner> listBannersByType(@Param("type") String type, @Param("visibleOnly") Boolean visibleOnly);

	List<MainPageBanner> listBannersVisibleOnly();
	
	void createBanner(MainPageBanner mainPageBanner);

	int getTypeMaxSeq(@Param("type") String type);

	void updateBanner(MainPageBanner mainPageBanner);

	MainPageBanner findBannerById(@Param("id")int id);

	void deleteBanner(@Param("id")int id);

	void setAllNewLetterBannersHidden();
	
	
}
