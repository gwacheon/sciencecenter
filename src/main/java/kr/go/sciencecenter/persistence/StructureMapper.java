package kr.go.sciencecenter.persistence;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import kr.go.sciencecenter.model.Structure;
import kr.go.sciencecenter.model.StructureUnavailable;

public interface StructureMapper {

	List<Structure> list();

	void create(Structure structure);

	int maxSeq();

	Structure find(int id);

	void update(Structure structure);

	void delete(int id);

	Structure findWithUnavailables(@Param("id") int id, @Param("date") Date today);

	List<StructureUnavailable> listInDates(
			@Param("id") int id, 
			@Param("startDateStr") String startDateStr, 
			@Param("endDateStr") String endDateStr);

	void addUnavailableDates(StructureUnavailable unavailables);

}
