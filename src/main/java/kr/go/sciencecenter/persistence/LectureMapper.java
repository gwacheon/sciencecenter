package kr.go.sciencecenter.persistence;

import java.util.List;
import java.util.Map;

import kr.go.sciencecenter.model.api.ReqLecture;

public interface LectureMapper {

	/**
	 * 아카데미 리스트
	 * @return
	 * @throws Exception
	 */
	List getAcademyList()throws Exception;
	
	/**
	 * 아카데미 클래스 리스트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	List getAcademyClassList(Map<String,Object> params)throws Exception;

	
	/**
	 * 클래스 리스트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	List getClassList(Map<String,Object> params)throws Exception;
	
	/**
	 * 교육형 리스트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	List getPeriodList(Map<String,Object> params)throws Exception;
	
	/**
	 * 날짜 / 회차 리스트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	List getPriceList(Map<String,Object> params)throws Exception;
	
	/**
	 * 날짜 / 회차 리스트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	List getPriceDayList(Map<String,Object> params)throws Exception;
	
	/**
	 * 가족 리스트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	List getFamilyList(Map<String,Object> params)throws Exception;
	
	/**
	 * 가족 리스트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	List getFriendList(Map<String,Object> params)throws Exception;
	
	/**
	 * 회원 등급 리스트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	List getMemberGradeList(Map<String,Object> params)throws Exception;
	
	/**
	 * 회원별 강좌 등급 리스트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	List getCourseGradeList(Map<String,Object> params)throws Exception;
	
	/**
	 * 회원 이름
	 * @param params
	 * @return
	 * @throws Exception
	 */
	List getMemberName(Map<String,Object> params)throws Exception;
	
	/**
	 * 신청 대상 리스트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	List getTargetList(Map<String,Object> params)throws Exception;
	
	/**
	 * 예약증
	 * @param params
	 * @return
	 * @throws Exception
	 */
	List getCompleteInfo(Map<String,Object> params)throws Exception;
	
	/**
	 * 강좌별 요금 리스트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	List getCoursePriceList(Map<String,Object> params)throws Exception;
	
	/**
	 * 강좌별 요금 리스트2
	 * @param params
	 * @return
	 * @throws Exception
	 */
	List getCoursePriceList2(Map<String,Object> params)throws Exception;
	
	/**
	 * 클래스에 따른 가맹점 정보
	 * @param params
	 * @return
	 * @throws Exception
	 */
	Map getMbrInfo(String classCd)throws Exception;
	
	/**
	 * 강좌 리스트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	List getLectureList(Map<String,Object> params)throws Exception;
	
	/**
	 * 강좌 리스트 카운트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	int getLectureListCount(Map<String,Object> params)throws Exception;
}
