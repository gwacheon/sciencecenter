package kr.go.sciencecenter.persistence;

import org.apache.ibatis.annotations.Param;

import kr.go.sciencecenter.model.Program;
import kr.go.sciencecenter.model.ProgramTime;

public interface ProgramTimeMapper {
	public void create(@Param("program") Program program, @Param("time") ProgramTime time);

	public void deleteByProgram(Program program);
}
