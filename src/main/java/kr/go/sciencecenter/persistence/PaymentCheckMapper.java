package kr.go.sciencecenter.persistence;

import java.util.List;
import java.util.Map;

public interface PaymentCheckMapper {
	
	/**
	 * 강좌 체크
	 * @param params
	 * @return
	 * @throws Exception
	 */
	List SELECT_COURSE_CHECK(Map<String,Object> params)throws Exception;
	
	/**
	 * 멤버 체크
	 * @param params
	 * @return
	 * @throws Exception
	 */
	List SELECT_MEMBER_CHECK(Map<String,Object> params)throws Exception;
	
	/**
	 * 강좌 신청자 수 / 총금액
	 * @param params
	 * @return
	 * @throws Exception
	 */
	List SELECT_COURSE_APPLY_PRICE_CHECK(Map<String,Object> params)throws Exception;
	
	/**
	 * 강좌 신청자 등록
	 * @param params
	 * @return
	 * @throws Exception
	 */
	int INSERT_COURSE_APPLY(Map<String,Object> params)throws Exception;
	
	/**
	 * 강좌 신청 가격 등록
	 * @param params
	 * @return
	 * @throws Exception
	 */
	int INSERT_COURSE_APPLY_PRICE(Map<String,Object> params)throws Exception;

	/**
	 * 강좌 신청자 체크
	 * @param params
	 * @return
	 * @throws Exception
	 */
	List SELECT_COURSE_APPLY_CHECK(Map<String,Object> params)throws Exception;
	
	/**
	 * 회사별 가맹점 번호
	 * @param params
	 * @return
	 * @throws Exception
	 */
	List SELECT_COMPANY_PAYMENT(Map<String,Object> params)throws Exception;
	
	/**
	 * 가상계좌 입금 기간설정
	 * @param params
	 * @return
	 * @throws Exception
	 */
	List getVaPeriod(Map<String,Object> params)throws Exception;
	
	/**
	 * 가상계좌 등록
	 * @param params
	 * @return
	 * @throws Exception
	 */
	int INSERT_VIRTUAL_ACCOUNT(Map<String,Object> params)throws Exception;
	
	/**
	 * 결제 정보
	 * @param params
	 * @return
	 * @throws Exception
	 */
	List SELECT_TRAN_PAYMENT(Map<String,Object> params)throws Exception;
	
	/**
	 * 거래 테이블 입력
	 * @param params
	 * @return
	 * @throws Exception
	 */
	int INSERT_TRAN_PAYMENT_APPROVE(Map<String,Object> params)throws Exception;
		
	/**
	 * 승인 테이블 등록
	 * @param params
	 * @return
	 * @throws Exception
	 */
	int INSERT_APPROVE(Map<String,Object> params)throws Exception;
	
	/**
	 * 승인 테이블 업데이트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	int UPDATE_APPROVE(Map<String,Object> params)throws Exception;
	
	/**
	 * 회원 정보
	 * @param params
	 * @return
	 * @throws Exception
	 */
	List SELECT_MEMBER(Map<String,Object> params)throws Exception;
	
	/**
	 * 강좌 신청 결제 업데이트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	int UPDATE_COURSE_APPLY_PAY(Map<String,Object> params)throws Exception;
	
	/**
	 * Discount 정보
	 * @param params
	 * @return
	 * @throws Exception
	 */
	List SELECT_COURSE_DISCOUNT_CHECK(Map<String,Object> params)throws Exception;
	
	/**
	 * 예약자 감소
	 * @param params
	 * @return
	 * @throws Exception
	 */
	Map<String,Object> SET_COURSE_RESER(Map<String,Object> params)throws Exception;
	
	/**
	 * 대기자 감소
	 * @param params
	 * @return
	 * @throws Exception
	 */
	Map<String,Object> SET_COURSE_WAIT(Map<String,Object> params)throws Exception;
	
	/**
	 * 인덱스 생성
	 * @param params
	 * @return
	 * @throws Exception
	 */
	Map<String,Object> GET_INDEX(Map<String,Object> params)throws Exception;
	
	/**
	 * 승인 취소
	 * @param params
	 * @return
	 * @throws Exception
	 */
	int INSERT_APPROVE_CANCEL(Map<String,Object> params)throws Exception;
	
	/**
	 * 거래 정보 입력
	 * @param params
	 * @return
	 * @throws Exception
	 */
	int INSERT_TRAN_PAYMENT(Map<String,Object> params)throws Exception;
	
	/**
	 * 거래 정보 업데이트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	int UPDATE_TRAN_PAYMENT_PROCESS(Map<String,Object> params)throws Exception;
	
	/**
	 * 신청자 취소 업데이트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	int UPDATE_COURSE_APPLY_CANCEL(Map<String,Object> params)throws Exception;
	
	/**
	 * Cms 입력
	 * @param params
	 * @return
	 * @throws Exception
	 */
	int INSERT_CMS(Map<String,Object> params)throws Exception;
	
	/**
	 * 환불 상태 업데이트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	int UPDATE_TRAN_PAYMENT_REF_STATUS(Map<String,Object> params)throws Exception;
	
	/**
	 * 수강 신청자 환불 업데이트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	int UPDATE_COURSE_APPLY_REFUND(Map<String,Object> params)throws Exception;

	/**
	 * 결제 완료 후 저장을 위해 예약된 내역을 가져옴
	 * @param string
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	Map get_TL_COURSE_APPLY(String string);

	@SuppressWarnings("rawtypes")
	int isTmMembershipTaTranPayment(Map map);

	int INSERT_COURSE_APPLY_OPTION(Map<String, Object> applyEntity);
}
