package kr.go.sciencecenter.persistence;

import java.util.List;

import kr.go.sciencecenter.model.ProgramTeacher;

public interface ProgramTeacherMapper {

  List<ProgramTeacher> list();

  void create(ProgramTeacher programTeacher);

  void delete(int i);

}
