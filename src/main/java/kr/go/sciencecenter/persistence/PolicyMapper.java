package kr.go.sciencecenter.persistence;

import java.util.List;

import kr.go.sciencecenter.model.Policy;

import org.apache.ibatis.annotations.Param;

public interface PolicyMapper {

	public List<Policy> list();
	
	public List<Policy> listAll();

	public void create(Policy policy);

	public void setPrevContents(@Param("type")String type);
	
	public Policy find(@Param("id")int id);

	public void update(Policy policy);

	public List<Policy> listPrevious(@Param("type")String type);

	public void delete(@Param("id")int id);

	public void setNextRecent(@Param("type")String type);

	public Policy findMostRecentByType(@Param("type")String type);
	
}
