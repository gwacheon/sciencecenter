package kr.go.sciencecenter.persistence;

import org.apache.ibatis.annotations.Param;

import kr.go.sciencecenter.model.Attatchment;

public interface AttatchmentMapper {
  public void create(Attatchment attatchment);
  public void update(@Param("id") int id, @Param("path")String path, @Param("name")String name);
}
