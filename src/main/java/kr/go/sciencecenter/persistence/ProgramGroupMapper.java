package kr.go.sciencecenter.persistence;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.model.ProgramGroup;

public interface ProgramGroupMapper {
	int count();
	List<ProgramGroup> list(@Param("page") Page page);
	int create(ProgramGroup group);
	void delete(int id);
	ProgramGroup find(int id);
	void update(ProgramGroup group);
}
