package kr.go.sciencecenter.persistence;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import kr.go.sciencecenter.model.DinnerApplyUser;
import kr.go.sciencecenter.model.Page;

public interface DinnerApplyUserMapper {
	
	public List<DinnerApplyUser> list(@Param("page")Page page);
	public int count();
	public DinnerApplyUser findByUserId(@Param("userId")String userId);
	public void create(DinnerApplyUser dinnerApplyUser);
	public DinnerApplyUser findById(@Param("id")int id);
	public void update(DinnerApplyUser dinnerApplyUser);
	public void delete(@Param("id")int id);
	
}
