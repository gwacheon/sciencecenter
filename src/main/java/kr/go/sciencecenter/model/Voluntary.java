package kr.go.sciencecenter.model;

import java.util.Date;
import java.util.List;

public class Voluntary {
	int id;
	String title;
	String content;
	Date beginDate;
	Date endDate;
	int maxMember;
	int waitMember;
	int reserveAvailableDay;
	String location;
	Date regDttm;
	Date updtDttm;
	
	List<VoluntaryTime> voluntaryTimes;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public int getMaxMember() {
		return maxMember;
	}
	public void setMaxMember(int maxMember) {
		this.maxMember = maxMember;
	}
	public int getWaitMember() {
		return waitMember;
	}
	public void setWaitMember(int waitMember) {
		this.waitMember = waitMember;
	}
	public int getReserveAvailableDay() {
		return reserveAvailableDay;
	}
	public void setReserveAvailableDay(int reserveAvailableDay) {
		this.reserveAvailableDay = reserveAvailableDay;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Date getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(Date regDttm) {
		this.regDttm = regDttm;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
	
	public List<VoluntaryTime> getVoluntaryTimes() {
		return voluntaryTimes;
	}
	public void setVoluntaryTimes(List<VoluntaryTime> voluntaryTimes) {
		this.voluntaryTimes = voluntaryTimes;
	}
}
