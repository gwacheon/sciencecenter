package kr.go.sciencecenter.model;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.base.Joiner;

import kr.go.sciencecenter.model.api.Member;
import kr.go.sciencecenter.service.BoardService;

public class Board {
	private static final Logger logger = LoggerFactory.getLogger(Board.class);
	
	public static String BOARD_FILE_PREFIX = "BOARD";
	public static String BOARD_MAIN_IMAGE_PREFIX = "BOARD_IMAGES";
	public static String BOARD_MAIN_VIDEO_PREFIX = "BOARD_VIDEOS";
	
	String memberNo;
	int id;
	int typeSeq;
	int boardId;
	int eventSeriesId;
	String title;
	String subTitle;
	String content;
	int count;
	String picture;
	MultipartFile file;
	String linkUrl;
	List<BoardFile> attatchments;
	String building;
	String object;
	
	String boardType;

	Date regDttm;
	Date updtDttm;

	Member member;
	boolean writtenByAdmin;

	String next_title;
	
	String redirectUrl;
	
	List<Board> replies;
	
	MultipartFile video;
	String videoPath;
	
	int isOld;
	
	boolean fixedOnTop;
	
	public String getNext_title() {
		return next_title;
	}

	public void setNext_title(String next_title) {
		this.next_title = next_title;
	}
	
	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public String getMemberNo() {
		return memberNo;
	}

	public void setMemberNo(String memberNo) {
		this.memberNo = memberNo;
	}

	public List<BoardFile> getAttatchments() {
		return attatchments;
	}

	public void setAttatchments(List<BoardFile> attatchments) {
		this.attatchments = attatchments;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTypeSeq() {
		return typeSeq;
	}

	public void setTypeSeq(int typeSeq) {
		this.typeSeq = typeSeq;
	}


	public int getBoardId() {
		return boardId;
	}

	public void setBoardId(int boardId) {
		this.boardId = boardId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getBoardType() {
		return boardType;
	}

	public void setBoardType(String boardType) {
		this.boardType = boardType;
	}

	public Date getRegDttm() {
		return regDttm;
	}

	public void setRegDttm(Date reg_dttm) {
		this.regDttm = reg_dttm;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	public void setUpdtDttm(Date updt_dttm) {
		this.updtDttm = updt_dttm;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public String getLinkUrl() {
		return linkUrl;
	}

	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}

	public String getBuilding() {
		return building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	public String getObject() {
		return object;
	}

	public void setObject(String object) {
		this.object = object;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}
	
	

	public int getEventSeriesId() {
		return eventSeriesId;
	}

	public void setEventSeriesId(int eventSeriesId) {
		this.eventSeriesId = eventSeriesId;
	}
	
	
	public boolean isWrittenByAdmin() {
		return writtenByAdmin;
	}

	public void setWrittenByAdmin(boolean writtenByAdmin) {
		this.writtenByAdmin = writtenByAdmin;
	}
	
	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	public List<Board> getReplies() {
		return replies;
	}

	public void setReplies(List<Board> replies) {
		this.replies = replies;
	}

	public MultipartFile getVideo() {
		return video;
	}

	public void setVideo(MultipartFile video) {
		this.video = video;
	}

	
	public String getVideoPath() {
		return videoPath;
	}

	public void setVideoPath(String videoPath) {
		this.videoPath = videoPath;
	}

	public int getIsOld() {
		return isOld;
	}

	public void setIsOld(int isOld) {
		this.isOld = isOld;
	}
	
	
	public boolean isFixedOnTop() {
		return fixedOnTop;
	}

	public void setFixedOnTop(boolean fixedOnTop) {
		this.fixedOnTop = fixedOnTop;
	}

	/**
	 * 게시판 게시글의 대표이미지 저장
	 * @param filePath
	 * @param contextPath
	 */
	public void saveMainImage(String filePath, String contextPath) {
		String relativePath = "/resources/upload/board_picture/";

		filePath += relativePath + this.getId() + "/";

		File dir = new File(filePath);
		if (dir.exists()) {
			dir.delete();
		}

		dir.mkdirs();

		try {
			this.getFile().transferTo(new File(filePath + this.getFile().getOriginalFilename()));
			
			this.setPicture(contextPath + relativePath + this.getId() + "/" + this.getFile().getOriginalFilename());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 게시글 List 를 가져온다.
	 * @param model
	 * @param boardService
	 * @param type 게시판 Type
	 * @param currentPage
	 * @param searchType
	 * @param searchKey
	 */
	public static void getBoards(Model model, BoardService boardService, String type, Integer currentPage, String searchType, String searchKey){
		
		String requestURI = boardService.getHttpServletRequest().getRequestURI();
		String path = requestURI + "?";
		List<String> parameters = new ArrayList<String>();
		
		if ( searchType != null && searchKey != null ) {
			model.addAttribute("searchType", searchType);
			model.addAttribute("searchKey", searchKey);
			
			parameters.add("searchType=" + searchType);
			parameters.add("searchKey=" + searchKey);
		}
		
		BoardType boardType = BoardType.BOARD_TYPES.get(type);
		model.addAttribute("boardType", boardType);
		
		
		// 게시글 총 카운트
		int boardsCount = boardService.count(type, searchType, searchKey);
		
		// 페이지 객체 설정
		int pagePerCount = 10;
		if(boardType.isGallary()){
			pagePerCount = 9;
		}
		
		Page page = new Page(currentPage, pagePerCount, boardsCount);
		page.setPath(path + Joiner.on("&").join(parameters) + "&");
		model.addAttribute("page", page);
		
		// 게시판 목록 가져오기
		List<Board> boards = boardService.list(type, page, searchType, searchKey, null);
		model.addAttribute("boards", boards);

	}
	
	public static void getMainBoards(Model model, BoardService boardService){
		int pagePerCount = 6;
				
		// 공지사항, FAQ, 의견수렴
		Page page = new Page(1, pagePerCount, boardService.count("notice", null, null));
		
		model.addAttribute("notices", boardService.mainList("notice", page, null, null));
		model.addAttribute("education", boardService.mainList("educationNotice", page, null, null));
		model.addAttribute("report", boardService.mainList("report", page, null, null));
		
		// 현장스케치 & 과학관 영상
		page = new Page(1, 6, boardService.count("sketch", null, null));
		model.addAttribute("sketches", boardService.mainList("sketch", page, null, null));
		model.addAttribute("programIntros", boardService.mainList("programIntro", page, null, null));
		
	}
	
	public static void getMainEventSeriesBoards(
			Model model, 
			BoardService boardService, 
			String type,
			Integer eventSeriesId,
			Integer currentPage, 
			String searchType, 
			String searchKey){
		
		String requestURI = boardService.getHttpServletRequest().getRequestURI();
		String path = requestURI + "?";
		List<String> parameters = new ArrayList<String>();
		
		if ( searchType != null && searchKey != null ) {
			model.addAttribute("searchType", searchType);
			model.addAttribute("searchKey", searchKey);
			
			parameters.add("searchType=" + searchType);
			parameters.add("searchKey=" + searchKey);
		}
		
		BoardType boardType = BoardType.BOARD_TYPES.get(type);
		model.addAttribute("boardType", boardType);

		int pagePerCount = 10;		
		if(boardType.isGallary){
			pagePerCount = 12;
		}
				
		Page page = new Page(currentPage, pagePerCount, boardService.countEventSeriesBoards(type, eventSeriesId, searchType, searchKey));
		
		//page.setPath(path + String.join("&", parameters) + "&");
		model.addAttribute("page", page);
		
		model.addAttribute("boards", boardService.listEventSeriesBoards(type, eventSeriesId, page, searchType, searchKey, null));
		
	}
}
