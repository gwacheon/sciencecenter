package kr.go.sciencecenter.model;

import java.util.Date;

public class OrgProgram {
	public static final String PROGRAM_STATUS_ACTIVE = "active";
	public static final String PROGRAM_STATUS_INACTIVE = "inactive";
	
	int id;
	String title;
	String description;
	int minDay;
	int maxMember;
	boolean used;
	String status;
	Date regDttm;
	Date updtDttm;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getMinDay() {
		return minDay;
	}
	public void setMinDay(int minDay) {
		this.minDay = minDay;
	}
	public int getMaxMember() {
		return maxMember;
	}
	public void setMaxMember(int maxMember) {
		this.maxMember = maxMember;
	}
	public boolean isUsed() {
		return used;
	}
	public void setUsed(boolean used) {
		this.used = used;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(Date regDttm) {
		this.regDttm = regDttm;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
}
