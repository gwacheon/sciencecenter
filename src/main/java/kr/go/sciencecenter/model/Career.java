package kr.go.sciencecenter.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class Career {
	int id;
	String title;
	int seq;
	Date beginDate;
	
	List<CareerSchedule> schedules;
	
	Date regDttm;
	Date updtDttm;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	public Date getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	public List<CareerSchedule> getSchedules() {
		return schedules;
	}
	public void setSchedules(List<CareerSchedule> schedules) {
		this.schedules = schedules;
	}
	public Date getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(Date regDttm) {
		this.regDttm = regDttm;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
	public void addSchedules(String jsonStr) {
		this.schedules = new ArrayList<CareerSchedule>();
		
		JSONArray jsonSchedules = new JSONArray(jsonStr);
		for (int i = 0; i < jsonSchedules.length(); i++) {
		    JSONObject jsonObj = jsonSchedules.getJSONObject(i);
		    schedules.add(new CareerSchedule(this.getId(), jsonObj));
		}
	}	
}
