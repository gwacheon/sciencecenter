
package kr.go.sciencecenter.model;

import java.util.Date;

import org.json.JSONObject;

public class Volunteer {
	public static final String STATUS_PENDING = "pending";
	public static final String STATUS_ACCEPT = "accept";
	public static final String STATUS_REJECT = "reject";
	public static final String STATUS_FAILED = "failed";
	public static final String STATUS_COMPLETION = "completion";
	
	int id;
	int voluntaryId;
	int voluntaryTimeId;
	int userId;
	
	String memberNo;
	
	String name;
	String phone;
	String email;
	String school;
	String grade;
	String groupNo;
	
	String introduction;
	String otherId;
	
	String myNo;
	
	String status;
	boolean waited;
	
	Date regDttm;
	Date updtDttm;
	
	Voluntary voluntary;
	VoluntaryTime voluntaryTime;
	User user;
	
	boolean reserved;
	
	public Volunteer() {
		
	}
	
	public Volunteer(String memberNo, String jsonStr) {
		JSONObject jsonObj = new JSONObject(jsonStr);
		this.memberNo = memberNo;
		this.name = jsonObj.getString("name");
		this.setPhone(jsonObj.getString("phone"));
		this.setEmail(jsonObj.getString("email"));
		this.setSchool(jsonObj.getString("school"));
		this.setGrade(jsonObj.getString("grade"));
		this.setGroupNo(jsonObj.getString("group_no"));
		this.setMyNo(jsonObj.getString("my_no"));
		this.setOtherId(jsonObj.getString("other_id"));
		this.setIntroduction(jsonObj.getString("introduction"));
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getVoluntaryId() {
		return voluntaryId;
	}
	public void setVoluntaryId(int voluntaryId) {
		this.voluntaryId = voluntaryId;
	}
	public int getVoluntaryTimeId() {
		return voluntaryTimeId;
	}
	public void setVoluntaryTimeId(int voluntaryTimeId) {
		this.voluntaryTimeId = voluntaryTimeId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSchool() {
		return school;
	}
	public void setSchool(String school) {
		this.school = school;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getGroupNo() {
		return groupNo;
	}
	public void setGroupNo(String groupNo) {
		this.groupNo = groupNo;
	}
	public String getMemberNo() {
		return memberNo;
	}
	public void setMemberNo(String memberNo) {
		this.memberNo = memberNo;
	}
	public String getIntroduction() {
		return introduction;
	}
	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public boolean isWaited() {
		return waited;
	}
	public void setWaited(boolean waited) {
		this.waited = waited;
	}
	public String getOtherId() {
		return otherId;
	}
	public void setOtherId(String otherId) {
		this.otherId = otherId;
	}
	public Date getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(Date regDttm) {
		this.regDttm = regDttm;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
	public String getMyNo() {
		return myNo;
	}
	public void setMyNo(String myNo) {
		this.myNo = myNo;
	}
	public Voluntary getVoluntary() {
		return voluntary;
	}
	public void setVoluntary(Voluntary voluntary) {
		this.voluntary = voluntary;
	}
	public VoluntaryTime getVoluntaryTime() {
		return voluntaryTime;
	}
	public void setVoluntaryTime(VoluntaryTime voluntaryTime) {
		this.voluntaryTime = voluntaryTime;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public boolean isReserved() {
		return reserved;
	}
	public void setReserved(boolean reserved) {
		this.reserved = reserved;
	}
}
