package kr.go.sciencecenter.model;

import java.util.Date;

public class DinnerApply {
	
	int id;
	String department;
	String name;
	Date regDttm;
	Date updtDttm;

	public DinnerApply() {
		
	}
	
	public DinnerApply(String department, String name) {
		this.department = department;
		this.name = name;
	}
	
	public DinnerApply(String department, String name, Date today) {
		this.department = department;
		this.name = name;
		this.regDttm = today;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getRegDttm() {
		return regDttm;
	}

	public void setRegDttm(Date regDttm) {
		this.regDttm = regDttm;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}	
	
}
