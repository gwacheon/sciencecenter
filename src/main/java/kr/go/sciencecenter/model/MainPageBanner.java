package kr.go.sciencecenter.model;

import java.io.File;
import java.io.IOException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import kr.go.sciencecenter.controller.admin.AdminMainPageController;

public class MainPageBanner {
	
	private static final Logger logger = LoggerFactory.getLogger(MainPageBanner.class);
	
	private static final String RELATIVE_PATH= "/upload/main_banner_images/";
	public static final String MAIN_BANNER_IMAGE_PREFIX = "MAIN_BANNER_IMAGES";
	
	int id;
	int typeSeq;
	String title;
	String bannerType;
	boolean visible;
	MultipartFile bannerImageFile;
	String bannerImageUrl;
	int seq;
	Date regDttm;
	Date updtDttm;
	
	String buildingName;
	String exhibitionName;
	String description;
	String bannerLinkUrl;
	
	private static final List<String> BANNER_TYPES = new ArrayList<String>();
	
	static {
		BANNER_TYPES.add("mainLeftSlideBanner");
		BANNER_TYPES.add("mainRightSlideBanner");
		BANNER_TYPES.add("subSlideBanner");
		BANNER_TYPES.add("newsLetterBanner");
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getTypeSeq() {
		return typeSeq;
	}
	public void setTypeSeq(int typeSeq) {
		this.typeSeq = typeSeq;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public boolean isVisible() {
		return visible;
	}
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	public MultipartFile getBannerImageFile() {
		return bannerImageFile;
	}
	public void setBannerImageFile(MultipartFile bannerImageFile) {
		this.bannerImageFile = bannerImageFile;
	}
	public String getBannerImageUrl() {
		return bannerImageUrl;
	}
	public void setBannerImageUrl(String bannerImageUrl) {
		this.bannerImageUrl = bannerImageUrl;
	}
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	public Date getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(Date regDttm) {
		this.regDttm = regDttm;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
	public static List<String> getBannerTypes() {
		return BANNER_TYPES;
	}
	public String getBannerType() {
		return bannerType;
	}
	public void setBannerType(String bannerType) {
		this.bannerType = bannerType;
	}
	
	public String getBuildingName() {
		return buildingName;
	}
	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}
	public String getExhibitionName() {
		return exhibitionName;
	}
	public void setExhibitionName(String exhibitionName) {
		this.exhibitionName = exhibitionName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getBannerLinkUrl() {
		if (this.bannerLinkUrl == null || this.bannerLinkUrl.equals("")) {
			this.bannerLinkUrl = "#";
		}
		
		return bannerLinkUrl;
	}
	public void setBannerLinkUrl(String bannerLinkUrl) {
		this.bannerLinkUrl = bannerLinkUrl;
	}
	public void saveBannerImage(String contextPath, String baseFilePath) {
		
		baseFilePath += RELATIVE_PATH + this.getId() + "/";
		
		File dir = new File(baseFilePath);
		if (dir.exists()) {
			try {
				FileUtils.deleteDirectory(dir);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		dir.mkdirs();

		try {			
			String originalFileName = this.getBannerImageFile().getOriginalFilename();
			// file name 이 NFC normailization 이 필요할 경우, 
			if(!Normalizer.isNormalized(originalFileName, Normalizer.Form.NFC)) {
				String normalizedFileName = Normalizer.normalize(this.getBannerImageFile().getOriginalFilename(), Normalizer.Form.NFC);				
				originalFileName = normalizedFileName;
			}
			this.getBannerImageFile().transferTo(new File(baseFilePath + originalFileName));			
			this.setBannerImageUrl(contextPath + RELATIVE_PATH + this.getId() + "/" + originalFileName);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void deleteBannerImage(String baseFilePath) {
		
		baseFilePath += RELATIVE_PATH + this.getId() + "/";
		
		File dir = new File(baseFilePath);
		if (dir.exists()) {
			try {
				FileUtils.deleteDirectory(dir);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	
	
}
