package kr.go.sciencecenter.model;

import java.util.Date;
import java.util.List;

public class Event {
	
	public static final String EVENT_FILE_PREFIX = "EVENT";
	public static final String EVENT_MAIN_IMAGE_PREFIX = "EVENT_MAIN_IMAGES";
	
    int id;
    String title;
    String type;
    Date startDate;
    Date endDate;
    String place;
    String pay;
    String url;
    String content;
    boolean status;
    
    String pictureUrl;
    
    List<EventFile> attatchments;
    
    public Event() {
    	super();
    }
    
    public Event(String type) {
    	this.type = type;
    }
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public Date getStartDate() {
        return startDate;
    }
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    public Date getEndDate() {
        return endDate;
    }
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    public String getPlace() {
        return place;
    }
    public void setPlace(String place) {
        this.place = place;
    }
    public String getPay() {
        return pay;
    }
    public void setPay(String pay) {
        this.pay = pay;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public boolean isStatus() {
        return status;
    }
    public void setStatus(boolean status) {
        this.status = status;
    }
    public String getPictureUrl() {
        return pictureUrl;
    }
    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

	public List<EventFile> getAttatchments() {
		return attatchments;
	}

	public void setAttatchments(List<EventFile> attatchments) {
		this.attatchments = attatchments;
	}
    
    
}