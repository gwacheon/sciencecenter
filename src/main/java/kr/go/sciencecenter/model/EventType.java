package kr.go.sciencecenter.model;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class EventType {
    
    public static final HashMap<String, EventType> EVENT_TYPES = new LinkedHashMap<String, EventType>();
    static{
	
    EventType.EVENT_TYPES.put("culture", new EventType("culture", "문화행사", "예약페이지 바로가기"));
	EventType.EVENT_TYPES.put("play", new EventType("play", "문화공연", "예약페이지 바로가기"));
	EventType.EVENT_TYPES.put("camp", new EventType("camp", "캠프", "예약페이지 바로가기"));
    
    }
    
    String type;
    String name;
    String link;
    
    
    public EventType(String type, String name, String link){
    	setType(type);
    	setName(name);
		setLink(link);
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getLink() {
        return link;
    }
    public void setLink(String link) {
        this.link = link;
    }
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public static HashMap<String, EventType> getEventTypes() {
		return EVENT_TYPES;
	}
    
    
}
