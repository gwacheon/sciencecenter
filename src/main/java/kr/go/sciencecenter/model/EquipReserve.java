package kr.go.sciencecenter.model;

import java.util.Date;

import kr.go.sciencecenter.model.api.Member;

public class EquipReserve {
	int id;
	int equipmentId;
	int equiSerialId;
	Date beginTime;
	Date endTime;
	String status;
	Date regDttm;
	Date updtDttm;
	
	String memberNo;
	
	Member member;
	
	Equipment equipment;
	EquipSerial serial;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getEquipmentId() {
		return equipmentId;
	}
	public void setEquipmentId(int equipmentId) {
		this.equipmentId = equipmentId;
	}
	public int getEquiSerialId() {
		return equiSerialId;
	}
	public void setEquiSerialId(int equiSerialId) {
		this.equiSerialId = equiSerialId;
	}
	public Date getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(Date regDttm) {
		this.regDttm = regDttm;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
	public String getMemberNo() {
		return memberNo;
	}
	public void setMemberNo(String memberNo) {
		this.memberNo = memberNo;
	}
	public Member getMember() {
		return member;
	}
	public void setMember(Member member) {
		this.member = member;
	}
	public Equipment getEquipment() {
		return equipment;
	}
	public void setEquipment(Equipment equipment) {
		this.equipment = equipment;
	}
	public EquipSerial getSerial() {
		return serial;
	}
	public void setSerial(EquipSerial serial) {
		this.serial = serial;
	}
}
