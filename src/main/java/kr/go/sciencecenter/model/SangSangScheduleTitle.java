package kr.go.sciencecenter.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SangSangScheduleTitle {
	int id;
	String title;
	Date beginMonth;
	Date regDttm;
	Date updtDttm;
	
	List<SangSangSchedule> schedules;
	
	public SangSangScheduleTitle(){
		
	}
	
	public SangSangScheduleTitle(String jsonStr) {
		try {
			JSONObject obj = new JSONObject(jsonStr);
			this.id = obj.getInt("id");
			this.title = obj.getString("title");
			
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			this.setBeginMonth(formatter.parse(obj.getString("beginMonth")));
			
			List<SangSangSchedule> schedules = new ArrayList<SangSangSchedule>();
			
			JSONArray dates = obj.getJSONArray("schedules");
			
			for (int i = 0; i < dates.length(); i++) {
				SangSangSchedule sangSangSchedule = new SangSangSchedule(dates.getJSONObject(i));
				sangSangSchedule.setSangsangScheduleId(this.id);
				schedules.add(sangSangSchedule);
			}
			
			this.schedules = schedules;
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Date getBeginMonth() {
		return beginMonth;
	}
	public void setBeginMonth(Date beginMonth) {
		this.beginMonth = beginMonth;
	}
	public Date getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(Date regDttm) {
		this.regDttm = regDttm;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
	public List<SangSangSchedule> getSchedules() {
		return schedules;
	}
	public void setSchedules(List<SangSangSchedule> schedules) {
		this.schedules = schedules;
	}
	
	
}
