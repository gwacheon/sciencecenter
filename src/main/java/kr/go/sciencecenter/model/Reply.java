package kr.go.sciencecenter.model;

import java.util.Date;
import java.util.List;

import org.json.JSONObject;

/*
 * 설문 답변 Model
 */
public class Reply {
	int id;
	int surveyId;
	String name;
	String phone;
	String email;
	
	String remoteIpAddress;
	
	Date regDttm;
	Date updtDttm;
	
	List<ReplyDetail> details;
	
	public Reply(){
		
	}
	
	public Reply(int surveyId, String remoteIpAddress, JSONObject jsonObject) {
		this.surveyId = surveyId;
		this.remoteIpAddress = remoteIpAddress;
		
		if(jsonObject.has("name")){
			this.name = jsonObject.getString("name");
		}
		if(jsonObject.has("phone")){
			this.phone = jsonObject.getString("phone");
		}
		if(jsonObject.has("email")){
			this.email = jsonObject.getString("email");
		}
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getSurveyId() {
		return surveyId;
	}
	public void setSurveyId(int surveyId) {
		this.surveyId = surveyId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(Date regDttm) {
		this.regDttm = regDttm;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
	public String getRemoteIpAddress() {
		return remoteIpAddress;
	}

	public void setRemoteIpAddress(String remoteIpAddress) {
		this.remoteIpAddress = remoteIpAddress;
	}

	public List<ReplyDetail> getDetails() {
		return details;
	}

	public void setDetails(List<ReplyDetail> details) {
		this.details = details;
	}
	
	
}
