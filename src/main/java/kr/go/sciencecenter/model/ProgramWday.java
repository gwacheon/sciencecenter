package kr.go.sciencecenter.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProgramWday implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final List<String> WDAYS;
	static{
		WDAYS = new ArrayList<String>();
		WDAYS.add("sun");
		WDAYS.add("mon");
		WDAYS.add("tue");
		WDAYS.add("wed");
		WDAYS.add("thu");
		WDAYS.add("fri");
		WDAYS.add("sat");
	}
	
	int programId;
	int wday;
}
