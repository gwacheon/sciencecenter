package kr.go.sciencecenter.model;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class MainEventType {
	
	public static final HashMap<String, MainEventType> MAIN_EVENT_TYPES = 
			new LinkedHashMap<String, MainEventType>();
	
	static {
		MAIN_EVENT_TYPES.put("major", new MainEventType("major", "대표문화행사"));
		MAIN_EVENT_TYPES.put("minor", new MainEventType("minor", "기타문화행사"));
	}
	
	String type;
	String name;
	
	public MainEventType(String type, String name) {
		this.type = type;
		this.name = name;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
