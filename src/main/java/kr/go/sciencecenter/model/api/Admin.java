package kr.go.sciencecenter.model.api;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("ADMIN")
public class Admin {
	public static final String KEY_FIELD = "TA_MANAGER_NO";
	
	@XStreamAlias("ADMIN_ID")
	String adminId;
	
	@XStreamAlias("PASSWORD")
	String password;
	
	@XStreamAlias("RSP_CD")
	String rspCd;
	
	@XStreamAlias("RSP_MSG")
	String rspMsg;
	
	@XStreamAlias("TA_MANAGER_NO")
	String taManagerNo;
	
	public Admin() {
		
	}
	
	public Admin(String taManagerNo) {
		this.taManagerNo = taManagerNo;
	}
	
	public String getAdminId() {
		return adminId;
	}
	public void setAdminId(String adminId) {
		this.adminId = adminId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRspCd() {
		return rspCd;
	}
	public void setRspCd(String rspCd) {
		this.rspCd = rspCd;
	}
	public String getRspMsg() {
		return rspMsg;
	}
	public void setRspMsg(String rspMsg) {
		this.rspMsg = rspMsg;
	}
	public String getTaManagerNo() {
		return taManagerNo;
	}
	public void setTaManagerNo(String taManagerNo) {
		this.taManagerNo = taManagerNo;
	}
	
	public void mergeMember(Admin admin) {
		if (admin.getTaManagerNo() != null) { this.taManagerNo = admin.getTaManagerNo(); }
		if (admin.getAdminId() != null) { this.adminId = admin.getAdminId(); }
		if (admin.getRspCd() != null) { this.rspCd = admin.getRspCd(); }
		if (admin.getRspMsg() != null) { this.rspMsg = admin.getRspMsg(); }	
	}
}
