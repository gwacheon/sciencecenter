package kr.go.sciencecenter.model.api;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Date;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.converters.basic.BooleanConverter;

@XStreamAlias("MEMBER")
public class Member {
	public static final String CURRENT_MEMBER_NO = "currentMemberNo";
	public static final String PROFILE_FILE_PREFIX = "MEMBER";
	
	@XStreamAlias("MEMBER_ID")
	String memberId;
	
	@XStreamAlias("PASSWORD")
	String password;
	
	@XStreamAlias("PASSWORD_CHECK")
	String passwordCheck;
	
	String passwordConfirmation;
	
	@XStreamAlias("MEMBER_NAME")
	String memberName;
	
	@XStreamAlias("BIRTH_DATE")
	String birthDate;
	
	@XStreamAlias("MEMBER_CEL")
	String memberCel;
	
	@XStreamAlias("MEMBER_EMAIL")
	String memberEmail;
	
	@XStreamAlias("MEMBER_ZIP_CD")
	String memberZipCode;
	
	@XStreamAlias("MEMBER_ADDR1")
	String memberAddr1;
	
	@XStreamAlias("MEMBER_ADDR2")
	String memberAddr2;
	
	@XStreamAlias("FILE_NAME")
	String fileName;
	
	@XStreamAlias("DM_FLAG")
	@XStreamConverter(value=BooleanConverter.class, booleans={false}, strings={"Y", "N"})
	boolean dmFlag;
	
	@XStreamAlias("SMS_FLAG")
	@XStreamConverter(value=BooleanConverter.class, booleans={false}, strings={"Y", "N"})
	boolean smsFlag;
	
	@XStreamAlias("EMAIL_FLAG")
	@XStreamConverter(value=BooleanConverter.class, booleans={false}, strings={"Y", "N"})
	boolean emailFlag;
	
	@XStreamAlias("REG_NO_DATE")
	String regNoDate;
	
	@XStreamAlias("GENDER_FLAG")
	String genderFlag;
	
	@XStreamAlias("CERTIFY_KEY")
	String certifyKey;
	
	@XStreamAlias("DUPLICATE_KEY")
	String duplicateKey;
	
	@XStreamAlias("RSP_CD")
	String rspCd;
	
	@XStreamAlias("RSP_MSG")
	String rspMsg;
	
	@XStreamAlias("MEMBER_PW")
	String memberPw;
	
	@XStreamAlias("MEMBER_NO")
	String memberNo;
	
	@XStreamAlias("DROP_REASON")
	String dropReason;
	
	@XStreamAlias("BIRTH_DAY")
	String birthDay;
	
	@XStreamAlias("USABLE_FLAG")
	@XStreamConverter(value=BooleanConverter.class, booleans={false}, strings={"Y", "N"})
    boolean usableFlag;
	
	@XStreamAlias("LIST")
	List<Family> families;
	
	int cnt;
	Date regDttm;
	
	/*
	 * 임시 Certify Key 및 Duplicate Key 생
	 */
	public Member(){
		//임의의 Key 발급 처리 - 추후 Algorithm 적용 예정
		SecureRandom random = new SecureRandom();
		this.certifyKey = new BigInteger(130, random).toString(26);;
		//this.duplicateKey = new BigInteger(130, random).toString(26);;
	}
	
	public Member(String memberNo) {
		this.memberNo = memberNo;
	}
	
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPasswordCheck() {
		return passwordCheck;
	}

	public void setPasswordCheck(String passwordCheck) {
		this.passwordCheck = passwordCheck;
	}
	public String getPasswordConfirmation() {
		return passwordConfirmation;
	}
	public void setPasswordConfirmation(String passwordConfirmation) {
		this.passwordConfirmation = passwordConfirmation;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getMemberCel() {
		return memberCel;
	}
	public void setMemberCel(String memberCel) {
		this.memberCel = memberCel;
	}
	public String getMemberEmail() {
		return memberEmail;
	}
	public void setMemberEmail(String memberEmail) {
		this.memberEmail = memberEmail;
	}
	public String getMemberZipCode() {
		return memberZipCode;
	}
	public void setMemberZipCode(String memberZipCode) {
		this.memberZipCode = memberZipCode;
	}
	public String getMemberAddr1() {
		return memberAddr1;
	}
	public void setMemberAddr1(String memberAddr1) {
		this.memberAddr1 = memberAddr1;
	}
	public String getMemberAddr2() {
		return memberAddr2;
	}
	public void setMemberAddr2(String memberAddr2) {
		this.memberAddr2 = memberAddr2;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public boolean isDmFlag() {
		return dmFlag;
	}
	public void setDmFlag(boolean dmFlag) {
		this.dmFlag = dmFlag;
	}
	public boolean isSmsFlag() {
		return smsFlag;
	}
	public void setSmsFlag(boolean smsFlag) {
		this.smsFlag = smsFlag;
	}
	public boolean isEmailFlag() {
		return emailFlag;
	}
	public void setEmailFlag(boolean emailFlag) {
		this.emailFlag = emailFlag;
	}
	public String getRegNoDate() {
		return regNoDate;
	}
	public void setRegNoDate(String regNoDate) {
		this.regNoDate = regNoDate;
	}
	public String getGenderFlag() {
		return genderFlag;
	}
	public void setGenderFlag(String genderFlag) {
		this.genderFlag = genderFlag;
	}
	public String getCertifyKey() {
		return certifyKey;
	}
	public void setCertifyKey(String certifyKey) {
		this.certifyKey = certifyKey;
	}
	public String getDuplicateKey() {
		return duplicateKey;
	}
	public void setDuplicateKey(String duplicateKey) {
		this.duplicateKey = duplicateKey;
	}
	public String getRspCd() {
		return rspCd;
	}
	public void setRspCd(String rspCd) {
		this.rspCd = rspCd;
	}
	public String getRspMsg() {
		return rspMsg;
	}
	public void setRspMsg(String rspMsg) {
		this.rspMsg = rspMsg;
	}
	public boolean isUsableFlag() {
		return usableFlag;
	}
	public void setUsableFlag(boolean usableFlag) {
		this.usableFlag = usableFlag;
	}
	public String getMemberPw() {
		return memberPw;
	}
	public void setMemberPw(String memberPw) {
		this.memberPw = memberPw;
	}
	public String getMemberNo() {
		return memberNo;
	}
	public void setMemberNo(String memberNo) {
		this.memberNo = memberNo;
	}
	public String getDropReason() {
		return dropReason;
	}
	public void setDropReason(String dropReason) {
		this.dropReason = dropReason;
	}
	
	public String getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}

	public List<Family> getFamilies() {
		return families;
	}
	public void setFamilies(List<Family> families) {
		this.families = families;
	}
	
	public int getCnt() {
		return cnt;
	}

	public void setCnt(int cnt) {
		this.cnt = cnt;
	}
	public Date getRegDttm() {
		return regDttm;
	}

	public void setRegDttm(Date regDttm) {
		this.regDttm = regDttm;
	}

	public void mergeMember(Member member) {
		this.memberId = member.memberId == null ? this.memberId : member.memberId;
		this.password = member.password == null ? this.password : member.password;
		this.passwordCheck = member.passwordCheck == null ? this.passwordCheck : member.passwordCheck;
		this.passwordConfirmation = member.passwordConfirmation == null ? this.passwordConfirmation : member.passwordConfirmation;
		this.memberName = member.memberName == null ? this.memberName : member.memberName;
		this.birthDate = member.birthDate == null ? this.birthDate : member.birthDate;
		this.birthDay = member.birthDay == null ? this.birthDay : member.birthDay;
		this.memberCel = member.memberCel == null ? this.memberCel : member.memberCel;
		this.memberEmail = member.memberEmail == null ? this.memberEmail : member.memberEmail;
		this.memberZipCode = member.memberZipCode == null ? this.memberZipCode : member.memberZipCode;
		this.memberAddr1 = member.memberAddr1 == null ? this.memberAddr1 : member.memberAddr1;
		this.memberAddr2 = member.memberAddr2 == null ? this.memberAddr2 : member.memberAddr2;
		this.dmFlag = member.dmFlag || this.dmFlag;
		this.smsFlag = member.smsFlag || this.smsFlag;
		this.emailFlag = member.emailFlag || this.emailFlag;
		this.regNoDate = member.regNoDate == null ? this.regNoDate : member.regNoDate;
		this.genderFlag = member.genderFlag == null ? this.genderFlag : member.genderFlag;
		this.certifyKey = member.certifyKey == null ? this.certifyKey : member.certifyKey;
		this.duplicateKey = member.duplicateKey == null ? this.duplicateKey : member.duplicateKey;
		this.rspCd = member.rspCd == null ? this.rspCd : member.rspCd;
		this.rspMsg = member.rspMsg == null ? this.rspMsg : member.rspMsg;
		this.memberPw = member.memberPw == null ? this.memberPw : member.memberPw;
		this.memberNo = member.memberNo == null ? this.memberNo : member.memberNo;
		this.dropReason = member.dropReason == null ? this.dropReason : member.dropReason;
		this.usableFlag = member.usableFlag || this.usableFlag;
		this.families = member.families == null ? this.families : member.families;
	}
}
