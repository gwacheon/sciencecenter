package kr.go.sciencecenter.model.api;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.converters.basic.BooleanConverter;

@XStreamAlias("LECTURE")
public class LectureDetail {
	@XStreamAlias("RSP_CD")
	String rspCd;

	@XStreamAlias("RSP_MSG")
	String rspMsg;

	@XStreamAlias("COURSE_START_DATE")
	String courseStartDate;

	@XStreamAlias("COURSE_END_DATE")
	String courseEndDate;

	@XStreamAlias("COURSE_INTRODUCTION")
	String courseIntroduction;

	@XStreamAlias("COURSE_CONTENTS")
	String courseContents;

	@XStreamAlias("COURSE_NAME")
	String courseName;

	@XStreamAlias("EXIST_START_DATE")
	String existStartDate;

	@XStreamAlias("EXIST_START_TIME")
	String existStartTime;

	@XStreamAlias("EXIST_END_DATE")
	String existEndDate;

	@XStreamAlias("EXIST_END_TIME")
	String existEndTime;

	@XStreamAlias("FACILITY_NAME")
	String faciltyName;

	@XStreamAlias("PLACE_INFO")
	String placeInfo;
	
	@XStreamAlias("PREVENTATION_NO")
	String preventationNo;

	@XStreamAlias("PLACE_NAME")
	String placeName;

	@XStreamAlias("ACADEMY_CD")
	String academyCd;

	@XStreamAlias("CLASS_CD")
	String classCd;

	@XStreamAlias("COURSE_CD")
	String courseCd;

	public String getRspCd() {
		return rspCd;
	}

	public void setRspCd(String rspCd) {
		this.rspCd = rspCd;
	}

	public String getRspMsg() {
		return rspMsg;
	}

	public void setRspMsg(String rspMsg) {
		this.rspMsg = rspMsg;
	}

	public String getCourseStartDate() {
		return courseStartDate;
	}

	public void setCourseStartDate(String courseStartDate) {
		this.courseStartDate = courseStartDate;
	}

	public String getCourseEndDate() {
		return courseEndDate;
	}

	public void setCourseEndDate(String courseEndDate) {
		this.courseEndDate = courseEndDate;
	}

	public String getCourseIntroduction() {
		return courseIntroduction;
	}

	public void setCourseIntroduction(String courseIntroduction) {
		this.courseIntroduction = courseIntroduction;
	}

	public String getCourseContents() {
		return courseContents;
	}

	public void setCourseContents(String courseContents) {
		this.courseContents = courseContents;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getExistStartDate() {
		return existStartDate;
	}

	public void setExistStartDate(String existStartDate) {
		this.existStartDate = existStartDate;
	}

	public String getExistStartTime() {
		return existStartTime;
	}

	public void setExistStartTime(String existStartTime) {
		this.existStartTime = existStartTime;
	}

	public String getExistEndDate() {
		return existEndDate;
	}

	public void setExistEndDate(String existEndDate) {
		this.existEndDate = existEndDate;
	}

	public String getExistEndTime() {
		return existEndTime;
	}

	public void setExistEndTime(String existEndTime) {
		this.existEndTime = existEndTime;
	}

	public String getFaciltyName() {
		return faciltyName;
	}

	public void setFaciltyName(String faciltyName) {
		this.faciltyName = faciltyName;
	}

	public String getPlaceInfo() {
		return placeInfo;
	}

	public void setPlaceInfo(String placeInfo) {
		this.placeInfo = placeInfo;
	}

	public String getPlaceName() {
		return placeName;
	}

	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}

	public String getAcademyCd() {
		return academyCd;
	}

	public void setAcademyCd(String academyCd) {
		this.academyCd = academyCd;
	}

	public String getClassCd() {
		return classCd;
	}

	public void setClassCd(String classCd) {
		this.classCd = classCd;
	}

	public String getCourseCd() {
		return courseCd;
	}

	public void setCourseCd(String courseCd) {
		this.courseCd = courseCd;
	}

	public String getPreventationNo() {
		return preventationNo;
	}

	public void setPreventationNo(String preventationNo) {
		this.preventationNo = preventationNo;
	}

}
