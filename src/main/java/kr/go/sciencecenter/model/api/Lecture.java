package kr.go.sciencecenter.model.api;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("LECTURE")
public class Lecture {
	@XStreamAlias("RSP_CD")
	String rspCd;
	
	@XStreamAlias("RSP_MSG")
	String rspMsg;
	
	@XStreamAlias("ALL_CNT")
	int allCnt;
	
	@XStreamAlias("LIST")
	List<LectureInfo> infos;

	public String getRspCd() {
		return rspCd;
	}

	public void setRspCd(String rspCd) {
		this.rspCd = rspCd;
	}

	public String getRspMsg() {
		return rspMsg;
	}

	public void setRspMsg(String rspMsg) {
		this.rspMsg = rspMsg;
	}

	public int getAllCnt() {
		return allCnt;
	}

	public void setAllCnt(int allCnt) {
		this.allCnt = allCnt;
	}

	public List<LectureInfo> getInfos() {
		return infos;
	}

	public void setInfos(List<LectureInfo> infos) {
		this.infos = infos;
	}
}
