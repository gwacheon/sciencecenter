package kr.go.sciencecenter.model.api;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("LECTURE")
public class ReqLecture {
	@XStreamAlias("ACADEMY_CD")
	String academyCd;
	
	@XStreamAlias("CLASS_CD")
	String classCd;
	
	@XStreamAlias("AGE")
	String age;
	
	@XStreamAlias("YYYYMM")
	String yyyymm;
	
	@XStreamAlias("SEARCH")
	String search;
	
	@XStreamAlias("FEE")
	String fee;
	
	@XStreamAlias("START_NUM")
	int startNum;
	
	@XStreamAlias("END_NUM")
	int endNum;
	
	@XStreamAlias("COURSE_CD")
	String courseCd;
	
	public ReqLecture() {
		
	}
	
	public ReqLecture(String courseCd) {
		this.courseCd = courseCd;
	}
	
	public String getAcademyCd() {
		return academyCd;
	}
	public void setAcademyCd(String academyCd) {
		this.academyCd = academyCd;
	}
	public String getClassCd() {
		return classCd;
	}
	public void setClassCd(String classCd) {
		this.classCd = classCd;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getYyyymm() {
		return yyyymm;
	}
	public void setYyyymm(String yyyymm) {
		this.yyyymm = yyyymm;
	}
	public String getSearch() {
		return search;
	}
	public void setSearch(String search) {
		this.search = search;
	}
	public String getFee() {
		return fee;
	}
	public void setFee(String fee) {
		this.fee = fee;
	}
	public int getStartNum() {
		return startNum;
	}
	public void setStartNum(int startNum) {
		this.startNum = startNum;
	}
	public int getEndNum() {
		return endNum;
	}
	public void setEndNum(int endNum) {
		this.endNum = endNum;
	}

	public void setSearchParams(String fee, String age, String search, String year, String month, String startNum,
			String endNum) {
		this.fee = "00";
		this.age = "00";
		this.yyyymm = "";
		this.search = "";
		this.startNum = 1;
		this.endNum = 12;
		
		if(fee != null && !fee.equals("")) {
			this.fee = fee;
		}
		
		if(age != null && !age.equals("")) {
			this.age = age;
		}
		
		if(search != null && !search.equals("")) {
			this.search = search;
		}
		
		if(year != null && !year.equals("") && month != null && !month.equals("")) {
			this.yyyymm = year + month;
		}
		
		if(startNum != null && !startNum.equals("") && endNum != null && !endNum.equals("")) {
			this.startNum = Integer.parseInt(startNum);
			this.endNum = Integer.parseInt(endNum);
		}
	}	
}
