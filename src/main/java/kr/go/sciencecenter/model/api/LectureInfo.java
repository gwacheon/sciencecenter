package kr.go.sciencecenter.model.api;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.converters.basic.BooleanConverter;

@XStreamAlias("INFO")
public class LectureInfo {
	@XStreamAlias("COURSE_NAME")
	String courseName;
	
	@XStreamAlias("SEMESTER_NAME")
	String semesterName;
	
	@XStreamAlias("CLASS_CD")
	String classCd;
	
	@XStreamAlias("RECEIPT_END_DATE")
	String receiptEndDate;
	
	@XStreamAlias("COURSE_FEE")
	int courseFee;
	
	@XStreamAlias("ACADEMY_CD")
	String academyCd;
	
	@XStreamAlias("COURSE_CD")
	String courseCd;
	
	@XStreamAlias("SEMESTER_CD")
	String semesterCd;
	
	@XStreamAlias("RECEIPT_START_DATE")
	String receiptStartDate;
	
	@XStreamAlias("WAIT_CAPACITY")
	int waitCapacity;
	
	@XStreamAlias("SCHEDULE_FLAG")
	@XStreamConverter(value=BooleanConverter.class, booleans={false}, strings={"Y", "N"})
	boolean scheduleFlag;
	
	@XStreamAlias("AGE_FLAG")
	@XStreamConverter(value=BooleanConverter.class, booleans={false}, strings={"Y", "N"})
	boolean ageFlag;
		
	@XStreamAlias("AGE_LIMIT_MAX")
	int ageLimitMax;
	
	@XStreamAlias("COURSE_CAPACITY")
	int courseCapacity;
	
	@XStreamAlias("COURSE_END_DATE")
	String courseEndDate;
	
	@XStreamAlias("ACADEMY_NAME")
	String academyName;
	
	@XStreamAlias("AGE_LIMIT_MIN")
	int ageLimitMin;
	
	@XStreamAlias("CLASS_NAME")
	String classNAme;
	
	@XStreamAlias("RECEIPT_END_TIME")
	String receiptEndTime;
	
	@XStreamAlias("RESER_CNT")
	int reserCnt;
	
	@XStreamAlias("RNUM")
	int rnum;
	
	@XStreamAlias("WAIT_CNT")
	int waitCnt;
	
	@XStreamAlias("COURSE_START_DATE")
	String courseStartDate;
	
	@XStreamAlias("AGE_TYPE")
	String ageType;
	
	@XStreamAlias("RECEIPT_START_TIME")
	String receiptStartTime;
	
	@XStreamAlias("TARGET_NAME")
	String targetName;
	
	@XStreamAlias("RECEIPT_CNT")
	int receiptCnt;
	
	@XStreamAlias("OWNER_LIMIT_FLAG")
	String ownerLimitFlag;
	
	@XStreamAlias("APPLY_CNT")
	String applyCnt;
	

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getSemesterName() {
		return semesterName;
	}

	public void setSemesterName(String semesterName) {
		this.semesterName = semesterName;
	}

	public String getClassCd() {
		return classCd;
	}

	public void setClassCd(String classCd) {
		this.classCd = classCd;
	}

	public String getReceiptEndDate() {
		return receiptEndDate;
	}

	public void setReceiptEndDate(String receiptEndDate) {
		this.receiptEndDate = receiptEndDate;
	}

	public int getCourseFee() {
		return courseFee;
	}

	public void setCourseFee(int courseFee) {
		this.courseFee = courseFee;
	}

	public String getAcademyCd() {
		return academyCd;
	}

	public void setAcademyCd(String academyCd) {
		this.academyCd = academyCd;
	}

	public String getCourseCd() {
		return courseCd;
	}

	public void setCourseCd(String courseCd) {
		this.courseCd = courseCd;
	}

	public String getSemesterCd() {
		return semesterCd;
	}

	public void setSemesterCd(String semesterCd) {
		this.semesterCd = semesterCd;
	}

	public String getReceiptStartDate() {
		return receiptStartDate;
	}

	public void setReceiptStartDate(String receiptStartDate) {
		this.receiptStartDate = receiptStartDate;
	}

	public int getWaitCapacity() {
		return waitCapacity;
	}

	public void setWaitCapacity(int waitCapacity) {
		this.waitCapacity = waitCapacity;
	}

	public boolean isScheduleFlag() {
		return scheduleFlag;
	}

	public void setScheduleFlag(boolean scheduleFlag) {
		this.scheduleFlag = scheduleFlag;
	}

	public boolean isAgeFlag() {
		return ageFlag;
	}

	public void setAgeFlag(boolean ageFlag) {
		this.ageFlag = ageFlag;
	}

	public int getAgeLimitMax() {
		return ageLimitMax;
	}

	public void setAgeLimitMax(int ageLimitMax) {
		this.ageLimitMax = ageLimitMax;
	}

	public int getCourseCapacity() {
		return courseCapacity;
	}

	public void setCourseCapacity(int courseCapacity) {
		this.courseCapacity = courseCapacity;
	}

	public String getCourseEndDate() {
		return courseEndDate;
	}

	public void setCourseEndDate(String courseEndDate) {
		this.courseEndDate = courseEndDate;
	}

	public String getAcademyName() {
		return academyName;
	}

	public void setAcademyName(String academyName) {
		this.academyName = academyName;
	}

	public int getAgeLimitMin() {
		return ageLimitMin;
	}

	public void setAgeLimitMin(int ageLimitMin) {
		this.ageLimitMin = ageLimitMin;
	}

	public String getClassNAme() {
		return classNAme;
	}

	public void setClassNAme(String classNAme) {
		this.classNAme = classNAme;
	}

	public String getReceiptEndTime() {
		return receiptEndTime;
	}

	public void setReceiptEndTime(String receiptEndTime) {
		this.receiptEndTime = receiptEndTime;
	}

	public int getReserCnt() {
		return reserCnt;
	}

	public void setReserCnt(int reserCnt) {
		this.reserCnt = reserCnt;
	}

	public int getRnum() {
		return rnum;
	}

	public void setRnum(int rnum) {
		this.rnum = rnum;
	}

	public int getWaitCnt() {
		return waitCnt;
	}

	public void setWaitCnt(int waitCnt) {
		this.waitCnt = waitCnt;
	}

	public String getCourseStartDate() {
		return courseStartDate;
	}

	public void setCourseStartDate(String courseStartDate) {
		this.courseStartDate = courseStartDate;
	}

	public String getAgeType() {
		return ageType;
	}

	public void setAgeType(String ageType) {
		this.ageType = ageType;
	}

	public String getReceiptStartTime() {
		return receiptStartTime;
	}

	public void setReceiptStartTime(String receiptStartTime) {
		this.receiptStartTime = receiptStartTime;
	}

	public String getTargetName() {
		return targetName;
	}

	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}

	public int getReceiptCnt() {
		return receiptCnt;
	}

	public void setReceiptCnt(int receiptCnt) {
		this.receiptCnt = receiptCnt;
	}

	public String getOwnerLimitFlag() {
		return ownerLimitFlag;
	}

	public void setOwnerLimitFlag(String ownerLimitFlag) {
		this.ownerLimitFlag = ownerLimitFlag;
	}

	public String getApplyCnt() {
		return applyCnt;
	}

	public void setApplyCnt(String applyCnt) {
		this.applyCnt = applyCnt;
	}
}
