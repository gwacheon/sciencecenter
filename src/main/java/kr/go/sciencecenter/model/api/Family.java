package kr.go.sciencecenter.model.api;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.converters.basic.BooleanConverter;

@XStreamAlias("FAMILY")
public class Family {
	@XStreamAlias("WELFARE_FLAG")
	@XStreamConverter(value=BooleanConverter.class, booleans={false}, strings={"Y", "N"})
	boolean welfareFlag;
	
	@XStreamAlias("BIRTH_DAY")
	String birthDay;
	
	@XStreamAlias("FAMILY_NO")
	String familyNo;
	
	@XStreamAlias("USE_FLAG")
	@XStreamConverter(value=BooleanConverter.class, booleans={false}, strings={"Y", "N"})
	boolean useFlag;
	
	@XStreamAlias("PAID_MEMBERSHIP_END_DATE")
	String paidMembershipEndDate;
	
	@XStreamAlias("FAMILY_CEL")
	String familyCel;
	
	@XStreamAlias("FAMILY_TEL")
	String familyTel;
	
	@XStreamAlias("COMPANY_CD")
	String companyCd;
	
	@XStreamAlias("MEMBERSHIP_UNIQUE_NBR")
	String membershipUniqueNbf;

	@XStreamAlias("MEMBERSHIP_TYPE")
	String membershipType;
	
	@XStreamAlias("FAMILY_NAME")
	String familyName;
	
	@XStreamAlias("MEMBERSHIP_END_DATE")
	String membershipEndDate;
	
	@XStreamAlias("LUNAR_FLAG")
	@XStreamConverter(value=BooleanConverter.class, booleans={false}, strings={"Y", "N"})
	boolean lunarFlag;
	
	@XStreamAlias("MEMBERSHIP_NO")
	String membershipNo;
	
	@XStreamAlias("RELATION_TYPE")
	String relationType;
	
	@XStreamAlias("MEMBER_NO")
	String memberNo;
	
	@XStreamAlias("GENDER_FLAG")
	String genderFlag;
	
	@XStreamAlias("MEMBERSHIP_NAME")
	String membershipName;
	
	@XStreamAlias("RELATION_TYPE_NAME")
	String relationTypeName;
	
	@XStreamAlias("SCHOOL_NAME")
	String schoolName;
	
	@XStreamAlias("BIRTH_DATE")
	String birthDate;
	
	@XStreamAlias("FAMILY_EMAIL")
	String familyEmail;
	
	@XStreamAlias("GRADE_NAME")
	String gradeName;
	
	@XStreamAlias("CLASS_NAME")
	String className;
	
	@XStreamAlias("FILE_NAME")
	String fileName;
	
	@XStreamAlias("FILE_URL")
	String fileUrl;
	
	@XStreamAlias("DISCOUNT_FLAG")
	String discountFlag;
	
	@XStreamAlias("RSP_CD")
	String rspCd;
	
	@XStreamAlias("RSP_MSG")
	String rspMsg;
	
	public Family() {
		
	}

	public boolean isWelfareFlag() {
		return welfareFlag;
	}

	public void setWelfareFlag(boolean welfareFlag) {
		this.welfareFlag = welfareFlag;
	}

	public String getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}

	public String getFamilyNo() {
		return familyNo;
	}

	public void setFamilyNo(String familyNo) {
		this.familyNo = familyNo;
	}

	public boolean isUseFlag() {
		return useFlag;
	}

	public void setUseFlag(boolean useFlag) {
		this.useFlag = useFlag;
	}

	public String getPaidMembershipEndDate() {
		return paidMembershipEndDate;
	}

	public void setPaidMembershipEndDate(String paidMembershipEndDate) {
		this.paidMembershipEndDate = paidMembershipEndDate;
	}

	public String getFamilyCel() {
		return familyCel;
	}

	public void setFamilyCel(String familyCel) {
		this.familyCel = familyCel;
	}

	public String getFamilyTel() {
		return familyTel;
	}

	public void setFamilyTel(String familyTel) {
		this.familyTel = familyTel;
	}

	public String getCompanyCd() {
		return companyCd;
	}

	public void setCompanyCd(String companyCd) {
		this.companyCd = companyCd;
	}

	public String getMembershipUniqueNbf() {
		return membershipUniqueNbf;
	}

	public void setMembershipUniqueNbf(String membershipUniqueNbf) {
		this.membershipUniqueNbf = membershipUniqueNbf;
	}

	public String getMembershipType() {
		return membershipType;
	}

	public void setMembershipType(String membershipType) {
		this.membershipType = membershipType;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public String getMembershipEndDate() {
		return membershipEndDate;
	}

	public void setMembershipEndDate(String membershipEndDate) {
		this.membershipEndDate = membershipEndDate;
	}

	public boolean isLunarFlag() {
		return lunarFlag;
	}

	public void setLunarFlag(boolean lunarFlag) {
		this.lunarFlag = lunarFlag;
	}

	public String getMembershipNo() {
		return membershipNo;
	}

	public void setMembershipNo(String membershipNo) {
		this.membershipNo = membershipNo;
	}

	public String getRelationType() {
		return relationType;
	}

	public void setRelationType(String relationType) {
		this.relationType = relationType;
	}

	public String getMemberNo() {
		return memberNo;
	}

	public void setMemberNo(String memberNo) {
		this.memberNo = memberNo;
	}

	public String getGenderFlag() {
		return genderFlag;
	}

	public void setGenderFlag(String genderFlag) {
		this.genderFlag = genderFlag;
	}

	public String getMembershipName() {
		return membershipName;
	}

	public void setMembershipName(String membershipName) {
		this.membershipName = membershipName;
	}

	public String getRelationTypeName() {
		return relationTypeName;
	}

	public void setRelationTypeName(String relationTypeName) {
		this.relationTypeName = relationTypeName;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getFamilyEmail() {
		return familyEmail;
	}

	public void setFamilyEmail(String familyEmail) {
		this.familyEmail = familyEmail;
	}
	
	public String getGradeName() {
		return gradeName;
	}

	public void setGradeName(String gradeName) {
		this.gradeName = gradeName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public String getFileUrl() {
		return fileUrl;
	}
	
	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}
	
	public String getDiscountFlag() {
		return discountFlag;
	}
	
	public void setDiscountFlag(String discountFlag) {
		this.discountFlag = discountFlag;
	}

	public String getRspCd() {
		return rspCd;
	}

	public void setRspCd(String rspCd) {
		this.rspCd = rspCd;
	}

	public String getRspMsg() {
		return rspMsg;
	}

	public void setRspMsg(String rspMsg) {
		this.rspMsg = rspMsg;
	}

	public void mergeMember(Family family) {
		this.rspCd = family.getRspCd() != null ? family.getRspCd() : "";
		this.rspMsg = family.getRspMsg() != null ? family.getRspMsg() : "";
	}
}
