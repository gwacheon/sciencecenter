package kr.go.sciencecenter.model.api;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("MEMBER")
public class MemberShip {
	@XStreamAlias("MEMBER_NO")
	String memberNo;
	
	@XStreamAlias("MEMBERSHIP_TYPE")
	String membershipType;
	
	@XStreamAlias("MEMBERSHIP_NO")
	String membershipNo;
	
	@XStreamAlias("BANK_CD")
	String bankCd;
	
	@XStreamAlias("ACCOUNT_NUM")
	String accountNum;
	
	@XStreamAlias("ACCOUNT_NAME")
	String accountName;
	
	@XStreamAlias("RSP_CD")
	String rspCd;
	
	@XStreamAlias("RSP_MSG")
	String rspMsg;

	public String getMemberNo() {
		return memberNo;
	}

	public void setMemberNo(String memberNo) {
		this.memberNo = memberNo;
	}

	public String getMembershipType() {
		return membershipType;
	}

	public void setMembershipType(String membershipType) {
		this.membershipType = membershipType;
	}

	public String getMembershipNo() {
		return membershipNo;
	}

	public void setMembershipNo(String membershipNo) {
		this.membershipNo = membershipNo;
	}

	public String getBankCd() {
		return bankCd;
	}

	public void setBankCd(String bankCd) {
		this.bankCd = bankCd;
	}

	public String getAccountNum() {
		return accountNum;
	}

	public void setAccountNum(String accountNum) {
		this.accountNum = accountNum;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getRspCd() {
		return rspCd;
	}

	public void setRspCd(String rspCd) {
		this.rspCd = rspCd;
	}

	public String getRspMsg() {
		return rspMsg;
	}

	public void setRspMsg(String rspMsg) {
		this.rspMsg = rspMsg;
	}
	
}
