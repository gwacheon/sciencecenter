package kr.go.sciencecenter.model;

import java.util.Date;

/*
 * 설문 항목별 답변 Model
 */
public class ReplyDetail {
	int id;
	int replyId;
	int articleId;
	String reply;
	Date regDttm;
	Date updtDttm;
	
	public ReplyDetail(){
		
	}
	
	public ReplyDetail(int replyId, int articleId, String reply) {
		this.replyId = replyId;
		this.articleId = articleId;
		this.reply = reply;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getReplyId() {
		return replyId;
	}
	public void setReplyId(int replyId) {
		this.replyId = replyId;
	}
	public int getArticleId() {
		return articleId;
	}
	public void setArticleId(int articleId) {
		this.articleId = articleId;
	}
	public String getReply() {
		return reply;
	}
	public void setReply(String reply) {
		this.reply = reply;
	}
	public Date getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(Date regDttm) {
		this.regDttm = regDttm;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
		
}
