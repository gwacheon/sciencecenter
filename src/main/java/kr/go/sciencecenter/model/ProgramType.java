package kr.go.sciencecenter.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProgramType {
	public static final Map<String, List<String>> PROGRAM_TYPE_MAP;
	static{
		PROGRAM_TYPE_MAP = new HashMap<String, List<String>>();
		
		List<String> programList = new ArrayList<String>();
		programList.add("private");
		programList.add("organization");
		programList.add("display");
		programList.add("ideaall");
		programList.add("astrology");
		programList.add("event");
		
		PROGRAM_TYPE_MAP.put("normal", programList);
	}
}
