package kr.go.sciencecenter.model;

import java.util.Date;

public class Attatchment {

	public static final String ATTATCHMENT_FILE_PREFIX = "CKEDITOR_ATTATCHMENTS";
	
	int id;
	String fileName;
	String filePath;
	Date regDttm;
	Date updtDttm;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public Date getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(Date regDttm) {
		this.regDttm = regDttm;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
	public String getUrl(){
		return this.filePath + "/" + this.fileName;
	}
}
