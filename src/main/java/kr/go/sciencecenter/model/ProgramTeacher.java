package kr.go.sciencecenter.model;

import java.sql.Date;

public class ProgramTeacher {
  int program_id;
  int teacher_id;
  
  Date reg_dttm;
  Date updt_dttm;
  
  public int getProgram_id() {
    return program_id;
  }
  public void setProgram_id(int program_id) {
    this.program_id = program_id;
  }
  public int getTeacher_id() {
    return teacher_id;
  }
  public void setTeacher_id(int teacher_id) {
    this.teacher_id = teacher_id;
  }
  public Date getReg_dttm() {
    return reg_dttm;
  }
  public void setReg_dttm(Date reg_dttm) {
    this.reg_dttm = reg_dttm;
  }
  public Date getUpdt_dttm() {
    return updt_dttm;
  }
  public void setUpdt_dttm(Date updt_dttm) {
    this.updt_dttm = updt_dttm;
  }
}
