package kr.go.sciencecenter.model;

import java.util.Date;

public class DinnerApplyTime {
	
	int id;
	Date applyBeginTime;
	Date applyEndTime;
	Date regDttm;
	Date updtDttm;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getApplyBeginTime() {
		return applyBeginTime;
	}

	public void setApplyBeginTime(Date applyBeginTime) {
		this.applyBeginTime = applyBeginTime;
	}

	public Date getApplyEndTime() {
		return applyEndTime;
	}

	public void setApplyEndTime(Date applyEndTime) {
		this.applyEndTime = applyEndTime;
	}

	public Date getRegDttm() {
		return regDttm;
	}

	public void setRegDttm(Date regDttm) {
		this.regDttm = regDttm;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}	
}
