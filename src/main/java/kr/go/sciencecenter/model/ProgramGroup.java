package kr.go.sciencecenter.model;

import java.util.Date;
import java.util.List;

public class ProgramGroup {
	public static final String [] CATEGORIES = {
		"education", "astrology", "display", "event"
	};
	
	int id;
	String groupNo;
	String category;
	String title;
	int openYear;
	String status;
	Date onlineOpen;
	Date onlineClose;
	Date preReservationTime;
	Date regDttm;
	Date updtDttm;
	
	int cnt;
	
	List<Program> programs;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getGroupNo() {
		return groupNo;
	}
	public void setGroupNo(String groupNo) {
		this.groupNo = groupNo;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getOpenYear() {
		return openYear;
	}
	public void setOpenYear(int openYear) {
		this.openYear = openYear;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getOnlineOpen() {
		return onlineOpen;
	}
	public void setOnlineOpen(Date onlineOpen) {
		this.onlineOpen = onlineOpen;
	}
	public Date getOnlineClose() {
		return onlineClose;
	}
	public void setOnlineClose(Date onlineClose) {
		this.onlineClose = onlineClose;
	}
	public Date getPreReservationTime() {
		return preReservationTime;
	}
	public void setPreReservationTime(Date preReservationTime) {
		this.preReservationTime = preReservationTime;
	}
	public Date getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(Date regDttm) {
		this.regDttm = regDttm;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
	
	public int getCnt() {
		return cnt;
	}
	public void setCnt(int cnt) {
		this.cnt = cnt;
	}
	public List<Program> getPrograms() {
		return programs;
	}
	public void setPrograms(List<Program> programs) {
		this.programs = programs;
	}
}
