package kr.go.sciencecenter.model;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class UserDetail extends User{
	private static final long serialVersionUID = 1L;
	
	private kr.go.sciencecenter.model.User user;

	public UserDetail(String username, String password,
			Collection<? extends GrantedAuthority> authorities, kr.go.sciencecenter.model.User user) {
		super(username, password, authorities);
		// TODO Auto-generated constructor stub
		this.user = user;
	}
	
	public kr.go.sciencecenter.model.User getUser() {
		return this.user;
	}
}
