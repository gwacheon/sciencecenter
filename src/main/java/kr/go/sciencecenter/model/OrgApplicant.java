
package kr.go.sciencecenter.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.common.base.Joiner;

public class OrgApplicant {
	public static final String APPLICANT_STATUS_PENDING = "pending";
	public static final String APPLICANT_STATUS_ACCEPT = "accept";
	public static final String APPLICANT_STATUS_REJECT = "reject";
	public static final String APPLICANT_STATUS_CANCEL = "cancel";
	
	int id;
	int orgProgramId;
	int userId;
	
	String memberNo;
	
	String title;
	String description;
	int members;
	String applicationType;
	String applicationStatus;
	Date applicationTime;
	Date regDttm;
	Date updtDttm;
	
	boolean duplicate;
	
	OrgProgram program;
	
	String searchBeginDate;
	String searchEndDate;
	String searchTitle;
	String searchStatus;
	
	public OrgApplicant(){
		
	}
	
	public OrgApplicant(int orgProgramId, String jsonStr) {
		JSONObject obj = new JSONObject(jsonStr).getJSONObject("applicant");
		this.orgProgramId = orgProgramId;
		this.members = obj.getInt("members");
		this.applicationType = obj.getString("applicantType");
		this.title = obj.getString("title");
		this.description = obj.getString("description");
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		try {
			this.applicationTime = formatter.parse(obj.getString("programTimeStr"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getOrgProgramId() {
		return orgProgramId;
	}
	public void setOrgProgramId(int orgProgramId) {
		this.orgProgramId = orgProgramId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public String getMemberNo() {
		return memberNo;
	}

	public void setMemberNo(String memberNo) {
		this.memberNo = memberNo;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getMembers() {
		return members;
	}
	public void setMembers(int members) {
		this.members = members;
	}
	public String getApplicationType() {
		return applicationType;
	}
	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}
	
	public String getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	public Date getApplicationTime() {
		return applicationTime;
	}
	public void setApplicationTime(Date applicationTime) {
		this.applicationTime = applicationTime;
	}
	public Date getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(Date regDttm) {
		this.regDttm = regDttm;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isDuplicate() {
		return duplicate;
	}

	public void setDuplicate(boolean duplicate) {
		this.duplicate = duplicate;
	}

	public OrgProgram getProgram() {
		return program;
	}

	public void setProgram(OrgProgram program) {
		this.program = program;
	}

	public String getSearchBeginDate() {
		return searchBeginDate;
	}

	public void setSearchBeginDate(String searchBeginDate) {
		this.searchBeginDate = searchBeginDate;
	}

	public String getSearchEndDate() {
		return searchEndDate;
	}

	public void setSearchEndDate(String searchEndDate) {
		this.searchEndDate = searchEndDate;
	}

	public String getSearchTitle() {
		return searchTitle;
	}

	public void setSearchTitle(String searchTitle) {
		this.searchTitle = searchTitle;
	}

	public String getSearchStatus() {
		return searchStatus;
	}

	public void setSearchStatus(String searchStatus) {
		this.searchStatus = searchStatus;
	}

	public String getQueryPath() {
		String queryPath = "?";
		List<String> parameters = new ArrayList<String>();
		
		if (this.searchTitle != null && !this.searchTitle.equals("")) {
			parameters.add("searchTitle=" + this.searchTitle);
		}
		
		if (this.searchBeginDate != null && !this.searchBeginDate.equals("")) {
			parameters.add("searchBeginDate=" + this.searchBeginDate);
		}
		
		if (this.searchEndDate != null && !this.searchEndDate.equals("")) {
			parameters.add("searchEndDate=" + this.searchEndDate);
		}
		
		if (this.searchStatus != null && !this.searchStatus.equals("")) {
			parameters.add("searchStatus=" + this.searchStatus);
		}
		
		if (parameters.size() > 0) {
			queryPath += Joiner.on("&").join(parameters);
		}
		
		return queryPath;
	}
	
}
