package kr.go.sciencecenter.model;

import java.io.Serializable;
import java.util.List;

public class MainEvent implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	int id;
    String name;
    String mainEventType;
    String subType;
    String linkUrl;
    int seq;
    
    List<MainEventSeries> series;
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getSeq() {
        return seq;
    }
    public void setSeq(int seq) {
        this.seq = seq;
    }
	public List<MainEventSeries> getSeries() {
		return series;
	}
	public void setSeries(List<MainEventSeries> series) {
		this.series = series;
	}
	public String getMainEventType() {
		return mainEventType;
	}
	public void setMainEventType(String mainEventType) {
		this.mainEventType = mainEventType;
	}
	public String getSubType() {
		return subType;
	}
	public void setSubType(String subType) {
		this.subType = subType;
	}
	public String getLinkUrl() {
		return linkUrl;
	}
	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}
}
