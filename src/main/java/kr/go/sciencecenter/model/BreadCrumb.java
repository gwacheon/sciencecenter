package kr.go.sciencecenter.model;

import java.util.ArrayList;
import java.util.List;

public class BreadCrumb {
	
	private static String PREFIX = "main.nav.catetory";
	
	String mainCategory;
	int mainCategoryNumber;
	String subCategory;
	int subCategoryNumber;
	String subCategoryUrl;
	String subChild;
	int subChildNumber;
	String subChildUrl;
	String hiddenChild;
	
	boolean dynamicEvent;
	int mainEventId;
	
	public BreadCrumb() {
		
	}
	
	public BreadCrumb(int mainCategoryNumber) {
		this.mainCategoryNumber = mainCategoryNumber;
		this.setMainCategory(PREFIX + mainCategoryNumber);
	}
	
	public BreadCrumb(int mainCategoryNumber, int subCategoryNumber) {
		this.mainCategoryNumber = mainCategoryNumber;
		this.setMainCategory(PREFIX + mainCategoryNumber);
		this.subCategoryNumber = subCategoryNumber;
		this.setSubCategory(this.mainCategory + ".sub" + subCategoryNumber);
	}
	
	
	public BreadCrumb(int mainCategoryNumber, int subCategoryNumber, int subChildNumber) {
		this.mainCategoryNumber = mainCategoryNumber;
		this.setMainCategory(PREFIX + mainCategoryNumber);
		this.setSubCategory(this.mainCategory + ".sub" + subCategoryNumber);
		this.setSubChild(this.subCategory + ".sub" + subChildNumber);
		this.setSubChildUrl(this.subChild + ".url");
	}
	
	public int getMainCategoryNumber() {
		return mainCategoryNumber;
	}

	public void setMainCategoryNumber(int mainCategoryNumber) {
		this.mainCategoryNumber = mainCategoryNumber;
		this.setMainCategory(PREFIX + mainCategoryNumber);
	}

	public String getMainCategory() {
		return mainCategory;
	}

	public void setMainCategory(String mainCategory) {
		this.mainCategory = mainCategory;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}
	
	public int getSubCategoryNumber() {
		return subCategoryNumber;
	}

	public void setSubCategoryNumber(int subCategoryNumber) {
		this.subCategoryNumber = subCategoryNumber;
		this.setSubCategory(this.getMainCategory() + ".sub" + subCategoryNumber);
		this.setSubCategoryUrl(this.subCategory + ".url");
	}
	
	public String getSubCategoryUrl() {
		return subCategoryUrl;
	}

	public void setSubCategoryUrl(String subCategoryUrl) {
		this.subCategoryUrl = subCategoryUrl;
	}

	public String getSubChild() {
		return subChild;
	}

	public void setSubChild(String subChild) {
		this.subChild = subChild;
	}
	
	public int getSubChildNumber() {
		return subChildNumber;
	}

	public void setSubChildNumber(int subChildNumber) {
		this.subChildNumber = subChildNumber;
		this.setSubChild(this.getSubCategory() + ".sub" + subChildNumber);
		this.setSubChildUrl(this.subChild + ".url");
	}
	
	public String getSubChildUrl() {
		return subChildUrl;
	}

	public void setSubChildUrl(String subChildUrl) {
		this.subChildUrl = subChildUrl;
	}

	public void setBreadCrumbs(List<String> breadCrumbs) {
		this.breadCrumbs = breadCrumbs;
	}

	public boolean isDynamicEvent() {
		return dynamicEvent;
	}

	public void setDynamicEvent(boolean dynamicEvent) {
		this.dynamicEvent = dynamicEvent;
	}

	public String getHiddenChild() {
		return hiddenChild;
	}

	public void setHiddenChild(String hiddenChild) {
		this.hiddenChild = hiddenChild;
	}

	
	public int getMainEventId() {
		return mainEventId;
	}

	public void setMainEventId(int mainEventId) {
		this.mainEventId = mainEventId;
	}

	List<String> breadCrumbs;
	
	public void addBreadCrumb(String crumb) {
		if (breadCrumbs == null) {
			breadCrumbs = new ArrayList<String>();
		}
		
		breadCrumbs.add(crumb);
	}
	
	public void getBreadCrumbs() {
		StringBuffer sb = new StringBuffer();
		
		if (breadCrumbs != null){
			int length = breadCrumbs.size();
			
			for(int i = 0; i < breadCrumbs.size(); i++){
				if (i < (length - 1)) {
					sb.append("<a href=''>");
					sb.append(breadCrumbs.get(i));
					sb.append("</a>");
					
					sb.append(" > ");
				}else {
					sb.append(breadCrumbs.get(i));
				}
			}
		}
		
		
	}
	
	
	
}
