package kr.go.sciencecenter.model;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class BoardType {
	
	
	public static final HashMap<String, BoardType> BOARD_TYPES = new LinkedHashMap<String, BoardType>();

	static {
		// 1. 관람안내 게시판 없음
		// 2. 전시관람 전시물 리뷰 게시판 사라짐
		//BOARD_TYPES.put("exhibitionReview", new BoardType("exhibitionReview", false, "display", "전시물리뷰", "", true, false, true));
		// 3. 과학교육
		BOARD_TYPES.put("educationNotice", new BoardType("educationNotice", false, "education", "교육공지", "", false, false, false));
		BOARD_TYPES.put("classBoard", new BoardType("classBoard", false, "education", "수업게시판", "", false, false, false));
		BOARD_TYPES.put("fusionScience", new BoardType("fusionScience", false, "education", "과학융합탐구", "", false, false, false));
		BOARD_TYPES.put("reverseScience", new BoardType("reverseScience", false, "education", "거꾸로 과학탐구 교실", "", false, false, false));
		BOARD_TYPES.put("tree", new BoardType("tree", false, "education", "생각트리 과학탐구 교실", "", false, false, false));
		BOARD_TYPES.put("educationArchieve", new BoardType("educationArchieve", false, "education", "과학교육 자료실", "", false, false, false));
		// 4. 문화행사
		// 메인 행사 에 붙는 Board Type
		BOARD_TYPES.put("mainEventSeriesBasicInfo", new BoardType("mainEventSeriesBasicInfo", false, "mainEvent", "", "", false, false, false));
		BOARD_TYPES.put("mainEventSeriesNotice", new BoardType("mainEventSeriesNotice", false, "mainEvent", "", "", false, false, false));
		BOARD_TYPES.put("mainEventSeriesQna", new BoardType("mainEventSeriesQna", false, "mainEvent", "", "", false, false, false, true));
		BOARD_TYPES.put("mainEventSeriesFaq", new BoardType("mainEventSeriesFaq", false, "mainEvent", "", "", false, false, false));
		BOARD_TYPES.put("mainEventSeriesLibrary", new BoardType("mainEventSeriesLibrary", false, "mainEvent", "", "", false, false, false));
		BOARD_TYPES.put("mainEventSeriesPictures", new BoardType("mainEventSeriesPictures", false, "mainEvent", "", "", true, false, false));
		BOARD_TYPES.put("mainEventSeriesWinning", new BoardType("mainEventSeriesWinning", false, "mainEvent", "", "", true, false, false));
		BOARD_TYPES.put("mainEventSeriesRecommend", new BoardType("mainEventSeriesRecommend", false, "mainEvent", "", "", true, false, false));
		// 문화행사의 특별기획전 (마지막에 추가됨 )
		BOARD_TYPES.put("specialEvent", new BoardType("specialEvent", false, "events", "특별기획전", "", false, false, false));
		
		// 5. 예약신청 게시판 없음
		// 6.고객소통
		BOARD_TYPES.put("lostProperty", new BoardType("lostProperty", false, "communication", "분실물 목록", "", false, false, false));
		BOARD_TYPES.put("faqTotal", new BoardType("faqTotal", false, "communication", "전체 FAQ", "", false, false, false));
		BOARD_TYPES.put("faqScience", new BoardType("faqScience", false, "communication", "과학행사", "", false, false, false));
		BOARD_TYPES.put("faqEducation", new BoardType("faqEducation", false, "communication", "교육", "", false, false, false));
		BOARD_TYPES.put("faqRole", new BoardType("faqRole", false, "communication", "비영리법인", "", false, false, false));
		BOARD_TYPES.put("faqFacilities", new BoardType("faqFacilities", false, "communication", "시설", "", false, false, false));
		BOARD_TYPES.put("faqRestaurant", new BoardType("faqRestaurant", false, "communication", "식당", "", false, false, false));
		BOARD_TYPES.put("faqUser", new BoardType("faqUser", false, "communication", "연간회원", "", false, false, false));
		BOARD_TYPES.put("faqDisplay", new BoardType("faqDisplay", false, "communication", "전시 관람", "", false, false, false));
		BOARD_TYPES.put("faqReservation", new BoardType("faqReservation", false, "communication", "전시관예약", "", false, false, false));
		BOARD_TYPES.put("faqPark", new BoardType("faqPark", false, "communication", "주차", "", false, false, false));
		BOARD_TYPES.put("faqHomepage", new BoardType("faqHomepage", false, "communication", "홈페이지", "", false, false, false));
		BOARD_TYPES.put("faqMarketing", new BoardType("faqMarketing", false, "communication", "홍보자료", "", false, false, false));				
		BOARD_TYPES.put("normalLibrary", new BoardType("normalLibrary", false, "communication", "일반자료실", "", false, false, false));
		BOARD_TYPES.put("roleLibrary", new BoardType("roleLibrary", false, "communication", "관련규정자료실", "", false, false, false));
		BOARD_TYPES.put("scienceColumn", new BoardType("scienceColumn", false, "communication", "과학컬럼", "", false, true, false));
		BOARD_TYPES.put("opinions", new BoardType("opinions", false, "communication", "의견수렴", "", false, false, false, true));
		// 7. 과학관소개
		BOARD_TYPES.put("notice", new BoardType("notice", false, "introduce", "공지/공고", "", false, false, false));
		BOARD_TYPES.put("news", new BoardType("news", true, "introduce", "소식지", "", true, false, false));
		BOARD_TYPES.put("report", new BoardType("report", false, "introduce", "보도자료", "", false, false, false));
		BOARD_TYPES.put("sketch", new BoardType("sketch", true, "introduce", "현장스케치상", "/introduce/sketch?", true, false, false));
		BOARD_TYPES.put("programIntro", new BoardType("programIntro", true, "introduce", "과학관UCC", "", true, false, false));
		
		//BOARD_TYPES.put("chiefHistory", new BoardType("chiefHistory", false, "introduce", "기관장 동정", "", false, false, false));
		// 8. 정보공개
		BOARD_TYPES.put("sharedInfoArchive", new BoardType("sharedInfoArchive", false, "government", "공개정보자료실", "", false, false, false));

		//사이버과학관
//		BOARD_TYPES.put("digitalEducation", new BoardType("digitalEducation", true, "cyber", "디지털 교육관", "", true, true, false));
//		BOARD_TYPES.put("sciencePlay", new BoardType("sciencePlay", true, "cyber", "과학놀이터", "", true, true, false));
	};

	String name;
	boolean isGallary;
	String category;
	String nameAdmin;
	String path;
	boolean hasMainPicture;
	boolean isUrlType;
	boolean hasSubTitle;
	boolean replied;

	public BoardType(String name, boolean isGallary, String category, String nameAdmin, String path, boolean hasMainPicture,
			boolean isUrlType, boolean hasSubTitle) {

		setName(name);
		setGallary(isGallary);
		setCategory(category);
		setNameAdmin(nameAdmin);
		setPath(path);
		setHasMainPicture(hasMainPicture);
		setUrlType(isUrlType);
		setHasSubTitle(hasSubTitle);
		setReplied(false);
	}
	
	public BoardType(String name
			, boolean isGallary
			, String category
			, String nameAdmin
			, String path
			, boolean hasMainPicture
			, boolean isUrlType
			, boolean hasSubTitle
			, boolean replied) {
		
		setName(name);
		setGallary(isGallary);
		setCategory(category);
		setNameAdmin(nameAdmin);
		setPath(path);
		setHasMainPicture(hasMainPicture);
		setUrlType(isUrlType);
		setHasSubTitle(hasSubTitle);
		setReplied(replied);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isGallary() {
		return isGallary;
	}

	public void setGallary(boolean isGallary) {
		this.isGallary = isGallary;
	}

	public String getNameAdmin() {
		return nameAdmin;
	}

	public void setNameAdmin(String nameAdmin) {
		this.nameAdmin = nameAdmin;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public boolean isHasMainPicture() {
		return hasMainPicture;
	}

	public void setHasMainPicture(boolean hasMainPicture) {
		this.hasMainPicture = hasMainPicture;
	}

	public boolean isUrlType() {
		return isUrlType;
	}

	public void setUrlType(boolean isUrlType) {
		this.isUrlType = isUrlType;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public boolean isHasSubTitle() {
		return hasSubTitle;
	}

	public void setHasSubTitle(boolean hasSubTitle) {
		this.hasSubTitle = hasSubTitle;
	}

	public boolean isReplied() {
		return replied;
	}

	public void setReplied(boolean replied) {
		this.replied = replied;
	}
}
