package kr.go.sciencecenter.model;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.util.List;



public class User implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String AUTHENTICATION = "authentication";
	public static final String RESET_PASSWORD = "reset_password";
	
	private int id;
	private String email;
	private String password;
	private String passwordConfirmation;
	private String name;
	private String phone;
	private String authentication_token;
	private String resetPasswordToken;
	private int confirmed;
	
	private List<UserRole> userRoles;
	
	private int chargedMember;
	
	private String zipcodeNew;
	
	private String address1;
	private String address2;
	
	
	private Date last_sign_in_at;
	
	public Date getLast_sign_in_at() {
    return last_sign_in_at;
  }

  public void setLast_sign_in_at(Date last_sign_in_at) {
    this.last_sign_in_at = last_sign_in_at;
  }

  public String getAddress1() {
    return address1;
  }

  public void setAddress1(String address1) {
    this.address1 = address1;
  }

  public String getAddress2() {
    return address2;
  }

  public void setAddress2(String address2) {
    this.address2 = address2;
  }

  public String getZipcodeNew() {
    return zipcodeNew;
  }

  public void setZipcodeNew(String zipcodeNew) {
    this.zipcodeNew = zipcodeNew;
  }

  public int getChargedMember() {
    return chargedMember;
  }

  public void setChargedMember(int chargedMember) {
    this.chargedMember = chargedMember;
  }
	
	public User(){
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getConfirmed() {
		return confirmed;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordConfirmation() {
		return passwordConfirmation;
	}

	public void setPasswordConfirmation(String passwordConfirmation) {
		this.passwordConfirmation = passwordConfirmation;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAuthenticationToken() {
		return authentication_token;
	}

	public void setAuthenticationToken(String authenticationToken) {
		this.authentication_token = authenticationToken;
	}

	public String getResetPasswordToken() {
		return resetPasswordToken;
	}

	public void setResetPasswordToken(String resetPasswordToken) {
		this.resetPasswordToken = resetPasswordToken;
	}

	public void setConfirmed(int confirmed) {
		this.confirmed = confirmed;
	}
	
	public List<UserRole> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(List<UserRole> userRoles) {
		this.userRoles = userRoles;
	}

	public void makeAuthenticationToken(String authType) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			md.reset();
			
			byte[] buffer = (this.email + System.currentTimeMillis()).getBytes(); 
			md.update(buffer);
			
			byte[] mdbytes = md.digest();
			StringBuffer hexString = new StringBuffer();
	    	for (int i=0;i<mdbytes.length;i++) {
	    	  hexString.append(Integer.toHexString(0xFF & mdbytes[i]));
	    	}
	    	
	    	if(authType.equals(User.AUTHENTICATION)){
	    		this.authentication_token = hexString.toString();
	    	}else if(authType.equals(User.RESET_PASSWORD)){
	    		this.authentication_token = hexString.toString();
	    	}
	    	
	    	
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
