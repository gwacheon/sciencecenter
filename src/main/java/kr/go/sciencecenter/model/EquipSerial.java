package kr.go.sciencecenter.model;

import java.util.Date;
import java.util.List;

public class EquipSerial {
	int id;
	int equipmentId;
	String serial;
	String status;
	Date regDttm;
	Date updtDttm;
	
	List<EquipReserve> equipReserves; 
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getEquipmentId() {
		return equipmentId;
	}
	public void setEquipmentId(int equipmentId) {
		this.equipmentId = equipmentId;
	}
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(Date regDttm) {
		this.regDttm = regDttm;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
	public List<EquipReserve> getEquipReserves() {
		return equipReserves;
	}
	public void setEquipReserves(List<EquipReserve> equipReserves) {
		this.equipReserves = equipReserves;
	}
}
