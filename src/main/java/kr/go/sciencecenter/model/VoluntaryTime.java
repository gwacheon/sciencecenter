package kr.go.sciencecenter.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

public class VoluntaryTime {
	int id;
	int voluntaryId;
	String title;
	String content;
	Date beginTime;
	Date endTime;
	Date regDttm;
	Date updtDttm;
	
	int applicationMembers;
	
	Voluntary voluntary;
	
	List<Volunteer> volunteers;
	
	public VoluntaryTime(){
		
	}
	
	public VoluntaryTime(int voluntaryId, JSONObject jsonObj) {
		this.voluntaryId = voluntaryId;
		this.title = jsonObj.getString("title");
		this.content = jsonObj.getString("content");
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd h:mm a", Locale.US);
		
		try {
			this.beginTime = dateFormat.parse(jsonObj.getString("beginTime"));
			this.endTime = dateFormat.parse(jsonObj.getString("endTime"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getVoluntaryId() {
		return voluntaryId;
	}
	public void setVoluntaryId(int voluntaryId) {
		this.voluntaryId = voluntaryId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public Date getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(Date regDttm) {
		this.regDttm = regDttm;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
	
	public int getApplicationMembers() {
		return applicationMembers;
	}

	public void setApplicationMembers(int applicationMembers) {
		this.applicationMembers = applicationMembers;
	}

	public Voluntary getVoluntary() {
		return voluntary;
	}
	public void setVoluntary(Voluntary voluntary) {
		this.voluntary = voluntary;
	}

	public List<Volunteer> getVolunteers() {
		return volunteers;
	}

	public void setVolunteers(List<Volunteer> volunteers) {
		this.volunteers = volunteers;
	}
}
