package kr.go.sciencecenter.model;

import java.util.Date;

import org.json.JSONObject;

public class CareerSchedule {
	int id;
	int careerId;
	String title;
	Date csDate;
	boolean available;
	int seq;
	
	String tueWord;
	boolean tueAvailable;
	String wedWord;
	boolean wedAvailable;
	String thuWord;
	boolean thuAvailable;
	String friWord;
	boolean friAvailable;
	
	Date regDttm;
	Date updtDttm;
	
	public CareerSchedule(){
		
	}
	
	public CareerSchedule(int careerId, JSONObject jsonObj) {
		this.careerId = careerId;
		
		this.tueWord = jsonObj.getString("tueWord");
		if(!this.tueWord.equals("") && jsonObj.getBoolean("tueAvailable")){
			this.tueAvailable = true;
		}
		this.wedWord = jsonObj.getString("wedWord");
		if(!this.wedWord.equals("") && jsonObj.getBoolean("wedAvailable")){
			this.wedAvailable = true;
		}
		this.thuWord = jsonObj.getString("thuWord");
		if(!this.thuWord.equals("") && jsonObj.getBoolean("thuAvailable")){
			this.thuAvailable = true;
		}
		this.friWord = jsonObj.getString("friWord");
		if(!this.friWord.equals("") && jsonObj.getBoolean("friAvailable")){
			this.friAvailable = true;
		}
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCareerId() {
		return careerId;
	}
	public void setCareerId(int careerId) {
		this.careerId = careerId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Date getCsDate() {
		return csDate;
	}
	public void setCsDate(Date csDate) {
		this.csDate = csDate;
	}
	public boolean isAvailable() {
		return available;
	}
	public void setAvailable(boolean available) {
		this.available = available;
	}
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	public String getTueWord() {
		return tueWord;
	}
	public void setTueWord(String tueWord) {
		this.tueWord = tueWord;
	}
	public boolean isTueAvailable() {
		return tueAvailable;
	}
	public void setTueAvailable(boolean tueAvailable) {
		this.tueAvailable = tueAvailable;
	}
	public String getWedWord() {
		return wedWord;
	}
	public void setWedWord(String wedWord) {
		this.wedWord = wedWord;
	}
	public boolean isWedAvailable() {
		return wedAvailable;
	}
	public void setWedAvailable(boolean wedAvailable) {
		this.wedAvailable = wedAvailable;
	}
	public String getThuWord() {
		return thuWord;
	}
	public void setThuWord(String thuWord) {
		this.thuWord = thuWord;
	}
	public boolean isThuAvailable() {
		return thuAvailable;
	}
	public void setThuAvailable(boolean thuAvailable) {
		this.thuAvailable = thuAvailable;
	}
	public String getFriWord() {
		return friWord;
	}
	public void setFriWord(String friWord) {
		this.friWord = friWord;
	}
	public boolean isFriAvailable() {
		return friAvailable;
	}
	public void setFriAvailable(boolean friAvailable) {
		this.friAvailable = friAvailable;
	}
	public Date getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(Date regDttm) {
		this.regDttm = regDttm;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
}
