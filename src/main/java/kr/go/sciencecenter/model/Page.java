package kr.go.sciencecenter.model;

public class Page {
	private static final int DEFAULT_PAGE_COUNT = 10;
	private static final int SIDE_PAGE_COUNT = 3;
	
	int currentPage;
	int perPage;
	int totalItems;
	String className;
	String path;
	
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	public int getTotalItems() {
		return totalItems;
	}
	public void setTotalItems(int totalItems) {
		this.totalItems = totalItems;
	}
	public int getPerPage() {
		return perPage;
	}
	public void setPerPage(int perPage) {
		this.perPage = perPage;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getClassName(){
		return this.className;
	}
	public void setClassName(String className){
		this.className = className;
	}
	public int getOffset(){
		if(this.currentPage > 1){
			return (currentPage - 1) * this.perPage;
		}else{
			return 0;
		}
	}
	
	public int getLimit(){
		return this.getOffset() + this.perPage;
	}
	
	public int getTotalPages(){
		if(this.totalItems % this.perPage == 0){
			return this.totalItems / this.perPage; 
		}else{
			return (this.totalItems / this.perPage) + 1;
		}
	}
	public int getBeginPage(){
		if(this.getTotalPages() <= DEFAULT_PAGE_COUNT || this.currentPage <= SIDE_PAGE_COUNT){
			return 1;
		}else{
			return this.currentPage - SIDE_PAGE_COUNT;
		}
	}
	public int getEndPage(){
		if(this.getTotalPages() <= DEFAULT_PAGE_COUNT){
			return this.getTotalPages();
		}else if(this.getTotalPages() - this.currentPage <= SIDE_PAGE_COUNT){
			return this.getTotalPages();
		}else{
			return this.currentPage + SIDE_PAGE_COUNT;
		}
	}
	
	public Page(){
		
	}
	
	public Page(int currentPage, int perPage){
		this.currentPage = currentPage;
		this.perPage = perPage;
	}
	
	public Page(int currentPage, int perPage, int totalItems){
		this.currentPage = currentPage;
		this.perPage = perPage;
		this.totalItems = totalItems;
	}
}
