package kr.go.sciencecenter.model;

import java.util.Date;

public class EquipUnavailable {
    int id;
    int equipmentId;
    int equiSerialId;
    Date beginTime;
    Date regDttm;
    Date updtDttm;
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getEquipmentId() {
        return equipmentId;
    }
    public void setEquipmentId(int equipmentId) {
        this.equipmentId = equipmentId;
    }
    public int getEquiSerialId() {
        return equiSerialId;
    }
    public void setEquiSerialId(int equiSerialId) {
        this.equiSerialId = equiSerialId;
    }
    public Date getBeginTime() {
        return beginTime;
    }
    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }
    public Date getRegDttm() {
        return regDttm;
    }
    public void setRegDttm(Date regDttm) {
        this.regDttm = regDttm;
    }
    public Date getUpdtDttm() {
        return updtDttm;
    }
    public void setUpdtDttm(Date updtDttm) {
        this.updtDttm = updtDttm;
    }
}
