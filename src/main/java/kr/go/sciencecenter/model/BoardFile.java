package kr.go.sciencecenter.model;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.springframework.web.multipart.MultipartFile;

public class BoardFile {
    int id;
    int boardId;
    String name;
    String url;
    MultipartFile file;
    
    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getUrl() {
	return url;
    }

    public void setUrl(String url) {
	this.url = url;
    }

    public MultipartFile getFile() {
	return file;
    }

    public void setFile(MultipartFile file) {
	this.file = file;
    }

    public int getBoardId() {
        return boardId;
    }

    public void setBoardId(int boardId) {
        this.boardId = boardId;
    }
    
//    public void saveAttatchmentFile(String uploadPath, int boardId){
		
//    	if (this.getFile() != null && this.getFile().getSize() > 0) {
//			
//			String filePath = baseFilePath + uploadPath + boardId + "/";
//			File dir = new File(filePath);
//			if (!dir.exists()) {
//				dir.mkdirs();
//			}
//			try {
//				this.getFile().transferTo(new File(filePath + this.getFile().getOriginalFilename()));
//				
//			} catch (IllegalStateException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//	}
    
    public boolean deleteAttachmentFile(String baseFilePath, String uploadPath) {
    	
    	
    	
		String filePath = baseFilePath + uploadPath + this.getBoardId() + "/" + this.getName();

		File dir = new File(filePath);
		if( dir.exists() && dir.delete()) {
			
			return true;
		}
		else {
			return false;
		}
	}
    
//    public static void deleteAllAttachmentFiles(String baseFilePath, String uploadPath, int boardId) {
//		
//		String filePath = baseFilePath;
//		filePath = filePath + uploadPath + boardId + "/";
//		
//		File dir = new File(filePath);
//		if( dir.exists() ) {
//			try {
//				FileUtils.deleteDirectory(dir);
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//	}
    
    
}
