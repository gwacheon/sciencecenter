package kr.go.sciencecenter.model;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import kr.go.sciencecenter.util.PropertyHelper;

/**
 * 대관 시설물 Model
 */
public class Structure {
	public static final String FILE_PREFIX = "STRUCTURES";
	public static final String DEFAULT_FILE_NAME = "structure";
	public static final String [] CATEGORIES = {
		"display", "camp", "classroom", "lab"
	};
	
    int id;
    String category;
    String title;
    String description;
    String picture;
    Double area;
    String person;
    int priceSpring;
    int priceSummer;
    int priceWinter;
    String etc;
    int seq;

    MultipartFile file;

    Date regDttm;
    Date updtDttm;

    List<StructureUnavailable> structureUnavailables;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Double getArea() {
		return area;
	}

	public void setArea(Double area) {
		this.area = area;
	}

	public String getPerson() {
		return person;
	}

	public void setPerson(String person) {
		this.person = person;
	}

	public int getPriceSpring() {
		return priceSpring;
	}

	public void setPriceSpring(int priceSpring) {
		this.priceSpring = priceSpring;
	}

	public int getPriceSummer() {
		return priceSummer;
	}

	public void setPriceSummer(int priceSummer) {
		this.priceSummer = priceSummer;
	}

	public int getPriceWinter() {
		return priceWinter;
	}

	public void setPriceWinter(int priceWinter) {
		this.priceWinter = priceWinter;
	}

	public String getEtc() {
		return etc;
	}

	public void setEtc(String etc) {
		this.etc = etc;
	}

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public Date getRegDttm() {
		return regDttm;
	}

	public void setRegDttm(Date regDttm) {
		this.regDttm = regDttm;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public List<StructureUnavailable> getStructureUnavailables() {
		return structureUnavailables;
	}

	public void setStructureUnavailables(List<StructureUnavailable> structureUnavailables) {
		this.structureUnavailables = structureUnavailables;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public void uploadFile(String filePath, String contextPath){
		String relativePath = "/resources/upload/structure_picture/";
		
		filePath += relativePath + this.getId() + "/";
		
		File dir = new File(filePath);
		if (dir.exists()) {
			dir.delete();
		}
		dir.mkdirs();
		
		BufferedOutputStream stream = null;
		try {
			File file = new File(filePath + this.getFile().getOriginalFilename());
			stream = new BufferedOutputStream(new FileOutputStream(file));
			stream.write(this.getFile().getBytes());
			this.setPicture(contextPath + relativePath
					+ this.getId() + "/" + this.getFile().getOriginalFilename());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String getUrl(){
		return this.getPicture();
	}
}
