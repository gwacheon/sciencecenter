package kr.go.sciencecenter.model;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.springframework.web.multipart.MultipartFile;

public class EventFile {
    int id;
    int eventId;
    String name;
    String url;
    MultipartFile file;
    
    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getUrl() {
	return url;
    }

    public void setUrl(String url) {
	this.url = url;
    }

    public MultipartFile getFile() {
	return file;
    }

    public void setFile(MultipartFile file) {
	this.file = file;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }
    
    public boolean deleteAttachmentFile(String baseFilePath, String uploadPath) {    	
		String filePath = baseFilePath + uploadPath + this.getEventId() + "/" + this.getName();

		File dir = new File(filePath);
		if( dir.exists() && dir.delete()) {
			
			return true;
		}
		else {
			return false;
		}
	}
    
}
