package kr.go.sciencecenter.model;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class MainEventSeries {
    int id;
    int mainEventId;
    String name;
    String contents;
    int seq;
    
    boolean mainEventSeriesNotice;
    boolean mainEventSeriesQna;
    boolean mainEventSeriesFaq;
    boolean mainEventSeriesLibrary;
    boolean mainEventSeriesPictures;
    boolean mainEventSeriesWinning;
    boolean mainEventSeriesRecommend;
    
    private Map<String, Boolean> boardMap = new LinkedHashMap<String, Boolean>();
    private List<String> visibleBoardNames = new ArrayList<String>();
    
    public void setDefaultBoardMap() {
    	    	
    	boardMap.put("mainEventSeriesNotice", this.mainEventSeriesNotice);
    	boardMap.put("mainEventSeriesQna", this.mainEventSeriesQna);
    	boardMap.put("mainEventSeriesFaq", this.mainEventSeriesFaq);
    	boardMap.put("mainEventSeriesLibrary", this.mainEventSeriesLibrary);
    	boardMap.put("mainEventSeriesPictures", this.mainEventSeriesPictures);
    	boardMap.put("mainEventSeriesWinning", this.mainEventSeriesWinning);
    	boardMap.put("mainEventSeriesRecommend", this.mainEventSeriesRecommend);
    }
    
    public void setDefaultVisibleBoardNames() {
    	Map<String, Boolean> tempMap = new LinkedHashMap<String, Boolean>();
    	
    	tempMap.put("mainEventSeriesNotice", this.mainEventSeriesNotice);
    	tempMap.put("mainEventSeriesQna", this.mainEventSeriesQna);
    	tempMap.put("mainEventSeriesFaq", this.mainEventSeriesFaq);
    	tempMap.put("mainEventSeriesLibrary", this.mainEventSeriesLibrary);
    	tempMap.put("mainEventSeriesPictures", this.mainEventSeriesPictures);
    	tempMap.put("mainEventSeriesWinning", this.mainEventSeriesWinning);
    	tempMap.put("mainEventSeriesRecommend", this.mainEventSeriesRecommend);
    	
    	for( String type : tempMap.keySet() ) {
    		// boolean 값이 true 인 것들만 visibleBoardNames에 넣는다.
    		if( tempMap.get(type) ) {
    			visibleBoardNames.add(type);
    		}
    	}
    }
    
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getMainEventId() {
		return mainEventId;
	}
	public void setMainEventId(int mainEventId) {
		this.mainEventId = mainEventId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	
	

	public boolean isMainEventSeriesNotice() {
		return mainEventSeriesNotice;
	}

	public void setMainEventSeriesNotice(boolean mainEventSeriesNotice) {
		this.mainEventSeriesNotice = mainEventSeriesNotice;
	}

	public boolean isMainEventSeriesQna() {
		return mainEventSeriesQna;
	}

	public void setMainEventSeriesQna(boolean mainEventSeriesQna) {
		this.mainEventSeriesQna = mainEventSeriesQna;
	}

	public boolean isMainEventSeriesFaq() {
		return mainEventSeriesFaq;
	}

	public void setMainEventSeriesFaq(boolean mainEventSeriesFaq) {
		this.mainEventSeriesFaq = mainEventSeriesFaq;
	}

	public boolean isMainEventSeriesLibrary() {
		return mainEventSeriesLibrary;
	}

	public void setMainEventSeriesLibrary(boolean mainEventSeriesLibrary) {
		this.mainEventSeriesLibrary = mainEventSeriesLibrary;
	}

	public boolean isMainEventSeriesPictures() {
		return mainEventSeriesPictures;
	}

	public void setMainEventSeriesPictures(boolean mainEventSeriesPictures) {
		this.mainEventSeriesPictures = mainEventSeriesPictures;
	}

	public boolean isMainEventSeriesWinning() {
		return mainEventSeriesWinning;
	}

	public void setMainEventSeriesWinning(boolean mainEventSeriesWinning) {
		this.mainEventSeriesWinning = mainEventSeriesWinning;
	}

	public boolean isMainEventSeriesRecommend() {
		return mainEventSeriesRecommend;
	}

	public void setMainEventSeriesRecommend(boolean mainEventSeriesRecommend) {
		this.mainEventSeriesRecommend = mainEventSeriesRecommend;
	}

	public Map<String, Boolean> getboardMap() {
		return boardMap;
	}

	public void setVisibleBoardMap(Map<String, Boolean> boardMap) {
		this.boardMap = boardMap;
	}
	public List<String> getVisibleBoardNames() {
		return visibleBoardNames;
	}
	public void setVisibleBoardNames(List<String> visibleBoardNames) {
		this.visibleBoardNames = visibleBoardNames;
	}
	
	
    
}
