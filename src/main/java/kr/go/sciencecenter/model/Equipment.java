package kr.go.sciencecenter.model;

import java.util.Date;
import java.util.List;

public class Equipment {
	public static final String EQUIPMENT_FILE_PREFIX = "EQUIPMENT";
	
	int id;
	String name;
	String modelName;
	String description;
	String picture;
	String status;
	String type;
	Date regDttm;
	Date updtDttm;
	
	List<EquipSerial> equipSerials;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getPicture() {
	    return picture;
	}
	public void setPicture(String picture) {
	    this.picture = picture;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(Date regDttm) {
		this.regDttm = regDttm;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
	
	public List<EquipSerial> getEquipSerials() {
		return equipSerials;
	}
	public void setEquipSerials(List<EquipSerial> equipSerials) {
		this.equipSerials = equipSerials;
	}
  public String getType() {
    return type;
  }
  public void setType(String type) {
    this.type = type;
  }
}
