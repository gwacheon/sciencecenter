package kr.go.sciencecenter.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Policy {
	int id;
	String type;
	String content;
	boolean mostRecent;
	Date regDttm;
	Date updtDttm;
	
	private Map<String,String> typeMap = new HashMap<String,String>();
	

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(Date regDttm) {
		this.regDttm = regDttm;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
	
	public boolean isMostRecent() {
		return mostRecent;
	}
	public void setMostRecent(boolean mostRecent) {
		this.mostRecent = mostRecent;
	}
	public Map<String, String> getTypeMap() {
		return typeMap;
	}
	public void setTypeMap(Map<String, String> typeMap) {
		this.typeMap = typeMap;
	}
	
	public void initTypeMap() {
		typeMap.put("policy", "이용약관");
		typeMap.put("media", "영상정보처리기기 운영/관리 방침");
		typeMap.put("privacy", "개인정보 취급방침");
	}
	
}


