package kr.go.sciencecenter.model;

import java.util.Date;
import java.util.List;

import org.json.JSONObject;

public class Article {
	int id;
	int surveyId;
	String question;
	String qType;
	String renderType;
	int seq;
	Date regDttm;
	Date updtDttm;
	
	List<Selection> selections;
	
	public Article(){
		
	}
	
	public Article(JSONObject jsonObject) {
		this.surveyId = jsonObject.getInt("survey_id");
		this.question = jsonObject.getString("title");
		this.qType = jsonObject.getString("q_type");
		this.renderType = jsonObject.getString("render_type");
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getSurveyId() {
		return surveyId;
	}
	public void setSurveyId(int surveyId) {
		this.surveyId = surveyId;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getqType() {
		return qType;
	}
	public void setqType(String qType) {
		this.qType = qType;
	}
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	public Date getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(Date regDttm) {
		this.regDttm = regDttm;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public List<Selection> getSelections() {
		return selections;
	}

	public void setSelections(List<Selection> selections) {
		this.selections = selections;
	}

	public String getRenderType() {
		return renderType;
	}

	public void setRenderType(String renderType) {
		this.renderType = renderType;
	}
	
}
