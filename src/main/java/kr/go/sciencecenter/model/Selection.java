package kr.go.sciencecenter.model;

import java.util.Date;

import org.json.JSONObject;

public class Selection {
	int id;
	int articleId;
	String example;
	int seq;
	boolean directInput;
	Date regDttm;
	Date updtDttm;
	
	public Selection(){
		
	}
	
	public Selection(JSONObject jsonObject) {
		this.seq = jsonObject.getInt("seq");
		this.example = jsonObject.getString("example");
		this.directInput = jsonObject.getBoolean("direct_input");
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getArticleId() {
		return articleId;
	}
	public void setArticleId(int articleId) {
		this.articleId = articleId;
	}
	public String getExample() {
		return example;
	}
	public void setExample(String example) {
		this.example = example;
	}
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	public Date getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(Date regDttm) {
		this.regDttm = regDttm;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
	public boolean isDirectInput() {
		return directInput;
	}

	public void setDirectInput(boolean directInput) {
		this.directInput = directInput;
	}
}
