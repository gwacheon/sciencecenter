package kr.go.sciencecenter.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Holiday {
	public enum TimeType{
		MORNING(1),
		AFTERNOON(2),
		EVENING(3),
		ALL(4);
		
		private final int value; 
		private TimeType(final int value){
			this.value = value;
		}
		
		public int getValue() { return value; }
	}
	
	int id;
	Date holiday;
	int timeType;
	String name;
	
	static Map<TimeType, Integer> timesMap;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getHoliday() {
		return holiday;
	}
	public void setHoliday(Date holiday) {
		this.holiday = holiday;
	}
	public int getTimeType() {
		return timeType;
	}
	public void setTimeType(int timeType) {
		this.timeType = timeType;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
