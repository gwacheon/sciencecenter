package kr.go.sciencecenter.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONObject;

public class StructureUnavailable {
	int id;
	int structureId;
	Date beginTime;
	Date endTime;
	Date regDttm;
	Date updtDttm;
	
	public StructureUnavailable(){
		
	}
	
	public StructureUnavailable(int structureId, String datesStr) {
		JSONObject obj = new JSONObject(datesStr);
		this.structureId = structureId;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		
		try {
			this.beginTime = formatter.parse(obj.getString("beginTime"));
			this.endTime = formatter.parse(obj.getString("endTime"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getStructureId() {
		return structureId;
	}
	public void setStructureId(int structureId) {
		this.structureId = structureId;
	}
	public Date getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Date getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(Date regDttm) {
		this.regDttm = regDttm;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}	
}
