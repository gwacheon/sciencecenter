package kr.go.sciencecenter.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

public class SangSangSchedule {
	int id;
	int sangsangScheduleId;
	String title;
	Date sDate;
	Date regDttm;
	Date updtDttm;
	
	public SangSangSchedule(){
		
	}
	
	public SangSangSchedule(JSONObject jsonObject) {
		try {
			this.title = jsonObject.getString("title");
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			this.setsDate(formatter.parse(jsonObject.getString("date")));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getSangsangScheduleId() {
		return sangsangScheduleId;
	}
	public void setSangsangScheduleId(int sangsangScheduleId) {
		this.sangsangScheduleId = sangsangScheduleId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Date getsDate() {
		return sDate;
	}
	public void setsDate(Date sDate) {
		this.sDate = sDate;
	}
	public Date getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(Date regDttm) {
		this.regDttm = regDttm;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
	
	
}
