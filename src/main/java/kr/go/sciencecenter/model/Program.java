package kr.go.sciencecenter.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.antlr.tool.Grammar.LabelElementPair;

public class Program implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final Map<String, List<String>> LOCATION_MAP;
	static{
		LOCATION_MAP = new LinkedHashMap<String, List<String>>();
		LOCATION_MAP.put("전시 및 공연", new ArrayList<String>(
			    Arrays.asList("상상홀", "창조홀", "어울림홀", "특별전시관")));
		LOCATION_MAP.put("캠프장 숙소", new ArrayList<String>(
			    Arrays.asList("캠프장숙소1인실(1층)", "캠프장숙소1인실(3층)", "캠프장숙소2인실", "캠프장숙소4인실")));
		LOCATION_MAP.put("강의실", new ArrayList<String>(
			    Arrays.asList("대강의실", "강의실2", "강의실3", "강의실4", "강의실5", "강의실6")));
		LOCATION_MAP.put("실험실", new ArrayList<String>(
			    Arrays.asList("실험실1", "실험실2", "실험실3", "실험실4", "실험실5", "실험실6", "실험실7", "실험실8")));
		LOCATION_MAP.put("회의실", new ArrayList<String>(
			    Arrays.asList("소회의실", "미래실", "창의실")));
		LOCATION_MAP.put("전시장", new ArrayList<String>(
			    Arrays.asList("첨단기술1관", "첨단기술1관 생명과학실험실",
			    		"첨단기술1관 로봇체험관", "첨단기술2관", "첨단기술2관 공작실",
			    		"자연사관 탐구교실", "기초과학관", "기초과학관 실험실",
			    		"전통과학관", "자연사관", "어린이탐구체험관 호기심방",
			    		"상설전시장", "생태공원", "중앙홀", "천체관측소")));
		LOCATION_MAP.put("무한상상실", new ArrayList<String>(
			    Arrays.asList("무한상상실")));
		LOCATION_MAP.put("곤충생태관", new ArrayList<String>(
			    Arrays.asList("곤충생태관 곤충교실")));
	}
	
	public static final List<String> ADDITIONAL_INFOS;
	static{
		ADDITIONAL_INFOS = new ArrayList<String>();
		ADDITIONAL_INFOS.add("성명");
		ADDITIONAL_INFOS.add("학생명");
		ADDITIONAL_INFOS.add("생년월일");
		ADDITIONAL_INFOS.add("학교명");
		ADDITIONAL_INFOS.add("연수지명번호");
		ADDITIONAL_INFOS.add("학교주소");
		ADDITIONAL_INFOS.add("학년");
		ADDITIONAL_INFOS.add("반");
		ADDITIONAL_INFOS.add("학과");
		ADDITIONAL_INFOS.add("번호");
		ADDITIONAL_INFOS.add("학번");
		ADDITIONAL_INFOS.add("보호자명");
		ADDITIONAL_INFOS.add("휴대폰");
		ADDITIONAL_INFOS.add("보호자휴대폰");
		ADDITIONAL_INFOS.add("주소");
		ADDITIONAL_INFOS.add("숙소신청");
		ADDITIONAL_INFOS.add("기타");
		ADDITIONAL_INFOS.add("성별");
		ADDITIONAL_INFOS.add("이메일");
		ADDITIONAL_INFOS.add("영문이름");
	}
	
	int id;
	int groupId;
	String programNo;
	String title;
	String content;
	boolean ageLimited;
	String ageType;
	int ageBegin;
	int ageEnd;
	boolean homepageReserved;
	int requisitionDay;
	
	Date refundDttm;
	Date beginTime;
	Date endTime;
	Date regDttm;
	Date updtDttm;
	
	int price;
	int maxMember;
	int waitMember;
	
	String locationCategory1;
	String locationCategory2;
	String location;
	
	ProgramGroup group;
	
	List<Integer> wdays;
	List<ProgramTime> times;
	List<String> addInfos;
	List<ProgramAddInfo> infos;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getGroupId() {
		return groupId;
	}
	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}
	public String getProgramNo() {
		return programNo;
	}
	public void setProgramNo(String programNo) {
		this.programNo = programNo;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public boolean isAgeLimited() {
		return ageLimited;
	}
	public void setAgeLimited(boolean ageLimited) {
		this.ageLimited = ageLimited;
	}
	public String getAgeType() {
		return ageType;
	}
	public void setAgeType(String ageType) {
		this.ageType = ageType;
	}
	public int getAgeBegin() {
		return ageBegin;
	}
	public void setAgeBegin(int ageBegin) {
		this.ageBegin = ageBegin;
	}
	public int getAgeEnd() {
		return ageEnd;
	}
	public void setAgeEnd(int ageEnd) {
		this.ageEnd = ageEnd;
	}
	public boolean isHomepageReserved() {
		return homepageReserved;
	}
	public void setHomepageReserved(boolean homepageReserved) {
		this.homepageReserved = homepageReserved;
	}
	public Date getRefundDttm() {
		return refundDttm;
	}
	public void setRefundDttm(Date refundDttm) {
		this.refundDttm = refundDttm;
	}
	public Date getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public Date getRegDttm() {
		return regDttm;
	}
	public void setRegDttm(Date regDttm) {
		this.regDttm = regDttm;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
	
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getMaxMember() {
		return maxMember;
	}
	public void setMaxMember(int maxMember) {
		this.maxMember = maxMember;
	}
	public int getWaitMember() {
		return waitMember;
	}
	public void setWaitMember(int waitMember) {
		this.waitMember = waitMember;
	}
	public String getLocationCategory1() {
		return locationCategory1;
	}
	public void setLocationCategory1(String locationCategory1) {
		this.locationCategory1 = locationCategory1;
	}
	public String getLocationCategory2() {
		return locationCategory2;
	}
	public void setLocationCategory2(String locationCategory2) {
		this.locationCategory2 = locationCategory2;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public ProgramGroup getGroup() {
		return group;
	}
	public void setGroup(ProgramGroup group) {
		this.group = group;
	}
	public List<Integer> getWdays() {
		return wdays;
	}
	public void setWdays(List<Integer> wdays) {
		this.wdays = wdays;
	}
	public List<ProgramTime> getTimes() {
		return times;
	}
	public void setTimes(List<ProgramTime> times) {
		this.times = times;
	}
	public List<String> getAddInfos() {
		return addInfos;
	}
	public void setAddInfos(List<String> addInfos) {
		this.addInfos = addInfos;
	}
	public List<ProgramAddInfo> getInfos() {
		return infos;
	}
	public void setInfos(List<ProgramAddInfo> infos) {
		this.infos = infos;
	}
	public int getRequisitionDay() {
		return requisitionDay;
	}
	public void setRequisitionDay(int requisitionDay) {
		this.requisitionDay = requisitionDay;
	}
	
}
