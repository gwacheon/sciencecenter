package kr.go.sciencecenter.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.go.sciencecenter.api.AuthenticationApi;
import kr.go.sciencecenter.model.Board;
import kr.go.sciencecenter.model.BoardFile;
import kr.go.sciencecenter.model.BoardType;
import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.model.api.Member;
import kr.go.sciencecenter.persistence.BoardFileMapper;
import kr.go.sciencecenter.persistence.BoardMapper;
import kr.go.sciencecenter.util.FileUploadHelper;

@Service
public class BoardService {
	private static final Logger logger = LoggerFactory.getLogger(BoardService.class);
	
	// User Authentication API for getting Member Info
	private static AuthenticationApi api = new AuthenticationApi();
	
	@Autowired
	private BoardMapper boardMapper;
	@Autowired
	private BoardFileMapper boardFileMapper;
	@Autowired
	private BoardFileService boardFileService;
	@Autowired
	private ServletContext servletContext;
	@Autowired
	private HttpServletRequest httpServletRequest;
	@Autowired
	private FileUploadHelper fileUploadHelper;
	
	
	private static final List<String> faqTypes = new ArrayList<String>();
	static {
		faqTypes.add("faqScience");
		faqTypes.add("faqEducation");
		faqTypes.add("faqRole");
		faqTypes.add("faqFacilities");
		faqTypes.add("faqRestaurant");
		faqTypes.add("faqUser");
		faqTypes.add("faqDisplay");
		faqTypes.add("faqReservation");
		faqTypes.add("faqPark");
		faqTypes.add("faqHomepage");
		faqTypes.add("faqMarketing");
	}

	public int count(String boardType, String searchType, String searchKey) {
		
		
		if( searchKey != null) {
			searchKey = "%" + searchKey +"%";
		}
		
		// faqTotal 타입일 경우에는, 모든 faq type 들을 가져와야 하므로 다른방법으로 counting 한다.
		if( boardType.equals("faqTotal")) {
			return boardMapper.countFaqTotal(faqTypes, searchType, searchKey);
		}
		else {
			return boardMapper.count(boardType, searchType, searchKey);						
		}
	}
	
	public int countEventSeriesBoards(String boardType, Integer eventSeriesId,
			String searchType, String searchKey) {
		if( searchKey != null) {
			searchKey = "%" + searchKey +"%";
		}

		return boardMapper.countEventSeriesBoards(boardType, eventSeriesId, searchType, searchKey);								
	}

	public List<Board> list(String type, Page page, String searchType, String searchKey, String lang) {
		
		List<Board> boards = new ArrayList<Board>();
		
		if( searchKey != null) {
			searchKey = "%" + searchKey +"%";
		}
		// faqTotal 타입일 경우에는, 모든 faq type 들을 가져와야 하므로 다른방법으로 list 를 가져온다.
		if( type.equals("faqTotal")) {
			boards = boardMapper.listFaqTotal(faqTypes, page, searchType, searchKey, lang);
		}
		else {
			boards = boardMapper.list(type, page, searchType, searchKey, lang);			
		}
		
		// mapping member info for each board
		this.setBoardWriters(boards);
		
		return boards;
	}
	
	public List<Board> mainList(String type, Page page, String searchType, String lang) {
		List<Board> boards = new ArrayList<Board>();
		
		// faqTotal 타입일 경우에는, 모든 faq type 들을 가져와야 하므로 다른방법으로 list 를 가져온다.
		boards = boardMapper.mainList(type, page, searchType, lang);
		
		return boards;
	}
	
	public List<Board> listEventSeriesBoards(String type, Integer eventSeriesId,
			Page page, String searchType, String searchKey, String lang) {
		
		if( searchKey != null) {
			searchKey = "%" + searchKey +"%";
		}

		List<Board> boards = boardMapper.listEventSeriesBoards(type, eventSeriesId, page, searchType, searchKey, lang);		

		// mapping member info for each board
		this.setBoardWriters(boards);

		return boards;
		
	}
	/**
	 * 게시판 글 생성
	 * @param board
	 */
	public void create(Board board) {
		boardMapper.create(board);
		String urlPath = Board.BOARD_FILE_PREFIX;
		
		// create Board Attachments
		if ( board.getAttatchments() != null && board.getAttatchments().size() != 0 ) {
			for (BoardFile boardFile : board.getAttatchments()) {
				urlPath += "/" + board.getId();
				boardFileService.create(boardFile, urlPath, board.getId());
			}
		}
		
		// save main image
		if (board.getFile() != null && board.getFile().getSize() > 0) {
			urlPath = Board.BOARD_MAIN_IMAGE_PREFIX + "/" + board.getId();
			if( fileUploadHelper.upload(board.getFile(), urlPath, null) ) {
				urlPath += "/" + board.getFile().getOriginalFilename();
				board.setPicture(urlPath);
				boardMapper.update(board);
			}
		}
		
		// save video
		if(board.getVideo() != null && !board.getVideo().isEmpty()) {
			urlPath = Board.BOARD_MAIN_VIDEO_PREFIX + "/" + board.getId();
			if( fileUploadHelper.upload(board.getVideo(), urlPath, null)){
				urlPath += "/"+ board.getVideo().getOriginalFilename();
				board.setVideoPath(urlPath);
				boardMapper.update(board);
			}
			
			System.out.println("video : "+board.getVideo().getOriginalFilename());
			
		}
	}

	public Board find(int id) {
		Board board = boardMapper.find(id); 
		this.setBoardWriter(board);
		return board;
	}

	public void update(Board board) {
		
		String urlPath = Board.BOARD_FILE_PREFIX;
		if ( board.getAttatchments() != null && board.getAttatchments().size() != 0 ) {
			for (BoardFile boardFile : board.getAttatchments()) {
				urlPath += "/" + board.getId();
				boardFileService.create(boardFile, urlPath, board.getId());
			}
		}

		// update main image
		if (board.getFile() != null && board.getFile().getSize() > 0) {
			urlPath = Board.BOARD_MAIN_IMAGE_PREFIX + "/" + board.getId();
			if ( fileUploadHelper.upload(board.getFile(), urlPath, null) ) {
				urlPath += "/" + board.getFile().getOriginalFilename();
				board.setPicture(urlPath);
			}
		}
		
		// save video
		if(board.getVideo() != null && !board.getVideo().isEmpty()){
			urlPath = Board.BOARD_MAIN_VIDEO_PREFIX + "/" + board.getId();
			if( fileUploadHelper.upload(board.getVideo(), urlPath, null)){
				urlPath += "/"+ board.getVideo().getOriginalFilename();
				board.setVideoPath(urlPath);
				boardMapper.update(board);
			}
			
			System.out.println("video : "+board.getVideo().getOriginalFilename());
			
		}
		
		
		boardMapper.update(board);
	}

	public void delete(int id) {
		boardMapper.deleteReplies(id);
		boardMapper.delete(id);
		String urlPath = Board.BOARD_FILE_PREFIX;
		urlPath += "/" + id;
		boardFileService.deleteFiles(id, urlPath);
	}

	public Board getPrevBoard(int id, String boardType) {
		return boardMapper.getPrevBoard(id, boardType);
	}

	public Board getNextBoard(int id, String boardType) {
		return boardMapper.getNextBoard(id, boardType);
	}

	public void addCount(Board board) {
		boardMapper.addCount(board);
	}

	public ServletContext getServletContext() {
		return servletContext;
	}

	public HttpServletRequest getHttpServletRequest() {
		return httpServletRequest;
	}

	public int countByTotalSearch(String searchKey) {
		if( searchKey != null) {
			searchKey = "%" + searchKey +"%";
		}
		return boardMapper.countByTotalSearch(searchKey);
	}

	public List<Board> listByTotalSearch(Page page, String searchKey) {
		if( searchKey != null) {
			searchKey = "%" + searchKey +"%";
		}
		List<Board> boards =  boardMapper.listByTotalSearch(page, searchKey);
		
		this.setBoardWriters(boards);
		
		return boards;
	}

	
	/**
	 * mapping member info for each board
	 * @param boards
	 * @return
	 */
	private void setBoardWriters(List<Board> boards) {
		for( Board board : boards ) {
			this.setBoardWriter(board);			
		}
	}
	
	private void setBoardWriter(Board board) {
		Member member = new Member();
		String boardMemberNo = board.getMemberNo();
		// 예전 게시판 데이터라면, MemberNo 컬럼에 작성자 이름이 들어있다.
		if( board.getIsOld() == 1 ) {
			member.setMemberName(boardMemberNo);
			board.setMember(member);
			member = new Member();
		}
		// 예전 데이터가 아니고 MemberNo가 있다면, api 를 통해서 사용자 정보를 가져온다.
		else if(boardMemberNo != null) {
			member.setMemberNo(boardMemberNo);
			board.setMember(api.getMemberInfo(member));
			member = new Member();
		}
		// member no 가 없고, 관리자가 작성한 글이라면,
		else if ( board.isWrittenByAdmin() == true ) {
			// 직접 관리자라고 멤버이름을 넣는다.
			member.setMemberName("관리자");
			board.setMember(member);
			member = new Member();
		}
	}

	public int countFixedOnTop(String type) {
		return boardMapper.countFixedOnTop(type);
	}

	public List<Board> listFixedOnTop(String type) {
		List<Board> boards = new ArrayList<Board>();
		boards = boardMapper.listFixedOnTop(type);
		this.setBoardWriters(boards);
		return boards;
	}
	
	
}
