package kr.go.sciencecenter.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import kr.go.sciencecenter.model.Event;
import kr.go.sciencecenter.model.EventFile;
import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.persistence.EventsMapper;
import kr.go.sciencecenter.util.FileUploadHelper;

@Service
public class EventsService {
	@Autowired
	EventsMapper eventsMapper;
	
	@Autowired
	EventFileService eventFileService;

	@Autowired
	private FileUploadHelper fileUploadHelper;
	
	public void create(Event event, MultipartFile file) {
		eventsMapper.create(event);
		this.saveMainImageFile(file, event);
		this.saveAttatchments(event);
		eventsMapper.update(event);
	}

	public Event prevEvent(int id, String type) {
		return eventsMapper.prevEvent(id, type);
	}
	
	public Event prevEventByMultipleTypes(int id, List<String> typeList) {
		return eventsMapper.prevEventByMultipleTypes(id, typeList);
	}

	public Event find(int id) {

		return eventsMapper.find(id);
	}

	public Event nextEvent(int id, String type) {
		return eventsMapper.nextEvent(id, type);
	}
	
	public Event nextEventByMultipleTypes(int id, List<String> typeList) {
		return eventsMapper.nextEventByMultipleTypes(id, typeList);
	}

	public void update(Event event, MultipartFile file) {
		this.saveMainImageFile(file, event);
		this.saveAttatchments(event);
		eventsMapper.update(event);
	}

	
	/**
	 * 		
	 * @param type
	 * @param status Mostly Boolean type. It also can be null, if it's called by listAll(...)
	 * @param page
	 * @return
	 */
	public List<Event> list(String type, Object status, Page page) {

		return eventsMapper.list(type, status, page);
	}
	
	public List<Event> listByMultipleTypes(List<String> typeList, boolean status, Page page) {
		return eventsMapper.listByMultipleTypes(typeList, status, page);
	}
	
	public List<Event> listAll(String type, Page page) {
		return list(type, null, page);
	}

	/**
	 * 
	 * @param type
	 * @param status Mostly Boolean type. It also can be null, if it's called by countAll(String type)
	 * @return
	 */
	public int count(String type, Object status) {
		return eventsMapper.count(type, status);
	}
	
	public int countByMultipleTypes(List<String> typeList, boolean status) {
		return eventsMapper.countByMultipleTypes(typeList, status);
	}
	
	public int countAll(String type) {
		return count(type, null);
	}
	
	/**
	 * 각 이벤트의 대표이미지를 저장한다.
	 * @param file
	 * @param event
	 */
	private void saveMainImageFile(MultipartFile file, Event event) {
		if (file != null && file.getSize() > 0) {
			String urlPath = Event.EVENT_MAIN_IMAGE_PREFIX;
			urlPath += "/" + event.getId();
			if( fileUploadHelper.upload(file, urlPath, null) ) {
				urlPath += "/" + file.getOriginalFilename();
				event.setPictureUrl(urlPath);				
			}
		}
	}
	
	private void saveAttatchments(Event event) {
		if( event.getAttatchments() != null && event.getAttatchments().size() != 0 ) {
			String urlPath = Event.EVENT_FILE_PREFIX;
			urlPath += "/" + event.getId();
			for( EventFile eventFile : event.getAttatchments() ) {
				 eventFileService.create(eventFile, urlPath, event.getId());
			}
		}
	}
	
	public void delete(int id) {
		eventsMapper.delete(id);
		eventFileService.deleteFiles(id);
		
	}

	

	

	
}
