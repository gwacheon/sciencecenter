package kr.go.sciencecenter.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kr.go.sciencecenter.model.api.LectureInfo;
import kr.go.sciencecenter.model.api.ReqLecture;
import kr.go.sciencecenter.persistence.LectureMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LectureService {
	private static final Logger logger = LoggerFactory.getLogger(LectureService.class);
	
	@Autowired
	LectureMapper lectureMapper;
	
	/**
	 * 아카데미 리스트
	 * @return
	 * @throws Exception
	 */
	public List getAcademyList()throws Exception{
		return lectureMapper.getAcademyList();
	}
	
	/**
	 * 아카데미 클래스 리스트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public List getAcademyClassList(Map<String,Object> params)throws Exception{
		return lectureMapper.getAcademyClassList(params);
	}
	
	/**
	 * 클래스 리스트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public List getClassList(Map<String,Object> params)throws Exception{
		return lectureMapper.getClassList(params);
	}
	
	/**
	 * 교육형 리스트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public List getPeriodList(Map<String,Object> params)throws Exception{
		return lectureMapper.getPeriodList(params);
	}
	
	/**
	 * 날짜 / 회차 리스트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public List getPriceList(Map<String,Object> params)throws Exception{
		return lectureMapper.getPriceList(params);
	}
	
	/**
	 * 날짜 / 회차 리스트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public List getPriceDayList(Map<String,Object> params)throws Exception{
		return lectureMapper.getPriceDayList(params);
	}
	
	/**
	 * 가족 리스트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public List getFamilyList(Map<String,Object> params)throws Exception{
		return lectureMapper.getFamilyList(params);
	}
	
	/**
	 * 가족 리스트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public List getFriendList(Map<String,Object> params)throws Exception{
		return lectureMapper.getFriendList(params);
	}
	
	/**
	 * 회원 등급 리스트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public List getMemberGradeList(Map<String,Object> params)throws Exception{
		return lectureMapper.getMemberGradeList(params);
	}
	
	/**
	 * 회원별 강좌 등급 리스트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public List getCourseGradeList(Map<String,Object> params)throws Exception{
		return lectureMapper.getCourseGradeList(params);
	}
	
	/**
	 * 회원 이름
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public List getMemberName(Map<String,Object> params)throws Exception{
		return lectureMapper.getMemberName(params);
	}
	
	/**
	 * 신청 대상 리스트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public List getTargetList(Map<String,Object> params)throws Exception{
		return lectureMapper.getTargetList(params);
	}
	
	/**
	 * 예약증
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public List getCompleteInfo(Map<String,Object> params)throws Exception{
		return lectureMapper.getCompleteInfo(params);
	}
	
	/**
	 * 강좌별 요금 리스트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List getCoursePriceList(Map<String,Object> params)throws Exception{
		logger.info("LectureService.getCoursePriceList -- ! " + params.toString());
		List<Map> list = lectureMapper.getCoursePriceList(params);
		List rList = new ArrayList();
		Map m = null;
		for(Map map : list){
			if(map.get("MEMBERSHIP_TYPE") == null
				|| map.get("MEMBERSHIP_TYPE") == ""
				|| map.get("MEMBERSHIP_TYPE").equals(params.get("STR"))){
				m = new HashMap();
				m.putAll(map);
				rList.add(m);
			}
		}
		return rList;
	}
	
	/**
	 * 강좌별 요금 리스트2
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List getCoursePriceList2(Map<String,Object> params)throws Exception{
		logger.info("LectureService.getCoursePriceList -- ! " + params.toString());
		List<Map> list = lectureMapper.getCoursePriceList2(params);
		List rList = new ArrayList();
		Map m = null;
		for(Map map : list){
			if(map.get("MEMBERSHIP_TYPE") == null
				|| map.get("MEMBERSHIP_TYPE") == ""
				|| map.get("MEMBERSHIP_TYPE").equals(params.get("STR"))){
				m = new HashMap();
				m.putAll(map);
				rList.add(m);
			}
		}
		return rList;
	}
	
	/**
	 * 강좌별 요금 리스트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public Map getMbrInfo(String classCd)throws Exception{
		return lectureMapper.getMbrInfo(classCd);
	}
	
	/**
	 * 강좌 리스트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public List getLectureList(Map<String,Object> params)throws Exception{
		return lectureMapper.getLectureList(params);
	}
	
	/**
	 * 강좌 리스트 카운트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public int getLectureListCount(Map<String,Object> params)throws Exception{
		return lectureMapper.getLectureListCount(params);
	}
}