package kr.go.sciencecenter.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.go.sciencecenter.model.DinnerApply;
import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.persistence.DinnerApplyMapper;

@Service
public class DinnerApplyService {

	@Autowired
	private DinnerApplyMapper dinnerApplyMapper;
	
	public int create(DinnerApply dinnerApply) {
		return dinnerApplyMapper.create(dinnerApply);
	}
	
	public List<DinnerApply> list(Page page) {
		return dinnerApplyMapper.list(page);
	}
	
	public int count(Date todayDate) {
		return dinnerApplyMapper.count(todayDate);
	}
	
	public void delete(int id) {
		dinnerApplyMapper.delete(id);
	}

	public DinnerApply findDuplicates(DinnerApply dinnerApply) {
		return dinnerApplyMapper.findDuplicates(dinnerApply);
	}
	
	public List<DinnerApply> listAllHistories(Page page) {
		return dinnerApplyMapper.listAllHistories(page);
	}
	
}
