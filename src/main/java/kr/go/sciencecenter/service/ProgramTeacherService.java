package kr.go.sciencecenter.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.go.sciencecenter.model.ProgramTeacher;
import kr.go.sciencecenter.persistence.ProgramTeacherMapper;

@Service
public class ProgramTeacherService {
  
  @Autowired
  ProgramTeacherMapper programTeacherMapper;
  
  public List<ProgramTeacher> list() {
    return programTeacherMapper.list();
  }

  public void create(ProgramTeacher programTeacher) {
    programTeacherMapper.create(programTeacher);
    
  }

  public void delete(int i) {
    programTeacherMapper.delete(i);
    }
  
}
