package kr.go.sciencecenter.service;

import java.util.List;

import kr.go.sciencecenter.model.SupportEvaluation;
import kr.go.sciencecenter.persistence.SupportEvaluationMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SupportEvaluationService {
	
	@Autowired
	private SupportEvaluationMapper supportEvaluationMapper;
	
//	public int count() {
//		return supportEvaluationMapper.count();
//	}
	
	public List<SupportEvaluation> list() {
		return supportEvaluationMapper.list();
	}
	
	public int create(SupportEvaluation supportEvaluation) {
		return supportEvaluationMapper.create(supportEvaluation);
	}

	public SupportEvaluation find(int id) {
		return supportEvaluationMapper.find(id);
	}

	public void update(SupportEvaluation supportEvaluation) {
		supportEvaluationMapper.update(supportEvaluation);
	}

	public void delete(int id) {
		supportEvaluationMapper.delete(id);
	}
	
}
