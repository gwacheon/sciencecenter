package kr.go.sciencecenter.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.go.sciencecenter.controller.admin.AdminSurveyController;
import kr.go.sciencecenter.model.Article;
import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.model.Reply;
import kr.go.sciencecenter.model.ReplyDetail;
import kr.go.sciencecenter.model.Selection;
import kr.go.sciencecenter.model.Survey;
import kr.go.sciencecenter.persistence.SurveyMapper;

@Service
public class SurveyService {
	private static final Logger logger = LoggerFactory.getLogger(SurveyService.class);
	
	@Autowired
	private SurveyMapper surveyMapper;

	public List<Survey> list(Page page) {
		return surveyMapper.list(page);
	}

	public void create(Survey survey) {
		surveyMapper.create(survey);
	}

	public Survey find(int id) {
		return surveyMapper.find(id);
	}

	public void createArticle(Article article) {
		surveyMapper.createArticle(article);
		
		logger.error("article id : " + article.getId());
		
		if(article.getSelections() != null && article.getSelections().size() > 0){
			for (Selection selection : article.getSelections()) {
				selection.setArticleId(article.getId());
				surveyMapper.createSelection(selection);
				
				logger.error("SELECTION id : " + selection.getId());
			}
		}
	}

	public Survey findWithArticles(int id) {
		return surveyMapper.findWithArticles(id);
	}

	public void update(Survey survey) {
		surveyMapper.update(survey);
	}

	public void deleteArticle(int articleId) {
		surveyMapper.deleteArticle(articleId);
	}

	public void createReply(Reply reply, List<ReplyDetail> details) {
		surveyMapper.createReply(reply);
		
		for (ReplyDetail replyDetail : details) {
			replyDetail.setReplyId(reply.getId());
			surveyMapper.createReplyDetail(replyDetail);
		}
	}

	public List<Reply> replies(int surveyId, Page page) {
		return surveyMapper.replies(surveyId, page);
	}
	
	public List<Reply> repliesWithDetails(int surveyId, Page page) {
		return surveyMapper.repliesWithDetails(surveyId, page);
	}

	public void updateArticleSort(int id, int[] articleIds) {
		for (int i = 0; i < articleIds.length; i++) {
			surveyMapper.updateArticleSeq(id, articleIds[i], i);
		}
	}

	public Reply findReplyWithIpAddress(int id, String remoteIpAddress) {
		return surveyMapper.findReplyWithIpAddress(id, remoteIpAddress);
	}

	public int count(int surveyId) {
		return surveyMapper.count(surveyId);
	}

	public void delete(int id) {
		surveyMapper.delete(id);
	}

	public int countAll() {
		return surveyMapper.countAll();
	}

}
