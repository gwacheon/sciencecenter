package kr.go.sciencecenter.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import kr.go.sciencecenter.model.UserDetail;
import kr.go.sciencecenter.model.UserRole;
import kr.go.sciencecenter.persistence.UserMapper;


@Service
public class UserService  implements UserDetailsService{
	@Autowired
	UserMapper userMapper;
	
	public void create(kr.go.sciencecenter.model.User user){
		user.setConfirmed(0);
		user.makeAuthenticationToken(kr.go.sciencecenter.model.User.AUTHENTICATION);
		userMapper.create(user);
	}

	public kr.go.sciencecenter.model.User findByEmail(String email) {
		kr.go.sciencecenter.model.User user = userMapper.findByEmail(email);
		if(user != null){
			user.setPassword("");
		}
		return user;
	}

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		kr.go.sciencecenter.model.User user = userMapper.findByEmailWithRoles(email);
		
		System.out.println("1");
		List authorities = buildUserAuthority(user.getUserRoles());
		System.out.println("2");
		
		return buildUserForAuthentication(user, authorities);
	}
	
	// Converts com.mkyong.users.model.User user to
	// org.springframework.security.core.userdetails.User
	private User buildUserForAuthentication(kr.go.sciencecenter.model.User user, List authorities) {
		return new UserDetail(user.getEmail(), 
				user.getPassword()
				, authorities
				, user);
	}
	
	private List buildUserAuthority(List<UserRole> roles) {

		Set setAuths = new HashSet();

		// Build user's authorities
		for (UserRole userRole : roles) {
			setAuths.add(new SimpleGrantedAuthority(userRole.getRole()));
		}

		List results = new ArrayList(setAuths);

		return results;
	}

  public kr.go.sciencecenter.model.User findByIdAndAuthenticationToken(int id, String authenticationToken) {
    return userMapper.findByIdAndAuthenticationToken(id, authenticationToken);
  }

  public void auth(kr.go.sciencecenter.model.User user) {
   userMapper.auth(user); 
  }

  public List<kr.go.sciencecenter.model.User> list() {
    
    return userMapper.list();
  }

  public void delete(int id) {
    userMapper.delete(id);
  }

  public void updateLastLoginDateById(int id) {
    userMapper.updateLastLoginDateById(id);
  }
  
  	public String findDuplicateKey(String id, String email, String name) {
  		return userMapper.findDuplicateKey(id, email, name);
  	}
  
  /**
   * 사용자 정보 MEMBER_NO를 이용해서 회원정보를 가져옴. API에 정의되어 있으나 여기서도 정의.(20160502 일 기준으로 회원정보 API와 같은 값을 가져옴)
   * @param map
   * @return
   */
  @SuppressWarnings("rawtypes")
  public Map getLegacyUser(Map map){
	  return userMapper.getLegacyUser(map);
  }

  @SuppressWarnings("rawtypes")
  public Map getMembershipPrice(Map map) {
	  return userMapper.getMembershipPrice(map);
  }
  
  /**
   * 모든 회원등급을 리스트함(후원회원 포함)
   * @return
   */
  @SuppressWarnings("rawtypes")
  public List getMembershipPriceList() {
	  return userMapper.getMembershipPriceList();
  }
  
  /**
   * MEMBER_NO 를 통해 사용자가 현재 등록되어 있는 사용자등급을 리스트함.
   * @param map map.put("MEMBER_NO") 으로 등록
   * @return
   */
  @SuppressWarnings("rawtypes")
  public List getMembershipList(Map map) {
	  return userMapper.getMembershipList(map);
  }

public int findDuplicateCode(String companyCd, String duplicateKey) {
	
	return userMapper.findDuplicateCode(companyCd, duplicateKey);
}
}
