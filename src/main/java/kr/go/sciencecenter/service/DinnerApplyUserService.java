package kr.go.sciencecenter.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.go.sciencecenter.model.DinnerApplyTime;
import kr.go.sciencecenter.model.DinnerApplyUser;
import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.persistence.DinnerApplyUserMapper;

@Service
public class DinnerApplyUserService {

	@Autowired
	private DinnerApplyUserMapper dinnerApplyUserMapper;

	public List<DinnerApplyUser> list(Page page) {
		return dinnerApplyUserMapper.list(page);
	}
	
	public int count() {
		return dinnerApplyUserMapper.count();
	}

	public DinnerApplyUser findByUserId(String userId) {
		return dinnerApplyUserMapper.findByUserId(userId);
	}

	public void create(DinnerApplyUser dinnerApplyUser) {
		dinnerApplyUserMapper.create(dinnerApplyUser);
	}

	public DinnerApplyUser findById(int id) {
		return dinnerApplyUserMapper.findById(id);
	}

	public void update(DinnerApplyUser dinnerApplyUser) {
		dinnerApplyUserMapper.update(dinnerApplyUser);
		
	}

	public void delete(int id) {
		dinnerApplyUserMapper.delete(id);
	}
	
}
