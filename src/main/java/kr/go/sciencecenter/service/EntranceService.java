package kr.go.sciencecenter.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.go.sciencecenter.controller.EntranceController;
import kr.go.sciencecenter.controller.EntranceController.Code;
import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.model.Program;
import kr.go.sciencecenter.model.ProgramAddInfo;
import kr.go.sciencecenter.model.ProgramGroup;
import kr.go.sciencecenter.model.ProgramTime;
import kr.go.sciencecenter.persistenceEnt.EntranceMapper;
import kr.go.sciencecenter.persistence.ProgramMapper;
import kr.go.sciencecenter.persistence.ProgramTimeMapper;
import kr.go.sciencecenter.util.PropertyHelper;

@Service
public class EntranceService {
	private static final Logger logger = LoggerFactory.getLogger(EntranceService.class);
	
	@Autowired
	EntranceMapper entranceMapper;
	
	@Autowired PropertyHelper 		propertyHelper;
	

	public List<HashMap<String,String>> getProgramList(HashMap<String, Object> param) {
		return entranceMapper.getProgramList(param);
	}

	public List<HashMap<String,String>> getScheduleList(HashMap<String, Object> param) {
		return entranceMapper.getScheduleList(param);
	}
	
	
	public List<HashMap<String,String>> getSeqList(HashMap<String, Object> param) {
		return entranceMapper.getSeqList(param);
	}

	// 회차별 권종 리스트
	public List<HashMap<String,String>> getPriceTypeList(HashMap<String, Object> param) {
		return entranceMapper.getPriceTypeList(param);
	}
	
	
	/**
	 * 예약정보 카운트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public int getPaymentListCount(Map<String,Object> params)throws Exception{
		return entranceMapper.getPaymentListCount(params);
	}
	
	
	/**
	 * 예약정보 리스트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public List<HashMap<String, String>> getPaymentList(Map<String,Object> params){
		return entranceMapper.getPaymentList(params);
	}
	
	
	/**
	 * 예약증 출력 API
	 * @param params
	 * @return
	 */
	public HashMap<String, Object> getReserveCard(Map<String,Object> params){
		return entranceMapper.getReserveCard(params);
	}
	
	
	/**
	 * 예약취소 API
	 * @param params
	 */
	@Transactional
	public void delReserve(Map<String,Object> params){
		entranceMapper.minusSeatsum(params);
		entranceMapper.delReserve(params);
	}
	
	/**
	 * 거래 일련 번호 채번
	 * @param param
	 * @return
	 */
	public HashMap<String,String> getMaxTranNbr(HashMap<String, Object> param) {
		return entranceMapper.getMaxTranNbr(param);
	}
	
	@Transactional
	public int regTran(HashMap<String, Object> param) {
		int tranCnt = 0;
		tranCnt = entranceMapper.regTran(param);
		return tranCnt;
	}
	
	@Transactional
	public int regReserve(HashMap<String, Object> param) {
		int tranCnt = 0;
		tranCnt = entranceMapper.regReserve(param);
		return tranCnt;
	}
	
	
	@Transactional
	public boolean postCardCancel(String sOrgInfUniqueNbr, String sUserID) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = new HashMap<String, Object>();
		
		// 거래 일련 번호 채번
		String strTranDate = EntranceController.getCurrentDate("yyyyMMdd");
		String strTranTime = EntranceController.getCurrentDate("HHmmss");

		param.put("TRAN_DATE",			strTranDate);
		param.put("TRAN_COMPANY_CD",	propertyHelper.getCompanycd());
		param.put("TRAN_WIN_CD",		Code.TRAN_WIN_CD);

		HashMap<String, String> mapTranSeq = getMaxTranNbr(param);
		String strTranSeq = (String) mapTranSeq.get("TRAN_NBR");
		if (strTranSeq == null) {
			return false;
		}
		
		// InfUniqueNbr 채번 
		HashMap<String, Object> mbrParam = new HashMap<String, Object>();
		mbrParam.put("COMPANY_CD", propertyHelper.getCompanycd());
		HashMap<String,String> mapInf = entranceMapper.getInfUniqueNbr(mbrParam);
		String sInfUniqueNbr = mapInf.get("INFUNIQUENBR");
		

		// T_TRAN			: Insert
		param = new HashMap<String, Object>();
		param.put("TRAN_DATE",		strTranDate);
		param.put("TRAN_NBR",		strTranSeq);
		param.put("TRAN_TIME",		strTranTime);
		param.put("INF_UNIQUE_NBR",	sOrgInfUniqueNbr);    

		System.out.println("regTran_cancel call : " + param.toString());
		entranceMapper.regTran_cancel(param);
		
		// T_RESERVE			: UPDATE
		param = new HashMap<String, Object>();
		param.put("TRAN_DATE",		strTranDate);
		param.put("TRAN_TIME",		strTranTime);
		param.put("USER_ID",		sUserID);
		param.put("TRAN_NBR",		strTranSeq);
		param.put("INF_UNIQUE_NBR",	sOrgInfUniqueNbr);    
		
		System.out.println("regReserve_cancel call : " + param.toString());
		entranceMapper.regReserve_cancel(param);
		
		
		// T_PAYMENTTICKET	: Insert
		param = new HashMap<String, Object>();
		param.put("TRAN_DATE",		strTranDate);
		param.put("TRAN_TIME",		strTranTime);
		param.put("TRAN_NBR",		strTranSeq);
		param.put("ORG_INF_UNIQUE_NBR",	sOrgInfUniqueNbr);
		param.put("INF_UNIQUE_NBR",	sInfUniqueNbr);
		
		System.out.println("regPaymentTicket_cancel call : " + param.toString());
		entranceMapper.regPaymentTicket_cancel(param);
		
		// T_PAYMENTTICKET	: UPDATE
		entranceMapper.regPaymentTicket_cancel_update(param);
		
		// T_CARDAPPROVE		: Insert
		param = new HashMap<String, Object>();
		param.put("TRAN_DATE",		strTranDate);
		param.put("TRAN_NBR",		strTranSeq);
		param.put("TRAN_TIME",		strTranTime);
		param.put("ORG_INF_UNIQUE_NBR",	sOrgInfUniqueNbr);
		param.put("INF_UNIQUE_NBR",	sInfUniqueNbr);

		System.out.println("regCardApprove_cancel call : " + param.toString());
		entranceMapper.regCardApprove_cancel(param);

		// T_PAYMENT_RESULT	: Update
		param = new HashMap<String, Object>();
		param.put("ORG_INF_UNIQUE_NBR",	sOrgInfUniqueNbr);    
		param.put("CANCEL_YN",		"Y");    
		
		System.out.println("updatePaymentResult_cancel call : " + param.toString());
		entranceMapper.updatePaymentResult_cancel(param);
		
		
		// T_SCHEDULE_SEAT_SUM	: Update
		param = new HashMap<String, Object>();
		param.put("INF_UNIQUE_NBR",		sOrgInfUniqueNbr);    
		System.out.println("regTran_cancel call : " + param.toString());
		HashMap<String, String> ptResult = entranceMapper.getPaymentTicketCount(param);
		
		param = new HashMap<String, Object>();
		param.put("CNT",		ptResult.get("CNT"));
		param.put("UNIQUE_NBR",	ptResult.get("UNIQUE_NBR"));
		System.out.println(">>>>> minusSeatsum call: " + param.toString());
		entranceMapper.minusSeatsum(param);

		return true;
	}
	
	
	@Transactional
	public boolean postVACancel(String sOrgInfUniqueNbr, String sUserID) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param = new HashMap<String, Object>();
		
		// 거래 일련 번호 채번
		String strTranDate = EntranceController.getCurrentDate("yyyyMMdd");
		String strTranTime = EntranceController.getCurrentDate("HHmmss");

		param.put("TRAN_DATE",			strTranDate);
		param.put("TRAN_COMPANY_CD",	propertyHelper.getCompanycd());
		param.put("TRAN_WIN_CD",		 Code.TRAN_WIN_CD);

		HashMap<String, String> mapTranSeq = getMaxTranNbr(param);
		String strTranSeq = (String) mapTranSeq.get("TRAN_NBR");
		if (strTranSeq == null) {
			return false;
		}
		
		// InfUniqueNbr 채번 
		HashMap<String, Object> mbrParam = new HashMap<String, Object>();
		mbrParam.put("COMPANY_CD", propertyHelper.getCompanycd());
		HashMap<String,String> mapInf = entranceMapper.getInfUniqueNbr(mbrParam);
		String sInfUniqueNbr = mapInf.get("INFUNIQUENBR");
		

		// T_TRAN			: Insert
		param = new HashMap<String, Object>();
		param.put("TRAN_DATE",		strTranDate);
		param.put("TRAN_NBR",		strTranSeq);
		param.put("TRAN_TIME",		strTranTime);
		param.put("INF_UNIQUE_NBR",	sOrgInfUniqueNbr);    

		entranceMapper.regTran_cancel(param);
		
		// T_RESERVE			: Update
		param = new HashMap<String, Object>();
		param.put("TRAN_DATE",		strTranDate);
		param.put("TRAN_TIME",		strTranTime);
		param.put("TRAN_NBR",		strTranSeq);
		param.put("USER_ID",		sUserID);
		param.put("INF_UNIQUE_NBR",	sOrgInfUniqueNbr);    

		entranceMapper.regReserve_cancel(param);
		
		
		// T_PAYMENTTICKET	: Insert
		param = new HashMap<String, Object>();
		param.put("TRAN_DATE",		strTranDate);
		param.put("TRAN_NBR",		strTranSeq);
		param.put("TRAN_TIME",		strTranTime);
		param.put("ORG_INF_UNIQUE_NBR",	sOrgInfUniqueNbr);
		param.put("INF_UNIQUE_NBR",	sInfUniqueNbr);
		
		entranceMapper.regPaymentTicket_cancel(param);
		
		// T_CARDAPPROVE		: Insert
//		param = new HashMap<String, Object>();
//		param.put("TRAN_DATE",		strTranDate);
//		param.put("TRAN_NBR",		strTranSeq);
//		param.put("TRAN_TIME",		strTranTime);
//		param.put("ORG_INF_UNIQUE_NBR",	sOrgInfUniqueNbr);
//		param.put("INF_UNIQUE_NBR",	sInfUniqueNbr);
//
//		entranceMapper.regCardApprove_cancel(param);

		// T_PAYMENT_RESULT	: Update
		param = new HashMap<String, Object>();
		param.put("INF_UNIQUE_NBR",	sInfUniqueNbr);    
		param.put("CANCEL_YN",		"Y");    
		 
		entranceMapper.updatePaymentResult_cancel(param);
		
		
		// T_SCHEDULE_SEAT_SUM	: Update
		param = new HashMap<String, Object>();
		param.put("INF_UNIQUE_NBR",		sOrgInfUniqueNbr);    
		HashMap<String, String> ptResult = entranceMapper.getPaymentTicketCount(param);
		
		param = new HashMap<String, Object>();
		param.put("CNT",		ptResult.get("CNT"));
		param.put("UNIQUE_NBR",	ptResult.get("UNIQUE_NBR"));
		System.out.println(">>>>> minusSeatsum");
		entranceMapper.minusSeatsum(param);

		return true;
	}
	
	
	
	/**
	 * 가상 계좌 입금 완료시 표준 결제로 부터 호출되는 콜백
	 * 
	 * @param map
	 * @return
	 */
	public boolean payCallback(Map map) {
		/*
		 	호출시 전달되는 파라미터
		 	---------------------------------------------
			TRGUBUN		거래구분	2	‘V2’
			STOREID		상점아이디	6	 씨스퀘어소프트에서 부여해줌
			ORDNO		주문번호	20	
			TRDATE		거래일자	8	‘YYYYMMDD’
			TRTIME		거래시간	6	‘HHMISS’
			ACCOUNTNO	가상계좌번호	14	
			RCPTNAME	입금자명	30	
			AMT			금액		8	‘0005000’
			BANK_CODE	은행코드	2(3)	
			REPLYCODE	응답코드	4	‘0000’
			

		 	파라미터 샘플 
		 	---------------------------------------------
			TRGUBUN=V2&
			STOREID=100011&
			ORDNO=C15090110000134&
			TRDATE=20150901&
			TRTIME=112714&
			ACCOUNTNO=08200987697412&
			RCPTNAME현정&
			RCPTSOCIALNO=0000000000000&
			REPLYCODE=0000&
			AMT=00001000&
			BANK_CODE=03
		 */
		
		logger.info("EntranceService.payCallback -- ! " + map.toString());
		
		HashMap<String, Object> resParam = new HashMap<String, Object>();
		resParam.put("UNIQUE_NBR", map.get("ORDNO"));
		resParam.put("TRAN_DATE", map.get("TRDATE"));
		resParam.put("TRAN_TIME", map.get("TRTIME"));
		resParam.put("AMT", map.get("AMT"));
		resParam.put("RECEIPT_NAME", map.get("RCPTNAME"));
		
		boolean result = false;
		
		//try {
			System.out.println("#### coun :" + entranceMapper.postPayProc(resParam));
			
			result = true;
		//} catch (Exception e) {
		//}

		return result;
	
	}
	
	
	
	public void print(HashMap<String, Object> map) {
		entranceMapper.print(map);
	}
	
	public Map<String, Object> getSmsData(String oid) {
		return entranceMapper.getSmsData(oid);
	}

	
	/**
	 * 예약취소2 API (HOLD처리된 좌석 수 계산하여 취소 처리)
	 * @param params
	 * 2016.07.06
	 */
	@Transactional
	public void ReleaseHoldSeat(Map<String,Object> params){
		entranceMapper.minusSeatsum2(params);
		entranceMapper.delReserve(params);
	}
	
	/**
	 * 좌석 HOLD 시 DB상의 현재 좌석 후 조회 후 체크
	 * @param param
	 * @return
	 * 2016.07.06
	 */
	public HashMap<String,String> getScheduleSeatCnt(HashMap<String, Object> param) {
		return entranceMapper.getScheduleSeatCnt(param);
	}
	

	/**
	 * 좌석 HOLD 이후 결제 진행이 없는 예매번호 리스트
	 * @return
	 * 2016.07.06
	 */
	public List<HashMap<String,String>> getHoldSeatList() {
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		param.put("TIMER", propertyHelper.getReleaseHoldSeatTime());
		
		return entranceMapper.getHoldSeatList(param);
	}

	
	/**
	 * 결제 진행 전 진행하는 예매번호가 존재하는지 체크 
	 * @param param
	 * @return
	 * 2016.07.06
	 */
	public HashMap<String,String> getUniqueNbrExists(HashMap<String, Object> param) {
		return entranceMapper.getUniqueNbrExists(param);
	}
	
	
	/**
	 * 결제 완료 후 거래정보 및 예매정보에 입금 구분 변경( 0 -> 1)
	 * @param params
	 * 2016.07.06
	 */
	@Transactional
	public void updateReserveTran(Map<String,Object> params){
		entranceMapper.updateReserveTran(params);
	}
	
}
