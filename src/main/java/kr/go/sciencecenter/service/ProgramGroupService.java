package kr.go.sciencecenter.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.model.ProgramGroup;
import kr.go.sciencecenter.persistence.ProgramGroupMapper;

@Service
public class ProgramGroupService {
	@Autowired
	ProgramGroupMapper groupMapper;
	
	public int count() {
		return groupMapper.count();
	}

	public List<ProgramGroup> list(Page page) {
		return groupMapper.list(page);
	}

	public int create(ProgramGroup group) {
		return groupMapper.create(group);
	}

	public void delete(int id) {
		groupMapper.delete(id);
	}

	public ProgramGroup find(int id) {
		return groupMapper.find(id);
	}

	public void update(ProgramGroup group) {
		groupMapper.update(group);
	}
	
}
