package kr.go.sciencecenter.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.go.sciencecenter.model.Event;
import kr.go.sciencecenter.model.EventFile;
import kr.go.sciencecenter.persistence.EventFileMapper;
import kr.go.sciencecenter.util.FileUploadHelper;

@Service
public class EventFileService {

	@Autowired
	EventFileMapper eventFileMapper;

	@Autowired
	private FileUploadHelper fileUploadHelper;
	
	public EventFile find(int id) {
		return eventFileMapper.find(id);
	}
	
	public void createAll(List<EventFile> files) {
		eventFileMapper.createAll(files);
	}

	public void create(EventFile eventFile, String urlPath, int eventId) {
		
		eventFile.setEventId(eventId);
		eventFile.setName(eventFile.getFile().getOriginalFilename());
		eventFile.setUrl(urlPath + "/" + eventFile.getFile().getOriginalFilename());
		
		eventFileMapper.create(eventFile);
		fileUploadHelper.upload(eventFile.getFile(), urlPath, eventFile.getFile().getOriginalFilename());
	}

	public void update(EventFile f) {
		eventFileMapper.update(f);
	}
	
	public boolean deleteFile(EventFile eventFile, String urlPath) {
		eventFileMapper.deleteFile(eventFile.getId());
		urlPath += "/" + eventFile.getEventId();
		return fileUploadHelper.delete(urlPath, eventFile.getName());
		
	}
	
	public void deleteFiles(int eventId) {
		eventFileMapper.deleteFiles(eventId);
		String urlPath = Event.EVENT_FILE_PREFIX + "/" + eventId;
		fileUploadHelper.multiDelete(urlPath);
		urlPath = Event.EVENT_MAIN_IMAGE_PREFIX + "/" + eventId;
		fileUploadHelper.multiDelete(urlPath);
	}
}
