package kr.go.sciencecenter.service;

import java.util.List;

import kr.go.sciencecenter.model.Policy;
import kr.go.sciencecenter.persistence.PolicyMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PolicyService {
	
	@Autowired
	private PolicyMapper policyMapper;
	
	public List<Policy> list() {
		return policyMapper.list();
	}
	
	public List<Policy> listAll() {
		return policyMapper.listAll();
	}

	public void create(Policy policy) {
		this.setPrevContents(policy.getType());
		policy.setMostRecent(true);
		policyMapper.create(policy);
	}
	
	public void setPrevContents(String type) {
		policyMapper.setPrevContents(type);
	}

	public Policy find(int id) {
		return policyMapper.find(id);
	}
	
	public Policy findMostRecentByType(String type) {
		return policyMapper.findMostRecentByType(type);
	}

	public void update(Policy policy) {
		this.setPrevContents(policy.getType());
		policy.setMostRecent(true);
		policyMapper.update(policy);
	}

	public List<Policy> listPrevious(String type) {
		return policyMapper.listPrevious(type);
	}

	public void delete(int id) {
		String deletedType= this.find(id).getType();
		policyMapper.delete(id);
		policyMapper.setNextRecent(deletedType);
	}
	
}
