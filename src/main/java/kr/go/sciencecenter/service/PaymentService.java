package kr.go.sciencecenter.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kr.co.mainticket.Utility.DateTime;
import kr.co.mainticket.Utility.Utility;
import kr.go.sciencecenter.persistence.PaymentCheckMapper;
import kr.go.sciencecenter.persistence.PaymentMapper;
import kr.go.sciencecenter.util.PropertyHelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentService {
	private static final Logger logger = LoggerFactory.getLogger(PaymentService.class);
	
	@Autowired	PaymentMapper 		paymentMapper;
	@Autowired	PaymentCheckMapper 	paymentCheckMapper;
	@Autowired	UserService 		userService;
	@Autowired 	PropertyHelper 		propertyHelper;
	
	/**
	 * 프로그램 리스트
	 * @param params
	 * @throws Exception
	 */
	public List getPaymentList(Map<String,Object> params)throws Exception{
		return paymentMapper.getPaymentList(params);
	}
	
	/**
	 * 프로그램 리스트 카운트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public int getPaymentListCount(Map<String,Object> params)throws Exception{
		return paymentMapper.getPaymentListCount(params);
	}
	
	/**
	 * 예약증 업데이트
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public int getPrintFlagUpdate(Map<String,Object> params)throws Exception{
		return paymentMapper.getPrintFlagUpdate(params);
	}
	
	/**
	 * 예약증 프린트 확인 유무
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public List getPrintFlag(Map<String,Object> params)throws Exception{
		return paymentMapper.getPrintFlag(params);
	}
	
	/**
	 * 0원 환불
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public int getUpdateCourseZero(Map<String,Object> params)throws Exception{
		return paymentMapper.getUpdateCourseZero(params);
	}
	
	/**
	 * 가상계좌은행 리스트
	 * @return
	 * @throws Exception
	 */
	public List getVatList()throws Exception{
		return paymentMapper.getVatList();
	}
	
	/**
	 * 거래정보
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List getTranPaymentList(Map<String,Object> param)throws Exception{
		return paymentMapper.getTranPaymentList(param);
	}
	
	/**
	 * 영수증
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List getReceiptInfo(Map<String,Object> param)throws Exception{
		return paymentMapper.getReceiptInfo(param);
	}
	
	
	/**
	 * 은행 리스트
	 * @return
	 * @throws Exception
	 */
	public List getBankList()throws Exception{
		return paymentMapper.getBankList();
	}
	
	/**
	 * 결제 데이터
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public List PAYMENT_DATA(Map<String,Object> params)throws Exception{
		return paymentMapper.PAYMENT_DATA(params);
	}
	
	/**
	 * 수강생 인원 정보
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public List MemberInfoList(Map<String,Object> params)throws Exception{
		return paymentMapper.MemberInfoList(params);
	}
	
	/**
	 * 파라미터 내용 테이블 저장 로그용 (VACALLBACK)
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public int insertVaCallBack(Map<String,Object> params)throws Exception{
		return paymentMapper.insertVaCallBack(params);
	}
	
	/**
	 * STOREID, ORDNO, ACCOUNTNO, RCPTNAME, AMT 을 비교하여 강좌내용에 있는지를 확인해서 UPDATE 처리
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public int updateLectureVaCallBack(Map<String,Object> params)throws Exception{
		return paymentMapper.updateLectureVaCallBack(params);
	}

	/**
	 * 가상계좌 입금
	 * @param map
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void updateVAPament(Map map) {
		paymentMapper.updateVAPament(map);
		paymentMapper.updateVAPament2(map);
		
	}

	public int isLecture(Map map) {
		return paymentMapper.countTA_TRAN_PAYMENT(map);
	}

	
	/**
	 * TM_MEMBERSHIP 입력
	 * @param map
	 * @return
	 */
	@SuppressWarnings({ "rawtypes" })
	public int insertTmMembership(Map map) {
		return paymentMapper.insertTM_MEMBERSHIP(map);
	}

	/**
	 * TA_APPROVE 입력
	 * @param map
	 * @return
	 * @throws Exception 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public int insertTaAppove(Map map) throws Exception {
		return paymentCheckMapper.INSERT_APPROVE(map);
	}
	/**
	 * TA_TRAN_PAYMENT 입력
	 * @param map
	 * @return
	 * @throws Exception 
	 */
	@SuppressWarnings("rawtypes")
	public int insertTaTranPayment(Map map) throws Exception {
		return paymentCheckMapper.INSERT_TRAN_PAYMENT_APPROVE(map);
	}


	/**
	 * TA_VIRTUAL_ACCOUNT 입력
	 * @param map
	 * @return
	 * @throws Exception 
	 */
	@SuppressWarnings("rawtypes")
	public int insertTaVirtualAccount(Map map) throws Exception {
		return paymentCheckMapper.INSERT_VIRTUAL_ACCOUNT(map);
	}
	

	/**
	 *  TA_APPROVE 수정
	 * @param map
	 * @return 
	 * @throws Exception 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public int updateTaAppove(Map map) throws Exception {
		
		map.put("CARD_MBR_NO","");
		map.put("APPROVE_NO", map.get("cardApprovNo"));	//? 이게 있음?
		map.put("APPROVE_DATE",map.get("cardApprovDate"));
		map.put("PAY_RSP_CD",map.get("rstCode"));
		map.put("PAY_RSP_MSG",map.get("rstMsg"));
		map.put("C2S_UNIQUE_NBR2",map.get("cardTradeNo"));
		map.put("PAY_ADD_MSG",map.get("vAccountBankCode"));
		map.put("PAY_ACCOUNT_NO",map.get("vAccount"));
		map.put("ISU_CD","");
		map.put("ISU_NAME","");
		map.put("ISU_CARD_NAME",map.get("cardName"));
		map.put("ACQ_CD","");
		map.put("ACQ_NAME","");
		map.put("EXTEND_APPROVE_NO","");
		map.put("CANCEL_TYPE","");
		map.put("STATUS_TYPE","C");
		map.put("INF_UNIQUE_NBR",map.get("oid"));
		return paymentCheckMapper.UPDATE_APPROVE(map);
	} 

	/**
	 * 연간회원 정보를 입력하기 위한 Map 정보를 만든다.
	 * @param map
	 * @return
	 * @throws Exception 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Map makeMap(Map map) throws Exception {
		
		//사용자 정보 map 에 병합
		map.putAll(userService.getLegacyUser(map));
		
		//1. TM_MEMBERSHIP 파라미터
		map = setParamTM_MEMBERSHIP(map);
		//2. TA_APPROVE 파라미터
		map = setParamTA_APPROVE(map);
		//3. TA_TRAN_PAYMENT 파라미터
		map = setParamTA_TRAN_PAYMENT(map);
		
		return map;
	}


	

	/**
	 * 1. TM_MEMBERSHIP 파라미터
	 * @param map
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Map setParamTM_MEMBERSHIP(Map map) {
		
		String CurrDate = DateTime.getFormatDateString("yyyyMMddHHmmss", 0);
		
		map.put("MEMBERSHIP_UNIQUE_NBR", map.get("oid"));
		map.put("MEMBERSHIP_NBR", "1");
		map.put("MEMBERSHIP_TRAN_TYPE", "R");
		if(map.get("payKind").equals("1")) map.put("SALE_TYPE", "000002");	//신용카드
		if(map.get("payKind").equals("2")) map.put("SALE_TYPE", "000003");	//가상계좌
		map.put("MEMBER_NO", map.get("member_no"));
		map.put("FAMILY_NO", map.get("family_no"));
		map.put("MEMBERSHIP_TYPE", "SV");
		map.put("DEFAULT_FLAG", "N");
		map.put("AGE_FLAG", "N");
		map.put("PRICE_NBR", 1);
		map.put("LIFELONG_FLAG", "N");
		map.put("VALID_MONTH", 12);
		map.put("FREE_FLAG", "N");
		
		Map m = new HashMap();
		m.put("MEMBERSHIP_TYPE", "SV");
		m.put("PRICE_NBR", "1");
		map.put("MEMBERSHIP_PRICE", userService.getMembershipPrice(m).get("MEMBERSHIP_PRICE"));
		
		map.put("DEDUCT_FLAG", "N");
		map.put("TOTAL_PRICE", userService.getMembershipPrice(m).get("MEMBERSHIP_PRICE"));
		map.put("PAYMENT_AMT", Integer.parseInt(map.get("salesPrice").toString()));
		map.put("VAT_AMT", Integer.parseInt(map.get("salesPrice").toString()) * 0.1);
		map.put("SVC_AMT", 0);
		map.put("RECEIPT_FLAG", "Y");
		map.put("RECEIPT_DATE", CurrDate.substring(0, 2) + map.get("cardApprovDate"));
		map.put("RECEIPT_TIME", map.get("cardApprovTime"));
		map.put("POINT_SAVE_FLAG", "N");
		map.put("POINT_SAVE_MONTH", 0);
		map.put("REFUND_FLAG", "N");
		map.put("REFUND_FLAG", "N");
		map.put("USE_FLAG", "Y");
		map.put("CONFIRM_FLAG", "N");
		map.put("INSERT_USER", map.get("member_no"));
		return map;
	}
	
	/**
	 * 2. TA_APPROVE 파라미터
	 * @param map
	 * @return
	 * @throws Exception 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Map setParamTA_APPROVE(Map map) throws Exception {
		
		String CurrDate = DateTime.getFormatDateString("yyyyMMddHHmmss", 0);
//		map.put("INF_UNIQUE_NBR", CurrDate.substring(2, 8) + map.get("COMPANY_CD") + "MEM" + Utility.LPAD(getIndex(map.get("COMPANY_CD").toString(), "INF_UNIQUE_NBR", CurrDate.substring(0, 8), 1), 5, '0'));
		map.put("INF_UNIQUE_NBR", map.get("oid"));
		map.put("TRAN_DATE", CurrDate.substring(0, 2) + map.get("cardApprovDate"));
		map.put("TRAN_TIME", map.get("cardApprovTime"));
		map.put("TRAN_COMPANY_CD", map.get("COMPANY_CD"));
		map.put("SYSTEM_TYPE", "MEM");
		if(map.get("payKind").equals("1")) map.put("SALE_TYPE", "000002");	//신용카드
		if(map.get("payKind").equals("2")) map.put("SALE_TYPE", "000003");	//가상계좌
		map.put("WIN_TYPE", "000000");
		map.put("PAY_TYPE", "A");
		map.put("PAY_SVC_TYPE", map.get("payType"));
		map.put("ORG_INF_UNIQUE_NBR", "");
		map.put("MBR_NO", map.get("mbrId"));
		map.put("PAY_MSG_TYPE", "0200");
		map.put("PAY_WIN_CD","");
		map.put("PAY_SHOP_CD","");
		map.put("PAY_HALL_CD","");
		if(map.get("payKind").equals("1")) map.put("PAY_TRAN_TYPE", "010");	//신용카드
		if(map.get("payKind").equals("2")) map.put("PAY_TRAN_TYPE", "810");	//가상계좌
		map.put("PAY_SALE_CNT", 1);
		map.put("PAY_APPLY_DATE", CurrDate.substring(0, 8)); 
		map.put("PAY_SALE_DATE", CurrDate.substring(0, 8)); 
		map.put("PAY_UNIT_AMT", Integer.parseInt(map.get("salesPrice").toString()));
		map.put("CANCEL_KEY_TYPE", "");
		map.put("KEYIN_TYPE", "");
		map.put("TRACK_DATA", ""); 
		map.put("INSTALL_NO", ""); 
		map.put("PAY_CURRENCY", "410");
		map.put("PAY_SALE_AMT", Integer.parseInt(map.get("salesPrice").toString())); 
		map.put("PAY_SALE_SVC_AMT", 0); 
		map.put("PAY_SALE_VAT_AMT", 0);
		map.put("PAY_CERTIFY_TYPE", ""); 
		map.put("PAY_REG_NO", "");
		map.put("CARD_PASSWORD", ""); 
		map.put("PAY_BANK_CD", map.get("vAccountBankCode"));
		map.put("PAY_WITHDRAW_NAME", ""); 
		map.put("FIRM_APPROVE_NO", "");
		map.put("ORG_APPROVE_DATE", "");
		map.put("SVC_CD1", "");
		map.put("SVC_CD2", "");
		map.put("SVC_CD3", "");
		map.put("CUSTOMER_NAME", map.get("MEMBER_NAME"));
		map.put("CUSTOMER_TEL", map.get("MEMBER_CEL"));
		map.put("CUSTOMER_EMAIL", map.get("MEMBER_EMAIL"));
		map.put("PRODUCT_NAME", propertyHelper.getPay_mbrName());
		map.put("STATUS_TYPE","E");
		return map;
	}
	
	
	/**
	 * 3. TA_TRAN_PAYMENT 파라미터
	 * @param map
	 * @return
	 * @throws Exception 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Map setParamTA_TRAN_PAYMENT(Map map) throws Exception {
		String CurrDate = DateTime.getFormatDateString("yyyyMMddHHmmss", 0);
		
		map.put("TRAN_DATE", CurrDate.substring(0, 2) + map.get("cardApprovDate"));
		map.put("TRAN_COMPANY_CD", map.get("COMPANY_CD"));
		map.put("TRAN_NBR", Utility.LPAD(getIndex(map.get("COMPANY_CD").toString(), "TRAN_NBR", CurrDate.substring(0, 8), 1), 6, '0'));
		map.put("SYSTEM_TRAN_TYPE", "M"); 
		map.put("TRAN_UNIQUE_NBR", map.get("oid")); 
		map.put("TRAN_DETAIL_NBR", 1); 
		map.put("TRAN_TIME", map.get("cardApprovTime")); 
		map.put("TRAN_TYPE", "11");
		if(map.get("payKind").equals("1")) map.put("PAYMENT_TYPE", "000002");	//신용카드
		if(map.get("payKind").equals("2")) map.put("PAYMENT_TYPE", "000003");	//가상계좌
		map.put("INF_UNIQUE_NBR", map.get("oid"));	//이건지는 확실하지 않음.
		map.put("INF_UNIQUE_NBR2", map.get("oid"));	//이건지는 확실하지 않음.
		map.put("REF_FLAG", "N"); 
		map.put("RECEIPT_FLAG", "Y"); 
		map.put("RECEIPT_DATE", CurrDate.substring(0, 2) + map.get("cardApprovDate")); 
		map.put("RECEIPT_TIME", map.get("cardApprovTime"));
		map.put("PAYMENT_AMT", Integer.parseInt(map.get("salesPrice").toString())); 
		map.put("VAT_AMT", Integer.parseInt(map.get("salesPrice").toString()) * 0.1); 
		map.put("SVC_AMT", 0);
		map.put("TAXSAVE_FLAG", "N"); 
		map.put("PROCESS_FLAG", "N"); 
		map.put("KEYIN_TYPE", ""); 
		map.put("PAY_CERTIFY_TYPE", ""); 
		map.put("TRACK_DATA", "");
		map.put("REF_STATUS", "N"); 
		map.put("USE_FLAG", "Y"); 
		map.put("CONFIRM_FLAG", "N");
		map.put("INSERT_USER", map.get("member_no")); 
		map.put("INSERT_DATE", CurrDate.substring(0, 8)); 
		map.put("INSERT_TIME", CurrDate.substring(8, 14)); 
		map.put("TRAN_INF_UNIQUE_NBR", map.get("oid")); 
		return map;
	}


	
	
	/**
	 * Get the Index value 채번
	 * @param CompanyCd
	 * @param IndexType
	 * @param SeedDate
	 * @param Interval
	 * @return
	 * @throws Exception
	 */
	public long getIndex(String CompanyCd, String IndexType, String SeedDate, int Interval) throws Exception {
		long IndexValue = 0;
		Map<String,Object> params = new HashMap<String,Object>();
		try{			
			params.put("COMPANY_CD", CompanyCd);
			params.put("INDEX_TYPE", IndexType);
			params.put("CURRENT_DATE", SeedDate);
			params.put("FIRST_INDEX", Interval);
			
			// 채번
			paymentCheckMapper.GET_INDEX(params);
			
			IndexValue = Long.parseLong(String.valueOf(params.get("INDEX")));
			
		}catch(Exception e){
			throw e;
		}
		return IndexValue;
	}

	/**
	 * @param map
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public boolean isTmMembershipTaTranPayment(Map map) {
		if(paymentCheckMapper.isTmMembershipTaTranPayment(map) > 0)	return true;
		else return false;
	}
	
	
	/**
	 * 강좌 수료증 발급
	 * @param map
	 * 
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Map getCompleteLecture(Map map) {
		return paymentMapper.getCompleteLecture(map);
	}

}
