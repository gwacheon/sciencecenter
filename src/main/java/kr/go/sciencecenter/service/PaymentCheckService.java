package kr.go.sciencecenter.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kr.co.mainticket.Entity.Entity;
import kr.co.mainticket.Entity.EntityList;
import kr.co.mainticket.Science.Interlock.EncManager;
import kr.co.mainticket.Utility.Utility;
import kr.go.sciencecenter.controller.ScheduleController;
import kr.go.sciencecenter.persistence.PaymentCheckMapper;
import kr.go.sciencecenter.util.C2SException;
import kr.go.sciencecenter.util.DateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentCheckService {
	
	private static final Logger logger = LoggerFactory.getLogger(PaymentCheckService.class);
	
	@Autowired
	PaymentCheckMapper paymentCheckMapper;
	
	@Autowired
	PayRefundCheckService payRefundCheckService;
	
	private String ErrCode;
	private String ErrMessage;
		
	public String getErrCode(){
		return Utility.CheckNull(this.ErrCode);
	}
	
	public String getErrMessage(){
		return Utility.CheckNull(this.ErrMessage).replaceAll("\n", "\\\\n").replaceAll("\"", "'");
	}
	
	
	/**
	 * Get the Index value 채번
	 * @param CompanyCd
	 * @param IndexType
	 * @param SeedDate
	 * @param Interval
	 * @return
	 * @throws Exception
	 */
	public long getIndex(String CompanyCd, String IndexType, String SeedDate, int Interval) throws Exception {
		long IndexValue = 0;
		Map<String,Object> params = new HashMap<String,Object>();
		try{			
			params.put("COMPANY_CD", CompanyCd);
			params.put("INDEX_TYPE", IndexType);
			params.put("CURRENT_DATE", SeedDate);
			params.put("FIRST_INDEX", Interval);
			
			// 채번
			paymentCheckMapper.GET_INDEX(params);
			
			IndexValue = Long.parseLong(String.valueOf(params.get("INDEX")));
			
		}catch(Exception e){
			throw e;
		}
		return IndexValue;
	} 
	
	/**
	 * 예약하기
	 * Apply the Course
	 * @param CompanyCd
	 * @param DataEntity
	 * @return
	 * @throws Exception
	 */
	public Map<String,Object> CourseApply(String CompanyCd, Entity DataEntity) throws Exception{
		
		logger.info("PaymentCheckService.CourseApply -- ! " + DataEntity.toString());
		logger.info("PaymentCheckService.CourseApply.COERCION_FLAG -- ! " + DataEntity.getFieldNoCheck("COERCION_FLAG"));
		
		Map<String,Object> result = new HashMap<String,Object>();
		
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try {
			String SaleType = DataEntity.getFieldNoCheck("SALE_TYPE");
			String WinType = DataEntity.getFieldNoCheck("WIN_TYPE");
			String CourseCd = DataEntity.getFieldNoCheck("COURSE_CD");
			String PriceNbr = DataEntity.getFieldNoCheck("PRICE_NBR");
			String MemberNo = DataEntity.getFieldNoCheck("MEMBER_NO");
			Entity StudentNoList = DataEntity.getFieldEntityNoCheck("STUDENT_LIST");
			String ApplyType = DataEntity.getFieldNoCheck("APPLY_TYPE");
			boolean CoercionFlag = DataEntity.getFieldNoCheck("COERCION_FLAG").equals("Y")?true:false;
			String WorkerMemberNo = DataEntity.getFieldNoCheck("WORKER_MEMBER_NO");
			String ClientIp = DataEntity.getFieldNoCheck("CLIENT_IP");
			//String PeopleCnt = DataEntity.getFieldNoCheck("PEOPLE_CNT");
			
			if(CourseCd.length() == 0){
				throw new C2SException("CR01", "프로그램 코드가 없습니다."); //NOT_EXIST_COURSE_CD
			}
			else if(PriceNbr.length() == 0){
				throw new C2SException("CR02", "NOT_EXIST_PRICE_NBR"); //NOT_EXIST_PRICE_NBR
			}
			else if(MemberNo.length() == 0){
				throw new C2SException("CR03", "회원번호가 없습니다."); //NOT_EXIST_MEMBER_NO
			}
			else if(StudentNoList.getFieldEntitySize("STUDENT_ITEM") == 0){
				throw new C2SException("CR04", "학생번호가 없습니다."); //NOT_EXIST_STUDENT_NO
			}
			else if(!ApplyType.equals("RESER") && !ApplyType.equals("WAIT")){
				throw new C2SException("CR05", "강좌내역이 없습니다._TYPE");
			}
			
			if(SaleType.length() == 0){
				SaleType = "000002";
			}
			
			if(WinType.length() == 0){
				WinType = "000000";
			}
			
			if(WorkerMemberNo.length() == 0){
				WorkerMemberNo = MemberNo;
			}
			
			String CurrDate = DateTime.getFormatDateString("yyyyMMddHHmmss", 0);
			/*
			 * Set the Course Data
			 */
			Map<String,Object> CheckCon = new HashMap<String,Object>();
			
			CheckCon.put("COMPANY_CD", CompanyCd);
			CheckCon.put("COURSE_CD", CourseCd);
			CheckCon.put("PRICE_NBR", PriceNbr);
			System.out.println("Check :"+CheckCon.toString());
			// 강좌 체크
			List CheckList = paymentCheckMapper.SELECT_COURSE_CHECK(CheckCon);
			
			if(CheckList.size() == 0){
				throw new C2SException("CR06", "프로그램이 없습니다."); //NOT_EXIST_COURSE
			}
			
			Map<String,Object> CourseEntity = (Map)CheckList.get(0);
			
			String SemesterName = (String)CourseEntity.get("SEMESTER_NAME");
			String CourseName = (String)CourseEntity.get("COURSE_NAME");
			String PriceName = (String)CourseEntity.get("PRICE_NAME");
			String CoursePeriod = "";
			String CourseSchedule = "";
			
			if(Utility.CheckNull((String)CourseEntity.get("COURSE_DATE")).length() > 0){
				CoursePeriod = Utility.MarkDate(String.valueOf(CourseEntity.get("COURSE_DATE")), "-");
				CourseSchedule = String.valueOf(CourseEntity.get("COURSE_DATE")).substring(4, 6) + "." + String.valueOf(CourseEntity.get("COURSE_DATE")).substring(6, 8)
								+ "(" + DateTime.whichDayString(String.valueOf(CourseEntity.get("COURSE_DATE"))) + ")-"
								+ String.valueOf(CourseEntity.get("COURSE_START_TIME")).substring(0, 2) + ":" + String.valueOf(CourseEntity.get("COURSE_START_TIME")).substring(2, 4);
			}
			else{
				CoursePeriod = Utility.MarkDate(String.valueOf(CourseEntity.get("COURSE_START_DATE")), "-") + " ~ " + Utility.MarkDate(String.valueOf(CourseEntity.get("COURSE_END_DATE")), "-");
			}
			CoursePeriod += " " + String.valueOf(CourseEntity.get("PRICE_NAME"));
			
			/*
			 * Set the Receipt Target
			 * 접수대상(E : 기존수강생, N : 신규수강생)
			 */
			String ReceiptTarget = "N";
			
			if(String.valueOf(CourseEntity.get("RECEIPT_STATUS")).equals("EXIST_RECEIPT")){
				ReceiptTarget = "E";
			}
			
			/*
			 * Set the Reserve Term and Limit Days
			 */
			int ReserveTerm = Utility.StringToInt(String.valueOf(CourseEntity.get("RESERVE_TERM")));
			int ReserLimitDays = 0;
			
			if(String.valueOf(CourseEntity.get("SCHEDULE_FLAG")).equals("N") && String.valueOf(CourseEntity.get("SCHEDULE_RECEIPT_FLAG")).equals("Y")){
				ReserLimitDays = DateTime.daysBetween(CurrDate.substring(0, 8), String.valueOf(CourseEntity.get("RECEIPT_END_DATE")));
			}
			else if(ReceiptTarget.equals("E")){
				ReserLimitDays = DateTime.daysBetween(CurrDate.substring(0, 8), String.valueOf(CourseEntity.get("EXIST_END_DATE")));
			}
			else{
				ReserLimitDays = DateTime.daysBetween(CurrDate.substring(0, 8), String.valueOf(CourseEntity.get("NEW_END_DATE")));
			}
			
			int AddTerm = 0;
			String ReserveLimitDate = "";
			
			if(ApplyType.equals("RESER")){
				/*
				 * Set the Reserve Term, Add Term and Limit Date
				 */
				if(DataEntity.getFieldNoCheck("RESERVE_TERM").length() > 0){
					// 예약기간 사용자 지정
					ReserveTerm = Utility.StringToInt(DataEntity.getFieldNoCheck("RESERVE_TERM"));
				}
				else{
					if(ReserLimitDays < ReserveTerm){
						ReserveTerm = ReserLimitDays;
					}
				}
				
				if(ReserveTerm < 0){
					ReserveTerm = 0;
				}
				
				if(ReserveTerm == 0){
					// 당일 예약 마감 시 23:00 이후 예약한 경우 1일 추가
					if(Utility.StringToInt(CurrDate.substring(8, 12)) >= 2300){
						AddTerm = 1;
					}
				}
				
				ReserveLimitDate = DateTime.addDays(CurrDate.substring(0, 8), ReserveTerm + AddTerm);
			}
			
			/*
			 * SMS or E-mail Layout
			 */
			String SmsSendFlag = DataEntity.getFieldNoCheck("SMS_SEND_FLAG");
			String EmailSendFlag = DataEntity.getFieldNoCheck("EMAIL_SEND_FLAG");
			
			String ContentsType = "";
			
			if(SmsSendFlag.equals("Y") || EmailSendFlag.equals("Y")){
				if(ApplyType.equals("RESER")){
					ContentsType = "L00001";
				}
				else if(ApplyType.equals("WAIT")){
					ContentsType = "L00002";
				}
			}
			
			String StudentNo = "";
			String ApplyUniqueNbr = "";
			int StudentCnt = StudentNoList.getFieldEntitySize("STUDENT_ITEM");
			
			Entity StudentEntity = null;
			Entity PriceDetailList = null;
			Entity PriceDetailItem = null;
			Entity StudentList	   = null;
			Entity StudentPriceItem = null;
			Entity TargetList		= null;
			Entity TargetItem		= null;
			int PriceDetailCnt = 0;
			int StudentListCnt = 0;
			String PriceDetailNbr = "";
			int PriceApplyCnt = 0;
			int ApplyCnt = 0;
			long TotalPrice = 0;
			Entity OptionList = null;
			Entity OptionItem = null;
			int OptionCnt = 0;
			String OptionCd = "";
			
			String RspCd = "";
			String RspMsg = "";
			Entity ApplyInfoEntity = null;
			
			boolean CheckFlag = false;
			boolean CapacityResult = false;
			Map<String,Object> FamilyEntity = null;
			Map<String,Object> ApplyEntity = null;
			Map<String,Object> ApplyPriceEntity = null;
			Map<String,Object> ApplyOptionEntity = null;
			int UpdateCount = 0;
			
			for(int idx = 0; idx < StudentCnt; idx++){
				StudentNo = "";
				ApplyUniqueNbr = "";
				
				try{
					StudentEntity = StudentNoList.getFieldEntityNoCheck("STUDENT_ITEM", idx);
					
					StudentList = StudentEntity.getFieldEntityNoCheck("STUDENTPRICE_LIST");
					TargetList = StudentEntity.getFieldEntityNoCheck("TARGET_LIST");
					PriceDetailList = StudentEntity.getFieldEntityNoCheck("PRICE_DETAIL_LIST");
					
					
					/*
					 * Check Course Apply Able
					 */
					StudentListCnt = StudentList.getFieldEntitySize("STUDENTPRICE_ITEM");
					for(int studentIdx = 0;studentIdx < StudentListCnt;studentIdx++){
						StudentPriceItem = StudentList.getFieldEntityNoCheck("STUDENTPRICE_ITEM",studentIdx);
						StudentNo = StudentPriceItem.getFieldNoCheck("STUDENT_NO");
						
						CheckFlag = CheckApply(ApplyType, CompanyCd, SaleType, CourseCd, PriceNbr, StudentNo);
						if(CheckFlag == false){
							break;
						}
					}
					
					if(!CheckFlag){
						throw new C2SException("CR08", "FAIL_CHECK_DATA");
					}
					
					ApplyUniqueNbr = CompanyCd + "LEC" + CurrDate.substring(2, 8) + Utility.LPAD(getIndex(CompanyCd, "TRAN_UNIQUE_NBR", CurrDate.substring(0, 8), 1), 5, '0');
					logger.info("ApplyUniqueNbr -- " + ApplyUniqueNbr);
					
					PriceDetailCnt = PriceDetailList.getFieldEntitySize("PRICE_DETAIL_ITEM");
		
					for(int PriceIdx = 0; PriceIdx < PriceDetailCnt; PriceIdx++){
						PriceDetailItem = PriceDetailList.getFieldEntityNoCheck("PRICE_DETAIL_ITEM", PriceIdx);
						
						PriceApplyCnt = Utility.StringToInt(PriceDetailItem.getFieldNoCheck("APPLY_CNT"));
						PriceDetailNbr = PriceDetailItem.getFieldNoCheck("PRICE_DETAIL_NBR");
						
						if(PriceApplyCnt > 0 && PriceDetailNbr.length() > 0){
							ApplyPriceEntity = new HashMap<String,Object>();
							
							//PriceApplyCnt = Integer.parseInt(PeopleCnt) != 0 ? Integer.parseInt(PeopleCnt) : PriceApplyCnt;
							
							ApplyPriceEntity.put("APPLY_UNIQUE_NBR", ApplyUniqueNbr);				// APPLY_UNIQUE_NBR
							ApplyPriceEntity.put("APPLY_NBR", String.valueOf(PriceIdx+1));			// APPLY_NBR
							ApplyPriceEntity.put("APPLY_CNT", Integer.toString(PriceApplyCnt));		// APPLY_CNT
							ApplyPriceEntity.put("COMPANY_CD", CompanyCd);							// COMPANY_CD
							ApplyPriceEntity.put("COURSE_CD", CourseCd);							// COURSE_CD
							ApplyPriceEntity.put("PRICE_DETAIL_NBR", PriceDetailNbr);				// PRICE_DETAIL_NBR
							
							UpdateCount = paymentCheckMapper.INSERT_COURSE_APPLY_PRICE(ApplyPriceEntity);
							
							if(UpdateCount == 0){
								throw new C2SException("CR09", "FAILED_APPLY_PRICE");
							}
						}
					}
					
					CheckCon = new HashMap<String,Object>();
					
					CheckCon.put("COMPANY_CD", CompanyCd);
					CheckCon.put("APPLY_UNIQUE_NBR", ApplyUniqueNbr);
					
					CheckList = paymentCheckMapper.SELECT_COURSE_APPLY_PRICE_CHECK(CheckCon);
					
					ApplyCnt = 0;
					TotalPrice = 0;
					
					if(CheckList.size() > 0){
						ApplyPriceEntity = (Map)CheckList.get(0);
						
						ApplyCnt = Utility.StringToInt(String.valueOf(ApplyPriceEntity.get("APPLY_CNT")));
						TotalPrice = Utility.StringToLong(String.valueOf(ApplyPriceEntity.get("TOTAL_PRICE")));
					} 
					
					if(ApplyCnt == 0){
						throw new C2SException("CR10", "NOT_EXIST_APPLY_CNT");
					}
					
					// 신청 또는 대기 인원 추가
					CapacityResult = false;
					
					if(ApplyType.equals("RESER")){					 
						CapacityResult = SetReser(CompanyCd, CourseCd, PriceNbr, (ReceiptTarget.equals("E")?true:false), CoercionFlag, true, ApplyCnt);
					}
					else if(ApplyType.equals("WAIT")){
						CapacityResult = SetWait(CompanyCd, CourseCd, PriceNbr, (ReceiptTarget.equals("E")?true:false), CoercionFlag, true, ApplyCnt);
					}
					
					if(!CapacityResult){
						// FULL : 정원 초과, OVER : 정원부족, ABLE_RESER : 대기신청 시 예약전환 가능
						throw new C2SException("CR11", "강의의 정원이 초과되었습니다.");
					}
					
					try{
						StudentListCnt = StudentList.getFieldEntitySize("STUDENTPRICE_ITEM");
						
						for(int ApplyIdx = 0; ApplyIdx < StudentListCnt;ApplyIdx++){
							StudentPriceItem = StudentList.getFieldEntityNoCheck("STUDENTPRICE_ITEM",ApplyIdx);
							TargetItem		 = TargetList.getFieldEntityNoCheck("TARGET_ITEM",ApplyIdx);
							
							ApplyEntity = new HashMap<String,Object>();
						
							ApplyEntity.put("APPLY_UNIQUE_NBR", ApplyUniqueNbr);																// APPLY_UNIQUE_NBR
							ApplyEntity.put("APPLY_NBR", String.valueOf(ApplyIdx+1));															// APPLY_NBR
							ApplyEntity.put("SALE_TYPE", SaleType);																				// SALE_TYPE
							ApplyEntity.put("STUDENT_NO", StudentPriceItem.getFieldNoCheck("STUDENT_NO"));										// STUDENT_NO
							ApplyEntity.put("MEMBER_NO", MemberNo);																				// MEMBER_NO
							ApplyEntity.put("RECEIPT_TARGET", ReceiptTarget);																	// RECEIPT_TARGET
							ApplyEntity.put("RETAKE_FLAG", "N");																				// RETAKE_FLAG
							ApplyEntity.put("COURSE_PRICE", Long.toString(Utility.StringToLong(StudentPriceItem.getFieldNoCheck("PRICE"))));	// COURSE_PRICE
							//ApplyEntity.put("APPLY_CNT", Integer.toString(1));																	// APPLY_CNT
							ApplyEntity.put("APPLY_CNT", Integer.toString(Utility.StringToInt(StudentPriceItem.getFieldNoCheck("APPLY_CNT"))));	// APPLY_CNT
							ApplyEntity.put("TOTAL_PRICE", Long.toString(TotalPrice));															// TOTAL_PRICE
							ApplyEntity.put("TARGET_CD", TargetItem.getFieldNoCheck("TARGET_CD"));												// TARGET_CD
							ApplyEntity.put("PARENT_NAME", DataEntity.getFieldNoCheck("PARENT_NAME"));											// PARENT_NAME
							ApplyEntity.put("PARENT_CEL", DataEntity.getFieldNoCheck("PARENT_CEL"));											// PARENT_CEL
							ApplyEntity.put("WAIT_USER", WorkerMemberNo);																		// WAIT_USER
							ApplyEntity.put("WAIT_DATE", CurrDate.substring(0, 8));																// WAIT_DATE
							ApplyEntity.put("WAIT_TIME", CurrDate.substring(8));																// WAIT_TIME
							ApplyEntity.put("WAIT_IP", ClientIp);																				// WAIT_IP
							ApplyEntity.put("RESERVE_TERM", Integer.toString(ReserveTerm));														// RESERVE_TERM
							ApplyEntity.put("ADD_TERM", Integer.toString(AddTerm));																// ADD_TERM
							ApplyEntity.put("RESERVE_LIMIT_DATE", ReserveLimitDate);															// RESERVE_LIMIT_DATE
							ApplyEntity.put("RESER_USER", WorkerMemberNo);																		// RESER_USER
							ApplyEntity.put("RESER_DATE", CurrDate.substring(0, 8));															// RESER_DATE
							ApplyEntity.put("RESER_TIME", CurrDate.substring(8));																// RESER_TIME
							ApplyEntity.put("RESER_IP", ClientIp);																				// RESER_IP
							ApplyEntity.put("CANCEL_FLAG", "N");																				// CANCEL_FLAG
						if(TotalPrice == 0){
							ApplyEntity.put("PAY_FLAG", "Y");																					// PAY_FLAG
							ApplyEntity.put("RECEIPT_FLAG", "Y");																				// RECEIPT_FLAG			
							ApplyEntity.put("RECEIPT_DATE", CurrDate.substring(0, 8));															// RECEIPT_DATE
							ApplyEntity.put("RECEIPT_TIME", CurrDate.substring(8));																// RECEIPT_TIME
						}else{
							ApplyEntity.put("PAY_FLAG", "N");																					// PAY_FLAG
							ApplyEntity.put("RECEIPT_FLAG", "N");																				// RECEIPT_FLAG
						}
							ApplyEntity.put("REFUND_FLAG", "N");																				// REFUND_FLAG
							ApplyEntity.put("USE_FLAG", "Y");																					// USE_FLAG
							ApplyEntity.put("CONFIRM_FLAG", "N");																				// CONFIRM_FLAG
							ApplyEntity.put("INSERT_USER", WorkerMemberNo);																		// INSERT_USER
							ApplyEntity.put("INSERT_DATE", CurrDate.substring(0, 8));															// INSERT_DATE
							ApplyEntity.put("INSERT_TIME", CurrDate.substring(8));																// INSERT_TIME
							ApplyEntity.put("COMPANY_CD", CompanyCd);																			// COMPANY_CD
							ApplyEntity.put("APPLY_TRAN_TYPE", ApplyType.equals("RESER")?"R":"W");												// APPLY_TRAN_TYPE
							ApplyEntity.put("COURSE_CD", CourseCd);																				// COURSE_CD
							ApplyEntity.put("PRICE_NBR", PriceNbr);																				// PRICE_NBR
							
							UpdateCount = paymentCheckMapper.INSERT_COURSE_APPLY(ApplyEntity);
							
							
							//봉사활동의 옵션정보 입력 
							//OPD001:학교 , OPD002:학년, OPD003:반, OPD004:번호
							OptionList = StudentPriceItem.getFieldEntityNoCheck("OPTION_LIST");
							
							
							OptionCnt = OptionList.getFieldEntitySize("OPTION_ITEM");
							System.out.println("OptionCnt" + OptionCnt);
							for(int i = 0; i<OptionCnt; i++){
								OptionItem = OptionList.getFieldEntityNoCheck("OPTION_ITEM", i);
								System.out.println("OPTION_CD" + OptionItem.getField("OPTION_CD_" + i));
								System.out.println("OPTION_DATA" + OptionItem.getField("OPTION_DATA_" + i));
								ApplyEntity.put("OPTION_CD", OptionItem.getField("OPTION_CD_" + i));
								ApplyEntity.put("OPTION_DATA", OptionItem.getField("OPTION_DATA_" + i));
								paymentCheckMapper.INSERT_COURSE_APPLY_OPTION(ApplyEntity);
							}
							
							
							if(UpdateCount == 0){
								break;
							}
						}
						
						if(UpdateCount == 0){
							throw new C2SException("CR12", "FAILED_APPLY");
						}

					}catch(Exception ae){
						// 예약, 대기신청 실패 시 인원 복원
						if(ApplyType.equals("RESER")){ 
							CapacityResult = SetReser(CompanyCd, CourseCd, PriceNbr, (ReceiptTarget.equals("E")?true:false), CoercionFlag, false, ApplyCnt);
						}
						else if(ApplyType.equals("WAIT")){
							CapacityResult = SetWait(CompanyCd, CourseCd, PriceNbr, (ReceiptTarget.equals("E")?true:false), CoercionFlag, false, ApplyCnt);
						}
						
						throw new C2SException("CR14", ae.getMessage());
					}
					
					RspCd = "0000";
					RspMsg = "정상";

				}catch(C2SException ce){
					RspCd = ce.getExceptCode();
					RspMsg = ce.getMessage();
					ce.printStackTrace();
				}catch(Exception e){
					RspCd = "E999";
					RspMsg = e.getMessage();
					e.printStackTrace();
				}
				
				if(StudentNo.length() > 0){
					
					logger.info("ApplyUniqueNbr -- " + ApplyUniqueNbr);
					result.put("STUDENT_NO", StudentNo);
					result.put("APPLY_UNIQUE_NBR", ApplyUniqueNbr);
					result.put("COURSE_NAME", CourseName);
					result.put("RSP_CD", RspCd);
					result.put("RSP_MSG", RspMsg);
					result.put("TOTAL_PRICE", Long.toString(TotalPrice));
					result.put("APPLY_CNT", ApplyCnt);
				}
			}
			result.put("result", true);
		}catch(C2SException ce){
			ce.printStackTrace();
			this.ErrCode = ce.getExceptCode();
			this.ErrMessage = ce.getMessage();
			result.put("result", false);
			result.put("Msg", ce.getExceptCode()+" :"+ce.getMessage());
		}catch(Exception e){
			e.printStackTrace();
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			result.put("result", false);
			result.put("Msg", "E999 :"+e.getMessage());
		}
				
		return result;
	}
	
	
	/**
	 * 예약하기전 체크하는 로직인데.. 젠장
	 * @param ApplyType
	 * @param CompanyCd
	 * @param SaleType
	 * @param CourseCd
	 * @param PriceNbr
	 * @param StudentNo
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public boolean CheckApply(String ApplyType, String CompanyCd, String SaleType, String CourseCd, String PriceNbr, String StudentNo) throws Exception{
		
		if(CourseCd.length() == 0) 		throw new C2SException("CR01", "프로그램 코드가 없습니다.");
		if(PriceNbr.length() == 0) 		throw new C2SException("CR02", "NOT_EXIST_PRICE_NBR");
		if(StudentNo.length() == 0)		throw new C2SException("CR03", "학생번호가 없습니다.");
		if(SaleType.length() == 0) 		SaleType = "000002";
		
		/*
		 * Check the Course and Semester
		 */
		Map<String,Object> CheckCon = new HashMap<String,Object>();
		CheckCon.put("COMPANY_CD", CompanyCd);
		CheckCon.put("COURSE_CD", CourseCd);
		CheckCon.put("PRICE_NBR", PriceNbr);
		
		// 강좌 체크
		List CheckList = paymentCheckMapper.SELECT_COURSE_CHECK(CheckCon);
		if(CheckList.size() == 0)	throw new C2SException("CR06", "프로그램이 없습니다."); //NOT_EXIST_COURSE
		
		Map<String,Object> CourseEntity = (Map)CheckList.get(0);
		
		if(!String.valueOf(CourseEntity.get("PRICE_USE_FLAG")).equals("Y") 
			|| !String.valueOf(CourseEntity.get("COURSE_USE_FLAG")).equals("Y") 
			|| !String.valueOf(CourseEntity.get("SEMESTER_USE_FLAG")).equals("Y"))
			throw new C2SException("CR05", "비활성화 된 프로그램 입니다.(CR05)");
			
		else if(!String.valueOf(CourseEntity.get("COURSE_OPEN_FLAG")).equals("Y") 
			|| !String.valueOf(CourseEntity.get("SEMESTER_OPEN_FLAG")).equals("Y"))
			throw new C2SException("CR06", "비활성화 된 프로그램 입니다.(CR06)");
		
		// 현장기능 제약 제외
		if(!SaleType.equals("000001")){
			if(!String.valueOf(CourseEntity.get("PRICE_ONLINE_FLAG")).equals("Y") 
				|| !String.valueOf(CourseEntity.get("COURSE_ONLINE_FLAG")).equals("Y") 
				|| !String.valueOf(CourseEntity.get("SEMESTER_ONLINE_FLAG")).equals("Y"))
				throw new C2SException("CR07", "온라인 프로그램이 아닙니다.(CR07)");
			
			else if(!String.valueOf(CourseEntity.get("RECEIPT_STATUS")).equals("EXIST_RECEIPT") 
				&& !String.valueOf(CourseEntity.get("RECEIPT_STATUS")).equals("NEW_RECEIPT"))
				throw new C2SException("CR08", "NOT_RECEIPT");
			
			else if(String.valueOf(CourseEntity.get("SCHEDULE_FLAG")).equals("N") 
				&& String.valueOf(CourseEntity.get("SCHEDULE_RECEIPT_FLAG")).equals("Y") 
				&& !String.valueOf(CourseEntity.get("SCHEDULE_STATUS")).equals("RECEIPT"))
				throw new C2SException("CR09", "NOT_SCHEDULE_RECEIPT");
		}
		
		if(ApplyType.equals("LOTS")){
			if(!String.valueOf(CourseEntity.get("LOTS_FLAG")).equals("Y")) 			throw new C2SException("CR10", "WRONG_LOTS_FLAG");
			else if(!String.valueOf(CourseEntity.get("CONFIRM_FLAG")).equals("N"))	throw new C2SException("CR11", "WRONG_CONFIRM_FLAG");
			
		}
		else	if(String.valueOf(CourseEntity.get("LOTS_FLAG")).equals("Y"))	throw new C2SException("CR12", "WRONG_LOTS_FLAG");
		
		CheckCon = new HashMap<String,Object>();
		CheckCon.put("COURSE_CD", CourseCd);
		CheckCon.put("PRICE_NBR", PriceNbr);
		CheckCon.put("COMPANY_CD", CompanyCd);
		CheckCon.put("STUDENT_NO", StudentNo);
		
		// 2016.02.16 변경		맴버체크
		CheckList = paymentCheckMapper.SELECT_MEMBER_CHECK(CheckCon);
		
		if(CheckList.size() != 1)	throw new C2SException("CR11", "사용자가 존재하지 않습니다.(CR11)");
		
		Map<String,Object> MemberEntity = (Map)CheckList.get(0);
		
		if(!String.valueOf(MemberEntity.get("FAMILY_USE_FLAG")).equals("Y") 
			|| !String.valueOf(MemberEntity.get("MEMBER_USE_FLAG")).equals("Y"))
			throw new C2SException("CR12", "사용자가 존재하지 않습니다.(CR12)");
		
		// 현장기능 제약 제외
		if(!SaleType.equals("000001")){
			if(String.valueOf(CourseEntity.get("SCHEDULE_FLAG")).equals("N")){
				// 일정형인 경우 내역(동일 회차여부) 확인 신청/대기 내역 확인
				if(Utility.StringToInt(String.valueOf(MemberEntity.get("APPLY_PRICE_CNT"))) > 0) throw new C2SException("CR13", "이미 예약한 내역이 있습니다.\n예약내역에서 확인하세요.");
			}
			else{
				// 일정형이 아닌 경우 내역 확인 	
				//신청/대기 내역 확인	내용확인필요
				if(Utility.StringToInt(String.valueOf(MemberEntity.get("APPLY_CNT"))) > 0) throw new C2SException("CR13", "이미 예약한 내역이 있습니다.\n예약내역에서 확인하세요."); 
				// 신청/대기 기준기간 내 4건 초과 확인
				if(Utility.StringToInt(String.valueOf(MemberEntity.get("APPLY_SEMESTER_CNT"))) >= 4) throw new C2SException("CR14", "OVER_SEMESTER_CNT");
				// 신청/대기 중복일정 확인
				else if(Utility.StringToInt(String.valueOf(MemberEntity.get("APPLY_DUPLICATE_CNT"))) > 0) throw new C2SException("CR15", "신청 및 대기 일정중에 중복된 일정이 있습니다.");
			}
			
			// 우선접수 시 회원등급 확인	내용확인필요
//			if(String.valueOf(CourseEntity.get("RECEIPT_STATUS")).equals("EXIST_RECEIPT") 
//				&& Utility.StringToInt(String.valueOf(MemberEntity.get("PAID_MEMBERSHIP_CNT"))) <= 0)
//				throw new C2SException("CR16", "잘못된 사용자입니다.SHIP_TYPE");
			
			
			/*
			 * Check the Limit (Online)
			 * 나이제한 체크
			 */
			if(String.valueOf(CourseEntity.get("AGE_FLAG")).equals("Y")){
				String BirthDate = String.valueOf(MemberEntity.get("BIRTH_DATE"));
				
				if(BirthDate.length() != 8)	throw new C2SException("CR18", "WRONG_STUDENT_BIRTH_DATE");
				
				String CurrDate = DateTime.getFormatDateString("yyyyMMddHHmmss", 0);
				int CurrYear = Utility.StringToInt(CurrDate.substring(0, 4));
				int BirthYear = Utility.StringToInt(BirthDate.substring(0, 4));
				
				int Years = CurrYear - BirthYear + 1;
				int FullYears = CurrYear - BirthYear;
				
				if(Utility.StringToInt(BirthDate.substring(4, 8)) > Utility.StringToInt(CurrDate.substring(4, 8)))
					FullYears = FullYears - 1;
				
				int AgeLimitMin = Utility.StringToInt(String.valueOf(CourseEntity.get("AGE_LIMIT_MIN")));
				int AgeLimitMax = Utility.StringToInt(String.valueOf(CourseEntity.get("AGE_LIMIT_MAX")));
				
				if(String.valueOf(CourseEntity.get("AGE_TYPE")).equals("F") && (FullYears < AgeLimitMin || FullYears > AgeLimitMax))
					throw new C2SException("CR19", "신청자의 나이가 프로그램과 맞지 않습니다.(CR19)");
				
				else if(Years < AgeLimitMin || Years > AgeLimitMax)
					throw new C2SException("CR20", "신청자의 나이가 프로그램과 맞지 않습니다.(CR20)");
			}
		}
		
		return true;
	}
	
	/**
	 * 결제
	 * Course Apply Pay
	 * @param CompanyCd
	 * @param DataEntity
	 * @return
	 * @throws Exception
	 */
	public Map<String,Object> CourseApplyPay(String CompanyCd, Entity DataEntity, Map<String,Object> paraMap) throws Exception{
		Map<String,Object> result = new HashMap<String,Object>();		
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try {
			String SaleType = DataEntity.getFieldNoCheck("SALE_TYPE");
			String WinType = DataEntity.getFieldNoCheck("WIN_TYPE");
			String ApplyUniqueNbr = DataEntity.getFieldNoCheck("APPLY_UNIQUE_NBR");
			String WorkerMemberNo = DataEntity.getFieldNoCheck("WORKER_MEMBER_NO");
			String ClientIp = DataEntity.getFieldNoCheck("CLIENT_IP");
			
			if(ApplyUniqueNbr.length() == 0){
				throw new C2SException("AP01", "NOT_EXIST_APPLY_UNIQUE_NBR");
			}
			
			if(SaleType.length() == 0){
				SaleType = "000002";
			}
			
			if(WinType.length() == 0){
				WinType = "000000";
			}
			/*
			 * Set the Default Data
			 */
			String DiscountNbr = DataEntity.getFieldNoCheck("DISCOUNT_NBR");
			String CurrDate = DateTime.getFormatDateString("yyyyMMddHHmmss", 0);
			
			/*
			 * Check the Course Apply
			 */
			Map<String,Object> CheckCon = new HashMap<String,Object>();
			
			CheckCon.put("COMPANY_CD", CompanyCd);
			CheckCon.put("APPLY_UNIQUE_NBR", ApplyUniqueNbr);
			
			List CheckList = paymentCheckMapper.SELECT_COURSE_APPLY_CHECK(CheckCon);
			
			if(CheckList.size() == 0){
				throw new C2SException("AP02", "강좌내역이 없습니다.");
			}
			
			Map<String,Object> CheckEntity = (Map)CheckList.get(0);
						
			if(!String.valueOf(CheckEntity.get("RESER_FLAG")).equals("Y")){
				throw new C2SException("AP03", "예약내역에 없습니다.");
			}
			else if(!String.valueOf(CheckEntity.get("CANCEL_FLAG")).equals("N")){
				throw new C2SException("AP04", "취소할 수 있는 내역이 아닙니다.");
			}
			else if(!String.valueOf(CheckEntity.get("PAY_FLAG")).equals("N")){
				throw new C2SException("AP05", "WRONG_PAY_FLAG");
			}
			else if(!String.valueOf(CheckEntity.get("RECEIPT_FLAG")).equals("N")){
				throw new C2SException("AP06", "결제를 한 내역이 아닙니다.");
			}
			else if(!String.valueOf(CheckEntity.get("REFUND_FLAG")).equals("N")){
				throw new C2SException("AP07", "환불 할 수 있는 내역이 아닙니다.");
			}
			else if(DiscountNbr.length() > 0 && !String.valueOf(CheckEntity.get("DISCOUNT_FLAG")).equals("Y")){
				throw new C2SException("AP08", "NOT_USE_DISCOUNT");
			}
			
			if(!SaleType.equals("000001")){
				if(DateTime.daysBetween(CurrDate.substring(0, 8), String.valueOf(CheckEntity.get("RESERVE_LIMIT_DATE"))) < 0){
					throw new C2SException("AP09", "OVER_LIMIT_DATE");
				}
				else if(!String.valueOf(CheckEntity.get("PAY_ABLE_FLAG")).equals("Y")){
					throw new C2SException("AP10", "WRONG_PAY_ABLE_FLAG");
				}
			}
			
			String CourseName = String.valueOf(CheckEntity.get("COURSE_NAME"));
			String PriceName = String.valueOf(CheckEntity.get("PRICE_NAME"));
			String MemberNo = String.valueOf(CheckEntity.get("MEMBER_NO"));
			String StudentNo = MemberNo;
			long CoursePrice = Utility.StringToLong(String.valueOf(CheckEntity.get("COURSE_PRICE")));
			String ReserveLimitDate = String.valueOf(CheckEntity.get("RESERVE_LIMIT_DATE"));
			String ClassCd = String.valueOf(CheckEntity.get("CLASS_CD"));
			
			/*
			 * Check the Payment Data
			 */
			payRefundCheckService.CheckPayment(CompanyCd, "LEC", ClassCd, SaleType, DataEntity, false);
			
			/*
			 * Set the DISCOUNT
			 */
			String MembershipFlag = "";
			String MembershipType = "";
			String DiscountType = "";
			String DiscountAmt = "";
			long DiscountPrice = 0;
			
			if(DiscountNbr.length() > 0){
				CheckCon = new HashMap<String,Object>();
				
				CheckCon.put("COMPANY_CD", CompanyCd);
				CheckCon.put("MEMBER_NO", MemberNo);
				CheckCon.put("STUDENT_NO", StudentNo);
				CheckCon.put("DISCOUNT_NBR", DiscountNbr);
				
				CheckList = paymentCheckMapper.SELECT_COURSE_DISCOUNT_CHECK(CheckCon);
				
				if(CheckList.size() == 0){
					throw new C2SException("AP11", "NOT_EXIST_DISCOUNT");
				}
				
				CheckEntity = (Map)CheckList.get(0);
				
				if(!String.valueOf(CheckEntity.get("USE_FLAG")).equals("Y")){
					throw new C2SException("AP12", "WRONG_USE_FLAG");
				}
				else if(!SaleType.equals("000001") && !String.valueOf(CheckEntity.get("ONLINE_FLAG")).equals("Y")){
					throw new C2SException("AP13", "WRONG_ONLINE_FLAG");
				}
				else if(!String.valueOf(CheckEntity.get("TARGET_FLAG")).equals("Y")){
					throw new C2SException("AP14", "WRONG_TARGET_FLAG");
				}
				
				MembershipFlag = String.valueOf(CheckEntity.get("MEMBERSHIP_FLAG"));
				MembershipType = String.valueOf(CheckEntity.get("MEMBERSHIP_TYPE"));
				DiscountType = String.valueOf(CheckEntity.get("DISCOUNT_TYPE"));
				DiscountAmt = String.valueOf(CheckEntity.get("DISCOUNT_AMT"));
				
				if(DiscountType.equals("R")){
					DiscountPrice = CoursePrice * Utility.StringToLong(DiscountAmt) / 100;
				}
				else{
					DiscountPrice =  Utility.StringToLong(DiscountAmt);
				}
				
				System.out.println("DiscountPrice : " + DiscountPrice);
			}
			
			/*
			 * Set the DEDUCT_FLAG
			 */
			String DeductFlag = DataEntity.getFieldNoCheck("DEDUCT_FLAG");
			String DeductMemo = "";
			long DeductPrice = 0;
			
			if(DeductFlag.equals("Y")){
				DeductMemo = DataEntity.getFieldNoCheck("DEDUCT_FLAG");
				DeductPrice = Utility.StringToLong(DataEntity.getFieldNoCheck("DEDUCT_PRICE"));
			}
			else{
				DeductFlag = "N";
			}
			
			/*
			 * Set the TOTAL_PRICE
			 */
			long TotalPrice = CoursePrice - DiscountPrice - DeductPrice;
			
			if(TotalPrice < 0){
				throw new C2SException("AP15", "WRONG_TOTAL_PRICE");
			}
			
			/*
			 * Check the Payment and set the RECEIPT_FLAG
			 */
			Entity PaymentList = DataEntity.getFieldEntityNoCheck("PAYMENT_LIST");
			int PaymentCnt = PaymentList.getFieldEntitySize("PAYMENT_ITEM");
			Entity PaymentEntity = null;
			
			String PaymentType = "";
			long PaymentPrice = 0;
			long PaymentAmt = 0;
			long VatAmt = 0;
			long SvcAmt = 0;
			String ReceiptFlag = "Y";
			String ReceiptDate = "";
			String ReceiptTime = "";
			
			logger.info("PaymentCnt == " + PaymentList.getFieldEntitySize("PAYMENT_ITEM"));
			
			
			for(int idx = 0; idx < PaymentCnt; idx++){
				PaymentEntity = PaymentList.getFieldEntityNoCheck("PAYMENT_ITEM", idx);
				
				PaymentType = PaymentEntity.getFieldNoCheck("PAYMENT_TYPE");
				PaymentAmt = Utility.StringToLong(PaymentEntity.getFieldNoCheck("PAYMENT_AMT"));
				
				PaymentEntity.setField("SVC_AMT", "0");
				PaymentEntity.setField("VAT_AMT", Long.toString(PaymentAmt / 10));
				
				if(PaymentEntity.getField("PAYMENT_TYPE").equals("000003")){
					ReceiptFlag = "N";
				}
				
				PaymentPrice += PaymentAmt;
			}
			
			logger.info("TotalPrice == " + TotalPrice);
			logger.info("PaymentPrice == " + PaymentPrice);
			
			if(TotalPrice != PaymentPrice){
				throw new C2SException("AP16", "NOT_MATCH_PRICE");
			}
			
			VatAmt = PaymentPrice / 10;
			
			if(ReceiptFlag.equals("Y")){
				ReceiptDate = CurrDate.substring(0, 8);
				ReceiptTime = CurrDate.substring(8);
			}
			
			/*
			 * Update on TL_COURSE_APPLY
			 */
			Map<String,Object> ApplyEntity = new HashMap<String,Object>();
			
			ApplyEntity.put("DISCOUNT_NBR", DiscountNbr);						// DISCOUNT_NBR
			ApplyEntity.put("MEMBERSHIP_FLAG", MembershipFlag);					// MEMBERSHIP_FLAG
			ApplyEntity.put("MEMBERSHIP_TYPE", MembershipType);					// MEMBERSHIP_TYPE
			ApplyEntity.put("DISCOUNT_TYPE", DiscountType);						// DISCOUNT_TYPE
			ApplyEntity.put("DISCOUNT_AMT", DiscountAmt);						// DISCOUNT_AMT
			ApplyEntity.put("DISCOUNT_PRICE", Long.toString(DiscountPrice));	// DISCOUNT_PRICE
			ApplyEntity.put("DEDUCT_FLAG", DeductFlag);							// DEDUCT_FLAG
			ApplyEntity.put("DEDUCT_MEMO", DeductMemo);							// DEDUCT_MEMO
			ApplyEntity.put("DEDUCT_PRICE", Long.toString(DeductPrice));		// DEDUCT_PRICE
			ApplyEntity.put("DISCOUNT_CNT", "0");								// DISCOUNT_CNT
			ApplyEntity.put("TOTAL_PRICE", Long.toString(TotalPrice));			// TOTAL_PRICE
			ApplyEntity.put("PAYMENT_AMT", Long.toString(PaymentPrice));		// PAYMENT_AMT
			ApplyEntity.put("VAT_AMT", Long.toString(VatAmt));					// VAT_AMT
			ApplyEntity.put("SVC_AMT", Long.toString(SvcAmt));					// SVC_AMT
			ApplyEntity.put("PAY_FLAG", "Y");									// PAY_FLAG
			ApplyEntity.put("RECEIPT_FLAG", ReceiptFlag);						// RECEIPT_FLAG
			ApplyEntity.put("RECEIPT_DATE", ReceiptDate);						// RECEIPT_DATE
			ApplyEntity.put("RECEIPT_TIME", ReceiptTime);						// RECEIPT_TIME
			ApplyEntity.put("COMPANY_CD", CompanyCd);							// COMPANY_CD
			ApplyEntity.put("APPLY_UNIQUE_NBR", ApplyUniqueNbr);				// APPLY_UNIQUE_NBR
				
			int UpdateCount = paymentCheckMapper.UPDATE_COURSE_APPLY_PAY(ApplyEntity);
			
			if(UpdateCount == 0){
				throw new C2SException("AP17", "FAILED_UPDATE_PAY");
			}
			
			/*
			 * Payment Process
			 */
			if(PaymentCnt > 0){
				String MemberName = "";
				String MemberCel = "";
				String MemberEmail = "";
				
				CheckCon = new HashMap<String,Object>();
				
				CheckCon.put("COMPANY_CD", CompanyCd);
				CheckCon.put("MEMBER_NO", MemberNo);
				
				CheckList = paymentCheckMapper.SELECT_MEMBER(CheckCon);
				
				if(CheckList.size() > 0){
					CheckEntity = (Map)CheckList.get(0);
					
					MemberName = String.valueOf(CheckEntity.get("MEMBER_NAME"));
					MemberCel = String.valueOf(CheckEntity.get("MEMBER_CEL"));
					MemberEmail = String.valueOf(CheckEntity.get("MEMBER_EMAIL"));
				}
				else{
					throw new C2SException("AP18", "NOT_EXIST_MEMBER");
				}
				
				Entity TranInfo = new Entity(15);
				
				TranInfo.setField("MEMBER_NO", MemberNo);
				TranInfo.setField("CUSTOMER_NAME", MemberName);
				TranInfo.setField("CUSTOMER_TEL", MemberCel);
				TranInfo.setField("CUSTOMER_EMAIL", MemberEmail);
				TranInfo.setField("PRODUCT_NAME", CourseName + "(" + PriceName + ")");
				TranInfo.setField("SYSTEM_TRAN_TYPE", "C");
				TranInfo.setField("TRAN_UNIQUE_NBR", ApplyUniqueNbr);
				TranInfo.setField("TRAN_DETAIL_NBR", "1");
				TranInfo.setField("TRAN_TYPE", "11");
				TranInfo.setField("SALE_TYPE", SaleType);
				TranInfo.setField("WIN_TYPE", WinType);
				TranInfo.setField("REF_FLAG", "N");
				TranInfo.setField("WORKER_MEMBER_NO", WorkerMemberNo);
				
				boolean PaymentResult = payRefundCheckService.Payment( CompanyCd, "LEC", ClassCd, TranInfo, PaymentList, paraMap);
				
				if(!PaymentResult){
					throw new C2SException(payRefundCheckService.getErrCode(), payRefundCheckService.getErrMessage());
				}
				result.put("result","00");
				result.put("Msg", "");
			} else {
				result.put("result","99");
				result.put("Msg", "NOT PaymentCnt");
			}
			
		}catch(C2SException ce){
			ce.printStackTrace();
			this.ErrCode = ce.getExceptCode();
			this.ErrMessage = ce.getMessage();
			result.put("result",false);
			result.put("Msg", ce.getExceptCode()+" :"+ce.getMessage());
		}catch(Exception e){
			e.printStackTrace();
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			result.put("result",false);
			result.put("Msg", "E999 :"+e.getMessage());
		}
		
		return result;
	}
	
	/**
	 * Course Apply Refund
	 * @param CompanyCd
	 * @param DataEntity
	 * @return
	 * @throws Exception
	 */
	public Map<String,Object> CourseApplyRefund(String CompanyCd, Entity DataEntity) throws Exception{
		Map<String,Object> result = new HashMap<String,Object>();
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try {
			String SaleType = DataEntity.getFieldNoCheck("SALE_TYPE");
			String WinType = DataEntity.getFieldNoCheck("WIN_TYPE");
			String ApplyUniqueNbr = DataEntity.getFieldNoCheck("APPLY_UNIQUE_NBR");
			String WorkerMemberNo = DataEntity.getFieldNoCheck("WORKER_MEMBER_NO");
			String ClientIp = DataEntity.getFieldNoCheck("CLIENT_IP");
			
			if(ApplyUniqueNbr.length() == 0){
				throw new C2SException("AR01", "APPLY_UNIQUE_NBR");
			}
			
			if(SaleType.length() == 0){
				SaleType = "000002";
			}
			
			if(WinType.length() == 0){
				WinType = "000000";
			}
			
			String CurrDate = DateTime.getFormatDateString("yyyyMMddHHmmss", 0);
			
			/*
			 * Check the Course Apply
			 */
			Map<String,Object> CheckCon = new HashMap<String,Object>();
			
			CheckCon.put("COMPANY_CD", CompanyCd);
			CheckCon.put("APPLY_UNIQUE_NBR", ApplyUniqueNbr);
			
			List CheckList = paymentCheckMapper.SELECT_COURSE_APPLY_CHECK(CheckCon);
			
			if(CheckList.size() == 0){
				throw new C2SException("AR02", "강좌내역이 없습니다.");
			}
			
			Map<String,Object> CheckEntity = (Map)CheckList.get(0);
			
			if(!String.valueOf(CheckEntity.get("RESER_FLAG")).equals("Y")){
				throw new C2SException("AR03", "예약내역에 없습니다.");
			}
			else if(!String.valueOf(CheckEntity.get("CANCEL_FLAG")).equals("N")){
				throw new C2SException("AR04", "취소할 수 있는 내역이 아닙니다.");
			}
			else if(!String.valueOf(CheckEntity.get("PAY_FLAG")).equals("Y")){
				throw new C2SException("AR05", "WRONG_PAY_FLAG");
			}
			else if(!String.valueOf(CheckEntity.get("RECEIPT_FLAG")).equals("Y")){
				throw new C2SException("AR06", "결제를 한 내역이 아닙니다.");
			}
			else if(!String.valueOf(CheckEntity.get("REFUND_FLAG")).equals("N")){
				throw new C2SException("AR07", "환불 할 수 있는 내역이 아닙니다.");
			}
						
			if(!SaleType.equals("000001")){
//				if(DateTime.timesBetween(CurrDate.substring(0, 12), (String.valueOf(CheckEntity.get("SCHEDULE_FLAG")).equals("N")?String.valueOf(CheckEntity.get("SCHEDULE_REFUND_LIMIT_DATE")):String.valueOf(CheckEntity.get("REFUND_LIMIT_DATE"))) + String.valueOf(CheckEntity.get("REFUND_LIMIT_TIME"))) < 0){
//					throw new C2SException("AR08", "OVER_LIMIT_DATE");
//				}
				if(DateTime.timesBetween(CurrDate.substring(0, 12), String.valueOf(CheckEntity.get("SCHEDULE_REFUND_LIMIT_DATE"))) < 0){
					throw new C2SException("AR08", "환불기간이 지났습니다.");
				}
				else if(!String.valueOf(CheckEntity.get("MEMBER_NO")).equals(WorkerMemberNo)){
					// 온라인 시 환불자와 신청자 동일 체크
					throw new C2SException("AR09", "잘못된 사용자입니다.");
				}
			}
			
			String CourseCd = String.valueOf(CheckEntity.get("COURSE_CD"));
			String PriceNbr = String.valueOf(CheckEntity.get("PRICE_NBR"));
			String ReceiptTarget = String.valueOf(CheckEntity.get("RECEIPT_TARGET"));
			int ApplyCnt = Utility.StringToInt(String.valueOf(CheckEntity.get("APPLY_CNT")));
			int NonCardCnt = Utility.StringToInt(String.valueOf(CheckEntity.get("NON_CARD_CNT")));
			
			String MemberNo = String.valueOf(CheckEntity.get("MEMBER_NO"));
			String ClassCd = String.valueOf(CheckEntity.get("CLASS_CD"));
			String SemesterName = String.valueOf(CheckEntity.get("SEMESTER_NAME"));
			String CourseName = String.valueOf(CheckEntity.get("COURSE_NAME"));
			String PriceName = String.valueOf(CheckEntity.get("PRICE_NAME"));
			String CoursePeriod = "";
			String CourseSchedule = "";
			if(Utility.CheckNull((String)CheckEntity.get("COURSE_DATE")).length() > 0){
				CoursePeriod = Utility.MarkDate(String.valueOf(CheckEntity.get("COURSE_DATE")), "-");
				CourseSchedule = String.valueOf(CheckEntity.get("COURSE_DATE")).substring(4, 6) + "." + String.valueOf(CheckEntity.get("COURSE_DATE")).substring(6, 8)
								+ "(" + DateTime.whichDayString(String.valueOf(CheckEntity.get("COURSE_DATE"))) + ")-"
								+ String.valueOf(CheckEntity.get("COURSE_START_TIME")).substring(0, 2) + ":" + String.valueOf(CheckEntity.get("COURSE_START_TIME")).substring(2, 4);
			}
			else{
				CoursePeriod = Utility.MarkDate(String.valueOf(CheckEntity.get("COURSE_START_DATE")), "-") + " ~ " + Utility.MarkDate(String.valueOf(CheckEntity.get("COURSE_END_DATE")), "-");
			}
			String StudentNo = String.valueOf(CheckEntity.get("MEMBER_NO"));
			
			/*
			 * Check the Refund Data
			 */
			String ConfirmFlag = "N";
			String ConfirmUser = "";
			String ConfirmDate = "";
			String ConfirmTime = "";
			
			if(Utility.StringToLong(String.valueOf(CheckEntity.get("COURSE_PRICE"))) == 0){
				ConfirmFlag = "Y";
			}
			else{
				if(NonCardCnt == 0){
					ConfirmFlag = "Y";
					ConfirmUser = WorkerMemberNo;
					ConfirmDate = CurrDate.substring(0, 8);
					ConfirmTime = CurrDate.substring(8);
				}
			}
			
			/*
			 * Update on TL_COURSE_APPLY
			 */
			Map<String,Object> ApplyEntity = new HashMap<String,Object>();
			
			ApplyEntity.put("REFUND_FLAG", "Y");														// REFUND_FLAG
			ApplyEntity.put("REFUND_USER", WorkerMemberNo);												// REFUND_USER
			ApplyEntity.put("REFUND_DATE", CurrDate.substring(0, 8));									// REFUND_DATE
			ApplyEntity.put("REFUND_TIME", CurrDate.substring(8));										// REFUND_TIME
			ApplyEntity.put("REFUND_IP", ClientIp);														// REFUND_IP
			ApplyEntity.put("REFUND_BANK_CD", DataEntity.getFieldNoCheck("REFUND_BANK_CD"));			// REFUND_BANK_CD
			ApplyEntity.put("REFUND_ACCOUNT_NO", EncManager.getEncData("TL_COURSE_APPLY", "REFUND_ACCOUNT_NO", DataEntity.getFieldNoCheck("REFUND_ACCOUNT_NO")));			// REFUND_ACCOUNT_NO
			ApplyEntity.put("REFUND_ACCOUNT_OWNER", DataEntity.getFieldNoCheck("REFUND_ACCOUNT_OWNER"));// REFUND_ACCOUNT_OWNER
			ApplyEntity.put("CONFIRM_FLAG", ConfirmFlag);												// CONFIRM_FLAG
			ApplyEntity.put("CONFIRM_USER", ConfirmUser);												// CONFIRM_USER
			ApplyEntity.put("CONFIRM_DATE", ConfirmDate);												// CONFIRM_DATE
			ApplyEntity.put("CONFIRM_TIME", ConfirmTime);												// CONFIRM_TIME
			ApplyEntity.put("COMPANY_CD", CompanyCd);													// COMPANY_CD
			ApplyEntity.put("APPLY_UNIQUE_NBR", ApplyUniqueNbr);										// APPLY_UNIQUE_NBR
			
			int UpdateCount = paymentCheckMapper.UPDATE_COURSE_APPLY_REFUND(ApplyEntity);
			
			if(UpdateCount == 0){
				throw new C2SException("AR10", "FAILED_UPDATE_PAY");
			}
			
			if(NonCardCnt == 0){
				/*
				 * Check the Payment Data
				 */
				
				payRefundCheckService.CheckPayment( CompanyCd, "LEC", ClassCd, SaleType, DataEntity, true);
				
				/*
				 * Check the MEMBER
				 */
				String MemberName = "";
				String MemberCel = "";
				String MemberEmail = "";
				
				CheckCon = new HashMap<String,Object>();
				
				CheckCon.put("COMPANY_CD", CompanyCd);
				CheckCon.put("MEMBER_NO", MemberNo);
				
				CheckList = paymentCheckMapper.SELECT_MEMBER(CheckCon);
				
				if(CheckList.size() > 0){
					CheckEntity = (Map)CheckList.get(0);
					
					MemberName = String.valueOf(CheckEntity.get("MEMBER_NAME"));
					MemberCel = String.valueOf(CheckEntity.get("MEMBER_CEL"));
					MemberEmail = String.valueOf(CheckEntity.get("MEMBER_EMAIL"));
				}
				else{
					throw new C2SException("AR11", "NOT_EXIST_MEMBER");
				}
				
				/*
				 * Check the TRAN_PAYMENT
				 */
				String PaymentType = "";
				long CardPaidAmt = 0;
				long CashPaidAmt = 0;
				long PointPaidAmt = 0;
				long CardRefAmt = 0;
				long CashRefAmt = 0;
				long PointRefAmt = 0;
				
				CheckCon = new HashMap<String,Object>();
				CheckCon.put("APPLY_UNIQUE_NBR", ApplyUniqueNbr);
				CheckList = paymentCheckMapper.SELECT_TRAN_PAYMENT(CheckCon);
				
				for(int idx = 0; idx < CheckList.size(); idx++){
					CheckEntity = (Map)CheckList.get(idx);
					
					PaymentType = String.valueOf(CheckEntity.get("PAYMENT_TYPE"));
					
					if(String.valueOf(CheckEntity.get("REF_FLAG")).equals("Y")){
						if(PaymentType.equals("000002")){
							CardRefAmt += Utility.StringToLong(String.valueOf(CheckEntity.get("PAYMENT_AMT")));
						}
						else if(PaymentType.equals("000005")){
							PointRefAmt += Utility.StringToLong(String.valueOf(CheckEntity.get("PAYMENT_AMT")));
						}
						else{
							CashRefAmt += Utility.StringToLong(String.valueOf(CheckEntity.get("PAYMENT_AMT")));
						}
					}
					else{
						if(PaymentType.equals("000002")){
							CardPaidAmt += Utility.StringToLong(String.valueOf(CheckEntity.get("PAYMENT_AMT")));
						}
						else if(PaymentType.equals("000005")){
							PointPaidAmt += Utility.StringToLong(String.valueOf(CheckEntity.get("PAYMENT_AMT")));
						}
						else{
							CashPaidAmt += Utility.StringToLong(String.valueOf(CheckEntity.get("PAYMENT_AMT")));
						}
					}
				}
				
				long PaidAmt = (CardPaidAmt + CashPaidAmt + PointPaidAmt) - (CardRefAmt + CashRefAmt + PointRefAmt);
				
				/*
				 * Check the Amt
				 */
				long ApproveCancelAmt = CardPaidAmt - CardRefAmt;
				long PointCancelAmt = PointPaidAmt - PointRefAmt;
				
				if(CardRefAmt > 0 || CashRefAmt > 0 || PointRefAmt > 0){
					throw new C2SException("AR12", "WRONG_APPROVE_CANCEL_FLAG");
				}
				
				/*
				 * Payment Process
				 */
				Map<String,Object> RefundInfo = new HashMap<String,Object>();
				
				RefundInfo.put("SYSTEM_TRAN_TYPE", "C");
				RefundInfo.put("TRAN_UNIQUE_NBR", ApplyUniqueNbr);
				RefundInfo.put("TRAN_DETAIL_NBR", "1");
				RefundInfo.put("SALE_TYPE", SaleType);
				RefundInfo.put("WIN_TYPE", WinType);
				RefundInfo.put("APPROVE_CANCEL_FLAG", "Y");
				RefundInfo.put("POINT_CANCEL_FLAG", "Y");
				RefundInfo.put("WORKER_MEMBER_NO", WorkerMemberNo);
				RefundInfo.put("MEMBER_NO", MemberNo);
				RefundInfo.put("CUSTOMER_NAME", MemberName);
				RefundInfo.put("CUSTOMER_TEL", MemberCel);
				RefundInfo.put("CUSTOMER_EMAIL", MemberEmail);
				RefundInfo.put("PRODUCT_NAME", "");
				System.out.println("refundInfo :"+RefundInfo.toString());
				Entity RefundPaymentList = new Entity(1);
				Entity PaymentList = new Entity(1);
				
				boolean PaymentResult = Refund(CompanyCd, "LEC", ClassCd, RefundInfo, RefundPaymentList, PaymentList);
				
				if(!PaymentResult){
					throw new C2SException(this.ErrCode, this.ErrMessage);
				}
			}
			
			boolean CoercionFlag = true;
			
			// 예약 인원 감소
			boolean CapacityResult = SetReser(CompanyCd, CourseCd, PriceNbr, (ReceiptTarget.equals("E")?true:false), CoercionFlag, false, ApplyCnt);
					
			result.put("result", true);
			result.put("Msg", "");
		}catch(C2SException ce){
			ce.printStackTrace();
			this.ErrCode = ce.getExceptCode();
			this.ErrMessage = ce.getMessage();
			
			result.put("result", false);
			result.put("Msg", ce.getExceptCode()+" :"+ce.getMessage());
		}catch(Exception e){
			e.printStackTrace();
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			
			result.put("result", false);
			result.put("Msg","E999 :"+e.getMessage());
		}
		
		return result;
	}
	
	/**
	 * Course Apply Refund
	 * @param CompanyCd
	 * @param DataEntity
	 * @return
	 * @throws Exception
	 */
	public Map<String,Object> CourseApplyRefundVa(String CompanyCd, Entity DataEntity) throws Exception{
		Map<String,Object> result = new HashMap<String,Object>();
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try {
			String SaleType = DataEntity.getFieldNoCheck("SALE_TYPE");
			String WinType = DataEntity.getFieldNoCheck("WIN_TYPE");
			String ApplyUniqueNbr = DataEntity.getFieldNoCheck("APPLY_UNIQUE_NBR");
			String WorkerMemberNo = DataEntity.getFieldNoCheck("WORKER_MEMBER_NO");
			String ClientIp = DataEntity.getFieldNoCheck("CLIENT_IP");
			
			if(ApplyUniqueNbr.length() == 0){
				throw new C2SException("AR01", "APPLY_UNIQUE_NBR");
			}
			
			if(SaleType.length() == 0){
				SaleType = "000002";
			}
			
			if(WinType.length() == 0){
				WinType = "000000";
			}
			
			String CurrDate = DateTime.getFormatDateString("yyyyMMddHHmmss", 0);
			
			/*
			 * Check the Course Apply
			 */
			Map<String,Object> CheckCon = new HashMap<String,Object>();
			
			CheckCon.put("COMPANY_CD", CompanyCd);
			CheckCon.put("APPLY_UNIQUE_NBR", ApplyUniqueNbr);
			
			List CheckList = paymentCheckMapper.SELECT_COURSE_APPLY_CHECK(CheckCon);
			
			if(CheckList.size() == 0){
				throw new C2SException("AR02", "강좌내역이 없습니다.");
			}
			
			Map<String,Object> CheckEntity = (Map)CheckList.get(0);
			
			if(!String.valueOf(CheckEntity.get("RESER_FLAG")).equals("Y")){
				throw new C2SException("AR03", "예약내역에 없습니다.");
			}
			else if(!String.valueOf(CheckEntity.get("CANCEL_FLAG")).equals("N")){
				throw new C2SException("AR04", "취소할 수 있는 내역이 아닙니다.");
			}
			else if(!String.valueOf(CheckEntity.get("PAY_FLAG")).equals("Y")){
				throw new C2SException("AR05", "WRONG_PAY_FLAG");
			}
			else if(!String.valueOf(CheckEntity.get("RECEIPT_FLAG")).equals("Y")){
				throw new C2SException("AR06", "결제를 한 내역이 아닙니다.");
			}
			else if(!String.valueOf(CheckEntity.get("REFUND_FLAG")).equals("N")){
				throw new C2SException("AR07", "환불 할 수 있는 내역이 아닙니다.");
			}
						
			if(!SaleType.equals("000001")){
//				if(DateTime.timesBetween(CurrDate.substring(0, 12), (String.valueOf(CheckEntity.get("SCHEDULE_FLAG")).equals("N")?String.valueOf(CheckEntity.get("SCHEDULE_REFUND_LIMIT_DATE")):String.valueOf(CheckEntity.get("REFUND_LIMIT_DATE"))) + String.valueOf(CheckEntity.get("REFUND_LIMIT_TIME"))) < 0){
//					throw new C2SException("AR08", "OVER_LIMIT_DATE");
//				}
				if(DateTime.timesBetween(CurrDate.substring(0, 12), String.valueOf(CheckEntity.get("SCHEDULE_REFUND_LIMIT_DATE"))) < 0){
					throw new C2SException("AR08", "환불기간이 지났습니다.");
				}
				else if(!String.valueOf(CheckEntity.get("MEMBER_NO")).equals(WorkerMemberNo)){
					// 온라인 시 환불자와 신청자 동일 체크
					throw new C2SException("AR09", "잘못된 사용자입니다.");
				}
			}
			
			String CourseCd = String.valueOf(CheckEntity.get("COURSE_CD"));
			String PriceNbr = String.valueOf(CheckEntity.get("PRICE_NBR"));
			String ReceiptTarget = String.valueOf(CheckEntity.get("RECEIPT_TARGET"));
			int ApplyCnt = Utility.StringToInt(String.valueOf(CheckEntity.get("APPLY_CNT")));
			int NonCardCnt = Utility.StringToInt(String.valueOf(CheckEntity.get("NON_CARD_CNT")));
			
			String MemberNo = String.valueOf(CheckEntity.get("MEMBER_NO"));
			String ClassCd = String.valueOf(CheckEntity.get("CLASS_CD"));
			String SemesterName = String.valueOf(CheckEntity.get("SEMESTER_NAME"));
			String CourseName = String.valueOf(CheckEntity.get("COURSE_NAME"));
			String PriceName = String.valueOf(CheckEntity.get("PRICE_NAME"));
			String CoursePeriod = "";
			String CourseSchedule = "";
			if(String.valueOf(CheckEntity.get("COURSE_DATE")).length() > 0){
				CoursePeriod = Utility.MarkDate(String.valueOf(CheckEntity.get("COURSE_DATE")), "-");
				CourseSchedule = String.valueOf(CheckEntity.get("COURSE_DATE")).substring(4, 6) + "." + String.valueOf(CheckEntity.get("COURSE_DATE")).substring(6, 8)
								+ "(" + DateTime.whichDayString(String.valueOf(CheckEntity.get("COURSE_DATE"))) + ")-"
								+ String.valueOf(CheckEntity.get("COURSE_START_TIME")).substring(0, 2) + ":" + String.valueOf(CheckEntity.get("COURSE_START_TIME")).substring(2, 4);
			}
			else{
				CoursePeriod = Utility.MarkDate(String.valueOf(CheckEntity.get("COURSE_START_DATE")), "-") + " ~ " + Utility.MarkDate(String.valueOf(CheckEntity.get("COURSE_END_DATE")), "-");
			}
			String StudentNo = String.valueOf(CheckEntity.get("MEMBER_NO"));
			
			/*
			 * Check the Refund Data
			 */
			String ConfirmFlag = "N";
			String ConfirmUser = "";
			String ConfirmDate = "";
			String ConfirmTime = "";
			
			if(Utility.StringToLong(String.valueOf(CheckEntity.get("COURSE_PRICE"))) == 0){
				ConfirmFlag = "Y";
			}
			else{
				if(NonCardCnt == 0){
					ConfirmFlag = "Y";
					ConfirmUser = WorkerMemberNo;
					ConfirmDate = CurrDate.substring(0, 8);
					ConfirmTime = CurrDate.substring(8);
				}
			}
			
			/*
			 * Update on TL_COURSE_APPLY
			 */
			Map<String,Object> ApplyEntity = new HashMap<String,Object>();
			
			ApplyEntity.put("REFUND_FLAG", "Y");														// REFUND_FLAG
			ApplyEntity.put("REFUND_USER", WorkerMemberNo);												// REFUND_USER
			ApplyEntity.put("REFUND_DATE", CurrDate.substring(0, 8));									// REFUND_DATE
			ApplyEntity.put("REFUND_TIME", CurrDate.substring(8));										// REFUND_TIME
			ApplyEntity.put("REFUND_IP", ClientIp);														// REFUND_IP
			ApplyEntity.put("REFUND_BANK_CD", DataEntity.getFieldNoCheck("REFUND_BANK_CD"));			// REFUND_BANK_CD
			ApplyEntity.put("REFUND_ACCOUNT_NO", EncManager.getEncData("TL_COURSE_APPLY", "REFUND_ACCOUNT_NO", DataEntity.getFieldNoCheck("REFUND_ACCOUNT_NO")));			// REFUND_ACCOUNT_NO
			ApplyEntity.put("REFUND_ACCOUNT_OWNER", DataEntity.getFieldNoCheck("REFUND_ACCOUNT_OWNER"));// REFUND_ACCOUNT_OWNER
			ApplyEntity.put("CONFIRM_FLAG", ConfirmFlag);												// CONFIRM_FLAG
			ApplyEntity.put("CONFIRM_USER", ConfirmUser);												// CONFIRM_USER
			ApplyEntity.put("CONFIRM_DATE", ConfirmDate);												// CONFIRM_DATE
			ApplyEntity.put("CONFIRM_TIME", ConfirmTime);												// CONFIRM_TIME
			ApplyEntity.put("COMPANY_CD", CompanyCd);													// COMPANY_CD
			ApplyEntity.put("APPLY_UNIQUE_NBR", ApplyUniqueNbr);										// APPLY_UNIQUE_NBR
			
			int UpdateCount = paymentCheckMapper.UPDATE_COURSE_APPLY_REFUND(ApplyEntity);
			
			if(UpdateCount == 0){
				throw new C2SException("AR10", "FAILED_UPDATE_PAY");
			}
			
		//	if(NonCardCnt == 0){
				/*
				 * Check the Payment Data
				 */
				
				payRefundCheckService.CheckPayment( CompanyCd, "LEC", ClassCd, SaleType, DataEntity, true);
				
				/*
				 * Check the MEMBER
				 */
				String MemberName = "";
				String MemberCel = "";
				String MemberEmail = "";
				
				CheckCon = new HashMap<String,Object>();
				
				CheckCon.put("COMPANY_CD", CompanyCd);
				CheckCon.put("MEMBER_NO", MemberNo);
				
				CheckList = paymentCheckMapper.SELECT_MEMBER(CheckCon);
				
				if(CheckList.size() > 0){
					CheckEntity = (Map)CheckList.get(0);
					
					MemberName = String.valueOf(CheckEntity.get("MEMBER_NAME"));
					MemberCel = String.valueOf(CheckEntity.get("MEMBER_CEL"));
					MemberEmail = String.valueOf(CheckEntity.get("MEMBER_EMAIL"));
				}
				else{
					throw new C2SException("AR11", "NOT_EXIST_MEMBER");
				}
				
				/*
				 * Check the TRAN_PAYMENT
				 */
				String PaymentType = "";
				long CardPaidAmt = 0;
				long CashPaidAmt = 0;
				long PointPaidAmt = 0;
				long CardRefAmt = 0;
				long CashRefAmt = 0;
				long PointRefAmt = 0;
				
				CheckCon = new HashMap<String,Object>();
				CheckCon.put("APPLY_UNIQUE_NBR", ApplyUniqueNbr);
				CheckList = paymentCheckMapper.SELECT_TRAN_PAYMENT(CheckCon);
				
				for(int idx = 0; idx < CheckList.size(); idx++){
					CheckEntity = (Map)CheckList.get(idx);
					
					PaymentType = String.valueOf(CheckEntity.get("PAYMENT_TYPE"));
					
					if(String.valueOf(CheckEntity.get("REF_FLAG")).equals("Y")){
						if(PaymentType.equals("000002")){
							CardRefAmt += Utility.StringToLong(String.valueOf(CheckEntity.get("PAYMENT_AMT")));
						}
						else if(PaymentType.equals("000005")){
							PointRefAmt += Utility.StringToLong(String.valueOf(CheckEntity.get("PAYMENT_AMT")));
						}
						else{
							CashRefAmt += Utility.StringToLong(String.valueOf(CheckEntity.get("PAYMENT_AMT")));
						}
					}
					else{
						if(PaymentType.equals("000002")){
							CardPaidAmt += Utility.StringToLong(String.valueOf(CheckEntity.get("PAYMENT_AMT")));
						}
						else if(PaymentType.equals("000005")){
							PointPaidAmt += Utility.StringToLong(String.valueOf(CheckEntity.get("PAYMENT_AMT")));
						}
						else{
							CashPaidAmt += Utility.StringToLong(String.valueOf(CheckEntity.get("PAYMENT_AMT")));
						}
					}
				}
				
				long PaidAmt = (CardPaidAmt + CashPaidAmt + PointPaidAmt) - (CardRefAmt + CashRefAmt + PointRefAmt);
				
				/*
				 * Check the Amt
				 */
				long ApproveCancelAmt = CardPaidAmt - CardRefAmt;
				long PointCancelAmt = PointPaidAmt - PointRefAmt;
				
				if(CardRefAmt > 0 || CashRefAmt > 0 || PointRefAmt > 0){
					throw new C2SException("AR12", "WRONG_APPROVE_CANCEL_FLAG");
				}
				
				/*
				 * Payment Process
				 */
				Map<String,Object> RefundInfo = new HashMap<String,Object>();
				
				RefundInfo.put("SYSTEM_TRAN_TYPE", "C");
				RefundInfo.put("TRAN_UNIQUE_NBR", ApplyUniqueNbr);
				RefundInfo.put("TRAN_DETAIL_NBR", "1");
				RefundInfo.put("SALE_TYPE", SaleType);
				RefundInfo.put("WIN_TYPE", WinType);
				RefundInfo.put("APPROVE_CANCEL_FLAG", "Y");
				RefundInfo.put("POINT_CANCEL_FLAG", "Y");
				RefundInfo.put("WORKER_MEMBER_NO", WorkerMemberNo);
				RefundInfo.put("MEMBER_NO", MemberNo);
				RefundInfo.put("CUSTOMER_NAME", MemberName);
				RefundInfo.put("CUSTOMER_TEL", MemberCel);
				RefundInfo.put("CUSTOMER_EMAIL", MemberEmail);
				RefundInfo.put("PRODUCT_NAME", "");
				
				Entity RefundPaymentList = DataEntity.getFieldEntityNoCheck("REFUND_PAYMENT_LIST");
				Entity PaymentList = new Entity(1);
				
				boolean PaymentResult = Refund(CompanyCd, "LEC", ClassCd, RefundInfo, RefundPaymentList, PaymentList);
				
				if(!PaymentResult){
					throw new C2SException(this.ErrCode, this.ErrMessage);
				}
		//	}
			
			boolean CoercionFlag = true;
			
			// 예약 인원 감소
			boolean CapacityResult = SetReser(CompanyCd, CourseCd, PriceNbr, (ReceiptTarget.equals("E")?true:false), CoercionFlag, false, ApplyCnt);
					
			result.put("result", true);
			result.put("Msg", "");
		}catch(C2SException ce){
			ce.printStackTrace();
			this.ErrCode = ce.getExceptCode();
			this.ErrMessage = ce.getMessage();
			
			result.put("result", false);
			result.put("Msg", ce.getExceptCode()+" :"+ce.getMessage());
		}catch(Exception e){
			e.printStackTrace();
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			
			result.put("result", false);
			result.put("Msg","E999 :"+e.getMessage());
		}
		
		return result;
	}
		
	/**
	 * Course Apply Cancel
	 * @param CompanyCd
	 * @param DataEntity
	 * @return
	 * @throws Exception
	 */
	public Map<String,Object> CourseApplyCancel(String CompanyCd, Entity DataEntity) throws Exception{
		Map<String,Object> result = new HashMap<String,Object>();
		Map<String,Object> params = new HashMap<String,Object>();
		
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try {
			String SaleType = DataEntity.getFieldNoCheck("SALE_TYPE");
			String WinType = DataEntity.getFieldNoCheck("WIN_TYPE");
			String ApplyUniqueNbr = DataEntity.getFieldNoCheck("APPLY_UNIQUE_NBR");
			String WorkerMemberNo = DataEntity.getFieldNoCheck("WORKER_MEMBER_NO");
			String ClientIp = DataEntity.getFieldNoCheck("CLIENT_IP");
			
			if(ApplyUniqueNbr.length() == 0){
				throw new C2SException("AC01", "NOT_EXIST_APPLY_UNIQUE_NBR");
			}
			
			if(SaleType.length() == 0){
				SaleType = "000002";
			}
			
			if(WinType.length() == 0){
				WinType = "000000";
			}
			
			String CurrDate = DateTime.getFormatDateString("yyyyMMddHHmmss", 0);
						
			/*
			 * Check the Course Apply
			 */
			Entity CheckCon = new Entity(3);
			
			params.put("COMPANY_CD", CompanyCd);
			params.put("APPLY_UNIQUE_NBR", ApplyUniqueNbr);
			
			// 강좌 신청자 체크
			List CheckList = paymentCheckMapper.SELECT_COURSE_APPLY_CHECK(params);
			
			if(CheckList.size() == 0){
				throw new C2SException("AC02", "강좌내역이 없습니다.");
			}
			
			Map<String,Object> CourseEntity = (Map)CheckList.get(0);
			
			if(!((String)CourseEntity.get("CANCEL_FLAG")).equals("N")){
				throw new C2SException("AC03", "취소할 수 있는 내역이 아닙니다.");
			}
			else if(!((String)CourseEntity.get("RECEIPT_FLAG")).equals("N")){
				throw new C2SException("AC04", "결제를 한 내역이 아닙니다.");
			}
			else if(!((String)CourseEntity.get("REFUND_FLAG")).equals("N")){
				throw new C2SException("AC05", "환불 할 수 있는 내역이 아닙니다.");
			}
			else if(!SaleType.equals("000001") && !((String)CourseEntity.get("MEMBER_NO")).equals(WorkerMemberNo)){
				// 온라인 시 취소자와 신청자 동일 체크
				throw new C2SException("AC06", "잘못된 사용자입니다.");
			}
			
			String CourseCd = String.valueOf(CourseEntity.get("COURSE_CD"));
			String ReceiptTarget = String.valueOf(CourseEntity.get("RECEIPT_TARGET"));
			String PriceNbr = String.valueOf(CourseEntity.get("PRICE_NBR"));
			int ApplyCnt = Utility.StringToInt(String.valueOf(CourseEntity.get("APPLY_CNT")));
			String ClassCd = String.valueOf(CourseEntity.get("CLASS_CD"));
			String ApplyType = "WAIT";
			
			if(String.valueOf(CourseEntity.get("RESER_FLAG")).equals("Y")){
				ApplyType = "RESER";
			}
			
			if(String.valueOf(CourseEntity.get("PAY_FLAG")).equals("Y")){
				/*
				 * Check the TRAN_PAYMENT
				 */
				
				Map<String,Object> CheckEntity = null;
				CheckList = paymentCheckMapper.SELECT_TRAN_PAYMENT(params);
				
				for(int idx = 0; idx < CheckList.size(); idx++){
					CheckEntity = (Map)CheckList.get(idx);
					
					if(String.valueOf(CheckEntity.get("RECEIPT_FLAG")).equals("Y")){
						throw new C2SException("AC07", "EXIST_RECEIPT_PAYMENT");
					}
				}
				
				/*
				 * Refund the Virtual Account (not receipt)
				 */
				Map<String,Object> RefundInfo = new HashMap<String,Object>();
				
				RefundInfo.put("SYSTEM_TRAN_TYPE", "C");
				RefundInfo.put("TRAN_UNIQUE_NBR", ApplyUniqueNbr);
				RefundInfo.put("TRAN_DETAIL_NBR", "1");
				RefundInfo.put("SALE_TYPE", SaleType);
				RefundInfo.put("WIN_TYPE", WinType);
				RefundInfo.put("APPROVE_CANCEL_FLAG", "Y");
				RefundInfo.put("POINT_CANCEL_FLAG", "Y");
				RefundInfo.put("WORKER_MEMBER_NO", WorkerMemberNo);
				
				Entity RefundPaymentList = new Entity(1);
				Entity PaymentList = new Entity(1);
				
				boolean PaymentResult = Refund(CompanyCd,"LEC", ClassCd, RefundInfo, RefundPaymentList, PaymentList);
				
				if(!PaymentResult){
					throw new C2SException(this.ErrCode, this.ErrMessage);
				}
			}
			
			/*
			 * Update on TL_COURSE_APPLY
			 */
			Map<String,Object> ApplyEntity = new HashMap<String,Object>();
			
			ApplyEntity.put("CANCEL_FLAG", "Y");									// CANCEL_FLAG
			ApplyEntity.put("CANCEL_USER", WorkerMemberNo);							// CANCEL_USER
			ApplyEntity.put("CANCEL_DATE", CurrDate.substring(0, 8));				// CANCEL_DATE
			ApplyEntity.put("CANCEL_TIME", CurrDate.substring(8));					// CANCEL_TIME
			ApplyEntity.put("CANCEL_IP", ClientIp);									// CANCEL_IP
			ApplyEntity.put("COMPANY_CD", CompanyCd);								// COMPANY_CD
			ApplyEntity.put("APPLY_UNIQUE_NBR", ApplyUniqueNbr);					// APPLY_UNIQUE_NBR
			
			int UpdateCount = paymentCheckMapper.UPDATE_COURSE_APPLY_CANCEL(ApplyEntity);
			
			if(UpdateCount == 0){
				throw new C2SException("AC08", "FAILED_UPDATE_CANCEL");
			}
			
			boolean CoercionFlag = true;
			boolean CapacityResult = false;
			
			if(ApplyType.equals("WAIT")){
				// 대기 인원 감소
				CapacityResult = SetWait(CompanyCd, CourseCd, PriceNbr, (ReceiptTarget.equals("E")?true:false), CoercionFlag, false, ApplyCnt);
			}
			else{
				// 예약 인원 감소
				CapacityResult = SetReser(CompanyCd, CourseCd, PriceNbr, (ReceiptTarget.equals("E")?true:false), CoercionFlag, false, ApplyCnt);
			}
			
			if(!CapacityResult){
				throw new C2SException("AC09", this.ErrMessage);
			}
			
			result.put("result", true);
		}catch(C2SException ce){
			ce.printStackTrace();
			this.ErrCode = ce.getExceptCode();
			this.ErrMessage = ce.getMessage();
			
			result.put("result", false);
			result.put("Msg", ce.getExceptCode()+" :"+ce.getMessage());
		}catch(Exception e){
			e.printStackTrace();
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			
			result.put("result", false);
			result.put("Msg","E999 :"+e.getMessage());
		}

		return result;
	}
	
	/**
	 * 환불 
	 * @param conn
	 * @param CompanyCd
	 * @param SystemType
	 * @param ServiceType
	 * @param RefundInfo
	 * @param RefundPaymentList
	 * @param PaymentList
	 * @return
	 * @throws Exception
	 */
	public boolean Refund(String CompanyCd, String SystemType, String ServiceType, Map<String,Object> RefundInfo, Entity RefundPaymentList, Entity PaymentList) throws Exception{
		boolean result = false;
		Map<String,Object> params = new HashMap<String,Object>();
		Map<String,Object> paraMap = new HashMap<String,Object>();
		
		try{
			if(ServiceType.length() == 0){
				ServiceType = "XXXXXX";
			}
			
			String CurrDate = DateTime.getFormatDateString("yyyyMMddHHmmss", 0);
			
			String SystemTranType = (String)RefundInfo.get("SYSTEM_TRAN_TYPE");
			String TranUniqueNbr = (String)RefundInfo.get("TRAN_UNIQUE_NBR");
			String TranDetailNbr = (String)RefundInfo.get("TRAN_DETAIL_NBR");
			String SaleType = (String)RefundInfo.get("SALE_TYPE");
			String WinType = (String)RefundInfo.get("WIN_TYPE");
			boolean ApproveCancelFlag = (String.valueOf(RefundInfo.get("APPROVE_CANCEL_FLAG")).equals("Y")?true:false);
			boolean PointCancelFlag = (String.valueOf(RefundInfo.get("POINT_CANCEL_FLAG")).equals("Y")?true:false);
			String WorkerMemberNo = (String)RefundInfo.get("WORKER_MEMBER_NO");
			
			paraMap.put("APPLY_UNIQUE_NBR", TranUniqueNbr);
			
			/*
			 * Refund Payment List
			 */
			int RefundPaymentCnt = RefundPaymentList.getFieldEntitySize("REFUND_PAYMENT_ITEM");
			boolean CashableRefundFlag = (RefundPaymentCnt > 0?true:false);
			
			String TranNbr = Utility.LPAD(getIndex(CompanyCd, "TRAN_NBR", CurrDate.substring(0, 8), 1), 6, '0');
			String TranType = "61";
			String RefFlag = "Y";
						
			String PaymentType = "";
			String InfUniqueNbr = "";
			
			Map<String,Object> CancelMap = null;
			Map<String,Object> RefStatusMap = null;
			Map<String,Object> TranPaymentMap = null;
			int UpdateCount = 0;
			
			/*
			 * Check Approve Cancel and Refund Cashable Payment
			 */
			if(ApproveCancelFlag || PointCancelFlag){
				/*
				 * Tran Payment Data
				 */
				boolean CheckCancelFlag = false;
				boolean CheckPaymentFlag = false;
				
				Map<String,Object> InfoCon = new HashMap<String,Object>();
				
				InfoCon.put("APPLY_UNIQUE_NBR", TranUniqueNbr);
				
				List OrgPaymentList = paymentCheckMapper.SELECT_TRAN_PAYMENT(InfoCon);
				Map<String,Object> OrgPaymentEntity = null;
				
				for(int idx = 0; idx < OrgPaymentList.size(); idx++){
					OrgPaymentEntity = (Map)OrgPaymentList.get(idx);
					
					PaymentType = String.valueOf(OrgPaymentEntity.get("PAYMENT_TYPE"));
					
					InfUniqueNbr = "";
					CheckCancelFlag = false;
					CheckPaymentFlag = false;
					
					if(String.valueOf(OrgPaymentEntity.get("REF_FLAG")).equals("N") && String.valueOf(OrgPaymentEntity.get("REF_STATUS")).equals("N")){
						if(ApproveCancelFlag && PaymentType.equals("000002")){
							CheckCancelFlag = true;
							CheckPaymentFlag = true;
						}
						else if(ApproveCancelFlag && PaymentType.equals("000003") && !String.valueOf(OrgPaymentEntity.get("RECEIPT_FLAG")).equals("Y")){
							CheckCancelFlag = true;
							CheckPaymentFlag = true;
						}
						else if(PointCancelFlag && PaymentType.equals("000005")){
							CheckCancelFlag = true;
							CheckPaymentFlag = true;
						}
						
						if(CheckCancelFlag){
							if(PaymentType.equals("000003") && !String.valueOf(OrgPaymentEntity.get("RECEIPT_FLAG")).equals("Y") && String.valueOf(OrgPaymentEntity.get("INF_UNIQUE_NBR2")).length() > 0){
								/*
								 * Cancel the Virtual Account
								 */
								
								InfUniqueNbr = CurrDate.substring(2, 8) + CompanyCd + SystemType + Utility.LPAD(getIndex(CompanyCd, "INF_UNIQUE_NBR", CurrDate.substring(0, 8), 1), 5, '0');
																
								CancelMap = new HashMap<String,Object>();
								
								CancelMap.put("INF_UNIQUE_NBR", InfUniqueNbr);												// INF_UNIQUE_NBR
								CancelMap.put("TRAN_DATE", "");																// TRAN_DATE
								CancelMap.put("TRAN_TIME", "");																// TRAN_TIME
								CancelMap.put("CANCEL_TYPE", "C");															// CANCEL_TYPE
								CancelMap.put("INF_UNIQUE_NBR2", String.valueOf(OrgPaymentEntity.get("INF_UNIQUE_NBR2")));	// ORG_INF_UNIQUE_NBR
								
								UpdateCount = paymentCheckMapper.INSERT_APPROVE_CANCEL(CancelMap);
								
								if(UpdateCount == 0){
									throw new C2SException("MR01", "FAILED_INSERT_APPROVE_CANCEL");
								}
								
								InfUniqueNbr = "";
								
								/*
								 * Delete the Tax Save Data
								 */
								CancelMap = new HashMap<String,Object>();
								
								CancelMap.put("INF_UNIQUE_NBR", "");														// INF_UNIQUE_NBR
								CancelMap.put("TAXSAVE_FLAG", "N");															// TAXSAVE_FLAG
								CancelMap.put("PROCESS_FLAG", "N");															// PROCESS_FLAG
								CancelMap.put("TRACK_DATA", "");															// TRACK_DATA
								CancelMap.put("INF_UNIQUE_NBR2", String.valueOf(OrgPaymentEntity.get("INF_UNIQUE_NBR2")));	// INF_UNIQUE_NBR2
								
								UpdateCount = paymentCheckMapper.UPDATE_TRAN_PAYMENT_PROCESS(CancelMap);
								
								if(UpdateCount == 0){
									throw new C2SException("MR02", "FAILED_UPDATE_TRAN_PAYMENT");
								}
							}
							else if(String.valueOf(OrgPaymentEntity.get("INF_UNIQUE_NBR")).length() > 0){
								/*
								 * Cancel the Approve Data
								 */
								
								InfUniqueNbr = CurrDate.substring(2, 8) + CompanyCd + SystemType + Utility.LPAD(getIndex(CompanyCd, "INF_UNIQUE_NBR", CurrDate.substring(0, 8), 1), 5, '0');
								
								CancelMap = new HashMap<String,Object>();
								
								CancelMap.put("INF_UNIQUE_NBR", InfUniqueNbr);												// INF_UNIQUE_NBR
								CancelMap.put("TRAN_DATE", "");																// TRAN_DATE
								CancelMap.put("TRAN_TIME", "");																// TRAN_TIME
								CancelMap.put("CANCEL_TYPE", "C");															// CANCEL_TYPE
								CancelMap.put("INF_UNIQUE_NBR2", String.valueOf(OrgPaymentEntity.get("INF_UNIQUE_NBR2")));	// ORG_INF_UNIQUE_NBR
								
								UpdateCount = paymentCheckMapper.INSERT_APPROVE_CANCEL(CancelMap);
								
								if(UpdateCount == 0){
									throw new C2SException("MR03", "FAILED_INSERT_APPROVE_CANCEL");
								}
							}
							
							RefStatusMap = new HashMap<String,Object>();
							
							RefStatusMap.put("REF_STATUS", "Y");															// REF_STATUS
							RefStatusMap.put("UPDATE_USER", WorkerMemberNo);												// UPDATE_USER
							RefStatusMap.put("UPDATE_DATE", CurrDate.substring(0, 8));										// UPDATE_DATE
							RefStatusMap.put("UPDATE_TIME", CurrDate.substring(8));											// UPDATE_TIME
							RefStatusMap.put("TRAN_DATE", String.valueOf(OrgPaymentEntity.get("TRAN_DATE")));				// TRAN_DATE
							RefStatusMap.put("TRAN_COMPANY_CD", String.valueOf(OrgPaymentEntity.get("TRAN_COMPANY_CD")));	// TRAN_COMPANY_CD
							RefStatusMap.put("TRAN_NBR", String.valueOf(OrgPaymentEntity.get("TRAN_NBR")));					// TRAN_NBR
							RefStatusMap.put("TRAN_PAY_NBR", String.valueOf(OrgPaymentEntity.get("TRAN_PAY_NBR")));			// TRAN_PAY_NBR
							
							UpdateCount = paymentCheckMapper.UPDATE_TRAN_PAYMENT_REF_STATUS(RefStatusMap);
							
							if(UpdateCount == 0){
								throw new C2SException("MR04", "FAILED_UPDATE_TRAN_PAYMENT_REF_STATUS");
							}
							
							/*
							 * Payment Data for Approve Cancel
							 */
							if(CheckPaymentFlag){
								TranPaymentMap = new HashMap<String,Object>();
								
								TranPaymentMap.put("TRAN_DATE", CurrDate.substring(0, 8));										// TRAN_DATE
								TranPaymentMap.put("TRAN_COMPANY_CD", CompanyCd);												// TRAN_COMPANY_CD
								TranPaymentMap.put("TRAN_NBR", TranNbr);														// TRAN_NBR
								TranPaymentMap.put("SYSTEM_TYPE", SystemType);													// SYSTEM_TYPE
								TranPaymentMap.put("SYSTEM_TRAN_TYPE", SystemTranType);											// SYSTEM_TRAN_TYPE
								TranPaymentMap.put("TRAN_UNIQUE_NBR", TranUniqueNbr);											// TRAN_UNIQUE_NBR
								TranPaymentMap.put("TRAN_DETAIL_NBR", TranDetailNbr);											// TRAN_DETAIL_NBR
								TranPaymentMap.put("TRAN_TIME", CurrDate.substring(8));											// TRAN_TIME
								TranPaymentMap.put("TRAN_TYPE", TranType);														// TRAN_TYPE
								TranPaymentMap.put("SALE_TYPE", SaleType);														// SALE_TYPE
								TranPaymentMap.put("WIN_TYPE", WinType);														// WIN_TYPE
								TranPaymentMap.put("PAYMENT_TYPE", PaymentType);												// PAYMENT_TYPE
								TranPaymentMap.put("INF_UNIQUE_NBR", InfUniqueNbr);												// INF_UNIQUE_NBR
								TranPaymentMap.put("INF_UNIQUE_NBR2", "");														// INF_UNIQUE_NBR2
								TranPaymentMap.put("REF_FLAG", RefFlag);														// REF_FLAG
								TranPaymentMap.put("RECEIPT_FLAG", String.valueOf(OrgPaymentEntity.get("RECEIPT_FLAG")));		// RECEIPT_FLAG
								TranPaymentMap.put("RECEIPT_DATE", CurrDate.substring(0, 8));									// RECEIPT_DATE
								TranPaymentMap.put("RECEIPT_TIME", CurrDate.substring(8));										// RECEIPT_TIME
								TranPaymentMap.put("DEPOSITOR", "");															// DEPOSITOR
								TranPaymentMap.put("PAYMENT_AMT", String.valueOf(OrgPaymentEntity.get("PAYMENT_AMT")));			// PAYMENT_AMT
								TranPaymentMap.put("VAT_AMT", String.valueOf(OrgPaymentEntity.get("VAT_AMT")));					// VAT_AMT
								TranPaymentMap.put("SVC_AMT", String.valueOf(OrgPaymentEntity.get("SVC_AMT")));					// SVC_AMT
								TranPaymentMap.put("BANK_CD", "");																// BANK_CD
								TranPaymentMap.put("ACCOUNT_NO", "");															// ACCOUNT_NO
								TranPaymentMap.put("LIMIT_RECEIPT_DATE", "");													// LIMIT_RECEIPT_DATE
								TranPaymentMap.put("TAXSAVE_FLAG", "N");														// TAXSAVE_FLAG
								TranPaymentMap.put("PROCESS_FLAG", "Y");														// PROCESS_FLAG
								TranPaymentMap.put("KEYIN_TYPE", "");															// KEYIN_TYPE
								TranPaymentMap.put("PAY_CERTIFY_TYPE", "");														// PAY_CERTIFY_TYPE
								TranPaymentMap.put("TRACK_DATA", "");															// TRACK_DATA
								TranPaymentMap.put("CMS_FLAG", "N");															// CMS_FLAG
								TranPaymentMap.put("CMS_AMT", "");																// CMS_AMT
								TranPaymentMap.put("CMS_COMMISSION_AMT", "");													// CMS_COMMISSION_AMT
								TranPaymentMap.put("REF_BANK_CD", "");															// REF_BANK_CD
								TranPaymentMap.put("REF_ACCOUNT_NO", "");														// REF_ACCOUNT_NO
								TranPaymentMap.put("REF_ACCOUNT_OWNER", "");													// REF_ACCOUNT_OWNER
								TranPaymentMap.put("REF_ACCOUNT_REG_NO", "");													// REF_ACCOUNT_REG_NO
								TranPaymentMap.put("REF_STATUS", "N");															// REF_STATUS
								TranPaymentMap.put("CONFIRM_FLAG", "N");														// CONFIRM_FLAG
								TranPaymentMap.put("USE_FLAG", "Y");															// USE_FLAG
								TranPaymentMap.put("INSERT_USER", WorkerMemberNo);												// INSERT_USER
								TranPaymentMap.put("INSERT_DATE", CurrDate.substring(0, 8));									// INSERT_DATE
								TranPaymentMap.put("INSERT_TIME", CurrDate.substring(8));										// INSERT_TIME
								
								UpdateCount = paymentCheckMapper.INSERT_TRAN_PAYMENT(TranPaymentMap);
								
								if(UpdateCount == 0){
									throw new C2SException("MR05", "FAILED_INSERT_TRAN_PAYMENT");
								}
							}
						}
					}
				}
			}
			
			/*
			 * Insert the Refund Payment
			 */
			if(RefundPaymentCnt > 0){
				long RefundPaymentAmt = 0;
				long RefundVatAmt = 0;
				long RefundSvcAmt = 0;
				long CmsCommissionAmt = 0;
				String CmsFlag = "";
				String CmsAmt = "";
				String RefBankCd = "";
				String RefAccountNo = "";
				String RefAccountOwner = "";
				String RefAccountRegNo = "";
				
				Entity RefundPaymentEntity = null;
				Map<String,Object> CmsEntity = null;
				
				for(int idx = 0; idx < RefundPaymentCnt; idx++){
					RefundPaymentEntity = RefundPaymentList.getFieldEntityNoCheck("REFUND_PAYMENT_ITEM", idx);
					
					PaymentType = RefundPaymentEntity.getFieldNoCheck("PAYMENT_TYPE");
					RefundPaymentAmt = Utility.StringToLong(String.valueOf(RefundPaymentEntity.getFieldNoCheck("PAYMENT_AMT")));
					RefundVatAmt = RefundPaymentAmt / 10;
					RefundVatAmt = 0;
					RefundSvcAmt = 0;
					CmsCommissionAmt = Utility.StringToLong(String.valueOf(RefundPaymentEntity.getFieldNoCheck("CMS_COMMISSION_AMT")));
					CmsFlag = RefundPaymentEntity.getFieldNoCheck("CMS_FLAG");
					RefBankCd = RefundPaymentEntity.getFieldNoCheck("REF_BANK_CD");
					RefAccountNo = RefundPaymentEntity.getFieldNoCheck("REF_ACCOUNT_NO");
					RefAccountOwner = RefundPaymentEntity.getFieldNoCheck("REF_ACCOUNT_OWNER");
					RefAccountRegNo = RefundPaymentEntity.getFieldNoCheck("REF_ACCOUNT_REG_NO");
					
					if(CmsFlag.equals("Y")){						
						
						InfUniqueNbr = CurrDate.substring(2, 8) + CompanyCd + SystemType + Utility.LPAD(getIndex(CompanyCd, "INF_UNIQUE_NBR", CurrDate.substring(0, 8), 1), 5, '0');								
						
						CmsAmt = Long.toString(RefundPaymentAmt);
						
						CmsEntity = new HashMap<String,Object>();
						
						CmsEntity.put("INF_UNIQUE_NBR", InfUniqueNbr);					// INF_UNIQUE_NBR
						CmsEntity.put("CMS_AMT", CmsAmt);								// CMS_AMT
						CmsEntity.put("CMS_COMMISSION_AMT", "");						// CMS_COMMISSION_AMT
						CmsEntity.put("REF_BANK_CD", RefBankCd);						// REF_BANK_CD
						CmsEntity.put("REF_ACCOUNT_NO", RefAccountNo);					// REF_ACCOUNT_NO
						CmsEntity.put("REF_ACCOUNT_OWNER", RefAccountOwner);			// REF_ACCOUNT_OWNER
						CmsEntity.put("REF_ACCOUNT_REG_NO", RefAccountRegNo);			// REF_ACCOUNT_REG_NO
						CmsEntity.put("CMS_STATUS", "R");								// CMS_STATUS
						CmsEntity.put("INSERT_USER", WorkerMemberNo);					// INSERT_USER
						CmsEntity.put("INSERT_DATE", CurrDate.substring(0, 8));			// INSERT_DATE
						CmsEntity.put("INSERT_TIME", CurrDate.substring(8));			// INSERT_TIME
						
						UpdateCount = paymentCheckMapper.INSERT_CMS(CmsEntity);
						
						if(UpdateCount == 0){
							throw new C2SException("MR06", "FAILED_INSERT_CMS");
						}
					}
					else{
						InfUniqueNbr = "";
						CmsFlag = "N";
						CmsAmt = "";
					}
					
					TranPaymentMap = new HashMap<String,Object>();
					
					TranPaymentMap.put("TRAN_DATE", CurrDate.substring(0, 8));												// TRAN_DATE
					TranPaymentMap.put("TRAN_COMPANY_CD", CompanyCd);														// TRAN_COMPANY_CD
					TranPaymentMap.put("TRAN_NBR", TranNbr);																// TRAN_NBR
					TranPaymentMap.put("SYSTEM_TYPE", SystemType);															// SYSTEM_TYPE
					TranPaymentMap.put("SYSTEM_TRAN_TYPE", SystemTranType);													// SYSTEM_TRAN_TYPE
					TranPaymentMap.put("TRAN_UNIQUE_NBR", TranUniqueNbr);													// TRAN_UNIQUE_NBR
					TranPaymentMap.put("TRAN_DETAIL_NBR", TranDetailNbr);													// TRAN_DETAIL_NBR
					TranPaymentMap.put("TRAN_TIME", CurrDate.substring(8));													// TRAN_TIME
					TranPaymentMap.put("TRAN_TYPE", TranType);																// TRAN_TYPE
					TranPaymentMap.put("SALE_TYPE", SaleType);																// SALE_TYPE
					TranPaymentMap.put("WIN_TYPE", WinType);																// WIN_TYPE
					TranPaymentMap.put("PAYMENT_TYPE", PaymentType);														// PAYMENT_TYPE
					TranPaymentMap.put("INF_UNIQUE_NBR", "");																// INF_UNIQUE_NBR
					TranPaymentMap.put("INF_UNIQUE_NBR2", InfUniqueNbr);													// INF_UNIQUE_NBR2
					TranPaymentMap.put("REF_FLAG", RefFlag);																// REF_FLAG
					TranPaymentMap.put("RECEIPT_FLAG", "Y");																// RECEIPT_FLAG
					TranPaymentMap.put("RECEIPT_DATE", CurrDate.substring(0, 8));											// RECEIPT_DATE
					TranPaymentMap.put("RECEIPT_TIME", CurrDate.substring(8));												// RECEIPT_TIME
					TranPaymentMap.put("DEPOSITOR", "");																	// DEPOSITOR
					TranPaymentMap.put("PAYMENT_AMT", Long.toString(RefundPaymentAmt));										// PAYMENT_AMT
					TranPaymentMap.put("VAT_AMT", Long.toString(RefundVatAmt));												// VAT_AMT
					TranPaymentMap.put("SVC_AMT", Long.toString(RefundSvcAmt));												// SVC_AMT
					TranPaymentMap.put("BANK_CD", "");																		// BANK_CD
					TranPaymentMap.put("ACCOUNT_NO", "");																	// ACCOUNT_NO
					TranPaymentMap.put("LIMIT_RECEIPT_DATE", "");															// LIMIT_RECEIPT_DATE
					TranPaymentMap.put("TAXSAVE_FLAG", "N");																// TAXSAVE_FLAG
					TranPaymentMap.put("PROCESS_FLAG", "N");																// PROCESS_FLAG
					TranPaymentMap.put("KEYIN_TYPE", "");																	// KEYIN_TYPE
					TranPaymentMap.put("PAY_CERTIFY_TYPE", "");																// PAY_CERTIFY_TYPE
					TranPaymentMap.put("TRACK_DATA", "");																	// TRACK_DATA
					TranPaymentMap.put("CMS_FLAG", CmsFlag);																// CMS_FLAG
					TranPaymentMap.put("CMS_AMT", CmsAmt);																	// CMS_AMT
					TranPaymentMap.put("CMS_COMMISSION_AMT", CmsCommissionAmt);															// CMS_COMMISSION_AMT
					TranPaymentMap.put("REF_BANK_CD", RefBankCd);															// REF_BANK_CD
					TranPaymentMap.put("REF_ACCOUNT_NO", EncManager.getEncData("TA_TRAN_PAYMENT", "REF_ACCOUNT_NO", RefAccountNo));			// REF_ACCOUNT_NO
					TranPaymentMap.put("REF_ACCOUNT_OWNER", RefAccountOwner);												// REF_ACCOUNT_OWNER
					TranPaymentMap.put("REF_ACCOUNT_REG_NO", "");															// REF_ACCOUNT_REG_NO
					TranPaymentMap.put("REF_STATUS", "N");																	// REF_STATUS
					TranPaymentMap.put("CONFIRM_FLAG", "N");																// CONFIRM_FLAG
					TranPaymentMap.put("USE_FLAG", "Y");																	// USE_FLAG
					TranPaymentMap.put("INSERT_USER", WorkerMemberNo);														// INSERT_USER
					TranPaymentMap.put("INSERT_DATE", CurrDate.substring(0, 8));											// INSERT_DATE
					TranPaymentMap.put("INSERT_TIME", CurrDate.substring(8));												// INSERT_TIME
					
					UpdateCount = paymentCheckMapper.INSERT_TRAN_PAYMENT(TranPaymentMap);
					
					if(UpdateCount == 0){
						throw new C2SException("MR07", "FAILED_INSERT_TRAN_PAYMENT");
					}
				}
			}
			
			/*
			 * Payment (Card, Point)
			 */
			int PaymentCnt = PaymentList.getFieldEntitySize("PAYMENT_ITEM");
			EntityList ReverseList = new EntityList();
			Entity ReverseEntity = null;
			
			if(PaymentCnt > 0){
				TranType = "11";
				RefFlag = "N";
				
				String MemberNo = String.valueOf(RefundInfo.get("MEMBER_NO"));
				String CustomerName = String.valueOf(RefundInfo.get("CUSTOMER_NAME"));
				String CustomerTel = String.valueOf(RefundInfo.get("CUSTOMER_TEL"));
				String CustomerEmail = String.valueOf(RefundInfo.get("CUSTOMER_EMAIL"));
				String ProductName = String.valueOf(RefundInfo.get("PRODUCT_NAME"));
				
				long PaymentAmt = 0;
				long VatAmt = 0;
				long SvcAmt = 0;
			
				Map<String,Object> ReverseMap = null;
				
				boolean ApproveResult = true;
				String ApproveErrCode = "";
				String ApproveErrMessage = "";
				
				Entity PaymentEntity = null;
				
				for(int idx = 0; idx < PaymentCnt; idx++){
					PaymentEntity = PaymentList.getFieldEntityNoCheck("PAYMENT_ITEM", idx);
					
					PaymentType = PaymentEntity.getFieldNoCheck("PAYMENT_TYPE");
					PaymentAmt = Utility.StringToLong(String.valueOf(PaymentEntity.getFieldNoCheck("PAYMENT_AMT")));
					VatAmt = PaymentAmt / 10;
					VatAmt = 0;
					SvcAmt = 0;
					
					if(PaymentAmt > 0){
						if(PaymentType.equals("000002") || PaymentType.equals("000005")){
							PaymentEntity.setField("MEMBER_NO", MemberNo);
							PaymentEntity.setField("SVC_AMT", Long.toString(SvcAmt));
							PaymentEntity.setField("VAT_AMT", Long.toString(VatAmt));
							PaymentEntity.setField("CUSTOMER_NAME", CustomerName);
							PaymentEntity.setField("CUSTOMER_TEL", CustomerTel);
							PaymentEntity.setField("CUSTOMER_EMAIL", CustomerEmail);
							PaymentEntity.setField("PRODUCT_NAME", ProductName);
							
							InfUniqueNbr = CurrDate.substring(2, 8) + CompanyCd + SystemType + Utility.LPAD(getIndex(CompanyCd, "INF_UNIQUE_NBR", CurrDate.substring(0, 8), 1), 5, '0');	
							
							if(PaymentType.equals("000002")){		// 신용카드
								InfUniqueNbr = CurrDate.substring(2, 8) + CompanyCd + SystemType + Utility.LPAD(getIndex(CompanyCd, "INF_UNIQUE_NBR", CurrDate.substring(0, 8), 1), 5, '0');			
								ApproveResult = payRefundCheckService.Approve(CompanyCd, SystemType, ServiceType, SaleType, WinType, InfUniqueNbr, PaymentEntity, paraMap);
							}
							
							if(ApproveResult){
								ReverseEntity = new Entity(2);
								
								ReverseEntity.setField("INF_UNIQUE_NBR", InfUniqueNbr);
								ReverseEntity.setField("CANCEL_TYPE", "C");
								
								ReverseList.add(ReverseEntity);
							}
							else{
								ApproveErrCode = getErrCode();
								ApproveErrMessage = getErrMessage();
								break;
							}
							
							TranPaymentMap = new HashMap<String,Object>();
							
							TranPaymentMap.put("TRAN_DATE", CurrDate.substring(0, 8));		// TRAN_DATE
							TranPaymentMap.put("TRAN_COMPANY_CD", CompanyCd);				// TRAN_COMPANY_CD
							TranPaymentMap.put("TRAN_NBR", TranNbr);						// TRAN_NBR
							TranPaymentMap.put("SYSTEM_TRAN_TYPE", SystemTranType);			// SYSTEM_TRAN_TYPE
							TranPaymentMap.put("TRAN_UNIQUE_NBR", TranUniqueNbr);			// TRAN_UNIQUE_NBR
							TranPaymentMap.put("TRAN_DETAIL_NBR", TranDetailNbr);			// TRAN_DETAIL_NBR
							TranPaymentMap.put("TRAN_TIME", CurrDate.substring(8));			// TRAN_TIME
							TranPaymentMap.put("TRAN_TYPE", TranType);						// TRAN_TYPE
							TranPaymentMap.put("PAYMENT_TYPE", PaymentType);				// PAYMENT_TYPE
							TranPaymentMap.put("INF_UNIQUE_NBR", InfUniqueNbr);				// INF_UNIQUE_NBR
							TranPaymentMap.put("INF_UNIQUE_NBR2", "");						// INF_UNIQUE_NBR2
							TranPaymentMap.put("REF_FLAG", RefFlag);						// REF_FLAG
							TranPaymentMap.put("RECEIPT_FLAG", "Y");						// RECEIPT_FLAG
							TranPaymentMap.put("RECEIPT_DATE", CurrDate.substring(0, 8));	// RECEIPT_DATE
							TranPaymentMap.put("RECEIPT_TIME", CurrDate.substring(8));		// RECEIPT_TIME
							TranPaymentMap.put("PAYMENT_AMT", Long.toString(PaymentAmt));	// PAYMENT_AMT
							TranPaymentMap.put("VAT_AMT", Long.toString(VatAmt));			// VAT_AMT
							TranPaymentMap.put("SVC_AMT", Long.toString(SvcAmt));			// SVC_AMT
							TranPaymentMap.put("TAXSAVE_FLAG", "N");						// TAXSAVE_FLAG
							TranPaymentMap.put("PROCESS_FLAG", "Y");						// PROCESS_FLAG
							TranPaymentMap.put("KEYIN_TYPE", "");							// KEYIN_TYPE
							TranPaymentMap.put("PAY_CERTIFY_TYPE", "");						// PAY_CERTIFY_TYPE
							TranPaymentMap.put("TRACK_DATA", "");							// TRACK_DATA
							TranPaymentMap.put("REF_STATUS", "N");							// REF_STATUS
							TranPaymentMap.put("USE_FLAG", "Y");							// USE_FLAG
							TranPaymentMap.put("CONFIRM_FLAG", "N");						// CONFIRM_FLAG
							TranPaymentMap.put("INSERT_USER", WorkerMemberNo);				// INSERT_USER
							TranPaymentMap.put("INSERT_DATE", CurrDate.substring(0, 8));	// INSERT_DATE
							TranPaymentMap.put("INSERT_TIME", CurrDate.substring(8));		// INSERT_TIME
							TranPaymentMap.put("INF_UNIQUE_NBR", InfUniqueNbr);				// INF_UNIQUE_NBR
							
							UpdateCount = paymentCheckMapper.INSERT_TRAN_PAYMENT_APPROVE(TranPaymentMap);
							
							if(UpdateCount == 0){
								ApproveResult = false;
								break;
							}
						}
						else{
							ApproveResult = false;
							ApproveErrCode = "MR08";
							ApproveErrMessage = "WRONG_PAYMENT_TYPE";
							
							break;
						}
					}
				}
				
				if(!ApproveResult){
					payRefundCheckService.ReverseApprove(CompanyCd, SystemType, ReverseList);				
					throw new C2SException(ApproveErrCode, ApproveErrMessage);
				}
			}
			
			result = true;
		}catch(C2SException ce){
			this.ErrCode = ce.getExceptCode();
			this.ErrMessage = ce.getMessage();
			result = false;
		}catch(Exception e){
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			result = false;
		}

		return result;
	}
	
	/**
	 * Set Course Reser
	 * @param CompanyCd
	 * @param CourseCd
	 * @param PriceNbr
	 * @param ExistFlag
	 * @param CoercionFlag
	 * @param ReserFlag
	 * @param ApplyCnt
	 * @return
	 * @throws Exception
	 */
	public boolean SetReser(String CompanyCd, String CourseCd, String PriceNbr, boolean ExistFlag, boolean CoercionFlag, boolean ReserFlag, int ApplyCnt) throws Exception {
		boolean SetResult = false;
		String RspCode = "";
		String RspMsg = "";
		this.ErrCode = "";
		this.ErrMessage = "";
		Map<String,Object> params = new HashMap<String,Object>();
		
		try {
			
			params.put("COMPANY_CD", CompanyCd);
			params.put("COURSE_CD", CourseCd);
			params.put("PRICE_NBR", PriceNbr);
			params.put("EXIST_FLAG", ExistFlag?"Y":"N");
			params.put("COERCION_FLAG", CoercionFlag?"Y":"N");
			params.put("RESER_FLAG", ReserFlag?"Y":"N");
			params.put("APPLY_CNT", ApplyCnt);
			
			logger.info("SetReser -- " + params.toString());
			paymentCheckMapper.SET_COURSE_RESER(params);
			
			RspCode = String.valueOf(params.get("RSP_CODE"));
			RspMsg = Utility.CheckNull((String)params.get("RSP_MSG"));
		}catch(Exception e){
			RspCode = "E999";
			RspMsg = e.getMessage();
		}
		
		if(RspCode.equals("0000")){
			SetResult = true;
			RspMsg = "";
		}
		else{
			SetResult = false;
			this.ErrCode = RspCode;
			this.ErrMessage = RspMsg;
		}
		
		return SetResult;
	}
	
	/**
	 * Set Course Wait
	 * @param CompanyCd
	 * @param CourseCd
	 * @param PriceNbr
	 * @param ExistFlag
	 * @param CoercionFlag
	 * @param WaitFlag
	 * @param ApplyCnt
	 * @return
	 * @throws Exception
	 */
	public boolean SetWait(String CompanyCd, String CourseCd, String PriceNbr, boolean ExistFlag, boolean CoercionFlag, boolean WaitFlag, int ApplyCnt) throws Exception {
		boolean SetResult = false;
		String RspCode = "";
		String RspMsg = "";
		this.ErrCode = "";
		this.ErrMessage = "";
		Map<String,Object> params = new HashMap<String,Object>();
		
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try {
			params.put("COMPANY_CD", CompanyCd);
			params.put("COURSE_CD", CourseCd);
			params.put("PRICE_NBR", PriceNbr);
			params.put("EXIST_FLAG", ExistFlag?"Y":"N");
			params.put("COERCION_FLAG", CoercionFlag?"Y":"N");
			params.put("WAIT_FLAG", WaitFlag?"Y":"N");
			params.put("APPLY_CNT", ApplyCnt);
			
			paymentCheckMapper.SET_COURSE_WAIT(params);
			
			RspCode = String.valueOf(params.get("RSP_CODE"));
			RspMsg = Utility.CheckNull((String)params.get("RSP_MSG"));
		}catch(Exception e){
			RspCode = "E999";
			RspMsg = e.getMessage();
		}
		
		if(RspCode.equals("0000")){
			SetResult = true;
			RspMsg = "";
		}
		else{
			SetResult = false;
			this.ErrCode = RspCode;
			this.ErrMessage = RspMsg;
		}
		
		return SetResult;
	}

	/**
	 * 결제 완료 후 저장을 위해 예약된 내역을 가져옴
	 * @param string
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Map get_TL_COURSE_APPLY(String string) {
		logger.info("PaymentCheckService.get_TL_COURSE_APPLY -- !" + string);
		return paymentCheckMapper.get_TL_COURSE_APPLY(string);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Map get_SELECT_COURSE_CHECK(Map map) throws Exception{
		return (Map)(paymentCheckMapper.SELECT_COURSE_CHECK(map).get(0));
	}
}
