package kr.go.sciencecenter.service;

import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.go.sciencecenter.model.MainPageBanner;
import kr.go.sciencecenter.persistence.MainPageMapper;
import kr.go.sciencecenter.util.FileUploadHelper;

@Service
public class MainPageService {
	
	@Autowired
	private MainPageMapper mainPageMapper;
	
	@Autowired
	private FileUploadHelper fileUploadHelper;

	public List<MainPageBanner> listBannersByType(String type, Boolean visibleOnly) {
		return mainPageMapper.listBannersByType(type, visibleOnly);
	}
	
	public void createBanner(MainPageBanner mainPageBanner) {
		mainPageBanner.setVisible(true);
		
		int maxTypeSeq = mainPageMapper.getTypeMaxSeq(mainPageBanner.getBannerType());
		mainPageBanner.setTypeSeq(maxTypeSeq + 1);
		mainPageMapper.createBanner(mainPageBanner);
		
		if(mainPageBanner.getBannerImageFile() != null && mainPageBanner.getBannerImageFile().getSize() > 0){
			String urlPath = MainPageBanner.MAIN_BANNER_IMAGE_PREFIX + "/" + mainPageBanner.getId() + "/";
			String fileName = mainPageBanner.getId()
					+ "." 
					+ FilenameUtils.getExtension(mainPageBanner.getBannerImageFile().getOriginalFilename());
			
			if (fileUploadHelper.upload(mainPageBanner.getBannerImageFile(), urlPath, fileName)) {
				urlPath += fileName;
				mainPageBanner.setBannerImageUrl(urlPath);
				mainPageMapper.updateBanner(mainPageBanner);
			}
		}
	}
	
	public void delete(MainPageBanner mainPageBanner) {
		String urlPath = MainPageBanner.MAIN_BANNER_IMAGE_PREFIX + "/" + mainPageBanner.getId() + "/";
		fileUploadHelper.multiDelete(urlPath);
		mainPageMapper.deleteBanner(mainPageBanner.getId());
	}	

	public void createBanner(MainPageBanner mainPageBanner, String contextPath, String baseFilePath) {
		
		setAllNewLetterBannersHidden(mainPageBanner);
		
		int maxTypeSeq = mainPageMapper.getTypeMaxSeq(mainPageBanner.getBannerType());
		mainPageBanner.setTypeSeq(maxTypeSeq + 1);
		
		mainPageMapper.createBanner(mainPageBanner);
		
		if(mainPageBanner.getBannerImageFile() != null && mainPageBanner.getBannerImageFile().getSize() > 0 ) {
			mainPageBanner.saveBannerImage(contextPath, baseFilePath);
			mainPageMapper.updateBanner(mainPageBanner);
		}
		
	}

	public MainPageBanner findBannerById(int id) {
		return mainPageMapper.findBannerById(id);
	}

	public void updateBanner(MainPageBanner mainPageBanner, String contextPath, String baseFilePath) {
		setAllNewLetterBannersHidden(mainPageBanner);
		
		if(mainPageBanner.getBannerImageFile() != null && mainPageBanner.getBannerImageFile().getSize() > 0 ) {
			mainPageBanner.saveBannerImage(contextPath, baseFilePath);
		}
		mainPageMapper.updateBanner(mainPageBanner);
	}

	public void deleteBanner(MainPageBanner mainPageBanner, String baseFilePath) {
		
		mainPageBanner.deleteBannerImage(baseFilePath);
		mainPageMapper.deleteBanner(mainPageBanner.getId());
		
	}
	
	private void setAllNewLetterBannersHidden(MainPageBanner mainPageBanner) {
		// 뉴스레터 배너는 하나만 보여지게 되므로,
		// 등록또는 수정때 visible true 가 설정되도록 하면, 나머지를 전부 숨김으로 바꿔놓는다.
		if( mainPageBanner.getBannerType().equals("newsLetterBanner")
				&& mainPageBanner.isVisible() == true ) {
			
			mainPageMapper.setAllNewLetterBannersHidden();
		}
	}
}
