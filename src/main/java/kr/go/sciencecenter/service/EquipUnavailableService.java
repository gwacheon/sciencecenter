package kr.go.sciencecenter.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.go.sciencecenter.model.EquipUnavailable;
import kr.go.sciencecenter.persistence.EquipUnavailableMapper;

@Service
public class EquipUnavailableService {
    @Autowired
    EquipUnavailableMapper equipUnavailableMapper;

    public void bulkInsert(List<EquipUnavailable> equipUnavailable) {

	equipUnavailableMapper.bulkInsert(equipUnavailable);
    }

    public List<EquipUnavailable> findByIdAndSerial(int equipmentId, int equiSerialId) {

	return equipUnavailableMapper.findByIdAndSerial(equipmentId, equiSerialId);
    }
}
