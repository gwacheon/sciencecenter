package kr.go.sciencecenter.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.go.sciencecenter.model.EquipReserve;
import kr.go.sciencecenter.persistence.EquipReserveMapper;

@Service
public class EquipReserveService {
	@Autowired
	EquipReserveMapper equipReserveMapper;
	
	public void create(EquipReserve equipReserve){
		equipReserveMapper.create(equipReserve);
	}

	public List<EquipReserve> list(int equipmentId, String dateStr) {
		return equipReserveMapper.list(equipmentId, dateStr);
	}

	public void bulkInsert(List<EquipReserve> equipReserves) {
		equipReserveMapper.bulkInsert(equipReserves);
	}

	public List<EquipReserve> getReservationsInMonth(int equipmentId, String beginDate, String endDate) {
		return equipReserveMapper.getReservationsInMonth(equipmentId, beginDate, endDate);
	}

	public List<EquipReserve> getReservationsPerMonth(int equiSerialId, String beginDate, String endDate) {
	    return equipReserveMapper.getReservationsPerMonth(equiSerialId, beginDate, endDate);
	}

	public List<EquipReserve>gerReserves(String memberNo) {
		
		return equipReserveMapper.getReserves(memberNo);
	}

	public void changeStatus(int id, String status) {
		equipReserveMapper.changeStatus(id,status);
		
	}
}
