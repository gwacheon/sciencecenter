package kr.go.sciencecenter.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kr.co.mainticket.Entity.Entity;
import kr.co.mainticket.Entity.EntityList;
import kr.co.mainticket.Exception.C2SException;
import kr.co.mainticket.Science.Interlock.EncManager;
import kr.co.mainticket.Utility.DateTime;
import kr.co.mainticket.Utility.Utility;
import kr.go.sciencecenter.persistence.PaymentCheckMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PayRefundCheckService {
	
	@Autowired
	PaymentCheckMapper paymentCheckMapper;
	
	@Autowired
	PaymentCheckService paymentCheckService;
	
	private String ErrCode;
	private String ErrMessage;
		
	public String getErrCode(){
		return Utility.CheckNull(this.ErrCode);
	}
	
	public String getErrMessage(){
		return Utility.CheckNull(this.ErrMessage).replaceAll("\n", "\\\\n").replaceAll("\"", "'");
	}
	
	/**
	 * Check the Payment and Refund Data
	 * @param conn
	 * @param CompanyCd
	 * @param SystemType
	 * @param ServiceType
	 * @param SaleType
	 * @param DataEntity
	 * @param DataCheckFlag
	 * @return
	 * @throws Exception
	 */
	public boolean CheckPayment(String CompanyCd, String SystemType, String ServiceType, String SaleType, Entity DataEntity, boolean DataCheckFlag) throws Exception{
		this.ErrCode = "";
		this.ErrMessage = "";
		
		String PaymentType = "";
		Map<String,Object> CheckCon = null;
		List CheckList = null;
		
		/*
		 * Check the Payment
		 */
		Entity PaymentList = DataEntity.getFieldEntityNoCheck("PAYMENT_LIST");
		int PaymentCnt = PaymentList.getFieldEntitySize("PAYMENT_ITEM");
		Entity PaymentEntity = null;
		
		for(int idx = 0; idx < PaymentCnt; idx++){
			PaymentEntity = PaymentList.getFieldEntityNoCheck("PAYMENT_ITEM", idx);
			
			PaymentType = PaymentEntity.getFieldNoCheck("PAYMENT_TYPE");
			
			CheckCon = new HashMap<String,Object>();
			
			CheckCon.put("COMPANY_CD", CompanyCd);
			CheckCon.put("SYSTEM_TYPE", SystemType);
			CheckCon.put("SERVICE_TYPE", ServiceType);
			CheckCon.put("SALE_TYPE", SaleType);
			CheckCon.put("PAYMENT_TYPE", PaymentType);
			System.out.println("Check :"+CheckCon.toString());
			CheckList = paymentCheckMapper.SELECT_COMPANY_PAYMENT(CheckCon);
			
			if(CheckList.size() <= 0){
				throw new C2SException("CP01", "NOT_EXIST_COMPANY_PAYMENT");
			}
		}
		
		return true;
	}
	
	/**
	 * Payment
	 * @param conn
	 * @param CompanyCd
	 * @param SystemType
	 * @param ServiceType
	 * @param TranInfo
	 * @param PaymentList
	 * @return
	 * @throws Exception
	 */
	public boolean Payment( String CompanyCd, String SystemType, String ServiceType, Entity TranInfo, Entity PaymentList, Map<String,Object> paraMap) throws Exception{
		boolean result = false;
		
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try{
			if(ServiceType.length() == 0){
				ServiceType = "XXXXXX";
			}
			
			String CurrDate = DateTime.getFormatDateString("yyyyMMddHHmmss", 0);
			String TranNbr = Utility.LPAD(paymentCheckService.getIndex(CompanyCd, "TRAN_NBR", CurrDate.substring(0, 8), 1), 6, '0');
			
			String MemberNo = TranInfo.getFieldNoCheck("MEMBER_NO");
			String CustomerName = TranInfo.getFieldNoCheck("CUSTOMER_NAME");
			String CustomerTel = TranInfo.getFieldNoCheck("CUSTOMER_TEL");
			String CustomerEmail = TranInfo.getFieldNoCheck("CUSTOMER_EMAIL");
			String ProductName = TranInfo.getFieldNoCheck("PRODUCT_NAME");
			String SystemTranType = TranInfo.getFieldNoCheck("SYSTEM_TRAN_TYPE");
			String TranUniqueNbr = TranInfo.getFieldNoCheck("TRAN_UNIQUE_NBR");
			String TranDetailNbr = TranInfo.getFieldNoCheck("TRAN_DETAIL_NBR");
			String TranType = TranInfo.getFieldNoCheck("TRAN_TYPE");
			String SaleType = TranInfo.getFieldNoCheck("SALE_TYPE");
			String WinType = TranInfo.getFieldNoCheck("WIN_TYPE");
			String RefFlag = TranInfo.getFieldNoCheck("REF_FLAG");
			String WorkerMemberNo = TranInfo.getFieldNoCheck("WORKER_MEMBER_NO");
			
			if(TranType.length() == 0){
				TranType = "11";
			}
			
			if(!RefFlag.equals("Y")){
				RefFlag = "N";
			}
			
			int PaymentCnt = PaymentList.getFieldEntitySize("PAYMENT_ITEM");
			Entity PaymentEntity = null;
			
			EntityList ReverseList = new EntityList();
			Entity ReverseEntity = null;
			
			String PaymentType = "";
			String PaySvcType = "";
			long PaymentAmt = 0;
			long VatAmt = 0;
			long SvcAmt = 0;
			
			String InfUniqueNbr = "";
			String InfUniqueNbr2 = "";
			String TranInfUniqueNbr = "";
			boolean ApproveResult = true;
			String ApproveErrCode = "";
			String ApproveErrMessage = "";
			
			String ReceiptFlag = "";
			String ReceiptDate = "";
			String ReceiptTime = "";
			String TaxsaveFlag = "";
			String ProcessFlag = "";
			String VaKeyinType = "";
			String VaTrackData = "";
			String VaPayCertifyType = "";
			String RefAccountNo = "";
			String RefAccountRegNo = "";
			String LimtReceiptDate = "";
			
			
			int UpdateCount = 0;
			
			for(int idx = 0; idx < PaymentCnt; idx++){
				PaymentEntity = PaymentList.getFieldEntityNoCheck("PAYMENT_ITEM", idx);
				
				PaymentType = PaymentEntity.getFieldNoCheck("PAYMENT_TYPE");
				PaySvcType = PaymentEntity.getFieldNoCheck("PAY_SVC_TYPE");
				PaymentAmt = Utility.StringToLong(PaymentEntity.getFieldNoCheck("PAYMENT_AMT"));
				VatAmt = PaymentAmt / 10;
				VatAmt = 0;
				SvcAmt = 0;
				Map<String,Object> TranPaymentMap = null;
				
				if(PaymentAmt > 0){
					InfUniqueNbr = "";
					InfUniqueNbr2 = "";
					TranInfUniqueNbr = "";
					
					ReceiptFlag = "Y";
					ReceiptDate = CurrDate.substring(0, 8);
					ReceiptTime = CurrDate.substring(8);
					TaxsaveFlag = "N";
					ProcessFlag = "N";
					VaKeyinType = "";
					VaTrackData = "";
					VaPayCertifyType = "";
					RefAccountNo = "";
					RefAccountRegNo = "";
					LimtReceiptDate = "";
					
					PaymentEntity.setField("MEMBER_NO", MemberNo);
					PaymentEntity.setField("CUSTOMER_NAME", CustomerName);
					PaymentEntity.setField("CUSTOMER_TEL", CustomerTel);
					PaymentEntity.setField("CUSTOMER_EMAIL", CustomerEmail);
					PaymentEntity.setField("PRODUCT_NAME", ProductName);
					
					if(PaymentType.equals("000002") || PaymentType.equals("000003")){
						
						if(PaymentType.equals("000002")){		// 신용카드
							InfUniqueNbr = CurrDate.substring(2, 8) + CompanyCd + SystemType + Utility.LPAD(paymentCheckService.getIndex(CompanyCd, "INF_UNIQUE_NBR", CurrDate.substring(0, 8), 1), 5, '0');
							TranInfUniqueNbr = InfUniqueNbr;
							
							ApproveResult = Approve(CompanyCd, SystemType, ServiceType, SaleType, WinType, InfUniqueNbr, PaymentEntity, paraMap);
						}
						else if(PaymentType.equals("000003")){		// 가상계좌, 계좌이체
							InfUniqueNbr2 = CurrDate.substring(2, 8) + CompanyCd + SystemType + Utility.LPAD(paymentCheckService.getIndex(CompanyCd, "INF_UNIQUE_NBR", CurrDate.substring(0, 8), 1), 5, '0');
							TranInfUniqueNbr = InfUniqueNbr2;
							
							if(PaymentType.equals("000003")){
								PaymentEntity.setField("PAY_SVC_TYPE", "VAT");
							}
							
							ApproveResult = Approve(CompanyCd, SystemType, ServiceType, SaleType, WinType, InfUniqueNbr2, PaymentEntity, paraMap);
							
							PaymentEntity.setField("PAY_SVC_TYPE", PaySvcType);
						}
						
						if(ApproveResult){
							if(PaymentType.equals("000002") || PaymentType.equals("000005")){
								ReverseEntity = new Entity(2);
								
								ReverseEntity.setField("INF_UNIQUE_NBR", InfUniqueNbr);
								ReverseEntity.setField("CANCEL_TYPE", "C");
								
								ReverseList.add(ReverseEntity);
							}
							else{
								ReverseEntity = new Entity(2);
								
								ReverseEntity.setField("INF_UNIQUE_NBR", InfUniqueNbr2);
								ReverseEntity.setField("CANCEL_TYPE", "C");
								
								ReverseList.add(ReverseEntity);
							}
						}
						else{
							ApproveErrCode = getErrCode();
							ApproveErrMessage = getErrMessage();
							
							break;
						}
												
						if(PaymentType.equals("000003")){
							ReceiptFlag = "N";
							ReceiptDate = "";
							ReceiptTime = "";
							
							if(PaySvcType.equals("001")){
								TaxsaveFlag = "Y";
								VaKeyinType = PaymentEntity.getFieldNoCheck("KEYIN_TYPE");
								VaTrackData = "37" + PaymentEntity.getFieldNoCheck("TRACK_DATA");
								VaPayCertifyType = PaymentEntity.getFieldNoCheck("CERTIFY_TYPE");
								
								if(VaKeyinType.equals("K")){
									VaTrackData += "=0000";
								}
							}
						}
						
						TranPaymentMap = new HashMap<String,Object>();
						
						TranPaymentMap.put("TRAN_DATE", CurrDate.substring(0, 8));		// TRAN_DATE
						TranPaymentMap.put("TRAN_COMPANY_CD", CompanyCd);				// TRAN_COMPANY_CD
						TranPaymentMap.put("TRAN_NBR", TranNbr);						// TRAN_NBR
						TranPaymentMap.put("SYSTEM_TRAN_TYPE", SystemTranType);			// SYSTEM_TRAN_TYPE
						TranPaymentMap.put("TRAN_UNIQUE_NBR", TranUniqueNbr);			// TRAN_UNIQUE_NBR
						TranPaymentMap.put("TRAN_DETAIL_NBR", TranDetailNbr);			// TRAN_DETAIL_NBR
						TranPaymentMap.put("TRAN_TIME", CurrDate.substring(8));			// TRAN_TIME
						TranPaymentMap.put("TRAN_TYPE", TranType);						// TRAN_TYPE
						TranPaymentMap.put("PAYMENT_TYPE", PaymentType);				// PAYMENT_TYPE
						TranPaymentMap.put("INF_UNIQUE_NBR", InfUniqueNbr);				// INF_UNIQUE_NBR
						TranPaymentMap.put("INF_UNIQUE_NBR2", InfUniqueNbr2);			// INF_UNIQUE_NBR2
						TranPaymentMap.put("REF_FLAG", RefFlag);						// REF_FLAG
						TranPaymentMap.put("RECEIPT_FLAG", ReceiptFlag);				// RECEIPT_FLAG
						TranPaymentMap.put("RECEIPT_DATE", ReceiptDate);				// RECEIPT_DATE
						TranPaymentMap.put("RECEIPT_TIME", ReceiptTime);				// RECEIPT_TIME
						TranPaymentMap.put("PAYMENT_AMT", Long.toString(PaymentAmt));	// PAYMENT_AMT
						TranPaymentMap.put("VAT_AMT", Long.toString(VatAmt));			// VAT_AMT
						TranPaymentMap.put("SVC_AMT", Long.toString(SvcAmt));			// SVC_AMT
						TranPaymentMap.put("TAXSAVE_FLAG", TaxsaveFlag);				// TAXSAVE_FLAG
						TranPaymentMap.put("PROCESS_FLAG", ProcessFlag);				// PROCESS_FLAG
						TranPaymentMap.put("KEYIN_TYPE", VaKeyinType);					// KEYIN_TYPE
						TranPaymentMap.put("PAY_CERTIFY_TYPE", VaPayCertifyType);		// PAY_CERTIFY_TYPE
						TranPaymentMap.put("TRACK_DATA", VaTrackData);					// TRACK_DATA
						TranPaymentMap.put("REF_STATUS", "N");							// REF_STATUS
						TranPaymentMap.put("USE_FLAG", "Y");							// USE_FLAG
						TranPaymentMap.put("CONFIRM_FLAG", "N");						// CONFIRM_FLAG
						TranPaymentMap.put("INSERT_USER", WorkerMemberNo);				// INSERT_USER
						TranPaymentMap.put("INSERT_DATE", CurrDate.substring(0, 8));	// INSERT_DATE
						TranPaymentMap.put("INSERT_TIME", CurrDate.substring(8));		// INSERT_TIME
						TranPaymentMap.put("TRAN_INF_UNIQUE_NBR", TranInfUniqueNbr);	// INF_UNIQUE_NBR
						
						UpdateCount = paymentCheckMapper.INSERT_TRAN_PAYMENT_APPROVE(TranPaymentMap);
					}					
					
					if(UpdateCount == 0){
						ApproveResult = false;
						break;
					}
				}
			}
			
			if(!ApproveResult){
				ReverseApprove(CompanyCd, SystemType, ReverseList);				
				throw new C2SException(ApproveErrCode, ApproveErrMessage);
			}
			
			result = true;
		}catch(C2SException ce){
			this.ErrCode = ce.getExceptCode();
			this.ErrMessage = ce.getMessage();
			ce.printStackTrace();
			result = false;
		}catch(Exception e){
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			e.printStackTrace();
			result = false;
		}

		return result;
	}
	
	/**
	 * Reverse Approve
	 * @param CompanyCd
	 * @param SystemType
	 * @param InfUniqueNbrList
	 * @return
	 * @throws Exception
	 */
	public int ReverseApprove(String CompanyCd, String SystemType, EntityList InfUniqueNbrList) throws Exception{
		int UpdateCount = 0;		
		try{
			Entity entity = null;
			Map<String,Object> UpdateEntity = new HashMap<String,Object>();
			String InfUniqueNbr = "";
			String CurrDate = DateTime.getFormatDateString("yyyyMMddHHmmss", 0);
			
			for(int idx = 0; idx < InfUniqueNbrList.size(); idx++){
				entity = InfUniqueNbrList.get(idx);
				
				InfUniqueNbr = InfUniqueNbr = CurrDate.substring(2, 8) + CompanyCd + SystemType + Utility.LPAD(paymentCheckService.getIndex(CompanyCd, "INF_UNIQUE_NBR", CurrDate.substring(0, 8), 1), 5, '0');
				
				UpdateEntity.put("INF_UNIQUE_NBR", InfUniqueNbr);								// INF_UNIQUE_NBR
				UpdateEntity.put("TRAN_DATE", "");												// TRAN_DATE
				UpdateEntity.put("TRAN_TIME", "");												// TRAN_TIME
				UpdateEntity.put("CANCEL_TYPE", entity.getFieldNoCheck("CANCEL_TYPE"));			// CANCEL_TYPE
				UpdateEntity.put("INF_UNIQUE_NBR2", entity.getFieldNoCheck("INF_UNIQUE_NBR"));	// ORG_INF_UNIQUE_NBR
				
				try{
					UpdateCount = paymentCheckMapper.INSERT_APPROVE_CANCEL(UpdateEntity);
				}catch(Exception e){
				}
			}
		}catch(Exception e){
			UpdateCount = 0;
		}
		return UpdateCount;
	}
	
	
	
	/**
	 * Approve
	 * @param CompanyCd
	 * @param SaleType
	 * @param PaymentType
	 * @param InfUniqueNbr
	 * @param DataEntity
	 * @return
	 * @throws Exception
	 */
	public boolean Approve(String CompanyCd, String SystemType, String ServiceType, String SaleType, String WinType, String InfUniqueNbr, Entity DataEntity, Map<String,Object> resultMap) throws Exception{
		boolean ApproveResult = false;
		Map<String,Object> params = new HashMap<String,Object>();
		
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try{
			String PaySvcType = DataEntity.getFieldNoCheck("PAY_SVC_TYPE");
			String PaymentType = "";
			
			if(PaySvcType.equals("001")){
				SaleType = "000001";
				PaymentType = "000001";
			}
			else if(PaySvcType.equals("002") || PaySvcType.equals("ISP") || PaySvcType.equals("3D")){
				PaymentType = "000002";
			}
			else if(PaySvcType.equals("VAT")){
				PaymentType = "000003";
			}
			else if(PaySvcType.equals("ATF")){
				PaymentType = "000004";
			}
			else if(PaySvcType.equals("PIT")){
				PaymentType = "000005";
			}
			else{
				throw new C2SException("AM01", "WRONG_PAY_SVC_TYPE");
			}
			
			if(ServiceType.length() == 0){
				ServiceType = "XXXXXX";
			}
			
			String MbrNo = "";
			int ReceiptPeriod = 0;
			int UpdateCount	= 0;
			
			params.put("COMPANY_CD", CompanyCd);
			params.put("SYSTEM_TYPE", SystemType);
			params.put("SERVICE_TYPE", ServiceType);
			params.put("SALE_TYPE", SaleType);
			params.put("PAYMENT_TYPE", PaymentType);
			
			List CheckList = paymentCheckMapper.SELECT_COMPANY_PAYMENT(params);
			Map<String,Object> CheckItem = null; 
			
			if(CheckList.size() > 0){
				CheckItem = (Map)CheckList.get(0);
				
				MbrNo = String.valueOf(CheckItem.get("MBR_NO"));
				ReceiptPeriod = Utility.StringToInt(String.valueOf(CheckItem.get("RECEIPT_PERIOD")));
				
				if(MbrNo.length() == 0){
					throw new C2SException("AM02", "NOT_EXIST_MBR_NO");
				}
			}
			else{
				throw new C2SException("AM03", "NOT_EXIST_COMPANY_PAYMENT");
			}
			
			/**********************************************************************************************/
			
			String CurrDate = DateTime.getFormatDateString("yyyyMMddHHmmss", 0);
			
			String PayType = "A";
			String PayTranType = "010";
			String TrackData = "";
			
			String PayApplyDate = CurrDate.substring(0, 8);
			String PaySaleDate = CurrDate.substring(0, 8);
			long PayUnitAmt = Utility.StringToLong(DataEntity.getFieldNoCheck("PAYMENT_AMT"));
			String PayRegNo = "";
			
			// 포인트시스템용
			String OrgInfUniqueNbr = "";
			String OrgApproveDate = "";
			
			if(PaySvcType.equals("001")){		// 현금영수증
				PayTranType = "900";
				TrackData = "37" + DataEntity.getFieldNoCheck("TRACK_DATA");
				
				if(DataEntity.getFieldNoCheck("KEYIN_TYPE").equals("K")){
					TrackData += "=0000";
				}
			}
			else if(PaySvcType.equals("002")){	// 신용카드(일반)
			}
			else if(PaySvcType.equals("ISP")){	// 신용카드(ISP)
			}
			else if(PaySvcType.equals("3D")){	// 신용카드(3D)
			}
			else if(PaySvcType.equals("VAT")){	// 가상계좌
				PayTranType = "810";
				
				List checkPeriod = paymentCheckMapper.getVaPeriod(resultMap);
				Map<String,Object> checkMap = new HashMap<String,Object>();
				
				if(checkPeriod.size() > 0){
					checkMap = (Map)checkPeriod.get(0);
					// 입금마감일
					if(String.valueOf(checkMap.get("RECEIPT_DATE")).equals("1")){
						PaySaleDate = DateTime.addDays(CurrDate.substring(0, 8), 1);
					}
					else{
						PaySaleDate = CurrDate.substring(0, 8);
					}
				}
				
				if(PayRegNo.length() == 0){
					PayRegNo = Utility.LPAD("", 13, '0');
				}
			}
						
			long PaySaleAmt = Utility.StringToLong(DataEntity.getFieldNoCheck("PAYMENT_AMT"));;
			long PaySaleSvcAmt = Utility.StringToLong(DataEntity.getFieldNoCheck("SVC_AMT"));;
			long PaySaleVatAmt = Utility.StringToLong(DataEntity.getFieldNoCheck("VAT_AMT"));;
			
			PaySaleSvcAmt = 0;
			PaySaleVatAmt = 0;
			
			String CustomerName = Utility.RPAD(DataEntity.getFieldNoCheck("CUSTOMER_NAME"), 30, ' ').trim();
			String CustomerTel = Utility.RPAD(DataEntity.getFieldNoCheck("CUSTOMER_TEL"), 15, ' ').trim();
			String CustomerEmail = Utility.RPAD(DataEntity.getFieldNoCheck("CUSTOMER_EMAIL"), 50, ' ').trim();
			String ProduceName = Utility.RPAD(DataEntity.getFieldNoCheck("PRODUCT_NAME"), 30, ' ').trim();
			
			Map<String,Object> ApproveEntity = new HashMap<String,Object>();
			
			ApproveEntity.put("INF_UNIQUE_NBR", InfUniqueNbr);											// INF_UNIQUE_NBR
			ApproveEntity.put("TRAN_DATE", CurrDate.substring(0, 8));									// TRAN_DATE
			ApproveEntity.put("TRAN_TIME", CurrDate.substring(8));										// TRAN_TIME
			ApproveEntity.put("TRAN_COMPANY_CD", CompanyCd);											// TRAN_COMPANY_CD
			ApproveEntity.put("SYSTEM_TYPE", SystemType);												// SYSTEM_TYPE
			ApproveEntity.put("SALE_TYPE", SaleType);													// SALE_TYPE
			ApproveEntity.put("WIN_TYPE", WinType);														// WIN_TYPE
			ApproveEntity.put("PAY_TYPE", PayType);														// PAY_TYPE
			ApproveEntity.put("PAY_SVC_TYPE", PaySvcType);												// PAY_SVC_TYPE
			ApproveEntity.put("ORG_INF_UNIQUE_NBR", OrgInfUniqueNbr);									// ORG_INF_UNIQUE_NBR
			ApproveEntity.put("MBR_NO", MbrNo);															// MBR_NO
			ApproveEntity.put("PAY_MSG_TYPE", "0200");													// PAY_MSG_TYPE
			ApproveEntity.put("PAY_WIN_CD", "");														// PAY_WIN_CD
			ApproveEntity.put("PAY_SHOP_CD", "");														// PAY_SHOP_CD
			ApproveEntity.put("PAY_HALL_CD", "");														// PAY_HALL_CD
			ApproveEntity.put("PAY_TRAN_TYPE", PayTranType);											// PAY_TRAN_TYPE
			ApproveEntity.put("PAY_SALE_CNT", "1");														// PAY_SALE_CNT
			ApproveEntity.put("PAY_APPLY_DATE", PayApplyDate);											// PAY_APPLY_DATE
			ApproveEntity.put("PAY_SALE_DATE", PaySaleDate);											// PAY_SALE_DATE
			ApproveEntity.put("PAY_UNIT_AMT", Long.toString(PayUnitAmt));								// PAY_UNIT_AMT
			ApproveEntity.put("CANCEL_KEY_TYPE", "");													// CANCEL_KEY_TYPE
			ApproveEntity.put("KEYIN_TYPE", DataEntity.getFieldNoCheck("KEYIN_TYPE"));					// KEYIN_TYPE
			ApproveEntity.put("TRACK_DATA", "");														// TRACK_DATA
			ApproveEntity.put("INSTALL_NO", "");														// UI 바뀐 뒤 값 안 넘어옴 INSTALL_NO
			ApproveEntity.put("PAY_CURRENCY", "410");													// PAY_CURRENCY
			ApproveEntity.put("PAY_SALE_AMT", Long.toString(PaySaleAmt));								// PAY_SALE_AMT
			ApproveEntity.put("PAY_SALE_SVC_AMT", Long.toString(PaySaleSvcAmt));						// PAY_SALE_SVC_AMT
			ApproveEntity.put("PAY_SALE_VAT_AMT", Long.toString(PaySaleVatAmt));						// PAY_SALE_VAT_AMT
			ApproveEntity.put("PAY_CERTIFY_TYPE", DataEntity.getFieldNoCheck("CERTIFY_TYPE"));			// PAY_CERTIFY_TYPE
			ApproveEntity.put("PAY_REG_NO", PayRegNo);													// PAY_REG_NO
			ApproveEntity.put("CARD_PASSWORD", "");														// CARD_PASSWORD
			ApproveEntity.put("PAY_BANK_CD", DataEntity.getFieldNoCheck("BANK_CD"));					// PAY_BANK_CD
			ApproveEntity.put("PAY_WITHDRAW_NAME", DataEntity.getFieldNoCheck("WITHDRAW_NAME"));		// PAY_WITHDRAW_NAME
			ApproveEntity.put("FIRM_APPROVE_NO", DataEntity.getFieldNoCheck("FIRM_APPROVE_NO"));		// FIRM_APPROVE_NO
			ApproveEntity.put("ORG_APPROVE_DATE", OrgApproveDate);										// ORG_APPROVE_DATE
			ApproveEntity.put("SVC_CD1", "");															// SVC_CD1
			ApproveEntity.put("SVC_CD2", "");															// SVC_CD2
			ApproveEntity.put("SVC_CD3", "");															// SVC_CD3
			ApproveEntity.put("CUSTOMER_NAME", CustomerName);											// CUSTOMER_NAME
			ApproveEntity.put("CUSTOMER_TEL", EncManager.getEncData("TA_APPROVE", "CUSTOMER_TEL", CustomerTel));			// CUSTOMER_TEL
			ApproveEntity.put("CUSTOMER_EMAIL", EncManager.getEncData("TA_APPROVE", "CUSTOMER_EMAIL", CustomerEmail));		// CUSTOMER_EMAIL
			ApproveEntity.put("PRODUCT_NAME", ProduceName);												// PRODUCT_NAME
			ApproveEntity.put("STATUS_TYPE", "R");														// STATUS_TYPE
		
			UpdateCount = paymentCheckMapper.INSERT_APPROVE(ApproveEntity);
			
			if(UpdateCount == 0){
				throw new C2SException("AM04", "FAILED_INSERT_APPROVE");
			}
			
			/**********************************************************************************************/
			ApproveResult = true;
			Entity ReqEntity = null;
			Entity RspEntity = null;
			
			boolean PayResult = false;
			String PayErrCd = "";
			String PayErrMsg = "";
			
			String CardMbrNo = "";
			String ApproveNo = String.valueOf(resultMap.get("CARD_APPROVE_NO"));
			String ApproveDate = "20"+String.valueOf(resultMap.get("CARD_APPROVE_DATE"));
			String PayRspCd = "00";
			String PayRspMsg = "정상";
			String C2sUniqueNbr = String.valueOf(resultMap.get("CARD_TRADE_NO"));
			String PayAddMsg = String.valueOf(resultMap.get("VACCOUNT"));
			String PayAccountNo = String.valueOf(resultMap.get("VACCOUNT"));
			String IsuCd = "";
			String IsuName = "";
			String IsuCardName = String.valueOf(resultMap.get("CARD_NAME"));
			String AcqCd = "";
			String AcqName = "";
			String ExtendApproveNo = "";
			String CancelType = "";
			String StatusType = "C";
			
			String VaBankCd = "";
			String VaAccountNo = String.valueOf(resultMap.get("VACCOUNT"));
			String VaLimitReceiptDate = "";
			String InstallNo = "";
						
			Map<String,Object> PayRspEntity = new HashMap<String,Object>();
			
			PayRspEntity.put("CARD_NBR_NO", CardMbrNo);
			PayRspEntity.put("APPROVE_NO", ApproveNo);
			PayRspEntity.put("APPROVE_DATE", ApproveDate);
			PayRspEntity.put("PAY_RSP_CD", PayRspCd);
			PayRspEntity.put("PAY_RSP_MSG", PayRspMsg);
			PayRspEntity.put("C2S_UNIQUE_NBR2", C2sUniqueNbr);
			PayRspEntity.put("PAY_ADD_MSG", PayAddMsg);
			PayRspEntity.put("PAY_ACCOUNT_NO", PayAccountNo);
			PayRspEntity.put("ISU_CD", IsuCd);
			PayRspEntity.put("ISU_NAME", IsuName);
			PayRspEntity.put("ISU_CARD_NAME", IsuCardName);
			PayRspEntity.put("ACQ_CD", AcqCd);
			PayRspEntity.put("ACQ_NAME", AcqName);
			PayRspEntity.put("EXTEND_APPROVE_NO", ExtendApproveNo);
			PayRspEntity.put("CANCEL_TYPE", CancelType);
			PayRspEntity.put("STATUS_TYPE", StatusType);
			PayRspEntity.put("INF_UNIQUE_NBR", InfUniqueNbr);
			
			UpdateCount = paymentCheckMapper.UPDATE_APPROVE(PayRspEntity);
			
			if(UpdateCount == 0){
				throw new C2SException("AM05", "FAILED_UPDATE_APPROVE");
			}
			
			if(ApproveResult){
				if(PaySvcType.equals("VAT")){
					Map<String,Object> VAEntity = new HashMap<String,Object>();
					
					VAEntity.put("INF_UNIQUE_NBR", InfUniqueNbr);			// INF_UNIQUE_NBR
					VAEntity.put("PROCESS_FLAG", "N");						// PROCESS_FLAG
					VAEntity.put("TAXSAVE_FLAG", "N");						// TAXSAVE_FLAG
					
					UpdateCount = paymentCheckMapper.INSERT_VIRTUAL_ACCOUNT(VAEntity);
					
					if(UpdateCount == 0){
						throw new C2SException("AM06", "FAILED_INSERT_VIRTUAL_ACCOUNT");
					}
				}
			}
			else{
				throw new C2SException("AM07", PayErrMsg + "[" + PayErrCd + "]");
			}
		}catch(C2SException ce){
			this.ErrCode = ce.getExceptCode();
			this.ErrMessage = ce.getMessage();
			ApproveResult = false;
		}catch(Exception e){
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			ApproveResult = false;
		}

		return ApproveResult;
	}
	
}
