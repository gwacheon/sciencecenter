package kr.go.sciencecenter.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.go.sciencecenter.model.Responsibility;
import kr.go.sciencecenter.persistence.ResponsibilityMapper;

@Service
public class ResponsibilityService {
	private static final Logger logger = LoggerFactory.getLogger(ResponsibilityService.class);
	
	@Autowired
	private ResponsibilityMapper responsibilityMapper;
	
	public Responsibility find(String requestURI){
		
		return responsibilityMapper.find(requestURI);
	}
	
	public List<Responsibility> list(String requestURI){
		return responsibilityMapper.list(requestURI);
	}

	public void create(Responsibility responsibility) {
		responsibilityMapper.create(responsibility);
	}

	public void update(Responsibility responsibility) {
		responsibilityMapper.update(responsibility);
	}

	public void createOrUpdate(Responsibility responsibility) {
		Responsibility existResponsibility = responsibilityMapper.findByResponsibility(responsibility);
		
		if(existResponsibility != null){
			logger.error("exist");
			responsibility.setId(existResponsibility.getId());
			responsibilityMapper.update(responsibility);
		}else{
			logger.error("insert");
			responsibilityMapper.create(responsibility);
		}
	}
}
