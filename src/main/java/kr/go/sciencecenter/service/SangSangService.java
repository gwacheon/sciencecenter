package kr.go.sciencecenter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.go.sciencecenter.model.SangSangSchedule;
import kr.go.sciencecenter.model.SangSangScheduleTitle;
import kr.go.sciencecenter.persistence.SangSangMapper;

@Service
public class SangSangService {
	@Autowired
	private SangSangMapper sangSangMapper;

	public SangSangScheduleTitle findWithSchedules() {
		return sangSangMapper.findWithSchedules();
	}

	public void saveSchedule(SangSangScheduleTitle sangSangScheduleTitle) {
		sangSangMapper.update(sangSangScheduleTitle);
		sangSangMapper.removeSchedules();
		
		for (SangSangSchedule schedule : sangSangScheduleTitle.getSchedules()) {
			sangSangMapper.createSchedule(schedule);
		}
	}
	
	
}
