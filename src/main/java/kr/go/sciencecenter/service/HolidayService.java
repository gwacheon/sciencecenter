package kr.go.sciencecenter.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.go.sciencecenter.model.Holiday;
import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.persistence.HolidayMapper;

@Service
public class HolidayService {
	@Autowired
	private HolidayMapper holidayMapper;
	
	public int create(Holiday holiday){
		return holidayMapper.create(holiday);
	}
	
	public int count(){
		return holidayMapper.count();
	}

	public List<Holiday> list(Page page) {
		return holidayMapper.list(page);
	}
	
	public void delete(int id){
		holidayMapper.delete(id);
	}
}
