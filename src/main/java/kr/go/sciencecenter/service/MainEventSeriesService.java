package kr.go.sciencecenter.service;


import kr.go.sciencecenter.model.MainEventSeries;
import kr.go.sciencecenter.persistence.BoardMapper;
import kr.go.sciencecenter.persistence.MainEventSeriesMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MainEventSeriesService {

	@Autowired
	MainEventSeriesMapper mainEventSeriesMapper;
	
	@Autowired
	BoardMapper boardMapper;
	
	
	public void create(MainEventSeries mainEventSeries) {

		mainEventSeriesMapper.create(mainEventSeries);
		// check all board boolean values		
	}

	public MainEventSeries find(int id) {
		return mainEventSeriesMapper.find(id);
	}

	public MainEventSeries findRecentByMainEventId(int mainEventId) {
		return mainEventSeriesMapper.findRecentByMainEventId(mainEventId);
	}

	public void update(MainEventSeries mainEventSeries) {
		mainEventSeriesMapper.update(mainEventSeries);
	}

	public void delete(int mainEventSeriesId) {
		boardMapper.deleteByMainEventSeriesId(mainEventSeriesId);
		mainEventSeriesMapper.delete(mainEventSeriesId);
		
	}
	
	public void deleteByMainEventId(int mainEventId) {
		// 1. 게시판 삭제.
		boardMapper.deleteByMainEventId(mainEventId);
		// 2. 해당 하는 모든 차수 삭제.
		mainEventSeriesMapper.deleteByMainEventId(mainEventId);
		
	}
	
	
    
}
