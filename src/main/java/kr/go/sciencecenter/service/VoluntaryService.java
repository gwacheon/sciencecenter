
package kr.go.sciencecenter.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.model.Voluntary;
import kr.go.sciencecenter.model.VoluntaryTime;
import kr.go.sciencecenter.model.Volunteer;
import kr.go.sciencecenter.persistence.VoluntaryMapper;

@Service
public class VoluntaryService {
	private static final Logger logger = LoggerFactory.getLogger(VoluntaryService.class);
	
	@Autowired
	private DataSourceTransactionManager txManager;
	
	@Autowired
	private VoluntaryMapper voluntaryMapper;

	public List<Voluntary> list() {
		return voluntaryMapper.list();
	}
	
	public Voluntary find(int id) {
		return voluntaryMapper.find(id);
	}
	
	public void create(Voluntary voluntary) {
		voluntaryMapper.create(voluntary);
	}

	public Voluntary findWithTimes(int id) {
		return voluntaryMapper.findWithTimes(id);
	}
	
	public void createTime(Voluntary voluntary, VoluntaryTime voluntaryTime) {
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

		TransactionStatus status = txManager.getTransaction(def);
		
		try {
			voluntaryMapper.createTime(voluntaryTime);
			
			for(int i = 0; i < voluntary.getWaitMember(); i++){
				Volunteer volunteer = new Volunteer();
				volunteer.setVoluntaryId(voluntary.getId());
				volunteer.setVoluntaryTimeId(voluntaryTime.getId());
				volunteer.setWaited(false);
				voluntaryMapper.createVolunteer(volunteer);
			}
			
			txManager.commit(status);
		} catch(Exception e){
			txManager.rollback(status);
		}
		
	}

	public int deleteTime(int id) {
		return voluntaryMapper.deleteTime(id);
	}

	public Voluntary findTimesWithVolunteerCount(int id, boolean showBefore, String order) {
		Voluntary voluntary = voluntaryMapper.find(id);
		voluntary.setVoluntaryTimes(voluntaryMapper.findTimesAndVolunteerCount(id, showBefore, order));
		return voluntary;
	}

	public VoluntaryTime findTime(int timeId) {
		return voluntaryMapper.findTime(timeId);
	}

	/*
	 * 지원봉사 신청
	 * @param  userId 사용자 id
	 * @param  time 지원봉사 회차 객체
	 * @return 지원자 상태
	 */
	public Volunteer applicationVolunteer(String memberNo, VoluntaryTime time) {
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = txManager.getTransaction(def);
		Volunteer volunteer = null;
		
		try {
			//현재 신청된 사용자 id로 해당 회차에 이미 신청을 하였는지 확인
			Volunteer existVolunteer = voluntaryMapper.findVolunteerWithTimeAndMember(time.getId(), memberNo);
			//기존 신청 이력이 있을경우 해당 객체를 반환
			if(existVolunteer != null){
				txManager.commit(status);
				return existVolunteer;
			}
			
			//신청 가능한 예약 자리 검색 (status IS NULL)
			volunteer = voluntaryMapper.findAvailVolunteer(time.getId());
			
			//해당 신청자에게 선점시킴 --> 선착순 정책 변경에 따라 선점이 아닌 승인 상태로 변경 --> 최종 정보 입력단계 : updateVolunteerInfo 호출시 처리
			if(volunteer != null){
				volunteer.setMemberNo(memberNo);
				volunteer.setStatus(Volunteer.STATUS_ACCEPT);
				voluntaryMapper.pendingVolunteer(volunteer);
				
				txManager.commit(status);
				return volunteer;
			} else {
				txManager.rollback(status);
			}
		} catch(Exception e){
			logger.error(e.toString());
			txManager.rollback(status);
		} 
		
		return null;
	}

	public Volunteer findVolunteer(int volunteerId) {
		return voluntaryMapper.findVolunteer(volunteerId);
	}

	/*
	 * 지원봉사 입력 정보 저장 : 최종 입력항목 저장과 함께 자원봉사 수락상태로 변경
	 * @param  volunteer 자원봉사 객체
	 */
	public void updateVolunteerInfo(Volunteer volunteer) {
		voluntaryMapper.updateVolunteerInfo(volunteer);
	}

	public VoluntaryTime findTimeWithVolunteers(int timeId) {
		return voluntaryMapper.findTimeWithVolunteers(timeId);
	}

	public void updateVolunteerStatus(Volunteer volunteer) {
		voluntaryMapper.updateVolunteerStatus(volunteer);
	}
	
	/*
	 * 봉사활동 신청 수행
	 * @author iskim
	 * @param timeId 봉사활동 회차 id
	 * @param volunteer 자원봉사 신청자 instance
	 */
	public void processApplicant(int timeId, Volunteer volunteer) {
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = txManager.getTransaction(def);
		
		try {
			//현재 신청된 사용자 id로 해당 회차에 이미 신청을 하였는지 확인
			Volunteer existVolunteer = voluntaryMapper.findVolunteerWithTimeAndMember(timeId, volunteer.getMemberNo());
			
			//기존 신청 이력이 있을경우 volunteer의 예약 flag 수
			if(existVolunteer != null){
				txManager.commit(status);
				volunteer.setReserved(true);
			} else {
				Volunteer availableVolunteer = voluntaryMapper.findAvailVolunteer(timeId);
				
				if (availableVolunteer != null) {
					//선착순 처리 방식에 따라 예약자는 기본 승인 처리함.
					volunteer.setId(availableVolunteer.getId());
					volunteer.setStatus(Volunteer.STATUS_ACCEPT);
					voluntaryMapper.updateVolunteerInfo(volunteer);
					txManager.commit(status);
				}
			}
		} catch(Exception e){
			logger.error(e.toString());
			txManager.rollback(status);
		} 
	}

	/*
	 * 봉사활동 대기자들에 대한 임의 추천
	 * @param  id  봉사활동 회차 id
	 * @return 선정된 자원봉사자 List
	 */
	public List<Volunteer> randomCast(int voluntaryId, int timeId) {
		Voluntary voluntary = voluntaryMapper.find(voluntaryId);
		
		List<Volunteer> acceptVolunteers = voluntaryMapper.volunteersByStatus(timeId, Volunteer.STATUS_ACCEPT);
		
		//현재 수락된 인원이 정원보다 적을 경우
		if(voluntary.getMaxMember() > acceptVolunteers.size()){
			int castMemberCnt = voluntary.getMaxMember() - acceptVolunteers.size();
			List<Volunteer> pendingVolunteers = voluntaryMapper.volunteersByStatus(timeId, Volunteer.STATUS_PENDING);
			
			List<Volunteer> castVolunteers;
			//추가로 선정해야 할 인원이 신청 대기인원보다 많은 경우 : 전원 선정 됨
			if(castMemberCnt > pendingVolunteers.size()){
				castVolunteers = pendingVolunteers;
			}else{
				long seed = System.nanoTime();
				Collections.shuffle(pendingVolunteers, new Random(seed));
				
				castVolunteers = new ArrayList<Volunteer>();
				for(int i = 0; i < castMemberCnt; i++){
					castVolunteers.add(pendingVolunteers.get(i));
				}
			}
			
			for (Volunteer volunteer : castVolunteers) {
				volunteer.setStatus(Volunteer.STATUS_ACCEPT);
				voluntaryMapper.updateVolunteerStatus(volunteer);
			}
			
			return castVolunteers;
		}
		
		return null;
	}

	public int volunteersCount(int timeId) {
		return voluntaryMapper.volunteersCount(timeId);
	}

	public List<Volunteer> findVolunteers(int timeId) {
		return voluntaryMapper.findVolunteers(timeId);
	}

	public List<Volunteer> myVolunteers(String currentMemberNo, Page page) {
		return voluntaryMapper.myVolunteers(currentMemberNo, page);
	}

	public int myVolunteersCount(String currentMemberNo) {
		return voluntaryMapper.myVolunteersCount(currentMemberNo);
	}

	public void update(Voluntary voluntary) {
		voluntaryMapper.update(voluntary);
	}

	public void delete(int id) {
		voluntaryMapper.delete(id);
	}

}
