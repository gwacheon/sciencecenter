package kr.go.sciencecenter.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.model.Program;
import kr.go.sciencecenter.model.ProgramAddInfo;
import kr.go.sciencecenter.model.ProgramGroup;
import kr.go.sciencecenter.model.ProgramTime;
import kr.go.sciencecenter.persistence.ProgramMapper;
import kr.go.sciencecenter.persistence.ProgramTimeMapper;

@Service
public class ProgramService {
	private static final Logger logger = LoggerFactory.getLogger(ProgramService.class);
	
	@Autowired
	ProgramMapper programMapper;
	
	@Autowired
	ProgramTimeMapper programTimeMapper;
	
	public int count(ProgramGroup group) {
		return programMapper.count(group);
	}

	public List<Program> list(ProgramGroup group, Page page) {
		return programMapper.list(group, page);
	}

	public void create(Program program) {
		programMapper.create(program);
		setTimes(program);
	}

	public void delete(int id) {
		programMapper.delete(id);
	}

	public Program find(int id) {
		// TODO Auto-generated method stub
		return programMapper.find(id);
	}

	public void update(Program program) {
		programMapper.update(program);
		programMapper.deleteWdays(program.getId());
		programMapper.deleteAddInfos(program.getId());
		
		if(program.getWdays() != null && program.getWdays().size() > 0){
			for (Integer wday : program.getWdays()) {
				if(wday != null){
					programMapper.createWday(program, wday.intValue());
				}
			}
		}
		
		setTimes(program);
	}
	
	public void setTimes(Program program){
		List<ProgramTime> times = new ArrayList<ProgramTime>();
		
		programTimeMapper.deleteByProgram(program);
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd h:mm a", Locale.US);
		
		for (ProgramTime time : program.getTimes()) {
			if(StringUtils.isNotBlank(time.getTitle())
					&& StringUtils.isNotBlank(time.getDateStr())
					&& StringUtils.isNotBlank(time.getBeginTimeStr())
					&& StringUtils.isNotBlank(time.getEndTimeStr())){
				try {
					time.setBeginTime(dateFormat.parse(time.getDateStr() + " " + time.getBeginTimeStr()));
					time.setEndTime(dateFormat.parse(time.getDateStr() + " " + time.getEndTimeStr()));
					times.add(time);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		program.setTimes(times);
		
		if(program.getTimes() != null && program.getTimes().size() > 0){
			for (ProgramTime time : program.getTimes()) {
				programTimeMapper.create(program, time);
			}
		}
		
		if(program.getAddInfos() != null && program.getAddInfos().size() > 0){
			for (String info : program.getAddInfos()){
				if(info != null && !info.equals("")){
					programMapper.addInfo(program, info);
				}
			}
		}
	}

	public List<Program> searchByDate(Date date) {		
		return programMapper.listByRange(date);
	}

}
