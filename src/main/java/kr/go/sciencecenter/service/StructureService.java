package kr.go.sciencecenter.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.go.sciencecenter.model.Structure;
import kr.go.sciencecenter.model.StructureUnavailable;
import kr.go.sciencecenter.persistence.StructureMapper;

@Service
public class StructureService {
	@Autowired
	private StructureMapper structureMapper;

	public List<Structure> list() {
		return structureMapper.list();
	}

	public void create(Structure structure) {
		int maxSeq = structureMapper.maxSeq();
		structure.setSeq(maxSeq);
		structureMapper.create(structure);
	}

	public Structure find(int id) {
		return structureMapper.find(id);
	}

	public void update(Structure structure) {
		structureMapper.update(structure);
	}

	public void delete(int id) {
		structureMapper.delete(id);
	}

	public Structure findWithUnavailables(int id, Date today) {
		return structureMapper.findWithUnavailables(id, today);
	}

	public List<StructureUnavailable> listInDates(int id, String startDateStr, String endDateStr) {
		return structureMapper.listInDates(id, startDateStr, endDateStr);
	}

	/*
	 * 대관 시설물 예약 불가능 설정
	 * @param unavailables 대관 시설물 예약 불가능 객체
	 */
	public void addUnavailableDate(StructureUnavailable unavailable) {
		structureMapper.addUnavailableDates(unavailable);
	}
}
