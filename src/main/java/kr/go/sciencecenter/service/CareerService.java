package kr.go.sciencecenter.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.go.sciencecenter.model.Career;
import kr.go.sciencecenter.model.CareerSchedule;
import kr.go.sciencecenter.persistence.CareerMapper;

@Service
public class CareerService {
	@Autowired
	private CareerMapper careerMapper;

	public List<Career> list() {
		return careerMapper.list();
	}

	public void create(Career career) {
		int maxSeq = careerMapper.maxSeq();
		career.setSeq(maxSeq);
		
		careerMapper.create(career);
	}

	public Career find(int id) {
		return careerMapper.find(id);
	}

	public void saveWithSchedules(Career career) {
		careerMapper.removeSchedules(career.getId());
		
		int i = 0;
		for (CareerSchedule schedule : career.getSchedules()) {
			schedule.setSeq(i++);
			careerMapper.createSchedule(schedule);
		}
	}

	public void delete(int id) {
		careerMapper.delete(id);
	}

	public void update(Career career) {
		careerMapper.update(career);
	}
	
}
