package kr.go.sciencecenter.service;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kr.co.mainticket.Entity.Entity;
import kr.co.mainticket.Exception.C2SException;
import kr.co.mainticket.Science.Interlock.EncManager;
import kr.co.mainticket.Utility.DateTime;
import kr.co.mainticket.Utility.Utility;
import kr.go.sciencecenter.persistence.FamilyModifyMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FamilyModifyService {
	
	private String ErrCode;
	private String ErrMessage;
	
	
	public String getErrCode(){
		return Utility.CheckNull(this.ErrCode);
	}
	
	public String getErrMessage(){
		return Utility.CheckNull(this.ErrMessage).replaceAll("\n", "\\\\n").replaceAll("\"", "'");
	}
	
	@Autowired
	FamilyModifyMapper familyModifyMapper;
	
	@Autowired
	PaymentCheckService paymentCheckService;
	
	
	/**
	 * Modify Family
	 * @param CompanyCd
	 * @param DataEntity
	 * @return
	 * @throws Exception
	 */
	public Map<String,Object> ModifyFamily(String CompanyCd, Entity DataEntity) throws Exception{
		Map<String,Object> result = new HashMap<String,Object>();
		
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try{
			String SaleType = DataEntity.getFieldNoCheck("SALE_TYPE");
			String MemberNo = DataEntity.getFieldNoCheck("MEMBER_NO");
			String WorkerMemberNo = DataEntity.getFieldNoCheck("WORKER_MEMBER_NO");
			
			if(MemberNo.length() == 0){
				throw new C2SException("FM01", "NOT_EXIST_MEMBER_NO");
			}
			
			if(SaleType.length() == 0){
				SaleType = "000002";
			}
			
			if(WorkerMemberNo.length() == 0){
				WorkerMemberNo = MemberNo;
			}
			
			Entity FamilyList = DataEntity.getFieldEntityNoCheck("FAMILY_LIST");
			
			boolean ModifyResult = MemberFamily(CompanyCd, MemberNo, SaleType, WorkerMemberNo, FamilyList);
			
			if(!ModifyResult){
				throw new C2SException(this.ErrCode, this.ErrMessage);
			}
			
			result.put("result", true);
			result.put("Msg", "");
		}catch(C2SException ce){
			this.ErrCode = ce.getExceptCode();
			this.ErrMessage = ce.getMessage();
			ce.printStackTrace();
			result.put("result", false);
			result.put("Msg", ce.getExceptCode()+" :"+ce.getMessage());
		}catch(Exception e){
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			e.printStackTrace();
			result.put("result", false);
			result.put("Msg", "E999 :"+e.getMessage());
		}

		return result;
	}
	
	/**
	 * Member Family
	 * @param conn
	 * @param CompanyCd
	 * @param MemberNo
	 * @param SaleType
	 * @param WorkerMemberNo
	 * @param DataEntity
	 * @return
	 * @throws Exception
	 */
	public boolean MemberFamily(String CompanyCd, String MemberNo, String SaleType, String WorkerMemberNo, Entity DataEntity) throws Exception{
		boolean result = false;
		
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try{
			if(WorkerMemberNo.length() == 0){
				WorkerMemberNo = MemberNo;
			}
			
			String CurrDate = DateTime.getFormatDateString("yyyyMMddHHmmss", 0);
			
			int FamilyCnt = DataEntity.getFieldEntitySize("FAMILY_ITEM");
			Entity FamilyItem = null;
			String FamilyNo = "";
			Map<String,Object> FamilyEntity = null;
			Map<String,Object> MembershipEntity = null;
			StringBuffer Target = new StringBuffer();
			String Paragraph = "";
			int FieldIdx = 1;
			int UpdateCount = 0;
			
			StringBuffer LogData = new StringBuffer();
			Map<String,Object> LogEntity = null;
			
			LogData.delete(0, LogData.length());
			for(int idx = 0; idx < FamilyCnt; idx++){
				FamilyItem = DataEntity.getFieldEntityNoCheck("FAMILY_ITEM", idx);
				FamilyNo = FamilyItem.getFieldNoCheck("FAMILY_NO");
				
				if(FamilyNo.length() > 0){
					LogData.append("BIRTH_DATE : " + FamilyItem.getField("BIRTH_DAY") + "\n");
					LogData.append("GENDER_FLAG : " + FamilyItem.getField("GENDER_FLAG") + "\n");
					LogData.append("FAMILY_CEL : " + FamilyItem.getField("FAMILY_CEL") + "\n");
					LogData.append("SCHOOL_NAME : " + FamilyItem.getField("SCHOOL_NAME") + "\n");
					LogData.append("GRADE_NAME : " + FamilyItem.getField("GRADE_NAME") + "\n");
					LogData.append("USE_FLAG : " + FamilyItem.getField("USE_FLAG") + "\n");
					
					/*
					 * Update on TM_MEMBER_FAMILY
					 */
					FamilyEntity = new HashMap<String,Object>();
					FamilyEntity.put("BIRTH_DATE", FamilyItem.getField("BIRTH_DAY"));
					FamilyEntity.put("GENDER_FLAG", FamilyItem.getField("GENDER_FLAG"));
					FamilyEntity.put("FAMILY_CEL", EncManager.getEncData("TM_MEMBER_FAMILY", "FAMILY_CEL", Utility.CheckNull(FamilyItem.getField("FAMILY_CEL"))));
					FamilyEntity.put("SCHOOL_NAME", FamilyItem.getField("SCHOOL_NAME"));
					FamilyEntity.put("GRADE_NAME", FamilyItem.getField("GRADE_NAME"));
					FamilyEntity.put("CLASS_NAME", FamilyItem.getField("CLASS_NAME"));
					FamilyEntity.put("CLASS_NO", FamilyItem.getField("CLASS_NO"));
					FamilyEntity.put("USE_FLAG", FamilyItem.getField("USE_FLAG"));					
					FamilyEntity.put("UPDATE_USER", WorkerMemberNo);			// UPDATE_USER
					FamilyEntity.put("UPDATE_DATE", CurrDate.substring(0, 8));	// UPDATE_DATE
					FamilyEntity.put("UPDATE_TIME", CurrDate.substring(8));		// UPDATE_TIME
					FamilyEntity.put("COMPANY_CD", CompanyCd);					// COMPANY_CD
					FamilyEntity.put("FAMILY_NO", FamilyNo);					// FAMILY_NO
					
					UpdateCount = familyModifyMapper.UPDATE_MEMBER_FAMILY(FamilyEntity);
					
					if(UpdateCount == 0){
						throw new C2SException("MF01", "NOT_UPDATE_MEMBER_FAMILY");
					}
				}
				else{
					if(FamilyItem.getFieldNoCheck("FAMILY_NAME").length() > 0 && FamilyItem.getFieldNoCheck("RELATION_TYPE").length() > 0){
						Map<String,Object> CheckCon = new HashMap<String,Object>();
						
						CheckCon.put("COMPANY_CD", CompanyCd);
						CheckCon.put("MEMBER_NO", MemberNo);
						CheckCon.put("FAMILY_NAME", FamilyItem.getFieldNoCheck("FAMILY_NAME"));
						
						List CheckList = familyModifyMapper.SELECT_MEMBER_FAMILY_CHECK(CheckCon);
						
						if(CheckList.size() > 0){
							Map<String,Object> cntMap = (Map)CheckList.get(0);
							if(Utility.StringToInt(String.valueOf(cntMap.get("CHECK_CNT"))) > 0){
								throw new C2SException("MF02", "이미 등록된 가족이 있습니다.");
							}
						}
						
						String LunarFlag = FamilyItem.getFieldNoCheck("LUNAR_FLAG");
						
						if(!LunarFlag.equals("Y")){
							LunarFlag = "N";
						}
						
						/*
						 * Insert on TM_MEMBER_FAMILY
						 */
						FamilyNo = "GNSMF" + CurrDate.substring(2, 10) + Utility.LPAD(paymentCheckService.getIndex(CompanyCd, "FAMILY_NO", "1980", 1), 6, '0');
						
						FamilyEntity = new HashMap<String,Object>();
						
						FamilyEntity.put("COMPANY_CD", CompanyCd);										// COMPANY_CD
						FamilyEntity.put("FAMILY_NO", FamilyNo);										// FAMILY_NO
						FamilyEntity.put("FAMILY_NAME", FamilyItem.getFieldNoCheck("FAMILY_NAME"));		// FAMILY_NAME
						FamilyEntity.put("MEMBER_NO", MemberNo);										// MEMBER_NO
						FamilyEntity.put("RELATION_TYPE", FamilyItem.getFieldNoCheck("RELATION_TYPE"));	// RELATION_TYPE
						FamilyEntity.put("BIRTH_DATE", FamilyItem.getFieldNoCheck("BIRTH_DAY"));		// BIRTH_DATE
						FamilyEntity.put("LUNAR_FLAG", LunarFlag);										// LUNAR_FLAG
						FamilyEntity.put("GENDER_FLAG", FamilyItem.getFieldNoCheck("GENDER_FLAG"));		// GENDER_FLAG
						FamilyEntity.put("FAMILY_CEL", EncManager.getEncData("TM_MEMBER_FAMILY", "FAMILY_CEL", Utility.CheckNull(FamilyItem.getFieldNoCheck("FAMILY_CEL"))));		// FAMILY_CEL
						FamilyEntity.put("SCHOOL_NAME", FamilyItem.getFieldNoCheck("SCHOOL_NAME"));		// SCHOOL_NAME
						FamilyEntity.put("GRADE_NAME", FamilyItem.getFieldNoCheck("GRADE_NAME"));		// GRADE_NAME
						FamilyEntity.put("WELFARE_FLAG", "N");											// WELFARE_FLAG
						FamilyEntity.put("USE_FLAG", "Y");												// USE_FLAG
						FamilyEntity.put("INSERT_USER", WorkerMemberNo);								// INSERT_USER
						FamilyEntity.put("INSERT_DATE", CurrDate.substring(0, 8));						// INSERT_DATE
						FamilyEntity.put("INSERT_TIME", CurrDate.substring(8));							// INSERT_TIME
						FamilyEntity.put("CLASS_NAME", FamilyItem.getFieldNoCheck("CLASS_NAME"));
						FamilyEntity.put("CLASS_NO", FamilyItem.getFieldNoCheck("CLASS_NO"));
						
						UpdateCount = familyModifyMapper.INSERT_MEMBER_FAMILY(FamilyEntity);
						
						if(UpdateCount == 0){
							throw new C2SException("MF03", "FAIL_INSERT_MEMBER_FAMILY");
						}
						
						MembershipEntity = new HashMap<String,Object>();
						
						MembershipEntity.put("SALE_TYPE", SaleType);					// SALE_TYPE
						MembershipEntity.put("FAMILY_NO", FamilyNo);					// FAMILY_NO
						MembershipEntity.put("INSERT_USER", WorkerMemberNo);			// INSERT_USER
						MembershipEntity.put("INSERT_DATE", CurrDate.substring(0, 8));	// INSERT_DATE
						MembershipEntity.put("INSERT_TIME", CurrDate.substring(8));		// INSERT_TIME
						MembershipEntity.put("COMPANY_CD", CompanyCd);					// COMPANY_CD
						MembershipEntity.put("MEMBER_NO", MemberNo);					// MEMBER_NO
						
						UpdateCount = familyModifyMapper.INSERT_MEMBERSHIP_FAMILY(MembershipEntity);
						
						if(UpdateCount == 0){
							throw new C2SException("MF04", "FAIL_INSERT_MEMBERSHIP");
						}
						
						LogData.append("FAMILY_NAME : " + FamilyItem.getFieldNoCheck("FAMILY_NAME") + "\n");
						LogData.append("BIRTH_DATE : " + FamilyItem.getFieldNoCheck("BIRTH_DATE") + "\n");
						LogData.append("BIRTH_DAY : " + FamilyItem.getFieldNoCheck("BIRTH_DAY") + "\n");
						LogData.append("GENDER_FLAG : " + FamilyItem.getFieldNoCheck("GENDER_FLAG") + "\n");
						LogData.append("FAMILY_TEL : " + FamilyItem.getFieldNoCheck("FAMILY_TEL") + "\n");
						LogData.append("FAMILY_CEL : " + FamilyItem.getFieldNoCheck("FAMILY_CEL") + "\n");
						LogData.append("FAMILY_EMAIL : " + FamilyItem.getFieldNoCheck("FAMILY_EMAIL") + "\n");
						LogData.append("SCHOOL_NAME : " + FamilyItem.getFieldNoCheck("SCHOOL_NAME") + "\n");
						LogData.append("GRADE_NAME : " + FamilyItem.getFieldNoCheck("GRADE_NAME") + "\n");
						LogData.append("CLASS_NAME : " + FamilyItem.getFieldNoCheck("CLASS_NAME") + "\n");
					}
				}
				
				if(LogData.length() > 0){
					LogEntity = new HashMap<String,Object>();
					
					LogEntity.put("COMPANY_CD", CompanyCd);																		// COMPANY_CD
					LogEntity.put("FAMILY_NO", FamilyNo);																		// FAMILY_NO
					LogEntity.put("LOG_DATA", EncManager.getEncData("TM_MEMBER_FAMILY_LOG", "LOG_DATA", LogData.toString()));	// LOG_DATA
					LogEntity.put("INSERT_USER", WorkerMemberNo);																// INSERT_USER
					LogEntity.put("INSERT_DATE", CurrDate.substring(0, 8));														// INSERT_DATE
					LogEntity.put("INSERT_TIME", CurrDate.substring(8));														// INSERT_TIME
					LogEntity.put("INSERT_IP", DataEntity.getFieldNoCheck("CLIENT_IP"));										// INSERT_IP
					LogEntity.put("INSERT_SESSION", DataEntity.getFieldNoCheck("CLIENT_SESSION"));								// INSERT_SESSION
					
					UpdateCount = familyModifyMapper.INSERT_MEMBER_FAMILY_LOG(LogEntity);
				}				
			}		
			result = true;
		}catch(C2SException ce){
			this.ErrCode = ce.getExceptCode();
			this.ErrMessage = ce.getMessage();
			ce.printStackTrace();
			result = false;
		}catch(Exception e){
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			e.printStackTrace();
			result = false;
		}
		

		return result;
	}
	
	/**
	 * Modify Friend
	 * @param CompanyCd
	 * @param DataEntity
	 * @return
	 * @throws Exception
	 */
	public Map<String,Object> ModifyFriend(String CompanyCd, Entity DataEntity) throws Exception{
		Map<String,Object> result = new HashMap<String,Object>();
		Connection conn = null;
		
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try{
			String SaleType = DataEntity.getFieldNoCheck("SALE_TYPE");
			String MemberNo = DataEntity.getFieldNoCheck("MEMBER_NO");
			String CourseCd = DataEntity.getFieldNoCheck("COURSE_CD");
			String WorkerMemberNo = DataEntity.getFieldNoCheck("WORKER_MEMBER_NO");
			
			if(MemberNo.length() == 0){
				throw new C2SException("FM01", "NOT_EXIST_MEMBER_NO");
			}
			
			if(SaleType.length() == 0){
				SaleType = "000002";
			}
			
			if(WorkerMemberNo.length() == 0){
				WorkerMemberNo = MemberNo;
			}
			
			Entity FamilyList = DataEntity.getFieldEntityNoCheck("FRIEND_LIST");
			
			boolean ModifyResult = MemberFriend( CompanyCd, MemberNo, CourseCd, SaleType, WorkerMemberNo, FamilyList);
			
			if(!ModifyResult){
				throw new C2SException(this.ErrCode, this.ErrMessage);
			}
			
			result.put("result", true);
			result.put("Msg", "");
		}catch(C2SException ce){
			this.ErrCode = ce.getExceptCode();
			this.ErrMessage = ce.getMessage();
			result.put("result", false);
			result.put("Msg", ce.getExceptCode()+" :"+ce.getMessage());
		}catch(Exception e){
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			result.put("result", false);
			result.put("Msg", "E999 :"+e.getMessage());
		}

		return result;
	}
	
	/**
	 * Member Friend
	 * @param conn
	 * @param CompanyCd
	 * @param MemberNo
	 * @param SaleType
	 * @param WorkerMemberNo
	 * @param DataEntity
	 * @return
	 * @throws Exception
	 */
	public boolean MemberFriend( String CompanyCd, String MemberNo, String CourseCd, String SaleType, String WorkerMemberNo, Entity DataEntity) throws Exception{
		boolean result = false;
		
		this.ErrCode = "";
		this.ErrMessage = "";
		
		try{
			if(WorkerMemberNo.length() == 0){
				WorkerMemberNo = MemberNo;
			}
			
			String CurrDate = DateTime.getFormatDateString("yyyyMMddHHmmss", 0);
			
			int FamilyCnt = DataEntity.getFieldEntitySize("FRIEND_ITEM");
			Entity FamilyItem = null;
			String FamilyNo = "";
			Map<String,Object> FamilyEntity = null;
			Map<String,Object> MembershipEntity = null;
			int UpdateCount = 0;
			
			StringBuffer LogData = new StringBuffer();
			Map<String,Object> LogEntity = null;
						
			for(int idx = 0; idx < FamilyCnt; idx++){
				FamilyItem = DataEntity.getFieldEntityNoCheck("FRIEND_ITEM", idx);
				
				FamilyNo = FamilyItem.getFieldNoCheck("FRIEND_NO");
				
				LogData.delete(0, LogData.length());
				
				if(FamilyNo.length() > 0){
					/*
					 * Update on TM_MEMBER_FAMILY
					 */
					FamilyEntity = new HashMap<String,Object>();
					
								
					LogData.append("BIRTH_DAY : " + FamilyItem.getField("BIRTH_DATE") + "\n");
					LogData.append("GENDER_FLAG : " + FamilyItem.getField("GENDER_FLAG") + "\n");
					LogData.append("FRIEND_CEL : " + FamilyItem.getField("FRIEND_CEL") + "\n");
					LogData.append("SCHOOL_NAME : " + FamilyItem.getField("SCHOOL_NAME") + "\n");
					LogData.append("USE_FLAG : " + FamilyItem.getField("USE_FLAG") + "\n");
					
					String LunarFlag = FamilyItem.getFieldNoCheck("LUNAR_FLAG");
						
					if(!LunarFlag.equals("Y")){
						LunarFlag = "N";
					}
					FamilyEntity.put("BIRTH_DAY", FamilyItem.getField("BIRTH_DAY"));
					FamilyEntity.put("GENDER_FLAG", FamilyItem.getField("GENDER_FLAG"));
					FamilyEntity.put("FRIEND_CEL", EncManager.getEncData("TM_MEMBER_FRIEND", "FRIEND_CEL", Utility.CheckNull(FamilyItem.getField("FRIEND_CEL"))));
					FamilyEntity.put("SCHOOL_NAME", FamilyItem.getField("SCHOOL_NAME"));
					FamilyEntity.put("USE_FLAG", FamilyItem.getField("USE_FLAG"));					
					FamilyEntity.put("UPDATE_USER", WorkerMemberNo);			// UPDATE_USER
					FamilyEntity.put("UPDATE_DATE", CurrDate.substring(0, 8));	// UPDATE_DATE
					FamilyEntity.put("UPDATE_TIME", CurrDate.substring(8));		// UPDATE_TIME
					FamilyEntity.put("COMPANY_CD", CompanyCd);					// COMPANY_CD
					FamilyEntity.put("FRIEND_NO", FamilyNo);					// FRIEND_NO
					FamilyEntity.put("COURSE_CD", CourseCd);					// COURSE_CD					
					
					UpdateCount = familyModifyMapper.UPDATE_MEMBER_FRIEND(FamilyEntity);
					
					if(UpdateCount == 0){
						throw new C2SException("MF01", "NOT_UPDATE_MEMBER_FRIEND");
					}
				}
				else{
					if(FamilyItem.getFieldNoCheck("FRIEND_NAME").length() > 0 && FamilyItem.getFieldNoCheck("RELATION_TYPE").length() > 0){
						Map<String,Object> CheckCon = new HashMap<String,Object>();
						
						CheckCon.put("COMPANY_CD", CompanyCd);
						CheckCon.put("MEMBER_NO", MemberNo);
						CheckCon.put("COURSE_CD", CourseCd);
						CheckCon.put("FRIEND_NAME", FamilyItem.getFieldNoCheck("FRIEND_NAME"));
						
						List CheckList = familyModifyMapper.SELECT_MEMBER_FRIEND_CHECK(CheckCon);
						
						if(CheckList.size() > 0){
							Map<String,Object> rMap = new HashMap<String,Object>();
							rMap = (Map)CheckList.get(0);
							if(Utility.StringToInt(String.valueOf(rMap.get("CHECK_CNT"))) > 0){
								throw new C2SException("MF02", "이미 등록된 가족이 있습니다.");
							}
						}
						
						String LunarFlag = FamilyItem.getFieldNoCheck("LUNAR_FLAG");
						
						if(!LunarFlag.equals("Y")){
							LunarFlag = "N";
						}
						
						/*
						 * Insert on TM_MEMBER_FAMILY
						 */
						FamilyNo = "GNSMF" + CurrDate.substring(2, 10) + Utility.LPAD(paymentCheckService.getIndex(CompanyCd, "FAMILY_NO", "1980", 1), 6, '0');
						
						FamilyEntity = new HashMap<String,Object>();
						
						FamilyEntity.put("COMPANY_CD", CompanyCd);											// COMPANY_CD
						FamilyEntity.put("FRIEND_NO", FamilyNo);											// FRIEND_NO
						FamilyEntity.put("COURSE_CD", CourseCd);											// COURSE_CD
						FamilyEntity.put("FRIEND_NAME", FamilyItem.getFieldNoCheck("FRIEND_NAME"));			// FRIEND_NAME
						FamilyEntity.put("MEMBER_NO", MemberNo);											// MemberNo
						FamilyEntity.put("RELATION_TYPE", FamilyItem.getFieldNoCheck("RELATION_TYPE"));		// RELATION_TYPE
						FamilyEntity.put("BIRTH_DAY", FamilyItem.getFieldNoCheck("BIRTH_DAY"));				// BIRTH_DAY
						FamilyEntity.put("LUNAR_FLAG", LunarFlag);											// LUNAR_FLAG
						FamilyEntity.put("GENDER_FLAG", FamilyItem.getFieldNoCheck("GENDER_FLAG"));			// GENDER_FLAG
						FamilyEntity.put("FRIEND_TEL", EncManager.getEncData("TM_MEMBER_FRIEND", "FRIEND_TEL", Utility.CheckNull(FamilyItem.getFieldNoCheck("FRIEND_TEL"))));		// FRIEND_TEL
						FamilyEntity.put("FRIEND_CEL", EncManager.getEncData("TM_MEMBER_FRIEND", "FRIEND_CEL", Utility.CheckNull(FamilyItem.getFieldNoCheck("FRIEND_CEL"))));		// FRIEND_CEL
						FamilyEntity.put("FRIEND_EMAIL", EncManager.getEncData("TM_MEMBER_FRIEND", "FRIEND_EMAIL", Utility.CheckNull(FamilyItem.getFieldNoCheck("FRIEND_EMAIL"))));	// FRIEND_EMAIL
						FamilyEntity.put("SCHOOL_NAME", FamilyItem.getFieldNoCheck("SCHOOL_NAME"));			// SCHOOL_NAME
						FamilyEntity.put("GRADE_NAME", FamilyItem.getFieldNoCheck("GRADE_NAME"));			// GRADE_NAME
						FamilyEntity.put("CLASS_NAME", FamilyItem.getFieldNoCheck("CLASS_NAME"));			// CLASS_NAME
						FamilyEntity.put("WELFARE_FLAG", "N");												// WELFARE_FLAG
						FamilyEntity.put("USE_FLAG", "Y");													// USE_FLAG
						FamilyEntity.put("INSERT_USER", WorkerMemberNo);									// INSERT_USER
						FamilyEntity.put("INSERT_DATE", CurrDate.substring(0, 8));							// INSERT_DATE
						FamilyEntity.put("INSERT_TIME", CurrDate.substring(8));								// INSERT_TIME
						
						UpdateCount = familyModifyMapper.INSERT_MEMBER_FRIEND(FamilyEntity);
						
						if(UpdateCount == 0){
							throw new C2SException("MF03", "FAIL_INSERT_MEMBER_FRIEND");
						}
						
						MembershipEntity = new HashMap<String,Object>();
						
						MembershipEntity.put("SALE_TYPE", SaleType);					// SALE_TYPE
						MembershipEntity.put("FAMILY_NO", FamilyNo);					// FAMILY_NO
						MembershipEntity.put("INSERT_USER", WorkerMemberNo);			// INSERT_USER
						MembershipEntity.put("INSERT_DATE", CurrDate.substring(0, 8));	// INSERT_DATE
						MembershipEntity.put("INSERT_TIME", CurrDate.substring(8));		// INSERT_TIME
						MembershipEntity.put("COMPANY_CD", CompanyCd);					// COMPANY_CD
						MembershipEntity.put("MEMBER_NO", MemberNo);					// MEMBER_NO
						
						UpdateCount = familyModifyMapper.INSERT_MEMBERSHIP_FAMILY(MembershipEntity);
						if(UpdateCount == 0){
							throw new C2SException("MF04", "FAIL_INSERT_MEMBERSHIP");
						}
						
						LogData.append("FRIEND_NAME : " + FamilyItem.getFieldNoCheck("FRIEND_NAME") + "\n");
						LogData.append("BIRTH_DATE : " + FamilyItem.getFieldNoCheck("BIRTH_DATE") + "\n");
						LogData.append("BIRTH_DAY : " + FamilyItem.getFieldNoCheck("BIRTH_DAY") + "\n");
						LogData.append("GENDER_FLAG : " + FamilyItem.getFieldNoCheck("GENDER_FLAG") + "\n");
						LogData.append("FRIEND_TEL : " + FamilyItem.getFieldNoCheck("FRIEND_TEL") + "\n");
						LogData.append("FRIEND_CEL : " + FamilyItem.getFieldNoCheck("FRIEND_CEL") + "\n");
						LogData.append("FRIEND_EMAIL : " + FamilyItem.getFieldNoCheck("FRIEND_EMAIL") + "\n");
						LogData.append("SCHOOL_NAME : " + FamilyItem.getFieldNoCheck("SCHOOL_NAME") + "\n");
						LogData.append("GRADE_NAME : " + FamilyItem.getFieldNoCheck("GRADE_NAME") + "\n");
						LogData.append("CLASS_NAME : " + FamilyItem.getFieldNoCheck("CLASS_NAME") + "\n");
					}
				}
				
				if(LogData.length() > 0){
					LogEntity = new HashMap<String,Object>();
					
					LogEntity.put("COMPANY_CD", CompanyCd);																		// COMPANY_CD
					LogEntity.put("FAMILY_NO", FamilyNo);																		// FAMILY_NO
					LogEntity.put("LOG_DATA", EncManager.getEncData("TM_MEMBER_FAMILY_LOG", "LOG_DATA", LogData.toString()));	// LOG_DATA
					LogEntity.put("INSERT_USER", WorkerMemberNo);																// INSERT_USER
					LogEntity.put("INSERT_DATE", CurrDate.substring(0, 8));														// INSERT_DATE
					LogEntity.put("INSERT_TIME", CurrDate.substring(8));														// INSERT_TIME
					LogEntity.put("INSERT_IP", DataEntity.getFieldNoCheck("CLIENT_IP"));										// INSERT_IP
					LogEntity.put("INSERT_SESSION", DataEntity.getFieldNoCheck("CLIENT_SESSION"));								// INSERT_SESSION
					
					UpdateCount = familyModifyMapper.INSERT_MEMBER_FAMILY_LOG(LogEntity);
				}
			}
			
			result = true;
		}catch(C2SException ce){
			this.ErrCode = ce.getExceptCode();
			this.ErrMessage = ce.getMessage();
			result = false;
		}catch(Exception e){
			this.ErrCode = "E999";
			this.ErrMessage = e.getMessage();
			result = false;
		}
		

		return result;
	}

}
