package kr.go.sciencecenter.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.go.sciencecenter.model.MainEvent;
import kr.go.sciencecenter.persistence.MainEventMapper;
import kr.go.sciencecenter.persistence.MainEventSeriesMapper;

@Service
public class MainEventService {

	@Autowired
	MainEventMapper mainEventMapper;
	
	@Autowired
	MainEventSeriesService mainEventSeriesService;

	public void create(MainEvent mainEvent) {
		mainEventMapper.create(mainEvent);
	}

	public List<MainEvent> listWithSeries(String type) {
		return mainEventMapper.listWithSeries(type);
	}
	
	public List<MainEvent> list() {
		return mainEventMapper.list();
	}
	
	public List<MainEvent> listOnlyHasSeries() {
		return mainEventMapper.listOnlyHasSeries();
	}
	public List<MainEvent> listByType(String type) {
		return mainEventMapper.listByType(type);
	}

	public void update(MainEvent mainEvent) {
		mainEventMapper.update(mainEvent);
	}

	public MainEvent find(int id) {
		return mainEventMapper.find(id);
	}

	public MainEvent findWithRecentSeries(int mainEventId) {

		return mainEventMapper.findWithRecentSeries(mainEventId);
	}
	
	public void delete(int id) {
		// 해당 카테고리의 series 들부터 삭제.
		mainEventSeriesService.deleteByMainEventId(id);
		// 카테고리 삭제
		mainEventMapper.delete(id);
	}
}
