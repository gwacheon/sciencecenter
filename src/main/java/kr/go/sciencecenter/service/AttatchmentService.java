package kr.go.sciencecenter.service;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import kr.go.sciencecenter.model.Attatchment;
import kr.go.sciencecenter.persistence.AttatchmentMapper;
import kr.go.sciencecenter.util.FileUploadHelper;

@Service
public class AttatchmentService {

	@Autowired
	AttatchmentMapper attatchmentMapper;
	
	@Autowired
	private FileUploadHelper fileUploadHelper;

	public void create(Attatchment attatchment, CommonsMultipartFile file){
		
		attatchmentMapper.create(attatchment);
		
		String urlPath = Attatchment.ATTATCHMENT_FILE_PREFIX;
		urlPath += "/" + attatchment.getId();
		
		String fileName = attatchment.getId() + "." + FilenameUtils.getExtension(file.getOriginalFilename());
		
		fileUploadHelper.upload(file, urlPath, fileName);
		
		attatchment.setFilePath(urlPath);
		attatchment.setFileName(fileName);
		
		attatchmentMapper.update(attatchment.getId(), attatchment.getFilePath(), attatchment.getFileName());
	}
}
