package kr.go.sciencecenter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.go.sciencecenter.model.DinnerApplyTime;
import kr.go.sciencecenter.persistence.DinnerApplyTimeMapper;

@Service
public class DinnerApplyTimeService {

	@Autowired
	private DinnerApplyTimeMapper dinnerApplyTimeMapper;

	public DinnerApplyTime findApplicationTimeMostRecent() {
		return dinnerApplyTimeMapper.findApplicationTimeMostRecent();
	}

	public void create(DinnerApplyTime dinnerApplyTime) {
		
		dinnerApplyTimeMapper.create(dinnerApplyTime);
	}
	
}
