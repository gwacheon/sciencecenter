package kr.go.sciencecenter.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.model.api.Member;
import kr.go.sciencecenter.persistence.MembershipLogMapper;

@Service
public class MembershipLogService {
	@Autowired
	MembershipLogMapper logMapper;
	
	public List<Member> searchMembership(String name) {
		return logMapper.searchMembership(name);
	}

	public void createLog(String memberNo, String name) {
		logMapper.createLog(memberNo, name);
	}
	
	public List<Member> statistics(Page page) {
		return logMapper.statistics(page);
	}
	
	public List<Member> logByMemberNo(String memberNo, Page page) {
		return logMapper.logByMemberNo(memberNo, page);
	}

	public int staticCount() {
		return logMapper.staticCount();
	}

	public int countByMember(String memberNo) {
		return logMapper.countByMember(memberNo);
	}
}
