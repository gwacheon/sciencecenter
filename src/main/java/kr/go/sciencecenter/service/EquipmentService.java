package kr.go.sciencecenter.service;

import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import kr.go.sciencecenter.api.AuthenticationApi;
import kr.go.sciencecenter.model.EquipReserve;
import kr.go.sciencecenter.model.EquipSerial;
import kr.go.sciencecenter.model.Equipment;
import kr.go.sciencecenter.model.api.Member;
import kr.go.sciencecenter.persistence.EquipmentMapper;
import kr.go.sciencecenter.util.FileUploadHelper;

@Service
public class EquipmentService {
	//User Authentication API for getting Member Info
	private static AuthenticationApi api = new AuthenticationApi();

	@Autowired
	EquipmentMapper equipmentMapper;

	@Autowired
	private FileUploadHelper fileUploadHelper;

	public List<Equipment> list() {
		return equipmentMapper.list();
	}

	public void create(Equipment equipment, MultipartFile file) {
		equipmentMapper.create(equipment);
		equipmentMapper.createSerials(equipment);
		
		if (file != null) {
			String urlPath = Equipment.EQUIPMENT_FILE_PREFIX;
			urlPath += "/" + equipment.getId();
			
			String fileName = equipment.getId() + "." + FilenameUtils.getExtension(file.getOriginalFilename());
			
			fileUploadHelper.upload(file, urlPath, fileName);
			
			equipment.setPicture(urlPath + "/" + fileName);

			equipmentMapper.update(equipment);
		}
	}

	public List<EquipSerial> serials(int equipmentId) {
		return equipmentMapper.serials(equipmentId);
	}

	public EquipSerial findSerial(int id) {
		EquipSerial serial =equipmentMapper.findSerial(id);
		for (EquipReserve reserve : serial.getEquipReserves()){
			Member member = new Member();
			member.setMemberNo(reserve.getMemberNo());
			reserve.setMember(api.getMemberInfo(member));
		}
		return serial;
		
	}

	public List<Equipment> listByType(String type) {
		return equipmentMapper.listByType(type);
	}

	public Equipment find(int id) {
		return equipmentMapper.find(id);
	}

	public void update(Equipment equipment, MultipartFile file) {
		if (file != null) {
			String urlPath = Equipment.EQUIPMENT_FILE_PREFIX;
			urlPath += "/" + equipment.getId();
			String fileName = equipment.getId() + "." + FilenameUtils.getExtension(file.getOriginalFilename());
			
			fileUploadHelper.delete(urlPath, fileName);
			fileUploadHelper.upload(file, urlPath, fileName);
			
			equipment.setPicture(urlPath + "/" + fileName);
		}
		
		equipmentMapper.update(equipment);

	}
}
