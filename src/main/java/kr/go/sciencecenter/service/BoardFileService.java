package kr.go.sciencecenter.service;

import java.util.List;

import kr.go.sciencecenter.model.BoardFile;
import kr.go.sciencecenter.persistence.BoardFileMapper;
import kr.go.sciencecenter.util.FileUploadHelper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BoardFileService {

	@Autowired
	BoardFileMapper boardFileMapper;

	@Autowired
	private FileUploadHelper fileUploadHelper;
	
	public BoardFile find(int id) {
		return boardFileMapper.find(id);
	}
	
	public void createAll(List<BoardFile> files) {
		boardFileMapper.createAll(files);
	}

	public void create(BoardFile boardFile, String urlPath, int boardId) {
		
		boardFile.setBoardId(boardId);
		boardFile.setName(boardFile.getFile().getOriginalFilename());
		boardFile.setUrl(urlPath + "/" + boardFile.getFile().getOriginalFilename());
		
		boardFileMapper.create(boardFile);
		fileUploadHelper.upload(boardFile.getFile(), urlPath, boardFile.getFile().getOriginalFilename());
	}

	public void update(BoardFile f) {
		boardFileMapper.update(f);
	}
	
	public boolean deleteFile(BoardFile boardFile, String urlPath) {
		boardFileMapper.deleteFile(boardFile.getId());
		urlPath += "/" + boardFile.getBoardId();
		return fileUploadHelper.delete(urlPath, boardFile.getName());
		
	}
	
	public void deleteFiles(int boardId, String urlPath) {
		boardFileMapper.deleteFiles(boardId);
		fileUploadHelper.multiDelete(urlPath);
		
	}
}
