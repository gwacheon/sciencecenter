package kr.go.sciencecenter.service;

import java.text.DecimalFormat;
import java.util.Map;

import kr.co.mainticket.Utility.DateTime;
import kr.go.sciencecenter.persistence.SmsEmailMapper;
import kr.go.sciencecenter.persistence.UserMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * SMS 와  EMAIL을 전송
 * @author ckh
 *
 */
@Service
public class SmsEmailService {
	private static final Logger logger = LoggerFactory.getLogger(SmsEmailService.class);
	
	@Value("${legacy.companycd}") private String companyCd;	//ckh추가 기존 회사코드 프라퍼티적용
	@Value("${crm.returnEmail}") private String returnEmail;	//이메일 발송지
	@Value("${crm.returnName}") private String returnName;	//이메일 발송자
	@Value("${crm.callback}") private String callback;	//sms 콜백번호
	
	@Autowired	SmsEmailMapper 	smsemailMapper;
	@Autowired	UserMapper 		userMapper;
	
	/**
	 * SMS EMAIL 전송시 호출
	 * @param map
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void setSmsEmail(Map map){
		logger.info("SmsEmailService.setSmsEmail -- !" + map.toString());
		//사용자 정보
		//MEMBER_CEL - 받는 번호
		//MEMBER_EMAIL	- 받는 이메일
		//MEMBER_NO	- 받는 맴버번호
		//INSERT_USER - MEMBER_NO
		Map um = userMapper.getLegacyUser(map);
		
		um.put("COMPANY_CD", companyCd);
		um.put("CRM_UNIQUE_NBR", "CRM" + DateTime.getFormatDateString("yyyyMMddHHmmss", 0) + (int) (Math.random() * 1000));
		um.put("RETURN_EMAIL", returnEmail);
		um.put("RETURN_NAME", returnName);
		um.put("CALLBACK", callback);
		um.put("SEND_FLAG", "N");
		
		
		Map layoutMap = smsemailMapper.selectContentsLayout(map);
		map.putAll(layoutMap);
		map.putAll(um);
		um.putAll(replaceLayout(map));
		
		logger.info("SmsEmailService.setSmsEmail.map -- !" + map.toString());
		
		if(map.get("SMS_FLAG") != null && map.get("SMS_FLAG").equals("Y") && map.get("SMS_FLAG_LAYOUT").equals("Y"))		setSms(map);
		if(map.get("EMAIL_FLAG") != null &&map.get("EMAIL_FLAG").equals("Y") && map.get("EMAIL_FLAG_LAYOUT").equals("Y"))	setEmail(map);
	}
	
	/**
	 * EMAIL 전송
	 * @param map
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", })
	private int setEmail(Map map){
		logger.info("SmsEmailService.setEmail -- !" + map.toString());
		return smsemailMapper.insertEmailHistory(map);
	}
	
	/**
	 * SMS 전송
	 * @param map
	 * @return
	 */
	@SuppressWarnings({ "rawtypes" })
	private int setSms(Map map){
		logger.info("SmsEmailService.setSms -- !" + map.toString());
		return smsemailMapper.insertSmsHistory(map);
	}
	
	/**
	 * 컨텐츠의 내용을 리플레이스
	 * @param map
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Map replaceLayout(Map map) {
		String smsContents = map.get("SMS_CONTENTS").toString();
//		logger.info("smsContents -- " + smsContents);
//		logger.info("map.get(SUB_TITLE_NAME) -- " + map.get("SUB_TITLE_NAME"));
		DecimalFormat df = new DecimalFormat("#,###");
		if(map.get("MEMBER_NAME") != null)		smsContents = smsContents.replaceAll("<#MEMBER_NAME#>", 	(String) map.get("MEMBER_NAME"));
		if(map.get("MEMBER_ID") != null)		smsContents = smsContents.replaceAll("<#MEMBER_ID#>", 		(String) map.get("MEMBER_ID"));
		if(map.get("PASSWORD") != null)			smsContents = smsContents.replaceAll("<#PASSWORD#>", 		(String) map.get("PASSWORD"));
		if(map.get("FAMILY_NAME") != null)		smsContents = smsContents.replaceAll("<#FAMILY_NAME#>", 	(String) map.get("FAMILY_NAME"));
		if(map.get("PAYMENT_AMT") != null)		smsContents = smsContents.replaceAll("<#PAYMENT_AMT#>", 	 df.format(map.get("PAYMENT_AMT")));
		if(map.get("BANK_NAME") != null)		smsContents = smsContents.replaceAll("<#BANK_NAME#>", 		(String) map.get("BANK_NAME"));
		if(map.get("ACCOUNT_NO") != null)		smsContents = smsContents.replaceAll("<#ACCOUNT_NO#>", 		(String) map.get("ACCOUNT_NO"));
		if(map.get("LIMIT_DATE") != null)		smsContents = smsContents.replaceAll("<#LIMIT_DATE#>", 		(String) map.get("LIMIT_DATE"));
		if(map.get("SUB_TITLE_NAME") != null)	smsContents = smsContents.replaceAll("<#SUB_TITLE_NAME#>", 	(String) map.get("SUB_TITLE_NAME"));
		if(map.get("PRICE_NAME") != null)		smsContents = smsContents.replaceAll("<#PRICE_NAME#>", 		(String) map.get("PRICE_NAME"));
		if(map.get("PERIOD") != null)			smsContents = smsContents.replaceAll("<#PERIOD#>", 			(String) map.get("PERIOD"));
		if(map.get("SCHEDULE") != null)			smsContents = smsContents.replaceAll("<#SCHEDULE#>", 		(String) map.get("SCHEDULE"));
		if(map.get("REGIST_DATE") != null)		smsContents = smsContents.replaceAll("<#REGIST_DATE#>", 	(String) map.get("REGIST_DATE"));
		//if(map.get("APPLY_CNT") != null)		smsContents = smsContents.replaceAll("<#APPLY_CNT#>", 		(String) map.get("APPLY_CNT"));
		if(map.get("REGIST_DATE") != null)		smsContents = smsContents.replaceAll("<#REGIST_DATE#>", 	(String) map.get("REGIST_DATE"));
		if(map.get("TITLE_NAME") != null)		smsContents = smsContents.replaceAll("<#TITLE_NAME#>", 		(String) map.get("TITLE_NAME"));
		if(map.get("SUB_TITLE_NAME") != null)	smsContents = smsContents.replaceAll("<#SUB_TITLE_NAME#>", 	(String) map.get("SUB_TITLE_NAME"));
		
//		if(map.get("CONTENTS_TYPE").equals("L00005"))	//예약신청
//			smsContents = smsContents.substring(0, smsContents.length() -1);
//		if(map.get("CONTENTS_TYPE").equals("L10001"))	//예약취소
//			smsContents = smsContents.substring(0, smsContents.length() -1);
//		if(map.get("CONTENTS_TYPE").equals("L10002"))	//예약확불	 
//			smsContents = smsContents.substring(0, smsContents.length() -1);
		
		map.put("SMS_CONTENTS", smsContents);
		return map;
	}
}