package kr.go.sciencecenter.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.go.sciencecenter.model.OrgApplicant;
import kr.go.sciencecenter.model.OrgProgram;
import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.persistence.OrgProgramMapper;

@Service
public class OrgProgramService {
	@Autowired
	OrgProgramMapper orgProgramMapper;
	
	public List<OrgProgram> adminList() {
		return orgProgramMapper.adminList();
	}

	public List<OrgProgram> list() {
		return orgProgramMapper.list();
	}

	public void create(OrgProgram orgProgram) {
		orgProgramMapper.create(orgProgram);
	}

	public OrgProgram find(int id) {
		return orgProgramMapper.find(id);
	}

	public List<OrgApplicant> applicants(int id, OrgApplicant searchOrgApplicant, Page page, String dateStr) {
		return orgProgramMapper.applicants(id, searchOrgApplicant, page, dateStr);
	}
	
	public int applicantsCount(int orgProgramId, OrgApplicant searchOrgApplicant, String dateStr) {
		return orgProgramMapper.applicantsCount(orgProgramId, searchOrgApplicant, dateStr);
	}

	public void createApplicant(OrgApplicant orgApplicant) {
		OrgApplicant applicant = orgProgramMapper.findByApplicant(orgApplicant);
		
		if (applicant == null) {
			orgProgramMapper.createApplicant(orgApplicant);
		} else {
			orgApplicant.setDuplicate(true);
		}
	}

	public List<OrgApplicant> myApplicants(String memberNo, Page page) {
		return orgProgramMapper.myApplicants(memberNo, page);
	}

	public int myApplicantsCount(String memberNo) {
		return orgProgramMapper.myApplicantsCount(memberNo);
	}

	public void update(OrgProgram orgProgram) {
		orgProgramMapper.update(orgProgram);
	}
}
