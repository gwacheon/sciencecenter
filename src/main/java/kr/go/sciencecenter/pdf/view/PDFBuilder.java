package kr.go.sciencecenter.pdf.view;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;

import kr.go.sciencecenter.model.Volunteer;
 
/**
 * This view class generates a PDF document 'on the fly' based on the data
 * contained in the model.
 * @author www.codejava.net
 *
 */
public class PDFBuilder extends AbstractITextPdfView {
	private static final Logger logger = LoggerFactory.getLogger(PDFBuilder.class);
	
	
 
    @Override
    protected void buildPdfDocument(Map<String, Object> model, Document doc,
            PdfWriter writer, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
    	
    	Volunteer volunteer = (Volunteer)model.get("volunteer");
    	
    	XMLWorkerHelper helper = XMLWorkerHelper.getInstance();
    	
    	String webappRootPath = request.getSession().getServletContext().getRealPath("/");
    	
    	
    	String fontPath = webappRootPath + "resources/fonts/NanumBarunGothic.ttf";
    	BaseFont objBaseFont = BaseFont.createFont(fontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
    	
    	//Title
    	Paragraph title = new Paragraph("봉사활동 수료증", new Font(objBaseFont, 24));
    	title.setAlignment(Element.ALIGN_CENTER);
    	doc.add(title);
    	
    	//Infos
    	Paragraph paragraph2 = new Paragraph();
    	paragraph2.setFont(new Font(objBaseFont, 16));
        paragraph2.setSpacingAfter(25);
        paragraph2.setSpacingBefore(25);
        paragraph2.add("이름 : " + volunteer.getName());
        
        doc.add(paragraph2);
    	
    	Image logoImage = Image.getInstance(webappRootPath + "/resources/img/main_a/logo_kr.png");
    	logoImage.setAbsolutePosition(400f, 50f);
    	doc.add(logoImage);
    	/*
    	String cssPath = webappRootPath + "/resources/css/pdf/volunteer.css";
    	String fontPath = webappRootPath + "/resources/fonts/nanumbarungothic/NanumBarunGothic.ttf";
    	
    	// CSS
    	CSSResolver cssResolver = new StyleAttrCSSResolver();
    	CssFile cssFile = helper.getCSS(new FileInputStream(cssPath));
    	cssResolver.addCss(cssFile);
    	     
    	// HTML, 폰트 설정
    	XMLWorkerFontProvider fontProvider = new XMLWorkerFontProvider(XMLWorkerFontProvider.DONTLOOKFORFONTS);
    	fontProvider.register(fontPath, "NanumGothic");
    	CssAppliers cssAppliers = new CssAppliersImpl(fontProvider);
    	 
    	HtmlPipelineContext htmlContext = new HtmlPipelineContext(cssAppliers);
    	htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());
    	 
    	// Pipelines
    	PdfWriterPipeline pdf = new PdfWriterPipeline(doc, writer);
    	HtmlPipeline html = new HtmlPipeline(htmlContext, pdf);
    	CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);
    	 
    	XMLWorker worker = new XMLWorker(css, true);
    	XMLParser xmlParser = new XMLParser(worker, Charset.forName("UTF-8"));
    	
    	StringBuffer htmlSb = new StringBuffer();
    	
    	htmlSb.append("<!DOCTYPE HTML>");
    	htmlSb.append("<html>");
    	htmlSb.append("<head>");
    	htmlSb.append("<meta charset='utf-8'/ ><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />");
    	htmlSb.append("</head>");
    	
    	htmlSb.append("<body>");
    	htmlSb.append("<h1 class='title'>봉사활동 수료증</h1>");
    	
    	htmlSb.append("<div class='volunteer-wrapper'>");
    		htmlSb.append("<div class='no'>");
    		htmlSb.append(volunteer.getId());
    		htmlSb.append("</div>");
    		
    		htmlSb.append("<div class='time'>");
    		htmlSb.append(volunteer.getRegDttm());
    		htmlSb.append("</div>");
    		
    		htmlSb.append("<div class='name'>");
    		htmlSb.append(volunteer.getSchool());
    		htmlSb.append("</div>");
    	htmlSb.append("</div>");
    	
    	htmlSb.append("<img src='http://localhost:8080/sciencecenter/resources/img/main_a/logo_kr.png' />");
    	htmlSb.append("</body>");
    	
    	htmlSb.append("</html>");
    	
    	StringReader strReader = new StringReader(htmlSb.toString());
    	xmlParser.parse(strReader);
    	*/
    	
    	
    	/*
        // get data model which is passed by the Spring container
        doc.add(new Paragraph("Recommended books for Spring framework"));
         
        PdfPTable table = new PdfPTable(5);
        table.setWidthPercentage(100.0f);
        table.setWidths(new float[] {3.0f, 2.0f, 2.0f, 2.0f, 1.0f});
        table.setSpacingBefore(10);
         
        // define font for table header row
        Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setColor(BaseColor.WHITE);
         
        // define table header cell
        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(BaseColor.BLUE);
        cell.setPadding(5);
         
        // write table header
        cell.setPhrase(new Phrase("Book Title", font));
        table.addCell(cell);
         
        cell.setPhrase(new Phrase("Author", font));
        table.addCell(cell);
 
        cell.setPhrase(new Phrase("ISBN", font));
        table.addCell(cell);
         
        cell.setPhrase(new Phrase("Published Date", font));
        table.addCell(cell);
         
        cell.setPhrase(new Phrase("Price", font));
        table.addCell(cell);
         
        // write table row data
        
        table.addCell("Title");
        table.addCell("getAuthor");
        table.addCell("getIsbn");
        table.addCell("getPublishedDate");
        table.addCell("getPrice");
         
        doc.add(table);
        */
    }
 
}