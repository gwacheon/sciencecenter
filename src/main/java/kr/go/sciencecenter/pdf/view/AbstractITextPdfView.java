package kr.go.sciencecenter.pdf.view;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.AbstractView;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

import kr.go.sciencecenter.pdf.view.AbstractITextPdfView.BorderEvent;
 
/**
 * This class is a work around for working with iText 5.x in Spring.
 * The code here is almost identical to the AbstractPdfView class.
 *
 */
public abstract class AbstractITextPdfView extends AbstractView {
	
	public class BorderEvent extends PdfPageEventHelper {
		@Override
		public void onEndPage(PdfWriter writer, Document document) {
			PdfContentByte canvas = writer.getDirectContent();
			Rectangle rect = document.getPageSize();
			
			rect.setBorder(Rectangle.BOX); // left, right, top, bottom border
			rect.setBorderWidth(2); // a width of 5 user units
			rect.setBorderColor(BaseColor.BLACK); // a red border
			rect.setUseVariableBorders(true); // the full width will be visible
			canvas.rectangle(rect);
		}
	}
 
    public AbstractITextPdfView() {
        setContentType("application/pdf");
    }
 
    @Override
    protected boolean generatesDownloadContent() {
        return true;
    }
         
    @Override
    protected void renderMergedOutputModel(Map<String, Object> model,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
        // IE workaround: write into byte array first.
    	response.setHeader("Content-Transper-Encoding", "binary");
    	response.setHeader("Content-Disposition", "inline; filename=" + "자원봉사 수료증" + ".pdf");
    	
        ByteArrayOutputStream baos = createTemporaryOutputStream();
 
        // Apply preferences and build metadata.
        Document document = newDocument();
        PdfWriter writer = newWriter(document, baos);
        prepareWriter(model, writer, request);
        buildPdfMetadata(model, document, request);
        
        //AddBorder
        BorderEvent borderEvent = new BorderEvent();
        writer.setPageEvent(borderEvent);
        // Build PDF document.
        document.open();
        buildPdfDocument(model, document, writer, request, response);
        document.close();
 
        // Flush to HTTP response.
        writeToResponse(response, baos);
    }
 
    protected Document newDocument() {
        return new Document(PageSize.A4, 36, 36, 36, 36);
    }
     
    protected PdfWriter newWriter(Document document, OutputStream os) throws DocumentException {
        return PdfWriter.getInstance(document, os);
    }
     
    protected void prepareWriter(Map<String, Object> model, PdfWriter writer, HttpServletRequest request)
            throws DocumentException {
 
        writer.setViewerPreferences(getViewerPreferences());
    }
     
    protected int getViewerPreferences() {
        return PdfWriter.ALLOW_PRINTING | PdfWriter.PageLayoutSinglePage;
    }
     
    protected void buildPdfMetadata(Map<String, Object> model, Document document, HttpServletRequest request) {
    }
     
    protected abstract void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
            HttpServletRequest request, HttpServletResponse response) throws Exception;
}