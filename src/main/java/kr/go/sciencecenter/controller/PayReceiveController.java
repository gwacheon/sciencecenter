package kr.go.sciencecenter.controller;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.co.mainticket.Entity.Entity;
import kr.co.mainticket.Utility.Box;
import kr.co.mainticket.Utility.HttpUtility;
import kr.co.mainticket.Utility.Utility;
import kr.go.sciencecenter.model.api.Member;
import kr.go.sciencecenter.service.PaymentCheckService;
import kr.go.sciencecenter.service.PaymentService;
import kr.go.sciencecenter.service.SmsEmailService;
import kr.go.sciencecenter.service.EntranceService;
import kr.go.sciencecenter.util.PropertyHelper;

/**
 * 결제리턴을 받는 컨트롤러
 * @author ckh
 *
 */
@Controller
@RequestMapping("/payresponse")
public class PayReceiveController {
	private static final Logger logger = LoggerFactory.getLogger(PayReceiveController.class);
	
	@Value("${legacy.companycd}") private String companyCd;	//ckh추가 기존 회사코드 프라퍼티적용
	
	@Autowired PaymentCheckService 	paymentCheckService;
	@Autowired PaymentService		paymentService;
	@Autowired SmsEmailService		smsEmailService;
	@Autowired PropertyHelper 		propertyHelper;
	@Autowired EntranceService		entranceService;
	
	/**
	 * 연간회원 결제리턴 후 API 적용
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value="/setMembershipUpdate")
	public @ResponseBody Model setMembershipUpdate(Model model,HttpServletRequest request,HttpServletResponse response)throws Exception{
		
		//family_no, member_no
		logger.info("PayReceiveController.setMembershipUpdate -- ! " + HttpUtility.getBox(request).toString());
		Map map = HttpUtility.handleRequest(request);
		
		//결제결과가 성공이 아닙면 null 리턴
		if(!map.get("rstCode").equals("0000") && !map.get("rstCode").equals("00")) return null;
		
		//1. 파라미터에서 나온 값으로 연간회원  DB 등록(기존 API로 등록했으나 결제방식 변경으로 새롭게 작업)
		//TM_MEMBERSHIP, TA_TRAN_PAYMENT, TA_APPROVE 테이블에 입력 - 가상계좌시 TA_VIRTUAL_ACCOUNT 입력
		map.put("COMPANY_CD", companyCd);
		map.put("MEMBER_NO", map.get("member_no"));
		map = paymentService.makeMap(map);
		paymentService.insertTmMembership(map);
		paymentService.insertTaAppove(map);
		paymentService.insertTaTranPayment(map);
		paymentService.updateTaAppove(map);
		
		//2. DB에 등록된 값이 파라미터와 넘어온 값이 같으면 TM_MEMBERSHIP, TA_TRAN_PAYMENT, TA_APPROVE 을 JOIN 한값이 있을경우
		if(paymentService.isTmMembershipTaTranPayment(map))	model.addAttribute("rescode","00");
		else												model.addAttribute("rescode","99");
		
		//3. SMS EMAIL 발송
		if(model.asMap().get("rescode").equals("00"))	setSmsEmail(map);
		
		return model;
	}
	
	/**
	 * SMS EMAIL 발송
	 * @param map
	 * @throws Exception 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void setSmsEmail(Map map) throws Exception {
		
		map.put("CONTENTS_TYPE", "M00005");
		map.put("ORDNO", map.get("oid"));
		map.put("SYSTEM_TYPE", "MEM");
		map.put("COMPANY_CD", propertyHelper.getCompanycd());
		map.put("MEMBER_NO", map.get("MEMBER_NO"));
		map.put("PAYMENT_AMT", Integer.parseInt(map.get("salesPrice").toString()));
		
		smsEmailService.setSmsEmail(map);
	}
	
	/**
	 * 강좌 결제결과 리턴
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unused", "rawtypes" })
	@RequestMapping(value="/CourseApplyPay")
	public @ResponseBody Model CourseApplyPay(Model model,HttpServletRequest request,HttpServletResponse response)throws Exception{
		
		logger.info("PayReceiveController.CourseApplyPay -- ! " + HttpUtility.getBox(request).toString());
		
		
		if ("0000".equals(request.getParameter("rstCode")) || "00".equals(request.getParameter("rstCode"))) {
			
			Box params = HttpUtility.getBox(request);
			Map<String,Object> paraMap = new HashMap<String,Object>();
			
			Map<String,Object> rsMap = new HashMap<String,Object>();
			
			//결제팝에서 내려온 값인 oid 값은 실제 TL_COURSE_APPLY 테이블의 APPLY_UNIQUE_NBR 값이므로 해당 테이블에서 정보를 가져옴
			Map cuap = paymentCheckService.get_TL_COURSE_APPLY(params.getString("oid"));	//강좌예매
			paraMap.put("MEMBER_NO", cuap.get("MEMBER_NO"));
			paraMap.put("APPLY_UNIQUE_NBR", cuap.get("APPLY_UNIQUE_NBR"));
			paraMap.put("SALE_TYPE", cuap.get("SALE_TYPE"));
			paraMap.put("TOTAL_PRICE", cuap.get("TOTAL_PRICE"));
			paraMap.put("APPLY_CNT", cuap.get("APPLY_CNT"));
			paraMap.put("COURSE_NAME", cuap.get("COURSE_NAME"));
			
			//결제팝업에서 내려오는 값
			paraMap.put("CARD_APPROVE_DATE", params.getString("cardApprovDate"));
			paraMap.put("CARD_APPROVE_NO", params.getString("cardApprovNo"));
			paraMap.put("CARD_TRADE_NO", params.getString("cardTradeNo"));
			paraMap.put("CARD_NAME", request.getParameter("cardName"));
			paraMap.put("PAY_KIND", params.getString("payKind"));
			paraMap.put("PAY_TYPE", params.getString("payType"));
			paraMap.put("VACCOUNT", params.getString("vAccount")); 
			paraMap.put("LIMIT_RECEIPT_DATE", params.getString("vCriticalDate").replaceAll("[^0-9]", ""));	//TA_TRAN_PAYMENT - LIMIT_RECEIPT_DATE
			
			
			Entity ApplyEntity = new Entity(15);
			
			ApplyEntity.setField("SALE_TYPE", cuap.get("SALE_TYPE").toString());
			ApplyEntity.setField("WIN_TYPE", "000000");
			ApplyEntity.setField("SMS_SEND_FLAG", "Y");
			ApplyEntity.setField("EMAIL_SEND_FLAG", "N");
			ApplyEntity.setField("APPLY_UNIQUE_NBR", cuap.get("APPLY_UNIQUE_NBR").toString());
			ApplyEntity.setField("WORKER_MEMBER_NO", cuap.get("MEMBER_NO").toString());
			ApplyEntity.setField("CLIENT_IP", request.getRemoteAddr());
			
			ApplyEntity.setField("PROCESS_TYPE", "PAY");
			
			String DeductFlag = "N";
			String DeductMemo = "";
			long DeductPrice = 0;
					
			ApplyEntity.setField("DEDUCT_FLAG", DeductFlag);
			ApplyEntity.setField("DEDUCT_MEMO", DeductMemo);
			ApplyEntity.setField("DEDUCT_PRICE", Long.toString(DeductPrice));
			
			long TotalPrice = Utility.StringToLong(cuap.get("TOTAL_PRICE").toString().replaceAll(",", ""));
			
			if(TotalPrice > 0){
				long PaymentAmt = Utility.StringToLong(cuap.get("TOTAL_PRICE").toString().replaceAll(",", ""));
				
				Entity PaymentList = new Entity(3);
				Entity PaymentItem = null;
				
				String KeyinType = "";
				String TrackData = "";
				
				if(PaymentAmt > 0){
					String PayKind = params.getString("payKind");
					String PaymentType ="";
					String PaySvcType = "";
					String InstallNo = "";
					String ValidMonth = "";
					String PaymentRegNo = "";
					String PaymentCertifyType = "1";	// 개인 / 사업자 구분 UI 바뀌면서 구분값 없음
					String BankCd = params.getString("vAccountBankCode");	//params.getString("VA_BANK_CD")
					String Depositor = "";
					String cardPassword = "";
					String cavv = "";
					
					KeyinType = "";
					TrackData = "";
					
					if(PayKind.equals("1")){			// 신용카드 card
						PaySvcType = params.getString("payType");
						KeyinType = "K";	
						PaymentType = "000002";
					}
					else if(PayKind.equals("2")){	// 가상계좌 VAT
						PaymentRegNo = "0000000000000";
						PaySvcType = params.getString("payType");
						KeyinType = "K";
						TrackData = "0100001234";
						PaymentCertifyType = "01";
						PaymentType = "000003";
					}
					
					PaymentItem = new Entity(10);
					
					PaymentItem.setField("PAYMENT_TYPE", PaymentType);
					PaymentItem.setField("PAY_SVC_TYPE", PaySvcType);
					PaymentItem.setField("PAYMENT_AMT", Long.toString(PaymentAmt));
					PaymentItem.setField("KEYIN_TYPE", KeyinType);	
					PaymentItem.setField("CERTIFY_TYPE", PaymentCertifyType);
					PaymentItem.setField("BANK_CD", BankCd);
					PaymentItem.setField("DEPOSITOR", Depositor);
					PaymentItem.setField("LIMIT_RECEIPT_DATE", params.getString("vCriticalDate").replaceAll("[^0-9]", ""));
					
					PaymentList.addField("PAYMENT_ITEM", PaymentItem);
				}
				
				ApplyEntity.setField("PAYMENT_LIST", PaymentList);
			}
			
			rsMap = paymentCheckService.CourseApplyPay(companyCd, ApplyEntity, paraMap);
			model.addAttribute("rescode",rsMap.get("result"));
			logger.info("result == " + rsMap.get("result"));
		} else {
			model.addAttribute("rescode","99");
		}
		
		return model;
	}
	
	/**
	 * 가상계좌 결과 리턴
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="/VACallback")
	public @ResponseBody String VACallback(Model model,HttpServletRequest request,HttpServletResponse response)throws Exception{
		
		logger.info("PayReceiveController.VACallback -- ! " + HttpUtility.handleRequest(request));
		
		/*
		 * 	TRGUBUN		거래구분	2	‘V2’
			STOREID		상점아이디	6	 씨스퀘어소프트에서 부여해줌
			ORDNO		주문번호	20	
			TRDATE		거래일자	8	‘YYYYMMDD’
			TRTIME		거래시간	6	‘HHMISS’
			ACCOUNTNO	가상계좌번호	14	
			RCPTNAME	입금자명	30	
			AMT		금액		8	‘0005000’
			BANK_CODE	은행코드	2(3)	
			REPLYCODE	응답코드	4	‘0000’
			
			TRGUBUN=V2&
			STOREID=100011&
			ORDNO=C15090110000134&
			TRDATE=20150901&
			TRTIME=112714&
			ACCOUNTNO=08200987697412&
			RCPTNAME=현정&
			RCPTSOCIALNO=0000000000000&
			REPLYCODE=0000&
			AMT=00001000&
			BANK_CODE=03
		 */
		
		request.setCharacterEncoding("EUC-KR");
		Map map = HttpUtility.handleRequest(request);
		map.put("IP", request.getRemoteAddr());
		
		logger.info("map -- ! " + map.toString());
		logger.info("RCPTNAME -- " + URLDecoder.decode(map.get("RCPTNAME").toString(), "UTF-8"));
		logger.info("RCPTNAME -- " + URLDecoder.decode(map.get("RCPTNAME").toString(), "EUC-KR"));
//		logger.info("RCPTNAME -- " + new String(request.getParameter(map.get("RCPTNAME").toString()).getBytes("UTF-8"), "EUC-KR"));
		//1. 파라미터 내용 테이블 저장 로그용 (VACALLBACK)
		paymentService.insertVaCallBack(map);
		
		//강좌인지 아닌지 확인하여 수행
		//2. STOREID, ORDNO, ACCOUNTNO, RCPTNAME, AMT 을 비교하여 강좌내용에 있는지를 확인해서 UPDATE 처리
		//3. update 가 false 이면 입장내용에 있는지 확인하여 UPDATE 처리(LJ SOFT 구현로직)
		if(paymentService.isLecture(map) > 0)
			paymentService.updateVAPament(map);
		else
			if(!updateEntranceVaCallBack(map)) return "FAIL";
		
		//4. 결과로 'OK' 를 찍어야 결제처리
		return "OK";
	}
	
	/**
	 * 2. STOREID, ORDNO, ACCOUNTNO, RCPTNAME, AMT 을 비교하여 강좌내용에 있는지를 확인해서 UPDATE 처리
	 * @param string
	 * @return
	 */
//	@SuppressWarnings("rawtypes")
//	private boolean updateLectureVaCallBack(Map map) {
//		if(paymentService.updateVAPament(map) == 2)	return true;
//		else return false;
//	}

	/**
	 * 3. update 가 false 이면 입장내용에 있는지 확인하여 UPDATE 처리(LJ SOFT 구현로직)
	 * @param string
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private boolean updateEntranceVaCallBack(Map map) {
		// TODO Auto-generated method stub
		return entranceService.payCallback(map);
	}

	public String getCurrentMemberNo(HttpServletRequest request) {
		return (String)request.getSession().getAttribute(Member.CURRENT_MEMBER_NO);
	}
}
