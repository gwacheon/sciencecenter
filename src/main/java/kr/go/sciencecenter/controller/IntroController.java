
package kr.go.sciencecenter.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreFilter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.common.base.Joiner;

import kr.go.sciencecenter.model.Board;
import kr.go.sciencecenter.model.BoardType;
import kr.go.sciencecenter.model.BreadCrumb;
import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.service.BoardService;
import kr.go.sciencecenter.util.DownloadHelper;

/**
 * 과학관소개 Controller
 * @author Prompt Technology
 *
 */
@Controller
@RequestMapping("/introduce")
public class IntroController {
	private static final Logger logger = LoggerFactory.getLogger(IntroController.class);
	
	@Autowired
	private BoardService boardService;
	
	
	/**
	 * 공지/공고 게시판 Index
	 * @param model
	 * @param currentPage
	 * @param searchType
	 * @param searchKey
	 * @return
	 */
	@RequestMapping(value = "/notice", method = RequestMethod.GET)
	public String notice(Model model, 
			@RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			@RequestParam(value = "searchType", required=false) String searchType,
			@RequestParam(value = "searchKey", required=false) String searchKey) {
		
		Board.getBoards(model, boardService, "notice", currentPage, searchType, searchKey);
		model.addAttribute("crumb", new BreadCrumb(6,1,1));
		return "intro/notice.app";
	}
	
	/**
	 * 공지/공고 게시판 Show
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/notice/{id}", method=RequestMethod.GET)
	public String noticeShow(@PathVariable int id,Model model){
		model.addAttribute("id", id);
		model.addAttribute("crumb", new BreadCrumb(6,1,1));
		return "intro/noticeShow.app"; 
	}

	/**
	 * 소식지 게시판 Index
	 * @param model
	 * @param currentPage
	 * @param searchType
	 * @param searchKey
	 * @return
	 */
	@RequestMapping(value = "/news", method = RequestMethod.GET)
	public String news(Model model, @RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			@RequestParam(value = "searchType", required=false) String searchType,
			@RequestParam(value = "searchKey", required=false) String searchKey) {
		model.addAttribute("crumb", new BreadCrumb(6,1,2));
		Board.getBoards(model, boardService, "news", currentPage, searchType, searchKey);
		return "intro/news.app";
	}
	
	/**
	 * 소식지 게시판 Show
	 * 소식지 게시판이 갤러리형 및 링크형 게시판으로 변경되면서, 미사용됨. 
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/news/{id}", method=RequestMethod.GET)
	public String newsShow(@PathVariable int id,Model model){
		
		model.addAttribute("content", boardService.find(id).getContent());
		return "intro/newsShow.kiosk"; 
	}
	
	
	/**
	 * 소식지 게시판 Index :
	 * 키요스크에서 접속할 e-소식지 Index Page.
	 * 기존의 소식지 게시판 페이지와 동일하지만, top, footer 및 navbar, scrollspy 전부 제외.
	 * 순수 갤러리 이미지들만 보여줌.
	 * 
	 * @param model
	 * @param currentPage
	 * @param searchType
	 * @param searchKey
	 * @return
	 */
	@RequestMapping(value = "/eNews", method = RequestMethod.GET)
	public String eNews(Model model, @RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			@RequestParam(value = "searchType", required=false) String searchType,
			@RequestParam(value = "searchKey", required=false) String searchKey) {
		
		String requestURI = boardService.getHttpServletRequest().getRequestURI();
		String path = requestURI + "?";
		List<String> parameters = new ArrayList<String>();
		
		String type= "news";
		BoardType boardType = BoardType.BOARD_TYPES.get(type);
		model.addAttribute("boardType", boardType);
		
		int pagePerCount = 6;
		Page page = new Page(currentPage, pagePerCount, boardService.count(type, searchType, searchKey));
		page.setPath(path + Joiner.on("&").join(parameters) + "&");
		model.addAttribute("page", page);
		
		model.addAttribute("boards", boardService.list(type, page, searchType, searchKey, null));
		
		return "intro/eNewsByKiosk.kiosk";
	}
	
	
	/**
	 * 보도자료 게시판 Index
	 * @param model
	 * @param currentPage
	 * @param searchType
	 * @param searchKey
	 * @return
	 */
	@RequestMapping(value = "/report", method = RequestMethod.GET)
	public String report(Model model, @RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			@RequestParam(value = "searchType", required=false) String searchType,
			@RequestParam(value = "searchKey", required=false) String searchKey) {
		model.addAttribute("crumb", new BreadCrumb(6,1,3));
		Board.getBoards(model, boardService, "report", currentPage, searchType, searchKey);
		return "intro/report.app";
	}

	/**
	 * 보도자료 게시판 Show
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/report/{id}", method=RequestMethod.GET)
	public String reportShow(@PathVariable int id,Model model){
		model.addAttribute("id", id);
		model.addAttribute("crumb", new BreadCrumb(6,1,3));
		return "intro/reportShow.app"; 
	}
	
	/**
	 * 현장스케치 게시판 Index
	 * @param model
	 * @param currentPage
	 * @param searchType
	 * @param searchKey
	 * @return
	 */
	@RequestMapping(value = "/sketch", method = RequestMethod.GET)
	public String sketch(Model model, @RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			@RequestParam(value = "searchType", required=false) String searchType,
			@RequestParam(value = "searchKey", required=false) String searchKey) {
		
		Board.getBoards(model, boardService, "sketch", currentPage, searchType, searchKey);
		model.addAttribute("crumb", new BreadCrumb(6,1,4));
		return "intro/sketch.app";
	}
	
	/**
	 * 현장스케치 게시판 Show
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/sketch/{id}", method = RequestMethod.GET)
	public String sketchShow(@PathVariable int id, Model model) {
		model.addAttribute("id", id);
		model.addAttribute("crumb", new BreadCrumb(6,1,4));
		return "intro/sketchShow.app";
	}
	
	/**
	 * 과학관UCC 게시판 index
	 * @param model
	 * @param currentPage
	 * @param searchType
	 * @param searchKey
	 * @return
	 */
	@RequestMapping(value = "/programIntro", method = RequestMethod.GET)
	public String programIntro(Model model, 
			@RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			@RequestParam(value = "searchType", required=false) String searchType,
			@RequestParam(value = "searchKey", required=false) String searchKey) {

		Board.getBoards(model, boardService, "programIntro", currentPage, searchType, searchKey);
		model.addAttribute("crumb", new BreadCrumb(6,1,5));
		return "intro/programIntro.app";
	}

	/**
	 * 과학관UCC 게시판 Show
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/programIntro/{id}", method = RequestMethod.GET)
	public String programIntroShow(@PathVariable int id, Model model) {
		model.addAttribute("crumb", new BreadCrumb(6,1,5));
		model.addAttribute("id", id);
		return "intro/programIntroShow.app";
	}
	
	/**
	 * 관장소개 페이지 (기관장 동정 게시판 index 포함)
	 * @param locale
	 * @param model
	 * @param currentPage
	 * @return
	 */
	@RequestMapping(value = "/chief", method = RequestMethod.GET)
	public String chief(Locale locale, Model model,
			@RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			@RequestParam(value = "searchType", required=false) String searchType,
			@RequestParam(value = "searchKey", required=false) String searchKey) {
//		model.addAttribute("currentPage", currentPage);
//		model.addAttribute("type", "chiefHistory");
//		model.addAttribute("isGallary", "false");
		model.addAttribute("crumb", new BreadCrumb(7,1,1));
		return "intro/chief.app";
	}
	
	/**
	 * 관장소개 페이지 (기관장 동정 게시판 Show 포함)
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/chief/{id}", method = RequestMethod.GET)
	public String chiefShow(@PathVariable int id, Model model) {
		model.addAttribute("id", id);
		return "intro/chiefShow.app";
	}
	
	/**
	 * 일반현황 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/history", method = RequestMethod.GET)
	public String history(Model model) {
		model.addAttribute("crumb", new BreadCrumb(7,1,2));
		return "intro/history.app";
	}
	
	/**
	 * 조직및업무 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/organization", method = RequestMethod.GET)
	public String organization(Model model) {
		
		model.addAttribute("crumb", new BreadCrumb(7,1,3));
		return "intro/organization.app";
	}
	
	/**
	 * 찾아오시는길 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/location", method = RequestMethod.GET)
	public String location(Model model,
			@RequestParam(value="scrollspyName", required=false)String scrollspyName) {
		if( scrollspyName != null ) {
        	model.addAttribute("scrollspyName", scrollspyName);
        }
		model.addAttribute("crumb", new BreadCrumb(7,2));
		return "intro/location.app";
	}
	
	/**
	 * 주변정보 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/surround", method = RequestMethod.GET)
	public String surround(Model model) {
		model.addAttribute("crumb", new BreadCrumb(7,3));
		return "intro/surround.app";
	}
	
	/**
	 * 관련사이트 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/familySites", method = RequestMethod.GET)
	public String familySites(Model model) {
		model.addAttribute("crumb", new BreadCrumb(7,4));
		return "intro/familySites.app";
	}
	
	
	/**
	 * 사이트맵 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/siteMap", method = RequestMethod.GET)
	public String siteMap(Model model) {
		model.addAttribute("crumb", new BreadCrumb(7,6));
		return "intro/siteMap.app";
	}
	
	/**
	 * 과학기술자료실 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/scienceArchive", method = RequestMethod.GET)
	public String archieve(Model model) {
		model.addAttribute("crumb", new BreadCrumb(7,5));
		return "intro/scienceArchive.app";
	}
	

	
	/**
	 * 일반현황 / 심벌마크 : jpg 다운로드
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/history/download/symbol_jpg", method = RequestMethod.GET)
	public void downloadHistorySimbolJpg(Model model,
			HttpServletRequest request, HttpServletResponse response) {

		DownloadHelper.downloadFile(request, response, "introduce", "GNSM_symbolmark.jpg","GNSM_symbolmark.jpg");
	}
	
	/**
	 * 일반현황 / 심벌마크 : ai 다운로드
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/history/download/symbol_ai", method = RequestMethod.GET)
	public void downloadHistorySimbolAi(Model model,
			HttpServletRequest request, HttpServletResponse response) {

		DownloadHelper.downloadFile(request, response, "introduce", "GNSM_symbolmark.ai","GNSM_symbolmark.ai");
	}
	
	/**
	 * 일반현황 / 레터마크 다운로드
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/history/download/lettermark", method = RequestMethod.GET)
	public void downloadHistoryLettermark(Model model,
			HttpServletRequest request, HttpServletResponse response) {

		DownloadHelper.downloadFile(request, response, "introduce", "GNSM_lettermark.jpg","GNSM_lettermark.jpg");
	}
	
	/**
	 * 일반현황 / 캐릭터 : jpg 다운로드
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/history/download/character_jpg", method = RequestMethod.GET)
	public void downloadHistoryCharacterJpg(Model model,
			HttpServletRequest request, HttpServletResponse response) {

		DownloadHelper.downloadFile(request, response, "introduce", "GNSM_character.jpg","GNSM_character.jpg");
	}
	
	/**
	 * 일반현황 / 캐릭터 : ai 다운로드
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/history/download/character_ai", method = RequestMethod.GET)
	public void downloadHistoryCharacterAi(Model model,
			HttpServletRequest request, HttpServletResponse response) {

		DownloadHelper.downloadFile(request, response, "introduce", "GNSM_character.ai","GNSM_character.ai");
	}


}
