package kr.go.sciencecenter.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import kr.go.sciencecenter.model.BreadCrumb;
import kr.go.sciencecenter.model.Structure;
import kr.go.sciencecenter.model.StructureUnavailable;
import kr.go.sciencecenter.service.StructureService;
import kr.go.sciencecenter.util.DownloadHelper;

@Controller
@RequestMapping("/structures")
public class StructureController {
	@Autowired
	private StructureService structureService;
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String index(Model model) {
		
		List<Structure> structures = structureService.list();
		model.addAttribute("structures", structures);
		model.addAttribute("crumb", new BreadCrumb(1,5,1));
		return "structures/index.app";
	}
	
	@RequestMapping(value = "/{id}/calendars", method = RequestMethod.GET)
	public String calendars(
			@PathVariable("id") int id,
			Model model) {
		
		Structure structure = structureService.find(id);
		model.addAttribute("structure", structure);
		model.addAttribute("crumb", new BreadCrumb(1,5,1));
		return "structures/calendars.app";
	}
	
	@RequestMapping(value = "/{id}/calendars/dates", method = RequestMethod.GET)
	public Model unavailableDates(
			@PathVariable("id") int id,
			@RequestParam("startDateStr") String startDateStr, 
			@RequestParam("endDateStr") String endDateStr,
			Model model) {
		
		List<StructureUnavailable> unavailables = structureService.listInDates(id, startDateStr, endDateStr);
		model.addAttribute("unavailables", unavailables);
		
		return model;
	}
	
	@RequestMapping(value = "/applicationForm", method = RequestMethod.GET)
	public void applicationFormDownload(HttpServletRequest request,
			HttpServletResponse response){
		
		FileInputStream inputStream;
		OutputStream outStream;
		
		try {
			String fileName = "대관신청서.hwp";
			String mimeType = "application/octet-stream";
			String webappRootPath = request.getSession().getServletContext().getRealPath("/");
	    	String filePath = webappRootPath + "/resources/downloads/structure/" + fileName;
	    	
	    	fileName = URLEncoder.encode(fileName, "utf-8");
	    	
	    	File downloadFile = new File(filePath);
	        
			inputStream = new FileInputStream(downloadFile);
	    	
	    	response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\";");
			response.setHeader("Content-Transfer-Encoding", "binary");
	    	response.setContentType(mimeType);
	        response.setContentLength((int) downloadFile.length());
	    	
	        outStream = response.getOutputStream();
	        
	        byte[] buffer = new byte[4096];
	        int bytesRead = -1;
	 
	        // write bytes read from the input stream into the output stream
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }
	 
	        inputStream.close();
	        outStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
		} 
	}
	
	/**
	 * 어울림홀 기술정보 다운로드
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/download/tech", method = RequestMethod.GET)
	public void downloadTech(Model model,
			HttpServletRequest request, HttpServletResponse response) {

		DownloadHelper.downloadFile(request, response, "structure", "eoullim_2016.zip","eoullim_2016.zip");
	}
	
	/**
	 * 어울림홀 극장도면 다운로드
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/download/floor", method = RequestMethod.GET)
	public void downloadFloor(Model model,
			HttpServletRequest request, HttpServletResponse response) {

		DownloadHelper.downloadFile(request, response, "structure", "어울림홀_극장도면_PDF.zip","PDF.zip");
	}
	
	/**
	 * 어울림홀 좌석배치도 다운로드
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/download/map", method = RequestMethod.GET)
	public void downloadMap(Model model,
			HttpServletRequest request, HttpServletResponse response) {

		DownloadHelper.downloadFile(request, response, "structure", "어울림홀_좌석배치도.xls","seat.xls");
	}
	
	/**
	 * 어울림홀 좌석배치도 다운로드
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/download/manual", method = RequestMethod.GET)
	public void downloadManual(Model model,
			HttpServletRequest request, HttpServletResponse response) {

		DownloadHelper.downloadFile(request, response, "structure", "어울림홀_공연장_안전매뉴얼.pdf","manual.pdf");
	}

}
