package kr.go.sciencecenter.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.time.DateUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.go.sciencecenter.model.DinnerApply;
import kr.go.sciencecenter.model.DinnerApplyTime;
import kr.go.sciencecenter.model.DinnerApplyUser;
import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.service.DinnerApplyService;
import kr.go.sciencecenter.service.DinnerApplyTimeService;
import kr.go.sciencecenter.service.DinnerApplyUserService;

@Controller
@RequestMapping("/dinnerApply")
public class DinnerApplyController {
	
	private static final Logger logger = LoggerFactory.getLogger(DinnerApplyController.class);
	
	@Autowired
	DinnerApplyService dinnerApplyService;
	
	@Autowired
	DinnerApplyTimeService dinnerApplyTimeService;
	
	@Autowired
	DinnerApplyUserService dinnerApplyUserService;
	
	/**
	 * 석식신청 리스트
	 * @param model
	 * @param currentPage
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String index(Model model,
			@RequestParam(value = "page", defaultValue = "1") Integer currentPage) {
		
		Page page = new Page(currentPage, 10, dinnerApplyService.count(this.getTodayDate()));
		page.setPath("/dinnerApply?");
		model.addAttribute("page", page);
		
		List<DinnerApply> dinnerApplies = dinnerApplyService.list(page);
		model.addAttribute("dinnerApplies", dinnerApplies);
		model.addAttribute("dinnerApplyTime", dinnerApplyTimeService.findApplicationTimeMostRecent());
		
		model.addAttribute("today", new Date() );
		
		return "dinnerApply/list.app";  
	}   
	
	/**
	 * 석식신청 Create
	 * @param jsonStr
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	public @ResponseBody Model create(@RequestBody String jsonStr,
						Model model) {
		
		boolean error = false;
		
		JSONObject jsonObj = new JSONObject(jsonStr);
		JSONObject jsonDinner = jsonObj.getJSONObject("dinnerApply");
		
		String department = jsonDinner.getString("department");
		String name = jsonDinner.getString("name");
		
		// get today's Date
		Date today = DateUtils.truncate(new Date(), Calendar.DATE);
		// find the matching department, name and date
		DinnerApply searchedDinner = dinnerApplyService.findDuplicates(new DinnerApply(department, name, today));
		
		// if the SAME PERSON already made at the SAME DAY,
		if ( searchedDinner != null )  {
			error = true;
			model.addAttribute("error_msg", "이미 신청하였습니다.");
		}
				
		// get current Time
		Calendar currentTime = Calendar.getInstance();
		currentTime.set(Calendar.SECOND, 0);
		
		DinnerApplyTime dinnerApplyTime = dinnerApplyTimeService.findApplicationTimeMostRecent();
		// get dinnerApply Begin Time
		Calendar startTime= Calendar.getInstance();
		startTime.setTime(dinnerApplyTime.getApplyBeginTime() );
		startTime.set(Calendar.YEAR, currentTime.get(Calendar.YEAR));
		startTime.set(Calendar.MONTH, currentTime.get(Calendar.MONTH));
		startTime.set(Calendar.DAY_OF_MONTH, currentTime.get(Calendar.DAY_OF_MONTH));
		
		// get dinnerApply End Time		
		Calendar endTime= Calendar.getInstance();
		endTime.setTime(dinnerApplyTime.getApplyEndTime());
		endTime.set(Calendar.YEAR, currentTime.get(Calendar.YEAR));  
		endTime.set(Calendar.MONTH, currentTime.get(Calendar.MONTH));
		endTime.set(Calendar.DAY_OF_MONTH, currentTime.get(Calendar.DAY_OF_MONTH));
		
		// 설정된 신청가능 시간과 현재 시간을 비교
		if( !currentTime.after(startTime) || !currentTime.before(endTime) ) {
			error = true;
			model.addAttribute("error_msg", "신청가능한 시간이 아닙니다.");
		}
		
		// if there is no error,
		// create the dinner apply
		if (!error) {
			DinnerApply dinnerApply = new DinnerApply(department, name);
			dinnerApplyService.create(dinnerApply);
		}
		
		model.addAttribute("error", error);
		return model;
	}
	
	/**
	 * 석식신청 취소
	 * @param id 삭제할 석식신청 id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public String delete(@PathVariable int id, Model model) {
		
		dinnerApplyService.delete(id);
			
		return "redirect:/dinnerApply";
	}
	
	/**
	 * 석식신청 로그인 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model model, HttpServletRequest request) {
		
		// 로그인 세션이 이미 있을 경우,
		// 석식신청 기본 페이지로 redirection
		String dinnerApplySession = (String)request.getSession().getAttribute("dinnerApplySession");
		if( dinnerApplySession != null && dinnerApplySession.equals("loggedIn") ) {
			return "redirect:/dinnerApply";
		}
		model.addAttribute("dinnerApplyUser", new DinnerApplyUser());
		return "dinnerApply/login.app";
	}
	
	/**
	 * 석식신청 로그인 POST
	 * @param dinnerApplyUser
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String loginPost(
			@ModelAttribute("dinnerApplyUser")DinnerApplyUser dinnerApplyUser,
			Model model, HttpServletRequest request) {
		
		String error = "";
		boolean userIdMatch = false;
		boolean userPasswordMatch = false;
		
		// check user id
		DinnerApplyUser recordedUser = dinnerApplyUserService.findByUserId(dinnerApplyUser.getUserId());
		if( recordedUser != null ) {
			userIdMatch = true;
			// check password only if the user id matched,
			userPasswordMatch = 
				new BCryptPasswordEncoder().matches(
						dinnerApplyUser.getPassword(), recordedUser.getPassword());
		}
		
		// if either user id or password does NOT match,
		if( !userIdMatch || !userPasswordMatch ) {
			if( !userIdMatch ) {
				error = "존재하지 않는 사용자입니다.";
			}
			else if ( !userPasswordMatch ) {
				error = "비밀번호가 일치하지 않습니다.";
			}
			model.addAttribute("error", error);
			dinnerApplyUser.setPassword("");
			model.addAttribute("dinnerApplyUser", dinnerApplyUser);
			return "dinnerApply/login.app";
		}
		else {
			// create session
			request.getSession().setAttribute("dinnerApplySession", "loggedIn");
			
		}
		
		return "redirect:/dinnerApply";
	}
	
	/**
	 * 오늘 날짜 구하기 Method
	 * @return
	 */
	private Date getTodayDate() {
		Date today = DateUtils.truncate(new Date(), Calendar.DATE);
		return today;
	}
}
