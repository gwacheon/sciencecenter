package kr.go.sciencecenter.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/markany")
public class MarkanyController {
	private static final Logger logger = LoggerFactory.getLogger(MarkanyController.class);
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String index(Model model, HttpServletRequest request) {
		
		return "jsp/MaSample.markany";
	}
	
	@RequestMapping(value = "/install", method = RequestMethod.GET)
	public String install(Model model, HttpServletRequest request) {
		
		return "jsp/GetInstall.markany";
	}
	
	@RequestMapping(value = "/download", method = RequestMethod.GET)
	public String download(Model model, HttpServletRequest request) {
		
		return "jsp/GetInstall.markany";
	}
	
	@RequestMapping(value = "/Mafndown", method = RequestMethod.GET)
	public String MafndownGet(Model model, HttpServletRequest request) {
		
		return "jsp/Mafndown.markany";
	}
	
	@RequestMapping(value = "/Mafndown", method = RequestMethod.POST)
	public String Mafndown(HttpServletRequest request, HttpServletResponse response) {
//		HttpSession session = request.getSession();
//		
//		String  	strCurrentPath 	= request.getSession().getServletContext().getRealPath("/WEB-INF/views/markany");
//		String  	strDownFolder  	= strCurrentPath + "/fn";
//		String    	strPrtDatDownFolder  	= strCurrentPath + "/bin";
//		
//		String requestFileNameAndPath = request.getParameter("fn");
//		  String requestFileServerIp = request.getParameter("fs");  
//		  String requestDatFile = request.getParameter("prtdat");
//			String id_str=session.getId();
//		  //out.println(id_str);
//		  String filePath = strDownFolder;
//		  int iPrtDatUpdate = 0;
//		  
//			if(requestFileNameAndPath == null)
//			{
//			  if(requestDatFile != null)
//		  	{
//		  	  //filePath = request.getRealPath(strPrtDatDownFolder) + "/"; //파일을 생성할 전체경로
//				  filePath = strPrtDatDownFolder;
//		  	  requestFileNameAndPath = "MaPrintInfoEPSmain.dat";
//				  iPrtDatUpdate = 1;
//		  	}
//			}
//			else
//			{
//			  if(requestFileNameAndPath.length() > 17 || !requestFileNameAndPath.matches("-?\\d+(\\.\\d+)?"))
//			    return "jsp/Mafndown.markany";
//			    
//			    //userid 
//		    String strUserID = (String)session.getAttribute("userid");  
//		  
//		    //MD5, MD4, SHA-1, SHA-256, SHA-512 등 사용 가능.
//		    String strHashID = Hash(strUserID, "MD5");
//		   // requestFileNameAndPath += strHashID + ".matmp";
//		   
//		   String strParamCookie = (String)session.getAttribute("strCookie");
//		   				strParamCookie += requestFileNameAndPath + ".matmp";
//				requestFileNameAndPath = strParamCookie;
//			}
//
//			//kLog(strDownFolder, strUserID);
//
//			InputStream in = null;
//			OutputStream os = null;
//			File file = null;
//			int iFileSize = 0;
//			String strOnlyFileName = new String();
//			
//			try {
//				os = response.getOutputStream();
//				//out.println(filePath);
//				// 파일을 읽어 스트림에 담기
//				file = new File(filePath, requestFileNameAndPath);
//
//				//out.println("111");
//				if (file.isFile()) {
//					//out.println("2222");
//					iFileSize = (int) file.length();
//					strOnlyFileName = file.getName();
//					if (iPrtDatUpdate != 1) {
//						strOnlyFileName += ".matmp";
//					}
//					
//					//out = pageContext.pushBody();
//					//out.clear();
//					response.reset();
//					response.setHeader("Content-Transfer-Encoding", "binary");
//					response.setHeader("Pragma", "no-cache;");
//					response.setHeader("Expires", "-1;");
//					response.setContentType("application/x-msdownload");
//					response.setHeader("Content-Disposition", "attachment; fileName=" + strOnlyFileName + ";");
//					response.setContentLength((int) iFileSize);
//				}
//
//				in = new FileInputStream(file);
//
//				byte b[] = new byte[(int) file.length() + 1];
//				int leng = 0;
//				
//				while ((leng = in.read(b)) > -1) {
//					os.write(b, 0, leng);
//				}
//			} catch (Exception e) {
//				//String[] sArray1 = e.toString().split(" ");
//				//out.println(sArray1[0]); //에러 발생시 메시지 출력
//				//e.printStackTrace();
//				 return "jsp/Mafndown.markany";
//			} finally {
//				if (in != null)
//					try {
//						in.close();
//					} catch (IOException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//				if (os != null)
//					//os.close();
//
//				if (requestDatFile == null && file != null)
//					file.delete();
//			}
			return "jsp/Mafndown.markany";

	}
	
	@RequestMapping(value = "/MaSessionCheck", method = RequestMethod.GET)
	public String MaSessionCheck(Model model, HttpServletRequest request) {
		
		logger.error("/MaSessionCheck Get Method Called.");
		return "jsp/MaSessionCheck.markany";
	}
	
	@RequestMapping(value = "/MaSessionCheck_Install", method = RequestMethod.GET)
	public String MaSessionCheck_Install(Model model, HttpServletRequest request) {
		
		return "jsp/MaSessionCheck_Install.markany";
	}
	
	@RequestMapping(value = "/MaIePopup", method = RequestMethod.GET)
	public String MaIePopup(Model model, HttpServletRequest request) {
		return "jsp/MaIePopup.markany";
	}
	@RequestMapping(value = "/MaSetInstall", method = RequestMethod.POST)
	public String MaSetInstall(Model model, HttpServletRequest request) {
		return "jsp/MaSetInstall.markany";
	}
	
	@RequestMapping(value = "/html/Install_Page_Windows", method = RequestMethod.GET)
	public String Install_Page_Windows(Model model, HttpServletRequest request) {
		
		return "html/Install_Page_Windows.markany";
	}
	
	@RequestMapping(value = "/html/Setup_ePageSafer", method = RequestMethod.GET)
	public void Setup_ePageSafer(HttpServletRequest request,
			HttpServletResponse response) {
		
		FileInputStream inputStream;
		OutputStream outStream;
		
		try {
			String fileName = "Setup_ePageSafer.exe";
			String mimeType = "application/octet-stream";
			String webappRootPath = request.getSession().getServletContext().getRealPath("/");
	    	String filePath = webappRootPath + "/resources/downloads/markany/" + fileName;
	    	
	    	fileName = URLEncoder.encode(fileName, "utf-8");
	    	
	    	File downloadFile = new File(filePath);
	        
			inputStream = new FileInputStream(downloadFile);
	    	
	    	response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\";");
			response.setHeader("Content-Transfer-Encoding", "binary");
	    	response.setContentType(mimeType);
	        response.setContentLength((int) downloadFile.length());
	    	
	        outStream = response.getOutputStream();
	        
	        byte[] buffer = new byte[4096];
	        int bytesRead = -1;
	 
	        // write bytes read from the input stream into the output stream
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }
	 
	        inputStream.close();
	        outStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
		} 
	}	
	
	public static String Hash(String str, String hashtype)
    {
	       java.security.MessageDigest sh = null;
	       String SHA = ""; 
	       try{
	        sh =  java.security.MessageDigest.getInstance(hashtype); 
	        sh.update(str.getBytes()); 
	        byte byteData[] = sh.digest();
	        StringBuffer sb = new StringBuffer(); 
	        for(int i = 0 ; i < byteData.length ; i++){
	         sb.append(Integer.toString((byteData[i]&0xff) + 0x100, 16).substring(1));
	        }
	        SHA = sb.toString();
	        
	       }catch(Exception e){
	        SHA = e.toString();
	       }
	       return SHA;
	    
	    }	
}


