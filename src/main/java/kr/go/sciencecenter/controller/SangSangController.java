package kr.go.sciencecenter.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import kr.go.sciencecenter.model.BreadCrumb;
import kr.go.sciencecenter.model.EquipReserve;
import kr.go.sciencecenter.model.EquipSerial;
import kr.go.sciencecenter.model.EquipUnavailable;
import kr.go.sciencecenter.model.Equipment;
import kr.go.sciencecenter.model.api.Member;
import kr.go.sciencecenter.service.EquipReserveService;
import kr.go.sciencecenter.service.EquipUnavailableService;
import kr.go.sciencecenter.service.EquipmentService;

@Controller
@RequestMapping("/sangsang")
@SessionAttributes(Member.CURRENT_MEMBER_NO)
public class SangSangController {
	private static final Logger logger = LoggerFactory.getLogger(SangSangController.class);

	@Autowired
	EquipmentService equipmentService;

	@Autowired
	EquipReserveService equipReserveService;

	@Autowired
	EquipUnavailableService equipUnavailableService;
	

	@RequestMapping(value = "/list/{type}", method = RequestMethod.GET)
	public String list(Model model, @PathVariable String type) {
		List<Equipment> equipments = equipmentService.listByType(type);
		model.addAttribute("equipments", equipments);
		model.addAttribute("sangsangType", type);
		
		model.addAttribute("crumb", new BreadCrumb(2,2,3));
		return "sangsang/list.app";
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String show(@PathVariable int id
			, HttpServletRequest request
			, Model model) {
		Equipment equipment = equipmentService.find(id);
		
		model.addAttribute("equipment", equipment);
		
		model.addAttribute("crumb", new BreadCrumb(2,2,3));
		
		return "sangsang/show.app";
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String index(Model model) {
		List<Equipment> equipments = equipmentService.list();
		model.addAttribute("equipments", equipments);

		return "sangsang/index.app";
	}

	@RequestMapping(value = "/getEquipReserves", method = RequestMethod.POST)
	public Model getReserves(@RequestBody String jsonStr, HttpServletRequest request, Model model)
			throws ParseException {
		JSONObject obj = new JSONObject(jsonStr);
		
		int equipmentId = obj.getInt("equipmentId");
		int equiSerialId = obj.getInt("equiSerialId");
		
		String dateStr = obj.getString("date");

		EquipSerial equipSerial = equipmentService.findSerial(equiSerialId);
		List<EquipReserve> equipReserves = equipReserveService.list(equiSerialId, dateStr);
		
		List<EquipUnavailable> unavailable = equipUnavailableService.findByIdAndSerial(equipmentId, equiSerialId);
		
		model.addAttribute("serial", equipSerial);
		model.addAttribute("equipReserves", equipReserves);
		model.addAttribute("unavailable", unavailable);
		return model;
	}

	@RequestMapping(value = "/getReservationsInMonth", method = RequestMethod.GET)
	public Model getReservationsInMonth(@RequestParam(value = "equipmentId") int equipmentId,
			@RequestParam(value = "beginDate") String beginDate, @RequestParam(value = "endDate") String endDate,
			HttpServletRequest request, Model model) throws ParseException {

		List<EquipReserve> equipReserves = equipReserveService.getReservationsInMonth(equipmentId, beginDate, endDate);
		model.addAttribute("equipReserves", equipReserves);

		return model;
	}

	@RequestMapping(value = "/getReservationsPerMonth", method = RequestMethod.GET)
	public Model getReservationsPerMonth(@RequestParam(value = "equiSerialId") int equiSerialId,
			@RequestParam(value = "beginDate") String beginDate, @RequestParam(value = "endDate") String endDate,
			HttpServletRequest request, Model model) throws ParseException {

		List<EquipReserve> equipReserves = equipReserveService.getReservationsPerMonth(equiSerialId, beginDate,
				endDate);
		model.addAttribute("equipReserves", equipReserves);
		return model;
	}

	@RequestMapping(value = "/equipReserves", method = RequestMethod.POST)
	public @ResponseBody Model createEquiReserves(@RequestBody String jsonStr
			, @ModelAttribute(Member.CURRENT_MEMBER_NO) String memberNo
			, Model model) {
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		List<EquipReserve> equipReserves = new ArrayList<EquipReserve>();

		JSONObject obj = new JSONObject(jsonStr.trim());
		Iterator<?> keys = obj.keys();
		while (keys.hasNext()) {
			String key = (String) keys.next();
			if (obj.get(key) instanceof JSONArray) {
				EquipSerial serial = equipmentService.findSerial(Integer.parseInt(key));
				JSONArray times = (JSONArray) obj.get(key);
				for (int i = 0; i < times.length(); i++) {
					JSONObject jsonObj = times.getJSONObject(i);

					EquipReserve reserve = new EquipReserve();
					reserve.setEquiSerialId(serial.getId());
					reserve.setEquipmentId(serial.getEquipmentId());
					reserve.setMemberNo(memberNo);
					try {
						reserve.setBeginTime(formatter.parse(jsonObj.getString("date") + " " + jsonObj.getString("time")));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					equipReserves.add(reserve);
				}
			}
		}
		
		logger.error("member_no : "+memberNo);
		equipReserveService.bulkInsert(equipReserves);
		model.addAttribute("equipReserves", equipReserves);
		model.addAttribute("result", "success");
		return model;
	}

	@RequestMapping(value = "/equipUnavailable", method = RequestMethod.POST)
	public Model createEquipUnavailable(@RequestBody String jsonStr, HttpServletRequest request, Model model)
			throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		JSONObject obj = new JSONObject(jsonStr);

		int equipmentId = obj.getInt("equipmentId");
		int equiSerialId = obj.getInt("equiSerialId");

		List<EquipUnavailable> list = new ArrayList<EquipUnavailable>();

		for (int i = 0; i < obj.getJSONArray("days").length(); i++) {
			EquipUnavailable item = new EquipUnavailable();
			item.setEquiSerialId(equiSerialId);
			item.setEquipmentId(equipmentId);
			item.setBeginTime(formatter.parse(obj.getJSONArray("days").getString(i)));

			list.add(item);
		}

		equipUnavailableService.bulkInsert(list);

		return model;
	}
	
	public String getCurrentMemberNo(HttpServletRequest request) {
		return (String)request.getSession().getAttribute(Member.CURRENT_MEMBER_NO);
	}
}
