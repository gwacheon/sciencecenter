package kr.go.sciencecenter.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.go.sciencecenter.model.Board;
import kr.go.sciencecenter.model.BoardType;
import kr.go.sciencecenter.model.BreadCrumb;
import kr.go.sciencecenter.model.MainEvent;
import kr.go.sciencecenter.model.SupportEvaluation;
import kr.go.sciencecenter.model.api.Member;
import kr.go.sciencecenter.service.BoardService;
import kr.go.sciencecenter.service.PolicyService;
import kr.go.sciencecenter.service.SupportEvaluationService;
import kr.go.sciencecenter.util.DownloadHelper;

/**
 * 고객소통 카테고리 Controller
 * @author Prompt Technology
 *
 */
@Controller
@RequestMapping("/communication")
public class CommunicationController {
	
	@Autowired
	private BoardService boardService;
	
	@Autowired
	private SupportEvaluationService supportEvaluationService;	
	
	@Autowired
	private PolicyService policyService;
	
	
	/**
	 * 고객서비스 헌장 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/charter", method = RequestMethod.GET)
	public String charter(Model model) {
		model.addAttribute("crumb", new BreadCrumb(6,2,1));
		return "communication/charter.app";
	}
	
	/**
	 * 실천의지 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/practicalWill", method = RequestMethod.GET)
	public String practicalWill(Model model) {
		model.addAttribute("crumb", new BreadCrumb(6,2,2));
		return "communication/practicalWill.app";
	}
	
	/**
	 * 평가결과 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/evalResult", method = RequestMethod.GET)
	public String evalResult(Model model) {
		List<SupportEvaluation> supportEvaluations = supportEvaluationService.list();
		model.addAttribute("supportEvaluations", supportEvaluations);
		model.addAttribute("crumb", new BreadCrumb(6,2,3));
		return "communication/evalResult.app";
	}
	
	/**
	 * 연간방문객 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/visitorsPerYear", method = RequestMethod.GET)
	public String visitorsPerYear(Model model) {
		model.addAttribute("crumb", new BreadCrumb(6,2,4));
		return "communication/visitorsPerYear.app";
	}
	
	
	/**
	 * 분실물 보관소 안내 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/lostPropertyGuide", method = RequestMethod.GET)
	public String lostProperty(Model model) {
		model.addAttribute("crumb", new BreadCrumb(6,2,5));
		return "communication/lostPropertyGuide.app";
	}
	
	
	/**
	 * 분실물 목록 게시판 Index
	 * @param model
	 * @param currentPage
	 * @param searchType
	 * @param searchKey
	 * @return
	 */
	@RequestMapping(value = "/lostProperty", method = RequestMethod.GET)
	public String lostProperty(Model model,
			@RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			@RequestParam(value = "searchType", required=false) String searchType,
			@RequestParam(value = "searchKey", required=false) String searchKey) {
		
		Board.getBoards(model, boardService, "lostProperty", currentPage, searchType, searchKey);
		model.addAttribute("crumb", new BreadCrumb(6,2,5));
		return "communication/lostProperty.app";
	}
	
	
	/**
	 * 분실물 목록 게시판 show
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/lostProperty/{id}", method = RequestMethod.GET)
	public String lostPropertyShow(@PathVariable int id, Model model) {
		model.addAttribute("id", id);
		model.addAttribute("crumb", new BreadCrumb(6,2,5));
		return "communication/lostPropertyShow.app";
	}
	
	/**
	 * 안전관리 페이지 : 관람객 대피 tab  & 안전사고수칙 tab
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/safety", method = RequestMethod.GET)
	public String safety(Model model,
			@RequestParam(value="scrollspyName", required=false)String scrollspyName) {
		if( scrollspyName != null ) {
        	model.addAttribute("scrollspyName", scrollspyName);
        }
		model.addAttribute("crumb", new BreadCrumb(6,3));
		return "communication/safety.app";
	}
	
	/**
	 * 자주묻는질문(FAQ) 게시판 index
	 * @param model
	 * @param type
	 * @param currentPage
	 * @param searchType
	 * @param searchKey
	 * @return
	 */
	@RequestMapping(value = "/faq/{type}", method = RequestMethod.GET)
	public String faq(Model model, 
			@PathVariable String type, 
			@RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			@RequestParam(value = "searchType", required=false) String searchType,
			@RequestParam(value = "searchKey", required=false) String searchKey) {
		model.addAttribute("type", type);
		Board.getBoards(model, boardService, type, currentPage, searchType, searchKey);
		model.addAttribute("crumb", new BreadCrumb(6,4,4));
		return "communication/faq.app";
	}  
	
	/**
	 * 자주묻는질문(FAQ) 게시판 Show
	 * @param type
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/faq/{type}/{id}", method = RequestMethod.GET)
	public String faqShow(@PathVariable String type, 
			@PathVariable int id, Model model) {
		model.addAttribute("boardType", BoardType.BOARD_TYPES.get(type));
		model.addAttribute("id", id);
		model.addAttribute("crumb", new BreadCrumb(6,4,4));
		return "communication/faqShow.app";
	}
	
	/**
	 * 일반자료실 게시판 Index
	 * @param model
	 * @param currentPage
	 * @param searchType
	 * @param searchKey
	 * @return
	 */
	@RequestMapping(value = "/normalLibrary", method = RequestMethod.GET)
	public String normalLibrary(Model model, @RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			@RequestParam(value = "searchType", required=false) String searchType,
			@RequestParam(value = "searchKey", required=false) String searchKey) {
		model.addAttribute("crumb", new BreadCrumb(6,4,1));
		Board.getBoards(model, boardService, "normalLibrary", currentPage, searchType, searchKey);
		return "communication/normalLibrary.app";
	}

	/**
	 * 일반자료실 게시판 Show
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/normalLibrary/{id}", method = RequestMethod.GET)
	public String normalLibraryShow(@PathVariable int id, Model model) {
		model.addAttribute("id", id);
		model.addAttribute("crumb", new BreadCrumb(6,4,1));
		return "communication/normalLibraryShow.app";
	}
	
	/**
	 * 관련규정자료실 게시판 Index
	 * @param model
	 * @param currentPage
	 * @param searchType
	 * @param searchKey
	 * @return
	 */
	@RequestMapping(value = "/roleLibrary", method = RequestMethod.GET)
	public String roleLibrary(Model model, @RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			@RequestParam(value = "searchType", required=false) String searchType,
			@RequestParam(value = "searchKey", required=false) String searchKey) {

		Board.getBoards(model, boardService, "roleLibrary", currentPage, searchType, searchKey);
		model.addAttribute("crumb", new BreadCrumb(6,4,2));
		return "communication/roleLibrary.app";
	}

	/**
	 * 관련규정자료실 게시판 Show
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/roleLibrary/{id}", method = RequestMethod.GET)
	public String roleLibraryShow(@PathVariable int id, Model model) {
		model.addAttribute("id", id);
		model.addAttribute("crumb", new BreadCrumb(6,4,2));
		return "communication/roleLibraryShow.app";
	}
	
	/**
	 * 과학컬럼 게시판 Index
	 * @param model
	 * @param currentPage
	 * @param searchType
	 * @param searchKey
	 * @return
	 */
	@RequestMapping(value = "/scienceColumn", method = RequestMethod.GET)
	public String scienceColumn(Model model, @RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			@RequestParam(value = "searchType", required=false) String searchType,
			@RequestParam(value = "searchKey", required=false) String searchKey) {

		Board.getBoards(model, boardService, "scienceColumn", currentPage, searchType, searchKey);
		model.addAttribute("crumb", new BreadCrumb(6,4,3));
		return "communication/scienceColumn.app";
	}

	/**
	 * 의견수렴 게시판 Index
	 * @param model
	 * @param currentPage
	 * @param searchType
	 * @param searchKey
	 * @return
	 */
	@RequestMapping(value = "/opinions", method = RequestMethod.GET)
	public String opinions(Model model, @RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			@RequestParam(value = "searchType", required=false) String searchType,
			@RequestParam(value = "searchKey", required=false) String searchKey) {
		
		Board.getBoards(model, boardService, "opinions", currentPage, searchType, searchKey);
		model.addAttribute("crumb", new BreadCrumb(6,4,5));
		return "communication/opinions/opinions.app";
	}
	
	/**
	 * 의견수렴 게시판 Show
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/opinions/{id}", method=RequestMethod.GET)
	public String opinionsShow(@PathVariable int id,Model model, HttpServletRequest request){
		
		BoardType boardType = BoardType.BOARD_TYPES.get("opinions");
		model.addAttribute("id", id);
		model.addAttribute("boardType", boardType);
		
		String memberNo = (String)request.getSession().getAttribute(Member.CURRENT_MEMBER_NO);
		if( memberNo != null && !memberNo.equals("")) {
			
		}
		
		
		model.addAttribute("crumb", new BreadCrumb(6,4,5));
		return "communication/opinions/opinionsShow.app"; 
	}
	
	/**
	 * 의견수렴 게시판 글 작성
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/opinions/private/new", method = RequestMethod.GET)
	public String newBoard(HttpServletRequest request, Model model) {
		
		Board board = new Board();
		board.setBoardType("opinions");
		model.addAttribute("board", board);
		
		BoardType boardType = BoardType.BOARD_TYPES.get("opinions");
		model.addAttribute("boardType", boardType);
		model.addAttribute("crumb", new BreadCrumb(6,4,5));
		return "communication/opinions/new.app";
		
	}
	
	/**
	 * 의견수렴 게시판 글 생성
	 * @param board
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/opinions/private/create", method = RequestMethod.POST)
	public String create(@ModelAttribute("board") Board board, Model model, HttpServletRequest request) {
		
		// 일반사용자가 작성한 경우이므로,
		// 일반 Member 번호를 넣어주고,
		String memberNo = getCurrentMemberNo(request);
		if( memberNo != null ) {
			board.setMemberNo(memberNo);
			board.setWrittenByAdmin(false);
		}
		// Member No가 비어있을 경우에는, 관리자로 가정한다.
		else {
			board.setWrittenByAdmin(true);
		}
		boardService.create(board);
		return "redirect:/communication/opinions";					
	}
	
	/**
	 * 의견수렴 게시판 글 수정페이지
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/opinions/private/{id}/edit", method = RequestMethod.GET)
	public String editOpinionsBoard(@PathVariable("id") int id,
			HttpServletRequest request, Model model) {
		
		Board board = boardService.find(id);
		model.addAttribute("board", board);
		
		BoardType boardType = BoardType.BOARD_TYPES.get("opinions");
		model.addAttribute("boardType", boardType);
		model.addAttribute("crumb", new BreadCrumb(6,4,5));
		return "communication/opinions/edit.app";
		
	}
	
	/**
	 * 의견수렴 게시판 글 업데이트
	 * @param board
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/opinions/private/update", method = RequestMethod.POST)
	public String updateOpinionsBoard(@ModelAttribute("board") Board board, Model model, HttpServletRequest request) {
		
		// 일반사용자가 작성한 경우이므로,
		// 일반 Member 번호를 넣어주고,
		String memberNo = getCurrentMemberNo(request);
		if( memberNo != null ) {
			board.setMemberNo(memberNo);
			board.setWrittenByAdmin(false);
		}
		// Member No가 비어있을 경우에는, 관리자로 가정한다.
		else {
			board.setWrittenByAdmin(true);
		}
		boardService.update(board);
		return "redirect:/communication/opinions";					
	}
	
	/**
	 * 의견수렴 자신의 게시판 글 삭제
	 * @param model
	 * @param jsonStr
	 * @return
	 */
	@RequestMapping(value = "/opinions/private/delete", method = RequestMethod.POST)
	public @ResponseBody Model deleteOpinionsBoard(Model model, @RequestBody String jsonStr){
		
		JSONObject jsonObj = new JSONObject(jsonStr);
		int boardId = jsonObj.getInt("id");
		boardService.delete(boardId);
		
		return model;
	}
	
	
	
	/**
	 * 소셜네트워크 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/social", method = RequestMethod.GET)
	public String social(Model model) {
		model.addAttribute("crumb", new BreadCrumb(6,4,7));
		return "communication/social.app";
	}
	
	/**
	 * 민원신청 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/complaintApply", method = RequestMethod.GET)
	public String compaintApply(Model model) {
		model.addAttribute("crumb", new BreadCrumb(6,5,1));
		return "communication/complaintApply.app";
	}
	
	/**
	 * 나의민원 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/myComplaint", method = RequestMethod.GET)
	public String myComplaint(Model model) {
		model.addAttribute("crumb", new BreadCrumb(6,5,2));
		return "communication/myComplaint.app";
	}
	
	
	/**
	 * 이용약관 및 개인정보정책 안내 페이지
	 * @param model
	 * @param type
	 * @return
	 */
	@RequestMapping(value = "/policies", method = RequestMethod.GET)
	public String policies(Model model,
			@RequestParam(value="type",required=false, defaultValue = "policy") String type ) {
		
		model.addAttribute("type", type);
		model.addAttribute("policies", policyService.list());
		model.addAttribute("prevPolicy", policyService.listPrevious("policy"));
		model.addAttribute("prevPrivacy", policyService.listPrevious("privacy"));
		return "communication/policies.app";
	}
	
	
	/**
	 * 개인정보 처리방침 별지 제8호 서식 다운로드
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/privacy/download/form8", method = RequestMethod.GET)
	public void downloadPrivacyForm08(Model model,
			HttpServletRequest request, HttpServletResponse response) {

		DownloadHelper.downloadFile(request, response, "privacy", "form08.hwp","form08.hwp");
	}
	
	/**
	 * 개인정보 처리방침 별지 제11호 서식 다운로드
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/privacy/download/form11", method = RequestMethod.GET)
	public void downloadPrivacyForm11(Model model,
			HttpServletRequest request, HttpServletResponse response) {

		DownloadHelper.downloadFile(request, response, "privacy", "form11.hwp","form11.hwp");
	}
	
	private String getCurrentMemberNo(HttpServletRequest request) {
		return (String)request.getSession().getAttribute(Member.CURRENT_MEMBER_NO);
	}
}

