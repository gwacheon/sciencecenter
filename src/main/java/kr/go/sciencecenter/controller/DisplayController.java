package kr.go.sciencecenter.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import kr.go.sciencecenter.model.Board;
import kr.go.sciencecenter.model.BreadCrumb;
import kr.go.sciencecenter.model.SangSangScheduleTitle;
import kr.go.sciencecenter.service.BoardService;
import kr.go.sciencecenter.service.SangSangService;
import kr.go.sciencecenter.util.DownloadHelper;

/**
 * 전시관 카테고리 Controller
 * @author Prompt Technology
 *
 */
@Controller
@RequestMapping("/display")
public class DisplayController {
	@Autowired
	SangSangService sangSangService;
	
	@Autowired
	BoardService boardService;
	
	/**
	 * 상설전시관 - 기초과학관 페이지
	 * @param model
	 * @return
	 */
    @RequestMapping(value="/mainBuilding/basicScience", method=RequestMethod.GET)
    public String basicScience(Model model){
    	model.addAttribute("crumb", new BreadCrumb(2,1,2));
    	return "display/mainBuilding/basicScience.app";
    }
    
    /**
	 * 상설전시관 - 어린이탐구체험관
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/mainBuilding/scienceParkForKids", method=RequestMethod.GET)
	public String scienceParkForKids(Model model){
		model.addAttribute("crumb", new BreadCrumb(2,1,1));
		return "display/mainBuilding/scienceParkForKids.app";
	}
    
    /**
     * 상설전시관 - 첨단기술관 1 페이지
     * @param model
     * @return
     */
    @RequestMapping(value="/mainBuilding/advancedTechnology1", method=RequestMethod.GET)
	public String advancedTechnology1(Model model){
    	model.addAttribute("crumb", new BreadCrumb(2,1,5));
		return "display/mainBuilding/advancedTechnology1.app";
	}
  
    /**
     * 상설전시관 - 첨단기술관 2 페이지
     * @param model
     * @return
     */
	@RequestMapping(value="/mainBuilding/advancedTechnology2", method=RequestMethod.GET)
	public String advancedTechnology2(Model model){
		model.addAttribute("crumb", new BreadCrumb(2,1,6));
		return "display/mainBuilding/advancedTechnology2.app";
	}
  
	/**
	 * 상설전시관 - 전통과학관
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/mainBuilding/traditionalSciences", method=RequestMethod.GET)
	public String traditionalSciences(Model model){
		model.addAttribute("crumb", new BreadCrumb(2,1,4));
		return "display/mainBuilding/traditionalSciences.app";
	}
	
	/**
	 * 상설전시관 - 자연사관
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/mainBuilding/naturalHistory", method=RequestMethod.GET)
	public String naturalHistory(Model model){
		model.addAttribute("crumb", new BreadCrumb(2,1,3));
		return "display/mainBuilding/naturalHistory.app";
	}
	
  
	/**
	 * 프론티어창작관 - 명예의전당
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/frontier/hallOfFame", method=RequestMethod.GET)
	public String hallOfFame(Model model){
		model.addAttribute("crumb", new BreadCrumb(2,2,2));
		return "display/frontier/hallOfFame.app";
	}
  
	/**
	 * 프론티어창작관 - 노벨상과나
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/frontier/nobelPrizeAndI", method=RequestMethod.GET)
	public String nobelPrizeAndI(Model model){
		model.addAttribute("crumb", new BreadCrumb(2,2,1));
		return "display/frontier/nobelPrizeAndI.app";  
	}
  
	
	/**
	 * 프론티어창작관 - 무한상상실
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/frontier/infiniteImagination", method=RequestMethod.GET)
	public String infiniteImagination(Model model){
		model.addAttribute("crumb", new BreadCrumb(2,2,3));
		return "display/frontier/infiniteImagination.app";  
	}
  
	/**
	 * 프론티어창작관 - 메이커랜드(키즈메이커스튜디오)
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/frontier/kidsMakerStudio", method=RequestMethod.GET)
	public String kidsMakerStudio(Model model){
		SangSangScheduleTitle sangSangSchedule = sangSangService.findWithSchedules();
		model.addAttribute("sangSangSchedule", sangSangSchedule);
		model.addAttribute("crumb", new BreadCrumb(2,2,3));
		return "display/frontier/kidsMakerStudio.app";  
	}
	
	/**
	 * 프론티어창작관 - 메이커랜드(프리마켓)
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/frontier/freeMarket", method=RequestMethod.GET)
	public String freeMarket(Model model){
		model.addAttribute("crumb", new BreadCrumb(2,2,3));
		return "display/frontier/freeMarket.app";  
	}
	
	/**
	 * 프론티어창작관 - 메이커랜드(패밀리S랜드)
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/frontier/familySLand", method=RequestMethod.GET)
	public String familySLand(Model model){
		model.addAttribute("crumb", new BreadCrumb(2,2,3));
		return "display/frontier/familySLand.app";  
	}
	
	/**
	 * 프론티어창작관 - 메이커랜드(메이커인큐베이터)
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/frontier/incubator", method=RequestMethod.GET)
	public String incubator (Model model){
		model.addAttribute("crumb", new BreadCrumb(2,2,5));
		return "display/frontier/incubator.app";  
	}
	
	/**
	 * 천문우주관 - 천체투영관
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/planetarium", method=RequestMethod.GET)
	public String planetarium(Model model){
		model.addAttribute("crumb", new BreadCrumb(2,3,1));
		return "display/planetarium/planetarium.app"; 
	}
  
	/**
	 * 천문우주관 - 천체관측소
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/planetarium/observation", method=RequestMethod.GET)
	public String observation(Model model){
		model.addAttribute("crumb", new BreadCrumb(2,3,2));
		return "display/planetarium/observation.app";
	}
	
	/**
	 * 천문우주관 - 스페이스월드
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/planetarium/spaceWorld", method=RequestMethod.GET)
	public String spaceWorld(Model model){
		model.addAttribute("crumb", new BreadCrumb(2,3,3));
		return "display/planetarium/spaceWorld.app";
	}
	
	/**
	 * 옥외생태관 - 곤충생태관
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/outdoorEcological/insectarium", method=RequestMethod.GET)
	public String insectarium(Model model){
		model.addAttribute("crumb", new BreadCrumb(2,4,1));
		return "display/outdoorEcological/insectarium.app";
	}
	
	/**
	 * 옥외생태관 - 생태공원
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/outdoorEcological/ecoPark", method=RequestMethod.GET)
	public String ecoPark(Model model){
		model.addAttribute("crumb", new BreadCrumb(2,4,2));
		return "display/outdoorEcological/ecoPark.app";
	}
	
	/**
	 * 옥외생태관 - 공룡역사광장
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/outdoorEcological/dinosaurAndHistory", method=RequestMethod.GET)
	public String dinosaurAndHistory(Model model){
		model.addAttribute("crumb", new BreadCrumb(2,4,3));
		return "display/outdoorEcological/dinosaurAndHistory.app";
	}
	
	/**
	 * 옥외생태관 - 옥외전시장
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/outdoorEcological/outdoor", method=RequestMethod.GET)
	public String outdoor(Model model){
		model.addAttribute("crumb", new BreadCrumb(2,4,4));
		return "display/outdoorEcological/outdoor.app";
	}
	
	/**
	 * 옥외생태관 - 과학문화광장
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/outdoorEcological/cultureSquare", method=RequestMethod.GET)
	public String cultureSquare(Model model){
		
		return "display/outdoorEcological/cultureSquare.app";
	}
	
	/**
	 * 전시관람코스 파일 다운로드
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value="/download/course", method=RequestMethod.GET)
	public void downloadCourse(Model model,
			HttpServletRequest request, HttpServletResponse response){
		
		DownloadHelper.downloadFile(request, response, "course", "전시관람코스.hwp","course.hwp");
	}
	
	/**
	 * 전시관체험 - 전시관실험실(둥지과학놀이)
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/experience/doongjiScience", method=RequestMethod.GET)
	public String doongjiScience(Model model){
		
		return "display/experience/doongjiScience.app";
	}
	
	/**
	 * 전시관체험 - 천문우주프로그램
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/experience/infiniteProgram", method=RequestMethod.GET)
	public String infiniteProgram(Model model){
		
		return "display/experience/infiniteProgram.app";
	}
	
	
	@RequestMapping(value="/exhibitionReview", method=RequestMethod.GET)
	public String exhibitionReview(Model model,
			@RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			@RequestParam(value = "searchType", required=false) String searchType,
			@RequestParam(value = "searchKey", required=false) String searchKey){
		
		Board.getBoards(model, boardService, "exhibitionReview", currentPage, searchType, searchKey);
		return "display/exhibitionReview.app";
	}
	
	@RequestMapping(value="/exhibitionReview/{id}", method=RequestMethod.GET)
	public String exhibitionReviewShow(@PathVariable int id, Model model){
		
		model.addAttribute("id", id);
		return "display/exhibitionReviewShow.app";
	}
	
} 
