package kr.go.sciencecenter.controller.admin;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.model.Program;
import kr.go.sciencecenter.model.ProgramGroup;
import kr.go.sciencecenter.model.ProgramWday;
import kr.go.sciencecenter.service.ProgramGroupService;
import kr.go.sciencecenter.service.ProgramService;

@Controller
@RequestMapping("/admin/program_groups/{groupId}/programs")
public class AdminProgramController {
	private static final Logger logger = LoggerFactory.getLogger(AdminProgramController.class);
	
	@Autowired
	ProgramGroupService groupService;
	
	@Autowired
	ProgramService programService;
	
	@RequestMapping(value="", method = RequestMethod.GET)
	public String index(Model model
			, @PathVariable int groupId
			, @RequestParam(value="page", required=false) Integer currentPage){
		
		String path = "/admin/program_groups/" + groupId + "/programs?";
		
		if(currentPage == null){
			currentPage = 1;
		}
		
		ProgramGroup group = groupService.find(groupId);
		model.addAttribute("group", group);
		
		Page page = new Page(currentPage, 10, programService.count(group));
		page.setPath(path);
		model.addAttribute("page", page);
		
		model.addAttribute("programs", programService.list(group, page));
		
		return "programs/index.admin";
		
	}
	
	@RequestMapping(value="/new", method = RequestMethod.GET)
	public String newProgram(Model model
			, @PathVariable int groupId){
		ProgramGroup group = groupService.find(groupId);
		model.addAttribute("group", group);
		
		Program program = new Program();
		program.setGroupId(groupId);
		model.addAttribute("program", program);
		model.addAttribute("categories", Program.LOCATION_MAP);
		model.addAttribute("additionalInfos", Program.ADDITIONAL_INFOS);
		
		return "programs/new.admin";
	}
	
	@RequestMapping(value="", method = RequestMethod.POST)
	public String create(@ModelAttribute("program") Program program
			, @PathVariable int groupId
			, BindingResult result
			, Model model){
		program.setGroupId(groupId);
		
		if(!result.hasErrors()){
			programService.create(program);
		}else{
			logger.error("Bind Err : " + result.toString());
		}
		
		
		return "redirect:/admin/program_groups/" + groupId + "/programs";
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public String show(@PathVariable int groupId, @PathVariable int id, Model model){
		model.addAttribute("program", programService.find(id));
		return "redirect:/admin/program_groups/" + groupId + "/programs";
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.DELETE)
	public String delete(@PathVariable int groupId, @PathVariable int id, Model model){
		programService.delete(id);
		return "redirect:/admin/program_groups/" + groupId + "/programs";
	}
	
	@RequestMapping(value="{id}/edit", method = RequestMethod.GET)
	public String edit(@PathVariable int groupId, @PathVariable int id, Model model){
		model.addAttribute("program", programService.find(id));
		model.addAttribute("categories", Program.LOCATION_MAP);
		model.addAttribute("additionalInfos", Program.ADDITIONAL_INFOS);
				
		return "programs/edit.admin";
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.POST)
	public String update(@PathVariable int groupId
			, @ModelAttribute("program") Program program
			, BindingResult result
			, Model model
			){
		
		if(!result.hasErrors()){
			programService.update(program);
		}else{
			logger.error("Err : " + result.toString());
		}
		
		return "redirect:/admin/program_groups/" + groupId + "/programs";
	}
	
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
}
