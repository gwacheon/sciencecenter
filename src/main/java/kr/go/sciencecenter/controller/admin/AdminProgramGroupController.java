package kr.go.sciencecenter.controller.admin;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.model.ProgramGroup;
import kr.go.sciencecenter.service.ProgramGroupService;

@Controller
@RequestMapping("/admin/program_groups")
public class AdminProgramGroupController {
	private static final Logger logger = LoggerFactory.getLogger(AdminProgramGroupController.class);
	
	@Autowired
	ProgramGroupService groupService;
	
	@RequestMapping(value="", method = RequestMethod.GET)
	public String index(Model model
			, @RequestParam(value="page", required=false) Integer currentPage){
		
		String path = "/admin/program_groups?";
		
		if(currentPage == null){
			currentPage = 1;
		}
		
		Page page = new Page(currentPage, 10, groupService.count());
		page.setPath(path);
		model.addAttribute("page", page);
		
		model.addAttribute("groups", groupService.list(page));
		
		return "program_groups/index.admin";
	}
	
	@RequestMapping(value="/new", method = RequestMethod.GET)
	public String newGroup(Model model){
		model.addAttribute("group", new ProgramGroup());
		model.addAttribute("categories", ProgramGroup.CATEGORIES);
		return "program_groups/new.admin";
	}
	
	@RequestMapping(value="{id}/edit", method = RequestMethod.GET)
	public String edit(@PathVariable int id, Model model){
		model.addAttribute("group", groupService.find(id));
		model.addAttribute("categories", ProgramGroup.CATEGORIES);
		return "program_groups/edit.admin";
	}
	
	@RequestMapping(value="", method = RequestMethod.POST)
	public String create(@ModelAttribute("group") ProgramGroup group, Model model){
		int result = groupService.create(group);
		
		return "redirect:/admin/program_groups";
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.DELETE)
	public String delete(@PathVariable int id, Model model){
		groupService.delete(id);
		return "redirect:/admin/program_groups";
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.PUT)
	public String update(@ModelAttribute("group") ProgramGroup group, Model model){
		groupService.update(group);
		return "redirect:/admin/program_groups";
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
}
