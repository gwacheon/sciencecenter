package kr.go.sciencecenter.controller.admin;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.go.sciencecenter.model.Article;
import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.model.Reply;
import kr.go.sciencecenter.model.Selection;
import kr.go.sciencecenter.model.Survey;
import kr.go.sciencecenter.service.SurveyService;

@Controller
@RequestMapping("/admin/surveys")
public class AdminSurveyController {
	private static final Logger logger = LoggerFactory.getLogger(AdminSurveyController.class);
	
	@Autowired
	private SurveyService surveyService;
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String index(@RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			Model model, HttpServletRequest request) {
		
		// pagination setting
		String requestURI = request.getRequestURI();
		Page page = new Page(currentPage, 10, surveyService.countAll());
		String path = requestURI + "?";
		page.setPath(path);
		model.addAttribute("page", page);
		
		List<Survey> surveys = surveyService.list(page);
		Date today = new Date();
		for (Survey survey : surveys) {
			if(survey.getBeginTime().compareTo(today) < 0 && survey.getEndTime().compareTo(today) > 0){
				survey.setAvailableType(1); //진행중
			}else if(survey.getBeginTime().compareTo(today) > 0){
				survey.setAvailableType(2); //예정
			}else{
				survey.setAvailableType(0); //종료
			}
		}
		model.addAttribute("surveys", surveys);

		return "survey/index.adm";
	}
	
	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public String newSurvey(Model model) {
		model.addAttribute("survey", new Survey());

		return "survey/new.adm";
	}
	
	@RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
	public String edit(@PathVariable int id, Model model) {
		model.addAttribute("survey", surveyService.find(id));

		return "survey/edit.adm";
	}
	
	@RequestMapping(value="", method = RequestMethod.POST)
	public String create(@ModelAttribute("survey") Survey survey
			, BindingResult result
			, Model model){
		surveyService.create(survey);
		
		return "redirect:/admin/surveys/" + survey.getId() + "/articles";
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.PUT)
	public String update(@PathVariable int id
			, @ModelAttribute("survey") Survey survey
			, BindingResult result
			, Model model){
		surveyService.update(survey);
		
		return "redirect:/admin/surveys/" + survey.getId() + "/articles";
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.DELETE)
	public String delete(@PathVariable int id
			, Model model){
		
		surveyService.delete(id);
		
		return "redirect:/admin/surveys";
	}
	
	@RequestMapping(value="/{id}/articles", method = RequestMethod.GET)
	public String articles(@PathVariable int id
			, Model model){
		Survey survey = surveyService.findWithArticles(id);
		
		model.addAttribute("survey", survey);
		
		return "survey/articles.adm";
	}
	
	@RequestMapping(value="/{id}/articles/sort", method = RequestMethod.POST)
	public @ResponseBody Model articleSort(@RequestBody String jsonStr
			, @PathVariable int id
			, Model model){
		
		JSONObject jsonObj = new JSONObject(jsonStr);
		JSONArray jsonArticleIds = jsonObj.getJSONArray("ids");
		int [] articleIds = new int [jsonArticleIds.length()];
		
		for (int i = 0; i < jsonArticleIds.length(); ++i) {
			articleIds[i] = jsonArticleIds.optInt(i);
		}
		
		surveyService.updateArticleSort(id, articleIds);
		
		return model;
	}
	
	@RequestMapping(value="/{id}/addArticles", method = RequestMethod.POST)
	public @ResponseBody Model addArticles(@RequestBody String jsonStr
			, @PathVariable int id
			, Model model){
		JSONObject jObj = new JSONObject(jsonStr);
		Article article = new Article(jObj.getJSONObject("article"));
		article.setSurveyId(id);
		
		if(jObj.has("selections")){
			JSONArray jsonSelections = jObj.getJSONArray("selections");
			if(jsonSelections.length() > 0){
				List<Selection> selections = new ArrayList<Selection>();
				
				for(int i = 0; i < jsonSelections.length(); i++){
					selections.add(new Selection(jsonSelections.getJSONObject(i)));
				}
				
				article.setSelections(selections);
			}
		}
		
		surveyService.createArticle(article);
		
		model.addAttribute("article", article);
		
		return model;
	}
	
	@RequestMapping(value="/deleteArticle/{id}", method = RequestMethod.DELETE)
	public @ResponseBody Model deleteArticle(@RequestBody String jsonStr, 
			Model model, @PathVariable int id){
		surveyService.deleteArticle(id);
		return model;
	}
	
	@RequestMapping(value = "/{id}/replies", method = RequestMethod.GET)
	public String replies(@PathVariable int id
			, @RequestParam(value = "page", defaultValue = "1") Integer currentPage
			, Model model
			, HttpServletRequest request) {
		
		String requestURI = request.getRequestURI();
		String path = requestURI + "?";
		int replyCount = surveyService.count(id);
		Page page = new Page(currentPage, 10, replyCount);
		page.setPath(path);
		model.addAttribute("page", page);
		
		model.addAttribute("survey", surveyService.find(id));
		List<Reply> replies = surveyService.replies(id, page);
		model.addAttribute("replies", replies);
		model.addAttribute("replyCount", replyCount);
		return "survey/replies.adm";
	}
	
	/**
	 * 설문조사 답변리스트 엑셀 출력
	 * @param id
	 * @param currentPage
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/{id}/replies/excel", method = RequestMethod.GET)
	public ModelAndView repliesExcel(@PathVariable int id
			, Model model) {
		
		Survey survey = surveyService.findWithArticles(id);
		List<Reply> replies = surveyService.repliesWithDetails(id, null);
		
		Map<String, Object> excelModel = new HashMap<String, Object>();
		excelModel.put("survey", survey);
		excelModel.put("replies", replies);
		
		return new ModelAndView("SurveyRepliesExcelView", excelModel);
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
}
