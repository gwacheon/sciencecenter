package kr.go.sciencecenter.controller.admin;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import kr.go.sciencecenter.model.Holiday;
import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.service.HolidayService;

@Controller
@RequestMapping("/admin/holidays")
public class AdminHolidayController {
	private static final Logger logger = LoggerFactory.getLogger(AdminHolidayController.class);
	
	@Autowired
	HolidayService holidayService;
	
	@RequestMapping(value="", method = RequestMethod.GET)
	public String index(Model model
			, @RequestParam(value="page", required=false) Integer currentPage){
		
		String path = "/admin/holidays?";
		
		if(currentPage == null){
			currentPage = 1;
		}
		
		Page page = new Page(currentPage, 10, holidayService.count());
		page.setPath(path);
		model.addAttribute("page", page);
		
		List<Holiday> holidays = holidayService.list(page);
		
		model.addAttribute("holidays", holidays);
		model.addAttribute("holiday", new Holiday());
		model.addAttribute("timeTypes", Holiday.TimeType.values());
		
		return "holidays/index.admin";
	}
	
	@RequestMapping(value="", method = RequestMethod.POST)
	public String create(@ModelAttribute("holiday") Holiday holiday, Model model){
		int result = holidayService.create(holiday);
		return "redirect:/admin/holidays";
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.DELETE)
	public String delete(@PathVariable int id, Model model){
		holidayService.delete(id);
		return "redirect:/admin/holidays";
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
		
	}
}
