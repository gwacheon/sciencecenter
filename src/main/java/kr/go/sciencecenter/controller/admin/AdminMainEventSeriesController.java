package kr.go.sciencecenter.controller.admin;

import java.util.List;

import kr.go.sciencecenter.model.Board;
import kr.go.sciencecenter.model.BoardType;
import kr.go.sciencecenter.model.MainEventSeries;
import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.model.User;
import kr.go.sciencecenter.model.UserDetail;
import kr.go.sciencecenter.model.api.Member;
import kr.go.sciencecenter.service.BoardService;
import kr.go.sciencecenter.service.MainEventSeriesService;
import kr.go.sciencecenter.service.MainEventService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/admin/mainEventSeries")
public class AdminMainEventSeriesController {

	private static final Logger logger = LoggerFactory.getLogger(AdminMainEventSeriesController.class);

	@Autowired
	MainEventSeriesService mainEventSeriesService;

	@Autowired
	MainEventService mainEventService;

	@Autowired
	private BoardService boardService;

	@ModelAttribute
	public void setCategories(Model model) {
		model.addAttribute("currentAdminCategory", "mainEvents");
	}
	
	/**
	 * 새로운 차수 new
	 * 
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/new/{id}", method = RequestMethod.GET)
	public String newMainEventSeries(Model model, @PathVariable int id) {
		MainEventSeries mainEventSeries = new MainEventSeries();
		mainEventSeries.setMainEventId(id);
		mainEventSeries.setDefaultBoardMap();
		model.addAttribute("mainEventName", mainEventService.find(id).getName());
		model.addAttribute("mainEventSeries", mainEventSeries);
		return "mainEventSeries/new.adm";
	}

	/**
	 * 새로운 차수 create
	 * 
	 * @param mainEventSeries
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String createMainEventSeries(@ModelAttribute("mainEventSeries") MainEventSeries mainEventSeries,
			Model model) {

		mainEventSeriesService.create(mainEventSeries);

		return "redirect:/admin/mainEvents";
	}

	/**
	 * 각 차수의 정보 보기 : 기본정보 페이지가 기본으로 보여진다.
	 * 
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String show(@PathVariable int id, Model model) {

		MainEventSeries mainEventSeries = mainEventSeriesService.find(id);

		mainEventSeries.setDefaultVisibleBoardNames();

		model.addAttribute("mainEventSeries", mainEventSeries);

		BoardType boardType = BoardType.BOARD_TYPES.get("mainEventSeriesBasicInfo");
		model.addAttribute("type", boardType);

		return "mainEventSeries/show.adm";
	}

	/**
	 * 각 차수 기본정보 수정 페이지: 기본정보 수정 및 각 게시판의 숨김 여부 수정.
	 * 
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/{id}/basicInfo/edit", method = RequestMethod.GET)
	public String editBasicInfo(@PathVariable int id, Model model) {

		MainEventSeries mainEventSeries = mainEventSeriesService.find(id);
		mainEventSeries.setDefaultVisibleBoardNames();
		mainEventSeries.setDefaultBoardMap();
		model.addAttribute("mainEventSeries", mainEventSeries);

		BoardType boardType = BoardType.BOARD_TYPES.get("mainEventSeriesBasicInfo");
		model.addAttribute("type", boardType);

		return "mainEventSeries/edit.adm";
	}

	/**
	 * 각 차수 기본정보 update
	 * 
	 * @param mainEventSeries
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/{id}/basicInfo/update", method = RequestMethod.PUT)
	public String updateBasicInfo(@ModelAttribute("mainEventSeries") MainEventSeries mainEventSeries, Model model) {

		mainEventSeriesService.update(mainEventSeries);

		return "redirect:/admin/mainEventSeries/" + mainEventSeries.getId();
	}

	/**
	 * 각 차수의 기본정보를 제외한 게시판 index
	 * 
	 * @param model
	 * @param id
	 * @param type
	 * @param currentPage
	 * @param searchType
	 * @param searchKey
	 * @return
	 */
	@RequestMapping(value = "/{id}/{type}", method = RequestMethod.GET)
	public String showMainEvent(Model model, @PathVariable int id, @PathVariable String type,
			@RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			@RequestParam(value = "searchType", required = false) String searchType,
			@RequestParam(value = "searchKey", required = false) String searchKey) {

		MainEventSeries mainEventSeries = mainEventSeriesService.find(id);
		mainEventSeries.setDefaultVisibleBoardNames();
		model.addAttribute("mainEventSeries", mainEventSeries);

		BoardType boardType = BoardType.BOARD_TYPES.get(type);
		model.addAttribute("type", boardType);

		Page page = new Page(currentPage, 10, boardService.countEventSeriesBoards(type, id, searchType, searchKey));
		page.setPath(boardType.getPath());
		model.addAttribute("page", page);

		List<Board> boards = boardService.listEventSeriesBoards(type, id, page, searchType, searchKey, null);
		model.addAttribute("boards", boards);

		return "boards/index.adm";
	}

	/**
	 * 각 차수 삭제
	 * 
	 * @param mainEventSeriesId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public String deleteMainEventSeries(@PathVariable("id") int mainEventSeriesId, Model model) {
		mainEventSeriesService.delete(mainEventSeriesId);

		return "redirect:/admin/mainEvents";
	}
}
