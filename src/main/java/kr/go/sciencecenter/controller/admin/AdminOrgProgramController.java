package kr.go.sciencecenter.controller.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.go.sciencecenter.api.AuthenticationApi;
import kr.go.sciencecenter.model.OrgApplicant;
import kr.go.sciencecenter.model.OrgProgram;
import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.model.api.Member;
import kr.go.sciencecenter.service.OrgProgramService;

@Controller
@RequestMapping("/admin/orgPrograms")
public class AdminOrgProgramController {
	private static final Logger logger = LoggerFactory.getLogger(AdminOrgProgramController.class);
	
	@Autowired
	OrgProgramService orgProgramService;
	
	@RequestMapping(value="", method = RequestMethod.GET)
	public String index(Model model) {
		List<OrgProgram> orgPrograms = orgProgramService.adminList();
		model.addAttribute("orgPrograms", orgPrograms);
		
		return "orgPrograms/index.adm";
	}
	
	@RequestMapping(value="/new", method = RequestMethod.GET)
	public String newProgram(Model model){
		model.addAttribute("orgProgram", new OrgProgram());
		return "orgPrograms/new.adm";
	}
	
	@RequestMapping(value="", method = RequestMethod.POST)
	public String create(Model model
			, @ModelAttribute("orgProgram") OrgProgram orgProgram
			, BindingResult result) {
		
		if (!result.hasErrors()) {
			orgProgramService.create(orgProgram);
		}else {
			logger.error("Org Program Binding Error");
			logger.error(result.toString());
		}
		
		return "redirect:/admin/orgPrograms";
	}
	
	@RequestMapping(value="/{orgProgramId}/edit", method = RequestMethod.GET)
	public String edit(Model model
			, @PathVariable("orgProgramId") int orgProgramId){
		model.addAttribute("orgProgram", orgProgramService.find(orgProgramId));
		return "orgPrograms/edit.adm";
	}
	
	@RequestMapping(value="/{orgProgramId}", method = RequestMethod.PUT)
	public String update(Model model
			, @PathVariable("orgProgramId") int orgProgramId
			, @ModelAttribute("orgProgram") OrgProgram orgProgram
			, BindingResult result){
		orgProgram.setId(orgProgramId);
		orgProgramService.update(orgProgram);
		return "redirect:/admin/orgPrograms/" + orgProgram.getId() + "/edit";
	}
	
	@RequestMapping(value="/{orgProgramId}/{status}", method = RequestMethod.GET)
	public String editStatus(Model model
			, @PathVariable("orgProgramId") int orgProgramId
			, @PathVariable("status") String status){
		OrgProgram orgProgram = orgProgramService.find(orgProgramId);
		orgProgram.setStatus(status);
		if (status.equals("active")) {
			orgProgram.setUsed(true);
		} else {
			orgProgram.setUsed(false);
		}
		orgProgramService.update(orgProgram);
		
		return "redirect:/admin/orgPrograms";
	}
	
	@RequestMapping(value="/getMemberDetail/{memberNo}", method = RequestMethod.GET)
	public @ResponseBody Model getMemberDetail(@PathVariable String memberNo
			,Model model) {
		
		Member member = AuthenticationApi.getInstance().getInfo(memberNo);
		model.addAttribute("member", member );
		return model;
	}
	
	@RequestMapping(value="/{orgProgramId}/applicants", method = RequestMethod.GET)
	public String applicants(Model model
			, HttpServletRequest request
			, @PathVariable("orgProgramId") int orgProgramId
			, @ModelAttribute("searchApplicant") OrgApplicant searchOrgApplicant
			, @RequestParam(value = "page", defaultValue = "1") Integer currentPage){
		
		if (searchOrgApplicant == null) {
			searchOrgApplicant = new OrgApplicant();
		}
		
		Page page = new Page(currentPage, 10, orgProgramService.applicantsCount(orgProgramId, searchOrgApplicant, null));
		page.setPath(request.getServletPath() + searchOrgApplicant.getQueryPath());
		model.addAttribute("page", page);		
		
		model.addAttribute("searchApplicant", searchOrgApplicant);
		
		OrgProgram program = orgProgramService.find(orgProgramId);
		model.addAttribute("program", program);
		
		List<OrgApplicant> applicants = orgProgramService.applicants(orgProgramId, searchOrgApplicant, page, null);
		model.addAttribute("applicants", applicants);
		
		return "orgPrograms/applicants.adm";
	}
}
