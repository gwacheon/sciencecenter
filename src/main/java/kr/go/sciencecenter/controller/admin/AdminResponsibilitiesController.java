package kr.go.sciencecenter.controller.admin;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.go.sciencecenter.model.Responsibility;
import kr.go.sciencecenter.service.ResponsibilityService;

@Controller
@RequestMapping("/admin/responsibilities")
public class AdminResponsibilitiesController {
	private static final Logger logger = LoggerFactory.getLogger(AdminResponsibilitiesController.class);
	
	@Autowired
	private ResponsibilityService responsibilityService;
	
	
	@RequestMapping(value="", method = RequestMethod.POST)
	public @ResponseBody Model createOrUpdate(@RequestBody String jsonStr, Model model){
		JSONObject jsonObj = new JSONObject(jsonStr);
		Responsibility responsibility = new Responsibility();
		responsibility.setRequestURI(jsonObj.getString("requestURI"));
		
		if(jsonObj.has("tabName")){
			responsibility.setTabName(jsonObj.getString("tabName"));
		}
		
		if(jsonObj.has("dept")){
			responsibility.setDept(jsonObj.getString("dept"));
		}
		
		if(jsonObj.has("member")){
			responsibility.setMember(jsonObj.getString("member"));
		}
		
		if(jsonObj.has("phone")){
			responsibility.setPhone(jsonObj.getString("phone"));
		}
		
		responsibilityService.createOrUpdate(responsibility);
		
		model.addAttribute("responsibility", responsibility);
		
		return model;
	}
	
	@RequestMapping(value="/", method = RequestMethod.POST)
	public String create(@ModelAttribute("responsibility") Responsibility responsibility
			, BindingResult result
			, Model model
			, HttpServletRequest request){
		
		responsibilityService.create(responsibility);
		
		return "redirect:" + responsibility.getRequestURI().substring(request.getContextPath().length());
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.POST)
	public String update(@PathVariable int id
			, @ModelAttribute("responsibility") Responsibility responsibility
			, BindingResult result
			, Model model
			, HttpServletRequest request){
		
		responsibility.setId(id);
		responsibilityService.update(responsibility);
		
		return "redirect:" + responsibility.getRequestURI().substring(request.getContextPath().length());
	}
}
