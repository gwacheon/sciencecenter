package kr.go.sciencecenter.controller.admin;

import java.text.ParseException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import kr.go.sciencecenter.api.AuthenticationApi;
import kr.go.sciencecenter.model.EquipReserve;
import kr.go.sciencecenter.model.EquipSerial;
import kr.go.sciencecenter.model.Equipment;
import kr.go.sciencecenter.model.SangSangScheduleTitle;
import kr.go.sciencecenter.service.EquipReserveService;
import kr.go.sciencecenter.service.EquipmentService;
import kr.go.sciencecenter.service.SangSangService;

@Controller
@RequestMapping("/admin/sangsang")
public class AdminSangSangController {
	private static final Logger logger = LoggerFactory.getLogger(AdminSangSangController.class);
	
	@Autowired
	EquipmentService equipmentService;
	
	@Autowired
	EquipReserveService equipReserveService;
	
	@Autowired
	SangSangService sangSangService;

	@Autowired
	Environment environment;

	@RequestMapping(value = "/{type}", method = RequestMethod.GET)
	public String index(Model model, @RequestParam(value = "page", required = false) Integer currentPage,
			@PathVariable String type) {

		List<Equipment> equipments = equipmentService.listByType(type);
		
		model.addAttribute("sangsangType", type);
		model.addAttribute("equipments", equipments);

		return "sangsang/index.adm";
	}

	@RequestMapping(value = "/show/{id}", method = RequestMethod.GET)
	public String show(Model model
			, @PathVariable int id
			, @RequestParam(value = "serialId", defaultValue = "0") int serialId) {
		
		Equipment equipment = equipmentService.find(id);
		if(serialId==0){
			serialId = equipment.getEquipSerials().get(0).getId();
		}
		EquipSerial equipSerial = equipmentService.findSerial(serialId);
		
		model.addAttribute("equipSerial", equipSerial);
		model.addAttribute("equipment", equipment);
		
		return "sangsang/show.adm";
	}

	@RequestMapping(value = "/unavailable/{id}")
	public String unavailable(Model model, @PathVariable int id) {
		Equipment equipment = equipmentService.find(id);

		model.addAttribute("equipment", equipment);

		return "sangsang/EquipUnavailable.adm";
	}

	@RequestMapping(value = "/newEquipment/{type}", method = RequestMethod.GET)
	public String newEquipment(Model model, @PathVariable String type) {
		Equipment equipment = new Equipment();
		equipment.setType(type);
		model.addAttribute("equipment", equipment);
		model.addAttribute("sangsangType", type);
		return "sangsang/newEquipment.adm";
	}

	@RequestMapping(value = "/editEquipment/{id}", method = RequestMethod.GET)
	public String editEquipment(Model model, @PathVariable int id) {
		Equipment equipment = equipmentService.find(id);
		model.addAttribute("equipment", equipment);
		model.addAttribute("sangsangType", equipment.getType());
		return "sangsang/editEquipment.adm";
	}

	@RequestMapping(value = "/newEquipment", method = RequestMethod.POST)
	public String create(@ModelAttribute("equipment") Equipment equipment, Model model, BindingResult result,
			@RequestParam("file") MultipartFile file) {

		equipmentService.create(equipment, file);

		return "redirect:/admin/sangsang/" + equipment.getType();
	}

	@RequestMapping(value = "/updateEquipment", method = RequestMethod.POST)
	public String update(@ModelAttribute("equipment") Equipment equipment, Model model, BindingResult result,
			@RequestParam("file") MultipartFile file) {

		equipmentService.update(equipment, file);

		return "redirect:/admin/sangsang/" + equipment.getType();
	}

	@RequestMapping(value = "/schedules/manage", method = RequestMethod.GET)
	public String manageSchedule(Model model) {
		SangSangScheduleTitle sangSangSchedule = sangSangService.findWithSchedules();
		model.addAttribute("sangSangSchedule", sangSangSchedule);

		return "sangsang/schedules.adm";
	}

	@RequestMapping(value = "/schedules/save", method = RequestMethod.POST)
	public @ResponseBody Model saveSchedule(@RequestBody String jsonStr, Model model) {
		SangSangScheduleTitle sangSangSchedule = new SangSangScheduleTitle(jsonStr);
		sangSangService.saveSchedule(sangSangSchedule);

		model.addAttribute("sangSangSchedule", sangSangSchedule);

		return model;
	}
	
	@RequestMapping(value = "/confirmation", method = RequestMethod.POST)
	public Model setConfirmation(@RequestBody String jsonStr 
			, HttpServletRequest request, Model model) throws ParseException {
		JSONObject jsonObj = new JSONObject(jsonStr);
		
		int id = jsonObj.getInt("id");
		String status = jsonObj.getString("status");
		
		equipReserveService.changeStatus(id,status);
		
		model.addAttribute("result", "sucess");
		return model;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Model setDeletedEquipment(@RequestBody String jsonStr 
			, HttpServletRequest request, Model model) throws ParseException {
		JSONObject jsonObj = new JSONObject(jsonStr);
		
		int id = jsonObj.getInt("id");
		
		Equipment equipment = equipmentService.find(id);
		equipment.setStatus("deleted");
		equipmentService.update(equipment, null);
		
		return model;
	}
}
