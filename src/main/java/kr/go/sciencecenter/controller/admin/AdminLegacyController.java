package kr.go.sciencecenter.controller.admin;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 기존 어드민 페이지 호출
 * @author ckh
 *
 */
@Controller
@RequestMapping("/admin/legacy")
public class AdminLegacyController
{
	private static final Logger logger = LoggerFactory.getLogger(AdminLegacyController.class);
    @Value("${c2_admin_url}") private String legacyDomain;
    
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String home(Locale locale, Model model, HttpServletRequest request)
    {
        logger.info("AdminLegacyController.index -- ! ");
        
        model.addAttribute("legacyDomain", legacyDomain);
        model.addAttribute("url", request.getParameter("url"));
        model.addAttribute("tmn", request.getParameter("tmn"));
        return "legacy/index.adm";
    }
}