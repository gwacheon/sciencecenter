package kr.go.sciencecenter.controller.admin;

import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.model.api.Member;
import kr.go.sciencecenter.service.MembershipLogService;

@Controller
@RequestMapping("/admin/membershipLog")
public class AdminMembershipLogController {
	@Autowired
	MembershipLogService logService;
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String home(Model model
			, @RequestParam(value = "page", defaultValue = "1") Integer currentPage){
		
		Page page = new Page(currentPage, 10, logService.staticCount());
		page.setPath("/admin/membershipLog?");
		model.addAttribute("page", page);
		
		List<Member> members = logService.statistics(page);
		model.addAttribute("members", members);
		
		return "membershiplog/index.adm";
	}
	
	@RequestMapping(value="/{memberNo}")
	public String detail(@PathVariable("memberNo") String memberNo
			, @RequestParam(value = "page", defaultValue = "1") Integer currentPage
			, Model model) {
		
		Page page = new Page(currentPage, 10, logService.countByMember(memberNo));
		page.setPath("/admin/membershipLog/" + memberNo + "?");
		model.addAttribute("page", page);
		
		List<Member> logs = logService.logByMemberNo(memberNo, page);
		model.addAttribute("logs", logs);
		
		return "membershiplog/details.adm";
	}
	
	@RequestMapping(value = "/manager", method = RequestMethod.GET)
	public String manager(Model model){
		return "membershiplog/manager.adm";
	}
	
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public @ResponseBody Model search(@RequestBody String jsonStr
			, Model model){
		JSONObject jsonObj = new JSONObject(jsonStr);
		String name = jsonObj.getString("name");
		
		List<Member> members = logService.searchMembership(name);
		model.addAttribute("members", members);
		
		return model;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody Model create(@RequestBody String jsonStr
			, Model model){
		JSONObject jsonObj = new JSONObject(jsonStr);
		String name = jsonObj.getString("name");
		String memberNo = jsonObj.getString("memberNo");
		
		logService.createLog(memberNo, name);
		
		return model;
	}
}
