package kr.go.sciencecenter.controller.admin;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.go.sciencecenter.model.MainPageBanner;
import kr.go.sciencecenter.service.MainPageService;
import kr.go.sciencecenter.util.FileUploadHelper;

@Controller
@RequestMapping("/admin/mainPage")
public class AdminMainPageController {
	
	private static final Logger logger = LoggerFactory.getLogger(AdminMainPageController.class);
	
	@Autowired
	private MainPageService mainPageService;
	
	@Autowired
	private ServletContext servletContext;
		
	@Autowired
	private Environment env;
	
	@ModelAttribute
	public void setCategories(Model model) {
		model.addAttribute("currentAdminCategory", "mainPage");
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String mainPage(Model model ) {
		
		for (int i = 1; i <= 7; i++) {
			model.addAttribute("type" + i + "Banners", mainPageService.listBannersByType("type" + i, true) );
		}
		
		
		List<List<MainPageBanner>> iconBanners = new ArrayList<List<MainPageBanner>>();
		for (int i = 0; i < 8; i++) {
			iconBanners.add(mainPageService.listBannersByType("iconBanner" + i, true));
		}
		model.addAttribute("iconBanners", iconBanners);
		
		List<List<MainPageBanner>> eventBanners = new ArrayList<List<MainPageBanner>>();
		for (int i = 0; i < 6; i++) {
			eventBanners.add(mainPageService.listBannersByType("eventBanner" + i, true));
		}
		model.addAttribute("eventBanners", eventBanners);
		
		List<List<MainPageBanner>> footerBanners = new ArrayList<List<MainPageBanner>>();
		for (int i = 0; i < 3; i++) {
			footerBanners.add(mainPageService.listBannersByType("footerBanner" + i, true));
		}
		model.addAttribute("footerBanners", footerBanners);
		
		model.addAttribute("mainPageBanner", new MainPageBanner());
		
		return "mainPage/index.adm";
	}
	
	/**
	 * 타입별 배너 리스트조회
	 * @param type
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getByTypes/{type}", method = RequestMethod.GET)
	public @ResponseBody Model getByTypes(@PathVariable String type
						,Model model) {
		
		model.addAttribute("banners", mainPageService.listBannersByType(type,false) );
		return model;
	}
	
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String createMainPage(@ModelAttribute("mainPageBanner")MainPageBanner mainPageBanner
			, Model model ) {
		
		mainPageService.createBanner(mainPageBanner);
		
		return "redirect:/admin/mainPage";
	}
	
	@RequestMapping(value = "/getById/{id}", method = RequestMethod.GET)
	public @ResponseBody Model getMainPageItem(Model model
			,@PathVariable("id") int id) {
		
		model.addAttribute("banner", mainPageService.findBannerById(id));
		
		return model;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public @ResponseBody Model remove(@PathVariable("id") int id,
			Model model ) {
		MainPageBanner mainPageBanner = mainPageService.findBannerById(id);
		mainPageService.delete(mainPageBanner);
		
		return model;
	}
	
	
	@RequestMapping(value = "/banners", method = RequestMethod.GET)
	public String index(@RequestParam(value = "type", defaultValue = "mainLeftSlideBanner")String type, 
			Model model ) {
		
		
		model.addAttribute("mainPageBanners", mainPageService.listBannersByType(type,false) );
		model.addAttribute("mainBannerTypes", MainPageBanner.getBannerTypes());
		model.addAttribute("currentType", type);
		
		return "mainPage/banners/index.admin";
	}
	
	@RequestMapping(value = "/banners/new", method = RequestMethod.GET)
	public String newBanner(@RequestParam(value = "type", required = true)String type, 
			Model model ) {
		
		model.addAttribute("mainBannerTypes", MainPageBanner.getBannerTypes());
		model.addAttribute("currentType", type);
		
		MainPageBanner mainPageBanner = new MainPageBanner();
		mainPageBanner.setBannerType(type);
		mainPageBanner.setVisible(true);
		model.addAttribute("mainPageBanner", mainPageBanner);
		
		return "mainPage/banners/new.admin";
	}
	
	@RequestMapping(value = "/banners", method = RequestMethod.POST)
	public String createBanner( @ModelAttribute("mainPageBanner")MainPageBanner mainPageBanner,
			Model model ) {
		
		mainPageService.createBanner(mainPageBanner, servletContext.getContextPath(), env.getProperty("file.path"));
		
		return "redirect:/admin/mainPage/banners?type=" + mainPageBanner.getBannerType();
	}
	
	@RequestMapping(value = "/banners/{id}/edit", method = RequestMethod.GET)
	public String editBanner( @PathVariable("id") int id,
			Model model ) {
		
		MainPageBanner mainPageBanner = mainPageService.findBannerById(id);
		
		model.addAttribute("mainBannerTypes", MainPageBanner.getBannerTypes());
		model.addAttribute("currentType", mainPageBanner.getBannerType());
		
		model.addAttribute("mainPageBanner", mainPageBanner);
		
		return "mainPage/banners/edit.admin";
	}
	
	@RequestMapping(value = "/banners/update", method = RequestMethod.POST)
	public String updateBanner(@ModelAttribute("mainPageBanner")MainPageBanner mainPageBanner,
			Model model ) {
		
		mainPageService.updateBanner(mainPageBanner, servletContext.getContextPath(), env.getProperty("file.path"));
		
		return "redirect:/admin/mainPage/";
	}
	
	@RequestMapping(value = "/banners/{id}/delete", method = RequestMethod.DELETE)
	public String deleteBanner(@PathVariable("id") int id,
			Model model ) {
		MainPageBanner mainPageBanner = mainPageService.findBannerById(id);
		mainPageService.deleteBanner(mainPageBanner, env.getProperty("file.path"));
		
		return "redirect:/admin/mainPage/banners?type=" + mainPageBanner.getBannerType();
	}
	
	@RequestMapping(value = "/sortable", method = RequestMethod.POST)
	public @ResponseBody Model sortable(Model model, @RequestBody String jsonStr){
		System.out.println(jsonStr);
		JSONObject jsonObj = new JSONObject(jsonStr);
		JSONArray array = jsonObj.getJSONArray("data");
		
		for(int i=0; i< array.length() ; i++){
			int id= array.getInt(i);
			MainPageBanner banner = mainPageService.findBannerById(id);
			banner.setSeq(i);
			mainPageService.updateBanner(banner, servletContext.getContextPath(), env.getProperty("file.path"));
		}
		
		return model;
	}
	
}
