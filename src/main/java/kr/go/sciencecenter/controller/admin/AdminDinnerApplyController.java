package kr.go.sciencecenter.controller.admin;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.go.sciencecenter.model.DinnerApply;
import kr.go.sciencecenter.model.DinnerApplyTime;
import kr.go.sciencecenter.model.DinnerApplyUser;
import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.service.DinnerApplyService;
import kr.go.sciencecenter.service.DinnerApplyTimeService;
import kr.go.sciencecenter.service.DinnerApplyUserService;

@Controller
@RequestMapping("/admin/dinnerApply")
public class AdminDinnerApplyController {
	
	private static final Logger logger = LoggerFactory.getLogger(AdminDinnerApplyController.class);
	
	@Autowired
	DinnerApplyService dinnerApplyService;
	
	@Autowired
	DinnerApplyTimeService dinnerApplyTimeService;
	
	@Autowired
	DinnerApplyUserService dinnerApplyUserService;
	
	@ModelAttribute
	public void setCategories(Model model) {
		model.addAttribute("currentAdminCategory", "dinnerApply");
	}	
	
	/**
	 * 금일 신청 내역 리스트
	 * 
	 * @param model
	 * @param currentPage
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String index(Model model,
			@RequestParam(value = "page", defaultValue = "1") Integer currentPage) {
		
		int totalCount = dinnerApplyService.count(this.getTodayDate());
		Page page = new Page(currentPage, 5, totalCount);
		page.setPath("/admin/dinnerApply?");
		model.addAttribute("page", page);
		
		List<DinnerApply> dinnerApplies = dinnerApplyService.list(page);
		model.addAttribute("dinnerApplies", dinnerApplies);
		model.addAttribute("totalCount", totalCount);
		
		model.addAttribute("today", new Date() );

		model.addAttribute("currentSideMenu", "applicationsToday");
		
		return "dinnerApply/index.adm";  
	}
	
	/**
	 * 현재 설정된 신청 시간 확인
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/applicationTime", method = RequestMethod.GET)
	public String showApplyTime(Model model) {
		
		DinnerApplyTime dinnerApplyTime = dinnerApplyTimeService.findApplicationTimeMostRecent();
		// 설정한 시간이 없다면,
		if( dinnerApplyTime == null ) {
			dinnerApplyTime = new DinnerApplyTime();
		}
			
		model.addAttribute("dinnerApplyTime", dinnerApplyTime);
   
		model.addAttribute("currentSideMenu", "applicationTime");
		return "dinnerApply/applicationTime.adm";  
	}
	
	
	/**
	 * 새로운 신청시간 생성
	 * @param dinnerApplyTime
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/applicationTime/create", method = RequestMethod.POST)
	public String createApplyTime(
			@RequestParam("applyBeginTime")String applyBeginTime,
			@RequestParam("applyEndTime")String applyEndTime,
			Model model ) {
		
		DateFormat formatter =  new SimpleDateFormat("HH:mm");
		Date beginTime;
		Date endTime;
		
		DinnerApplyTime dinnerApplyTime = new DinnerApplyTime();
		
		try {
			beginTime = formatter.parse(applyBeginTime);
			endTime= formatter.parse(applyEndTime);
			dinnerApplyTime.setApplyBeginTime(beginTime);
			dinnerApplyTime.setApplyEndTime(endTime);
			
			dinnerApplyTimeService.create(dinnerApplyTime);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return "redirect:/admin/dinnerApply/applicationTime";  
	}
	
	/**
	 * 석식신청 로그인 계정 관리 Index
	 * @param applyBeginTime
	 * @param applyEndTime
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public String userIndex(
			@RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			Model model ) {
		try{
			Page page = new Page(currentPage, 10, dinnerApplyUserService.count());
			page.setPath("/admin/dinnerApply/users?");
			model.addAttribute("page", page);
			
			List<DinnerApplyUser> dinnerApplyUsers = dinnerApplyUserService.list(page); 
			model.addAttribute("dinnerApplyUsers", dinnerApplyUsers );

			model.addAttribute("currentSideMenu", "users");
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		
		return "dinnerApply/users/index.adm";  
	}
	
	/**
	 * 석식신청 로그인 계정 생성 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/users/new", method = RequestMethod.GET)
	public String newUser(Model model) {
		
		model.addAttribute("dinnerApplyUser", new DinnerApplyUser() );
		
		
		model.addAttribute("currentSideMenu", "users");
		
		return "dinnerApply/users/new.adm";
	}
	
	/**
	 * 석식신청 로그인 계정 생성 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/users", method = RequestMethod.POST)
	public String createUser(
			@ModelAttribute("dinnerApplyUser") DinnerApplyUser dinnerApplyUser,
			Model model) {
		
		// if there is any user with same user id,
		if( dinnerApplyUserService.findByUserId(dinnerApplyUser.getUserId()) != null ) {
			model.addAttribute("dinnerApplyUser", dinnerApplyUser);
			model.addAttribute("error", "아이디가 이미 존재합니다.");
			model.addAttribute("currentSideMenu", "users");
			return "dinnerApply/users/new.adm";
		}
		
			
		// encrypt password
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hashedPassword = passwordEncoder.encode(dinnerApplyUser.getPassword());
		dinnerApplyUser.setPassword(hashedPassword);
		
		// create
		dinnerApplyUserService.create(dinnerApplyUser);
		
		return "redirect:/admin/dinnerApply/users";
	}
	
	/**
	 * 석식신청 로그인 계정 수정
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/users/{id}/edit", method = RequestMethod.GET)
	public String editUser(@PathVariable("id") int id, Model model) {
		
		// find the user
		DinnerApplyUser dinnerApplyUser = dinnerApplyUserService.findById(id);
		// make the password blank
		dinnerApplyUser.setPassword("");
		model.addAttribute("dinnerApplyUser", dinnerApplyUser);
		
		model.addAttribute("formType", "edit");
		model.addAttribute("currentSideMenu", "users");
		
		return "dinnerApply/users/edit.adm";
	}
	
	
	/**
	 * 석식신청 로그인 계정 수정시 현재 비밀번호 확인
	 * @param jsonStr
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/users/checkPassword", method = RequestMethod.POST)
	public @ResponseBody Model checkPassword(@RequestBody String jsonStr,
			Model model) {
		
		boolean error = false;
		
		JSONObject jsonObj = new JSONObject(jsonStr);
		JSONObject jsonApplyUser = jsonObj.getJSONObject("dinnerApplyUser");
		
		int typedId= jsonApplyUser.getInt("id");
		String typedPassword= jsonApplyUser.getString("password");
		
		// compare the current Password
		DinnerApplyUser recordedUser = dinnerApplyUserService.findById(typedId);
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		if( !passwordEncoder.matches(typedPassword, recordedUser.getPassword()) ) {
			error = true;
			model.addAttribute("error_msg", "현재 비밀번호가 일치하지 않습니다.");
		}
		
		model.addAttribute("error", error);
		return model;
	}
	
	
	/**
	 * 석식신청 계정 정보 업데이트
	 * @param dinnerApplyUser
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/users/update", method = RequestMethod.PUT)
	public String updateUser(
			@ModelAttribute("dinnerApplyUser") DinnerApplyUser dinnerApplyUser,
			Model model) {
		
			
		// encrypt password
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hashedPassword = passwordEncoder.encode(dinnerApplyUser.getPassword());
		dinnerApplyUser.setPassword(hashedPassword);
		
		// update
		dinnerApplyUserService.update(dinnerApplyUser);
		
		return "redirect:/admin/dinnerApply/users";
	}

	/**
	 * 석식신청 계정 삭제
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
	public String deleteUser(@PathVariable("id")int id, Model model) {
		
		dinnerApplyUserService.delete(id);
		return "redirect:/admin/dinnerApply/users";
	}
	
	@RequestMapping(value = "/allHistories", method = RequestMethod.GET)
	public String allHistories(Model model,
			@RequestParam(value = "page", defaultValue = "1") Integer currentPage) {
		
		Page page = new Page(currentPage, 5, dinnerApplyService.count(null));
		page.setPath("/admin/dinnerApply/allHistories?");
		model.addAttribute("page", page);
		
		List<DinnerApply> dinnerApplies = dinnerApplyService.listAllHistories(page);
		model.addAttribute("dinnerApplies", dinnerApplies);
		
		model.addAttribute("currentSideMenu", "allHistories");
		
		return "dinnerApply/allHistories.adm";
	}
	
	private Date getTodayDate() {
		Date today = DateUtils.truncate(new Date(), Calendar.DATE);
		return today;
	}
	
	
}
