package kr.go.sciencecenter.controller.admin;

import java.util.List;

import kr.go.sciencecenter.model.Policy;
import kr.go.sciencecenter.model.SupportEvaluation;
import kr.go.sciencecenter.service.PolicyService;
import kr.go.sciencecenter.service.SupportEvaluationService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/admin/supportEvaluation")
public class AdminSupportController {
	
	private static final Logger logger = LoggerFactory.getLogger(AdminSupportController.class);
	
	@Autowired
	private SupportEvaluationService supportEvaluationService;
	
	@ModelAttribute
	public void setCategories(Model model) {
		model.addAttribute("currentAdminCategory", "supportEvaluation");
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String index(Model model) {
		
		List<SupportEvaluation> supportEvaluations = supportEvaluationService.list();
		model.addAttribute("supportEvaluations", supportEvaluations);
		return "supportEvaluation/index.adm";
	}
	
	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public String newEvaluation(Model model) {
		
		SupportEvaluation supportEvaluation = new SupportEvaluation();
		System.out.println("evaluation year: " + supportEvaluation.getYear());
		model.addAttribute("supportEvaluation", supportEvaluation);
		return "supportEvaluation/new.adm";
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String create(@ModelAttribute("supportEvaluation") SupportEvaluation supportEvaluation, 
			BindingResult result,
			Model model) {
		
		System.out.println(supportEvaluation.getYear());
		System.out.println(supportEvaluation.getSeq());
		System.out.println(supportEvaluation.getDuration());
		System.out.println(supportEvaluation.getEvaluator());
		
		supportEvaluationService.create(supportEvaluation);
		return "redirect:/admin/supportEvaluation";
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String show(@PathVariable int id, Model model) {
				
		SupportEvaluation supportEvaluation = supportEvaluationService.find(id);
		model.addAttribute("supportEvaluation", supportEvaluation);
		
		return  "supportEvaluation/show.adm";
	}
	
	@RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
	public String edit(@PathVariable int id, Model model) {
				
		SupportEvaluation supportEvaluation = supportEvaluationService.find(id);
		model.addAttribute("supportEvaluation", supportEvaluation);
		
		return  "supportEvaluation/edit.adm";
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public String update(@ModelAttribute("supportEvaluation") SupportEvaluation supportEvaluation
			, Model model) {
		int id = supportEvaluation.getId();
		supportEvaluationService.update(supportEvaluation);
		return "redirect:/admin/supportEvaluation/" + id;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public String delete(@PathVariable int id, Model model) {
		supportEvaluationService.delete(id);
		return "redirect:/admin/supportEvaluation";
	}
	
	
	
}
