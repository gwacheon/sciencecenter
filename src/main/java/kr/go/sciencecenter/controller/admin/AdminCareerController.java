package kr.go.sciencecenter.controller.admin;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.go.sciencecenter.model.Career;
import kr.go.sciencecenter.service.CareerService;

@Controller
@RequestMapping("/admin/career")
public class AdminCareerController {
	private static final Logger logger = LoggerFactory.getLogger(AdminCareerController.class);
	
	@Autowired
	private CareerService careerService;
	
	@RequestMapping(value="", method = RequestMethod.GET)
	public String index(Model model){
		List<Career> careers = careerService.list();
		model.addAttribute("careers", careers);
		
		return "career/index.adm";
	}
	
	@RequestMapping(value="", method = RequestMethod.POST)
	public String create(
			@ModelAttribute("career") Career career, 
			BindingResult result,
			Model model){
		
		careerService.create(career);
		
		return "redirect:/admin/career/" + career.getId() + "/edit";
	}
	
	@RequestMapping(value="{id}", method = RequestMethod.PUT)
	public String update(
			@PathVariable("id") int id,
			@ModelAttribute("career") Career career, 
			BindingResult result,
			Model model){
		
		career.setId(id);
		careerService.update(career);
		
		return "redirect:/admin/career/" + career.getId() + "/edit";
	}
	
	@RequestMapping(value="/new", method = RequestMethod.GET)
	public String newCareer(Model model){
		Career career = new Career();
		model.addAttribute("career", career);
		
		return "career/new.adm";
	}
	
	@RequestMapping(value="/{id}/edit", method = RequestMethod.GET)
	public String edit(
			@PathVariable("id") int id,
			Model model){
		Career career = careerService.find(id);
		model.addAttribute("career", career);
		
		return "career/edit.adm";
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.DELETE)
	public String delete(
			@PathVariable("id") int id,
			Model model){
		
		careerService.delete(id);
		
		return "redirect:/admin/career";
	}
	
	@RequestMapping(value="/{id}/addSchedules", method = RequestMethod.POST)
	public @ResponseBody Model replies(@RequestBody String jsonStr
			, Model model, @PathVariable int id
			, HttpServletRequest request){
		
		Career career = careerService.find(id);
		model.addAttribute("career", career);
		
		career.addSchedules(jsonStr);
		careerService.saveWithSchedules(career);
		
		return model;
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
}
