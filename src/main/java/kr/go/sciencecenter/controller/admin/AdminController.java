package kr.go.sciencecenter.controller.admin;

import java.util.Locale;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import kr.go.sciencecenter.api.AdminApi;
import kr.go.sciencecenter.api.AuthenticationApi;
import kr.go.sciencecenter.model.api.Admin;
import kr.go.sciencecenter.model.api.Member;
import org.springframework.web.bind.support.SessionStatus;

@Controller
@RequestMapping("/admin")
@SessionAttributes(Admin.KEY_FIELD)
public class AdminController {
	private static final Logger logger = LoggerFactory.getLogger(AdminController.class);
	
	/**
	 * 관리자 메인화면 : 
	 * index.admin 으로 보내지 않고, '메인페이지 관리' 페이지로 보낸다.
	 * @return
	 */
	@RequestMapping(value="", method = RequestMethod.GET)
	public String home(HttpServletRequest request) {
		logger.error("Session Check: " + (String)request.getSession().getAttribute(Admin.KEY_FIELD));
		return "redirect:/admin/mainPage";
	}
	
	private Member getCurrentMember(String memberNo) {
		return AuthenticationApi.getInstance().getInfo(memberNo);
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model model) {
		model.addAttribute("admin", new Admin());
		
		return "admin/login.app";
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(SessionStatus status,
			HttpServletRequest request,
			HttpServletResponse response) {		
		Admin admin = new Admin((String) request.getSession().getAttribute(Admin.KEY_FIELD));
		
		AdminApi adminApi = AdminApi.getInstance();
		adminApi.logout(admin);

		request.getSession().removeAttribute(Admin.KEY_FIELD);
		status.setComplete();

		Cookie cookie = new Cookie(Admin.KEY_FIELD, ""); // Not necessary, but saves bandwidth.
		cookie.setPath("/");
		cookie.setValue(null);
		cookie.setMaxAge(0); // Don't set to -1 or it will become a session cookie!
		response.addCookie(cookie);
		
		return "redirect:/admin";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String authentication(@ModelAttribute("admin") Admin admin,
			BindingResult result, Model model,
			HttpServletRequest request,
			HttpServletResponse response) {
		AdminApi adminApi = AdminApi.getInstance();
		adminApi.login(admin);
		
		if(admin.getRspCd().equals("0000")) {
			request.getSession().setAttribute(Admin.KEY_FIELD, admin.getTaManagerNo());
			
			Cookie cookie = new Cookie(Admin.KEY_FIELD, admin.getTaManagerNo());
			cookie.setPath("/");
			cookie.setMaxAge(60*60);
			//cookie.setDomain("localhost");
			response.addCookie(cookie);
			
			return "redirect:/admin/mainPage";
		} else {
			return "admin/login.app";
		}		
	}
}
