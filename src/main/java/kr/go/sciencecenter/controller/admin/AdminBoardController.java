package kr.go.sciencecenter.controller.admin;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import kr.go.sciencecenter.model.Board;
import kr.go.sciencecenter.model.BoardFile;
import kr.go.sciencecenter.model.BoardType;
import kr.go.sciencecenter.model.MainEventSeries;
import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.model.api.Admin;
import kr.go.sciencecenter.model.api.Member;
import kr.go.sciencecenter.service.BoardFileService;
import kr.go.sciencecenter.service.BoardService;
import kr.go.sciencecenter.service.MainEventSeriesService;

@Controller
@RequestMapping("/admin/boards")
@SessionAttributes(Admin.KEY_FIELD)
public class AdminBoardController {
	private static final Logger logger = LoggerFactory.getLogger(AdminBoardController.class);

	@Autowired
	private BoardService boardService;

	@Autowired
	private BoardFileService boardFileService;
	
	@Autowired
	private MainEventSeriesService mainEventSeriesService;
	
	@ModelAttribute
	public void setCategories(Model model) {
		model.addAttribute("currentAdminCategory", "board");
	}
	
	/**
	 * 게시판 Index 
	 * @param model
	 * @param currentPage
	 * @param type
	 * @param searchType
	 * @param searchKey
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String index(Model model, @RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			@RequestParam(value = "type", defaultValue = "notice") String type,
			@RequestParam(value = "searchType", required = false) String searchType,
			@RequestParam(value = "searchKey", required = false) String searchKey) {
				
		BoardType boardType = BoardType.BOARD_TYPES.get(type);

		Page page = new Page(currentPage, 10, boardService.count(type, searchType, searchKey));
		String requestURI = boardService.getHttpServletRequest().getRequestURI();
		String path = requestURI + "?type=" + type + "&";
		page.setPath(path);
		model.addAttribute("page", page);

		List<Board> boards = boardService.list(type, page, searchType, searchKey, null);
		model.addAttribute("boards", boards);
		model.addAttribute("board_type_list", BoardType.BOARD_TYPES);
		model.addAttribute("type", boardType);

		return "boards/index.adm";
	}
	
	/**
	 * 게시판 글 생성 페이지
	 * @param type
	 * @param mainEventSeriesId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/{type}/new", method = RequestMethod.GET)
	public String newBoard(@PathVariable String type, 
			@RequestParam(value = "mainEventSeriesId", required = false) Integer mainEventSeriesId,
			Model model) {
		Board board = new Board();
		board.setFixedOnTop(false);
		board.setBoardType(type);
		model.addAttribute("board", board);
		BoardType boardType = BoardType.BOARD_TYPES.get(type);
		if( boardType.getCategory().equals("mainEvent")) {
			// set mainEventSeriesId
			board.setEventSeriesId(mainEventSeriesId);
		}
		model.addAttribute("board_type", boardType);
		
		model.addAttribute("board_type_list", BoardType.BOARD_TYPES);

		return "boards/new.adm";
	}
	
	/**
	 * 게시판 글 상세보기 페이지
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String boardShow(@PathVariable int id, Model model) {
		Board board = boardService.find(id);

		model.addAttribute("board", board);
		model.addAttribute("board_type_list", BoardType.BOARD_TYPES);
		
		BoardType boardType = BoardType.BOARD_TYPES.get(board.getBoardType());
		if( boardType.getCategory().equals("mainEvent")) {
			MainEventSeries mainEventSeries = mainEventSeriesService.find(board.getEventSeriesId());
			mainEventSeries.setDefaultVisibleBoardNames();
			model.addAttribute("mainEventSeries", mainEventSeries);
			model.addAttribute("currentAdminCategory", "mainEvents");
		}
		model.addAttribute("type", boardType);
		return "boards/show.adm";
		
	}
	
	/**
	 * 
	 * 게시판 글 수정 페이지
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
	public String edit(@PathVariable int id, Model model) {
		Board board = boardService.find(id);
		model.addAttribute("board", board);
		
		BoardType boardType = BoardType.BOARD_TYPES.get(board.getBoardType());
		if( boardType.getCategory().equals("mainEvent")) {
			MainEventSeries mainEventSeries = mainEventSeriesService.find(board.getEventSeriesId());
			mainEventSeries.setDefaultVisibleBoardNames();
			model.addAttribute("mainEventSeries", mainEventSeries);
			model.addAttribute("currentAdminCategory", "mainEvents");
		}
		
		model.addAttribute("board_type", boardType);
		model.addAttribute("board_type_list", BoardType.BOARD_TYPES);
		return "boards/edit.adm";
	}
	
	/**
	 * 게시판 글 생성
	 * @param board
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String create(@ModelAttribute("board") Board board, Model model, HttpServletRequest request) {

		// 관리자가 로그인 한 상태에서 작성한 게시글이라면,
		String currentAdminNo = (String)request.getSession().getAttribute(Admin.KEY_FIELD);
		if( currentAdminNo != null ) {
			// 관리자 작성 flag 값을 y 로 설정.
			board.setWrittenByAdmin(true);
		}
		
		boardService.create(board);

		String typeName = board.getBoardType();
		BoardType boardType = BoardType.BOARD_TYPES.get(typeName);
		if(boardType.getCategory().equals("mainEvent")) {
			int mainEventSeriesId = board.getEventSeriesId();
			return "redirect:/admin/mainEventSeries/" + mainEventSeriesId + "/" + typeName;
		}
		else {
			return "redirect:/admin/boards?type=" + board.getBoardType();			
		}
	}
	
	/**
	 * 게시판 글 업데이트
	 * @param board
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.POST)
	public String update(@ModelAttribute("board") Board board, Model model, HttpServletRequest request) {
		
		// 관리자가 로그인 한 상태에서 업데이트된 게시글이라면,
		String currentAdminNo = (String)request.getSession().getAttribute(Admin.KEY_FIELD);
		if( currentAdminNo != null ) {
			// 관리자 작성 flag 값을 y 로 설정.
			board.setWrittenByAdmin(true);
		}
		
		boardService.update(board);
		
		String typeName = board.getBoardType();
		BoardType boardType = BoardType.BOARD_TYPES.get(typeName);
		if(boardType.getCategory().equals("mainEvent")) {
			int mainEventSeriesId = board.getEventSeriesId();
			return "redirect:/admin/mainEventSeries/" + mainEventSeriesId + "/" + typeName;
		}
		else {
			return "redirect:/admin/boards?type=" + board.getBoardType();			
		}
	}

	/**
	 * 게시판 글 삭제
	 * @param id
	 * @param type
	 * @param mainEventSeriesId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public String delete(@PathVariable int id, 
			@RequestParam(value = "type", defaultValue = "1") String type,
			@RequestParam(value = "mainEventSeriesId", required = false) Integer mainEventSeriesId,
			Model model) {
		
		Board board = boardService.find(id);
		// if it is reply,
		// get the original board's type
		if( type.equals("reply")) {
			Board originalBoard = boardService.find(board.getBoardId());
			type = originalBoard.getBoardType();
		}
		// delete the board
		boardService.delete(id);
		
		// redirect
		if( mainEventSeriesId != null ) {
			return "redirect:/admin/mainEventSeries/" + mainEventSeriesId + "/" + type;
		}
		else {
			return "redirect:/admin/boards?type=" + type;			
		}
	}
	
	
	/**
	 * 게시판 답글 작성 페이지 (관리자)
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/reply/{id}/show", method=RequestMethod.GET)
	public String showReply(@PathVariable("id") int replyId, Model model){
		// get reply
		Board reply = boardService.find(replyId);
		model.addAttribute("reply", reply);
		
		// get original Board
		Board board = boardService.find(reply.getBoardId());
		model.addAttribute("board", board);
		
		// board type attributes
		model.addAttribute("board_type_list", BoardType.BOARD_TYPES);
		BoardType boardType = BoardType.BOARD_TYPES.get(board.getBoardType());
		model.addAttribute("type", boardType);
		
		// MainEventSeries 내의 Dynamic 게시판에 대한 답글일 경우, 
		int eventSeriesId = board.getEventSeriesId(); 
		if( eventSeriesId > 0 ) {
			MainEventSeries mainEventSeries = mainEventSeriesService.find(eventSeriesId);
			mainEventSeries.setDefaultVisibleBoardNames();
			model.addAttribute("mainEventSeries", mainEventSeries);
			model.addAttribute("currentAdminCategory", "mainEvents");
		}
		
		return "boards/replyShow.adm"; 
	}
	
	/**
	 * 게시판 답글 작성 페이지 (관리자)
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/reply/{id}", method=RequestMethod.GET)
	public String newReply(@PathVariable int id,
			@RequestParam(value="redirectUrl", required=false)String redirectUrl,
			Model model){
		Board board = boardService.find(id);
		model.addAttribute("board", board);
		model.addAttribute("board_type_list", BoardType.BOARD_TYPES);
		
		BoardType boardType = BoardType.BOARD_TYPES.get(board.getBoardType());
		model.addAttribute("boardType", boardType);
		model.addAttribute("type", boardType);
		
		Board reply = new Board();
		model.addAttribute("reply", reply);
		
		model.addAttribute("redirectUrl", redirectUrl);
		
		// 답글을 등록하려는 게시판이 MainEventSeries 내의 Dynamic 게시판일 경우, 
		int eventSeriesId = board.getEventSeriesId(); 
		if( eventSeriesId > 0 ) {
			MainEventSeries mainEventSeries = mainEventSeriesService.find(eventSeriesId);
			mainEventSeries.setDefaultVisibleBoardNames();
			model.addAttribute("mainEventSeries", mainEventSeries);
			model.addAttribute("currentAdminCategory", "mainEvents");
		}
		
		
		return "boards/reply.adm"; 
	}
	
	/**
	 * 게시판 답글 생성
	 * @param id
	 * @param reply
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/reply/{id}", method = RequestMethod.POST)
	public String createReply(@PathVariable("id") int id
			, @ModelAttribute("reply") Board reply
			, Model model) {
		
		reply.setWrittenByAdmin(true);
		reply.setBoardType("reply");
		reply.setBoardId(id);
		
		boardService.create(reply);
		
		Board board = boardService.find(id);
		BoardType boardType = BoardType.BOARD_TYPES.get(board.getBoardType());
		if(boardType.getCategory().equals("mainEvent")) {
			int mainEventSeriesId = board.getEventSeriesId();
			return "redirect:/admin/mainEventSeries/" + mainEventSeriesId + "/" + board.getBoardType();
		}
		return "redirect:/admin/boards/"+ id;
		
	}

	/**
	 * 답글 수정 page
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/reply/{id}/edit", method=RequestMethod.GET)
	public String editReply(@PathVariable("id") int replyId,Model model){
		
		// get reply
		Board reply= boardService.find(replyId);
		model.addAttribute("reply", reply);
		
		// get original board of reply
		int boardId = reply.getBoardId();
		Board board = boardService.find(boardId);
		model.addAttribute("board", board);
		
		model.addAttribute("board_type_list", BoardType.BOARD_TYPES);
		BoardType boardType = BoardType.BOARD_TYPES.get(board.getBoardType());
		model.addAttribute("boardType", boardType);
		model.addAttribute("type", boardType);

		// 메인이벤트게시판에 대한 답글 수정일 경우,
		int eventSeriesId = board.getEventSeriesId(); 
		if( eventSeriesId > 0 ) {
			MainEventSeries mainEventSeries = mainEventSeriesService.find(eventSeriesId);
			mainEventSeries.setDefaultVisibleBoardNames();
			model.addAttribute("mainEventSeries", mainEventSeries);
			model.addAttribute("currentAdminCategory", "mainEvents");
		}
		
		return "boards/replyEdit.adm"; 
	}
	
	/**
	 * 답글 Update
	 * @param replyId
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/reply/{id}/update", method=RequestMethod.POST)
	public String updateReply(@ModelAttribute("reply") Board reply, Model model, HttpServletRequest request){
		
		// 관리자가 로그인 한 상태에서 업데이트된 게시글이라면,
		String currentAdminNo = (String)request.getSession().getAttribute(Admin.KEY_FIELD);
		if( currentAdminNo != null ) {
			// 관리자 작성 flag 값을 y 로 설정.
			reply.setWrittenByAdmin(true);
			// update reply
			boardService.update(reply);
			
			// get original board 
			Board board = boardService.find(reply.getBoardId());
			
			// set redirect page
			String typeName = board.getBoardType();
			BoardType boardType = BoardType.BOARD_TYPES.get(typeName);
			if(boardType.getCategory().equals("mainEvent")) {
				int mainEventSeriesId = board.getEventSeriesId();
				return "redirect:/admin/mainEventSeries/" + mainEventSeriesId + "/" + typeName;
			}
			else {
				return "redirect:/admin/boards?type=" + board.getBoardType();			
			}
		}
		// if admin is not logged in,
		else {
			return "redirect:/admin/login";
		}
		
		
		
	}
	
	@RequestMapping(value="/{id}/deleteFile", method = RequestMethod.POST)
	public @ResponseBody Model deleteAttachmentFile(@RequestBody String jsonStr,
			Model model, 
			@PathVariable int id,
			HttpServletRequest request) {
		
		boolean isDeleted = false;
		
		JSONObject jsonObj = new JSONObject(jsonStr);
		int fileId = jsonObj.getInt("fileId");
		BoardFile boardFile = boardFileService.find(fileId);
		String urlPath = Board.BOARD_FILE_PREFIX;
		isDeleted = boardFileService.deleteFile(boardFile, urlPath);
		logger.error("" + isDeleted);
		model.addAttribute("success", isDeleted);
		
		return model;
	}
	
	@RequestMapping(value="/{id}/deleteVideo", method = RequestMethod.POST)
	public @ResponseBody Model deleteVideo(@RequestBody String jsonStr,
			Model model, 
			@PathVariable int id,
			HttpServletRequest request) {
		
		boolean isDeleted = false;
		
		JSONObject jsonObj = new JSONObject(jsonStr);
		
		Board board= boardService.find(id);
		board.setVideoPath(""); 
		boardService.update(board);
		
		logger.error("" + isDeleted);
		model.addAttribute("success", isDeleted);
		
		return model;
	}
	
}
