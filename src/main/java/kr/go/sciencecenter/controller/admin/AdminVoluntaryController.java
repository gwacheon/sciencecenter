package kr.go.sciencecenter.controller.admin;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.model.Voluntary;
import kr.go.sciencecenter.model.VoluntaryTime;
import kr.go.sciencecenter.model.Volunteer;
import kr.go.sciencecenter.service.VoluntaryService;

@Controller
@RequestMapping("/admin/voluntaries")
public class AdminVoluntaryController {
	private static final Logger logger = LoggerFactory.getLogger(AdminVoluntaryController.class);
	
	@Autowired
	private VoluntaryService voluntaryService;
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String index(Model model) {
		List<Voluntary> voluntaries = voluntaryService.list();
		model.addAttribute("voluntaries", voluntaries);
		return "voluntary/index.adm";
	}
	
	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public String newVoluntary(Model model) {
		
		model.addAttribute("voluntary", new Voluntary());
		
		return "voluntary/new.adm";
	}
	
	
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String create(@ModelAttribute("voluntary") Voluntary voluntary
			, BindingResult result
			, Model model) {
		
		voluntaryService.create(voluntary);
		
		return "redirect:/admin/voluntaries/" + voluntary.getId() + "/times";
	}
	
	@RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
	public String edit(@PathVariable("id") int id
			, Model model) {
		
		Voluntary voluntary = voluntaryService.findTimesWithVolunteerCount(id, true, "desc");
		
		model.addAttribute("voluntary", voluntary);
		return "voluntary/edit.adm";
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public String update(@PathVariable("id") int id
			, @ModelAttribute("voluntary") Voluntary voluntary
			, BindingResult result
			, Model model) {
		voluntary.setId(id);
		voluntaryService.update(voluntary);
		model.addAttribute("voluntary", voluntary);
		return "redirect:/admin/voluntaries";
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public @ResponseBody Model delete(@PathVariable("id") int id
			, Model model) {
		voluntaryService.delete(id);
		
		model.addAttribute("isDeleted", true);
		
		return model;
	}
	
	@RequestMapping(value = "/{id}/times", method = RequestMethod.GET)
	public String times(@PathVariable("id") int id
			, Model model) {
		
		Voluntary voluntary = voluntaryService.findTimesWithVolunteerCount(id, true, "desc");
		
		model.addAttribute("voluntary", voluntary);
		return "voluntary/times.adm";
	}
	
	@RequestMapping(value = "/{voluntaryId}/times/{id}", method = RequestMethod.GET)
	public String volunteers(@PathVariable("voluntaryId") int voluntaryId, 
			@PathVariable("id") int timeId,
			@RequestParam(value="page", required=false) Integer currentPage,
			@RequestParam(value="searchType", required=false) String searchType,
			@RequestParam(value="searchKey", required=false) String searchKey,
			Model model){
		
		String path = "/admin/voluntaries/" + voluntaryId + "/times/" + timeId + "?";
		
		if(currentPage == null){
			currentPage = 1;
		}
		
		if(searchType != null && searchKey != null){
			path += "searchType=" + searchType + "&searchKey=" + searchKey +"&";
			model.addAttribute("searchType", searchType);
			model.addAttribute("searchKey", searchKey);
		}
		
		Page page = new Page(currentPage, 10, voluntaryService.volunteersCount(timeId));
		page.setPath(path);
		model.addAttribute("page", page);
		
		Voluntary voluntary = voluntaryService.find(voluntaryId);
		model.addAttribute("voluntary", voluntary);
		
		VoluntaryTime voluntaryTime = voluntaryService.findTimeWithVolunteers(timeId);
		model.addAttribute("voluntaryTime", voluntaryTime);
		
		String timeStatus = "regist";
		if(voluntaryTime.getEndTime().compareTo(new Date()) < 0){
			timeStatus = "complete";
		}
		model.addAttribute("timeStatus", timeStatus);
		
		return "voluntary/volunteers.adm";
	}
	
	@RequestMapping(value = "/volunteers/{volunteerId}", method = RequestMethod.GET)
	public Model volunteerDetail(@PathVariable("volunteerId") int volunteerId,
			Model model){
		
		Volunteer volunteer = voluntaryService.findVolunteer(volunteerId);
		model.addAttribute("volunteer", volunteer);
		
		return model;
	}
	
	@RequestMapping(value = "/volunteers/{volunteerId}/{status}", method = RequestMethod.POST)
	public Model setVolunteerStatus(@PathVariable("volunteerId") int volunteerId,
			@PathVariable("status") String status,
			Model model){
		
		Volunteer volunteer = voluntaryService.findVolunteer(volunteerId);
		volunteer.setStatus(status);
		
		voluntaryService.updateVolunteerStatus(volunteer);
		
		model.addAttribute("volunteer", volunteer);
		
		return model;
	}
	
	@RequestMapping(value = "/{id}/times", method = RequestMethod.POST)
	public @ResponseBody Model addTime(@RequestBody String jsonStr
			, Model model
			, @PathVariable int id){
		
		Voluntary voluntary = voluntaryService.find(id);
		
		JSONObject jsonObj = new JSONObject(jsonStr);
		
		VoluntaryTime voluntaryTime = new VoluntaryTime(id, jsonObj);
		voluntaryService.createTime(voluntary, voluntaryTime);
		
		model.addAttribute("voluntaryTime", voluntaryTime);
		
		return model;
	}	
	
	@RequestMapping(value = "/{voluntaryId}/times/{id}", method = RequestMethod.DELETE)
	public Model deleteTime(@PathVariable("voluntaryId") int voluntaryId, @PathVariable("id") int id, Model model){
		boolean error = false;
		int deletedRow = voluntaryService.deleteTime(id);
		
		if(deletedRow == 0){
			error = true;
		}
		
		model.addAttribute("error", error);
		
		return model;
	}
	
	@RequestMapping(value = "/{voluntaryId}/times/{id}/randomCast", method = RequestMethod.GET)
	public Model randomCast(@PathVariable("voluntaryId") int voluntaryId,
			@PathVariable("id") int id,
			Model model){
		
		List<Volunteer> volunteers = voluntaryService.randomCast(voluntaryId, id);
		model.addAttribute("castedVolunteers", volunteers);
		
		return model;
	}
	
	@RequestMapping(value = "/{voluntaryId}/times/{id}/excel", method = RequestMethod.GET)
	public ModelAndView excel(@PathVariable("voluntaryId") int voluntaryId,
			@PathVariable("id") int id,
			Model model){
		
		List<Volunteer> volunteers = voluntaryService.findVolunteers(id);
		
		return new ModelAndView("VolunteerExcelView", "volunteers", volunteers);
	}
	
	
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
}
