package kr.go.sciencecenter.controller.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import kr.go.sciencecenter.model.User;
import kr.go.sciencecenter.service.UserService;

@Controller
@RequestMapping(value="/admin/users")
public class AdminUserController {
  
  @Autowired
  UserService userService;
  
  @RequestMapping(value="", method=RequestMethod.GET)
  public String index(HttpServletRequest request, Model model){
    List<User> users = userService.list();
    model.addAttribute("users", users);
    
    return "users/index.admin";
  }
  
  @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
  public String delete(@PathVariable("id") int id,HttpServletRequest request){
    userService.delete(id);

    return "redirect:/admin/users";
  }
  
  @RequestMapping(value="/excelDownload", method=RequestMethod.GET)
  public ModelAndView getExcel(){
    
    return new ModelAndView("UserListExcelView","userList",userService.list());
  }
}
