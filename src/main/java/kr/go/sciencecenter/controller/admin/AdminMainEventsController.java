package kr.go.sciencecenter.controller.admin;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.go.sciencecenter.model.MainEvent;
import kr.go.sciencecenter.model.MainEventType;
import kr.go.sciencecenter.model.MainPageBanner;
import kr.go.sciencecenter.service.MainEventService;

/**
 * 대표문화행사 및 기타문화행사 관리
 * @author gunro
 *
 */
@Controller
@RequestMapping("/admin/mainEvents")
public class AdminMainEventsController {

	private static final Logger logger = LoggerFactory.getLogger(AdminMainEventsController.class);
	
	@Autowired
	MainEventService mainEventService;
	
	@ModelAttribute
	public void setCategories(Model model) {
		model.addAttribute("currentAdminCategory", "mainEvents");
	}
	
	/**
	 * 상위카테고리 및 하위 차수 인덱스 페이지
	 * @param model
	 * @param type
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String index(Model model,
			@RequestParam(value = "type", defaultValue="major")String type) {

		model.addAttribute("mainEvent", new MainEvent());
		model.addAttribute("mainEvents", mainEventService.listWithSeries(null));
		model.addAttribute("mainEventType", MainEventType.MAIN_EVENT_TYPES.get(type));
		return "mainEvents/index.adm";
	}
	
	/**
	 * 각 mainEvent 의 순서 설정 페이지
	 * @param model
	 * @param type
	 * @return
	 */
	@RequestMapping(value = "/editSequence/{type}", method = RequestMethod.GET)
	public @ResponseBody Model editSeq(@PathVariable String type, 
								Model model) {

		model.addAttribute("mainEvents", mainEventService.listWithSeries(null));
		
		return model;
	}
	
	/**
	 * 순서 변경
	 * @param model
	 * @param jsonStr
	 * @return
	 */
	@RequestMapping(value = "/sortable", method = RequestMethod.POST)
	public @ResponseBody Model sortable(Model model, @RequestBody String jsonStr){
		System.out.println(jsonStr);
		JSONObject jsonObj = new JSONObject(jsonStr);
		JSONArray array = jsonObj.getJSONArray("data");
		
		for(int i=0; i< array.length() ; i++){
			int id= array.getInt(i);
			MainEvent mainEvent= mainEventService.find(id);
			
			mainEvent.setSeq(i);
			mainEventService.update(mainEvent);
			
		}
		
		return model;
	}

	/**
	 * 메인이벤트 카테고리 생성
	 * @param mainEvent
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String create(@ModelAttribute("mainEvent") MainEvent mainEvent, Model model) {

		mainEventService.create(mainEvent);
		
		return "redirect:/admin/mainEvents";
	}
	
	/**
	 * 카테고리 수정
	 * @param mainEvent
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/update")
	public String update(@ModelAttribute("mainEvent") MainEvent mainEvent, Model model) {
		mainEventService.update(mainEvent);

		return "redirect:/admin/mainEvents";
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public String deleteMainEvent(@PathVariable("id")int mainEventId, Model model) {
		MainEvent mainEvent = mainEventService.find(mainEventId);
		mainEventService.delete(mainEventId);

		return "redirect:/admin/mainEvents";
	}
}
