package kr.go.sciencecenter.controller.admin;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import kr.go.sciencecenter.model.Board;
import kr.go.sciencecenter.model.Event;
import kr.go.sciencecenter.model.EventFile;
import kr.go.sciencecenter.model.EventType;
import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.service.BoardService;
import kr.go.sciencecenter.service.EventFileService;
import kr.go.sciencecenter.service.EventsService;

/**
 * 행사/공연/캠프 관리
 * @author Prompt Technology
 *
 */
@Controller
@RequestMapping("/admin/events")
public class AdminEventsController {
	
	private static final Logger logger = LoggerFactory.getLogger(AdminEventsController.class);
	
	@Autowired
	EventsService eventsService;
	
	@Autowired
	EventFileService eventFileService;
	
	@Autowired
	Environment environment;
	
	@ModelAttribute
	public void setCategories(Model model) {
		model.addAttribute("currentAdminCategory", "events");
	}
	
	/**
	 * 행사/과학문화공연/캠프 Index 
	 * @param type
	 * @param model
	 * @param currentPage
	 * @return
	 */
	@RequestMapping(value = "/list/{type}", method = RequestMethod.GET)
	public String index(@PathVariable("type") String type, Model model,
			@RequestParam(value = "page", defaultValue = "1") Integer currentPage) {

		String path = "/admin/events/list/"+type+"?";
		Page page = new Page(currentPage,6,eventsService.countAll(type));
		page.setPath(path);

		List<Event> events = eventsService.listAll(type, page);
		
		model.addAttribute("events", events);
		model.addAttribute("eventTypes", EventType.EVENT_TYPES);
		model.addAttribute("eventType", EventType.EVENT_TYPES.get(type));
		model.addAttribute("page", page);

		return "events/index.adm";
	}
	
	/**
	 * 행사/과학문화공연/캠프 상세보기
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String show(Model model,@PathVariable int id){
		Event event = eventsService.find(id);

		model.addAttribute("prevEvent", eventsService.prevEvent(id, event.getType()));
		model.addAttribute("event", event);
		model.addAttribute("nextEvent", eventsService.nextEvent(id, event.getType()));
		
		model.addAttribute("eventTypes", EventType.EVENT_TYPES);
		model.addAttribute("eventType", EventType.EVENT_TYPES.get(event.getType()));

		return "events/show.adm";
	}
	
	/**
	 * 행사/과학문화공연/캠프 생성 페이지
	 * @param model
	 * @param type
	 * @return
	 */
	@RequestMapping(value = "/{type}/new", method = RequestMethod.GET)
	public String newEvent(Model model,
			@PathVariable("type") String type) {
		Event event = new Event(type);
		model.addAttribute("event", event);
		
		model.addAttribute("eventTypes", EventType.EVENT_TYPES);
		model.addAttribute("eventType", EventType.EVENT_TYPES.get(type));
		
		return "events/new.adm";
	}

	/**
	 * 행사/과학문화공연/캠프 수정 페이지
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public String edit(Model model, @PathVariable int id) {

		Event event = eventsService.find(id);
		model.addAttribute("event", event);
		
		model.addAttribute("eventTypes", EventType.EVENT_TYPES);
		model.addAttribute("eventType", EventType.EVENT_TYPES.get(event.getType()));
		return "events/edit.adm";
	}
	
	/**
	 * Event Create
	 * @param event Event 모델
	 * @param result
	 * @param model
	 * @param request
	 * @param file 대표이미지 File
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String create(@ModelAttribute("event") Event event, BindingResult result, Model model,
			HttpServletRequest request, 
			@RequestParam(value = "file", required = false) MultipartFile file) {
		
		eventsService.create(event, file);
		return "redirect:/admin/events/list/" + event.getType();
	}

	/**
	 * Event Update
	 * @param event
	 * @param result
	 * @param model
	 * @param request
	 * @param file
	 * @return
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.POST)
	public String update(@ModelAttribute("event") Event event,
			@PathVariable int id,
			Model model,
			@RequestParam(value = "file", required = false) MultipartFile file) {

		eventsService.update(event, file);
		return "redirect:/admin/events/list/" + event.getType();
	}
	
	/**
	 * Event 삭제
	 * @param id
	 * @param type
	 * @param mainEventSeriesId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/{id}/delete", method = RequestMethod.DELETE)
	public String delete(@PathVariable int id, Model model) {
		
		Event event = eventsService.find(id);
		
		eventsService.delete(id);
		
		return "redirect:/admin/events/list/" + event.getType();
	}
	
	
	/**
	 * 첨부파일 삭제
	 * 
	 * @param jsonStr
	 * @param model
	 * @param id
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/{id}/deleteFile", method = RequestMethod.POST)
	public @ResponseBody Model deleteAttachmentFile(@RequestBody String jsonStr,
			Model model, 
			@PathVariable int id,
			HttpServletRequest request) {
		
		boolean isDeleted = false;
		
		JSONObject jsonObj = new JSONObject(jsonStr);
		int fileId = jsonObj.getInt("fileId");
		EventFile eventFile = eventFileService.find(fileId);
		String urlPath = Event.EVENT_FILE_PREFIX;
		isDeleted = eventFileService.deleteFile(eventFile, urlPath);
		
		model.addAttribute("success", isDeleted);
		
		return model;
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
}
