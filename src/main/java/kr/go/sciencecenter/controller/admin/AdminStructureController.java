package kr.go.sciencecenter.controller.admin;

import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.go.sciencecenter.model.Structure;
import kr.go.sciencecenter.model.StructureUnavailable;
import kr.go.sciencecenter.service.StructureService;
import kr.go.sciencecenter.util.FileUploadHelper;
import kr.go.sciencecenter.util.PropertyHelper;

@Controller
@RequestMapping("/admin/structures")
public class AdminStructureController {
	private static final Logger logger = LoggerFactory.getLogger(AdminStructureController.class);

	@Autowired
	private StructureService structureService;
	
	@Autowired
	PropertyHelper propertyHelper;
	
	@Autowired
	FileUploadHelper fileUploadHelper;
	
	Long STRUCTURE_KEY;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute("fileRootUrl", propertyHelper.getFileRootUrl());
		
		List<Structure> structures = structureService.list();
		model.addAttribute("structures", structures);
		return "structures/index.adm";
	}

	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public String newStructure(Model model
			, HttpSession session) {
		setStructureKey();
		
		model.addAttribute("structure", new Structure());
		model.addAttribute("categories", Structure.CATEGORIES);
		return "structures/new.adm";
	}
	
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String create(@ModelAttribute("structure") Structure structure
			, BindingResult result
			, Model model
			, HttpSession session) {
		Long lastStructureKey = (Long) session.getAttribute("LAST_STRUCTURE_KEY");
		
		if (lastStructureKey == null || !lastStructureKey.equals(STRUCTURE_KEY)) {
			session.setAttribute("LAST_STRUCTURE_KEY", STRUCTURE_KEY);
			
			structureService.create(structure);
			if (structure.getFile() != null && structure.getFile().getSize() > 0) {
				String subPath = Structure.FILE_PREFIX + "/" + structure.getId();
				String fileName = Structure.DEFAULT_FILE_NAME + "." + FilenameUtils.getExtension(structure.getFile().getOriginalFilename());
				
				fileUploadHelper.upload(structure.getFile(), subPath, fileName);
				
				structure.setPicture(subPath + "/" + fileName);
			}
			structureService.update(structure);
		}

		return "redirect:/admin/structures";
	}

	@RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
	public String edit(@PathVariable("id") int id, Model model) {
		setStructureKey();
		
		model.addAttribute("structure", structureService.find(id));
		model.addAttribute("categories", Structure.CATEGORIES);
		return "structures/edit.adm";
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.POST)
	public String update(@ModelAttribute("structure") Structure structure
			, @PathVariable("id") int id
			, BindingResult result
			, Model model
			, HttpSession session) {

		Long lastStructureKey = (Long) session.getAttribute("LAST_STRUCTURE_KEY");
		
		if (lastStructureKey == null || !lastStructureKey.equals(STRUCTURE_KEY)) {
			structure.setId(id);

			if (structure.getFile() != null && structure.getFile().getSize() > 0) {
				String subPath = Structure.FILE_PREFIX + "/" + structure.getId();
				String fileName = Structure.DEFAULT_FILE_NAME + "." + FilenameUtils.getExtension(structure.getFile().getOriginalFilename());
				
				fileUploadHelper.upload(structure.getFile(), subPath, fileName);
				
				structure.setPicture(subPath + "/" + fileName);
			}
			
			structureService.update(structure);
		}

		return "redirect:/admin/structures";
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public String delete(@PathVariable("id") int id, Model model) {
		structureService.delete(id);

		return "redirect:/admin/structures";
	}

	@RequestMapping(value = "/{id}/calendars", method = RequestMethod.GET)
	public String calendars(@PathVariable("id") int id, Model model) {
		Structure structure = structureService.find(id);

		model.addAttribute("structure", structure);

		return "structures/calendars.adm";
	}

	@RequestMapping(value = "/{id}/calendars/dates", method = RequestMethod.GET)
	public Model unavailableDates(@PathVariable("id") int id, @RequestParam("startDateStr") String startDateStr,
			@RequestParam("endDateStr") String endDateStr, Model model) {

		List<StructureUnavailable> unavailables = structureService.listInDates(id, startDateStr, endDateStr);

		model.addAttribute("unavailables", unavailables);

		return model;
	}

	@RequestMapping(value = "/{id}/calendars/dates", method = RequestMethod.POST)
	public @ResponseBody Model addUnavailableDates(@PathVariable("id") int id, @RequestBody String jsonStr,
			Model model) {

		StructureUnavailable unavailable = new StructureUnavailable(id, jsonStr);

		structureService.addUnavailableDate(unavailable);

		model.addAttribute("unavailable", unavailable);

		return model;
	}
	
	private void setStructureKey() {
		STRUCTURE_KEY = (new Random()).nextLong();
	}
}
