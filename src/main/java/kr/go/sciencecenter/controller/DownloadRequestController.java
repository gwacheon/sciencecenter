package kr.go.sciencecenter.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/downloadRequest")
public class DownloadRequestController {
	@Value("${webroot.path}") private String fileRoot;
	@Value("${legacy.companycd}") private String companycd;
	
	@RequestMapping(value="", method=RequestMethod.GET)
    public void basicScience(Model model, @RequestParam(value = "filePath", required = true ) String filePath, 
    		HttpServletRequest request,
    		HttpServletResponse response){
    	
		FileInputStream inputStream;
		OutputStream outStream;

		try {
			
			String mimeType = "application/octet-stream";
			
			filePath = fileRoot + companycd + "/" + filePath;
			String fileName = FilenameUtils.getName(filePath);
			fileName = URLEncoder.encode(fileName, "utf-8");
			File downloadFile = new File(filePath);
			
			inputStream = new FileInputStream(downloadFile);
			
			response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\";");
			response.setHeader("Content-Transfer-Encoding", "binary");
	    	response.setContentType(mimeType);
	        response.setContentLength((int) downloadFile.length());
	        
	        outStream = response.getOutputStream();
	        
	        byte[] buffer = new byte[4096];
	        int bytesRead = -1;
			
	        // write bytes read from the input stream into the output stream
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }
	        
	        inputStream.close();
	        outStream.close();
	        
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
}
