package kr.go.sciencecenter.controller;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ExceptionHandlingController {
	private static final Logger logger = LoggerFactory.getLogger(ExceptionHandlingController.class);
	
	@RequestMapping(value = "/400")
	public String error400(HttpServletRequest request) {
		printLog(request);
		return "errors/400.app";
	}
	
	@RequestMapping(value = "/404")
	public String error404(HttpServletRequest request) {
		printLog(request);
		return "errors/404.app";
	}
	
	@RequestMapping(value = "/500")
	public String error500(HttpServletRequest request) {
		printLog(request);
		return "errors/500.app";
	}
	
	private void printLog(HttpServletRequest request) {		
		Exception e = (Exception) request.getAttribute("javax.servlet.error.exception");
		StringWriter errors = new StringWriter();
		if (e != null) {
			e.printStackTrace(new PrintWriter(errors));
			logger.error("ERROR : " + errors.toString());
		}
	}
}
