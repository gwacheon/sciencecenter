package kr.go.sciencecenter.controller;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.co.mainticket.Entity.Entity;
import kr.co.mainticket.Utility.Box;
import kr.co.mainticket.Utility.DateTime;
import kr.co.mainticket.Utility.HttpUtility;
import kr.co.mainticket.Utility.Utility;
import kr.go.sciencecenter.model.api.Member;
import kr.go.sciencecenter.service.LectureService;
import kr.go.sciencecenter.service.PaymentCheckService;
import kr.go.sciencecenter.service.PaymentService;
import kr.go.sciencecenter.service.SmsEmailService;
import kr.go.sciencecenter.service.UserService;
import kr.go.sciencecenter.util.Paging;
import kr.go.sciencecenter.util.PropertyHelper;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/payment")
public class PaymentController {
	private static final Logger logger = LoggerFactory.getLogger(PaymentController.class);
	
	@Autowired PaymentService 		paymentService;
	@Autowired PaymentCheckService 	paymentCheckService;
	@Autowired LectureService 		lectureService;
	@Autowired UserService 			userService;
	@Autowired SmsEmailService		smsEmailService;
	@Autowired PropertyHelper 		propertyHelper;
	
	/**
	 * 웹 / 모바일 구분
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private boolean isMobile(HttpServletRequest request)throws Exception{
		String userAgent = request.getHeader("user-agent");
		boolean mobile1 = userAgent.matches(".*(iPhone|iPod|Android|Windows CE|BlackBerry|Symbian|Windows Phone|webOS|Opera Mini|Opera Mobi|POLARIS|IEMobile|lgtelecom|nokia|SonyEricsson).*");
		boolean mobile2 = userAgent.matches(".*(LG|SAMSUNG|Samsung).*");
		if(mobile1 || mobile2){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * 모바일 결제시
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	@RequestMapping(value="/paymentMobileModule")
	public String paymentMobileModule(Model model,HttpServletRequest request)throws Exception{
		
		logger.info("PaymentController.paymentMobileModule -- !");
		
		Box params = HttpUtility.getBox(request);
		Map<String,Object> paraMap = new HashMap<String,Object>();
		
		String ApplyUniqueNbr = params.getString("APPLY_UNIQUE_NBR");
		String SaleType = "000005";//params.getString("SALE_TYPE");
		String rsCode = params.getString("resCode");
		String rstCode = params.getString("rstCode");
		
		Map<String,Object> rsMap = new HashMap<String,Object>();
		
		paraMap.put("MEMBER_NO", params.getString("MEMBER_NO"));
		paraMap.put("APPLY_UNIQUE_NBR", params.getString("APPLY_UNIQUE_NBR"));
		paraMap.put("SALE_TYPE", SaleType);
		paraMap.put("TOTAL_PRICE", params.getString("TOTAL_PRICE"));
		paraMap.put("APPLY_CNT", params.getString("APPLY_CNT"));
		paraMap.put("COURSE_NAME", params.getString("COURSE_NAME"));
		paraMap.put("CARD_APPROVE_DATE", params.getString("cardApprovDate"));
		paraMap.put("CARD_APPROVE_NO", params.getString("cardApprovNo"));
		paraMap.put("CARD_TRADE_NO", params.getString("cardTradeNo"));
		paraMap.put("CARD_NAME", Utility.CheckNull(request.getParameter("cardName")));
		paraMap.put("PAY_KIND", params.getString("payKind"));
		paraMap.put("PAY_TYPE", params.getString("payType"));
		paraMap.put("VACCOUNT", params.getString("vAccount"));
		paraMap.put("VACCOUNT_BANK_NAME", Utility.CheckNull(request.getParameter("vAccountBankName")));
		
		if( params.getString("cardTradeNo").equals("")){
			paraMap.put("CARD_TRADE_NO", params.getString("vAccountTradeNo"));
		}
		
		System.out.println("param :"+params.toString());
		Entity ApplyEntity = new Entity(15);
		
		ApplyEntity.setField("SALE_TYPE", SaleType);
		ApplyEntity.setField("WIN_TYPE", "000000");
		ApplyEntity.setField("SMS_SEND_FLAG", params.getString("SMS_SEND_FLAG"));
		ApplyEntity.setField("EMAIL_SEND_FLAG", params.getString("EMAIL_SEND_FLAG"));
		ApplyEntity.setField("APPLY_UNIQUE_NBR", ApplyUniqueNbr);
		ApplyEntity.setField("WORKER_MEMBER_NO", getCurrentMemberNo(request));
		ApplyEntity.setField("CLIENT_IP", request.getRemoteAddr());
		
		ApplyEntity.setField("PROCESS_TYPE", "PAY");
		
		String DeductFlag = "N";
		String DeductMemo = "";
		long DeductPrice = 0;
				
		ApplyEntity.setField("DEDUCT_FLAG", DeductFlag);
		ApplyEntity.setField("DEDUCT_MEMO", DeductMemo);
		ApplyEntity.setField("DEDUCT_PRICE", Long.toString(DeductPrice));
		
		long TotalPrice = Utility.StringToLong(params.getString("TOTAL_PRICE").replaceAll(",", ""));
		
		if(rsCode.equals("0000") || rstCode.equals("0000")){
			if(TotalPrice > 0){
				long PaymentAmt = Utility.StringToLong(params.getString("PAYMENT_AMT").replaceAll(",", ""));
				
				Entity PaymentList = new Entity(3);
				Entity PaymentItem = null;
				
				String KeyinType = "";
				String TrackData = "";
				
				if(PaymentAmt > 0){
					String PayKind = params.getString("payKind");
					String PaymentType ="";
					String PaySvcType = "";
					String InstallNo = "";
					String ValidMonth = "";
					String PaymentRegNo = "";
					String PaymentCertifyType = "1";	// 개인 / 사업자 구분 UI 바뀌면서 구분값 없음
					String BankCd = "";
					String Depositor = "";
					String cardPassword = "";
					String cavv = "";
					KeyinType = "";
					TrackData = "";
					
					if(PayKind.equals("CSQ") || PayKind.equals("1")){			// 신용카드
						PaySvcType = "002";
						KeyinType = "K";	
						PaymentType = "000002";
					}
					else if(PayKind.equals("2") || PayKind.equals("vAccount")){	// 가상계좌
						
						PaymentRegNo = "0000000000000";
						BankCd = params.getString("VA_BANK_CD");
						PaySvcType = params.getString("PAY_TYPE");
						KeyinType = "K";
						TrackData = "0100001234";
						PaymentCertifyType = "01";
						PaymentType = "000003";
					}
					
					PaymentItem = new Entity(10);
					
					PaymentItem.setField("PAYMENT_TYPE", PaymentType);
					PaymentItem.setField("PAY_SVC_TYPE", PaySvcType);
					PaymentItem.setField("PAYMENT_AMT", Long.toString(PaymentAmt));
					PaymentItem.setField("KEYIN_TYPE", KeyinType);	
					PaymentItem.setField("CERTIFY_TYPE", PaymentCertifyType);
					PaymentItem.setField("BANK_CD", BankCd);
					PaymentItem.setField("DEPOSITOR", Depositor);
					
					PaymentList.addField("PAYMENT_ITEM", PaymentItem);
				}
				
				ApplyEntity.setField("PAYMENT_LIST", PaymentList);
			}
		
			rsMap = paymentCheckService.CourseApplyPay(propertyHelper.getCompanycd(), ApplyEntity, paraMap);
			
			model.addAttribute("result",rsMap.get("result"));
			model.addAttribute("Msg",(String)rsMap.get("Msg"));
			model.addAttribute("APPLY_UNIQUE_NBR",ApplyUniqueNbr);
			System.out.println("param :"+model.toString());
		}
		else{
			model.addAttribute("result",false);
			model.addAttribute("Msg",params.getString("resMsg"));
		}

		return "schedules/showDummy.app";
	}
	
	/**
	 * 결제 페이지
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="")
	public String programList(Model model,HttpServletRequest request)throws Exception{
		
		logger.info("PaymentController.programList -- !");
		
		Map<String,Object> params = new HashMap<String,Object>();
		String saleType ="";
		String pageNum = Utility.CheckNull(request.getParameter("pageNum"));
		String SearchName = Utility.CheckNull(request.getParameter("SEARCH_NAME"));
		String StatusType = Utility.CheckNull(request.getParameter("STATUS_TYPE"));
    	
    	int pageSize = 10;
    	    	
    	if(pageNum.equals("")){
    		pageNum = "1";
    	}
    	int CurPage = Integer.parseInt(pageNum);
    	
    	int BlockPage = 10;
    	int StartPage = pageSize*(CurPage-1)+1;
    	int EndPage = pageSize*CurPage;
    	int TotCnt = 0;
    			
		if(isMobile(request)){
			saleType = "000005";
		}else{
			saleType = "000002";
		}
				
		params.put("COMPANY_CD",propertyHelper.getCompanycd());
		params.put("MEMBER_NO",getCurrentMemberNo(request));
		params.put("StartPage", StartPage);
		params.put("EndPage", EndPage);
		params.put("SearchName", SearchName);
		params.put("StatusType", StatusType);
		
		TotCnt = paymentService.getPaymentListCount(params);
		
		Paging pg = new Paging(CurPage, TotCnt, pageSize, BlockPage);
		model.addAttribute("PaymentList",paymentService.getPaymentList(params));
		model.addAttribute("VatList", paymentService.getVatList());
		model.addAttribute("SALE_TYPE",saleType);
		model.addAttribute("MEMBER_NO",getCurrentMemberNo(request));
		model.addAttribute("pg",pg.showBlock_page());
		model.addAttribute("pageNum",pageNum);
		
		model.addAttribute("pay_bizNo", propertyHelper.getPay_bizNo());
		model.addAttribute("pay_returnType", propertyHelper.getPay_returnType());
		model.addAttribute("pay_version", propertyHelper.getPay_version());
		model.addAttribute("pay_targetUrl", propertyHelper.getPay_targetUrl());
		model.addAttribute("pay_authType", propertyHelper.getPay_authType());
		model.addAttribute("pay_server", propertyHelper.getPay_server());
		model.addAttribute("c2_homepage_url", propertyHelper.getC2_homepage_url());
		
		Map m = new HashMap();
		m.put("COMPANY_CD", propertyHelper.getCompanycd());
		m.put("MEMBER_NO", getCurrentMemberNo(request));
		Map rm = (Map) userService.getLegacyUser(m);
		model.addAttribute("MEMBER_NAME", rm.get("MEMBER_NAME"));
		String member_cel = "";
		if(rm.get("MEMBER_CEL") != null) member_cel = rm.get("MEMBER_CEL").toString().replaceAll("-", "");
		model.addAttribute("MEMBER_CEL", member_cel);
		model.addAttribute("MEMBER_EMAIL", rm.get("MEMBER_EMAIL"));
		
		return "myPayment/paymentList.app";
	}
	
	/**
	 * 결제 페이지
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/Search")
	public String programSearch(Model model,HttpServletRequest request,HttpServletResponse response)throws Exception{
		
		logger.info("PaymentController.programSearch -- !");
		
		Map<String,Object> params = new HashMap<String,Object>();
		
		String saleType ="";
		String pageNum = Utility.CheckNull(request.getParameter("pageNum"));
		String SearchName = Utility.CheckNull(request.getParameter("SEARCH_NAME"));
		String StatusType = Utility.CheckNull(request.getParameter("STATUS_TYPE"));
    	
		
    	int pageSize = 10;
    	    	
    	if(pageNum.equals("")){
    		pageNum = "1";
    	}
    	int CurPage = Integer.parseInt(pageNum);
    	
    	int BlockPage = 10;
    	int StartPage = pageSize*(CurPage-1)+1;
    	int EndPage = pageSize*CurPage;
    	int TotCnt = 0;
    			
		if(isMobile(request)){
			saleType = "000005";
		}else{
			saleType = "000002";
		}
				
		params.put("COMPANY_CD",propertyHelper.getCompanycd());
		params.put("MEMBER_NO",getCurrentMemberNo(request));
		params.put("StartPage", StartPage);
		params.put("EndPage", EndPage);
		params.put("SearchName", SearchName);
		params.put("StatusType", StatusType);
		
		TotCnt = paymentService.getPaymentListCount(params);
		
		Paging pg = new Paging(CurPage, TotCnt, pageSize, BlockPage);
		model.addAttribute("PaymentList",paymentService.getPaymentList(params));
		model.addAttribute("VatList", paymentService.getVatList());
		model.addAttribute("SALE_TYPE",saleType);
		model.addAttribute("pg",pg.showBlock_page());
		model.addAttribute("pageNum",pageNum);
		model.addAttribute("SearchName",SearchName);
		model.addAttribute("StatusType",StatusType);
				
		return "myPayment/paymentList.app";
	}
	
	/**
	 * 예약증
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="/ReceiptPay")
	public @ResponseBody Model ReceiptPay(Model model,HttpServletRequest request,HttpServletResponse response)throws Exception{
		
		logger.info("PaymentController.ReceiptPay -- !");
		
		Box params = HttpUtility.getBox(request);
		Map<String,Object> param = new HashMap<String,Object>();
		
		param.put("COMPANY_CD", propertyHelper.getCompanycd());
		param.put("APPLY_UNIQUE_NBR",params.getString("APPLY_UNIQUE_NBR"));
		
		List list = lectureService.getCompleteInfo(param);
		
		if(!list.isEmpty()){
			model.addAttribute("CompleteInfo",list.get(0));
		}
		
		model.addAttribute("ApplyUniqueNbr",params.getString("APPLY_UNIQUE_NBR"));		
		return model;
	}
	
	/**
	 * 영수증출력 페이지
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="/Receipe")
	public @ResponseBody Model Receipe(Model model,HttpServletRequest request,HttpServletResponse response)throws Exception{
		
		logger.info("PaymentController.Receipe -- !");
		
		Box params = HttpUtility.getBox(request);
		Map<String,Object> param = new HashMap<String,Object>();
		
		param.put("APPLY_UNIQUE_NBR",params.getString("APPLY_UNIQUE_NBR"));
		param.put("APPLY_NBR", "1");
		
		List TranInfo = paymentService.getTranPaymentList(param);
		if(!TranInfo.isEmpty()){
			param = (Map)TranInfo.get(0);
			param.put("COMPANY_CD",propertyHelper.getCompanycd());
		}
		
		model.addAttribute("TranPaymentInfo",paymentService.getReceiptInfo(param));
		return model;
	}
	
	/**
	 * 예약증 프린트 업데이트
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes", "unused" })
	@RequestMapping(value="/paymentPrint")
	public @ResponseBody Model paymentPrint(Model model,HttpServletRequest request,HttpServletResponse response)throws Exception{

		logger.info("PaymentController.paymentPrint -- !");
		
		Box params = HttpUtility.getBox(request);
		Map<String,Object> param = new HashMap<String,Object>();
		Map<String,Object> rsMap = new HashMap<String,Object>();
		int result = 0;
		
		param.put("COMPANY_CD", propertyHelper.getCompanycd());
		param.put("APPLY_UNIQUE_NBR",params.getString("APPLY_UNIQUE_NBR"));
		
		List list = paymentService.getPrintFlag(param);
		
		if(!list.isEmpty()){
			rsMap = (Map)list.get(0);
			if(((String)rsMap.get("PRINT_FLAG")).equals("N")){
				result = paymentService.getPrintFlagUpdate(param);
				model.addAttribute("result",true);
			}else{
				model.addAttribute("result",false);
				model.addAttribute("Msg","이미 예약증을 인쇄했습니다.\n예약증은 1회만 인쇄가 가능합니다.");
			}
		}
		
		return model;
	}
	
	/**
	 * 수강생 인원 정보
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/MemberInfo")
	public @ResponseBody Model MemberInfo(Model model,HttpServletRequest request)throws Exception{
		
		logger.info("PaymentController.MemberInfo -- !");
		
		Box params = HttpUtility.getBox(request);
		Map<String,Object> param = new HashMap<String,Object>();
		
		String ApplyUniqueNbr = params.getString("APPLY_UNIQUE_NBR");
		String CourseCd		  = params.getString("COURSE_CD");
		
		param.put("MEMBER_NO", getCurrentMemberNo(request));
		param.put("APPLY_UNIQUE_NBR", ApplyUniqueNbr);
		param.put("COURSE_CD", CourseCd);
		
		model.addAttribute("MemberInfo",paymentService.MemberInfoList(param));
		return model;
	}
	
	/**
	 * 환불 페이지
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="/RefundPay")
	public @ResponseBody Model RefundPay(Model model,HttpServletRequest request)throws Exception{
		
		logger.info("PaymentController.RefundPay -- !");
		
		Box params = HttpUtility.getBox(request);
		Map<String,Object> param = new HashMap<String,Object>();
		
		param.put("COMPANY_CD", propertyHelper.getCompanycd());
		param.put("APPLY_UNIQUE_NBR",params.getString("APPLY_UNIQUE_NBR"));
		
		List InfoList = lectureService.getCompleteInfo(param);
		
		if(!InfoList.isEmpty()){
			model.addAttribute("CompleteInfo",InfoList.get(0));
			model.addAttribute("result",true);
		}else{
			model.addAttribute("result",false);
			model.addAttribute("Msg","정보 데이터가 없습니다.");
		}
		
		List BankList = paymentService.getBankList();
		
		param.put("APPLY_NBR", "1");
		
		List tranPaymentList = paymentService.getTranPaymentList(param);

		if(tranPaymentList.isEmpty()){
			model.addAttribute("result",true);
			model.addAttribute("Msg","결제 정보가 없습니다.");
		}else{
			model.addAttribute("result",true);
		}
		model.addAttribute("ApplyUniqueNbr", params.getString("APPLY_UNIQUE_NBR"));
		model.addAttribute("BankList", BankList);
		model.addAttribute("TranPaymentList", tranPaymentList);
		
		return model;
	}
	
	/**
	 * 결제 데이터
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/CardRedfundData")
	public @ResponseBody Model CardRedfundData(Model model,HttpServletRequest request)throws Exception{
		
		logger.info("PaymentController.CardRedfundData -- !");
		
		Box params = HttpUtility.getBox(request);	
		String ApplyUniqueNbr = params.getString("APPLY_UNIQUE_NBR");
		Map<String,Object> paraMap = new HashMap<String,Object>();
		
		paraMap.put("APPLY_UNIQUE_NBR", ApplyUniqueNbr);
		model.addAttribute("PaymentInfo",paymentService.PAYMENT_DATA(paraMap));
		
		return model;
	}
	
	@RequestMapping(value="/CourseApplyCardRefund_1")
	public ModelAndView CourseApplyCardRefund_1(Model model,HttpServletRequest request)throws Exception{
		
		logger.info("PaymentController.CourseApplyCardRefund_1 -- !");
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("mbrId",request.getParameter("mbrId"));
		mav.addObject("mbrName",request.getParameter("mbrName"));
		mav.addObject("cardTradeNo",request.getParameter("cardTradeNo"));
		mav.addObject("cardApprovDate",request.getParameter("cardApprovDate"));
		mav.addObject("salesPrice",request.getParameter("salesPrice"));
		mav.addObject("payType",request.getParameter("payType"));
		
		return new ModelAndView("http://testpg.mainpay.co.kr:8080/csStdPayment/cardCancel.do");
	}
	
	/**
	 * 승인취소요청  API
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="/CourseCardRefundJson")
	public @ResponseBody Model CourseCardRefundJson(Model model,HttpServletRequest request)throws Exception{

		logger.info("PaymentController.CourseCardRefundJson -- !" + HttpUtility.getBox(request).toString());
		
		String Url = propertyHelper.getPay_cancel();
		HttpClient  client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(Url);
		JSONObject obj = null;
		JSONParser parser = new JSONParser();
		InputStream is = null;
		String payType = Utility.CheckNull(request.getParameter("payType"));
//		if(!payType.equals("3D"))	payType = "CSQ";

		List urlParameters = new ArrayList();
		
		urlParameters.add(new BasicNameValuePair("mbrId", request.getParameter("mbrId")));
		urlParameters.add(new BasicNameValuePair("mbrName", request.getParameter("mbrName")));
		urlParameters.add(new BasicNameValuePair("cardTradeNo", request.getParameter("cardTradeNo")));
		urlParameters.add(new BasicNameValuePair("cardApprovDate", request.getParameter("cardApprovDate")));
		urlParameters.add(new BasicNameValuePair("salesPrice", request.getParameter("salesPrice")));
		urlParameters.add(new BasicNameValuePair("buyerName", request.getParameter("buyerName")));
		urlParameters.add(new BasicNameValuePair("buyerMobile", request.getParameter("buyerMobile")));
		urlParameters.add(new BasicNameValuePair("productName", request.getParameter("productName")));
		urlParameters.add(new BasicNameValuePair("payType",payType));
		
		UrlEncodedFormEntity entity = new UrlEncodedFormEntity(urlParameters,"UTF-8");
		post.setEntity(entity);
		
		try{
			HttpResponse rs = client.execute(post);
			HttpEntity resEntity = rs.getEntity();
			is = resEntity.getContent();
			
			BufferedReader buf = new BufferedReader(new InputStreamReader(is,"UTF-8"));
			String line = null;
			String page = "";
			
			while((line = buf.readLine()) !=null){
				page =line;	
			}		
			is.close();	
			
			obj = (JSONObject)parser.parse(page);
			
			logger.info("PaymentController.CourseCardRefundJson -- obj" + obj.toJSONString());
			
			model.addAttribute("result",obj.get("resultCode"));
			model.addAttribute("Msg",obj.get("resultMsg"));
		}catch(Exception ce){
			model.addAttribute("result",false);
			model.addAttribute("Msg","취소처리 중 오류가 발생하였습니다.");
		}
		
		return model;
	}
		
	/**
	 * 카드 환불 하기
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="/CourseApplyCardRefund")
	public @ResponseBody Model CourseApplyCardRefund(Model model,HttpServletRequest request)throws Exception{
		
		logger.info("PaymentController.CourseApplyCardRefund -- !");
		
		Box params = HttpUtility.getBox(request);
		
		String ApplyUniqueNbr = params.getString("APPLY_UNIQUE_NBR");
		String SaleType = params.getString("SALE_TYPE");
		
		Map<String,Object> ProcessResult = new HashMap<String,Object>();
		
		Entity ApplyEntity = new Entity(15);
		
		ApplyEntity.setField("SALE_TYPE", SaleType);
		ApplyEntity.setField("WIN_TYPE", "000000");
		ApplyEntity.setField("SMS_SEND_FLAG", params.getString("SMS_SEND_FLAG"));
		ApplyEntity.setField("EMAIL_SEND_FLAG", params.getString("EMAIL_SEND_FLAG"));
		ApplyEntity.setField("APPLY_UNIQUE_NBR", ApplyUniqueNbr);
		ApplyEntity.setField("WORKER_MEMBER_NO", getCurrentMemberNo(request));
		ApplyEntity.setField("CLIENT_IP", request.getRemoteAddr());
		
		ApplyEntity.setField("PROCESS_TYPE", "REFUND");
		ApplyEntity.setField("CONFIRM_FLAG", "Y");
		
		ProcessResult = paymentCheckService.CourseApplyRefund(propertyHelper.getCompanycd(), ApplyEntity);
		
		model.addAttribute("result",ProcessResult.get("result"));
		model.addAttribute("Msg",(String)ProcessResult.get("Msg"));
		
		//예약환불(L10002)
		if((Boolean) ProcessResult.get("result")){
			Map smsMap = new HashMap();
			smsMap.put("CONTENTS_TYPE", "L10002");
			smsMap.put("ORDNO", ApplyUniqueNbr);
			setSmsEmail(smsMap);
		}
		
		return model;
	}
	
	/**
	 * 환불 하기
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/CourseApplyRefundVa")
	public @ResponseBody Model CourseApplyRefundVa(Model model,HttpServletRequest request)throws Exception{
		
		logger.info("PaymentController.CourseApplyRefundVa -- !");
		
		Box params = HttpUtility.getBox(request);
		
		String ApplyUniqueNbr = params.getString("APPLY_UNIQUE_NBR");
		String SaleType = params.getString("SALE_TYPE");
		
		Map<String,Object> ProcessResult = new HashMap<String,Object>();
		
		Entity ApplyEntity = new Entity(15);
		
		ApplyEntity.setField("SALE_TYPE", SaleType);
		ApplyEntity.setField("WIN_TYPE", "000000");
		ApplyEntity.setField("SMS_SEND_FLAG", params.getString("SMS_SEND_FLAG"));
		ApplyEntity.setField("EMAIL_SEND_FLAG", params.getString("EMAIL_SEND_FLAG"));
		ApplyEntity.setField("APPLY_UNIQUE_NBR", ApplyUniqueNbr);
		ApplyEntity.setField("WORKER_MEMBER_NO", getCurrentMemberNo(request));
		ApplyEntity.setField("REFUND_ACCOUNT_OWNER",request.getParameter("REF_ACCOUNT_OWNER"));
		ApplyEntity.setField("REF_ACCOUNT_REG_NO",params.getString("REF_ACCOUNT_REG_NO"));
		ApplyEntity.setField("REFUND_BANK_CD",params.getString("REF_BANK_CD"));
		ApplyEntity.setField("REFUND_ACCOUNT_NO",params.getString("REF_ACCOUNT_NO"));
		ApplyEntity.setField("CMS_FLAG","N");
		ApplyEntity.setField("CLIENT_IP", request.getRemoteAddr());
		ApplyEntity.setField("PROCESS_TYPE", "REFUND");
		ApplyEntity.setField("CONFIRM_FLAG", "Y");
		
		long RefundPaymentAmt = Utility.StringToLong(params.getString("REFUND_PAYMENT_AMT").replaceAll(",", ""));
		
		if(RefundPaymentAmt > 0){
			Entity RefundPaymentList = new Entity(1);
			Entity RefundPaymentItem = new Entity(7);
			
			RefundPaymentItem.setField("PAYMENT_TYPE", params.getString("REFUND_PAYMENT_TYPE"));
			RefundPaymentItem.setField("PAYMENT_AMT", Long.toString(RefundPaymentAmt));
			RefundPaymentItem.setField("CMS_FLAG", (params.getString("REFUND_PAYMENT_TYPE").equals("R00001")?"Y":"N"));
			RefundPaymentItem.setField("CMS_COMMISSION_AMT", params.getString("CMS_COMMISSION_AMT"));
			RefundPaymentItem.setField("REF_BANK_CD", params.getString("REF_BANK_CD"));
			RefundPaymentItem.setField("REF_ACCOUNT_NO", params.getString("REF_ACCOUNT_NO"));
			RefundPaymentItem.setField("REF_ACCOUNT_OWNER", params.getString("REF_ACCOUNT_OWNER"));
			RefundPaymentItem.setField("REF_ACCOUNT_REG_NO", params.getString("REF_ACCOUNT_REG_NO"));
			
			RefundPaymentList.addField("REFUND_PAYMENT_ITEM", RefundPaymentItem);
			
			ApplyEntity.setField("REFUND_PAYMENT_LIST", RefundPaymentList);
		}
		
		ProcessResult = paymentCheckService.CourseApplyRefundVa(propertyHelper.getCompanycd(), ApplyEntity);
		
		model.addAttribute("result",ProcessResult.get("result"));
		model.addAttribute("Msg",(String)ProcessResult.get("Msg"));
		
		return model;
	}
	
	/**
	 * 0원 결제 취소
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/CoureseApplyZeroCancel")
	public @ResponseBody Model CourseApplyZeroCancel(Model model,HttpServletRequest request,HttpServletResponse response)throws Exception{
		
		logger.info("PaymentController.CourseApplyZeroCancel -- !");
		
		Box params = HttpUtility.getBox(request);
		
		String ApplyUniqueNbr = params.getString("APPLY_UNIQUE_NBR");
		String SaleType = params.getString("SALE_TYPE");
		
		Map<String,Object> ProcessResult = new HashMap<String,Object>();
		
		Entity ApplyEntity = new Entity(15);
		
		ApplyEntity.setField("SALE_TYPE", SaleType);
		ApplyEntity.setField("WIN_TYPE", "000000");
		ApplyEntity.setField("SMS_SEND_FLAG", "N");
		ApplyEntity.setField("EMAIL_SEND_FLAG", "N");
		ApplyEntity.setField("APPLY_UNIQUE_NBR", ApplyUniqueNbr);
		ApplyEntity.setField("WORKER_MEMBER_NO", getCurrentMemberNo(request));
		ApplyEntity.setField("CLIENT_IP", request.getRemoteAddr());
		ApplyEntity.setField("REFUND_ALL_FLAG", "Y");
		ApplyEntity.setField("REFUND_START_DATE", "");
		ApplyEntity.setField("REFUND_END_DATE", "");
		ApplyEntity.setField("REFUND_MEMBERSHIP_DAYS", "");
		ApplyEntity.setField("REFUND_DAYS_PER_PRICE", "");
		ApplyEntity.setField("REFUND_DAYS", "");
		ApplyEntity.setField("REFUND_RATE", "");
	
		ApplyEntity.setField("PROCESS_TYPE", "REFUND");
		ApplyEntity.setField("CONFIRM_FLAG", "Y");
		
		ProcessResult = paymentCheckService.CourseApplyRefund(propertyHelper.getCompanycd(), ApplyEntity);
		
		model.addAttribute("result",ProcessResult.get("result"));
		model.addAttribute("Msg",(String)ProcessResult.get("Msg"));
		
		return model;
	}

	/**
	 * 예약취소
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="/CoureseApplyCancel")
	public @ResponseBody Model CourseApplyCancel(Model model,HttpServletRequest request,HttpServletResponse response)throws Exception{
		
		logger.info("PaymentController.CourseApplyCancel -- !");
		
		Box params = HttpUtility.getBox(request);
		Map<String,Object> rsCheck = new HashMap<String,Object>();
		
		String ApplyUniqueNbr = params.getString("APPLY_UNIQUE_NBR");
		String SaleType = params.getString("SALE_TYPE");
		
		Entity ApplyEntity = new Entity(15);
		
		ApplyEntity.setField("SALE_TYPE", SaleType);
		ApplyEntity.setField("WIN_TYPE", "000000");
		ApplyEntity.setField("SMS_SEND_FLAG", params.getString("SMS_SEND_FLAG"));
		ApplyEntity.setField("EMAIL_SEND_FLAG", params.getString("EMAIL_SEND_FLAG"));
		ApplyEntity.setField("APPLY_UNIQUE_NBR", ApplyUniqueNbr);
		ApplyEntity.setField("WORKER_MEMBER_NO", getCurrentMemberNo(request));
		ApplyEntity.setField("CLIENT_IP", request.getRemoteAddr());
	
		ApplyEntity.setField("PROCESS_TYPE", "CANCEL");
			
		rsCheck = paymentCheckService.CourseApplyCancel(propertyHelper.getCompanycd(), ApplyEntity);
		model.addAttribute("result",rsCheck.get("result"));
		model.addAttribute("Msg",rsCheck.get("Msg"));
		
		//예약취소(L10001)
		if((Boolean) rsCheck.get("result")){
			Map smsMap = new HashMap();
			smsMap.put("CONTENTS_TYPE", "L10001");
			smsMap.put("ORDNO", params.getString("APPLY_UNIQUE_NBR"));
			setSmsEmail(smsMap);
		}
			
		return model;
	}
	
	/**
	 * @param map
	 * @throws Exception 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void setSmsEmail(Map map) throws Exception {
		//FAMILY_NO - null
		//GUEST_NAME - null
		//CAMPAIGN_NBR	- null
		//CAMPAIGN_LOG_NBR	- null
		//LINK_UNIQUE_NBR - null
		//LINK_UNIQUE_NBR2 - null
		//LINK_UNIQUE_NBR3 - null
		
		//예약 관련 SMS를 전송 할 시 아래 6가지 내용을 입력하여 smsEmailService.setSmsEmail(map) 에 map으로 파라미터를 전송한다.
		
		//SYSTEM_TYPE		1. 1. LEC: 강의, MEM: 맴버
		//RESER_DATETIME - 	2. 예약일시
		//SUB_TITLE_NAME - 	3. 프로그램명
		//SCHEDULE	- 		4. 프로그램 일자,시간
		//PAYMENT_AMT	- 	5. 총 결제금액
		//LIMIT_DATE	- 	6. 결제를 해야 할 기간
		//MEMBER_NO	- 		6. 사용자번호
		
		map.put("SYSTEM_TYPE", "LEC");
		map.put("COMPANY_CD", propertyHelper.getCompanycd());
		//예약자 정보인 ORDNO 으로 COURSE_CD,PRICE_NBR, MEMBER_NO, PAYMENT_AMT, LIMIT_DATE (RESER_DATE, RESER_TIME = RESER_DATETIME) 을 가져옴
		Map cam = paymentCheckService.get_TL_COURSE_APPLY(map.get("ORDNO").toString());
		map.put("COURSE_CD", cam.get("COURSE_CD"));
		map.put("PRICE_NBR", cam.get("PRICE_NBR"));
		map.put("MEMBER_NO", cam.get("MEMBER_NO"));
		String reserDate = Utility.CheckObjectNull(cam.get("RESER_DATE"));
		String reserTime = Utility.CheckObjectNull(cam.get("RESER_TIME"));
		map.put("RESER_DATETIME", reserDate + reserTime);
		map.put("PAYMENT_AMT", cam.get("TOTAL_PRICE"));
		String limitDate = Utility.CheckObjectNull(cam.get("RESERVE_LIMIT_DATE"));
		logger.info("limitDate -- " + limitDate);
		limitDate = limitDate.equals("") ? "" : limitDate.substring(2, 4) + "년  " + limitDate.substring(4, 6) + "월 " + limitDate.substring(6, 8) + "일 "; 
		map.put("LIMIT_DATE", limitDate);
		
		//가져온 COURSE_CD, PRICE_NBR로 예약정보를 가져옴. SUB_TITLE_NAME, SCHEDULE
		Map cm = paymentCheckService.get_SELECT_COURSE_CHECK(map);
		map.put("SUB_TITLE_NAME", cm.get("COURSE_NAME") + " " + cm.get("PRICE_NAME"));
		String schedule = "";
		if(cm.get("COURSE_DATE") != null){
			schedule += Utility.CheckObjectNull(cm.get("COURSE_DATE")).substring(4, 6) + "." + Utility.CheckObjectNull(cm.get("COURSE_DATE")).substring(6, 8);
			schedule += "(" + DateTime.whichDayString(Utility.CheckObjectNull(cm.get("COURSE_DATE"))) + ")-";
		}
		schedule += Utility.CheckObjectNull(cm.get("COURSE_START_TIME")).substring(0, 2) + ":" + Utility.CheckObjectNull(cm.get("COURSE_START_TIME")).substring(2, 4);
		map.put("SCHEDULE", schedule);
		
		smsEmailService.setSmsEmail(map);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping({"/getCompleteLecture"})
	  public String getCompleteLecture(Model model, HttpServletRequest request)
	    throws Exception
	  {
	    logger.info("PaymentController.getCompleteLecture -- !" + HttpUtility.getBox(request).toString());
	    Map map = new HashMap();
	    map.put("APPLY_UNIQUE_NBR", request.getParameter("CompleteApplyUniqueNbr"));
	    map.put("COMPANY_CD", this.propertyHelper.getCompanycd());
	    model.addAttribute("lecture", this.paymentService.getCompleteLecture(map));

	    if ((request.getParameter("CompleteAcademyCd").equals("ACD004")) || (request.getParameter("CompleteAcademyCd").equals("ACD009"))) {
	      return "jsp/confirmationForm.markany";
	    }
	    return "jsp/completeLectureForm.markany";
	  }
	
	public String getCurrentMemberNo(HttpServletRequest request) {
		return (String)request.getSession().getAttribute(Member.CURRENT_MEMBER_NO);
	}
}
