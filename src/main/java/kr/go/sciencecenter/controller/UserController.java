package kr.go.sciencecenter.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sci.v2.pcc.secu.SciSecuManager;
import com.sci.v2.pcc.secu.hmac.SciHmac;

import gov.mogaha.gpin.sp.proxy.GPinProxy;
import kr.co.mainticket.Utility.Box;
import kr.co.mainticket.Utility.HttpUtility;
import kr.go.sciencecenter.api.AuthenticationApi;
import kr.go.sciencecenter.model.Policy;
import kr.go.sciencecenter.model.User;
import kr.go.sciencecenter.model.api.Member;
import kr.go.sciencecenter.service.PolicyService;
import kr.go.sciencecenter.service.UserService;

@Controller
@RequestMapping(value = "/users")
@PropertySource("classpath:/application.properties")
public class UserController {
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	ServletContext servletContext;
	
	@Autowired
	UserService userService;
	
	@Autowired
	private PolicyService policyService;
	
	/**
	 * 사용자 로그인 view
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(HttpServletRequest request, Model model) {
		if(request.getSession().getAttribute(Member.CURRENT_MEMBER_NO) != null){
			return "redirect:/";
		}
		
		if (request.getParameter("error") != null) {
			model.addAttribute("error", request.getParameter("error"));
		}
		
		return "/users/login.app";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(Model model
			, HttpServletRequest request, HttpServletResponse response) {
		
		AuthenticationApi api = new AuthenticationApi();
		
		Member member = new Member();
		member.setMemberId(request.getParameter("memberId"));
		member.setPassword(request.getParameter("password"));
		
		member = api.login(member);
		
		if (member.getRspCd().equals("0000")){
			request.getSession().setAttribute(Member.CURRENT_MEMBER_NO, member.getMemberNo());
			
			//과거 예약내역을 다시보기 위해 쿠키로 인증(3개월만 사용)
			Cookie cookie = new Cookie(Member.CURRENT_MEMBER_NO, member.getMemberNo());
			cookie.setPath("/");
			cookie.setMaxAge(60*60);
			//cookie.setDomain("localhost");
			response.addCookie(cookie);
			
			if(request.getSession().getAttribute("lastPath") != null) {
				return "redirect:" + (String)request.getSession().getAttribute("lastPath");
			}else {
				return "redirect:/";
			}
			
		} else {
			System.out.println("로그인 실패 : " + member.getRspMsg());
			return "redirect:/users/login?error=" + member.getRspMsg();
		}
	}
	

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String create(@ModelAttribute("member") Member member
			, HttpServletRequest request
			, BindingResult result
			, Model model) {
		
		if (!result.hasErrors()) {
			AuthenticationApi api = new AuthenticationApi();
			Member registMember = api.regist(member);
			
			//사용자 등록처리 완료		
			if (registMember.getRspCd().equals("0000")){
				System.out.println("사용자 등록 처리 완료");
				model.addAttribute("member", registMember);
				return "redirect:/";
			}else {
				//예외처리 필요
				return "redirect:/";
			}
		} else {
			System.out.println("Member Model Binding Exepction");
			return "redirect:/users/signup";
		}
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	public String logout(HttpServletRequest request, HttpServletResponse response){
		request.getSession().removeAttribute(Member.CURRENT_MEMBER_NO);
		
		//과거 예약내역을 다시보기 위해 쿠키로 인증(3개월만 사용)
	    Cookie cookie = new Cookie(Member.CURRENT_MEMBER_NO, null);
	    cookie.setPath("/");
	    cookie.setMaxAge(0);
	    response.addCookie(cookie);
		
		return "redirect:/";
	}
	
	@RequestMapping(value = "/signupIntro", method = RequestMethod.GET)
	public String signupIntro(Model model) {
		return "users/signupIntro.app";
	}
	
	@RequestMapping(value = "/signup/certification", method = RequestMethod.GET)
	public String certification(Model model
			,HttpServletRequest request) {
		String id       = "SKIK001";                               // 본인실명확인 회원사 아이디
	    String srvNo    = "004004";                            // 본인실명확인 서비스번호
	    String reqNum   = "";                           // 본인실명확인 요청번호
		String exVar    = "0000000000000000";                                       // 복호화용 임시필드
	    String retUrl   = "http://www.sciencecenter.go.kr/scipia/users/signup/phoneResult";                           // 본인실명확인 결과수신 URL
		String certDate	= "";                         // 본인실명확인 요청시간
		String certGb	= "H";                           // 본인실명확인 본인확인 인증수단
		String addVar	= "";                           // 본인실명확인 
		
		Calendar today = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		String day = sdf.format(today.getTime());
		
		Random ran = new Random();
		
		//랜덤 문자 길이
		int numLength = 6;
		String randomStr = "";

		for (int i = 0; i < numLength; i++) {
			//0 ~ 9 랜덤 숫자 생성
			randomStr += ran.nextInt(10);
		}

		//reqNum은 최대 40byte 까지 사용 가능
		reqNum = day + randomStr;
		certDate=day;
		
		request.getSession().setAttribute("CertReqNum", reqNum);
		
		SciSecuManager seed = new SciSecuManager();
		//02. 1차 암호화
		String encStr = "";
		String reqInfo      = id+"^"+srvNo+"^"+reqNum+"^"+certDate+"^"+certGb+"^"+addVar+"^"+exVar;  // 데이터 암호화
		encStr              = seed.getEncPublic(reqInfo);
		
		//03. 위변조 검증 값 생성
		SciHmac hmac = new SciHmac(); 
		String hmacMsg = hmac.HMacEncriptPublic(encStr);

		//03. 2차 암호화
		reqInfo  = seed.getEncPublic(encStr + "^" + hmacMsg + "^" + "0000000000000000");  //2차암호화
		
		logger.info("Req Info : " + reqInfo);
		model.addAttribute("reqInfo", reqInfo);
		model.addAttribute("retUrl", retUrl);
		
		return "users/certification.app";
	}
	
	@RequestMapping(value = "/signup/phoneResult", method = RequestMethod.GET)
	public String phoneResult(Model model){
		
		return "users/policy.app";
	}
	
	@RequestMapping(value = "/signup/policy", method = RequestMethod.GET)
	public String consentPolicy(Model model) {
		
		Policy policy = policyService.findMostRecentByType("policy");
		model.addAttribute("policy", policy);
		return "users/policy.app";
	}
	
	@RequestMapping(value = "/signup/gpinResult", method = RequestMethod.GET)
	public String gpinResult(Model model, HttpServletRequest request) {
		String SamlResponse = request.getParameter("SAMLResponse");
		
		model.addAttribute("SAMLResponse", SamlResponse);
		
		return "users/gpinResult.app";
	}
	
	@RequestMapping(value = "/signup/privacy", method = RequestMethod.GET)
	public String consentPrivacy(Model model) {
		
		Policy policy = policyService.findMostRecentByType("privacy");
		model.addAttribute("policy", policy);
		return "users/privacy.app";
	}

	/*
	 * C2 SignUp Form 
	 */
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signup(Model model, HttpServletRequest request) {
		if (request.getSession().getAttribute("user") != null) {
			return "redirect:/";
		}

		//C2 API 연동을 위한 사용자정의
		Member member = new Member();
		model.addAttribute("member", member);
		
		return "users/signup.app";
	}
	
	@RequestMapping(value = "/signup/checkId", method = RequestMethod.GET)
	public Model checkId(Model model, HttpServletRequest request) {
		AuthenticationApi api = new AuthenticationApi();
		Member member = api.checkId(request.getParameter("memberId"));
		model.addAttribute("member", member);
		
		return model;
	}
	
	@RequestMapping(value = "/signupComplete", method = RequestMethod.GET)
	public String signupComplete(Model model, HttpServletRequest request) {
		
		return "users/signupComplete.app";
	}
	
	@RequestMapping(value = "/signupSponsor", method = RequestMethod.GET)
	public String signupSponsor(Model model, HttpServletRequest request) {

		User user = new User();
		model.addAttribute("user", user);
		
		return "users/signupSponsor.app";
	}
	
	@RequestMapping(value = "/signupSponsorComplete", method = RequestMethod.GET)
	public String signupSponsorComplete(Model model, HttpServletRequest request) {
		
		return "users/signupSponsorComplete.app";
	}

	/**
	 * 사용자 id 찾기 view
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findId", method = RequestMethod.GET)
	public String findId(Model model) {
		model.addAttribute("member", new Member());
		return "users/findId.app";
	}
	
	/**
	 * C2 API를 활용한 사용자 id 찾기
	 * @param jsonStr JSON 기반의 member data (memberName, memberEmail)
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findId", method = RequestMethod.POST)
	public @ResponseBody Model findIdProcess(@RequestBody String jsonStr
			, Model model) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			Member member = mapper.readValue(jsonStr, Member.class);
			member = AuthenticationApi.getInstance().findId(member);
			
			model.addAttribute("member", member);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return model;
	}

	
	/**
	 * 사용자 password 찾기 view
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findPassword", method = RequestMethod.GET)
	public String findPassword(Model model) {
		model.addAttribute("member", new Member());
		return "users/findPassword.app";
	}
	
	
	/**
	 * C2 API를 활용한 사용자 password 찾기
	 * @param jsonStr JSON 기반의 member data (memberName, memberEmail, memberId)
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findPassword", method = RequestMethod.POST)
	public @ResponseBody Model findPasswordProcess(@RequestBody String jsonStr
			, Model model) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			JSONObject obj = new JSONObject(jsonStr);
			
			String id = obj.getString("memberId");
			String email = obj.getString("memberEmail");
			String name = obj.getString("memberName");
			
			String duplicateKey = userService.findDuplicateKey(id, email, name);
			
			if (duplicateKey != null) {
				Member member = new Member();
				member.setDuplicateKey(duplicateKey);
				member = AuthenticationApi.getInstance().findPassword(member);
				
				model.addAttribute("member", member);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return model;
	}
	
	@RequestMapping(value = "/auth/{id}/{authenticationToken}", method = RequestMethod.GET)
	public String auth(@PathVariable int id, @PathVariable String authenticationToken, HttpServletRequest request) {

		User user = userService.findByIdAndAuthenticationToken(id, authenticationToken);

		if (user != null) {
			userService.auth(user);
			request.getSession().setAttribute("currentUser", user);
		}

		return "redirect:/";
	}

	@RequestMapping(value = "/{email}/check", method = RequestMethod.GET)
	public Model check(@PathVariable String email, Model model) {
		User user = userService.findByEmail(email);
		if (user != null) {
			model.addAttribute("email", user.getEmail());
		}
		return model;
	}
	
	/**
	 * IPIN 인증 요청
	 * @param model
	 * @param request
	 * @return 
	 * @throws Exception
	 */
	@RequestMapping(value = "/signup/myPinAuthRequest", method = RequestMethod.GET)
	public String myPinAuthRequest(Model model, HttpServletRequest request){

		return "users/myPinAuthRequest.app";
	}
	
	/**
	 * IPIN 인증 결과
	 * @param model
	 * @param request
	 * @return 
	 * @throws Exception
	 */
	@RequestMapping(value = "/signup/myPinAuthRequestResult", method = RequestMethod.POST)
	public String myPinAuthRequestResult(Model model
			, HttpServletRequest request){
		String SAMLResponse = (String)request.getSession().getAttribute("SAMLResponse");
		
		String[] AttrNames = new String[] {"dupInfo", "virtualNo", "realName", "sex", "age", "birthDate", "nationalInfo", "authInfo", "GPIN_AQ_SERVICE_SITE_USER_CONFIRM"}; 
		
		GPinProxy proxy = GPinProxy.getInstance(servletContext);
		
		String[] AttrData;
		try {
			AttrData = proxy.parseSAMLResponse(SAMLResponse, AttrNames);
			
			System.out.println("인증완료 이름 : "+AttrData[2]);
		} catch (Exception e) {
			//인증 에러
			e.printStackTrace();
		}
		
		return "users/policy.app";
	}
	
	/**
	 * 휴대폰 인증 결과
	 * @param model
	 * @param request
	 * @return 
	 * @throws Exception
	 */
	@RequestMapping(value = "/signup/phoneAuthResult", method = RequestMethod.GET)
	public String phoneAuthResult(Model model, HttpServletRequest request){
		
		Box params = HttpUtility.getBox(request);
		String CertifyType = "A00005";
		String CheckMsg = "";
		boolean CheckFlag = false;
		
	    // 변수 --------------------------------------------------------------------------------
	    String retInfo		= "";																// 결과정보

		//복화화용 변수
		String encPara		= "";
		String encMsg		= "";
		String msgChk       = "N";  
		
	    //-----------------------------------------------------------------------------------------------------------------
	    String cookiereqNum = null;
		try{
			cookiereqNum = (String)request.getSession().getAttribute("CertReqNum");
		
			retInfo = params.getString("retInfo").trim();
			 // 1. 암호화 모듈 (jar) Loading
	        com.sci.v2.pcc.secu.SciSecuManager sciSecuMg = new com.sci.v2.pcc.secu.SciSecuManager();
	        //쿠키에서 생성한 값을 Key로 생성 한다.
	        retInfo  = sciSecuMg.getDec(retInfo, cookiereqNum);

	        // 2.1차 파싱---------------------------------------------------------------
	        String[] aRetInfo1 = retInfo.split("\\^");

			encPara  = aRetInfo1[0];         //암호화된 통합 파라미터
	        encMsg   = aRetInfo1[1];    //암호화된 통합 파라미터의 Hash값
			
			String  encMsg2   = sciSecuMg.getMsg(encPara);
				// 3.위/변조 검증 ---------------------------------------------------------------
	        if(encMsg2.equals(encMsg)){
	            msgChk="Y";
	        }
		}catch(Exception e){
	    		e.printStackTrace();
	    		CheckFlag = false;
	    		CheckMsg = "인증 복호화 오류";
	    	}
		if(cookiereqNum != null && cookiereqNum != "" && msgChk =="Y"){
			System.out.println("인증 완료");
			return "users/policy.app";
		}
		
		return "users/policy.app";
	}
}
