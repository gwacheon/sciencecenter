package kr.go.sciencecenter.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import kr.go.sciencecenter.model.Board;
import kr.go.sciencecenter.model.BreadCrumb;
import kr.go.sciencecenter.model.Event;
import kr.go.sciencecenter.model.EventType;
import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.service.BoardService;
import kr.go.sciencecenter.service.EventsService;
import kr.go.sciencecenter.service.MainEventService;

@Controller
@RequestMapping("/events")
public class EventsController {
    
    @Autowired
    EventsService eventsService;
    
    @Autowired
    MainEventService mainEventService;
    
    @Autowired
	private BoardService boardService;
    
	/**
	 * 행사, 문화/공연, 문화행사, 캠프 index page
	 * @param type
	 * @param model
	 * @param status
	 * @param currentPage
	 * @return
	 */
	@RequestMapping(value="/intro", method = RequestMethod.GET)
	public String intro(Model model){
		model.addAttribute("crumb", new BreadCrumb(4,1,1));
		return "events/intro.app";
	}
	
    @RequestMapping(value="/list/{type}", method = RequestMethod.GET)
    public String index(@PathVariable("type") String type, Model model,
    		@RequestParam(value="status", defaultValue="true")boolean status,
    		@RequestParam(value = "page", defaultValue = "1") Integer currentPage) {    	
    	
    	String path = "/events/list/"+type+"?status="+status+"&";
    	Page page = new Page(currentPage,6,eventsService.count(type, status));
    	page.setPath(path);
    	model.addAttribute("page", page);
    	
    	model.addAttribute("events", eventsService.list(type, status, page));
    	model.addAttribute("eventType", EventType.EVENT_TYPES.get(type));
    	if(type.equals("camp")){
    		model.addAttribute("crumb", new BreadCrumb(3,3,4));
    	}else if(type.equals("culture")){
    		model.addAttribute("crumb", new BreadCrumb(4,1,2));
    	}else if(type.equals("play")){
    		model.addAttribute("crumb", new BreadCrumb(4,3));
    	}else if(type.equals("event")){
    		model.addAttribute("crumb", new BreadCrumb(4,5));
    		
    	}
    	
		return "events/index.app";
    }
    
    /**
     * 행사, 문화/공연, 캠프 show
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String show(@PathVariable int id, Model model) {
    		
    	Event event = eventsService.find(id);
    	model.addAttribute("event", event);
    	EventType eventType = EventType.EVENT_TYPES.get(event.getType());
    	model.addAttribute("eventType", eventType);
    	// 이전글 다음글 
    	model.addAttribute("prevEvent", eventsService.prevEvent(id, event.getType()));
    	model.addAttribute("nextEvent", eventsService.nextEvent(id, event.getType()));
    	
    	if(eventType.getType().equals("camp")){
    		model.addAttribute("crumb", new BreadCrumb(3,3,4));
    	}else if(eventType.getType().equals("culture")){
    		model.addAttribute("crumb", new BreadCrumb(4,1,2));
    	}else if(eventType.getType().equals("play")){
    		model.addAttribute("crumb", new BreadCrumb(4,4));
    	}else if(eventType.getType().equals("event")){
    		model.addAttribute("crumb", new BreadCrumb(4,5));
    	}
    	
    	return "events/show.app";
    }
    
    
    /**
     * 특별기획전 게시판 index
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/specialEvent", method = RequestMethod.GET)
    public String specialEvent(Model model,
    		@RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			@RequestParam(value = "searchType", required=false) String searchType,
			@RequestParam(value = "searchKey", required=false) String searchKey) {
    		
    	Board.getBoards(model, boardService, "specialEvent", currentPage, searchType, searchKey);
    	
    	return "events/specialEvent.app";
    }
    
    /**
     * 특별기획전 게시판 Show
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value="/specialEvent/{id}", method=RequestMethod.GET)
	public String specialEventShow(@PathVariable int id,Model model){
		model.addAttribute("id", id);
		
		return "intro/specialEventShow.app"; 
	}
    


}
