package kr.go.sciencecenter.controller;

import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import kr.co.mainticket.Utility.DateTime;
import kr.co.mainticket.Utility.HttpUtility;
import kr.co.mainticket.Utility.Utility;
import kr.go.sciencecenter.model.api.Member;
import kr.go.sciencecenter.persistence.PaymentCheckMapper;
import kr.go.sciencecenter.persistence.SmsEmailMapper;
import kr.go.sciencecenter.persistence.UserMapper;
import kr.go.sciencecenter.persistenceEnt.EntranceMapper;
import kr.go.sciencecenter.service.EntranceService;
import kr.go.sciencecenter.service.FamilyModifyService;
import kr.go.sciencecenter.service.LectureService;
import kr.go.sciencecenter.service.PaymentCheckService;
import kr.go.sciencecenter.service.ProgramService;
import kr.go.sciencecenter.service.SmsEmailService;
import kr.go.sciencecenter.util.Paging;
import kr.go.sciencecenter.util.PropertyHelper;

@Controller
@RequestMapping("/entrance")
public class EntranceController {
	private static final Logger logger = LoggerFactory.getLogger(EntranceController.class);
	@Autowired
	SmsEmailMapper smsemailMapper;
	
	@Value("${crm.returnEmail}")
	private String returnEmail; // 이메일 발송지
	@Value("${crm.returnName}")
	private String returnName; // 이메일 발송자
	@Value("${crm.callback}")
	private String callback; // sms 콜백번호	   

	///////////////////////////////////////////////////////////////////////
	//                              운영 환경                              // 
	///////////////////////////////////////////////////////////////////////
	
	@Autowired PropertyHelper 		propertyHelper;
	
	// 개발용
	public class Code {
		public static final String COMPANY_NAME			= "과천과학관";					// 회사명 
		
		public static final String SALE_TYPE			= "000002";
		public static final String TRAN_WIN_CD			= "001";						// 거래창구코드
		
		public static final String TRAN_TYPE_RESERVE	= "11";							// 예매
		public static final String TRAN_TYPE_CANCEL		= "12";							// 취소
	}
	
	// 상용
//	private static final String CARD_CANCEL_API_URL		= "https://pg.mainpay.co.kr/csStdPayment/cardCancel.do";						// 취소 API URL
//	public class Code {
//		public static final String COMPANY			= "800937";
//		public static final String COMPANY_NAME		= "과천과학관";					// 회사명 
//		
//		public static final String SALE_TYPE		= "000002";
//		public static final String TRAN_WIN_CD		= "001";						// 거래창구코드
//		
//		// 가맹점 ID (상용)
//		private static final String MBR_ID_10000002	= "550447";						// 가맹점 ID (국립과천과학관_천문_홈)
//		private static final String MBR_ID_10000003	= "550590";						// 가맹점 ID (국립과천과학관_스페이스월드_홈)
//		
//		public static final String TRAN_TYPE_RESERVE	= "11";						// 예매
//		public static final String TRAN_TYPE_CANCEL	= "12";							// 취소
//	}
	
	
	 static final int CMS_COMMISSION_AMT	= 500;														// CMS수수료

	
	@Autowired
	EntranceService entranceService;

	@Autowired
	EntranceMapper entranceMapper;
	
	@Autowired
	ProgramService programService;

	@Autowired
	LectureService lectureService;

	@Autowired
	PaymentCheckService paymentCheckService;

	@Autowired
	PaymentCheckMapper paymentCheckMapper;

	@Autowired
	FamilyModifyService FamilyModifyService;

	@Autowired
	UserMapper userMapper;

	@Autowired 
	SmsEmailService		smsEmailService;
	
	@RequestMapping(value = "/print")
	public @ResponseBody String print(Model model, HttpServletRequest request, @RequestParam HashMap<String, Object> params) throws Exception {
		
		entranceService.print(params);
		return "OK";
		
	}
	
	
	/**
	 * 가상계좌 입금 콜백
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/reserve/postpayPrc")
	public @ResponseBody String postpayPrc(Model model, HttpServletRequest request, @RequestParam Map<String, Object> params) throws Exception {
		/*
	 	호출시 전달되는 파라미터
	 	---------------------------------------------
		TRGUBUN		거래구분	2	‘V2’
		STOREID		상점아이디	6	 씨스퀘어소프트에서 부여해줌
		ORDNO		주문번호	20	
		TRDATE		거래일자	8	‘YYYYMMDD’
		TRTIME		거래시간	6	‘HHMISS’
		ACCOUNTNO	가상계좌번호	14	
		RCPTNAME	입금자명	30	
		AMT			금액		8	‘0005000’
		BANK_CODE	은행코드	2(3)	
		REPLYCODE	응답코드	4	‘0000’
		

	 	파라미터 샘플 
	 	---------------------------------------------
		TRGUBUN=V2&
		STOREID=100011&
		ORDNO=C15090110000134&
		TRDATE=20150901&
		TRTIME=112714&
		ACCOUNTNO=08200987697412&
		RCPTNAME현정&
		RCPTSOCIALNO=0000000000000&
		REPLYCODE=0000&
		AMT=00001000&
		BANK_CODE=03
	 */
		
		boolean update = (boolean)entranceService.payCallback(params);
		if(update) return "OK";
		else return "FAIL";
	}
	
	
	/**
	 * 예매내역 리스트
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/payment")
	public String payment(Model model, HttpServletRequest request) throws Exception {
		Map user_info = getCurrentUserInfo(request);			// 회원 정보
		model.addAttribute("UserInfo", user_info);
		
		if (user_info == null) {
			// 로그인 되어 있지 않으면 로그인 페이지로 이동
//			return "redirect:" + request.getContextPath() + "/users/login";
			return "redirect:" + "/users/login";
		}

		Map<String,Object> params = new HashMap<String,Object>();
		String pageNum = Utility.CheckNull(request.getParameter("pageNum"));
		String start_date = Utility.CheckNull(request.getParameter("start-date"));
		String end_date = Utility.CheckNull(request.getParameter("end-date"));
		
		
		int pageSize = 10;
    	
    	if(pageNum.equals("")){
    		pageNum = "1";
    	}
    	int CurPage = Integer.parseInt(pageNum);
    	
    	int BlockPage = 10;
    	int StartPage = pageSize*(CurPage-1)+1;
    	int EndPage = pageSize*CurPage;
    	int TotCnt = 0;
    	
    	params.put("MEMBER_NO", user_info.get("MEMBER_NO"));
		params.put("StartPage", StartPage);
		params.put("EndPage", EndPage);
		params.put("start_date", start_date);
		params.put("end_date", end_date);
		
    	TotCnt = entranceService.getPaymentListCount(params);
    	Paging pg = new Paging(CurPage, TotCnt, pageSize, BlockPage);
    	
    	
    	model.addAttribute("pg",pg.showBlock_page());
    	model.addAttribute("pageNum",pageNum);
    	model.addAttribute("start_date",start_date);
    	model.addAttribute("end_date",end_date);
    	model.addAttribute("pageNum",pageNum);
    	model.addAttribute("PaymentList",entranceService.getPaymentList(params));
    	
    	
		return "entrance/my_page_movie.app";
	}
	
	
	
	/**
	 * 예약증 출력 API
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/card")
	public HashMap<String,Object> getReserveCard(Model model, HttpServletRequest request) throws Exception {
		Map user_info = getCurrentUserInfo(request);			// 회원 정보
		model.addAttribute("UserInfo", user_info);
		
		Map<String,Object> params = new HashMap<String,Object>();
		String unique_nbr = Utility.CheckNull(request.getParameter("UNIQUE_NBR"));
		
		params.put("MEMBER_NO", user_info.get("MEMBER_NO"));
		params.put("unique_nbr", unique_nbr);
		//model.addAttribute("PaymentList",entranceService.getPaymentList(params));
    	
		HashMap<String, Object> result = new HashMap<String, Object>();
		HashMap<String, Object> mapCard = entranceService.getReserveCard(params);
		mapCard .put("MBR_ID", getMbrId((String) mapCard.get("PROGRAM_CD")));
		result.put("result", mapCard);		
				
		return result;
	}
	
	
	/**
	 * 예약취소 API
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delReserve")
	public HashMap<String,Object> delReserve(Model model, HttpServletRequest request) throws Exception {
		Map user_info = getCurrentUserInfo(request);			// 회원 정보
		model.addAttribute("UserInfo", user_info);
		
		Map<String,Object> params = new HashMap<String,Object>();
		String unique_nbr = Utility.CheckNull(request.getParameter("UNIQUE_NBR"));
		String cnt = Utility.CheckNull(request.getParameter("CNT"));
				
		params.put("UNIQUE_NBR", unique_nbr);
		params.put("CNT", cnt);
		entranceService.delReserve(params);
		
		HashMap<String, Object> result = new HashMap<String, Object>();
		result.put("result", "OK");		
				
		return result;
	}
	
	
	// TODO : 입장 프로그램 리스트
	/*
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String index(Model model, @RequestParam(value = "programType", required = false) String programType,
			@RequestParam(value = "searchKey", required = false) String searchKey, HttpServletRequest request)
			throws Exception {

		Map<String, Object> params = new HashMap<String, Object>();
		model.addAttribute("programTypes", ProgramType.PROGRAM_TYPE_MAP.get("normal"));

		if (programType == null) {
			model.addAttribute("programType", ProgramType.PROGRAM_TYPE_MAP.get("normal").get(0));
		}

		String AcademyCd = Utility.CheckNull(request.getParameter("ACADEMY_CD"));
		String ClassCd = Utility.CheckNull(request.getParameter("CLASS_CD"));

		if (AcademyCd.equals("")) {
			AcademyCd = "ACD005";
		}

		if (ClassCd.equals("")) {
			ClassCd = "CL5001";
		}

		params.put("companyCd", propertyHelper.getCompanycd());
		params.put("academyCd", AcademyCd);
		params.put("classCd", ClassCd);

		model.addAttribute("selectedDate", new Date());
		model.addAttribute("AcamedyList", lectureService.getAcademyList());
		model.addAttribute("ClassList", lectureService.getAcademyClassList(params));
		model.addAttribute("AcademyCd", AcademyCd);
		model.addAttribute("ClassCd", ClassCd);

		List<Program> programs = programService.searchByDate(new Date());
		model.addAttribute("programs", programs);
		model.addAttribute("currentCategory", "main.nav.catetory6");

		return "schedules/index.app";
	}
	*/

	
	/**
	 * 클래스 리스트
	 * 
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	/*
	@RequestMapping(value = "/AcademyChange")
	public String AcademyChange(Model model, HttpServletRequest request) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		Map<String, Object> ClassMap = new HashMap<String, Object>();

		model.addAttribute("programTypes", ProgramType.PROGRAM_TYPE_MAP.get("normal"));

		String AcademyCd = Utility.CheckNull(request.getParameter("ACADEMY_CD"));

		params.put("companyCd", propertyHelper.getCompanycd());
		params.put("academyCd", AcademyCd);

		List ClassList = lectureService.getAcademyClassList(params);
		ClassMap = (Map) ClassList.get(0);

		params.put("classCd", (String) ClassMap.get("CLASS_CD"));

		model.addAttribute("selectedDate", new Date());
		model.addAttribute("AcamedyList", lectureService.getAcademyList());
		model.addAttribute("AcademyCd", AcademyCd);
		model.addAttribute("ClassList", lectureService.getClassList(params));
		model.addAttribute("AcademyClassList", ClassList);

		List<Program> programs = programService.searchByDate(new Date());
		model.addAttribute("programs", programs);
		model.addAttribute("currentCategory", "main.nav.catetory6");

		
		//----------------------------
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("SALE_COMPANY_CD","800937");
		param.put("SALE_TYPE_CD",	"000002");
		param.put("PROGRAM_CD",		"10000002");
		
		List<HashMap<String, String>> pList = entranceService.getProgramList(param);
		System.out.println(pList.toString());
		
		return "schedules/index.app";
	}
	*/
	
	
	
	
	/**
	 * 예약
	 * 
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/reserve")
	public String reserve(Model model, HttpServletRequest request) throws Exception {
		Map user_info = getCurrentUserInfo(request);			// 회원 정보
		model.addAttribute("UserInfo", user_info);
		
		if (user_info == null) {
			// 로그인 되어 있지 않으면 로그인 페이지로 이동
//			return "redirect:" + request.getContextPath() + "/users/login";
			return "redirect:" + "/users/login";
		}

		
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("SALE_COMPANY_CD",	propertyHelper.getCompanycd());
		param.put("SALE_TYPE_CD",		Code.SALE_TYPE);
		param.put("PROGRAM_CD",			Utility.CheckNull(request.getParameter("PROGRAM_CD"))); 
		
		
		// 프로그램 리스트
		List<HashMap<String, String>> pList = entranceService.getProgramList(param);
		model.addAttribute("ProgramList", pList);
		System.out.println(pList.toString());
		
		if("".equals(""+request.getParameter("ACADEMY_CD")) || null == request.getParameter("ACADEMY_CD"))
			param.put("academyCd", "ACD007");
		else
			param.put("academyCd", ""+request.getParameter("ACADEMY_CD"));
		
		if("".equals(""+request.getParameter("CLASS_CD")) || null == request.getParameter("CLASS_CD"))
			param.put("classCd", "CL7001");
		else
			param.put("classCd", ""+request.getParameter("CLASS_CD"));
		
		
		// 상단 네비게이션 
		model.addAttribute("AcademyCd",	param.get("academyCd"));
		model.addAttribute("ClassCd",	param.get("classCd"));
		
		
		model.addAttribute("currentCategory", "main.nav.catetory6");
		model.addAttribute("AcamedyList",lectureService.getAcademyList());
		model.addAttribute("ClassList", lectureService.getAcademyClassList(param));
		
		model.addAttribute("MbrId",	getMbrId(Utility.CheckNull(request.getParameter("PROGRAM_CD"))));
		
		model.addAttribute("pay_targetUrl", propertyHelper.getPay_targetUrl());

		return "entrance/movie_show.app";
	}
	
	
	
	/**
	 * 예약 결제 완료 (모바일)
	 * 
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/reserve/complete")
	public String reserve_complete(Model model, HttpServletRequest request) throws Exception {

		HttpSession session = request.getSession();
		Enumeration attrs = session.getAttributeNames();

		while (attrs.hasMoreElements()) {
			String key = attrs.nextElement().toString();
			String val = session.getAttribute(key).toString();

			System.out.println(key + " : " + val);
		}

		// 
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("UNIQUE_NBR",	request.getParameter("oid"));
		HashMap<String, Object> mapReserve = entranceMapper.getReserve(param);
		model.addAttribute("reserve_info",	new JSONObject(mapReserve));

		
		return "entrance/complete.app";
	}
	

	
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	//                                           Rest API                                             //
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 월별 스케쥴 조회
	 * 
	 * 2016.06.15 요청으로 당일 내역 제외
	 * 
	 * @param id
	 * @param modelr
	 * @return
	 */
	@RequestMapping(value = "/api/scheduleList", method = RequestMethod.GET)
	public HashMap<String, Object> getScheduleList(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("SALE_COMPANY_CD",	propertyHelper.getCompanycd());
		param.put("REG_COMPANY_CD",		propertyHelper.getCompanycd());
		param.put("PLAY_COMPANY_CD",	propertyHelper.getCompanycd());
		param.put("SALE_TYPE_CD",		Code.SALE_TYPE);
//		param.put("MEM_GRADE_CD",		"000002");
		param.put("MEM_GRADE_CD",		getCurrUserGrade(request));
		
		param.put("PROGRAM_CD",			Utility.CheckNull(request.getParameter("program_cd"))); 		// SAMPLE : 10000002
		param.put("PLACE_CD",			Utility.CheckNull(request.getParameter("place_cd")));			// SAMPLE : 1002
		param.put("TARGET_YM",			Utility.CheckNull(request.getParameter("target_ym")));			// SAMPLE : 201604
		
		/*
		System.out.println("--------------------------------------");
		System.out.println("월별 스케쥴 조회 : " + param);
		*/
		List<HashMap<String, String>> sList = entranceService.getScheduleList(param);
		HashMap<String, Object> result = new HashMap<String, Object>();
		result.put("list", sList);
		return result;
	}
	
	
	/**
	 * 일별 스케쥴 조회
	 * 
	 * @param id
	 * @param modelr
	 * @return
	 */
	@RequestMapping(value = "/api/seqList", method = RequestMethod.GET)
	public HashMap<String, Object> getSeqList(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("SALE_COMPANY_CD",	propertyHelper.getCompanycd());
		param.put("REG_COMPANY_CD",		propertyHelper.getCompanycd());
		param.put("PLAY_COMPANY_CD",	propertyHelper.getCompanycd());
		
		param.put("PROGRAM_CD",			Utility.CheckNull(request.getParameter("program_cd"))); 		// SAMPLE : 10000002
		param.put("PLACE_CD",			Utility.CheckNull(request.getParameter("place_cd")));			// SAMPLE : 1002
		param.put("SALE_TYPE_CD",		Code.SALE_TYPE);
//		param.put("MEM_GRADE_CD",		"000002");														// TODO : Session에서 가져오는 것인지?
		param.put("MEM_GRADE_CD",		getCurrUserGrade(request));

		param.put("PLAY_DATE",			Utility.CheckNull(request.getParameter("play_date")));			// SAMPLE : 201604
		
		List<HashMap<String, String>> sList = entranceService.getSeqList(param);
		System.out.println(sList);

		HashMap<String, Object> result = new HashMap<String, Object>();
		result.put("list", sList);
		
		return result;
	}
	
	
	/**
	 * 권종 리스트 조회
	 * 
	 * @param id
	 * @param modelr
	 * @return
	 */
	@RequestMapping(value = "/api/priceTypeList", method = RequestMethod.GET)
	public HashMap<String, Object> getPriceTypeList(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("PLAY_COMPANY_CD",	propertyHelper.getCompanycd());
		param.put("PROGRAM_CD",			Utility.CheckNull(request.getParameter("program_cd"))); 		// SAMPLE : 10000002
		param.put("PLACE_CD",			Utility.CheckNull(request.getParameter("place_cd")));			// SAMPLE : 1002
		param.put("SALE_TYPE_CD",		Code.SALE_TYPE);
//		param.put("MEM_GRADE_CD",		"000002");														// TODO : Session에서 가져오는 것인지?
		param.put("MEM_GRADE_CD",		getCurrUserGrade(request));

		param.put("PLAY_DATE",			Utility.CheckNull(request.getParameter("play_date")));			// SAMPLE : 201604
		param.put("PLAY_SEQ_CD",		Utility.CheckNull(request.getParameter("play_seq_cd")));		// SAMPLE : 000005
		
		List<HashMap<String, String>> sList = entranceService.getPriceTypeList(param);
		System.out.println(sList);

		HashMap<String, Object> result = new HashMap<String, Object>();
		result.put("list", sList);
		
		return result;
	}
	
	
	
	/**
	 * 결제 승인 완료시 호출 (API)
	 * 
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/reserve/payment/return")
	public HashMap<String, Object> reservePamentReturn(Model model, HttpServletRequest request) throws Exception {
		//System.out.println(">>>>>> /reserve/payment/return START");
		debugPrintParameter(request);

		
		boolean result = false;
		
		if ("0000".equals(request.getParameter("rstCode")) || "00".equals(request.getParameter("rstCode"))) {
			if (("1".equals(request.getParameter("payKind"))) || ("2".equals(request.getParameter("payKind")))) {
				result = postPaymentReturn(request);
			}  else {
				// 지원하지 않는 결제 타입
			}
		} else {
			// 결제 실패
			
		}
		
		
		HashMap<String, Object> mapResult = new HashMap<String, Object>();
		if (result) {
			mapResult.put("rescode", "00");
			sendSMS((String) request.getParameter("oid"));
		} else {
			mapResult.put("rescode", "99");
		}
		
		
		System.out.println("Return : " + mapResult.toString());

		System.out.println(">>>>>> /reserve/payment/return END");
		return mapResult;
	}
	
	
	
	/**
	 * 신용카드 결제 승인 완료시 호출 (Page 이동)
	 * 카드 결제 승인 완료 및 DB 저장 완료
	 * 
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/reserve/payment/callback")
	public String reservePamentCallback(Model model, HttpServletRequest request) throws Exception {
		System.out.println("/reserve/payment/callback 호출");
		debugPrintParameter(request);

		int transactionCnt = 0;
		Map mapParam;
		JSONObject json = new JSONObject();
		
		/*
		// T_PAYMENTRESULT 의 정보 UPDATE는 /reserve/payment/return 에서 처리하도록 변경
		try {
			mapParam = HttpUtility.handleRequest(request);
			
			////////////////////////////////////////////////////
			// T_PAYMENTRESULT (지불 결과 정보) UPDATE
			HashMap<String, Object> paramPR = new HashMap<String, Object>();
			paramPR.put("SALESPRICE",				mapParam.get("salesPrice"));
			paramPR.put("PAYKIND",					mapParam.get("payKind"));
			paramPR.put("RESCODE",					mapParam.get("resCode")==null?mapParam.get("rstCode"):mapParam.get("resCode"));
			paramPR.put("RESMSG",					mapParam.get("resMsg")==null?mapParam.get("rstMsg"):mapParam.get("resMsg"));
			paramPR.put("RSTCODE",					mapParam.get("rstCode"));
			paramPR.put("RSTMSG",					mapParam.get("rstMsg"));
			paramPR.put("HASHVALUE",				mapParam.get("hashValue"));
			paramPR.put("OID",						mapParam.get("oid"));
			paramPR.put("TRADETIME",				mapParam.get("tradeTime"));
			paramPR.put("CARD_MBRID",				mapParam.get("mbrId"));
			paramPR.put("CARD_PAYTYPE",				mapParam.get("payType"));
			paramPR.put("CARD_AUTHTYPE",			mapParam.get("authType"));
			paramPR.put("CARD_TRADENO",				mapParam.get("cardTradeNo"));
			paramPR.put("CARD_APPROVDATE",			mapParam.get("cardApprovDate"));
			paramPR.put("CARD_APPROVNO",			mapParam.get("cardApprovNo"));
			paramPR.put("CARD_NAME",				mapParam.get("cardName"));
			paramPR.put("CARD_CODE",				mapParam.get("cardCode"));
			paramPR.put("VA_TRADENO",				mapParam.get("vAccountTradeNo"));
			paramPR.put("VA_BANKNAME",				mapParam.get("vAccountBankName"));
			paramPR.put("VA_BANKCODE",				mapParam.get("bankCode"));
			paramPR.put("VA_ACCOUNT",				mapParam.get("vAccount"));
			paramPR.put("VA_RETURNTYPE",			mapParam.get("returnType"));
			paramPR.put("VA_ACCOUNTNAME",			mapParam.get("vAccountName"));
			paramPR.put("VA_CRITICALDATE",			mapParam.get("vCriticalDate"));
			paramPR.put("VA_ACCOUNTAPPROVTIME",		mapParam.get("vAccountApprovTime"));
			
			transactionCnt = entranceMapper.updatePaymentResult(paramPR);
			
			//T_TRAN & T_RESERVE : 결제 완료 후 입금구분 상태 변경 : 2016.07.06
			HashMap<String, Object> paramRT = new HashMap<String, Object>();
			
			paramRT.put("OID",						mapParam.get("oid"));
			//카드결제일 경우 'CA'으로 FIX
			if(mapParam.get("payKind") != null && mapParam.get("payKind").equals("1"))
				paramRT.put("TRAN_CODE",					"CA");
			else
				paramRT.put("TRAN_CODE",					"");
			
			paramRT.put("CARD_APPROVNO",			mapParam.get("cardApprovNo"));
			paramRT.put("CARD_MBRID",				mapParam.get("mbrId"));
			//T_PAYMENT_RESULT 변경이 있을 경우 변경
			entranceMapper.updateReserveTran(paramRT);
			
		} catch (Exception e) {
			System.out.println("신용카드 결제 승인 완료시 호출 오류 발생 : " + e.getMessage());
		}
		*/
		
		// Parameter를 Json으로 변환  
		//JSONObject json = new JSONObject();

		Enumeration<String> em = request.getParameterNames();
		while (em.hasMoreElements()) {
			String key = em.nextElement();
			String val = (String)request.getParameter(key);
			
			json.put(key, val);
//			out.println(key + " :: " + URLDecoder.decode(val) + "<br>");
		}

		model.addAttribute("result", json);

		return "entrance/callback.app";
	}
	
	
	/**
	 * 본인 회원 정보 조회
	 * 
	 * @param id
	 * @param modelr
	 * @return
	 */
	@RequestMapping(value = "/api/current/user", method = RequestMethod.GET)
	public HashMap<String, Object> getCurrentUser(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map user_info = getCurrentUserInfo(request);			// 회원 정보
		
		HashMap<String, Object> result = new HashMap<String, Object>();
		if (user_info != null) {
			result.put("user_info", user_info);
		} else {
			HashMap<String, Object> error = new HashMap<String, Object>();
			error.put("code", 9999);
			error.put("message", "로그인 정보를 찾을 수 없음");
			
			result.put("error", error);
		}
		
		return result;
	}
	
	
	
	/**
	 * 예약 Setp 01
	 * @return
	 */
	@RequestMapping(value = "/api/reserve/step01", method = RequestMethod.POST)
	public HashMap<String, Object> postReserveStep01(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		HashMap<String, Object> result = new HashMap<String, Object>();

		String strTranSeq = null;				// 거래 일련 번호
		String strUniqueNbr = null;				// 거래고유번호
		String strRsvNbr = null;				// 예매번호

		String strTranDate = getCurrentDate("yyyyMMdd");
		String strTranTime = getCurrentDate("HHmmss");

		// 회원 정보 추출
		Map user_info = getCurrentUserInfo(request);
		JSONObject reserve_info = new JSONObject(request.getParameter("reserve_info"));
		
		if (user_info != null) {
			try {
				result.put("user_info", user_info);
				System.out.println("User Info : " + user_info);
				
				int choiceSeat =  reserve_info.getInt("total_count");
				
                //좌석 HOLD 시 DB상의 현재 좌석 후 조회 후 체크 : 2016.07.06
				param.put("PLAY_COMPANY_CD",	propertyHelper.getCompanycd());
				param.put("PLACE_CD",			reserve_info.get("place_cd"));
				param.put("REG_COMPANY_CD",		propertyHelper.getCompanycd());
				param.put("PROGRAM_CD",			reserve_info.get("program_cd"));
				param.put("PLAY_DATE",			reserve_info.get("play_date"));
				param.put("PLAY_SEQ_CD",		reserve_info.get("play_seq_cd"));
				param.put("PLAY_ST_TIME",		reserve_info.get("play_start_time"));	
				
				HashMap<String, String> remainSeatSum = entranceService.getScheduleSeatCnt(param);
				int remainSeat = 0;
				if (remainSeatSum == null) {
					throw new Exception("좌석 조회 실패");
				}
				 
				remainSeat = Integer.parseInt(String.valueOf(remainSeatSum.get("SUM_REMAIN_SEAT_CNT")));
				if(choiceSeat > remainSeat){
					result.put("errorCd", "RS");
					throw new Exception("잔여 인원을 초과하여 예약할수 없습니다.");
				}
				//좌석 HOLD 시 DB상의 현재 좌석 후 조회 후 체크
				
				param = new HashMap<String, Object>();
				// 거래 일련 번호 채번
				param.put("TRAN_DATE",			strTranDate);
				param.put("TRAN_COMPANY_CD",	propertyHelper.getCompanycd());
				param.put("TRAN_WIN_CD",		Code.TRAN_WIN_CD);

				HashMap<String, String> mapTranSeq = entranceService.getMaxTranNbr(param);
				strTranSeq = (String) mapTranSeq.get("TRAN_NBR");
				if (strTranSeq == null) {
					throw new Exception("거래 일련번호 채번 실패");
				}

				// 거래고유번호
				strUniqueNbr = strTranDate + propertyHelper.getCompanycd() + Code.TRAN_WIN_CD + strTranSeq;						
				
				// 예매번호 채번
				param = new HashMap<String, Object>();
				param.put("MEMBER_ID",				user_info.get("MEMBER_ID"));									// 회원아이디
				HashMap<String, String> mapRsvNbr = entranceMapper.getRsvNbr(param);
				strRsvNbr = (String) mapRsvNbr.get("RSV_NBR");
				if (strRsvNbr == null) {
					throw new Exception("예매번호 채번 실패");
				}

				int transactionTranCnt = 0;
				//  T_TRAN 등록 (건당 1Row)
				param = new HashMap<String, Object>();
				param.put("TRAN_DATE",				strTranDate);													// 거래일자
				param.put("TRAN_COMPANY_CD",		propertyHelper.getCompanycd());									// 거래회사
				param.put("TRAN_WIN_CD",			Code.TRAN_WIN_CD);												// 거래창구코드
				param.put("TRAN_NBR",				strTranSeq);													// 거래일련번호
				param.put("UNIQUE_NBR",				strUniqueNbr);													// 거래고유번호
				param.put("TRAN_TYPE",				Code.TRAN_TYPE_RESERVE);										// 거래형태
				param.put("SALE_TYPE_CD",			Code.SALE_TYPE);												// 판매형태
				param.put("TKT_TOT_AMT",			reserve_info.getInt("total_price"));							// 총티켓금액
				param.put("TRAN_TOT_CNT",			reserve_info.getInt("total_count"));							// 총매수
				param.put("TOT_FUND_AMT",			0);																// 총기금
				param.put("TOT_VAT_AMT",			0);																// 총부가세
				param.put("TRAN_TOT_SVC_AMT",		0);																// 총티켓수수료
				param.put("DLVR_AMT",				0);																// 배송료
				param.put("TRAN_AMT",				reserve_info.getInt("total_price"));							// 총지불금액
				param.put("TRAN_TIME",				strTranTime);													// 거래시간
				param.put("TRAN_REF_FLAG",			"N");															// 거래환불구분
				param.put("RECEIPT_TYPE",			"0");															// 입금구분 (0 : 미입금, 1 : 입금완료, 2 : 부분입금)
				param.put("LAST_TRAN_TYPE",			"10");															// 거래최종상태
				param.put("RSV_NBR",				strRsvNbr);														// 예매번호			CHAR(10)
				param.put("RECEIPT_AMT",			0);																// 예매입금액
				param.put("RECEIPT_LIMIT_DATE",		"");															// 입금제한일
				param.put("RECEIPT_LIMIT_TIME",		"");															// 입금제한시간
				param.put("TOT_RF_RATE",			0);																// 총환불/취소율
				param.put("TOT_RF_AMT",				0);																// 총환불/취소금액
				param.put("TOT_RF_FUND_AMT",		0);																// 총환불/취소기금 
				param.put("TOT_RF_VAT_AMT",			0);																// 총환불/취소부가세
				param.put("TOT_RF_SVC_AMT",			0);																// 총환불/취소티켓수수료
				param.put("TOT_RF_CMS_AMT",			0);																// 총환불/CMS수수료
				param.put("TOT_RF_RSVSVC_AMT",		0);																// 예매수수료환불금액
				param.put("MEMBER_TYPE",			"P");															// 회원/단체구분
				param.put("MEMBER_NO",				user_info.get("MEMBER_NO"));									// 회원번호
				param.put("MEMBER_ID",				user_info.get("MEMBER_ID"));									// 회원아이디
				param.put("MEMBER_NAME",			user_info.get("MEMBER_NAME"));									// 회원이름
				param.put("MEMBER_NAME2",			"");															// 회원이름2
				param.put("REG_NO",					"");															// 주민번호
				param.put("MEMBER_TEL1",			user_info.get("MEMBER_CEL"));									// 회원연락처1
				param.put("MEMBER_TEL2",			"");															// 회원연락처2
				param.put("MEMBER_EMAIL",			user_info.get("MEMBER_EMAIL"));									// 회원이메일
				param.put("MEM_GRADE_CD",		    getCurrUserGrade(request));

				param.put("MEMGRD_TKT_CNT",			0);																// 회원등급별제한권종매수
				param.put("FIND_TYPE",				"000001");														// 수령형태
				param.put("INSERT_USER",			user_info.get("MEMBER_ID"));									// 등록자
				param.put("FOREIGN_FLAG",			"N");															// 외국인여부
				param.put("POINT_MINUS",			0);																// 
				param.put("POINT_PLUS",				0);																// 
				param.put("GOODS_FLAG",				"N");															// 상품추가구매여부
				param.put("GOODS_TYPE",				"1");															// 상품구매형태(1:Sk전용결합상품)
				param.put("HREF_SVC_AMT",			0);																// 인터페이스업체환불수수료
				param.put("ENC_REG_NO",				"");															// 주민번호2		
				
				transactionTranCnt = entranceService.regTran(param);
				
 
				// T_RESERVE 등록 (권종별 인당 1Row)
				int transactionReserveCnt = 0;
				int nTranSeqNbr = 0;						// 
				JSONArray arrMemberPrice = reserve_info.getJSONArray("member_price");
				for (int i = 0; i < arrMemberPrice.length(); i++) {
					JSONObject jsonMemberPrice = arrMemberPrice.getJSONObject(i);
					
					for (int j = 0; j < jsonMemberPrice.getInt("cnt"); j++) {
						nTranSeqNbr++;
						
						param = new HashMap<String, Object>();
						param.put("TRAN_DATE",				strTranDate);													// 거래일자
						param.put("TRAN_COMPANY_CD",		propertyHelper.getCompanycd());									// 거래회사
						param.put("TRAN_WIN_CD",			Code.TRAN_WIN_CD);												// 거래창구코드
						param.put("TRAN_NBR",				strTranSeq);													// 거래일련번호
						param.put("TRAN_SEQ_NBR",			nTranSeqNbr);													// 거래세부번호
						param.put("UNIQUE_NBR",				strUniqueNbr);													// 거래고유번호
						param.put("TRAN_TYPE",				Code.TRAN_TYPE_RESERVE);										// 거래형태    
						param.put("RSV_NBR",				strRsvNbr);														// 예매번호    
						param.put("TRAN_TIME",				strTranTime);													// 거래시간        
						param.put("SALE_TYPE_CD",			Code.SALE_TYPE);												// 판매형태
						param.put("PLAY_COMPANY_CD",		propertyHelper.getCompanycd());									// 공연회사    
						param.put("PLACE_CD",				reserve_info.get("place_cd"));									// 공연장코드  
						param.put("REG_COMPANY_CD",			propertyHelper.getCompanycd());									// 등록회사    
						param.put("PROGRAM_CD",				reserve_info.get("program_cd"));								// 프로그램코드
						param.put("PLAY_DATE",				reserve_info.get("play_date"));									// 공연일자    
						param.put("PLAY_SEQ_CD",			reserve_info.get("play_seq_cd"));								// 회차코드    
						param.put("PLAY_ST_TIME",			reserve_info.get("play_start_time"));							// 공연시작시간
						param.put("PLAY_SEQ_NM",			reserve_info.get("play_seq_name"));								// 회차명      
						param.put("PHYMAP_NBR",				"001");															// 좌석도순번  
						param.put("BLOCK_CD",				"");														    // 블럭코드    
						param.put("ROW_NBR",				0);																// 블럭별열    
						param.put("FLOOR_TYPE",				"");														    // 층구분      
						param.put("ROW_DISP_NAME",			"");															// 표시용열    
						param.put("SEAT_NBR",				0);																// 좌석번호    
						param.put("SEAT_INFO_CD",			"NS");															// 좌석정보코드
						param.put("SEAT_GRADE_CD",			"08");															// 좌석등급코드
						param.put("PRICE_TYPE_CD",			jsonMemberPrice.getString("price_type_cd"));					// 권종코드      
						param.put("PRICE",					jsonMemberPrice.getInt("price"));								// 가격          
						param.put("FUND_PRICE",				0);																// 기금          
						param.put("VAT_PRICE",				0);																// 부가세        
						param.put("TKT_SVC_AMT",			0);																// 티켓수수료    
						param.put("RECEIPT_TYPE",			"0");															// 입금구분 (0 : 미입금, 1 : 입금완료, 2 : 부분입금)    
						param.put("RECEIPT_TRAN_DATE",		strTranDate);													// 입금처리일자  
						param.put("TKT_CAN_TYPE",			"0");															// 발권/취소여부 
						param.put("RC_RATE",				0);																// 취소율        
						param.put("RC_AMT",					0);																// 취소금액      
						param.put("RC_FUND_AMT",			0);																// 취소기금      
						param.put("RC_VAT_AMT",				0);																// 취소부가세    
						param.put("RC_SVC_AMT",				0);																// 취소티켓수수료
						param.put("PRINT_PMSG",				"");															// 개인출력메세지
						param.put("MEMBER_NO",				user_info.get("MEMBER_NO"));									// 회원번호      
						param.put("MEMBER_ID",				user_info.get("MEMBER_ID"));									// 회원아이디    
						param.put("REG_NO",					"");															// 주민번호      
						param.put("INSERT_USER",			user_info.get("MEMBER_ID"));									// 등록자        
						param.put("MEMBER_NAME",			user_info.get("MEMBER_NAME"));									// 회원이름
						param.put("MEMBER_TEL1",			user_info.get("MEMBER_CEL"));									// 회원연락처1
						param.put("DISCOUNT_AMT",			0);																// 
						param.put("DISCOUNT_RATE",			0);																// 
						param.put("ENC_REG_NO_FLAG",		"");															// 주민번호2암호화여부(1:일반, 2:암호화)
						param.put("PRICE_TYPE_NAME",		"개인");														    // 
						param.put("DISP_PRICE",				0);																// 
						param.put("UNIT_PRICE",				0);																// 
//						param.put("MEM_GRADE_CD",			"000001");														// 회원 등급 
						param.put("MEM_GRADE_CD",			getCurrUserGrade(request));
						param.put("LMT_TYPE_FLAG",			"N");															// 
						param.put("ADVTICKET_FLAG",			"N");															// 
						transactionReserveCnt += entranceService.regReserve(param);
					}
				}
				
				//T_RESERVE 와  T_TRAN 에 한곳이라도 저장이 누락되면 ROLLBACK
				if(transactionTranCnt == 0 || transactionReserveCnt == 0){
					HashMap<String, Object> params = new HashMap<String,Object>();
					params.put("UNIQUE_NBR",		strUniqueNbr);
					entranceService.ReleaseHoldSeat(params);

					result.put("error", ERR_DEF.UNKNOWN.toHashMap());
					result.put("errorCd", "RS");
					throw new Exception("예약 중 오류가 발생하였습니다. 다시 시도하여 주세요");
				}
					
				// T_SCHEDULE_SEAT_SUM	: Update
				HashMap<String, Object> ssParam = new HashMap<String, Object>();
				ssParam.put("CNT",			nTranSeqNbr);
				ssParam.put("UNIQUE_NBR",	strUniqueNbr);
				System.out.println(">>>>> plusSeatsum");
				entranceMapper.plusSeatsum(ssParam);
			} catch (Exception e) {
				System.out.println("예약 등록중 오류 발생 : " + e.getMessage());
				result.put("error", ERR_DEF.UNKNOWN.toHashMap());
				result.put("errorMsg", e.getMessage());
				return result;
			}

		} else {
			result.put("error", ERR_DEF.NOT_LOGIN.toHashMap());
			return result;
		}
		

		result.put("tran_seq",		strTranSeq);			// 거래 일련 번호
		result.put("unique_nbr",	strUniqueNbr);			// 거래고유번호
		result.put("rsv_nbr",		strRsvNbr);				// 예매번호

		result.put("tran_date",		strTranDate);			// 예약일자
		result.put("tran_time",		strTranTime);			// 예약시간

		return result;
	}
	
	
	
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	//                                           결제 (Payment)                                       //
	////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * 결제 Hash 생성
	 * 
	 * @param 판매가격
	 * @param 거래번호
	 *            
	 * @return
	 */
	@RequestMapping(value = "/api/payment/hash", method = RequestMethod.GET)
	public HashMap<String, Object> getPaymentHash(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		HashMap<String, Object> result = new HashMap<String, Object>();
		HashMap<String, Object> error = new HashMap<String, Object>();
		
		System.out.println("/api/payment/hash -- ! " + HttpUtility.getBox(request).toString());
		
		try {
			int salesPrice		= Integer.parseInt(request.getParameter("salesPrice"));			// 판매가격
			String oid			= request.getParameter("oid");									// 거래번호
			String mbr_id		= "";								// 가맹점 코드 
			if(request.getParameter("mbr_id") != null)	mbr_id = request.getParameter("mbr_id");
			if(request.getParameter("mbrId") != null)	mbr_id = request.getParameter("mbrId");
			
			if ((oid == null) || (oid.length() == 0)) {
				throw new Exception("파라미터 오류");
			}
			
			//결제 전 거래번호가 존재하는지 체크 : 2016.07.06
			String type			= request.getParameter("type"); //입장예매일 경우에만 체크
			if(type != null && type.equals("enter")){
					
				HashMap<String, Object> param = new HashMap<String, Object>();
				param.put("UNIQUE_NBR",		oid);
				HashMap<String, String> uniqueNbrCheck = entranceService.getUniqueNbrExists(param);
				
				int uniqueNbrCnt = 0;
				if (uniqueNbrCheck == null) {
					result.put("errorCd", "XXX");
					throw new Exception("거래번호가 존재하지 않습니다. 다시 좌석을 선택하여 진행 해 주세요.");
				}
				 
				uniqueNbrCnt = Integer.parseInt(String.valueOf(uniqueNbrCheck.get("UNIQUE_NBR_CNT")));
				if(uniqueNbrCnt == 0){
					result.put("errorCd", "XXX");
					throw new Exception("결제 시간이 초과하였습니다. 다시 좌석을 선택하여 진행 해 주세요.");
				}
			}
			//결제 전 거래번호가 존재하는지 체크
			
			Date now = new Date();
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
			String timeStamp	= format.format(now);
			
			
			String hash = timeStamp + hash(String.format("%s|%d|%s|%s", mbr_id, salesPrice, oid, timeStamp));

//			//프로그램 코드에 따른 가맹점 코드 변경 로직 반영
//			String MBR_ID = "";
//			String hash = timeStamp + hash(String.format("%s|%d|%s|%s", request.getParameter("mbrId"), salesPrice, oid, timeStamp));

			result.put("hash", hash);

			/*
			System.out.println("mbrId : " + mbr_id);
			System.out.println("salesPrice : " + salesPrice);
			System.out.println("oid : " + oid);
			System.out.println("timeStamp : " + timeStamp);
			System.out.println("hash : " + hash);
			*/

		} catch (Exception e) {
			e.printStackTrace();
			error.put("code", 999);
			error.put("message", "파라미터 오류");			
			result.put("error", error);
			
			result.put("errorMsg", e.getMessage());
		}
		
		return result;
	}
	
	
	
	/**
	 * 카드 결제 취소 요청
	 * 
	 * @param
	 *            
	 * @return
	 */
	@RequestMapping(value = "/api/payment/cancel", method = RequestMethod.GET)
	public HashMap<String, Object> getPaymentCancel(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		HashMap<String, Object> result = new HashMap<String, Object>();

		String mbr_id = getMbrId(request.getParameter("PROGRAM_CD"));
		String product_name = request.getParameter("PROGRAM_NAME");

		HashMap<String, Object> param = new HashMap<String, Object>();		
//		param.put("COMPANY_CD",		CODE_COMPANY);
		param.put("COMPANY_CD",		propertyHelper.getCompanycd());
		param.put("MEMBER_NO",		(String)request.getSession().getAttribute(Member.CURRENT_MEMBER_NO));

		Map user_info = getCurrentUserInfo(request);
		String sUserID = (String) user_info.get("MEMBER_ID");
		
		param = new HashMap<String, Object>();
		param.put("INF_UNIQUE_NBR", request.getParameter("INF_UNIQUE_NBR"));
		HashMap<String, Object> mapPay = entranceMapper.getLastPaymentResult(param);

		if ("1".equals(mapPay.get("PAYKIND"))) {
			// 신용카드 
			if ("0000".equals(mapPay.get("RESCODE")) || "00".equals(mapPay.get("RESCODE"))) {
				// 결제 성공 
				// 카드 승인 취소 요청 
				JSONObject json = reqCancelCardApprov(
						mbr_id,
						"씨스퀘어",
						(String)mapPay.get("CARD_TRADENO"),
						(String)mapPay.get("CARD_APPROVDATE"),
						(String)user_info.get("MEMBER_NAME"),
						(String)user_info.get("MEMBER_CEL"),
						product_name,
						(String)mapPay.get("SALESPRICE"),
						(String)mapPay.get("CARD_PAYTYPE")		
				);

				// DB 처리 
//				if ().json..
				entranceService.postCardCancel(request.getParameter("INF_UNIQUE_NBR").toString(), sUserID);

			}
		}  else if ("2".equals(mapPay.get("PAYKIND"))) {
			// 가상계좌 
			String depositor_name	= request.getParameter("DEPOSITOR_NAME");			// 예금주명
			String depositor_birth	= request.getParameter("DEPOSITOR_BIRTH");			// 생년월일
			String depositor_bank	= request.getParameter("DEPOSITOR_CD");				// 은행 코드 
			String depositor_account= request.getParameter("DEPOSITOR_ACCOUNT");		// 계좌번호
			String depositor_phone	= request.getParameter("DEPOSITOR_PHONE");			// 전화번호
			
			// T_TRAN
			HashMap<String, Object> resParam = new HashMap<String, Object>();
			resParam.put("UNIQUE_NBR", request.getParameter("UNIQUE_NBR"));
			HashMap<String, Object> mapReserve = entranceMapper.getReserve(resParam);
//			CURR_DATE,TRAN_COMPANY_CD,TRAN_WIN_CD,TRAN_NBR,UNIQUE_NBR,TRAN_TYPE,SALE_TYPE_CD,TKT_TOT_AMT,TRAN_TOT_CNT,TOT_FUND_AMT,TOT_VAT_AMT,TRAN_TOT_SVC_AMT,DLVR_AMT,TRAN_AMT,TRAN_TIME,TRAN_REF_FLAG,RECEIPT_TYPE,LAST_TRAN_TYPE,RSV_NBR,RECEIPT_AMT,RECEIPT_LIMIT_DATE,RECEIPT_LIMIT_TIME,TOT_RF_RATE,TOT_RF_AMT,TOT_RF_FUND_AMT,TOT_RF_VAT_AMT,TOT_RF_SVC_AMT,TOT_RF_CMS_AMT,TOT_RF_RSVSVC_AMT,MEMBER_TYPE,MEMBER_NO,MEMBER_ID,MEMBER_NAME,MEMBER_NAME2,REG_NO,MEMBER_TEL1,MEMBER_TEL2,MEMBER_EMAIL,MEM_GRADE_CD,MEMGRD_TKT_CNT,FIND_TYPE,DELIVERY_STATUS,DLVR_ZIP_CD,DLVR_ADDR1,DLVR_ADDR2,DLVR_UPDATE_USER,DLVR_UPDATE_DATE,DLVR_UPDATE_TIME,PKG_NBR,AFTER_PAY_FLAG,INSERT_USER,BOOK_NBR,BOOK_GROUP,BOOK_COMPANY_CD,BOOK_PERSON_ID,FOREIGN_FLAG,SATI_CENTER_ID,SATI_GROUP_ID,SATI_MEM_CENTER_ID,MEM_ZIP_CD,PORTAL_UNIQUE_NBR,PLACE_UNIQUE_NBR,OCB_DSC_FLAG,OCB_USE_FLAG,OCB_ACC_FLAG1,OCB_ACC_POINT1,OCB_ACC_INF_NBR1,OCB_ACC_CAN_INF_NBR1,OCB_ACC_FLAG2,OCB_ACC_POINT2,OCB_ACC_INF_NBR2,OCB_ACC_CAN_INF_NBR2,OCB_ACC_TRACK_DATA,OCB_ACC_WCC,OCB_ACC_STD_AMT1,OCB_ACC_STD_AMT2,POINT_MINUS,POINT_PLUS,SKT_UNIQUE_NBR,SKT_UNIQUE_NBR2,GOODS_FLAG,GOODS_TYPE,OCB_COUPON_ACC_FLAG,OCB_COUPON_ACC_STD_AMT,OCB_COUPON_ACC_AMT,OCB_COUPON_ACC_INF_NBR,HREF_SVC_AMT,ENC_REG_NO_FLAG,ENC_REG_NO,PT_ACC_TYPE1,PT_ACC_DATA_TYPE1,PT_ACC_ENC_DATA_FLAG,PT_ACC_ENC_DATA,PT_ACC_FLAG1,PT_ACC_STD_AMT1,PT_ACC_AMT1,PT_ACC_INF_NBR1,REQ_TRAN_COMPANY_CD,RT_SALE_FLAG,RT_RSV_NBR,RT_UNIQUE_NBR,RT_INF_TYPE
			
//			request.getParameter("INF_UNIQUE_NBR");
//			request.getParameter("UNIQUE_NBR");

			String CURR_DATE = getCurrentDate("yyyyMMdd");

			String a = mapReserve.get("TOT_AMT").toString();									// 결제수단별가격
			int b = Integer.parseInt(a);
			System.out.println(b);
			
			int payment_amt			= Integer.parseInt(mapReserve.get("TOT_AMT").toString());	// 결제수단별가격
			int tot_cms_amt			= payment_amt - CMS_COMMISSION_AMT;							// 총입금금액
			
			
			HashMap<String, Object> paramCMS = new HashMap<String, Object>();

			// CMS Table 등록
//			paramCMS.put("CMS_SEQ",				"");										// CMS거래번호 (date(6) + company_cd + seq(6)) - 18Byte
			paramCMS.put("TRAN_DATE",			CURR_DATE);									// 거래일자
			paramCMS.put("TRAN_COMPANY_CD",		propertyHelper.getCompanycd());				// 거래회사
			paramCMS.put("TRAN_WIN_CD",			Code.TRAN_WIN_CD);							// 거래창구코드
//			paramCMS.put("TRAN_NBR",			"");										// 거래일련번호
			paramCMS.put("TRAN_PAY_NBR",		"01");										// 결제수단별일련번호
			paramCMS.put("UNIQUE_NBR",			request.getParameter("UNIQUE_NBR"));		// 거래고유번호
			paramCMS.put("TRAN_TIME",			getCurrentDate("HHmmss"));					// 거래시간
			paramCMS.put("CMS_COMMISSION_AMT",	CMS_COMMISSION_AMT);						// CMS수수료
			paramCMS.put("PAYMENT_AMT",			payment_amt);								// 결제수단별가격
			paramCMS.put("TOT_CMS_AMT",			tot_cms_amt);								// 총입금금액
			paramCMS.put("RF_BANK_CD",			depositor_bank);							// 환불은행코드
			paramCMS.put("RF_ACCNT_NO",			depositor_account);							// 환불계좌번호
			paramCMS.put("MBR_NO",				mbr_id);									// 가맹점번호
			paramCMS.put("COMPANY_NAME",		Code.COMPANY_NAME);							// 회사명
			paramCMS.put("RF_ACCNT_OWNER",		depositor_name);							// 환불계좌주
			paramCMS.put("CMS_RESD_ID",			"");										// 주민번호/사업자번호  -> NVL
			paramCMS.put("RF_RCPT_FLAG",		"");										// 환불입금처리상태
			paramCMS.put("CMS_SEND_DATE",		"");										// 전송일자
			paramCMS.put("INF_UNIQUE_NBR",		request.getParameter("INF_UNIQUE_NBR"));	// 인터페이스용거래고유번호
			paramCMS.put("ORG_TRAN_DATE",		mapReserve.get("CURR_DATE"));				// 원거래승인일자
			paramCMS.put("CMS_RSP_CODE",		"");										// CMS응답코드
//			paramCMS.put("INSERT_USER",			"");
//			paramCMS.put("INSERT_DATE",			"");
//			paramCMS.put("INSERT_TIME",			"");
//			paramCMS.put("UPDATE_USER",			"");
//			paramCMS.put("UPDATE_DATE",			"");
//			paramCMS.put("UPDATE_TIME",			"");
//			paramCMS.put("ENC_RF_ACCNT_NO_FLAG","1");										// 환불계좌번호암호화여부(1:일반, 2:암호화)
//			paramCMS.put("ENC_RF_ACCNT_NO",		"");
//			paramCMS.put("ENC_REG_NO_FLAG",		"1");										// 주민번호2암호화여부(1:일반, 2:암호화)
//			paramCMS.put("ENC_REG_NO",			"");
			
			if (tot_cms_amt > 0) {
				// 총입금금액(결제금액 - 수수료)이 0원 이하인 경우 CMS에 등록하지 않음 (2016.06.03 : 이소미 부장 요청)
				entranceMapper.regCMS(paramCMS);
			}
			// 입장 취소 처리 
			entranceService.postVACancel(request.getParameter("INF_UNIQUE_NBR"), sUserID);

		}
		
		

		
		HashMap<String, Object> map = new HashMap<String, Object>();
//		Iterator<?> keys = json.keys();
//
//		while(keys.hasNext()){
//			String key = (String)keys.next();
//			String value = json.getString(key); 
//			map.put(key, value);
//		}
		

		return map;
	}
	
	
	/**
	 * 좌석 HOLD 시 DB상의 현재 좌석 후 조회 후 체크 
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
     * 2016.07.06
	 */
	@RequestMapping(value = "/api/getScheduleSeatCnt", method = RequestMethod.GET)
	public HashMap<String, Object> getScheduleSeatCnt(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("PLAY_COMPANY_CD",	propertyHelper.getCompanycd());
		param.put("PLACE_CD",			Utility.CheckNull(request.getParameter("place_cd")));
		param.put("REG_COMPANY_CD",		propertyHelper.getCompanycd());
		param.put("PROGRAM_CD",			Utility.CheckNull(request.getParameter("program_cd")));
		param.put("PLAY_DATE",			Utility.CheckNull(request.getParameter("play_date")));
		param.put("PLAY_SEQ_CD",		Utility.CheckNull(request.getParameter("play_seq_cd")));
		param.put("PLAY_ST_TIME",		Utility.CheckNull(request.getParameter("play_start_time")));
		
		HashMap<String, String> remainSeat = entranceService.getScheduleSeatCnt(param);
		System.out.println(remainSeat);

		HashMap<String, Object> result = new HashMap<String, Object>();
		result.put("list", remainSeat);
		
		return result;
	}
	
	
	/**
	 * HOLD처리된 좌석 수 계산하여 취소 처리
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
     * 2016.07.06
	 */
	@RequestMapping(value = "/ReleaseHoldSeat")
	public HashMap<String,Object> ReleaseHoldSeat(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Map<String,Object> params = new HashMap<String,Object>();
		HashMap<String, Object> result = new HashMap<String, Object>();
		JSONObject reserve_info = new JSONObject(request.getParameter("reserve_info"));
		
		try{
			params.put("UNIQUE_NBR",		reserve_info.get("unique_nbr"));	
			entranceService.ReleaseHoldSeat(params);
		
			result.put("result", "000");		
		}catch (Exception e) {
			e.printStackTrace();
			HashMap<String, Object> error = new HashMap<String, Object>();
			error.put("code", 999);
			error.put("message", "파라미터 오류");

			result.put("error", error);
		}
		return result;
	}
	
	//--------------------------------------------------------------------------------------------------------------------------------------

	
	
	/**
	 * Http Request Parameter Print (for Debug)
	 * @param request
	 */
	private void debugPrintParameter(HttpServletRequest request) {
		// Parameter 출력
		Enumeration eNames = request.getParameterNames();
		if (eNames.hasMoreElements()) {
//			String title = "Parameters";
//			Map entries = new TreeMap();
			while (eNames.hasMoreElements()) {
				String name = (String) eNames.nextElement();
				String[] values = request.getParameterValues(name);
				if (values.length > 0) {
					String value = values[0];
					for (int i = 1; i < values.length; i++) {
						value += "," + values[i];
					}
					System.out.println(name + " : " + value);
				}
			}
		}

	}
	
	/**
	 * SHA-256 Hash 생성
	 *
	 * @param data
	 * @param algorithm
	 * @return
	 * @throws Exception
	 */
	private static String hash(String data) throws Exception {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(data.getBytes("UTF-8"));
		byte[] hashbytes = md.digest();

		StringBuilder sbuilder = new StringBuilder();
		for (int i = 0; i < hashbytes.length; i++) {

			// %02x 부분은 0 ~ f 값 까지는 한자리 수이므로 두자리 수로 보정하는 역할을 한다.
			sbuilder.append(String.format("%02x", hashbytes[i] & 0xff));
		}
		return sbuilder.toString();
	}

	
	private Map getCurrentUserInfo(HttpServletRequest request) {
		// 회원 정보
		HashMap<String, Object> param = new HashMap<String, Object>();		
//		param.put("COMPANY_CD",		CODE_COMPANY);
		param.put("COMPANY_CD",		propertyHelper.getCompanycd());
		param.put("MEMBER_NO",		(String)request.getSession().getAttribute(Member.CURRENT_MEMBER_NO));
		Map user_info = userMapper.getLegacyUser(param);
		return user_info;
	}
	
	public static String getCurrentDate(String format) {
		// "yyyyMMddHHmmss"
		Date now = new Date();
		SimpleDateFormat sdFormat = new SimpleDateFormat(format);
		return sdFormat.format(now);
	}

	
	/**
	 * Program CD에 따른 가맹점 코드 리턴 
	 * @param programCD
	 * @return
	 * @throws Exception 
	 */
	private String getMbrId(String programCD) throws Exception {
		String mbr_id = null;
		if ("10000002".equals(programCD)) 
			mbr_id = (String) ((Map)lectureService.getMbrInfo("CL7001")).get("MBR_ID");		//천체투영관
		else if("10000003".equals(programCD)) 
			mbr_id = (String) ((Map)lectureService.getMbrInfo("CL7002")).get("MBR_ID");		//스페이스월드
		
		return mbr_id;
	}
	
	
	private String getCurrUserGrade(HttpServletRequest request) {
		Map user_info = getCurrentUserInfo(request);			// 회원 정보
		String grade = (String) user_info.get("MEMBERSHIP_TYPE_ENTRANCE");
		
		/*
		System.out.println(">>>>>" + " 회원 등급 ");
		System.out.println(">>>>>" + grade);
		*/
		
		return grade;
	}
	
	
	/**
	 * 카드 승인 취소 
	 * 
	 * @param map
	 * @return
	 */
	private JSONObject reqCancelCardApprov(String mbrId, String mbrName, String cardTradeNo, String cardApprovDate, String buyerName, String buyerMobile, String productName, String salesPrice, String payType) {
	
//		카드 승인 취소 요청 API URL : https://pg.mainpay.co.kr/csStdPayment/cardCancel.do
		// 요청 방식: HTTPRequest POST
		// Request Header 추가 내용
		// Content-Type: application/x-www-form-urlencoded
		 
		//---------------
//		mbrId				가맹점아이디			씨스퀘어에서 발급한 가맹점 아이디
//		mbrName				가맹점명				씨스퀘어에 등록한 가맹점 명
//		cardTradeNo			카드거래번호			카드결제시 응답 받은 거래번호			7548437557845784
//		cardApprovDate		카드승인일				YYMMDD							160401
//		buyerName			구매자명												홍길동
//		buyerMobile			구매자 전화번호											01012345678
//		productName			제품명												전자패드
//		salesPrice			결제금액												10000
//		payType				결제타입												ISP/3D
		
		
		buyerMobile = buyerMobile.replace("-", "");
		
		
		JSONObject result = null;
		 
		try {
			String strBody = String.format("mbrId=%s&mbrName=%s&cardTradeNo=%s&cardApprovDate=%s&salesPrice=%s&buyerName=%s&buyerMobile=%s&productName=%s&payType=%s",
					mbrId, mbrName,
					cardTradeNo, cardApprovDate, 
					salesPrice,	buyerName,
					buyerMobile, productName, 
					payType
			);
			
			HttpResponse<String> response = Unirest.post(propertyHelper.getPay_cancel())
					  .header("cache-control", "no-cache")
					  .header("content-type", "application/x-www-form-urlencoded")
					  .body(strBody)
					  .asString();
			//System.out.println(">>>>>" + response.getBody());
			result = new JSONObject(response.getBody());
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		return result;
	}
	
	
	
	
	
	/**
	 * 결제 Return URL DB 처리
	 * 
	 * @param request
	 * @return
	 */
	private boolean postPaymentReturn(HttpServletRequest request) {
		Map mapParam;
		try {
			mapParam = HttpUtility.handleRequest(request);
		} catch (Exception e1) {
			return false;
		}

		System.out.println(">>>>> postPaymentReturn()");
		System.out.println(">>>>> Param : ");
		System.out.println(">>>>> " + mapParam.toString());

		//표준결제 승인결과(공통)
		String mbrid =(String) mapParam.get("mbrId");
		String rstCode = (String) mapParam.get("rstCode");
		String rstMsg = (String) mapParam.get("rstMsg");
		String resCode = (String) mapParam.get("resCode");
		String resMsg = (String) mapParam.get("resMsg");
		String salesPrice =(String) mapParam.get("salesPrice");
		String payKind = (String) mapParam.get("payKind");
		String oid = (String) mapParam.get("oid");
		String hashValue = (String) mapParam.get("hashValue");
		System.out.println("hashValue:"+(String) mapParam.get("hashValue"));
		//표준결제 승인결과(카드)
		String payType = payKind.equals("1")?(String) mapParam.get("payType"):""; //결제타입 :(ISP/3D)
		String authType = payKind.equals("1")?(String) mapParam.get("authType"):""; //인증타입:auth
		String cardTradeNo = payKind.equals("1")?(String) mapParam.get("cardTradeNo"):""; //거래번호
		String cardApprovDate = payKind.equals("1")?(String) mapParam.get("cardApprovDate"):""; //카드승인일
		String cardApprovTime = payKind.equals("1")?(String) mapParam.get("cardApprovTime"):""; //카드승인시간
		System.out.println("cardApprovTime:"+(String) mapParam.get("cardApprovTime"));
		String cardName = payKind.equals("1")?(String) mapParam.get("cardName"):"";
		String cardCode = payKind.equals("1")?(String) mapParam.get("cardCode"):"";
		String cardApprovNo = payKind.equals("1")?(String) mapParam.get("cardApprovNo"):"";
		//표준결제 승인결과(가상계좌)
		String vAccountTradeNo = payKind.equals("2")?(String) mapParam.get("vAccountTradeNo"):"";
		String vAccount = payKind.equals("2")?(String) mapParam.get("vAccount"):"";
		String vCriticalDate = payKind.equals("2")?(String) mapParam.get("vCriticalDate"):"";
		String vAccountBankName =payKind.equals("2")?(String) mapParam.get("vAccountBankName"):"";
		String vAccountBankCode = payKind.equals("2")?(String) mapParam.get("vAccountBankCode"):"";
		String returnType = payKind.equals("2")?(String) mapParam.get("returnType"):"";
		String vAccountName = payKind.equals("2")?(String) mapParam.get("vAccountName"):"";
		String vAccountApprovTime = payKind.equals("2")?(String) mapParam.get("vAccountApprovTime"):"";
		
		String paymentType ="";
		if(payKind != null && payKind.equals("1")){
			paymentType = "000002"; //신용카드
		}else if(payKind != null && payKind.equals("2")){
			paymentType = "000001"; //가상계좌
		}
		
		// 거래 날짜 시간 
		String sTranDate = getCurrentDate("yyyyMMdd");
		String sTranTime = getCurrentDate("HHmmss");
		
		// 예약 정보 조회
		HashMap<String, Object> resParam = new HashMap<String, Object>();
		resParam.put("UNIQUE_NBR", oid);
		HashMap<String, Object> mapReserve = entranceMapper.getReserve(resParam);
		if (mapReserve == null)
			return false;

		// InfUniqueNbr 채번 
		HashMap<String, Object> mbrParam = new HashMap<String, Object>();
		mbrParam.put("COMPANY_CD", mapReserve.get("TRAN_COMPANY_CD"));
		HashMap<String,String> mapInf = entranceMapper.getInfUniqueNbr(mbrParam);
		//String sInfUniqueNbr = mapInf.get("INFUNIQUENBR");
		
		// 웹예매에서 채번된 거래번호를 사용
		String sInfUniqueNbr = (String) mapParam.get("cardTradeNo");
		
		
		////////////////////////////////////////////////////
		// T_PAYMENT_RESULT (지불 결과 정보) INSERT
		HashMap<String, Object> paramPR = new HashMap<String, Object>();
		paramPR.put("INF_UNIQUE_NBR",			sInfUniqueNbr);							// 
		paramPR.put("SALESPRICE",				salesPrice);
		paramPR.put("PAYKIND",					payKind);
		paramPR.put("RESCODE",					resCode);
		paramPR.put("RESMSG",					resMsg);
		paramPR.put("RSTCODE",					rstCode);
		paramPR.put("RSTMSG",					rstMsg);
		paramPR.put("HASHVALUE",				hashValue);
		paramPR.put("OID",						oid);
		
		paramPR.put("CARD_APPROVTIME",			cardApprovTime);
		paramPR.put("CARD_MBRID",				mbrid);
		paramPR.put("CARD_PAYTYPE",				payType);
		paramPR.put("CARD_AUTHTYPE",			authType);
		paramPR.put("CARD_TRADENO",				cardTradeNo);
		paramPR.put("CARD_APPROVDATE",			cardApprovDate);
		paramPR.put("CARD_APPROVNO",			cardApprovNo);
		paramPR.put("CARD_NAME",				cardName);
		paramPR.put("CARD_CODE",				cardCode);

		paramPR.put("VA_TRADENO",				vAccountTradeNo);
		paramPR.put("VA_ACCOUNT",				vAccount);
		paramPR.put("VA_CRITICALDATE",			vCriticalDate);
		paramPR.put("VA_BANKNAME",				vAccountBankName);
		paramPR.put("VA_BANKCODE",				vAccountBankCode);

		////////////////////////////////////////////////////
		// T_PAYMENTTICKET (결제수단별거래정보) INSERT
		HashMap<String, Object> paramPT = new HashMap<String, Object>();

		paramPT.put("INF_UNIQUE_NBR",		sInfUniqueNbr);							// 
		paramPT.put("TRAN_DATE",			sTranDate);								// 거래일자 (PK)		CHAR(8)	NOT NULL
		paramPT.put("TRAN_COMPANY_CD",		mapReserve.get("TRAN_COMPANY_CD"));		// 거래회사 (PK)		CHAR(6)	NOT NULL
		paramPT.put("TRAN_WIN_CD",			mapReserve.get("TRAN_WIN_CD"));			// 거래창구코드 (PK)		CHAR(3)	NOT NULL
		paramPT.put("TRAN_NBR",				mapReserve.get("TRAN_NBR"));			// 거래일련번호 (PK)		CHAR(7)	NOT NULL
		paramPT.put("TRAN_PAY_NBR",			"01");									// 결제수단별일련번호 (PK)	CHAR(2)	NOT NULL
		paramPT.put("PAYMENT_TYPE",			paymentType);	                        // 결제구분				CHAR(6)
		paramPT.put("UNIQUE_NBR",			oid);					                // 거래고유번호			CHAR(24)
		paramPT.put("TRAN_TYPE",			Code.TRAN_TYPE_RESERVE);				// 거래형태				CHAR(2)
		paramPT.put("APP_TRAN_TYPE",		"1");									// 승인/취소구분			CHAR(1)
		paramPT.put("TRAN_TIME",			sTranTime);								// 거래시간				CHAR(6)
		paramPT.put("SALE_TYPE_CD",			Code.SALE_TYPE);						// 판매형태				CHAR(6)
		paramPT.put("PAYMENT_AMT",			salesPrice);			                // 결제수단별가격			NUMBER(12)
		paramPT.put("PAYMENT_VAT_AMT",		0);										// 결제수단별세금			NUMBER(9)
		paramPT.put("PAYMENT_SVC_AMT",		0);										// 결제수단별서비스료		NUMBER(9)
		paramPT.put("BANK_CD",				vAccountBankCode);				        // 은행코드				CHAR(6)
		paramPT.put("MANUAL_CARD_FLAG",		"N");									// 수기카드여부			CHAR(1)
		paramPT.put("RECEIPT_TYPE",			payKind.equals("1")?"1":0);				// 입금구분				CHAR(1)
		paramPT.put("RECEIPT_AMT",			payKind.equals("1")?salesPrice:0);		// 예매입금액			NUMBER(12)
		paramPT.put("COMMISSION_AMT",		0);										// 계산된정산수수료		NUMBER(6)
		paramPT.put("COMMISSION_INOUT",		"-");									// 정산수수료+/-입출구분	CHAR(1)
		paramPT.put("TRAN_RCPT_ACCOUNT_NO",	"");									// 계좌번호				VARCHAR2(30)
		paramPT.put("REF_FLAG",				"N");									// 환불여부				CHAR(1)
		paramPT.put("TKT_SVC_AMT",			0);										// 티켓수수료			NUMBER(9)
		paramPT.put("DLVR_AMT",				0);										// 배송료				NUMBER(5)
		paramPT.put("INSERT_USER",			"SYSTEM");								// 등록자				CHAR(12)
		paramPT.put("SATI_CERT_FLAG",		"N");									// 사랑티켓인증여부		CHAR(1)
		paramPT.put("SATI_DISCOUNT_AMT",	0);										// 사랑티켓장당기본금액		NUMBER(12)
		paramPT.put("MEMBER_NO",			mapReserve.get("MEMBER_NO"));			// MEMBER_NO		VARCHAR2(20)
		paramPT.put("MEMBER_ID",			mapReserve.get("MEMBER_ID"));			// MEMBER_ID		VARCHAR2(20)
		paramPT.put("MEM_GRADE_CD",			mapReserve.get("MEM_GRADE_CD"));		// MEM_GRADE_CD			CHAR(6)
		paramPT.put("ENC_CARD_NO_FLAG",		"2");									// ENC_CARD_NO_FLAG		CHAR(1)
		paramPT.put("ENC_RF_ACCNT_NO_FLAG",	"1");									// ENC_RF_ACCNT_NO_FLAG	CHAR(1)
		paramPT.put("ENC_CMS_RESD_ID_FLAG",	"1");									// ENC_CMS_RESD_ID_FLAG	CHAR(1)
		paramPT.put("AUTH_NO",				cardApprovNo);			                // 승인번호						CHAR(8)
		paramPT.put("TRAN_CODE",			payKind.equals("1")?"CA":"");           //
		paramPT.put("MBR_NO",				mbrid);			                        //
		
		////////////////////////////////////////////////////
		// T_CARDAPPROVE (결제 내역) INSERT
		HashMap<String, Object> paramCA = new HashMap<String, Object>();

		paramCA.put("INF_UNIQUE_NBR",		sInfUniqueNbr);							// 
		paramCA.put("TRAN_COMPANY_CD",		mapReserve.get("TRAN_COMPANY_CD"));		// 거래회사					 	CHAR(6)
		paramCA.put("TRAN_DATE",			sTranDate);								// 거래일자 (PK)			    CHAR(8)	NOT NULL
		paramCA.put("TRAN_TIME",			sTranTime);								// 거래시간					 	CHAR(6)
		paramCA.put("PAY_SVC_TYPE",			payType);				                // 서비스구분					CHAR(3)			>> 파라미터 payType 인듯하나 자리수가 모자름 (vAccount)
		paramCA.put("TRAN_WIN_CD",			mapReserve.get("TRAN_WIN_CD"));			// 거래창구코드					CHAR(3)
		paramCA.put("SALE_TYPE_CD",			Code.SALE_TYPE);						// 판매형태					 	CHAR(6)
		paramCA.put("MBR_NO",				mbrid);					                // 가맹점번호					CHAR(6)
		paramCA.put("CUSTOMER_CODE",		"0000");								// 요청기관						CHAR(4)
		paramCA.put("PLAY_COMPANY_CD",		mapReserve.get("TRAN_COMPANY_CD"));		// 공연회사						CHAR(6)
		paramCA.put("PLACE_CD",				mapReserve.get("PLACE_CD"));			// 공연장코드					CHAR(4)
		paramCA.put("REG_COMPANY_CD",		mapReserve.get("TRAN_COMPANY_CD"));		// 등록회사						CHAR(6)
		paramCA.put("PROGRAM_CD",			mapReserve.get("PROGRAM_CD"));			// 프로그램코드					CHAR(8)
		paramCA.put("APPROVE_TRAN_TYPE",	"010");									// 승인취소전문거래구분			 	CHAR(3)
		paramCA.put("TRAN_TOT_CNT",			mapReserve.get("TOT_CNT"));				// 총매수						NUMBER(8)
		paramCA.put("PLAY_DATE",			mapReserve.get("PLAY_DATE"));			// 공연일자						CHAR(8)
		paramCA.put("PLAY_SEQ_CD",			mapReserve.get("PLAY_SEQ_CD"));			// 회차코드						CHAR(6)
		paramCA.put("PLAY_ST_TIME",			mapReserve.get("PLAY_ST_TIME"));		// 공연시작시간					CHAR(6)
		paramCA.put("PLAY_SEQ_NM",			mapReserve.get("PLAY_SEQ_NM"));			// 회차명						VARCHAR2(40)
		paramCA.put("UNIT_MAX_AMT",			mapReserve.get("MAX_AMT"));				// 단위당최고가격					NUMBER(8)
		paramCA.put("MERCH_NBR",			mbrid);	        		                // 카드사가맹점번호				CHAR(16)
		paramCA.put("APP_TRAN_TYPE",		"1");									// 승인/취소구분					CHAR(1)
		paramCA.put("KEYIN_IND",			"K");									// keyin/swipe구분			CHAR(1)
		paramCA.put("TRACK_DATA",			"");									// 트랙데이타					CHAR(39)
		paramCA.put("INSTALL_NO",			"00");									// 할부개월						CHAR(2)
		paramCA.put("CURRENCY_CD",			"410");									// 통화코드						CHAR(3)
		paramCA.put("APP_AMT",				salesPrice);			                // 카드금액						NUMBER(8)
		paramCA.put("APP_SVC_AMT",			0);										// 카드봉사료					NUMBER(8)
		paramCA.put("APP_TAX_AMT",			0);										// 카드세금						NUMBER(8)
		paramCA.put("AUTH_NO",				cardApprovNo);			                // 승인번호						CHAR(8)
		paramCA.put("APP_DATE",				cardApprovDate);		                // 승인일자						CHAR(6)
		paramCA.put("APP_RSP_CD",			rstCode.toString().substring(0, 2));	// 응답코드						CHAR(2)
		paramCA.put("APP_RSP_MSG",			rstMsg);				                // 응답메세지					VARCHAR2(30)
		paramCA.put("APP_C2S_TRAN_NBR",		mapReserve.get("TRAN_NBR"));			// 씨스퀘어거래번호				CHAR(12)
		paramCA.put("ISU_CD",				cardCode);								// 발급사코드					CHAR(2)
		paramCA.put("ISU_NAME",				cardName);				                // 발급사명						VARCHAR2(16)
		paramCA.put("ISU_CARD_NAME",		cardName);				                // 발급사카드명					VARCHAR2(16)
		paramCA.put("ACQ_CD",				"");									// 매입사코드					CHAR(2)
		paramCA.put("ACQ_NAME",				"");									// 매입사명						VARCHAR2(16)
		paramCA.put("APP_CUST_TEL",			"");									// 카드고객연락처					CHAR(15)
		paramCA.put("APP_CUST_EMAIL",		"");			                        // 카드고객이메일					VARCHAR2(50)
		paramCA.put("GOODS_NAME",			mapReserve.get("TRAN_COMPANY_CD").toString() + mapReserve.get("PROGRAM_CD").toString());		// 상품명(등록회사+프로그램코드)		VARCHAR2(30)
		paramCA.put("SETTLE_FLAG",			"N");									// 매입여부					 	CHAR(1)
		paramCA.put("CANCEL_FLAG",			"N");									// 취소여부					 	CHAR(1)
		paramCA.put("INSERT_USER",			"SYSTEM");								// 등록자						CHAR(12)
		paramCA.put("UPDATE_DATE",			"");									// 수정일자					 	CHAR(8)
		paramCA.put("UPDATE_TIME",			"");									// 수정시간					 	CHAR(6)
		paramCA.put("RSP_DISC_AMT",			0);										// 응답할인금액					NUMBER(8)
		paramCA.put("REQ_APP_AMT",			salesPrice);			                // 승인요청금액					NUMBER(8)
		paramCA.put("STATUS",				"3");									// 처리상태						CHAR(1)
		paramCA.put("ENC_CARD_NO_FLAG",		"2");									// ENC_CARD_NO_FLAG			CHAR(1)
		paramCA.put("REMARK",		        mapParam.get("cardTradeNo"));		    // C2 결제번호
		
		//T_TRAN & T_RESERVE : 결제 완료 후 입금구분 상태 변경 : 2016.07.06
		HashMap<String, Object> paramRT = new HashMap<String, Object>();
		paramRT.put("OID",						mapParam.get("oid"));
		paramRT.put("LAST_TRAN_TYPE",           Code.TRAN_TYPE_RESERVE);
		
		// 가상계좌 전용
//		vAccountTradeNo
//		vAccountBankName
//		bankCode
//		vAccount
//		returnType										// callback
//		vAccountName									// callback
//		vCriticalDate									// callback
//		vAccountApprovTime								// callback
		
		boolean resultPR = false;
		boolean resultPT = false;
		boolean resultCA = false;
		boolean resultRT = false;
		try {
			System.out.println(">>>>> paramPR :\n" + paramPR.toString());
			entranceMapper.regPaymentResult(paramPR);
			resultPR = true;
			
			System.out.println(">>>>> paramPT :\n" + paramPT.toString());
			entranceMapper.regPaymentTicket(paramPT);
			resultPT = true;

			System.out.println(">>>>> paramCA :\n" + paramCA.toString());
			entranceMapper.regCardApprove(paramCA);
			resultCA = true;
			
			//T_PAYMENT_RESULT 변경이 있을 경우 변경
			System.out.println(">>>>> paramRT :\n" + paramRT.toString());
			entranceMapper.updateReserveTran(paramRT);
			resultRT = true;
			
//			sendSMS((String) mapParam.get("oid"));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return (resultPR && resultPT && resultCA && resultRT);
	}
	
	
	private void sendSMS(String aOID) {
	      //결제완료 시 SMS 발송
	      
	      //SYSTEM_TYPE      1. 1. LEC: 강의, MEM: 맴버
	      //RESER_DATETIME -    2. 예약일시
	      //SUB_TITLE_NAME -    3. 프로그램명
	      //SCHEDULE   -       4. 프로그램 일자,시간
	      //PAYMENT_AMT   -    5. 총 결제금액
	      //LIMIT_DATE   -    6. 결제를 해야 할 기간
	      //MEMBER_NO   -       6. 사용자번호
        try{
	      Map map = new HashMap();
	         
	      map.put("SYSTEM_TYPE", "LEC");
	      map.put("COMPANY_CD", propertyHelper.getCompanycd());
	      //map.put("companyCd", propertyHelper.getCompanycd());


	      //예약자 정보인 ORDNO 으로 COURSE_CD,PRICE_NBR, MEMBER_NO, PAYMENT_AMT, LIMIT_DATE (RESER_DATE, RESER_TIME = RESER_DATETIME) 을 가져옴
	      Map cam = entranceService.getSmsData(aOID);
	      //map.put("COURSE_CD", cam.get("COURSE_CD"));
	      //map.put("PRICE_NBR", cam.get("PRICE_NBR"));
	      map.put("MEMBER_NO", cam.get("MEMBER_NO"));
	      map.put("RESER_DATETIME", cam.get("TRAN_DATE").toString() + cam.get("TRAN_TIME").toString());
	      map.put("PAYMENT_AMT", cam.get("PAYMENT_AMT"));
	      //String limitDate = cam.get("RESERVE_LIMIT_DATE").toString();
	      
	      //limitDate = limitDate.substring(2, 4) + "년  " + limitDate.substring(4, 6) + "월 " + limitDate.substring(6, 8) + "일 "; 
	      map.put("LIMIT_DATE", cam.get("VA_CRITICALDATE"));
	      
	      //가져온 COURSE_CD, PRICE_NBR로 예약정보를 가져옴. SUB_TITLE_NAME, SCHEDULE
	      //Map cm = paymentCheckService.get_SELECT_COURSE_CHECK(map);
	      map.put("SUB_TITLE_NAME", cam.get("PROGRAM_NAME"));
	      //String schedule = "";
	      //if(cm.get("COURSE_DATE") != null){
	      //   schedule += Utility.CheckObjectNull(cm.get("COURSE_DATE")).substring(4, 6) + "." + Utility.CheckObjectNull(cm.get("COURSE_DATE")).substring(6, 8);
	      //   schedule += "(" + DateTime.whichDayString(Utility.CheckObjectNull(cm.get("COURSE_DATE"))) + ")-";
	      //}
	      //schedule += Utility.CheckObjectNull(cm.get("COURSE_START_TIME")).substring(0, 2) + ":" + Utility.CheckObjectNull(cm.get("COURSE_START_TIME")).substring(2, 4);
	      map.put("SCHEDULE", cam.get("SCHEDULE"));
	      map.put("SMS_FLAG", "Y");
	      map.put("SMS_FLAG_LAYOUT", "Y");
	      
	      map.put("CONTENTS_TITLE", "국립과천과학관 예약신청 안내");
	      map.put("SMS_CONTENTS", "[국립과천과학관] " + cam.get("PROGRAM_NAME") + " " + cam.get("SCHEDULE") + "예약 됐습니다.");
	      
	      
	      Map um = userMapper.getLegacyUser(map);
	      
	      um.put("CRM_UNIQUE_NBR", "CRM" + DateTime.getFormatDateString("yyyyMMddHHmmss", 0) + (int) (Math.random() * 1000));
	      um.put("RETURN_EMAIL", returnEmail);
	      um.put("RETURN_NAME", returnName);
	      um.put("CALLBACK", callback);
	      um.put("SEND_FLAG", "N");
	      
	      map.putAll(um);
	      
	      smsemailMapper.insertSmsHistory(map);
	      //smsEmailService.setSms(map);
        }catch(Exception e){
        	e.printStackTrace();
        }	      
	   }	
	
	
	
	enum ERR_DEF {
		NOT_LOGIN	(501, "로그인 정보를 찾을 수 없음"),
		UNKNOWN		(999, "알수 없는 오류")
		;
        
		private int code;
		private String message;
 
        private ERR_DEF(int code, String message) {
			this.code = code;
			this.message = message;
        }
        
		public HashMap<String, Object> toHashMap() {
			HashMap<String, Object> result = new HashMap<String, Object>();
			result.put("code",		this.code);
			result.put("message",	this.message);

			return result;
		}
	}
	
//	
//	public interface ITelegramConverter {
//		/**
//		 * Map을 전문 통신 규격에 맞게 변환
//		 */
//		public String toTelegram(HashMap map);
//		/**
//		 * 전문 통신 응답을 Map으로 변환
//		 */
//		public HashMap toMap(String telegram);
//	}
//	
//	/**
//	 * 입장 등록 전문 통신 변환
//	 * @author jmkim
//	 *
//	 */
//	public class EntRegConv implements ITelegramConverter {
//		@Override
//		public String toTelegram(HashMap map) {
//			StringBuilder builder = new StringBuilder();
//			
//			builder.append(String.format("%4s", map.get("name")));		// 이름
//			
//			builder.append(12)
//				.append("aaa")
//				.append(123)
//			;
//			
//			//...
//			
//			return builder.toString();
//		}
//
//		@Override
//		public HashMap toMap(String telegram) {
//			HashMap<String, Object> map = new HashMap<String, Object>();
//			
//			map.put("name", telegram.substring(10, 20));
//			// ....
//			
//			return map;
//		}
//	}
//	
//	
//	/**
//	 * 입장 수정 전문 통신 변환
//	 * @author jmkim
//	 *
//	 */
//	public class EntModConv implements ITelegramConverter {
//		@Override
//		public String toTelegram(HashMap map) {
//			StringBuilder builder = new StringBuilder();
//			
//			builder.append(String.format("%4s", map.get("name")));		// 이름
//			//...
//			
//			return builder.toString();
//		}
//
//		@Override
//		public HashMap toMap(String telegram) {
//			HashMap<String, Object> map = new HashMap<String, Object>();
//			
//			map.put("name", telegram.substring(10, 20));
//			// ....
//			
//			return map;
//		}
//	}
//	
//	
//	/**
//	 * 입장 취소 전문 통신 변환
//	 * @author jmkim
//	 *
//	 */
//	public class EntCanConv implements ITelegramConverter {
//		@Override
//		public String toTelegram(HashMap map) {
//			StringBuilder builder = new StringBuilder();
//			
//			builder.append(String.format("%4s", map.get("name")));		// 이름
//			//...
//			
//			return builder.toString();
//		}
//
//		@Override
//		public HashMap toMap(String telegram) {
//			HashMap<String, Object> map = new HashMap<String, Object>();
//			
//			map.put("name", telegram.substring(10, 20));
//			// ....
//			
//			return map;
//		}
//	}
//
//
//
//	/**
//	 * 전문 통신 클래스
//	 * 
//	 * @author jmkim
//	 *
//	 */
//	public class Telegram {
//		private String mServerIP = "";
//		private int mServerPort = 0;
//		private boolean isDebug = false;
//		
//		public Telegram(String ip, int port) {
//			mServerIP = ip;
//			mServerPort = port;
//		}
//
//		public Telegram(String ip, int port, boolean debug) {
//			mServerIP = ip;
//			mServerPort = port;
//			isDebug = debug;
//		}
//
//		public String send(String msg) {
//			Socket socket = null;
//			DataInputStream dis = null;
//			DataOutputStream dos = null;
//			
//			String recvMsg = null;
//			
//			try {
//				printDebug("연결중... : " + mServerIP + ":" + mServerPort);
//				socket = new Socket(mServerIP, mServerPort);
//
//				InputStream in = socket.getInputStream();
//				dis = new DataInputStream(in);				
//				OutputStream out = socket.getOutputStream();
//				dos = new DataOutputStream(out);
//				
//				// Send Message
//				dos.writeUTF(msg);
//				printDebug("Send Message : " + msg);
//				
//				// Recv Message
//				recvMsg = dis.readUTF();
//				printDebug("Recv Message : " + recvMsg);
//			} catch (Exception e) {
//				e.printStackTrace();
//			} finally {
//				try {
//					dis.close();
//				} catch (IOException e) {}
//
//				try {
//					dos.close();
//				} catch (IOException e) {}
//
//				try {
//					socket.close();
//				} catch (IOException e) {}
//			}
//			
//			return recvMsg;
//		}
//		
//		private void printDebug(String message) {
//			if (isDebug)
//				System.out.println(message);
//		}
//	}
}
