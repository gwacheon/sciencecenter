package kr.go.sciencecenter.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.go.sciencecenter.model.BreadCrumb;
import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.model.Reply;
import kr.go.sciencecenter.model.ReplyDetail;
import kr.go.sciencecenter.model.Survey;
import kr.go.sciencecenter.service.SurveyService;

@Controller
@RequestMapping("/surveys")
public class SurveyController {
private static final Logger logger = LoggerFactory.getLogger(SurveyController.class);
	
	@Autowired
	private SurveyService surveyService;
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String index(@RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			Model model,
			HttpServletRequest request) {
		
		// pagination setting
		String requestURI = request.getRequestURI();
		Page page = new Page(currentPage, 10, surveyService.countAll());
		String path = requestURI + "?";
		page.setPath(path);
		model.addAttribute("page", page);
		
		List<Survey> surveys = surveyService.list(page);
		
		Date today = new Date();
		
		for (Survey survey : surveys) {
			if(survey.getBeginTime().compareTo(today) < 0 && survey.getEndTime().compareTo(today) > 0){
				survey.setAvailableType(1); //진행중
			}else if(survey.getBeginTime().compareTo(today) > 0){
				survey.setAvailableType(2); //예정
			}else{
				survey.setAvailableType(0); //종료
			}
		}
		
		model.addAttribute("surveys", surveys);
		
		model.addAttribute("crumb", new BreadCrumb(6,4,6));
		
		return "survey/index.app";
	}
	
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String show(Model model, @PathVariable int id) {
		
		Survey survey = surveyService.findWithArticles(id);
		
		model.addAttribute("survey", survey);
		model.addAttribute("crumb", new BreadCrumb(6,4,6));
		return "survey/show.app";
	}
	
	@RequestMapping(value = "/{id}/replies", method = RequestMethod.POST)
	public @ResponseBody Model replies(@RequestBody String jsonStr
			, Model model, @PathVariable int id
			, HttpServletRequest request) {
		
		String remoteIpAddress = request.getRemoteAddr();
		
		JSONObject jsonObj = new JSONObject(jsonStr);
		
		Survey survey = surveyService.find(id);
		
		boolean error = false;
		
		if(!survey.isMultiple()){
			Reply existReply = surveyService.findReplyWithIpAddress(id, remoteIpAddress);
			
			if(existReply != null){
				error = true;
				model.addAttribute("error_msg", "이미 설문조사에 참여한 IP 입니다.");
			}
		}
		
		
		if(!error){
			Reply reply = new Reply(id, remoteIpAddress, jsonObj.getJSONObject("reply"));
			List<ReplyDetail> details = new ArrayList<ReplyDetail>();
			
			if(jsonObj.has("reply_details")){
				JSONObject jsonDetails = jsonObj.getJSONObject("reply_details");
				Set<String> keys = jsonDetails.keySet();
				
				for (String key : keys) 
				{
					int articleId = Integer.parseInt(key);
					JSONArray detailItems = jsonDetails.getJSONArray(key);
					
					for(int i = 0; i < detailItems.length(); i++){
						details.add(new ReplyDetail(reply.getId(), articleId, detailItems.getString(i)));
					}
				}
			}
			
			surveyService.createReply(reply, details);
		}
		
		model.addAttribute("error", error);
		
		return model;
	}
}
