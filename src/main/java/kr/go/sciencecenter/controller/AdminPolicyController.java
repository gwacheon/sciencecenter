package kr.go.sciencecenter.controller;

import java.util.List;

import kr.go.sciencecenter.controller.admin.AdminSupportController;
import kr.go.sciencecenter.model.Policy;
import kr.go.sciencecenter.service.PolicyService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/admin/policies")
public class AdminPolicyController {

	private static final Logger logger = LoggerFactory.getLogger(AdminPolicyController.class);
	
	@Autowired
	private PolicyService policyService;
	
	@ModelAttribute
	public void setCategories(Model model) {
		model.addAttribute("currentAdminCategory", "policies");
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String list(Model model) {
		
		List<Policy> policies = policyService.listAll();
		
		model.addAttribute("policies", policies);
		return "policies/index.adm";
	}
	
	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public String newPolicy(Model model) {
		
		Policy policy = new Policy();
		policy.initTypeMap();
		model.addAttribute("policy", policy);
		return "policies/new.adm";
	}
	
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String create(@ModelAttribute("supportEvaluation") Policy policy,
			Model model) {
		
		policyService.create(policy);
		return "redirect:/admin/policies";
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String showPolicy(@PathVariable int id, Model model) {
		
		Policy policy = policyService.find(id);
		model.addAttribute("policy", policy);
		return "policies/show.adm";
	}
	
	@RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
	public String editPolicy(@PathVariable int id, Model model) {
		
		Policy policy = policyService.find(id);
		policy.initTypeMap();
		
		model.addAttribute("policy", policy);
		return "policies/edit.adm";
	}
	
	@RequestMapping(value = "/{id}/update", method = RequestMethod.PUT)
	public String updatePolicy(@ModelAttribute("policy") Policy policy, 
			Model model) {
		
		policyService.update(policy);
		return "redirect:/admin/policies";
	}
	
	@RequestMapping(value = "/{id}/delete", method = RequestMethod.DELETE)
	public String deletePolicy(@PathVariable int id, Model model) {
		
		policyService.delete(id);
		return "redirect:/admin/policies";
	}
}
