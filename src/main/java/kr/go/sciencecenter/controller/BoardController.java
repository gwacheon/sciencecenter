package kr.go.sciencecenter.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import kr.go.sciencecenter.model.Board;
import kr.go.sciencecenter.model.api.Member;
import kr.go.sciencecenter.service.BoardService;

@Controller
@RequestMapping("/boards")
@SessionAttributes(Member.CURRENT_MEMBER_NO)
public class BoardController {
	private static final Logger logger = LoggerFactory.getLogger(BoardController.class);

	@Autowired
	BoardService boardService;
	
	@Autowired
	private Environment env;

	@RequestMapping(value = "{type}", method = RequestMethod.GET)
	public String list(Model model, @PathVariable String type) {
		model.addAttribute("type", type);
		return "boards/index.app";
	}

//	@RequestMapping(value = "/getBoards/{type}", method = RequestMethod.POST)
//	public Model getBoards(@RequestBody String jsonStr, HttpServletRequest request, Model model, Locale locale,
//			@PathVariable String type, @RequestParam(value = "page", defaultValue = "1") Integer currentPage,
//			@RequestParam(value = "searchType", required = false) String searchType,
//			@RequestParam(value = "searchKey", required = false) String searchKey) {
//
//		String path = "/boards?";
//		Page page = null;
//		
//		List<Board> boards = null;
//		// faq total
//		if(type.equals("faqTotal")) {
//			List<String> faqTypes = new ArrayList<String>();
//			faqTypes.add("faqScience");
//			faqTypes.add("faqEducation");
//			faqTypes.add("faqRole");
//			faqTypes.add("faqFacilities");
//			faqTypes.add("faqRestaurant");
//			faqTypes.add("faqUser");
//			faqTypes.add("faqDisplay");
//			faqTypes.add("faqReservation");
//			faqTypes.add("faqPark");
//			faqTypes.add("faqHomepage");
//			faqTypes.add("faqMarketing");
//			
//			page = new Page(currentPage, 9, boardService.countFaqTotal(faqTypes));
//			
//			boards = boardService.listFaqTotal(faqTypes, page, searchType, searchKey, null);
//
//			model.addAttribute("boardType", "faqTotal");
//
//			page.setPath(path);
//			page.setClassName("mobile_page");
//			model.addAttribute("page", page);
//		}
//		else {
//			page = new Page(currentPage, 9, boardService.count(type, searchType, searchKey));
//			
//			boards = boardService.list(type, page, searchType, searchKey, null);
//			
//			model.addAttribute("boardType", BoardType.BOARD_TYPES.get(type));			
//		}
//		page.setPath(path);
//		page.setClassName("mobile_page");
//		model.addAttribute("page", page);
//		
//		model.addAttribute("boards", boards);
//		model.addAttribute("type", type);
//		return model;
//	}

	@RequestMapping(value = "/showBoard/{id}", method = RequestMethod.POST)
	public Model showBoard(@PathVariable int id, HttpServletRequest request, Model model, Locale locale) {

		Board board = boardService.find(id);
		if( board != null ) {
			boardService.addCount(board);
			board.setCount(board.getCount() + 1);
		}
		model.addAttribute("board", board);
		
		if (board.getBoardId() > 0) {
			Board parentBoard = boardService.find(board.getBoardId());
			model.addAttribute("prevBoard", boardService.getPrevBoard(parentBoard.getId(), parentBoard.getBoardType()));
			model.addAttribute("nextBoard", boardService.getNextBoard(parentBoard.getId(), parentBoard.getBoardType()));
		} else {
			model.addAttribute("prevBoard", boardService.getPrevBoard(id, board.getBoardType()));
			model.addAttribute("nextBoard", boardService.getNextBoard(id, board.getBoardType()));
		}

		return model;
	}

	@RequestMapping(value = "/board_attatchment/{id}/{filename:.+}", method = RequestMethod.GET)
	public void attatchmentDowonload(@PathVariable int id, @PathVariable String filename, HttpServletRequest request,
			HttpServletResponse response) {

		FileInputStream inputStream;
		OutputStream outStream;

		try {
			String fileName = filename;
			String mimeType = "application/octet-stream";

			String filePath = env.getProperty("file.path");
			filePath += "resources/upload/board_attatchment/" +id + "/"+filename;

			fileName = URLEncoder.encode(fileName, "utf-8");

			File downloadFile = new File(filePath);

			inputStream = new FileInputStream(downloadFile);

			response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\";");
			response.setHeader("Content-Transfer-Encoding", "binary");
			response.setContentType(mimeType);
			response.setContentLength((int) downloadFile.length());

			outStream = response.getOutputStream();

			byte[] buffer = new byte[4096];
			int bytesRead = -1;

			// write bytes read from the input stream into the output stream
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}

			inputStream.close();
			outStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}