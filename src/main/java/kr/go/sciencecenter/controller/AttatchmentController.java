package kr.go.sciencecenter.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import kr.go.sciencecenter.model.Attatchment;
import kr.go.sciencecenter.model.BoardFile;
import kr.go.sciencecenter.service.AttatchmentService;
import kr.go.sciencecenter.service.BoardFileService;

@Controller
@RequestMapping(value = "/attatchments")
public class AttatchmentController {

	@Autowired
	private AttatchmentService attatchmentService;
	
	@Autowired
	private BoardFileService boardFileService;

	/**
	 * Ckeditor 이미지 업로드
	 * @param CKEditorFuncNum
	 * @param file
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String createModel(@RequestParam(value = "CKEditorFuncNum", required = false) int CKEditorFuncNum,
			@RequestParam(value = "upload", required = false) CommonsMultipartFile file, Model model) {
		
		Attatchment attatchment = new Attatchment();
		attatchmentService.create(attatchment, file);

		model.addAttribute("CKEditorFuncNum", CKEditorFuncNum);
		model.addAttribute("url", attatchment.getUrl());

		return "/attatchment/success.app";
	}
	
	@RequestMapping(value = "/download/{id}", method = RequestMethod.GET)
	public void download(@PathVariable("id")int id
			,HttpServletRequest request, HttpServletResponse response) {
		
		BoardFile file = boardFileService.find(id);
		
		
	}
}
