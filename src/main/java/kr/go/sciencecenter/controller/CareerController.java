package kr.go.sciencecenter.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import kr.go.sciencecenter.model.Career;
import kr.go.sciencecenter.service.CareerService;

@Controller
@RequestMapping("/career")
public class CareerController {
	
	@Autowired
	private CareerService careerService;
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String index(Model model){
		List<Career> careers = careerService.list();
		model.addAttribute("careers", careers);
		
		return "career/index.app";
	}
}
