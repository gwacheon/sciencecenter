package kr.go.sciencecenter.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import kr.go.sciencecenter.api.AuthenticationApi;
import kr.go.sciencecenter.model.Voluntary;
import kr.go.sciencecenter.model.VoluntaryTime;
import kr.go.sciencecenter.model.Volunteer;
import kr.go.sciencecenter.model.api.Member;
import kr.go.sciencecenter.service.VoluntaryService;

@Controller
@RequestMapping(value = "/voluntary")
@SessionAttributes(Member.CURRENT_MEMBER_NO)
public class VoluntaryController {
	private static final Logger logger = LoggerFactory.getLogger(VoluntaryController.class);
	
	@Autowired
	VoluntaryService voluntaryService;
	
	@ModelAttribute
	public void setCategories(Model model) {
		model.addAttribute("currentCategory", "main.nav.catetory5");
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String index(Model model){
		return "schedules/voluntary.app";
	}
	
	@RequestMapping(value = "/application", method = RequestMethod.GET)
	public String application(@ModelAttribute(Member.CURRENT_MEMBER_NO) String memberNo
			, Model model){
		logger.info("Member No : " + memberNo);
		Member currentMember = getCurrentMember(memberNo);
		model.addAttribute("currentMember", currentMember);
		
		List<Voluntary> voluntaries = voluntaryService.list();
		model.addAttribute("voluntaries", voluntaries);
		
		return "voluntary/application.app";
	}
	
	@RequestMapping(value = "/voluntaryForm", method = RequestMethod.GET)
	public void voluntaryFormDownload(HttpServletRequest request,
			HttpServletResponse response){
		
		FileInputStream inputStream;
		OutputStream outStream;

		try {
			String fileName = "volunteer_form.hwp";
			String mimeType = "application/octet-stream";
			String webappRootPath = request.getSession().getServletContext().getRealPath("/");
			String filePath = webappRootPath + "/resources/downloads/voluntary/" + fileName;

			fileName = URLEncoder.encode(fileName, "utf-8");
			File downloadFile = new File(filePath);
			
			inputStream = new FileInputStream(downloadFile);
			
			response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\";");
			response.setHeader("Content-Transfer-Encoding", "binary");
	    	response.setContentType(mimeType);
	        response.setContentLength((int) downloadFile.length());
	        
	        outStream = response.getOutputStream();
	        
	        byte[] buffer = new byte[4096];
	        int bytesRead = -1;
			
	        // write bytes read from the input stream into the output stream
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }
	        
	        inputStream.close();
	        outStream.close();
	        
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	@RequestMapping(value = "/{id}/times", method = RequestMethod.GET)
	public Model times(@PathVariable("id") int id, Model model){
		Voluntary voluntary = voluntaryService.findTimesWithVolunteerCount(id, false, "asc");
		
		model.addAttribute("voluntary", voluntary);
		
		return model;
	}
	
	@RequestMapping(value = "/application/{timeId}/back", method = RequestMethod.POST)
	public Model application(@PathVariable("timeId") int timeId
			, @ModelAttribute(Member.CURRENT_MEMBER_NO) String memberNo
			, Model model){
		VoluntaryTime time = voluntaryService.findTime(timeId);
		
		Volunteer volunteer = voluntaryService.applicationVolunteer(memberNo, time);
		model.addAttribute("volunteer", volunteer);
		model.addAttribute("currentTime", new Date());
		
		return model;
	}
	
	@RequestMapping(value = "/application/{timeId}", method = RequestMethod.POST)
	public @ResponseBody Model processApplicant(@RequestBody String jsonStr,
			@PathVariable("timeId") int timeId,
			HttpServletRequest request, Model model){
		
		String currentMemberNo = getCurrentMemberNo(request);
		Volunteer volunteer = new Volunteer(currentMemberNo, jsonStr);
		
		voluntaryService.processApplicant(timeId, volunteer);
		
		model.addAttribute("volunteer", volunteer);
		model.addAttribute("currentTime", new Date());
		
		return model;
	}
	
	@RequestMapping(value = "/application/{volunteerId}/info", method = RequestMethod.POST)
	public @ResponseBody Model addInfo(@RequestBody String jsonStr,
			HttpServletRequest request,
			@PathVariable("volunteerId") int volunteerId, Model model){
		
		Volunteer volunteer = voluntaryService.findVolunteer(volunteerId);
		
		JSONObject jsonObj = new JSONObject(jsonStr);
		volunteer.setName(jsonObj.getString("name"));
		volunteer.setPhone(jsonObj.getString("phone"));
		volunteer.setEmail(jsonObj.getString("email"));
		volunteer.setSchool(jsonObj.getString("school"));
		volunteer.setGrade(jsonObj.getString("grade"));
		volunteer.setGroupNo(jsonObj.getString("group_no"));
		volunteer.setMemberNo(jsonObj.getString("member_no"));
		volunteer.setOtherId(jsonObj.getString("other_id"));
		volunteer.setIntroduction(jsonObj.getString("introduction"));
		
		voluntaryService.updateVolunteerInfo(volunteer);
		
		model.addAttribute("volunteer", volunteer);
		
		return model;
	}
	
	@RequestMapping(value = "/{id}/certificate", method = RequestMethod.GET)
	public ModelAndView certificate(@PathVariable("id") int volunteerId){
		Volunteer volunteer = voluntaryService.findVolunteer(volunteerId);
		
		if(volunteer.getStatus().equals("completion")){
			
		}
		
		return new ModelAndView("PdfView","volunteer", volunteer);
	}
	
	public String getCurrentMemberNo(HttpServletRequest request) {
		return (String)request.getSession().getAttribute(Member.CURRENT_MEMBER_NO);
	}
	
	private Member getCurrentMember(String memberNo) {
		return AuthenticationApi.getInstance().getInfo(memberNo);
	}
}
