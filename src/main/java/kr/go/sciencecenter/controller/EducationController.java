package kr.go.sciencecenter.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.go.sciencecenter.model.Board;
import kr.go.sciencecenter.model.BreadCrumb;
import kr.go.sciencecenter.service.BoardService;
import kr.go.sciencecenter.util.DownloadHelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/education")
public class EducationController {

	private static final Logger logger = LoggerFactory.getLogger(EducationController.class);
	
	@Autowired
	private BoardService boardService;
	
	/**
	 * 교육안내 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/educationGuide", method = RequestMethod.GET)
	public String educationIntro(Model model) {
		model.addAttribute("crumb", new BreadCrumb(3,1,1));
		return "education/educationGuide.app";
	}
	
	
	/**
	 * 교육공지 게시판
	 * @param model
	 * @param currentPage
	 * @param searchType
	 * @param searchKey
	 * @return
	 */
	@RequestMapping(value = "/educationNotice", method = RequestMethod.GET)
	public String educationNotice(Model model, 
			@RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			@RequestParam(value = "searchType", required=false) String searchType,
			@RequestParam(value = "searchKey", required=false) String searchKey) {
		
		Board.getBoards(model, boardService, "educationNotice", currentPage, searchType, searchKey);
		model.addAttribute("crumb", new BreadCrumb(3,1,2));
		
		return "education/educationNotice.app";
	}
	
	
	/**
	 * 수업게시판 show
	 * @param id 게시판 id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/educationNotice/{id}", method = RequestMethod.GET)
	public String educationNoticeShow(@PathVariable int id, Model model) {

		model.addAttribute("id", id);
		model.addAttribute("crumb", new BreadCrumb(3,1,2));
		return "education/educationNoticeShow.app";
	}
	
	
	/**
	 * 환불안내 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/refund", method = RequestMethod.GET)
	public String refund(Model model) {
		
		model.addAttribute("crumb", new BreadCrumb(3,1,3));
		return "education/refund.app";
	}
	
	/**
	 * 전시관체험 - 천문우주프로그램
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/planetarium/totalProgram", method=RequestMethod.GET)
	public String planetariumProgram(Model model){
		
		model.addAttribute("currentCategory", "main.nav.catetory3");
		
		model.addAttribute("crumb", new BreadCrumb(3,2,3));
		return "display/planetarium/totalProgram.app";
	}
	
	/**
	 * 수업게시판 index
	 * @param model
	 * @param currentPage pagination 현재페이지
	 * @param searchType 게시판 검색타입
	 * @param searchKey 게시판 검색어
	 * @return
	 */
	@RequestMapping(value = "/classBoard", method = RequestMethod.GET)
	public String classBoard(Model model, 
			@RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			@RequestParam(value = "searchType", required=false) String searchType,
			@RequestParam(value = "searchKey", required=false) String searchKey) {
		
		Board.getBoards(model, boardService, "classBoard", currentPage, searchType, searchKey);
		model.addAttribute("crumb", new BreadCrumb(3,2,4));
		return "education/classBoard.app";
	}
	
	/**
	 * 수업게시판 show
	 * @param id 게시판 id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/classBoard/{id}", method = RequestMethod.GET)
	public String classBoardShow(@PathVariable int id, Model model) {
		model.addAttribute("crumb", new BreadCrumb(3,2,4));
		model.addAttribute("id", id);
		return "education/classBoardShow.app";
	}
	
	/**
	 * 과학융합탐구 게시판 index
	 * @param model
	 * @param currentPage pagination 현재페이지
	 * @param searchType 게시판 검색타입
	 * @param searchKey 게시판 검색어
	 * @return
	 */
	@RequestMapping(value = "/fusionScience", method = RequestMethod.GET)
	public String fusionScience(Model model,
			@RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			@RequestParam(value = "searchType", required=false) String searchType,
			@RequestParam(value = "searchKey", required=false) String searchKey) {
		
		Board.getBoards(model, boardService, "fusionScience", currentPage, searchType, searchKey);
		model.addAttribute("crumb", new BreadCrumb(3,1,3));
		return "education/fusionScience.app";
	}
	
	/**
	 * 과학융합탐구 게시판 show
	 * @param id 게시판 id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/fusionScience/{id}", method = RequestMethod.GET)
	public String fusionScienceShow(@PathVariable int id, Model model) {
		
		model.addAttribute("id", id);
		model.addAttribute("crumb", new BreadCrumb(3,1,3));
		return "education/fusionScienceShow.app";
	}
	
	
	/**
	 * 거꾸로 과학탐구 교실 게시판 index
	 * @param model
	 * @param currentPage pagination 현재페이지
	 * @param searchType 게시판 검색타입
	 * @param searchKey 게시판 검색어
	 * @return
	 */
	@RequestMapping(value = "/reverseScience", method = RequestMethod.GET)
	public String reverseScience(Model model,
			@RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			@RequestParam(value = "searchType", required=false) String searchType,
			@RequestParam(value = "searchKey", required=false) String searchKey) {
		
		Board.getBoards(model, boardService, "reverseScience", currentPage, searchType, searchKey);
		model.addAttribute("crumb", new BreadCrumb(3,1,3));
		return "education/reverseScience.app";
	}
	
	/**
	 * 거꾸로 과학탐구 교실 게시판 show
	 * @param id 게시판 id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/reverseScience/{id}", method = RequestMethod.GET)
	public String reverseScienceShow(@PathVariable int id, Model model) {
		
		model.addAttribute("id", id);
		model.addAttribute("crumb", new BreadCrumb(3,1,3));
		return "education/reverseScienceShow.app";
	}
	
	/**
	 * 생각트리 과학탐구 교실 게시판 index
	 * @param model
	 * @param currentPage pagination 현재페이지
	 * @param searchType 게시판 검색타입
	 * @param searchKey 게시판 검색어
	 * @return
	 */
	@RequestMapping(value = "/tree", method = RequestMethod.GET)
	public String tree(Model model,
			@RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			@RequestParam(value = "searchType", required=false) String searchType,
			@RequestParam(value = "searchKey", required=false) String searchKey) {
		
		Board.getBoards(model, boardService, "tree", currentPage, searchType, searchKey);
		model.addAttribute("crumb", new BreadCrumb(3,1,3));
		return "education/tree.app";
	}	
	
	/**
	 * 생각트리 과학탐구 교실 게시판 show
	 * @param id 게시판 id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/tree/{id}", method = RequestMethod.GET)
	public String treeShow(@PathVariable int id, Model model) {
		model.addAttribute("id", id);
		model.addAttribute("crumb", new BreadCrumb(3,1,3));
		return "education/treeShow.app";
	}
	
	/**
	 * 창의체험 / 개인(주말) 신청
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/experienceWeekends", method = RequestMethod.GET)
	public String experienceWeekends(Model model) {
		
		model.addAttribute("crumb", new BreadCrumb(3,2,1));
		return "education/experienceWeekends.app";
	}
	
	/**
	 * 창의체험 / 개인(방학) 신청
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/experienceVacation", method = RequestMethod.GET)
	public String experienceVacation(Model model) {
		model.addAttribute("crumb", new BreadCrumb(3,2,2));
		return "education/experienceVacation.app";
	}
	
	/**
	 * 창의체험 / 단체(주중) 신청
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/group", method = RequestMethod.GET)
	public String group(Model model) {
		model.addAttribute("crumb", new BreadCrumb(3,3,3));
		return "education/group.app";
	}
	
	/**
	 * 단체(주중) 체험 페이지 내의 "주제별 설명 및 유의사항 다운로드"
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/download/groupDescription", method = RequestMethod.GET)
	public void downloadGroupDescription(Model model,
			HttpServletRequest request, HttpServletResponse response) {

		DownloadHelper.downloadFile(request, response, "education", "첨부2. 16년 창의체험 과학탐구(단체) 주제별 강의계획서(16.07.22).hwp","1.hwp");
	}
	
	/**
	 * 단체(주중) 체험 페이지 내의 "신청서 다운로드"
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/download/groupApplication", method = RequestMethod.GET)
	public void downloadGroupApplication(Model model,
			HttpServletRequest request, HttpServletResponse response) {

		DownloadHelper.downloadFile(request, response, "education", "첨부3. 16년 창의체험 과학탐구(단체) 참가신청서(16.07.22).hwp","2.hwp");
	}
	
	/**
	 * 단체(주중) 체험 페이지 내의 "토요일 동아리 신청서 다운로드"
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/download/SATApplication", method = RequestMethod.GET)
	public void downloadGroupSATApplication(Model model,
			HttpServletRequest request, HttpServletResponse response) {

		DownloadHelper.downloadFile(request, response, "education", "첨부4. 16년 창의체험 과학탐구(단체) 동아리 참가신청서(16.07.22).hwp","3.hwp");
	}
	
	/**
	 * 단체(주중) 체험 페이지 내의 "변경 및 취소 신청서"
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/download/groupCancel", method = RequestMethod.GET)
	public void downloadGroupCancel(Model model,
			HttpServletRequest request, HttpServletResponse response) {

		DownloadHelper.downloadFile(request, response, "education", "첨부4.+16년+창의체험+과학탐구%28단체%29+취소+변경+신청서.hwp","4.hwp");
	}
	
	
	
	
	/**
	 * 자유학기제 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/course", method = RequestMethod.GET)
	public String course(Model model) {
		model.addAttribute("crumb", new BreadCrumb(3,3,1));
		return "education/course.app";
	}
	
	/**
	 * 주제선택활동 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/topicChoice", method = RequestMethod.GET)
	public String topicChoice(Model model) {
		model.addAttribute("crumb", new BreadCrumb(3,3,2));
		return "education/topicChoice.app";
	}

	/**
	 * 자유학기제 페이지 : "프로그램 신청 시 유의 사항" 다운로드
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/course/download/applicationNotice", method = RequestMethod.GET)
	public void downloadCourseDescription(Model model,
			HttpServletRequest request, HttpServletResponse response) {

		DownloadHelper.downloadFile(request, response, "education", "프로그램신청시유의사항.hwp","5.hwp");
	}
	
	/**
	 * 자유학기제 페이지 : "프로그램 상세안내" 다운로드
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/course/download/description", method = RequestMethod.GET)
	public void downloadCourseNewApplication(Model model,
			HttpServletRequest request, HttpServletResponse response) {

		DownloadHelper.downloadFile(request, response, "education", "프로그램상세안내.hwp","6.hwp");
	}
	
	/**
	 * 자유학기제 페이지 : "집중 및 상시 기간 안내" 다운로드
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/course/download/schedule", method = RequestMethod.GET)
	public void downloadCourseEditApplication(Model model,
			HttpServletRequest request, HttpServletResponse response) {

		DownloadHelper.downloadFile(request, response, "education", "집중+및+상시+접수+기간으로+구분하여+접수.hwp","7.hwp");
	}
	
	
	/**
	 * 자유학기제(진로체험) 페이지 : "신청서" 다운로드
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/course/download/experienceApplication", method = RequestMethod.GET)
	public void downloadCourseExperinceApplication(Model model,
			HttpServletRequest request, HttpServletResponse response) {
		
		DownloadHelper.downloadFile(request, response, "education", "진로체험+참가신청서.hwp","11.hwp");
	}
	
	/**
	 * 자유학기제 페이지 : "변경 및 취소 요청서" 다운로드
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/course/download/editApplication", method = RequestMethod.GET)
	public void downloadCourseApplicationEdit(Model model,
			HttpServletRequest request, HttpServletResponse response) {

		DownloadHelper.downloadFile(request, response, "education", "진로체험+변경+및+취소+신청서.hwp","9.hwp");
	}
	
	/**
	 * 자유학기제(주제선택활동) 페이지 : "신청서" 다운로드
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/course/download/application", method = RequestMethod.GET)
	public void downloadCourseApplication(Model model,
			HttpServletRequest request, HttpServletResponse response) {

		DownloadHelper.downloadFile(request, response, "education", "주제선택활동+참가신청서.hwp","8.hwp");
	}
	
	
	
	
	/**
	 * 교사연수 : 교사현장연수 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/teachersOnSite", method = RequestMethod.GET)
	public String teachersOnSite(Model model) {
		model.addAttribute("crumb", new BreadCrumb(3,4,3));
		return "education/teachersOnSite.app";
	}
	
	/**
	 * 교사연수 : 교사현장연수 계획서 다운로드
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/teachersOnSite/download/plan", method = RequestMethod.GET)
	public void downloadTeachersOnSitePlan(Model model,
			HttpServletRequest request, HttpServletResponse response) {

		DownloadHelper.downloadFile(request, response, "education", "제2기+청춘과학대학+공고문%28확정본%29%28홈페이지%29160622.hwp","10.hwp");
	}
	

	
	/**
	 * 교사연수 : NTTP교사 위탁연수 페이지
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/teachersNTTP", method = RequestMethod.GET)
	public String teachersNTTP(Model model) {
		model.addAttribute("crumb", new BreadCrumb(3,4,3));
		return "education/teachersNTTP.app";
	}
	
	/**
	 * 교육자료실 게시판 index
	 * @param model
	 * @param currentPage pagination 현재페이지
	 * @param searchType 게시판 검색타입
	 * @param searchKey 게시판 검색어
	 * @return
	 */
	@RequestMapping(value = "/educationArchieve", method = RequestMethod.GET)
	public String educationArchieve(Model model, 
			@RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			@RequestParam(value = "searchType", required=false) String searchType,
			@RequestParam(value = "searchKey", required=false) String searchKey) {

		Board.getBoards(model, boardService, "educationArchieve", currentPage, searchType, searchKey);

		return "education/educationArchieve.app";
	}
	
	/**
	 * 교육자료실 게시판 show
	 * @param id boardId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/educationArchieve/{id}", method = RequestMethod.GET)
	public String educationArchieveShow(@PathVariable int id, Model model) {

		model.addAttribute("id", id);
		return "education/educationArchieveShow.app";
	}
	
	/**
	 * 청춘과학대학 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/youthScience", method = RequestMethod.GET)
	public String youthScience(Model model) {
		model.addAttribute("crumb", new BreadCrumb(3,4,1));
		return "education/youthScience.app";
	}
	
}
