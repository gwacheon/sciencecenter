package kr.go.sciencecenter.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.go.sciencecenter.model.Board;
import kr.go.sciencecenter.model.BoardType;
import kr.go.sciencecenter.model.BreadCrumb;
import kr.go.sciencecenter.model.MainEvent;
import kr.go.sciencecenter.model.MainEventSeries;
import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.model.api.Member;
import kr.go.sciencecenter.service.BoardService;
import kr.go.sciencecenter.service.MainEventSeriesService;
import kr.go.sciencecenter.service.MainEventService;

@Controller
@RequestMapping("/mainEvents")
public class MainEventsController {
	
	private static final Logger logger = LoggerFactory.getLogger(MainEventsController.class);
	
	@Autowired
	MainEventService mainEventService;
	
	@Autowired
	MainEventSeriesService mainEventSeriesService;
	
	@Autowired
	private BoardService boardService;
	
	/**
	 * 대표문화행사 '골드버그대회' 등의 상세 페이지
	 * @param mainEventId
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/{mainEventId}", method = RequestMethod.GET)
    public String showMainEvent(@PathVariable int mainEventId, Model model) {
    	
		// option dropdown button 을 위해 해당 Main Event 의 모든 series 를 가져와야 한다.
		MainEvent mainEvent = mainEventService.find(mainEventId);
		// 가장 최근의 Series 를 가져온다.
		MainEventSeries mainEventSeries = mainEventSeriesService.findRecentByMainEventId(mainEventId);
		
		return "redirect:/mainEvents/" + mainEvent.getId() + "/series/" + mainEventSeries.getId();
    }  
	
	
	/**
	 * 각 대표문화행사의 각 차수의 기본 안내페이지
	 * @param mainEventId 메인이벤트 id
	 * @param eventSeriesId 메인이벤트 차수 id
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/{mainEventId}/series/{eventSeriesId}", method = RequestMethod.GET)
	public String showMainEventSeries(@PathVariable int mainEventId,
			@PathVariable int eventSeriesId,
			Model model) {
		
		MainEvent mainEvent = mainEventService.find(mainEventId);
		MainEventSeries mainEventSeries = mainEventSeriesService.find(eventSeriesId);
		mainEventSeries.setDefaultVisibleBoardNames();
		model.addAttribute("mainEvent", mainEvent);
		model.addAttribute("mainEventSeries", mainEventSeries);
			
		
		model.addAttribute("type", "basicInfo");

		BreadCrumb crumb = new BreadCrumb(4,2);
		crumb.setMainEventId(mainEventId);
		model.addAttribute("crumb", crumb);
		
		return "events/mainEvents/show.app";
	}
	
	/**
	 * 각 대표문화행사 차수의 게시판 index
	 * @param mainEventId
	 * @param eventSeriesId
	 * @param type 게시판 종류
	 * @param currentPage
	 * @param searchType
	 * @param searchKey
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/{mainEventId}/series/{eventSeriesId}/{type}", method = RequestMethod.GET)
	public String mainEventSeriesBoardsIndex(@PathVariable("mainEventId") int mainEventId,
			@PathVariable("eventSeriesId") int eventSeriesId, 
			@PathVariable("type") String type,
			@RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			@RequestParam(value = "searchType", required = false) String searchType,
			@RequestParam(value = "searchKey", required = false) String searchKey,
			Model model) {
		
		MainEvent mainEvent = mainEventService.find(mainEventId);
		MainEventSeries mainEventSeries = mainEventSeriesService.find(eventSeriesId);
		mainEventSeries.setDefaultVisibleBoardNames();
		model.addAttribute("mainEvent", mainEvent);
		model.addAttribute("mainEventSeries", mainEventSeries);
		

		int itemsPerPage = 10;
		BoardType boardType = BoardType.BOARD_TYPES.get(type);
		if( boardType.isHasMainPicture() ) {
			itemsPerPage = 6;
		}
		Page page = new Page(currentPage, itemsPerPage, boardService.countEventSeriesBoards(type, eventSeriesId, searchType, searchKey));
		
		page.setPath(boardType.getPath());
		model.addAttribute("page", page);
		List<Board> boards = boardService.listEventSeriesBoards(type, eventSeriesId, page, searchType, searchKey, null);
		model.addAttribute("boards", boards);
		model.addAttribute("type", boardType);
			
		BreadCrumb crumb = new BreadCrumb(4,2);
		crumb.setMainEventId(mainEventId);
		model.addAttribute("crumb", crumb);
		
		return "events/mainEvents/boardIndex.app";
	}
	
	/**
	 * 각 대표문화행사 차수의 게시판 show
	 * @param mainEventId
	 * @param eventSeriesId
	 * @param type
	 * @param currentPage
	 * @param searchType
	 * @param searchKey
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/{mainEventId}/series/{eventSeriesId}/{type}/{id}", method = RequestMethod.GET)
	public String mainEventSeriesBoardsShow(@PathVariable int mainEventId,
			@PathVariable int eventSeriesId, 
			@PathVariable String type,
			@PathVariable int id,
			Model model) {
		
		MainEvent mainEvent = mainEventService.find(mainEventId);
		MainEventSeries mainEventSeries = mainEventSeriesService.find(eventSeriesId);
		mainEventSeries.setDefaultVisibleBoardNames();
		model.addAttribute("mainEvent", mainEvent);
		model.addAttribute("mainEventSeries", mainEventSeries);
		
		BoardType boardType = BoardType.BOARD_TYPES.get(type);
		Board board = boardService.find(id);
		model.addAttribute("type", boardType);
		model.addAttribute("board", board);
		
		if (board.getBoardId() > 0) {
			Board parentBoard = boardService.find(board.getBoardId());
			model.addAttribute("prevBoard", boardService.getPrevBoard(parentBoard.getId(), parentBoard.getBoardType()));
			model.addAttribute("nextBoard", boardService.getNextBoard(parentBoard.getId(), parentBoard.getBoardType()));
		} else {
			model.addAttribute("prevBoard", boardService.getPrevBoard(id, board.getBoardType()));
			model.addAttribute("nextBoard", boardService.getNextBoard(id, board.getBoardType()));
		}
		
		BreadCrumb crumb = new BreadCrumb(4,2);
		crumb.setMainEventId(mainEventId);
		model.addAttribute("crumb", crumb);
		
		return "events/mainEvents/boardShow.app";
	}
	
	/**
	 * 각 시리즈 차수의 Q&A 게시판 글작성 페이지 (일반 사용자)
	 * @param mainEventId
	 * @param eventSeriesId
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/{mainEventId}/series/{eventSeriesId}/mainEventSeriesQna/private/new", method=RequestMethod.GET)
	public String qnaBoardNew(@PathVariable int mainEventId,
			@PathVariable int eventSeriesId,
			HttpServletRequest request, 
			Model model) {
		
		Board board = new Board();
		board.setBoardType("mainEventSeriesQna");
		model.addAttribute("board", board);
		
		MainEvent mainEvent = mainEventService.find(mainEventId);
		MainEventSeries mainEventSeries = mainEventSeriesService.find(eventSeriesId);
		
		model.addAttribute("mainEvent", mainEvent);
		model.addAttribute("mainEventSeries", mainEventSeries);
		
		BoardType boardType = BoardType.BOARD_TYPES.get("mainEventSeriesQna");
		model.addAttribute("boardType", boardType);
		
		BreadCrumb crumb = new BreadCrumb(4,2);
		crumb.setMainEventId(mainEventId);
		model.addAttribute("crumb", crumb);
		
		return "events/mainEvents/qnaBoards/new.app";
	}
	
	/**
	 * 각 시리즈 차수의 Q&A 게시판 글작성 (일반 사용자)
	 * @param board
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/{mainEventId}/series/{eventSeriesId}/mainEventSeriesQna/private/create", method = RequestMethod.POST)
	public String qnaBoarCreate(@ModelAttribute("board") Board board, 
			@PathVariable int mainEventId,
			@PathVariable int eventSeriesId,
			Model model, HttpServletRequest request) {
		
		// set event series id for the board
		board.setEventSeriesId(eventSeriesId);
		
		logger.error(board.getBoardType());
		
		// 일반사용자가 작성한 경우이므로,
		// 일반 Member 번호를 넣어주고,
		String memberNo = getCurrentMemberNo(request);
		if( memberNo != null ) {
			board.setMemberNo(memberNo);
			board.setWrittenByAdmin(false);
		}
		// Member No가 비어있을 경우에는, 관리자로 가정한다.
		else {
			board.setWrittenByAdmin(true);
		}
		boardService.create(board);
		return "redirect:/mainEvents/" + mainEventId + "/series/" + eventSeriesId + "/mainEventSeriesQna";					
	}
	
	/**
	 * 각 시리즈 차수의 Q&A 게시판 글 수정 페이지 (일반 사용자)
	 * @param board
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/{mainEventId}/series/{eventSeriesId}/mainEventSeriesQna/private/{id}/edit", method = RequestMethod.GET)
	public String editQnaBoard(@PathVariable("mainEventId")int mainEventId,
			@PathVariable("eventSeriesId")int eventSeriesId,
			@PathVariable("id") int id,
			HttpServletRequest request, Model model) {
		
		Board board = boardService.find(id);
		model.addAttribute("board", board);

		BoardType boardType = BoardType.BOARD_TYPES.get("mainEventSeriesQna");
		model.addAttribute("boardType", boardType);
		
		MainEvent mainEvent = mainEventService.find(mainEventId);
		MainEventSeries mainEventSeries = mainEventSeriesService.find(eventSeriesId);
		
		model.addAttribute("mainEvent", mainEvent);
		model.addAttribute("mainEventSeries", mainEventSeries);
		
		BreadCrumb crumb = new BreadCrumb(4,2);
		crumb.setMainEventId(mainEventId);
		model.addAttribute("crumb", crumb);
		
		return "events/mainEvents/qnaBoards/edit.app";
		
	}
	
	/**
	 * 각 시리즈 차수의 Q&A 게시판 글 Update (일반 사용자)
	 * @param board
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/{mainEventId}/series/{eventSeriesId}/mainEventSeriesQna/private/update", method = RequestMethod.POST)
	public String updateOpinionsBoard(
			@ModelAttribute("board") Board board, 
			@PathVariable("mainEventId")int mainEventId,
			@PathVariable("eventSeriesId")int eventSeriesId,
			Model model, 
			HttpServletRequest request) {
		
		// 일반사용자가 작성한 경우이므로,
		// 일반 Member 번호를 넣어주고,
		String memberNo = getCurrentMemberNo(request);
		if( memberNo != null ) {
			board.setMemberNo(memberNo);
			board.setWrittenByAdmin(false);
		}
		// Member No가 비어있을 경우에는, 관리자로 가정한다.
		else {
			board.setWrittenByAdmin(true);
		}
		boardService.update(board);
		return "redirect:/mainEvents/" + mainEventId + "/series/" + eventSeriesId + "/mainEventSeriesQna";					
	}
	
	
	/**
	 * 각 시리즈 차수의 Q&A 게시판 글 Update (일반 사용자)
	 * @param model
	 * @param jsonStr
	 * @return
	 */
	@RequestMapping(value = "/{mainEventId}/series/{eventSeriesId}/mainEventSeriesQna/private/delete", method = RequestMethod.POST)
	public @ResponseBody Model deleteOpinionsBoard(Model model, @RequestBody String jsonStr){
		
		JSONObject jsonObj = new JSONObject(jsonStr);
		int boardId = jsonObj.getInt("id");
		boardService.delete(boardId);
		
		return model;
	}
	
	private String getCurrentMemberNo(HttpServletRequest request) {
		return (String)request.getSession().getAttribute(Member.CURRENT_MEMBER_NO);
	}
}
