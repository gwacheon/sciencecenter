package kr.go.sciencecenter.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 키오스크에서 호출
 * @author ckh
 *
 */
@Controller
@RequestMapping("/kiosk")
public class KioskController {

	private static final Logger logger = LoggerFactory.getLogger(KioskController.class);
	
	@RequestMapping(value = "/FileList", method = RequestMethod.GET)
	public void fileList(Model model, HttpServletRequest request, HttpServletResponse response) {
		
		logger.info("KioskController.FileList -- !");
		
		String DefaultPath = "/home/tktdev/client_down/";
		// for local 
		//String DefaultPath = "D:/src-dev2/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/work/Catalina/localhost/home/tktdev/client_down/";
		
		String FilePath = "";
		String ListData = "";
		
		String Path = "";
				
		try{
			if(request.getParameter("Path") == null){
				Path = "";
			}
			else{		
				Path = new String(request.getParameter("Path").getBytes("8859_1"), "UTF-8");
			}
			
			FilePath = DefaultPath + Path + "/";
			FilePath = FilePath.replaceAll("//", "/");
			
			/*
			System.out.println("Path : " + Path);
			System.out.println("FilePath : " + FilePath);
			*/
			
			File file = new File(FilePath);
			File[] FileList = file.listFiles();
			
			SimpleDateFormat Formatter = new SimpleDateFormat("yyyyMMddHHmmss", Locale.KOREA);
			
			for(int i = 0; i < FileList.length; i++){
				if(i == 0){
					ListData = "|";
				}
				
				// |File Name^File Type^File Size^File Modify DateTime|
				ListData += FileList[i].getName() + "^" + (FileList[i].isDirectory()?"D":"F") + "^" + FileList[i].length() + "^" + Formatter.format(new Date(FileList[i].lastModified())) + "|";
			}
			
			//System.out.println("ListData : " + ListData);
			response.getOutputStream().println(ListData);        
		}catch(Exception e){
			logger.info("KioskController.FileList, " + e.getMessage());
		}
	}

	@RequestMapping(value = "/FileManager", method = {RequestMethod.HEAD, RequestMethod.GET})
	public void fileManager(Model model, HttpServletRequest request, HttpServletResponse response) {
		
		logger.info("KioskController.FileManager -- !");	
		
		String DefaultPath = "/home/tktdev/client_down/";
		//for local 
		//String DefaultPath = "D:/src-dev2/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/work/Catalina/localhost/home/tktdev/client_down/";
		
		String FilePath = "";		
		String Path = "";
		String FileName = "";
		
		
		try {
			if(request.getParameter("Path") == null){
				Path = "";
			}
			else{			
				Path = new String(request.getParameter("Path").getBytes("8859_1"), "UTF-8");
			}
			
			if(request.getParameter("FileName") == null){
				FileName = "";
			}
			else{			
				FileName = new String(request.getParameter("FileName").getBytes("8859_1"), "UTF-8");
			}
			
			FilePath = DefaultPath + Path + "/" + FileName;
			FilePath = FilePath.replaceAll("//", "/");
			
			String strClient  = request.getHeader("User-Agent");
			if(strClient.indexOf("MSIE 5.5") != -1) {
				response.setHeader("Content-Disposition", "filename=" + FileName);
				response.setContentType("doesn/matter;");
			}
			else{
				response.setHeader("Content-Disposition", "attachment;filename=" + FileName);
				response.setContentType("application/octet-stream;");
			}    	
			response.setHeader("Content-Transfer-Encoding", "binary");

			File file = new File(FilePath);
	        response.setContentLength((int) file.length());
	        
	        if(file.isFile()){
	        	byte buff[] = new byte[1024];
	        	BufferedInputStream fin = new BufferedInputStream(new FileInputStream(file));
				OutputStream outs = response.getOutputStream();		        
		        BufferedOutputStream fout = new BufferedOutputStream(outs);
				
				int read = 0;
				try{
					while ((read = fin.read(buff)) != -1){
						fout.write(buff, 0, read);
						fout.flush();
						//System.out.println("KioskController.FileManager> " + read);
					}
					fin.close();
					fout.close();
				}catch (Exception e) {
					logger.info("KioskController.FileManager, " + e.getMessage());
				}finally {
					if(fin != null) fin.close();
				}
	        }
		} catch (FileNotFoundException e) {
			logger.info("KioskController.FileManager, " + e.getMessage());
		} catch (Exception e){
			logger.info("KioskController.FileManager, " + e.getMessage());
		} 
	}

}
