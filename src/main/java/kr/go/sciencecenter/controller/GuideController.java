package kr.go.sciencecenter.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import kr.go.sciencecenter.model.BreadCrumb;
import kr.go.sciencecenter.service.BoardService;
import kr.go.sciencecenter.service.SangSangService;

/**
 * 전시관 카테고리 Controller
 * @author Prompt Technology
 *
 */
@Controller
@RequestMapping("/guide")
public class GuideController {
	@Autowired
	SangSangService sangSangService;
	
	@Autowired
	BoardService boardService;

	/**
	 * 이용안내 페이지
	 * @param model
	 * @return
	 */
    @RequestMapping(value="/totalGuide", method=RequestMethod.GET)
    public String totalDisplay(Model model,
    		@RequestParam(value="scrollspyName", required=false)String scrollspyName){
        if( scrollspyName != null ) {
        	model.addAttribute("scrollspyName", scrollspyName);
        }
        
        model.addAttribute("crumb", new BreadCrumb(1,1));
    	return "guide/totalGuide.app";
    }
    
    /**
	 * 전시해설 페이지
	 * @param model
	 * @return
	 */
    @RequestMapping(value="/displayExplanation", method=RequestMethod.GET)
    public String displayExplanation(Model model,
    		@RequestParam(value="scrollspyName", required=false)String scrollspyName){
        if( scrollspyName != null ) {
        	model.addAttribute("scrollspyName", scrollspyName);
        }
        model.addAttribute("crumb", new BreadCrumb(1,2));
    	return "guide/displayExplanation.app";
    }
    
    /**
	 * 전시관체험 - 체험전시물
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/displayExperience", method=RequestMethod.GET)
	public String experienceIndex(Model model,
			@RequestParam(value="scrollspyName", required=false)String scrollspyName){
		if( scrollspyName != null ) {
        	model.addAttribute("scrollspyName", scrollspyName);
        }
		model.addAttribute("crumb", new BreadCrumb(1,3));
		return "guide/displayExperience.app";
	}
    
    /**
     * 후원회원 페이지
     * @param model
     * @return
     */
    @RequestMapping(value = "/fundMember", method = RequestMethod.GET)
	public String fundMember(Model model) {
    	model.addAttribute("crumb", new BreadCrumb(1,4,1));
		return "guide/fundMember.app";
	}
    
    /**
     * 연간회원 페이지
     * @param model
     * @return
     */
    @RequestMapping(value = "/paidMember", method = RequestMethod.GET)
	public String paidMember(Model model,
			@RequestParam(value="scrollspyName", required=false)String scrollspyName) {
    	if( scrollspyName != null ) {
        	model.addAttribute("scrollspyName", scrollspyName);
        }
    	model.addAttribute("crumb", new BreadCrumb(1,4,2));
		return "guide/paidMember.app";
	}
    
    /**
     * 편의시설 페이지
     * @param type
     * @param model
     * @return
     */
    @RequestMapping(value="/convenience", method=RequestMethod.GET)
    public String convenience(
    		@RequestParam(value="type",required=false) String type,
    		Model model){
        
    	if(type == null){
			type="playroom";
		}
		
		model.addAttribute("type", type);
		model.addAttribute("crumb", new BreadCrumb(1,5,2));
    	return "guide/convenience.app";
    }
    
    /**
     * 식음시설 페이지
     * @param type
     * @param model
     * @return
     */
    @RequestMapping(value="/food", method=RequestMethod.GET)
    public String food(
    		@RequestParam(value="type",required=false) String type,
    		Model model){
    	
    	if(type == null) {
			type = "foodCourt";
		}
		
		model.addAttribute("type", type);
		model.addAttribute("crumb", new BreadCrumb(1,5,3));
        return "guide/food.app";
    }
  
    /**
	 * 추천관람코스
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/recommendCourse", method=RequestMethod.GET)
	public String recommendCourse(Model model){
		model.addAttribute("crumb", new BreadCrumb(1,6));
		return "display/recommendCourse.app";
	}
    
} 
