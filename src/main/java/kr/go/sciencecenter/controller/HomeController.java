package kr.go.sciencecenter.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.go.sciencecenter.mailer.UserMailer;
import kr.go.sciencecenter.model.Board;
import kr.go.sciencecenter.model.BoardType;
import kr.go.sciencecenter.model.ContactUsEmail;
import kr.go.sciencecenter.model.MainPageBanner;
import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.service.BoardService;
import kr.go.sciencecenter.service.EventsService;
import kr.go.sciencecenter.service.MainPageService;
import kr.go.sciencecenter.service.SmsEmailService;
import kr.go.sciencecenter.service.UserService;

@Controller
public class HomeController {
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Autowired
	private BoardService boardService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	MainPageService mainPageService;
	
	//@Autowired
	//UserMailer userMailer;
	
	@Autowired
	EventsService eventsService;
	
	@Autowired
	SmsEmailService smsEmailService;
	
	/**
	 * 메인 화면 Request 
	 * @param locale
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/", method = RequestMethod.GET)
	public String home(Locale locale, Model model, HttpServletRequest request) {
		
		// 문화행사 & 문화공연
		List<String> typeList = new ArrayList<String>();
		typeList.add("culture");
		typeList.add("play");
		model.addAttribute("events", eventsService.listByMultipleTypes(typeList, true, new Page(1, 6, 1)));
		// 현장스케치, 과학관영상
		Board.getMainBoards(model, boardService);
		
		//model.addAttribute("mainPageBanners", );
		List<MainPageBanner> bannerTotalList = mainPageService.listBannersByType(null, true);
		HashMap<String, List<MainPageBanner>> bannerHash = new HashMap<String, List<MainPageBanner>>();
		
		for(MainPageBanner banner : bannerTotalList){
			if(banner.getBannerType().contains("iconBanner")){
				banner.setBannerType("iconBanners");
			}else if(banner.getBannerType().contains("footerBanner")){
				banner.setBannerType("footerBanners");
			}else if(banner.getBannerType().contains("eventBanner")){
				banner.setBannerType("eventBanners");
			}
			List<MainPageBanner> valueList = bannerHash.get(banner.getBannerType());
			if (valueList == null) {
				valueList = new ArrayList<MainPageBanner>();
				valueList.add(banner);
				bannerHash.put(banner.getBannerType(), valueList);
			}else{
				bannerHash.get(banner.getBannerType()).add(banner);
			}
		}
		
		for(String key : bannerHash.keySet()){
			model.addAttribute(key, bannerHash.get(key));
		}		
		model.addAttribute("mainEducationMenuFlag", true);
		
		return "index.app";
	}
		
	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public String accessDenied(){
		return "403.app";
	}
	
	@RequestMapping(value = "/pdfViews", method = RequestMethod.GET)
	public ModelAndView pdfView(){
		
		return new ModelAndView("PdfView","userList",userService.list());
	}
	
	/**
	 * 상단 검색 기능
	 */
	@RequestMapping(value = "/totalSearch", method = RequestMethod.GET)
	public String totalSearch(Model model,
			@RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			@RequestParam(value = "searchKey", required=true) String searchKey) {
		
		String requestURI = boardService.getHttpServletRequest().getRequestURI();
		String path = requestURI + "?searchKey=" + searchKey + "&";
		int totalCount = boardService.countByTotalSearch(searchKey);
		Page page = new Page(currentPage, 10, totalCount);
		page.setPath(path);
		model.addAttribute("page", page);
		
		model.addAttribute("searchKey", searchKey);
		model.addAttribute("totalCount", totalCount);
		
		List<Board> boards = boardService.listByTotalSearch(page, searchKey);
		Map<Board, BoardType> searchResults = new LinkedHashMap<Board, BoardType>();
		for( Board board : boards) {
			BoardType boardType = BoardType.BOARD_TYPES.get(board.getBoardType());
			searchResults.put(board, boardType);
		}
		
		model.addAttribute("searchResults", searchResults);
		return "totalSearch/index.app";
	}
	
	/**
	 * Footer 이메일 문의 (미사용)
	 * @param model
	 * @param currentPage
	 * @param searchKey
	 * @return
	 */
	@RequestMapping(value = "/contactUsEmail", method = RequestMethod.POST)
	public @ResponseBody Model contactUsEmail(@RequestBody String jsonStr,
			HttpServletRequest request, Model model){
		
//		String message = "";
//		
//		ContactUsEmail contactUsEmail = new ContactUsEmail();
//		
//		JSONObject jsonObj = new JSONObject(jsonStr);
//		JSONObject jsonEmail = jsonObj.getJSONObject("contactUsEmail");
//		
//		contactUsEmail.setName(jsonEmail.getString("name"));
//		contactUsEmail.setEmail(jsonEmail.getString("email"));
//		contactUsEmail.setTitle(jsonEmail.getString("title"));
//		contactUsEmail.setContent(jsonEmail.getString("content"));
//		
//		//userMailer.sendEmailToAdmin(contactUsEmail);
//		Map map = new HashMap();
//		map.put("", "");
//		
//		//smsEmailService.setSmsEmail(map);
//		
//		message = "이메일이 정상적으로 발송되었습니다.";
//		model.addAttribute("message", message);
//		
		return model;
	}
	
	

}