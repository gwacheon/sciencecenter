package kr.go.sciencecenter.controller;

import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.base.Joiner;

import kr.co.mainticket.Entity.Entity;
import kr.co.mainticket.Utility.Box;
import kr.co.mainticket.Utility.DateTime;
import kr.co.mainticket.Utility.HttpUtility;
import kr.co.mainticket.Utility.Utility;
import kr.go.sciencecenter.api.LectureApi;
import kr.go.sciencecenter.model.BreadCrumb;
import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.model.Program;
import kr.go.sciencecenter.model.ProgramType;
import kr.go.sciencecenter.model.api.LectureDetail;
import kr.go.sciencecenter.model.api.Member;
import kr.go.sciencecenter.model.api.ReqLecture;
import kr.go.sciencecenter.persistence.PaymentCheckMapper;
import kr.go.sciencecenter.service.FamilyModifyService;
import kr.go.sciencecenter.service.LectureService;
import kr.go.sciencecenter.service.PaymentCheckService;
import kr.go.sciencecenter.service.ProgramService;
import kr.go.sciencecenter.service.SmsEmailService;
import kr.go.sciencecenter.service.UserService;
import kr.go.sciencecenter.util.PropertyHelper;

@Controller
@RequestMapping("/schedules")
public class ScheduleController {
	private static final Logger logger = LoggerFactory.getLogger(ScheduleController.class);

	@Autowired
	ProgramService programService;
	@Autowired
	LectureService lectureService;
	@Autowired
	PaymentCheckService paymentCheckService;
	@Autowired
	PaymentCheckMapper paymentCheckMapper;
	@Autowired
	FamilyModifyService FamilyModifyService;
	@Autowired
	UserService userService;
	@Autowired
	SmsEmailService smsEmailService;
	@Autowired
	PropertyHelper propertyHelper;

	@ModelAttribute
	public void setCategories(Model model) {
		model.addAttribute("crumb", new BreadCrumb(5));
	}

	@RequestMapping(value = "")
	public String index(Model model, 
			@RequestParam(value = "programType", required = false) String programType,
			@RequestParam(value = "searchKey", required = false) String searchKey, 
			@RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			HttpServletRequest request)
			throws Exception {

		logger.info("ScheduleController.index -- !" + HttpUtility.getBox(request).toString());

		LectureApi lectureApi = new LectureApi();
		ReqLecture reqLecture = new ReqLecture();

		// 상단 메뉴는 ACADEMY_CD 이고 페이지 중앙의 탭은 CLASS_CD 값
		// 페이지 초기시 아무 값이 없을때에는 코드가 고정 값이므로 밖아둔다.
		String AcademyCd = Utility.CheckNull(request.getParameter("ACADEMY_CD"));
		String ClassCd = Utility.CheckNull(request.getParameter("CLASS_CD"));

		if (AcademyCd.equals(""))
			AcademyCd = "ACD005";

		if (ClassCd.equals("")) {
			if (AcademyCd.equals("ACD001"))
				ClassCd = "CL6006";
			if (AcademyCd.equals("ACD004"))
				ClassCd = "CL4001";
			if (AcademyCd.equals("ACD005"))
				ClassCd = "CL5001";
			/*if (AcademyCd.equals("ACD006"))
				ClassCd = "CL6001";*/
			if (AcademyCd.equals("ACD003"))
				ClassCd = "CL3001";
			if (AcademyCd.equals("ACD006"))
				ClassCd = "CL6003";
			if (AcademyCd.equals("ACD007"))
				ClassCd = "CL7001";
			if (AcademyCd.equals("ACD008"))
				ClassCd = "CL8001";
			if (AcademyCd.equals("ACD009"))
				ClassCd = "CL9001";
		}

		reqLecture.setAcademyCd(AcademyCd);
		reqLecture.setClassCd(ClassCd);

		String fee = request.getParameter("fee");
		String age = request.getParameter("age");
		String search = request.getParameter("search");
		String year = request.getParameter("year");
		String month = request.getParameter("month");
		int itemsPerPage = Integer.parseInt(request.getParameter("itemsPerPage") == null ? "50" : request.getParameter("itemsPerPage"));
//		String startNum = request.getParameter("startNum");
//		String endNum = request.getParameter("endNum");
		
		// Promptech 추가.
		// Pagination 을 위해 프로그램들의 총 개수를 가져와야 하므로, endNum을 가능한한 최대로 잡아, 총 개수를 센다.
		
		reqLecture.setSearchParams(fee, age, search, year, month, "1", "50");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("yearMm", reqLecture.getYyyymm());
		params.put("search", reqLecture.getSearch());
		params.put("startNum", reqLecture.getStartNum());
		params.put("endNum", reqLecture.getEndNum());
		params.put("companyCd", propertyHelper.getCompanycd());
		params.put("academyCd", AcademyCd);
		params.put("classCd", ClassCd);
		int lectureTotalCount = lectureService.getLectureList(params).size();
		model.addAttribute("lectureTotalCount", lectureTotalCount);
		// 페이지번호, 페이지당 프로그램수, 총 프로그램수 를 통해 page 객체 생성.
		Page page = new Page(currentPage, itemsPerPage, lectureTotalCount);
		// pagination 경로 설정 - parameter setting
		String requestURI = request.getRequestURI();
		String path = requestURI + "?";
		List<String> pageParameters = new ArrayList<String>();
		pageParameters.add("ACADEMY_CD=" + AcademyCd);
		pageParameters.add("CLASS_CD=" + ClassCd);
		if( fee != null ) {
			pageParameters.add("fee=" + fee);
		}
		if( age != null ) {
			pageParameters.add("age=" + age != null ? age : "");			
		}
		if( search != null ) {
			pageParameters.add("search=" + search != null ? search : "");			
		}
		if( month != null ) {
			pageParameters.add("year=" + year != null ? year : "");			
		}
		if( month != null ) {
			pageParameters.add("month=" + month != null ? month : "");			
		}
		pageParameters.add("itemsPerPage=" + itemsPerPage);
		page.setPath(path + Joiner.on("&").join(pageParameters) + "&");
		model.addAttribute("page", page);
		
		// 생성된 page 객체를 통해, startNum 과 endNum 계산.
		String startNum = String.valueOf(page.getOffset()+1);
		String endNum = String.valueOf(page.getLimit());
		// 계산된 startNum, endNum 을 param 에 다시 setting.
		params.put("startNum", startNum);
		params.put("endNum", endNum);
		

		reqLecture.setSearchParams(fee, age, search, year, month, startNum, endNum);
		model.addAttribute("reqLecture", reqLecture);

		/*list api 사용안함*/
		//Lecture lecture = lectureApi.getList(reqLecture);
		//model.addAttribute("lecture", lecture);
		
		
		List lecture = lectureService.getLectureList(params);
		int lectureCount = lectureService.getLectureListCount(params);
		
		model.addAttribute("lecture", lecture);
		model.addAttribute("lectureCount", lectureCount);
		
		model.addAttribute("programTypes", ProgramType.PROGRAM_TYPE_MAP.get("normal"));

		if (programType == null) {
			model.addAttribute("programType", ProgramType.PROGRAM_TYPE_MAP.get("normal").get(0));
		}

		model.addAttribute("selectedDate", new Date());
		model.addAttribute("AcamedyList", lectureService.getAcademyList());
		model.addAttribute("ClassList", lectureService.getClassList(params));
		model.addAttribute("AcademyCd", AcademyCd);
		model.addAttribute("ClassCd", ClassCd);

		List<Program> programs = programService.searchByDate(new Date());
		model.addAttribute("programs", programs);

		List ClassList = lectureService.getAcademyClassList(params);
		model.addAttribute("AcademyClassList", ClassList);
		model.addAttribute("AcademyClassSize", ClassList.size());

		model.addAttribute("forOpen", request.getParameter("forOpen"));

		return "schedules/index.app";
	}

	@RequestMapping(value = "/voluntary")
	public String voluntary(Model model) throws Exception {
		model.addAttribute("AcamedyList", lectureService.getAcademyList());

		return "schedules/voluntary.app";
	}

	@RequestMapping(value = "/{courseCd}")
	public String showLecture(@PathVariable("courseCd") String courseCd, Model model, HttpServletRequest request)
			throws Exception {

		LectureApi lectureApi = new LectureApi();
		ReqLecture reqLecture = new ReqLecture(courseCd);
		Map<String, Object> params = new HashMap<String, Object>();
		LectureDetail detail = lectureApi.getDetail(reqLecture);

		String AcademyCd = Utility.CheckNull(detail.getAcademyCd());
		String ClassCd = Utility.CheckNull(detail.getClassCd());
		String CourseCd = Utility.CheckNull(detail.getCourseCd());
		String SemesterCd = Utility.CheckNull(request.getParameter("SEMESTER_CD"));
		String flag = Utility.CheckNull(request.getParameter("FLAG"));
		String type = Utility.CheckNull(request.getParameter("TYPE"));
		String ownerLimitFlag = Utility.CheckNull(request.getParameter("OWNER_LIMIT_FLAG"));

		if (AcademyCd.equals(""))
			AcademyCd = "ACD004";

		if (ClassCd.equals("")) {
			if (AcademyCd.equals("ACD001"))
				ClassCd = "CL6006";
			if (AcademyCd.equals("ACD004"))
				ClassCd = "CL4001";
			if (AcademyCd.equals("ACD005"))
				ClassCd = "CL5001";
			if (AcademyCd.equals("ACD006"))
				ClassCd = "CL6001";
			if (AcademyCd.equals("ACD007"))
				ClassCd = "CL7001";
			if (AcademyCd.equals("ACD008"))
				ClassCd = "CL8001";
			if (AcademyCd.equals("ACD009"))
				ClassCd = "CL9001";
		}

		params.put("companyCd", propertyHelper.getCompanycd());
		params.put("academyCd", AcademyCd);
		params.put("classCd", ClassCd);

		List ClassList = lectureService.getAcademyClassList(params);

		model.addAttribute("AcademyClassList", ClassList);
		model.addAttribute("AcademyClassSize", ClassList.size());
		model.addAttribute("AcademyCd", AcademyCd);
		model.addAttribute("ClassCd", ClassCd);
		model.addAttribute("CourseCd", CourseCd);
		model.addAttribute("SemesterCd", SemesterCd);
		model.addAttribute("type", type);
		model.addAttribute("flag", flag);
		model.addAttribute("ownerLimitFlag",ownerLimitFlag);

		model.addAttribute("AcamedyList", lectureService.getAcademyList());
		model.addAttribute("detail", detail);

		return "schedules/detail.app";
	}

	/**
	 * 리스트 호출에 사용되나 지워짐. - index로 통합 ckh 수정 클래스 리스트
	 * 
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/AcademyChange")
	@Deprecated
	public String AcademyChange(Model model, HttpServletRequest request) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		Map<String, Object> ClassMap = new HashMap<String, Object>();

		model.addAttribute("programTypes", ProgramType.PROGRAM_TYPE_MAP.get("normal"));

		String AcademyCd = Utility.CheckNull(request.getParameter("ACADEMY_CD"));

		params.put("companyCd", propertyHelper.getCompanycd());
		params.put("academyCd", AcademyCd);

		List ClassList = lectureService.getAcademyClassList(params);
		ClassMap = (Map) ClassList.get(0);

		params.put("classCd", (String) ClassMap.get("CLASS_CD"));

		model.addAttribute("selectedDate", new Date());
		model.addAttribute("AcamedyList", lectureService.getAcademyList());
		model.addAttribute("AcademyCd", AcademyCd);
		model.addAttribute("ClassList", lectureService.getClassList(params));
		model.addAttribute("AcademyClassList", ClassList);

		List<Program> programs = programService.searchByDate(new Date());
		model.addAttribute("programs", programs);

		return "schedules/index.app";
	}

	/**
	 * 웹 / 모바일 구분
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private boolean isMobile(HttpServletRequest request) throws Exception {
		String userAgent = request.getHeader("user-agent");
		boolean mobile1 = userAgent.matches(
				".*(iPhone|iPod|Android|Windows CE|BlackBerry|Symbian|Windows Phone|webOS|Opera Mini|Opera Mobi|POLARIS|IEMobile|lgtelecom|nokia|SonyEricsson).*");
		boolean mobile2 = userAgent.matches(".*(LG|SAMSUNG|Samsung).*");
		if (mobile1 || mobile2) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * ckh 수정 예매 날짜/회차,param courseCd,companyCd,scheduleFlag 필요
	 * 
	 * @param id
	 * @param modelr
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/Show")
	public String show(Model model, HttpServletRequest request) throws Exception {
		/*
		 * Program program = programService.find(id);
		 * model.addAttribute("program", program);
		 */
		logger.info("ScheduleController.show -- ! " + HttpUtility.getBox(request).toString());

		String AcademyCd = Utility.CheckNull(request.getParameter("ACADEMY_CD"));
		String ClassCd = Utility.CheckNull(request.getParameter("CLASS_CD"));

		// if(AcademyCd.equals("")) AcademyCd = "ACD005";
		// if(ClassCd.equals("")) ClassCd = "CL5001";

		Box params = HttpUtility.getBox(request);

		Date now = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy");
		String YEAR = format.format(now);
		String saleType = "";
		String Flag = params.getString("FLAG"); // 창의체험 :N , 그외 :Y
		String CourseCd = params.getString("COURSE_CD");
		String SemesterCd = params.getString("SEMESTER_CD");
		String Type = params.getString("TYPE");
		String ownerLimitFlag = params.getString("OWNER_LIMIT_FLAG");

		if (isMobile(request))
			saleType = "000005";
		else
			saleType = "000002";

		// if(Flag.equals("N")){
		Map<String, Object> param = new HashMap<String, Object>();

		param.put("companyCd", propertyHelper.getCompanycd());
		param.put("courseCd", CourseCd);
		param.put("semesterCd", SemesterCd);

		List LectureList = lectureService.getPeriodList(param);

		model.addAttribute("LectureList", LectureList);
		// }

		model.addAttribute("YEAR", YEAR);
		model.addAttribute("SALE_TYPE", saleType);
		model.addAttribute("COMPANY_CD", propertyHelper.getCompanycd());
		model.addAttribute("COURSE_CD", CourseCd);
		model.addAttribute("SEMESTER_CD", SemesterCd);
		model.addAttribute("MEMBER_NO", getCurrentMemberNo(request));
		model.addAttribute("SCHEDULE_FLAG", Flag);
		model.addAttribute("APPLY_TYPE", Type);
		model.addAttribute("OWNER_LIMIT_FLAG", ownerLimitFlag);

		model.addAttribute("AcademyCd", AcademyCd);
		model.addAttribute("ClassCd", ClassCd);

		Map map = new HashMap();
		map.put("companyCd", propertyHelper.getCompanycd());
		map.put("academyCd", AcademyCd);
		map.put("classCd", ClassCd);

		model.addAttribute("AcamedyList", lectureService.getAcademyList());
		List ClassList = lectureService.getAcademyClassList(map);
		model.addAttribute("AcademyClassList", ClassList);
		model.addAttribute("AcademyClassSize", ClassList.size());

		model.addAttribute("pay_mbrInfo", lectureService.getMbrInfo(params.getString("CLASS_CD")));
		model.addAttribute("pay_returnType", propertyHelper.getPay_returnType());
		model.addAttribute("pay_version", propertyHelper.getPay_version());
		model.addAttribute("pay_targetUrl", propertyHelper.getPay_targetUrl());
		model.addAttribute("pay_authType", propertyHelper.getPay_authType());
		model.addAttribute("pay_server", propertyHelper.getPay_server());
		model.addAttribute("c2_homepage_url", propertyHelper.getC2_homepage_url());

		Map m = new HashMap();
		m.put("COMPANY_CD", propertyHelper.getCompanycd());
		m.put("MEMBER_NO", getCurrentMemberNo(request));
		Map rm = (Map) userService.getLegacyUser(m);
		model.addAttribute("MEMBER_NAME", rm.get("MEMBER_NAME"));
		String member_cel = "";
		if (rm.get("MEMBER_CEL") != null)
			member_cel = rm.get("MEMBER_CEL").toString().replaceAll("-", "");
		model.addAttribute("MEMBER_CEL", member_cel);
		model.addAttribute("MEMBER_EMAIL", rm.get("MEMBER_EMAIL"));
		model.addAttribute("MEMBERSHIP_TYPE", rm.get("MEMBERSHIP_TYPE"));
		model.addAttribute("MEMBERSHIP_TYPE_CNT", rm.get("MEMBERSHIP_TYPE_CNT"));
		model.addAttribute("MEMBERSHIP_PAY_CNT", rm.get("MEMBERSHIP_PAY_CNT"));
		
		return "schedules/show.app";
	}

	/**
	 * 날짜 가져오기
	 * 
	 * @param model
	 * @param request
	 * @return
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/dayList")
	public @ResponseBody Model dayList(Model model, HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		logger.info("ScheduleController.dayList -- ! " + HttpUtility.getBox(request).toString());

		Map<String, Object> param = new HashMap<String, Object>();

		param.put("companyCd", propertyHelper.getCompanycd());
		param.put("courseCd", request.getParameter("COURSE_CD"));
		param.put("semesterCd", request.getParameter("SEMESTER_CD"));
		param.put("startDay", request.getParameter("DAY"));

		List LectureList = lectureService.getPriceDayList(param);

		model.addAttribute("LectureList", LectureList);

		return model;
	}

	/**
	 * 회차 가져오기
	 * 
	 * @param model
	 * @param request
	 * @return
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/priceList")
	public @ResponseBody Model priceList(Model model, HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		logger.info("ScheduleController.priceList -- ! " + HttpUtility.getBox(request).toString());

		Map<String, Object> param = new HashMap<String, Object>();

		param.put("companyCd", propertyHelper.getCompanycd());
		param.put("courseCd", request.getParameter("COURSE_CD"));
		param.put("semesterCd", request.getParameter("SEMESTER_CD"));
		param.put("startDay", request.getParameter("DAY"));

		List LectureList = lectureService.getPriceList(param);

		model.addAttribute("LectureList", LectureList);

		return model;
	}

	/**
	 * 예약하기
	 * 
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/Reser")
	public @ResponseBody Model reser(Model model, HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		logger.info("ScheduleController.reser -- ! " + HttpUtility.getBox(request).toString());

		Box params = HttpUtility.getBox(request);

		String CompanyCd = Utility.CheckNull(request.getParameter("COMPANY_CD"));
		String SaleType = Utility.CheckNull(request.getParameter("SALE_TYPE"));
		String WinType = "000000";
		String CourseCd = Utility.CheckNull(request.getParameter("COURSE_CD"));
		String MemberNo = Utility.CheckNull(request.getParameter("MEMBER_NO"));
		String ApplyType = Utility.CheckNull(request.getParameter("APPLY_TYPE"));
		String PriceNbr = Utility.CheckNull(request.getParameter("PRICE_NBR"));
		// String PeopleCnt =
		// Utility.CheckNull(request.getParameter("PEOPLE_CNT"));

		Entity ApplyEntity = new Entity(20);
		// if(PeopleCnt.equals("")) PeopleCnt = "0";

		ApplyEntity.setField("PROCESS_TYPE", "APPLY");
		ApplyEntity.setField("SALE_TYPE", SaleType);
		ApplyEntity.setField("WIN_TYPE", WinType);
		ApplyEntity.setField("SMS_SEND_FLAG", "N");
		ApplyEntity.setField("EMAIL_SEND_FLAG", "N");
		ApplyEntity.setField("COURSE_CD", CourseCd);
		ApplyEntity.setField("PRICE_NBR", PriceNbr);
		ApplyEntity.setField("MEMBER_COMPANY_CD", CompanyCd);
		ApplyEntity.setField("MEMBER_NO", MemberNo);
		ApplyEntity.setField("APPLY_TYPE", ApplyType);
		ApplyEntity.setField("SWITCH_FLAG", "N");
		ApplyEntity.setField("COERCION_FLAG", "N");
		ApplyEntity.setField("WORKER_MEMBER_NO", MemberNo);
		ApplyEntity.setField("CLIENT_IP", request.getRemoteAddr());
		// ApplyEntity.setField("PEOPLE_CNT", PeopleCnt);

		Entity StudentList = new Entity(5);
		Entity StudentItem = new Entity(5);

		Entity PriceDetailList = new Entity(5);
		Entity PriceDetailItem = null;

		int PriceDetailCnt = Utility.StringToInt(params.getString("PRICE_CNT"));

		for (int idx = 0; idx < PriceDetailCnt; idx++) {
			PriceDetailItem = new Entity(5);

			String cnt = !params.getString("PEOPLE_CNT_" + idx).equals("") ? params.getString("PEOPLE_CNT_" + idx)
					: "1";

			PriceDetailItem.setField("APPLY_CNT", cnt);
			PriceDetailItem.setField("PRICE_DETAIL_NBR", params.getString("PRICE_DETAIL_NBR_" + idx));

			PriceDetailList.addField("PRICE_DETAIL_ITEM", PriceDetailItem);
		}

		StudentItem.setField("PRICE_DETAIL_LIST", PriceDetailList);

		Entity TargetList = new Entity(5);
		Entity TargetItem = null;

		for (int targetIdx = 0; targetIdx < PriceDetailCnt; targetIdx++) {
			TargetItem = new Entity(5);

			TargetItem.setField("TARGET_CD", params.getString("TARGET_CD_" + targetIdx));

			TargetList.addField("TARGET_ITEM", TargetItem);
		}

		StudentItem.setField("TARGET_LIST", TargetList);

		if (PriceDetailCnt > 0) {
			Entity StudentPriceList = new Entity(5);
			Entity StudentPrice = null;
			for (int idx = 0; idx < PriceDetailCnt; idx++) {
				StudentPrice = new Entity(5);

				int cnt = Integer.parseInt(!params.getString("PEOPLE_CNT_" + idx).equals("")
						? params.getString("PEOPLE_CNT_" + idx) : "1");

				StudentPrice.setField("STUDENT_NO", params.getString("STUDENT_" + idx));
				StudentPrice.setField("PRICE", params.getString("PRICE_" + idx));
				StudentPrice.setField("APPLY_CNT", String.valueOf(cnt));

				// 봉사활동시 옵션정보
				Entity OptionList = new Entity(5);

				String optionStr = params.getString("OPTION_LIST_" + idx);
				String[] optionArry = Utility.CheckNull(URLDecoder.decode(optionStr, "UTF-8")).split("\\^");

				if (optionArry.length > 3) {
					for (int i = 0; i < 4; i++) {
						Entity OptionItem = new Entity(2);
						OptionItem.setField("OPTION_CD_" + i, "OPD00" + (i + 1));
						OptionItem.setField("OPTION_DATA_" + i, optionArry[i]);
						OptionList.addField("OPTION_ITEM", OptionItem);
					}
				}

				StudentPrice.setField("OPTION_LIST", OptionList);
				// 봉사활동시 옵션정보

				StudentPriceList.addField("STUDENTPRICE_ITEM", StudentPrice);

			}
			StudentItem.setField("STUDENTPRICE_LIST", StudentPriceList);
		}

		StudentList.addField("STUDENT_ITEM", StudentItem);
		ApplyEntity.setField("STUDENT_LIST", StudentList);

		String RspCd = "";
		String RspMsg = "";
		String ApplyUniqueNbr = "";

		Map<String, Object> result = paymentCheckService.CourseApply(CompanyCd, ApplyEntity);

		if (String.valueOf(result.get("result")).equals("true")) {
			int InfoCnt = String.valueOf(result.get("APPLY_UNIQUE_NBR")).length();

			if (InfoCnt > 0) {
				RspCd = String.valueOf(result.get("RSP_CD"));
				RspMsg = (String) result.get("RSP_MSG");
				ApplyUniqueNbr = String.valueOf(result.get("APPLY_UNIQUE_NBR"));

				model.addAttribute("RspMsg", RspMsg);
				model.addAttribute("rs", RspCd);
				model.addAttribute("ApplyUniqueNbr", ApplyUniqueNbr);
				model.addAttribute("TotalPrice", String.valueOf(result.get("TOTAL_PRICE")));
				model.addAttribute("ApplyCnt", String.valueOf(result.get("APPLY_CNT")));
				model.addAttribute("CoureseName", String.valueOf(result.get("COURSE_NAME")));
			} else {
				RspCd = "E999";
				model.addAttribute("RspMsg", (String) result.get("RSP_MSG"));
				model.addAttribute("rs", RspCd);
			}
		} else {
			model.addAttribute("RspMsg", (String) result.get("Msg"));
			model.addAttribute("rs", RspCd);
		}

		return model;
	}

	/**
	 * 완료 페이지
	 * 
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/CompleteProc")
	public String CompleteProc(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {

		logger.info("ScheduleController.CompleteProc -- ! " + HttpUtility.getBox(request).toString());

		Box params = HttpUtility.getBox(request);
		Map<String, Object> param = new HashMap<String, Object>();

		param.put("COMPANY_CD", propertyHelper.getCompanycd());
		param.put("APPLY_UNIQUE_NBR", params.getString("APPLY_UNIQUE_NBR"));

		List list = lectureService.getCompleteInfo(param);
		List payList = paymentCheckMapper.SELECT_TRAN_PAYMENT(param);

		if (!list.isEmpty()) {
			model.addAttribute("CompleteInfo", list.get(0));
		}
		if (!payList.isEmpty()) {
			model.addAttribute("PaymentInfo", payList.get(0));
		}

		model.addAttribute("ApplyUniqueNbr", params.getString("APPLY_UNIQUE_NBR"));

		// 예약신청(L00005)
		Map smsMap = new HashMap();
		smsMap.put("CONTENTS_TYPE", "L00005");
		smsMap.put("ORDNO", params.getString("APPLY_UNIQUE_NBR"));
		setSmsEmail(smsMap);

		// 강좌예약 유료문자(L20001)
		// if(Integer.parseInt(((Map)list.get(0)).get("TOTAL_PRICE").toString())
		// > 0
		// &&
		// !((Map)list.get(0)).get("APPLY_STATUS").toString().equals("결제완료")){
		// //예약신청(L00005)
		// smsMap.put("CONTENTS_TYPE", "L20001");
		// setSmsEmail(smsMap);
		// }
		return "schedules/complete.app";
	}

	/**
	 * 예약환불 SMS EMAIL 발송
	 * 
	 * @param map
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void setSmsEmail(Map map) throws Exception {
		// FAMILY_NO - null
		// GUEST_NAME - null
		// CAMPAIGN_NBR - null
		// CAMPAIGN_LOG_NBR - null
		// LINK_UNIQUE_NBR - null
		// LINK_UNIQUE_NBR2 - null
		// LINK_UNIQUE_NBR3 - null

		// 예약 관련 SMS를 전송 할 시 아래 6가지 내용을 입력하여 smsEmailService.setSmsEmail(map) 에
		// map으로 파라미터를 전송한다.

		// SYSTEM_TYPE 1. 1. LEC: 강의, MEM: 맴버
		// RESER_DATETIME - 2. 예약일시
		// SUB_TITLE_NAME - 3. 프로그램명
		// SCHEDULE - 4. 프로그램 일자,시간
		// PAYMENT_AMT - 5. 총 결제금액
		// LIMIT_DATE - 6. 결제를 해야 할 기간
		// MEMBER_NO - 6. 사용자번호

		map.put("SYSTEM_TYPE", "LEC");
		map.put("COMPANY_CD", propertyHelper.getCompanycd());
		// 예약자 정보인 ORDNO 으로 COURSE_CD,PRICE_NBR, MEMBER_NO, PAYMENT_AMT,
		// LIMIT_DATE (RESER_DATE, RESER_TIME = RESER_DATETIME) 을 가져옴
		Map cam = paymentCheckService.get_TL_COURSE_APPLY(map.get("ORDNO").toString());
		map.put("COURSE_CD", cam.get("COURSE_CD"));
		map.put("PRICE_NBR", cam.get("PRICE_NBR"));
		map.put("MEMBER_NO", cam.get("MEMBER_NO"));
		String reserDate = Utility.CheckObjectNull(cam.get("RESER_DATE"));
		String reserTime = Utility.CheckObjectNull(cam.get("RESER_TIME"));
		map.put("RESER_DATETIME", reserDate + reserTime);
		map.put("PAYMENT_AMT", cam.get("TOTAL_PRICE"));
		String limitDate = Utility.CheckObjectNull(cam.get("RESERVE_LIMIT_DATE"));
		logger.info("limitDate -- " + limitDate);
		limitDate = limitDate.equals("") ? ""
				: limitDate.substring(2, 4) + "년  " + limitDate.substring(4, 6) + "월 " + limitDate.substring(6, 8)
						+ "일 ";
		map.put("LIMIT_DATE", limitDate);

		// 가져온 COURSE_CD, PRICE_NBR로 예약정보를 가져옴. SUB_TITLE_NAME, SCHEDULE
		Map cm = paymentCheckService.get_SELECT_COURSE_CHECK(map);
		map.put("SUB_TITLE_NAME", cm.get("COURSE_NAME") + " " + cm.get("PRICE_NAME"));
		String schedule = "";
		if (cm.get("COURSE_DATE") != null) {
			schedule += Utility.CheckObjectNull(cm.get("COURSE_DATE")).substring(4, 6) + "."
					+ Utility.CheckObjectNull(cm.get("COURSE_DATE")).substring(6, 8);
			schedule += "(" + DateTime.whichDayString(Utility.CheckObjectNull(cm.get("COURSE_DATE"))) + ")-";
		}
		schedule += Utility.CheckObjectNull(cm.get("COURSE_START_TIME")).substring(0, 2) + ":"
				+ Utility.CheckObjectNull(cm.get("COURSE_START_TIME")).substring(2, 4);
		map.put("SCHEDULE", schedule);

		smsEmailService.setSmsEmail(map);
	}

	/**
	 * 가족 리스트
	 * 
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/FamilyList")
	public @ResponseBody Model FamilyList(Model model, HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		logger.info("ScheduleController.FamilyList -- ! " + HttpUtility.getBox(request).toString());

		Map<String, Object> params = new HashMap<String, Object>();
		Map<String, Object> rs;
		String COMPNAY_CD = Utility.CheckNull(request.getParameter("COMPANY_CD"));
		String MEMBER_NO = Utility.CheckNull(request.getParameter("MEMBER_NO"));
		String PRICE_NBR = Utility.CheckNull(request.getParameter("PRICE_NBR"));
		String OWNER_LIMIT_FLAG = Utility.CheckNull(request.getParameter("OWNER_LIMIT_FLAG"));
		String AGE_FLAG = Utility.CheckNull(request.getParameter("AGE_FLAG"));
		String AGE_TYPE = Utility.CheckNull(request.getParameter("AGE_TYPE"));
		String AGE_LIMIT_MIN = Utility.CheckNull(request.getParameter("AGE_LIMIT_MIN"));
		String AGE_LIMIT_MAX = Utility.CheckNull(request.getParameter("AGE_LIMIT_MAX"));
		String COURSE_CD = Utility.CheckNull(request.getParameter("COURSE_CD"));
		String SEMESTER_CD = Utility.CheckNull(request.getParameter("SEMESTER_CD"));
		String STR = "";

		params.put("COMPNAY_CD", COMPNAY_CD);
		params.put("MEMBER_NO", MEMBER_NO);
		params.put("PRICE_NBR", PRICE_NBR);
		params.put("OWNER_LIMIT_FLAG", OWNER_LIMIT_FLAG);
		params.put("AGE_FLAG", AGE_FLAG);
		params.put("AGE_TYPE", AGE_TYPE);
		params.put("AGE_LIMIT_MIN", AGE_LIMIT_MIN);
		params.put("AGE_LIMIT_MAX", AGE_LIMIT_MAX);
		params.put("COURSE_CD", COURSE_CD);

		List FamilyList = lectureService.getFamilyList(params);
		List FriendList = lectureService.getFriendList(params);
		List TargetList = lectureService.getTargetList(params);
		

		params.put("companyCd", COMPNAY_CD);
		params.put("courseCd", COURSE_CD);
		params.put("semesterCd", SEMESTER_CD);
		if (!FamilyList.isEmpty()) {
			rs = (Map) FamilyList.get(0);
			STR = (String) rs.get("FAMILY_NO");
		}
		params.put("familyNo", STR);

		// 회원등급 리스트
		List memberGradeList = lectureService.getMemberGradeList(params);

		if (!memberGradeList.isEmpty()) {
			rs = (Map) memberGradeList.get(0);
			STR = (String) rs.get("MEMBERSHIP_TYPE");
		}

		params.put("STR", STR);
		// 강좌등급 리스트
		List CourseGradeList = lectureService.getCoursePriceList2(params);
		
		model.addAttribute("FriendList", FriendList);
		model.addAttribute("CourseGradeList", CourseGradeList);
		model.addAttribute("FamilyList", FamilyList);
		model.addAttribute("TargetList", TargetList);
		model.addAttribute("SCHEDULE_FLAG", Utility.CheckNull(request.getParameter("SCHEDULE_FLAG")));

		return model;
	}

	/**
	 * 수강생별 가격
	 * 
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/StudentPrice")
	public @ResponseBody Model StudentPrice(Model model, HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		logger.info("ScheduleController.StudentPrice -- ! " + HttpUtility.getBox(request).toString());

		Map<String, Object> params = new HashMap<String, Object>();
		Map<String, Object> rs;

		String companyCd = Utility.CheckNull(request.getParameter("COMPANY_CD"));
		String courseCd = Utility.CheckNull(request.getParameter("COURSE_CD"));
		String day = Utility.CheckNull(request.getParameter("DAY"));
		String priceNbr = Utility.CheckNull(request.getParameter("PRICE_NBR"));
		String familyNo = Utility.CheckNull(request.getParameter("FAMILY_NO"));
		String targetCd = Utility.CheckNull(request.getParameter("TARGET_CD"));
		String courseStartDate = Utility.CheckNull(request.getParameter("COURSE_START_DATE"));
		String courseEndDate = Utility.CheckNull(request.getParameter("COURSE_END_DATE"));
		String scheduleFlag = Utility.CheckNull(request.getParameter("SCHEDULE_FLAG"));
		String optionList = Utility.CheckNull(URLDecoder.decode(request.getParameter("OPTION_LIST"), "UTF-8"));
		String peopleCnt = Utility.CheckNull(request.getParameter("PEOPLE_CNT"));
		String priceNum = Utility.CheckNull(request.getParameter("PRICE_NUM"));

		String STR = "";

		params.put("companyCd", companyCd);
		params.put("courseCd", courseCd);
		params.put("DAY", day);
		params.put("priceNbr", priceNbr);
		params.put("familyNo", familyNo);
		params.put("courseStartDate", courseStartDate);
		params.put("courseEndDate", courseEndDate);
		params.put("scheduleFlag", scheduleFlag);
		// 회원등급 리스트
		List memberGradeList = lectureService.getMemberGradeList(params);

		if (!memberGradeList.isEmpty()) {
			rs = (Map) memberGradeList.get(0);
			STR = (String) rs.get("MEMBERSHIP_TYPE");
		}

		params.put("STR", STR);

		// 강좌등급 리스트
		List courseGradeList = lectureService.getCoursePriceList(params);

		// 회원 이름
		List memberName = lectureService.getMemberName(params);

		model.addAttribute("courseGradeList", courseGradeList);
		model.addAttribute("memberName", memberName);
		model.addAttribute("TargetCd", targetCd);
		model.addAttribute("optionList", optionList);
		model.addAttribute("peopleCnt", peopleCnt);
		model.addAttribute("priceNum", priceNum);
		return model;
	}

	/**
	 * 가족 회원 등록
	 * 
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/RegFamily")
	public @ResponseBody Model RegFamily(Model model, HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		logger.info("ScheduleController.RegFamily -- ! " + HttpUtility.getBox(request).toString());

		String[] FamilyName = request.getParameterValues("FAMILY_NM");
		String FamilyNo = request.getParameter("FAMILY_NO");
		String[] GenderFlag = request.getParameterValues("GENDER_FLAG");
		String[] FamilyCel = request.getParameterValues("FAMILY_CEL");
		String[] BirthDay = request.getParameterValues("BIRTHDAY_FAMILY");
		String[] SchoolName = request.getParameterValues("SCHOOL_NM");
		String[] SchoolGrade = request.getParameterValues("SCHOOL_GD");
		String[] ClassName = request.getParameterValues("CLASS_NM");
		String[] ClassNo = request.getParameterValues("CLASS_NO");
		String SaleType = request.getParameter("SALE_TYPE");
		String MemberNo = request.getParameter("MEMBER_NO");
		String CompanyCd = request.getParameter("COMPANY_CD");

		Entity FamilyEntity = new Entity(7);

		FamilyEntity.setField("MODIFY_TYPE", "FAMILY");
		FamilyEntity.setField("SALE_TYPE", SaleType);
		FamilyEntity.setField("MEMBER_NO", MemberNo);
		FamilyEntity.setField("WORKER_MEMBER_NO", MemberNo);
		FamilyEntity.setField("CLIENT_IP", request.getRemoteAddr());

		Entity FamilyList = new Entity(1);
		Entity FamilyItem;

		if (FamilyName != null) {
			for (int i = 0; i < FamilyName.length; i++) {
				FamilyItem = new Entity(13);

				FamilyItem.setField("FAMILY_NO", Utility.CheckNull(FamilyNo));
				FamilyItem.setField("FAMILY_NAME", Utility.CheckNull(URLDecoder.decode(FamilyName[i], "UTF-8")));
				FamilyItem.setField("RELATION_TYPE", "FAMILY");
				FamilyItem.setField("BIRTH_DAY", Utility.CheckNull(BirthDay[i]));
				FamilyItem.setField("GENDER_FLAG", Utility.CheckNull(GenderFlag[i]));
				FamilyItem.setField("FAMILY_CEL", Utility.CheckNull(FamilyCel[i].replaceAll("-", "")));
				FamilyItem.setField("FAMILY_EMAIL", "");
				FamilyItem.setField("SCHOOL_NAME", Utility.CheckNull(URLDecoder.decode(SchoolName[i], "UTF-8")));
				FamilyItem.setField("GRADE_NAME", Utility.CheckNull(URLDecoder.decode(SchoolGrade[i], "UTF-8")));
				FamilyItem.setField("CLASS_NAME", Utility.CheckNull(URLDecoder.decode(ClassName[i], "UTF-8")));
				FamilyItem.setField("CLASS_NO", Utility.CheckNull(URLDecoder.decode(ClassNo[i], "UTF-8")));
				FamilyItem.setField("WELFARE_FLAG", "N");
				FamilyItem.setField("USE_FLAG", "Y");

				FamilyList.addField("FAMILY_ITEM", FamilyItem);
			}
		}

		FamilyEntity.setField("FAMILY_LIST", FamilyList);
		Map<String, Object> result = FamilyModifyService.ModifyFamily(CompanyCd, FamilyEntity);

		model.addAttribute("result", result.get("result"));
		model.addAttribute("Msg", (String) result.get("Msg"));

		logger.info("result -- " + result.toString());

		return model;
	}

	/**
	 * 가족외 회원 등록
	 * 
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/RegFriend")
	public @ResponseBody Model RegFriend(Model model, HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		logger.info("ScheduleController.RegFriend -- ! " + HttpUtility.getBox(request).toString());

		String[] FamilyName = request.getParameterValues("FRIEND_NM");
		String FamilyNo = request.getParameter("FRIEND_NO");
		String[] GenderFlag = request.getParameterValues("GENDER_FLAG");
		String[] FamilyCel = request.getParameterValues("FRIEND_CEL");
		String[] BirthDay = request.getParameterValues("BIRTHDAY_FRIEND");
		String[] SchoolName = request.getParameterValues("SCHOOL_NM");
		String SaleType = request.getParameter("SALE_TYPE");
		String MemberNo = request.getParameter("MEMBER_NO");
		String CompanyCd = request.getParameter("COMPANY_CD");
		String CourseCd = request.getParameter("COURSE_CD");

		Entity FamilyEntity = new Entity(8);

		FamilyEntity.setField("MODIFY_TYPE", "FAMILY");
		FamilyEntity.setField("SALE_TYPE", SaleType);
		FamilyEntity.setField("MEMBER_NO", MemberNo);
		FamilyEntity.setField("COURSE_CD", CourseCd);
		FamilyEntity.setField("WORKER_MEMBER_NO", MemberNo);
		FamilyEntity.setField("CLIENT_IP", request.getRemoteAddr());

		Entity FamilyList = new Entity(1);
		Entity FamilyItem;

		if (FamilyName != null) {
			for (int i = 0; i < FamilyName.length; i++) {
				FamilyItem = new Entity(12);

				FamilyItem.setField("FRIEND_NO", Utility.CheckNull(FamilyNo));
				FamilyItem.setField("FRIEND_NAME", Utility.CheckNull(URLDecoder.decode(FamilyName[i], "UTF-8")));
				FamilyItem.setField("RELATION_TYPE", "FRIEND");
				FamilyItem.setField("BIRTH_DAY", Utility.CheckNull(BirthDay[i]));
				FamilyItem.setField("GENDER_FLAG", Utility.CheckNull(GenderFlag[i]));
				FamilyItem.setField("FRIEND_CEL", Utility.CheckNull(FamilyCel[i].replaceAll("-", "")));
				FamilyItem.setField("FRIEND_EMAIL", "");
				FamilyItem.setField("SCHOOL_NAME", Utility.CheckNull(URLDecoder.decode(SchoolName[i], "UTF-8")));
				FamilyItem.setField("GRADE_NAME", "");
				FamilyItem.setField("CLASS_NAME", "");
				FamilyItem.setField("CLASS_NO", "");
				FamilyItem.setField("WELFARE_FLAG", "N");
				FamilyItem.setField("USE_FLAG", "Y");

				FamilyList.addField("FRIEND_ITEM", FamilyItem);
			}
		}

		FamilyEntity.setField("FRIEND_LIST", FamilyList);

		Map<String, Object> result = FamilyModifyService.ModifyFriend(CompanyCd, FamilyEntity);

		model.addAttribute("result", result.get("result"));
		model.addAttribute("Msg", (String) result.get("Msg"));
		return model;
	}

	public String getCurrentMemberNo(HttpServletRequest request) {
		return (String) request.getSession().getAttribute(Member.CURRENT_MEMBER_NO);
	}
}
