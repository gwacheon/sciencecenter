package kr.go.sciencecenter.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import kr.co.mainticket.Utility.DateTime;
import kr.co.mainticket.Utility.Utility;
import kr.go.sciencecenter.api.AuthenticationApi;
import kr.go.sciencecenter.api.MembershipApi;
import kr.go.sciencecenter.model.OrgApplicant;
import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.model.Voluntary;
import kr.go.sciencecenter.model.Volunteer;
import kr.go.sciencecenter.model.api.Family;
import kr.go.sciencecenter.model.api.Member;
import kr.go.sciencecenter.model.api.MemberShip;
import kr.go.sciencecenter.persistence.PaymentCheckMapper;
import kr.go.sciencecenter.service.EquipReserveService;
import kr.go.sciencecenter.service.OrgProgramService;
import kr.go.sciencecenter.service.UserService;
import kr.go.sciencecenter.service.VoluntaryService;
import kr.go.sciencecenter.util.FileUploadHelper;
import kr.go.sciencecenter.util.PropertyHelper;

@Controller
@RequestMapping("/mypage")
@SessionAttributes(Member.CURRENT_MEMBER_NO)
public class MypageController {
	private static final Logger logger = LoggerFactory.getLogger(MypageController.class);
	
	@Autowired
	UserService userService;
	
	@Autowired
	VoluntaryService voluntaryService;
	
	@Autowired
	OrgProgramService orgProgramService;
	
	@Autowired
	FileUploadHelper fileUploadHelper;
	
	@Autowired
	EquipReserveService equipReserveService;
	
	@Autowired PropertyHelper propertyHelper;
	@Autowired PaymentCheckMapper paymentCheckMapper;
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String index(Model model){
		return "redirect:/payment/";
	}
	
	@RequestMapping(value = "/voluntaries", method = RequestMethod.GET)
	public String voluntaries(@ModelAttribute(Member.CURRENT_MEMBER_NO) String memberNo, 
			HttpServletRequest request, Model model,
			@RequestParam(value = "page", defaultValue = "1") Integer currentPage){
		
		Page page = new Page(currentPage, 10, voluntaryService.myVolunteersCount(memberNo));
		page.setPath(request.getServletPath() + "?");
		model.addAttribute("page", page);
		
		List<Volunteer> volunteers = voluntaryService.myVolunteers(memberNo, page);
		model.addAttribute("volunteers", volunteers);
		
		return "mypage/voluntaries.app";
	}
	
	@RequestMapping(value = "/privateInfo", method = RequestMethod.GET)
	public String privateInfo(@ModelAttribute(Member.CURRENT_MEMBER_NO) String memberNo, Model model) {
		Member currentMember = getCurrentMember(memberNo);
		model.addAttribute("member", currentMember);
		
		return "mypage/privateInfo.app";
	}
	
	@RequestMapping(value = "/sangsang", method = RequestMethod.GET)
	public String sangsangList(@ModelAttribute(Member.CURRENT_MEMBER_NO) String memberNo, Model model) {
		Member currentMember = getCurrentMember(memberNo);
		model.addAttribute("member", currentMember);
		
		model.addAttribute("reserves", equipReserveService.gerReserves(memberNo));
		return "mypage/sangsang.app";
	}
	
	@RequestMapping(value = "/reservations", method = RequestMethod.GET)
	public String myReservations(HttpServletRequest request, Model model) {
		return "mypage/reservations/index.app";
	}
	
	@RequestMapping(value = "/reservations/orgApplicants", method = RequestMethod.GET)
	public String myOrgPrograms(@ModelAttribute(Member.CURRENT_MEMBER_NO) String memberNo
			, HttpServletRequest request
			, @RequestParam(value = "page", defaultValue = "1") Integer currentPage
			, Model model) {
		Page page = new Page(currentPage, 10, orgProgramService.myApplicantsCount(memberNo));
		page.setPath(request.getServletPath() + "?");
		model.addAttribute("page", page);
		
		List<OrgApplicant> orgApplicants = orgProgramService.myApplicants(memberNo, page);
		model.addAttribute("orgApplicants", orgApplicants);
		return "mypage/reservations/orgPrograms.app";
	}
	
	
	@RequestMapping(value = "/{memberNo}", method = RequestMethod.PUT)
	public String update(@ModelAttribute("member") Member member
			, @ModelAttribute("memberNo") String currentMemberNo
			, @PathVariable("memberNo") String memberNo
			, HttpServletRequest request
			, BindingResult result
			, Model model) {
		Member currentMember = getCurrentMember(currentMemberNo);
		model.addAttribute("member", currentMember);
		
		if (currentMember.getMemberNo().equals(member.getMemberNo())) {
			AuthenticationApi api = new AuthenticationApi();
			api.update(member);
			
			if (member.getRspCd().equals("0000")) {
				model.addAttribute("member", member);
				model.addAttribute("status", "complete");
			} else if(member.getRspCd().equals("E901")) {
				model.addAttribute("status", "emailerror");
				model.addAttribute("errMsg", "API Email Error");
			} else {
				model.addAttribute("status", "error");
				model.addAttribute("errMsg", "API Server Error");
			}
		} else {
			model.addAttribute("status", "error");
			model.addAttribute("errMsg", "Not Match User Info");
		}
		
		return "mypage/privateInfo.app";
		//return "redirect:/mypage/privateInfo";
	}
	
	@RequestMapping(value = "/modify", method = RequestMethod.POST)
	public String edit(@ModelAttribute("member") Member member
			, HttpServletRequest request
			, BindingResult result
			, Model model) {
		AuthenticationApi api = new AuthenticationApi();
		member.setMemberNo((String)request.getSession().getAttribute(Member.CURRENT_MEMBER_NO));
		member = api.update(member);
		
		if (member.getRspCd().equals("0000")){
			return "redirect:/mypage";
		} else {
			System.out.println("정보수정 실패 : " + member.getRspMsg());
			return "redirect:redirect:/mypage/privateInfo";
		}
	}
	
	@RequestMapping(value = "/changePassword", method = RequestMethod.GET)
	public String editPassword(@ModelAttribute(Member.CURRENT_MEMBER_NO) String memberNo, Model model) {
		Member member = new Member(memberNo);
		model.addAttribute("member", member);
		
		return "mypage/changePassword.app";
	}
	
	@RequestMapping(value = "/changePassword", method = RequestMethod.POST)
	public String changePassword(@ModelAttribute("member") Member member
			, HttpServletRequest request
			, BindingResult result
			, Model model) {
		
		AuthenticationApi.getInstance().changePassword(member);
		
		if (member.getRspCd().equals("0000")) {
			model.addAttribute("updateStatus", "complete");
		} else {
			model.addAttribute("updateStatus", "failed");
		}
		
		return "mypage/changePassword.app";
	}
	
	@RequestMapping(value = "/privateInfo/family", method = RequestMethod.GET)
	public String myFamily(@ModelAttribute(Member.CURRENT_MEMBER_NO) String memberNo, Model model) {
		Member currentMember = getCurrentMember(memberNo);
		model.addAttribute("member", currentMember);
		
		Member memberWithFamily = AuthenticationApi.getInstance().getFamilyList(currentMember);
		model.addAttribute("memberWithFamily", memberWithFamily);
		
		return "mypage/family.app";
	}
	
	@RequestMapping(value="/addFamily", method = RequestMethod.POST)
	public @ResponseBody Model addFamily(@ModelAttribute(Member.CURRENT_MEMBER_NO) String memberNo
			, @RequestBody String jsonStr, Model model) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			Family family = mapper.readValue(jsonStr, Family.class);
			family.setMemberNo(memberNo);
			family.setUseFlag(true);
			family.setFileName("Temp file name");
			family = AuthenticationApi.getInstance().registFamily(family);
			
			if(family.getRspCd().equals("0000")) {
				model.addAttribute("status", "complete");
			}else {
				model.addAttribute("status", "error");
				model.addAttribute("errorMsg", family.getRspMsg());
			}
			
		} catch (Exception e) {
			logger.error(e.toString());
			model.addAttribute("status", "error");
			model.addAttribute("errorMsg", "Family Register Error");
		}
		return model;
	}
	
	@RequestMapping(value="/updateFamily", method = RequestMethod.POST)
	public @ResponseBody Model updateFamily(@ModelAttribute(Member.CURRENT_MEMBER_NO) String memberNo
			, @RequestBody String jsonStr, Model model) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			Family family = mapper.readValue(jsonStr, Family.class);
			family.setMemberNo(memberNo);
			family.setFamilyNo(family.getFamilyNo());
			family.setUseFlag(true);
			family.setFileName("Temp file name");
			family = AuthenticationApi.getInstance().updateFamily(family);
			
			if(family.getRspCd().equals("0000")) {
				model.addAttribute("status", "complete");
			}else {
				model.addAttribute("status", "error");
				model.addAttribute("errorMsg", family.getRspMsg());
			}
			
		} catch (Exception e) {
			logger.error(e.toString());
			model.addAttribute("status", "error");
			model.addAttribute("errorMsg", "Family Register Error");
		}
		return model;
	}
	
	@RequestMapping(value="/profileImage", method = RequestMethod.POST)
	public @ResponseBody Model profileImage(@RequestParam("file") MultipartFile file
			, @ModelAttribute(Member.CURRENT_MEMBER_NO) String memberNo
			, Model model) {
		
		String subPath = Member.PROFILE_FILE_PREFIX;
		String fileName = memberNo + "." + FilenameUtils.getExtension(file.getOriginalFilename());
		
		model.addAttribute("status", "error");
		
		if (fileUploadHelper.upload(file, subPath, fileName)) {
			Member member = new Member();
			member.setMemberNo(memberNo);
			member.setFileName(fileName);
			
			AuthenticationApi.getInstance().updatePicture(member);
			
			if (member.getRspCd().equals("0000")){
				model.addAttribute("status", "complete");
				model.addAttribute("file", fileName);
				model.addAttribute("fileUrl", "/scipia/"+subPath + "/" + fileName);
			} else {
				model.addAttribute("errMsg", member.getRspCd());
			}
		} else {
			model.addAttribute("errMsg", "File Store Error");
		}
		
		return model;
	}
	
	
	/**
	 * 후원회원 등록 신청 화면
	 * @param memberNo
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/privateInfo/fundMember", method = RequestMethod.GET)
	public String fundMember(@ModelAttribute(Member.CURRENT_MEMBER_NO) String memberNo, Model model) {
		Member currentMember = getCurrentMember(memberNo);
		model.addAttribute("member", currentMember);
		return "mypage/fundMember.app";
	}
	
	/**
	 * 후원회원 등록 신청
	 * @param memberNo
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/privateInfo/fundMember", method = RequestMethod.POST)
	public @ResponseBody Model requestFundMember(
			@ModelAttribute(Member.CURRENT_MEMBER_NO) String memberNo
			, @RequestBody String jsonStr
			, Model model) {
		
		model.addAttribute("status", "failed");
		
		if(memberNo != null && !memberNo.equals("")) {
			ObjectMapper mapper = new ObjectMapper();
			
			try {
				MemberShip memberShip = mapper.readValue(jsonStr, MemberShip.class);
				memberShip.setMemberNo(memberNo);
				
				MembershipApi api = new MembershipApi();
				memberShip = api.requestSponsor(memberShip);
				
				if (memberShip.getRspCd().equals("0000")){
					model.addAttribute("status", "complete");
					model.addAttribute("memberShip", memberShip);
				}else {
					model.addAttribute("errorMsg", memberShip.getRspMsg());
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		return model;
	}
	
	@RequestMapping(value = "/dropout", method = RequestMethod.GET)
	public String dropout(@ModelAttribute(Member.CURRENT_MEMBER_NO) String memberNo, Model model) {
		Member currentMember = getCurrentMember(memberNo);
		model.addAttribute("member", currentMember);
		return "mypage/dropout.app";
	}
	
	@RequestMapping(value = "/dropout", method = RequestMethod.POST)
	public String submitDropout(@ModelAttribute(Member.CURRENT_MEMBER_NO) String memberNo
			, @ModelAttribute("member") Member member
			, Model model
			, HttpServletRequest request) {
		
		if(member.getMemberNo().equals(memberNo)) {
			AuthenticationApi.getInstance().delete(member);
			
			if(member.getRspCd().equals("0000")) {
				request.getSession().removeAttribute(Member.CURRENT_MEMBER_NO);
				model.addAttribute("status", "complete");
			}
		}
		
		return "mypage/dropout.app";
	}
	
	@RequestMapping(value = "/confirmationForm/{id}", method = RequestMethod.GET)
	public String confirmationForm(Model model, @PathVariable("id") int id){
		
		
		Volunteer volunteer = voluntaryService.findVolunteer(id);
		model.addAttribute("volunteer", volunteer);
		
		return "jsp/confirmationForm.markany";
	}
	
	@RequestMapping(value="/deleteFamily", method = RequestMethod.POST)
	public @ResponseBody Model deleteFamily(@ModelAttribute(Member.CURRENT_MEMBER_NO) String memberNo
			, @RequestBody String jsonStr, Model model) {
		Family family = new Family();
		family.setMemberNo(memberNo);
		family.setFamilyNo(new JSONObject(jsonStr).getString("familyNo"));
		
		AuthenticationApi.getInstance().deleteFamily(family);
		
		if(family.getRspCd() != null && family.getRspCd().equals("0000")) {
			model.addAttribute("status", "complete");
		}else {
			model.addAttribute("status", "error");
			model.addAttribute("errorMsg", family.getRspMsg());
		}
		
		return model;
	}
	
	/**
	 * 씨스퀘어에서 테스트용으로 초기 등록
	 * 연간회원 가입 초기 페이지
	 * @param memberNo
	 * @param member
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/updateMemberShip")
	public String updateMemberShip(@ModelAttribute(Member.CURRENT_MEMBER_NO) String memberNo, Model model, HttpServletRequest request) throws Exception {
		
		//맴버정보
		Member currentMember = getCurrentMember(memberNo);
		model.addAttribute("member", currentMember);
		
		//가족정보
		Member memberWithFamily = AuthenticationApi.getInstance().getFamilyList(currentMember);
		model.addAttribute("memberWithFamily", memberWithFamily);
		
		//결제시 필요한 정보
		model.addAttribute("pay_mbrId", 		propertyHelper.getPay_mbrId());
		model.addAttribute("pay_mbrName", 		propertyHelper.getPay_mbrName());
		model.addAttribute("pay_returnType",	propertyHelper.getPay_returnType());
		model.addAttribute("pay_version", 		propertyHelper.getPay_version());
		model.addAttribute("pay_targetUrl", 	propertyHelper.getPay_targetUrl());
		model.addAttribute("pay_authType", 		propertyHelper.getPay_authType());
		model.addAttribute("pay_server", 		propertyHelper.getPay_server());
		model.addAttribute("c2_homepage_url", 	propertyHelper.getC2_homepage_url());
		
		//결제시 필요한 UNIQUE 값
		String CurrDate = DateTime.getFormatDateString("yyyyMMddHHmmss", 0);
		//model.addAttribute("tranUniqueNbr", 	propertyHelper.getCompanycd() + "MEM" + CurrDate.substring(2, 8) + Utility.LPAD(getIndex(propertyHelper.getCompanycd(), "TRAN_UNIQUE_NBR", CurrDate.substring(0, 8), 1), 5, '0'));
		model.addAttribute("tranUniqueNbr", 	CurrDate.substring(0, 8) + Utility.LPAD(getIndex(propertyHelper.getCompanycd(), "TRAN_UNIQUE_NBR", CurrDate.substring(0, 6), 1), 4, '0'));
		
		//연간회원 가격정보
		Map m = new HashMap();
		m.put("MEMBERSHIP_TYPE", "SM");
		m.put("PRICE_NBR", "2");
		model.addAttribute("membeship_price", userService.getMembershipPrice(m));
		
		//사용자 정보
		model.addAttribute("MEMBER_NO", memberNo);
		model.addAttribute("MEMBER_NAME", currentMember.getMemberName());
		model.addAttribute("MEMBER_CEL", currentMember.getMemberCel().toString().replaceAll("-", ""));
		model.addAttribute("MEMBER_EMAIL", currentMember.getMemberEmail());
		
		//결제시 필요한 웹 모바일 구분
		if(isMobile(request)) 	model.addAttribute("SALE_TYPE","000005");
		else 					model.addAttribute("SALE_TYPE","000002");
		
		return "mypage/updateMemberShip.app";
	}
	
	private Member getCurrentMember(String memberNo) {
		return AuthenticationApi.getInstance().getInfo(memberNo);
	}
	
	/**
	 * Get the Index value 채번
	 * @param CompanyCd
	 * @param IndexType
	 * @param SeedDate
	 * @param Interval
	 * @return
	 * @throws Exception
	 */
	public long getIndex(String CompanyCd, String IndexType, String SeedDate, int Interval) throws Exception {
		long IndexValue = 0;
		Map<String,Object> params = new HashMap<String,Object>();
		try{			
			params.put("COMPANY_CD", CompanyCd);
			params.put("INDEX_TYPE", IndexType);
			params.put("CURRENT_DATE", SeedDate);
			params.put("FIRST_INDEX", Interval);
			
			// 채번
			paymentCheckMapper.GET_INDEX(params);
			
			IndexValue = Long.parseLong(String.valueOf(params.get("INDEX")));
			
		}catch(Exception e){
			throw e;
		}
		return IndexValue;
	} 
	
	/**
	 * 웹 / 모바일 구분
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private boolean isMobile(HttpServletRequest request)throws Exception{
		String userAgent = request.getHeader("user-agent");
		boolean mobile1 = userAgent.matches(".*(iPhone|iPod|Android|Windows CE|BlackBerry|Symbian|Windows Phone|webOS|Opera Mini|Opera Mobi|POLARIS|IEMobile|lgtelecom|nokia|SonyEricsson).*");
		boolean mobile2 = userAgent.matches(".*(LG|SAMSUNG|Samsung).*");
		if(mobile1 || mobile2){
			return true;
		}else{
			return false;
		}
	}
}

