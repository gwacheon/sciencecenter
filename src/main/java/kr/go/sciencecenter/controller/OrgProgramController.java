package kr.go.sciencecenter.controller;

import java.util.List;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import kr.go.sciencecenter.api.AuthenticationApi;
import kr.go.sciencecenter.model.OrgApplicant;
import kr.go.sciencecenter.model.OrgProgram;
import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.model.api.Member;
import kr.go.sciencecenter.service.OrgProgramService;

@Controller
@RequestMapping("/orgPrograms")
@SessionAttributes(Member.CURRENT_MEMBER_NO)
public class OrgProgramController {
	private static final Logger logger = LoggerFactory.getLogger(OrgProgramController.class);
	
	@Autowired
	OrgProgramService orgProgramService;
	
	@ModelAttribute
	public void setCategories(Model model) {
		model.addAttribute("currentCategory", "main.nav.catetory5");
	}	
	@RequestMapping(value="", method = RequestMethod.GET)
	public String index(Model model) {
		setCurrentCategory(model);
		
		List<OrgProgram> orgPrograms = orgProgramService.list();
		model.addAttribute("orgPrograms", orgPrograms);
		
		return "orgPrograms/index.app";
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public String index(@PathVariable("id") int id
			,Model model) {
		setCurrentCategory(model);
		
		OrgProgram orgProgram = orgProgramService.find(id);
		model.addAttribute("orgProgram", orgProgram);
		
		return "orgPrograms/show.app";
	}
	
	@RequestMapping(value="/{id}/reservation", method = RequestMethod.GET)
	public String reservation(@ModelAttribute(Member.CURRENT_MEMBER_NO) String memberNo
			, @PathVariable("id") int id
			, Model model){
		setCurrentCategory(model);
		
		OrgProgram orgProgram = orgProgramService.find(id);
		model.addAttribute("orgProgram", orgProgram);
		
		Member currentMember = getCurrentMember(memberNo);
		model.addAttribute("member", currentMember);
		
		return "orgPrograms/reservation.app";
	}
	
	@RequestMapping(value="/{id}/calendars", method = RequestMethod.POST)
	public @ResponseBody Model calendars(@PathVariable("id") int id
			, @RequestBody String jsonStr
			, Model model){
		JSONObject jsonObject = new JSONObject(jsonStr);
		String dateStr = jsonObject.getString("dateStr");
		
		Page page = new Page(1, 10, orgProgramService.applicantsCount(id, null, null));
		model.addAttribute("page", page);
				
		List<OrgApplicant> applicants = orgProgramService.applicants(id, null, page, dateStr);
		model.addAttribute("applicants", applicants);
		
		return model;
	}
	
	@RequestMapping(value="/{id}/applicant", method = RequestMethod.POST)
	public @ResponseBody Model applicant(@PathVariable("id") int id
			, @RequestBody String jsonStr
			, @ModelAttribute(Member.CURRENT_MEMBER_NO) String memberNo
			, Model model){
		OrgApplicant orgApplicant = new OrgApplicant(id, jsonStr);
		orgApplicant.setMemberNo(memberNo);
		orgApplicant.setApplicationStatus(OrgApplicant.APPLICANT_STATUS_PENDING);
		
		orgProgramService.createApplicant(orgApplicant);
		model.addAttribute("orgApplicant", orgApplicant);
		
		return model;
	}
	
	private void setCurrentCategory(Model model){
		model.addAttribute("currentCategory", "main.nav.catetory5");
	}
	
	private Member getCurrentMember(String memberNo) {
		return AuthenticationApi.getInstance().getInfo(memberNo);
	}
}
