package kr.go.sciencecenter.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import kr.go.sciencecenter.model.Event;
import kr.go.sciencecenter.model.Page;
import kr.go.sciencecenter.service.EventsService;
import kr.go.sciencecenter.service.MainEventService;

@Controller
@RequestMapping("/eventsTotal")
public class EventsTotalController {
    
    @Autowired
    EventsService eventsService;
    
    @Autowired
    MainEventService mainEventService;
    
    /**
	 * 카테고리 Dropdown 
	 * @param model
	 */
	@ModelAttribute
	public void setCategories(Model model) {
		model.addAttribute("currentCategory", "main.nav.catetory5");
	}
    
	
	/**
	 * 행사/공연 통합 페이지 index
	 * @param type
	 * @param model
	 * @param status
	 * @param currentPage
	 * @return
	 */
    @RequestMapping(value="", method = RequestMethod.GET)
    public String index(Model model,
    		@RequestParam(value="status", defaultValue="true")boolean status,
    		@RequestParam(value = "page", defaultValue = "1") Integer currentPage) {    	
    	
    	String path = "/eventsTotal?"+"status="+status+"&";
    	List<String> typeList = new ArrayList<String>();
    	typeList.add("event");
    	typeList.add("play");
    	Page page = new Page(currentPage,6,eventsService.countByMultipleTypes(typeList, status));
    	
    	page.setPath(path);
    	model.addAttribute("page", page);
    	
    	model.addAttribute("events", eventsService.listByMultipleTypes(typeList, status, page));
		return "eventsTotal/index.app";
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String show(@PathVariable int id, Model model) {
    		
    	Event event = eventsService.find(id);
    	model.addAttribute("event", event);
    	
    	// 이전글 다음글
    	List<String> typeList = new ArrayList<String>();
    	typeList.add("event");
    	typeList.add("play");
    	model.addAttribute("prevEvent", eventsService.prevEventByMultipleTypes(id, typeList));
    	model.addAttribute("nextEvent", eventsService.nextEventByMultipleTypes(id, typeList));
    	
    	return "eventsTotal/show.app";
    }


}
