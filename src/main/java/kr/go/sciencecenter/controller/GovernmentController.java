package kr.go.sciencecenter.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import kr.go.sciencecenter.model.Board;
import kr.go.sciencecenter.model.BreadCrumb;
import kr.go.sciencecenter.service.BoardService;

/**
 * 정보공개 Controller
 * @author Prompt Technology
 *
 */
@Controller
@RequestMapping("/government")
public class GovernmentController {
	
	@Autowired
	private BoardService boardService;

	/**
	 * 정보공개제도안내 페이지
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/intro", method = RequestMethod.GET)
	public String intro(Locale locale,Model model){
		model.addAttribute("crumb", new BreadCrumb(8,1));
		return "government/intro.app";
	}
	
	/**
	 * 사전정보공개 페이지
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/open", method = RequestMethod.GET)
	public String open(Locale locale,Model model){
		model.addAttribute("crumb", new BreadCrumb(8,2));
		return "government/open.app";
	}
	
	/**
	 * 정보목록 페이지
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/list", method = RequestMethod.GET)
	public String list(Locale locale,Model model){
		model.addAttribute("crumb", new BreadCrumb(8,3));
		return "government/list.app";
	}
	
	/**
	 * 공공데이터개방 페이지
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/data", method = RequestMethod.GET)
	public String data(Locale locale,Model model){
		model.addAttribute("crumb", new BreadCrumb(8,5));
		return "government/data.app";
	}
	
	/**
	 * 공개정보자료실 게시판 Index
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/sharedInfoArchive", method = RequestMethod.GET)
	public String sharedInfoArchive(Locale locale, 
			Model model,
			@RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			@RequestParam(value = "searchType", required=false) String searchType,
			@RequestParam(value = "searchKey", required=false) String searchKey) {
	
		Board.getBoards(model, boardService, "sharedInfoArchive", currentPage, searchType, searchKey);
		model.addAttribute("crumb", new BreadCrumb(8,6));
		return "government/sharedInfoArchive.app";
	}
	
	/**
	 * 공개정보자료실 게시판 Show
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/sharedInfoArchive/{id}", method = RequestMethod.GET)
	public String sharedInfoArchiveShow(@PathVariable int id, Model model) {
		
		model.addAttribute("id", id);
		model.addAttribute("crumb", new BreadCrumb(8,6));
		return "government/sharedInfoArchiveShow.app";
	}
}
