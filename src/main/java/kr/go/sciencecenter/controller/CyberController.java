package kr.go.sciencecenter.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import kr.go.sciencecenter.model.Board;
import kr.go.sciencecenter.model.BreadCrumb;
import kr.go.sciencecenter.service.BoardService;

@Controller
@RequestMapping("/cyber")
public class CyberController {
	@Autowired
	private BoardService boardService;

	@RequestMapping(value = "/vrExhibition", method = RequestMethod.GET)
	public String cyberExhibition(Model model){

		model.addAttribute("crumb", new BreadCrumb(3,1));
		return "cyber/cyberExhibition.app";
	}

	@RequestMapping(value = "/digitalEducation", method = RequestMethod.GET)
	public String digitalEducation(Model model,
			@RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			@RequestParam(value = "searchType", required=false) String searchType,
			@RequestParam(value = "searchKey", required=false) String searchKey){
		
		Board.getBoards(model, boardService, "digitalEducation", currentPage, searchType, searchKey);
		model.addAttribute("crumb", new BreadCrumb(3,2));

		return "cyber/digitalEducation.app";
	}

	@RequestMapping(value="/sciencePlayground")
	public String sciencePlay(Model model,
			@RequestParam(value = "page", defaultValue = "1") Integer currentPage,
			@RequestParam(value = "searchType", required=false) String searchType,
			@RequestParam(value = "searchKey", required=false) String searchKey){

		Board.getBoards(model, boardService, "digitalEducation", currentPage, searchType, searchKey);
		model.addAttribute("crumb", new BreadCrumb(3,3));

		return "cyber/sciencePlay.app";
	}
}
