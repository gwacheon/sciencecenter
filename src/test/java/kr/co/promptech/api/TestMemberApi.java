package kr.co.promptech.api;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.google.common.base.Joiner;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import kr.go.sciencecenter.api.AuthenticationApi;
import kr.go.sciencecenter.model.api.Family;
import kr.go.sciencecenter.model.api.Member;

public class TestMemberApi {
	private static AuthenticationApi api;
	
	@BeforeClass
	public static void initAuthenticationApi(){
		api = new AuthenticationApi();
	}
	
	@Test
	public void testRegistFamily() {
		Member member = new Member();
		member.setMemberNo("GNSM13012617131842");
		
		Family family = new Family();
		family.setMemberNo(member.getMemberNo());
		family.setBirthDate("19841010");
		family.setFamilyCel("010-123-1234");
		family.setFamilyEmail("family@gg.kr");
		family.setSchoolName("가족학교");
		family.setClassName("34");
		//family.setLunarFlag("N");
		family.setGenderFlag("M");
		
		api.registFamily(family);
	}
	
	@Test
	public void testGetFamily() {
		Member member = new Member();
		member.setMemberNo("GNSM13012617131842");
		
		api.getFamilyList(member);
		
		for(Family f : member.getFamilies()) {
			System.out.println(f.getMemberNo());
		}
	}
	
	@Test
	public void testUpdate() {
		Member member = new Member();
		member.setMemberNo("GNSM08111008000265");
		member.setPasswordCheck("GNSM08111008000265");
		member.setMemberCel("010-0101-0101");
		member.setMemberEmail("update@co.kr");
		member.setDmFlag(true);
		member.setSmsFlag(true);
		member.setEmailFlag(true);
		
		//Update 호출
		assertEquals(true, api.update(member).getRspCd().equals("0000"));
		
		//Update 이후 변경 된 값 확인
		api.getMemberInfo(member);
		assertEquals(true, member.isSmsFlag());
		
		member.setSmsFlag(false);
		api.update(member);
		
		api.getMemberInfo(member);
		assertEquals(false, member.isSmsFlag());
		
	}
	
	@Test
	public void testWithdraw() {
		Member member = new Member();
		member.setMemberNo("GNSM08111008000265");
		member.setDropReason("탈퇴 시켜주세요.");
		
		assertEquals(true, api.withdraw(member).getRspCd().equals("0000"));
	}
	
	@Test
	public void testChangePassword() {
		Member member = new Member();
		member.setMemberNo("GNSM08111008000265");
		member.setPassword("1");
		
		assertEquals(true, api.changePassword(member).getRspCd().equals("0000"));
	}
	
	@Test
	public void testMemberInfo() {
		Member member = new Member();
		member.setMemberNo("GNSM16032815011668");

		api.getMemberInfo(member);
		
		assertEquals(true, member.getRspCd().equals("0000"));
	}
	
	@Test
	public void testFindPw() {
		Member member = new Member();
		member.setDuplicateKey("1E195C8A2");
		
		api.findPassword(member);
		assertEquals(true, member.getRspCd().equals("0000"));
	}
	
	@Test
	public void testFindId() {
		Member member = new Member();
		member.setMemberName("일");
		member.setMemberEmail("111@co.kr");
		
		api.findId(member);
		assertEquals(true, member.getRspCd().equals("0000"));
	}
	
	
	@Test
	public void testCheckId() {
		String testMemberId = "ckh8599";
		
		Member member = new Member();
		member.mergeMember(api.checkId(testMemberId));
		
		//RspCd 0000 --> 사용가능
		//RspCd CI02 --> ExistUser
		//UsableFlag의 경우 default 값을 가지고 있기 때문에 판단 여부로 사용하기 어려움
		assertEquals(true, (member.getRspCd().equals("0000") || member.getRspCd().equals("CI02")));
	}
	
	@Test
	public void testLogin() {
		Member member = new Member();
		member.setMemberId("ckh85");
		member.setPassword("1");
		
		api.login(member);
		
		assertEquals(true, member.getRspCd().equals("0000"));
	}
	
	@Test
	public void testRegist() {
		Member member = new Member();
		member.setMemberId("iskim");
		member.setPassword("qweqweqwe");
		member.setMemberName("김인성");
		member.setMemberEmail("iskim1018@gmail.com");
		member.setMemberCel("010-123-1234");
		member.setBirthDate("19841018");
		member.setMemberZipCode("151-802");
		member.setMemberAddr1("서울 관악구 남부순환로260길 11");
		member.setMemberAddr2("209호");
		member.setEmailFlag(false);
		member.setDmFlag(false);
		member.setSmsFlag(true);
		member.setGenderFlag("M");
		member.mergeMember(api.regist(member));
		
		assertEquals(true, member.getRspCd().equals("0000"));
	}
}
