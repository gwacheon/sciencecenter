# 국립과천과학관 홈페이지 #

## 개발환경 세팅 ##

### Database Configuration
* /src/main/resources/database.properties 생성 (database_sample.properties 파일 참고하여 각자의 개발환경에 맞게 설정)
* DB Migration
  * DB Init : mvn initialize flyway:init
  * Migration : mvn initialize flyway:migrate
  * SASS runtime build : mvn sass:watch

### Application Configuration
* /src/main/resources/application.properties 생성 (application_sample.properties 파일참고)
* server_url : root url 명세
  * mode : 개발환경 명세 ( development OR production)
  * Email 서버 설정은 각 개인 설정에 맞춰서 변경

### Oracle JDBC Maven 설정(ojdbc14.jar 다운로드)
mvn install:install-file -Dfile=/다운로드절대경로/ojdbc14.jar -DgroupId=com.oracle -DartifactId=ojdbc14 -Dversion=10.2.0.5 -Dpackaging=jar

### Markany Library Maven 설정( libMarkAnyMaPreprocessor.jar 다운로드 필요)
mvn install:install-file -Dfile=/다운로드절대경로/libMarkAnyMaPreprocessor.jar -DgroupId=com.markany -DartifactId=markany -Dversion=1.0.0 -Dpackaging=jar


### SSL SETTING
1. 임시 Keystore 파일 생성: (필요한 폴더 생성및 alias 이름과 생성될 keystore file 이름 개인별로 지정 )
    keytool -genkey -alias gunro -keyalg RSA -keystore /Users/gunro/temp_keystore/gunro.keystore

2. Tomcat server.xml 의 SSL Connector 설정:
    예)

    <Connector port="8443" 
    	       protocol="org.apache.coyote.http11.Http11Protocol"
               maxThreads="150" 
               SSLEnabled="true" 
               keystoreFile="/Users/gunro/temp_keystore/gunro.keystore"
               keystorePass="promptech" 
               scheme="https" secure="true"
               clientAuth="false" sslProtocol="TLS" /> 

3. Spring Security 설정: web.xml 에 아래 부분 추가 (현재 프로젝트에는 적용되어 있음. 필요 x )

     <context-param>
          <param-name>contextConfigLocation</param-name>
          <param-value>/WEB-INF/spring/root-context.xml
           /WEB-INF/spring/appServlet/application-security.xml           
          </param-value>
     </context-param>

     <filter>
       <filter-name>springSecurityFilterChain</filter-name>
       <filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
	</filter>
	<filter-mapping>
	       <filter-name>springSecurityFilterChain</filter-name>
	       <url-pattern>/*</url-pattern>
	</filter-mapping>